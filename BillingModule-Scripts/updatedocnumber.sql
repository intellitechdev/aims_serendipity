ALTER PROC _Billing_Module_UpdateDocNumber
	@docnumber varchar(250),
	@dateused smalldatetime
AS
BEGIN
	UPDATE dbo.mDocNumber
		SET 
			fdDateUsed = @dateused 
	WHERE 
		  fcDocNumber = @docnumber 
	AND
		  [fcDocType] = 'BS'
END
