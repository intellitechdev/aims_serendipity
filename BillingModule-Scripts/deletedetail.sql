ALTER PROC _Billing_Module_Delete
	@docNumber varchar(50)
AS
BEGIN
	DELETE FROM [dbo].[Billing_Details_v2]
	WHERE fcDocNumber = @docNumber
END