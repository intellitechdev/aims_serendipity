ALTER PROC _Billing_Module_Delete_Header
	@docnumber varchar(50)
AS
BEGIN
	DELETE FROM [dbo].[Billing_Header]WHERE [fcDocNumber] = @docnumber
	DELETE FROM [dbo].[Billing_Details_v2]WHERE [fcDocNumber] = @docnumber
	UPDATE dbo.mDocNumber
		SET 
			fdDateUsed = null 
	WHERE 
		  fcDocNumber = @docnumber 
	AND
		  [fcDocType] = 'BS'
END