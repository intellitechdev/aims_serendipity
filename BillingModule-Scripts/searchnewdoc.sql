USE [Coop_accounting_ledger]
GO
/****** Object:  StoredProcedure [dbo].[_Billing_SelectDocNumber]    Script Date: 7/28/2015 3:14:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [_Billing_SelectDocNumber] 'b'
ALTER PROC [dbo].[_Billing_SelectDocNumber]
	@search varchar(250)
AS
BEGIN
	SELECT fcDocNumber as [Document Number]
	FROM dbo.mDocNumber
	WHERE 
		@search = '' and fdDateUsed is null and fcDocType = 'BS'
	or
		fcDocNumber LIKE '%' + @search + '%'  and fdDateUsed is null and fcDocType = 'BS'
END
