ALTER PROC _Billing_Final_LoadFilters
	@type as varchar(50)
AS
BEGIN
	if @type = 'Group'
		Begin
			SELECT distinct fcGroup FROM [Coop_Membership_Development].dbo.cims_m_Member
			where fcGroup is not null and fcGroup <> '' order by fcGroup asc
		END
	if @type = 'SubGroup'
		Begin
			SELECT distinct fcSubGroup FROM [Coop_Membership_Development].dbo.cims_m_Member
			where fcSubGroup is not null and fcSubGroup <> '' order by fcSubGroup asc			
		End
	if	@type = 'Category'
		Begin
			SELECT distinct fcCategory FROM [Coop_Membership_Development].dbo.cims_m_Member
			where fcCategory is not null and fcCategory <> '' order by fcCategory asc	
		END
	if @type = 'type'
		Begin
			SELECT distinct fcType FROM [Coop_Membership_Development].dbo.cims_m_Member
			where fcType is not null and fcType <> '' order by fcType asc	
		End
END