ALTER PROC _Billing_Module_SelectExisting
	@docNumber varchar(50)
AS
BEGIN
	SELECT
		fcIDNo, fcName, fcLoanRef, fcAcctRef, fcCode, fcTitle, fdDueAmt, fdPastDue, fdTotal
	FROM [dbo].[Billing_Details_v2]
	WHERE [fcDocNumber] = @docNumber
END