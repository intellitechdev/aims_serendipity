USE [Coop_accounting_ledger]
GO
/****** Object:  StoredProcedure [dbo].[_Billing_Module_Generate]    Script Date: 7/27/2015 10:37:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Billing Generate 
-- Created by : Vincent Nacar
-- 7/27/15


-- _Billing_Module_Generate '12/30/15','All','','',''
ALTER PROC [dbo].[_Billing_Module_Generate]
	@duedate as smalldatetime,
	@xGroup VARCHAR(100)='',
	@Subgroup VARCHAR(100)='',
	@Category VARCHAR(100)= '',
	@Type VARCHAR(100)='',
	@Rank VARCHAR(100)='',
	@Status VARCHAR(100)=''
AS
BEGIN

	declare @table as table(vIDNo varchar(250),
	vName varchar(250),vLoanRef varchar(50),vAcctRef varchar(50),vCode varchar(50),
	vTitle varchar(250),vDueAmt decimal(18,2),vPastAmt decimal(18,2), vTotal decimal(18,2),fkLoanType int,ctr int,terms decimal(18,2),principalAmt decimal(18,2))

	-- Select Principal Due/Past 
	INSERT INTO @table(vIDNo,vName,vLoanRef,vAcctRef,vCode,vTitle,vDueAmt,vPastAmt,vTotal,fkLoanType,ctr,terms,principalAmt)
	SELECT 
		c.fcEmployeeNo as [ID no],
		c.fcLastName + ', ' + c.fcFirstname as [Name],
		a.fnLoanNo as [Loan Ref],
		'' as [Acct Ref],
		lt.fcAccountCode as [Code],
		lt.fcLoanTypeName as [Title],
		isnull(ams.fdPrincipal,'0.00') as [Due Principal],
		isnull(amb.fdPrincipal,'0.00') as [Past Due Principal],
		isnull(ams.fdPrincipal,0) + isnull(amb.fdPrincipal,0) as [Total],
		lt.fnLoanTypeCode,
		0,
		a.fnTermsInMonths,
		a.fdPrincipal
	FROM [Coop_Membership_Development].dbo.cims_t_member_Loans a
	INNER JOIN [Coop_Membership_Development].dbo.cims_m_member c on a.fk_Employee = c.pk_Employee
	LEFT JOIN [Coop_Membership_Development].dbo.cims_m_loans_Loantype lt on lt.pk_LoanType = a.fk_LoanType
	LEFT JOIN [Coop_Membership_Development].dbo.cims_t_member_amortizationschedule ams on a.fnLoanNo = ams.fcLoanNo
	LEFT JOIN [Coop_Membership_Development].dbo.cims_t_member_amortizationBalances amb on a.fnLoanNo = amb.fcLoanNo and amb.fcDate < @duedate
	WHERE
		ams.fcDate = @duedate
	AND
	((@xGroup = 'All' and @Subgroup = '' and @Category = '' and @Type  = '' and @Rank = '' and @Status = '' and fbDeleted = 0)-- Default All
	or (@xGroup = 'All'  and C.fcSubgroup = @Subgroup and @Category = '' and @Type = '' and @Rank = '' and @Status = '' and fbDeleted = 0 )
	or (@xGroup = 'All'  and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and @Type = '' and @Rank = '' and @Status = '' and fbDeleted = 0 )-- All,Category
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and C.fcType = @Type and @Rank = '' and @Status = '' and fbDeleted = 0) -- All , Category,Type
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and C.fcType = @Type 
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All + Rank
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and C.fcType = @Type 
		and C.fcRank = @Rank and fbDeleted = 0)-- All Filter
		
	-- Independent Filtering
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and @Type = ''
		and @Rank = '' and fbDeleted = 0)-- All + Status
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and @Type = ''
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All + Rank
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and @Rank = '' and  @Status = '' and fbDeleted = 0)-- All + Type
	or (@xGroup = 'All' and @Subgroup = '' and C.fcCategory = @Category and @Type = ''
		and @Rank = '' and @Status = '' and fbDeleted = 0)-- All + Category
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and @Category = '' and @Type = ''
		and @Rank = '' and @Status = '' and fbDeleted = 0)-- All + Subgroup
		
	-- Phase 1:
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and @Category = '' and @Type = ''
		and C.fcRank = @Rank and fbDeleted = 0)-- All,Subgroup,Stat,Rank
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and @Category = '' and C.fcType = @Type 
		and C.fcRank = @Rank and fbDeleted = 0)-- All,Subgroup,Stat,Rank,Type
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and @Category = '' and C.fcType = @Type 
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All,Subgroup,Type,Rank
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and @Type = ''
		and @Rank = '' and fbDeleted = 0)-- All,Subgroup,Stat,Category
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and  @Type = ''
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All,Subgroup,Category,Rank
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and @Category = '' and C.fcType = @Type 
		and @Rank = ''  and fbDeleted = 0)-- All,Subgroup,Type,Stat
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and @Category = '' and C.fcType = @Type 
		and @Rank = '' and @Status = '' and fbDeleted = 0)-- All+Subgroup+ Type	
	or (@xGroup = 'All' and C.fcSubgroup = @Subgroup and C.fcCategory = @Category and  @Type = '' 
		and C.fcRank = @Rank and fbDeleted = 0)-- All+Subgroup+Category+Rank+Status
						
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and @Type = ''
		and C.fcRank = @Rank and fbDeleted = 0)-- All,Stat,Rank
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and C.fcRank = @Rank and fbDeleted = 0)-- All,Stat,Rank,Type
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All,Type,Rank
	or (@xGroup = 'All' and @Subgroup = '' and C.fcCategory = @Category and @Type = ''
		and @Rank = '' and fbDeleted = 0)-- All,Stat,Category
	or (@xGroup = 'All' and @Subgroup = '' and C.fcCategory = @Category and  @Type = ''
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All,Category,Rank
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and @Rank = '' and fbDeleted = 0)-- All,Type,Stat
	or (@xGroup = 'All' and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and @Rank = '' and @Status = '' and fbDeleted = 0)-- All+ Type	
	or (@xGroup = 'All' and @Subgroup = '' and C.fcCategory = @Category and  @Type = '' 
		and C.fcRank = @Rank and fbDeleted = 0)-- All+Category+Rank+Status
		
	-- Phase 2:
	or (C.fcGroup = @xGroup and @Subgroup = '' and @Category = '' and @Type = ''
		and C.fcRank = @Rank and fbDeleted = 0)-- All,Stat,Rank
	or (C.fcGroup = @xGroup and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and C.fcRank = @Rank and fbDeleted = 0)-- All,Stat,Rank,Type
	or (C.fcGroup = @xGroup and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All,Type,Rank
	or (C.fcGroup = @xGroup and @Subgroup = '' and C.fcCategory = @Category and @Type = ''
		and @Rank = ''  and fbDeleted = 0)-- All,Stat,Category
	or (C.fcGroup = @xGroup and @Subgroup = '' and C.fcCategory = @Category and  @Type = ''
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All,Category,Rank
	or (C.fcGroup = @xGroup and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and @Rank = '' and fbDeleted = 0)-- All,Type,Stat
	or (C.fcGroup = @xGroup and @Subgroup = '' and @Category = '' and C.fcType = @Type 
		and @Rank = '' and @Status = '' and fbDeleted = 0)-- All+ Type
	or (C.fcGroup = @xGroup and @Subgroup = '' and C.fcCategory = @Category and  @Type = '' 
		and C.fcRank = @Rank and fbDeleted = 0)-- All+Category+Rank+Status
	
	or	(C.fcGroup = @xGroup and @Subgroup = '' and  @Category = '' and @Type  = '' and @Rank = '' and @Status = '' and fbDeleted = 0)-- Default All
	or (C.fcGroup = @xGroup and @Subgroup = ''  and C.fcCategory = @Category and @Type = '' and @Rank = '' and @Status = '' and fbDeleted = 0)-- All,Category
	or (C.fcGroup = @xGroup  and C.fcSubgroup = @Subgroup and @Category = '' and @Type = '' and @Rank = '' and @Status = '' and fbDeleted = 0)-- All,Subgroup
	or (C.fcGroup = @xGroup and @Subgroup = ''  and C.fcCategory = @Category and C.fcType = @Type and @Rank = '' and @Status = '' and fbDeleted = 0) -- All , Category,Type
	or (C.fcGroup = @xGroup and @Subgroup = ''  and C.fcCategory = @Category and C.fcType = @Type 
		and C.fcRank = @Rank and @Status = '' and fbDeleted = 0)-- All + Rank
	or (C.fcGroup = @xGroup and @Subgroup = ''  and C.fcCategory = @Category and C.fcType = @Type 
		and C.fcRank = @Rank and fbDeleted = 0 ))


	-- Select additional deductions
	INSERT INTO @table(vIDNo,vName,vLoanRef,vAcctRef,vCode,vTitle,vDueAmt,vPastAmt,vTotal,fkLoanType,ctr)
	SELECT distinct 
		b.vIDNo,
		b.vName,
		b.vLoanRef,

		ISNULL((SELECT
			ar.fcDocNumber
		 FROM Coop_Membership_Development.dbo.CIMS_Masterfile_AccountRegister ar
		 LEFT JOIN Coop_Membership_Development.dbo.CIMS_Masterfile_DebitCredit dc on
		 dc.fxkey_AccountID = ar.fxKey_Account
		 WHERE  dc.fcAccountCode COLLATE DATABASE_DEFAULT = c.acnt_code COLLATE DATABASE_DEFAULT 
			AND	ar.fcEmployeeNo COLLATE DATABASE_DEFAULT  = b.vIDNo COLLATE DATABASE_DEFAULT ),' ') as [Acct. Ref],
					
		c.acnt_code as [code],
		a.[Description],

		ISNULL((SELECT CASE a.AcctType
				WHEN 'Interest'
					THEN 
					(SELECT ISNULL(fdInterest,0.00) FROM [Coop_Membership_Development].dbo.CIMS_t_Member_AmortizationSchedule 
					WHERE fcLoanNo COLLATE DATABASE_DEFAULT = b.vLoanRef COLLATE DATABASE_DEFAULT 
					and fcStatus = 'Unpaid' and fcDate = @duedate)
				WHEN 'Interest Income'
					THEN 
					(SELECT ISNULL(fdInterest,0.00) FROM [Coop_Membership_Development].dbo.CIMS_t_Member_AmortizationSchedule 
					WHERE fcLoanNo COLLATE DATABASE_DEFAULT = b.vLoanRef COLLATE DATABASE_DEFAULT 
					and fcStatus = 'Unpaid' and fcDate = @duedate)					
				WHEN 'Others'
					THEN cast(((b.principalAmt * ([Percent]/100)) / b.terms) as decimal(18,2))
				END),'0.00') as [due amount],

		ISNULL((SELECT CASE a.AcctType
				WHEN 'Interest'
					THEN 
					(SELECT ISNULL(fdInterest,0.00) FROM [Coop_Membership_Development].dbo.cims_t_member_amortizationBalances 
					WHERE fcLoanNo COLLATE DATABASE_DEFAULT = b.vLoanRef COLLATE DATABASE_DEFAULT 
					 and fcDate < @duedate)
				WHEN 'Interest Income'
					THEN 
					(SELECT ISNULL(fdInterest,0.00) FROM [Coop_Membership_Development].dbo.cims_t_member_amortizationBalances 
					WHERE fcLoanNo COLLATE DATABASE_DEFAULT = b.vLoanRef COLLATE DATABASE_DEFAULT 
					 and fcDate < @duedate)					
				WHEN 'Others'
					THEN '0.00'
				END),'0.00') as [past due],

		'0.00',
		b.fkLoanType,
		1
	FROM [Coop_Membership_Development].[dbo].[CIMS_m_Loans_Accounts] a
	INNER JOIN @table b on a.pk_LoanTypeID = b.fkLoanType
	INNER JOIN [coop_accounting].dbo.mAccounts c on a.fk_Account = c.acnt_id
	WHERE a.InPayment = 1 


	-- Select result
	SELECT vIDNo,vName,vLoanRef,vAcctRef,vCode,vTitle,vDueAmt,vPastAmt,(vDueAmt + vPastAmt) as [total] FROM @table order by vName,vLoanRef,ctr

END