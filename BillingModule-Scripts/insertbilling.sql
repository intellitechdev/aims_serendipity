ALTER PROC _Billing_Module_InsertDetail
	@docNumber varchar(50),
	@fcIDNo varchar(250),
	@fcName varchar(250),
	@fcLoanRef varchar(50),
	@fcAcctRef varchar(50),
	@fcCode varchar(50),
	@fcTitle varchar(50),
	@fdDueAmt decimal(18,2),
	@fdPastDue decimal(18,2),
	@fdTotal decimal(18,2)
AS
BEGIN
	INSERT INTO [dbo].[Billing_Details_v2]
	(fcDocNumber, fcIDNo, fcName, fcLoanRef, fcAcctRef, fcCode, fcTitle, fdDueAmt, fdPastDue, fdTotal)
	VALUES
	(@docNumber,@fcIDNo,@fcName,@fcLoanRef,@fcAcctRef,@fcCode,@fcTitle,@fdDueAmt,@fdPastDue,@fdTotal)
END