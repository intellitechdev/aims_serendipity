-- _Billing_Module_Report 'test123'
ALTER PROC _Billing_Module_Report
	@docnumber varchar(50)
AS
BEGIN
	SELECT 
		a.fcIDNo,a.fcName,
		(Select sum(fdTotal) from [Billing_Details_v2]
		 where fcIDNo = a.fcIDNo
			and
			   fcDocNumber = @docnumber) as [Total],
		(select dtDate from [dbo].[Billing_Header] where fcDocNumber = @docnumber) as [date],
		(select fcCreatedby from [dbo].[Billing_Header] where fcDocNumber = @docnumber) as [prepared by]
	FROM [dbo].[Billing_Details_v2] a
	
	WHERE
		fcDocNumber = @docnumber

	GROUP BY a.fcIDNo,a.fcName
	ORDER BY a.fcName

END
