USE [Coop_accounting_ledger]
GO
/****** Object:  StoredProcedure [dbo].[_Billing_Search_DocNo]    Script Date: 7/28/2015 2:22:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- _Billing_Search_DocNo ''
ALTER PROC [dbo].[_Billing_Search_DocNo]
	@key varchar(250)
AS
BEGIN
	SELECT
		fcDocNumber as [Document Number],
		CAST(dtDate as varchar(12)) as [Date],
		[fcCreatedBy] as [Created by]
	FROM dbo.Billing_Header 
	WHERE
		@key = ''
	or
		fcDocNumber LIKE '%' + @key +'%'
	ORDER BY fcDocNumber asc
END
