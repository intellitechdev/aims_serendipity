﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmRptCashPosition

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub SetupColumnsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetupColumnsToolStripMenuItem.Click
        frmCPR_Setup.StartPosition = FormStartPosition.CenterScreen
        frmCPR_Setup.ShowDialog()
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Sub _Print_report()
        Try
            Me.Text = "Daily Cash Position Report"
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\CashPosition.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@Dt", dtDate.Text)
            crvRpt.ReportSource = rptsummary
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        _Print_report()
    End Sub

End Class