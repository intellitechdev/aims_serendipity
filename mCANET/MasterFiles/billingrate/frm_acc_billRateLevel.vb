Imports Microsoft.ApplicationBlocks.Data

Public Class frm_acc_billRateLevel

    Private sKeyBillRate As String
    Private sHourlyRate As Decimal
    Private gCon As New Clsappconfiguration

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_billRateLevelAdd.MdiParent = Me.MdiParent
        frm_MF_billRateLevelAdd.ShowDialog()
    End Sub

    Private Sub EditPriceLevelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        With frm_MF_billRateLevelEdit
            '     .MdiParent = Me.MdiParent
            .Text = "Edit Billing Rate Level"
            .KeyBillRate = sKeyBillRate
            .ShowDialog()
        End With
    End Sub

    Private Sub DuplicateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Duplicate.Click
        With frm_MF_billRateLevelEdit
            .MdiParent = Me.MdiParent
            .HourlyRate = sHourlyRate
            .ShowDialog()
        End With
    End Sub

    Private Sub frm_acc_billRateLevel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshform()
    End Sub

    Private Sub refreshform()
        m_loadBillingRate(grd_BillRateLev)
        grdSettings()
        'sKeyBillRate = grd_BillRateLev.Item(0, 0).Value.ToString
    End Sub

    Private Sub deleteBillRateLevel()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mBillRateLevel WHERE fxKeyBillRate = '" & sKeyBillRate & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub grdSettings()
        With grd_BillRateLev
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(0).Visible = False
            .Columns(1).HeaderText = "Bill Rate Name"
            .Columns(2).HeaderText = "Rate Type"
            .Columns(3).HeaderText = "Hourly Rate"
        End With
    End Sub

    Private Sub grd_BillRateLev_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grd_BillRateLev.CellClick
        With grd_BillRateLev
            If .SelectedRows.Count > 0 AndAlso Not .SelectedRows(0).Index = _
                                                .Rows.Count - 1 Then
                sKeyBillRate = grd_BillRateLev.CurrentRow.Cells(0).Value.ToString
                sHourlyRate = grd_BillRateLev.CurrentRow.Cells(3).Value.ToString
            End If
        End With
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        deleteBillRate()
        refreshform()
    End Sub

    Private Sub deleteBillRate()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mBillRateLevel WHERE fxKeyBillRate = '" & sKeyBillRate & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

End Class