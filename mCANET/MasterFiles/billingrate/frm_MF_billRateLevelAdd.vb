Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_billRateLevelAdd
    Private gCon As New Clsappconfiguration
    Private sKeyBillRate As String = Guid.NewGuid.ToString
    Private sFormOrigin As String
    Public Property FormOrigin() As String
        Get
            Return sFormOrigin
        End Get
        Set(ByVal value As String)
            sFormOrigin = value
        End Set
    End Property

    Public Property KeyBillRate() As String
        Get
            Return sKeyBillRate
        End Get
        Set(ByVal value As String)
            sKeyBillRate = value
        End Set
    End Property

    Private Sub frm_MF_billRateLevelAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rbtnFixed.Checked = True
    End Sub

    Private Sub settings()
        If rbtnFixed.Checked = True Then
            txtHourlyRate.Enabled = True
        Else
            txtHourlyRate.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Sub rbtnFixed_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnFixed.CheckedChanged
        settings()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        BillRateLevel_Add()
        If sFormOrigin = "frm_vend_masterVendorAddEdit" Then
            With frm_vend_masterVendorAddEdit
                'm_loadBillingRate(.cboBillingRateLvlName, .cboBillingRateLvlID)
            End With
        Else
            With frm_acc_billRateLevel
                m_loadBillingRate(.grd_BillRateLev)
            End With
        End If
        Me.Close()
    End Sub
   
    Private Sub BillRateLevel_Add()
        Dim sSQL_CommandText As String = "usp_m_BillRateLevel_Insert "
        sSQL_CommandText &= "@fxKeyBillRate='" & sKeyBillRate & "'"
        sSQL_CommandText &= ",@fcBillRateName='" & txtBillRateName.Text & "'"

        'sSQL_CommandText &= ",@fbFixedRate='" & rbtnFixed.Checked & "'"

        If rbtnFixed.Checked Then
            sSQL_CommandText &= ",@fbFixedRate='" & "1'"
        Else
            sSQL_CommandText &= ",@fbFixedRate='" & "0'"
        End If
        sSQL_CommandText &= ",@fxKeyCompany='" & gCompanyID() & "'"

        If rbtnFixed.Checked Then
            sSQL_CommandText &= ",@fdHourlyRate ='" & txtHourlyRate.Text & "'"
        End If

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, Data.CommandType.Text, sSQL_CommandText)
            MessageBox.Show("Bill Rate Level successfully updated", "Add/Edit Bill Rate Level ")
        Catch
            MessageBox.Show(Err.Description, "Add/Edit Bill Rate Level")
        End Try

    End Sub

End Class