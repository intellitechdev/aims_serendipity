﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmCashBalanceAccount
    Private gCon As New Clsappconfiguration()

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        Dim acntID As String
        Dim acntCode As String
        Dim acntName As String
        frmCOAFilter.xModule = "Cash Balance"
        frmCOAFilter.ShowDialog()
        If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
            acntID = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
            acntCode = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_code").Value.ToString()
            acntName = frmCOAFilter.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            SaveAccount(acntID, acntCode, acntName)
        End If
    End Sub

    Private Sub SaveAccount(ByVal acntID As String, ByVal acntCode As String, ByVal acntTitle As String)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CashBalance_Insert", _
                New SqlParameter("@acntID", acntID),
                New SqlParameter("@acntCode", acntCode),
                New SqlParameter("@acntTitle", acntTitle),
                New SqlParameter("@coid", gCompanyID))

        LoadAccounts()
    End Sub

    Private Sub LoadAccounts()
        grdList.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CashBalance_List",
                                                                            New SqlParameter("@coid", gCompanyID))
        While rd.Read = True
            Try
                Dim row As String() =
                 {rd.Item("acntID").ToString, rd.Item("acntCode").ToString, rd.Item("acntTitle").ToString}

                Dim nRowIndex As Integer
                With grdList

                    .Rows.Add(row)
                    '.ClearSelection()
                    nRowIndex = .Rows.Count - 1
                    .FirstDisplayedScrollingRowIndex = nRowIndex
                    '.Rows.Add()
                End With
            Catch
                MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        End While
    End Sub

    Private Sub CashBalanceAccount_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadAccounts()
    End Sub

    Private Sub RemoveToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveToolStripMenuItem.Click
        Dim cdelete As String
        Dim i As Integer = grdList.CurrentRow.Index
        cdelete = grdList.Item("acntID", i).Value.ToString
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CashBalance_Delete", _
                New SqlParameter("@acntID", cdelete))

        grdList.Rows.Remove(grdList.CurrentRow)
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class