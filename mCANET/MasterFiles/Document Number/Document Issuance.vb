﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class Document_Issuance

    Private Sub Document_Issuance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadDoctype()
        Call ChkPreceedStat()
    End Sub

#Region "Variables"
    Private gCon As New Clsappconfiguration()
    Private DocType As String
    Private lvlIndex As String
    Private DocNum As String
#End Region

#Region "Events"

    Private Sub LoadDoctype()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("Masterfile_LoadDoctype", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Document")
            With cboDoctype
                .ValueMember = "pk_Initial"
                .DisplayMember = "fcDocName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Document Type")
        End Try
    End Sub

    Private Sub DeleteDocNum(ByVal lvlIndex As String, ByVal doctype As String, ByVal coid As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As New DataSet
        Try
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_CheckDependencies", _
                                       New SqlParameter("@docnum", lvlIndex))
            If ds.Tables(0).Rows.Count >= 1 Then
                MsgBox("Document Number " & lvlIndex & " in use.", MsgBoxStyle.Critical, "Document Number Issuance")
            Else
                SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Delete", _
                                       New SqlParameter("@fcDocNumber", lvlIndex),
                                       New SqlParameter("@fcDocType", doctype),
                                       New SqlParameter("@coid", coid))
            End If
        Catch ex As Exception
            MsgBox("Document Number " & lvlIndex & " in use.", MsgBoxStyle.Critical, "Document Number Issuance")
        End Try
    End Sub

    Private Sub ViewDocNum(ByVal doctype As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_List", _
                                   New SqlParameter("@fcDocType", doctype),
                                   New SqlParameter("@coid", gCompanyID))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Document Number", 200, HorizontalAlignment.Center)
            .Columns.Add("Date Used", 150, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "View Document Number")
            End Try
        End With
    End Sub

    Private Sub AddDocNum(ByVal DocType As String, ByVal Initials As String, ByVal DocNum As String)
        Dim mycon As New Clsappconfiguration
        SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Add", _
                                   New SqlParameter("@fcDocType", DocType),
                                   New SqlParameter("@fcDocNumber", DocNum),
                                   New SqlParameter("@Initials", Initials),
                                   New SqlParameter("@coid", gCompanyID()))
        Call ViewDocNum(cboDoctype.Text)
    End Sub

    Private Sub SearchDocNum(ByVal DocNum As String, ByVal DocType As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Search", _
                                   New SqlParameter("@fcDocNumber", DocNum),
                                   New SqlParameter("@fcDocType", DocType),
                                   New SqlParameter("@coid", gCompanyID()))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True

            .Columns.Add("Document Number", 200, HorizontalAlignment.Center)
            .Columns.Add("Date Used", 150, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                'rd.Close()
            Catch ex As Exception
                'MessageBox.Show(ex.Message, "View Document Number")
            Finally
                rd.Close()
            End Try
        End With
    End Sub

    Private Sub ChkPreceedStat()
        If chkPreceedingZero.Checked = False Then
            cboPreceedingZero.Enabled = False
            cboPreceedingZero.Text = "1"
        Else
            cboPreceedingZero.Enabled = True
        End If
    End Sub

    Private Sub CheckEmptyFields()
        If cboDoctype.Text = "" Then
            MsgBox("Please Select Document Type First.", MsgBoxStyle.Exclamation, "Document Number Issuance")
            cboDoctype.DroppedDown = True
        ElseIf txtDocNum.Text <> "" And txtQuantity.Text = "" Then
            MsgBox("Please Set Document Quantity.", MsgBoxStyle.Exclamation, "Document Number Issuance")
            txtQuantity.Focus()
        ElseIf txtDocNum.Text = "" And txtQuantity.Text <> "" Then
            MsgBox("Nothing to add.", MsgBoxStyle.Exclamation, "Document Number Issuance")
            txtDocNum.Focus()
        End If
    End Sub
#End Region

#Region "Buttons"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If MsgBox("Delete selected document number(s)?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Document Number Issuance") = MsgBoxResult.Yes Then
            For i As Integer = 0 To listDocNumber.Items.Count - 1
                If listDocNumber.Items(i).Checked = True Then
                    Call DeleteDocNum(listDocNumber.Items(i).Text, cboDoctype.Text, gCompanyID())
                End If
            Next
            Call ViewDocNum(DocType)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        CheckEmptyFields()

        Dim DocumentNumber As String
        'Counters
        Dim DocCounter As String
        Dim NumCounter As Integer

        DocCounter = txtDocNum.Text
        NumCounter = Val(txtQuantity.Text)

        Try
            While NumCounter <> 0
                DocumentNumber = DocCounter.ToString.PadLeft(cboPreceedingZero.Text, "0"c)

                'Check if Duplicate Document Number
                Dim mycon As New Clsappconfiguration
                Dim rd As SqlDataReader
                rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Search_Duplicate", _
                                           New SqlParameter("@fcDocType", cboDoctype.Text),
                                           New SqlParameter("@fcDocNumber", DocumentNumber),
                                           New SqlParameter("@Initials", txtInitials.Text),
                                           New SqlParameter("@coid", gCompanyID()))
                If rd.Read = True Then
                    MsgBox("Duplicate Document Number")
                    Exit While
                End If

                'Add Document Number
                Call AddDocNum(cboDoctype.Text, txtInitials.Text, DocumentNumber)
                DocCounter = DocCounter + 1
                NumCounter = NumCounter - 1

                'Display after successful adding of document number
                If NumCounter = 0 Then
                    MsgBox("Add Successful")
                    txtInitials.Text = ""
                    txtDocNum.Text = ""
                    txtQuantity.Text = ""
                    cboPreceedingZero.Text = ""
                    chkPreceedingZero.Checked = False
                End If

            End While

        Catch ex As Exception

        End Try
    End Sub


#End Region

#Region "Keypress"
    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtDocNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDocNum.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtInitials_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtInitials.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 96 Or Asc(e.KeyChar) > 122 Then
                e.Handled = True
            End If
        End If
    End Sub
#End Region

    Private Sub cboDoctype_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cboDoctype.MouseWheel
        Dim disable As HandledMouseEventArgs = e
        disable.Handled = True
    End Sub

    Private Sub cboDoctype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDoctype.TextChanged
        DocType = cboDoctype.Text
        Call ViewDocNum(DocType)
    End Sub

    Private Sub listDocNumber_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles listDocNumber.Click
        Try
            listDocNumber.SelectedItems(0).Checked = True
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        DocNum = txtSearch.Text
        Call SearchDocNum(DocNum, DocType)
    End Sub

    Private Sub chkPreceedingZero_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPreceedingZero.Click
        ChkPreceedStat()
    End Sub

    Private Sub txtDocNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDocNum.TextChanged

    End Sub
End Class