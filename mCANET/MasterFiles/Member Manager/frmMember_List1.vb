﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_List1

    Private Sub frmMember_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_Member_List()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If GrdAccounts.SelectedCells.Count <> 0 Then
            For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                frmMasterfile_AccountRegister.GetEmployeeID = row.Cells(1).Value.ToString
                frmMasterfile_AccountRegister.txtName.Text = row.Cells(2).Value.ToString
            Next
            Me.Close()
        End If
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub GrdAccounts_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrdAccounts.DoubleClick
        If GrdAccounts.SelectedCells.Count <> 0 Then
            For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                frmMasterfile_AccountRegister.GetEmployeeID = row.Cells(1).Value.ToString
                frmMasterfile_AccountRegister.txtName.Text = row.Cells(2).Value.ToString
            Next
            Me.Close()
        End If
    End Sub

    Private Sub txtEmployeeNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmployeeNo.TextChanged
        Search_MemberByID(txtEmployeeNo.Text)
    End Sub

    Private Sub txtLastName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLastName.TextChanged
        Search_MemberByLast(txtLastName.Text)
    End Sub

    Private Sub txtEmployeeNo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEmployeeNo.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtEmployeeNo.Text <> "" Then
                Search_MemberByID(txtEmployeeNo.Text)
            End If
        End If
    End Sub

    Private Sub Load_Member_List()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Member_MasterList")
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).HeaderText = "Employee ID"
                .Columns(1).Width = 100
                .Columns(2).HeaderText = "Fullname"
                .Columns(2).Width = 360
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Employee List", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Search_MemberByID(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Member_MasterListByID",
                                                     New SqlParameter("@fcEmployeeNo", ID))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).HeaderText = "Employee ID"
                .Columns(1).Width = 100
                .Columns(2).HeaderText = "Fullname"
                .Columns(2).Width = 360
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Employee List by ID", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Search_MemberByLast(ByVal Last As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Member_MasterListByLastname",
                                                     New SqlParameter("@fcLastName", Last))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).HeaderText = "Employee ID"
                .Columns(1).Width = 100
                .Columns(2).HeaderText = "Fullname"
                .Columns(2).Width = 360
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Employee List by Lastname", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class