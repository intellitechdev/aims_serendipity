Imports System.Data.SqlClient
Public Class frmEmployee_JobDescription

#Region "variables"
    Public myconnection As New Clsappconfiguration_CMS
    Public getjobdesc_id As String
#End Region
#Region "Get property"
    Public Property getempJobDescID() As String
        Get
            Return getjobdesc_id
        End Get
        Set(ByVal value As String)
            getjobdesc_id = value
        End Set
    End Property
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Call Job_Description_Insert()
        'Call Job_Description_Get()
        Me.Close()
    End Sub
#Region "Employee Job Description Insert / stored proc"
    Private Sub Job_Description_Insert()
        Try
            Dim cmd As New SqlCommand("usp_per_employee_jobdescription_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@IDnumber", SqlDbType.VarChar, 50).Value = frmMember_Master.txtEmployeeNo.Text
                .Add("@ReportingTo", SqlDbType.VarChar, 60).Value = Me.txtReportingto.Text
                .Add("@Location", SqlDbType.VarChar, 100).Value = Me.txtLocation.Text
                .Add("@JobDescription", SqlDbType.Text).Value = Me.txtJobDescription.Text
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    '#Region "Employee Job Description Get / stored proc"
    '    Public Sub Job_Description_Get()
    '        Dim cmd As New SqlCommand("usp_per_employee_jobdescription_get", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        myconnection.sqlconn.Open()
    '        cmd.Parameters.Add("@Idnumber", SqlDbType.VarChar, 50).Value = form6.txtreplicateIDnumber.Text 'form6.tempid_no.Text
    '        With form6.LvlJobDescription
    '            .Clear()
    '            .View = View.Details
    '            .GridLines = True
    '            .FullRowSelect = True
    '            .Columns.Add("Reporting To", 100, HorizontalAlignment.Left)
    '            .Columns.Add("Location", 150, HorizontalAlignment.Left)
    '            .Columns.Add("Job Description", 600, HorizontalAlignment.Left)

    '            Dim myreader As SqlDataReader
    '            myreader = cmd.ExecuteReader
    '            Try
    '                While myreader.Read
    '                    With .Items.Add(myreader.Item(0))
    '                        .SubItems.Add(myreader.Item(1))
    '                        .SubItems.Add(myreader.Item(2))
    '                        .SubItems.Add(myreader.Item(3)) 'idnumber
    '                        .subitems.add(myreader.Item(4)) 'key_id
    '                    End With
    '                End While
    '            Finally
    '                myreader.Close()
    '                myconnection.sqlconn.Close()
    '            End Try
    '        End With
    '    End Sub
    '#End Region

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
       
        Me.Close()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub frmEmployee_JobDescription_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class