﻿'<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_List1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    '<System.Diagnostics.DebuggerNonUserCode()> _
    'Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    '    Try
    '        If disposing AndAlso components IsNot Nothing Then
    '            components.Dispose()
    '        End If
    '    Finally
    '        MyBase.Dispose(disposing)
    '    End Try
    'End Sub

    'Required by the Windows Form Designer
    'Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_List1))
        'Me.PanePanel1 = New WindowsApplication2.PanePanel()
        'Me.Label1 = New System.Windows.Forms.Label()
        'Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.GrdAccounts = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtEmployeeNo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        'Me.PanePanel1.SuspendLayout()
        'Me.PanePanel2.SuspendLayout()
        CType(Me.GrdAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanePanel1
        '
        'Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        'Me.PanePanel1.Controls.Add(Me.Label1)
        'Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        'Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        'Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        'Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        'Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        'Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        'Me.PanePanel1.Name = "PanePanel1"
        'Me.PanePanel1.Size = New System.Drawing.Size(499, 33)
        'Me.PanePanel1.TabIndex = 9
        '
        'Label1
        '
        'Me.Label1.AutoSize = True
        'Me.Label1.BackColor = System.Drawing.Color.Transparent
        'Me.Label1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        'Me.Label1.ForeColor = System.Drawing.Color.White
        'Me.Label1.Location = New System.Drawing.Point(3, 5)
        'Me.Label1.Name = "Label1"
        'Me.Label1.Size = New System.Drawing.Size(102, 22)
        'Me.Label1.TabIndex = 2
        'Me.Label1.Text = "Client List"
        '
        'PanePanel2
        '
        'Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        'Me.PanePanel2.Controls.Add(Me.btnOk)
        'Me.PanePanel2.Controls.Add(Me.btnclose)
        'Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        'Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        'Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        'Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.LawnGreen
        'Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Green
        'Me.PanePanel2.Location = New System.Drawing.Point(0, 316)
        'Me.PanePanel2.Name = "PanePanel2"
        'Me.PanePanel2.Size = New System.Drawing.Size(499, 40)
        'Me.PanePanel2.TabIndex = 10
        '
        'btnOk
        '
        Me.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOk.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        'Me.btnOk.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOk.Location = New System.Drawing.Point(356, 7)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(66, 28)
        Me.btnOk.TabIndex = 6
        Me.btnOk.Text = "Ok"
        Me.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnclose
        '
        Me.btnclose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnclose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(428, 7)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 5
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'GrdAccounts
        '
        Me.GrdAccounts.AllowUserToAddRows = False
        Me.GrdAccounts.AllowUserToDeleteRows = False
        Me.GrdAccounts.AllowUserToResizeRows = False
        Me.GrdAccounts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdAccounts.BackgroundColor = System.Drawing.SystemColors.Window
        Me.GrdAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdAccounts.GridColor = System.Drawing.Color.Black
        Me.GrdAccounts.Location = New System.Drawing.Point(8, 110)
        Me.GrdAccounts.MultiSelect = False
        Me.GrdAccounts.Name = "GrdAccounts"
        Me.GrdAccounts.RowHeadersVisible = False
        Me.GrdAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GrdAccounts.Size = New System.Drawing.Size(487, 200)
        Me.GrdAccounts.TabIndex = 100
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 19)
        Me.Label2.TabIndex = 101
        Me.Label2.Text = "Lastname:"
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(127, 72)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(260, 27)
        Me.txtLastName.TabIndex = 102
        '
        'txtEmployeeNo
        '
        Me.txtEmployeeNo.Location = New System.Drawing.Point(127, 39)
        Me.txtEmployeeNo.Name = "txtEmployeeNo"
        Me.txtEmployeeNo.Size = New System.Drawing.Size(260, 27)
        Me.txtEmployeeNo.TabIndex = 104
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 19)
        Me.Label3.TabIndex = 103
        Me.Label3.Text = "Employee No."
        '
        'frmMember_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(499, 356)
        Me.Controls.Add(Me.txtEmployeeNo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtLastName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GrdAccounts)
        'Me.Controls.Add(Me.PanePanel1)
        'Me.Controls.Add(Me.PanePanel2)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMember_List"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Client List"
        'Me.PanePanel1.ResumeLayout(False)
        'Me.PanePanel1.PerformLayout()
        'Me.PanePanel2.ResumeLayout(False)
        CType(Me.GrdAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    'Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    'Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents GrdAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtEmployeeNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
