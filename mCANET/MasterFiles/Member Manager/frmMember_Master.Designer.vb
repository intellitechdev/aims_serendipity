<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_Master))
        Me.tabMember = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.dtBoardApproval = New System.Windows.Forms.DateTimePicker()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.txtHDMF = New System.Windows.Forms.TextBox()
        Me.txtPhilhealth = New System.Windows.Forms.TextBox()
        Me.txtSSS = New System.Windows.Forms.TextBox()
        Me.txtTin = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.grpPayrollStatus = New System.Windows.Forms.GroupBox()
        Me.rdoNonExempt = New System.Windows.Forms.RadioButton()
        Me.rdoExempt = New System.Windows.Forms.RadioButton()
        Me.chkBereaveYes = New System.Windows.Forms.CheckBox()
        Me.chkBereaveNo = New System.Windows.Forms.CheckBox()
        Me.mem_WithdrawDate = New System.Windows.Forms.DateTimePicker()
        Me.mem_PaycontDate = New System.Windows.Forms.DateTimePicker()
        Me.chkNo = New System.Windows.Forms.CheckBox()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.txtpayrollcontriamount = New System.Windows.Forms.TextBox()
        Me.chkyes = New System.Windows.Forms.CheckBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtwithdrawal = New System.Windows.Forms.TextBox()
        Me.mem_MemberDate = New System.Windows.Forms.DateTimePicker()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.cboemp_status = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtContractPrice = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.txtEcola = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.cbotaxcode = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboPositionLevel = New System.Windows.Forms.ComboBox()
        Me.emp_company = New System.Windows.Forms.TextBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.emp_Ytenures = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.txtlocalofficenumber = New System.Windows.Forms.TextBox()
        Me.btnrestricted = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtofficenumber = New System.Windows.Forms.TextBox()
        Me.txtofficeadd = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.emp_Datehired = New System.Windows.Forms.DateTimePicker()
        Me.cbopayroll = New System.Windows.Forms.ComboBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.cborate = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboEmp_type = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.cbotitledesignation = New System.Windows.Forms.ComboBox()
        Me.txtbasicpay = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnUpdateBA = New System.Windows.Forms.Button()
        Me.btnDeleteBA = New System.Windows.Forms.Button()
        Me.btnNewBA = New System.Windows.Forms.Button()
        Me.lvlBankInfo = New System.Windows.Forms.ListView()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnUpdateSInc = New System.Windows.Forms.Button()
        Me.btnDeleteSInc = New System.Windows.Forms.Button()
        Me.btnsaveSInc = New System.Windows.Forms.Button()
        Me.lvlSourceIncome = New System.Windows.Forms.ListView()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.BtnEditContact = New System.Windows.Forms.Button()
        Me.BtnDeleteContact = New System.Windows.Forms.Button()
        Me.btnaddContact = New System.Windows.Forms.Button()
        Me.lvlNearestRelatives = New System.Windows.Forms.ListView()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.LvlEmployeeDependent = New System.Windows.Forms.ListView()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.emp_pic = New System.Windows.Forms.PictureBox()
        Me.temp_marks = New System.Windows.Forms.TextBox()
        Me.txtitemno = New System.Windows.Forms.TextBox()
        Me.txtremarks = New System.Windows.Forms.TextBox()
        Me.temp_add3 = New System.Windows.Forms.TextBox()
        Me.txtparent_ID = New System.Windows.Forms.TextBox()
        Me.txtmonthreqular = New System.Windows.Forms.TextBox()
        Me.txtkeysection = New System.Windows.Forms.TextBox()
        Me.temp_height = New System.Windows.Forms.TextBox()
        Me.txtdescsection = New System.Windows.Forms.TextBox()
        Me.txtsalarygrade = New System.Windows.Forms.TextBox()
        Me.txtdescdepartment = New System.Windows.Forms.TextBox()
        Me.temp_add2 = New System.Windows.Forms.TextBox()
        Me.txtdescdivision = New System.Windows.Forms.TextBox()
        Me.txtweight = New System.Windows.Forms.TextBox()
        Me.txtkeydepartment = New System.Windows.Forms.TextBox()
        Me.txtdateregular = New System.Windows.Forms.TextBox()
        Me.txtkeydivision = New System.Windows.Forms.TextBox()
        Me.Dateregular = New System.Windows.Forms.DateTimePicker()
        Me.txtkeycompany = New System.Windows.Forms.TextBox()
        Me.emp_extphone = New System.Windows.Forms.MaskedTextBox()
        Me.cbosalarygrade = New System.Windows.Forms.ComboBox()
        Me.temp_citizen = New System.Windows.Forms.TextBox()
        Me.temp_designation = New System.Windows.Forms.TextBox()
        Me.txtType_employee = New System.Windows.Forms.TextBox()
        Me.txtfxkeypositionlevel = New System.Windows.Forms.TextBox()
        Me.txtfullname = New System.Windows.Forms.TextBox()
        Me.TXTRECID = New System.Windows.Forms.TextBox()
        Me.cbodept = New System.Windows.Forms.TextBox()
        Me.Dateresigned = New System.Windows.Forms.DateTimePicker()
        Me.TXTKEYEMPLOYEEID = New System.Windows.Forms.TextBox()
        Me.btnemp_editdepdts = New System.Windows.Forms.Button()
        Me.btndelete_dep = New System.Windows.Forms.Button()
        Me.btnadd_dep = New System.Windows.Forms.Button()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.others = New System.Windows.Forms.TabPage()
        Me.btnEditMobile = New System.Windows.Forms.Button()
        Me.gridMobileNos = New System.Windows.Forms.DataGridView()
        Me.txtEloadingLimit = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtEloaderPIN = New System.Windows.Forms.TextBox()
        Me.chkShowPINChar = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnAddMobileNo = New System.Windows.Forms.Button()
        Me.chkIsEloader = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMobileNo = New System.Windows.Forms.MaskedTextBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.dgvemail = New System.Windows.Forms.DataGridView()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.dgvphone = New System.Windows.Forms.DataGridView()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.dgvcivil = New System.Windows.Forms.DataGridView()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.dgvaddress = New System.Windows.Forms.DataGridView()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.dgvlastname = New System.Windows.Forms.DataGridView()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Cbomem_Status = New System.Windows.Forms.ComboBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.txtMemberID = New System.Windows.Forms.TextBox()
        Me.temp_placebirth = New System.Windows.Forms.TextBox()
        Me.txtage1 = New System.Windows.Forms.TextBox()
        Me.lblage = New System.Windows.Forms.Label()
        Me.EMP_DATEbirth = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.MaskedTextBox5 = New System.Windows.Forms.MaskedTextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtEmployeeNo = New System.Windows.Forms.TextBox()
        Me.temp_fname = New System.Windows.Forms.TextBox()
        Me.temp_lname = New System.Windows.Forms.TextBox()
        Me.temp_midname = New System.Windows.Forms.TextBox()
        Me.txtprovincialadd = New System.Windows.Forms.TextBox()
        Me.cboemp_gender = New System.Windows.Forms.ComboBox()
        Me.cboemp_civil = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtresidencephone = New System.Windows.Forms.MaskedTextBox()
        Me.txtEmailAddress = New System.Windows.Forms.TextBox()
        Me.txtorgchart = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.txtResidenceadd = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.txtDepartment2 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnSearchProvince = New System.Windows.Forms.Button()
        Me.btnSearchHome = New System.Windows.Forms.Button()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtpk_Employee = New System.Windows.Forms.TextBox()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnMaxSignature = New System.Windows.Forms.Button()
        Me.btnMaxPhoto = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.btnBrowseSignature = New System.Windows.Forms.Button()
        Me.picEmpSignature = New System.Windows.Forms.PictureBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnBrowsePicture = New System.Windows.Forms.Button()
        Me.picEmpPhoto = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.cbofGroup = New System.Windows.Forms.ComboBox()
        Me.cbofType = New System.Windows.Forms.ComboBox()
        Me.cbofCategory = New System.Windows.Forms.ComboBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.cboRank = New System.Windows.Forms.ComboBox()
        Me.txtCompanyChart = New System.Windows.Forms.TextBox()
        Me.cboSubgroup = New System.Windows.Forms.ComboBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.btnemp_close = New System.Windows.Forms.Button()
        Me.btnemp_next = New System.Windows.Forms.Button()
        Me.btnemp_delete = New System.Windows.Forms.Button()
        Me.BTNSEARCH = New System.Windows.Forms.Button()
        Me.btnemp_edit = New System.Windows.Forms.Button()
        Me.btnemp_previous = New System.Windows.Forms.Button()
        Me.btnemp_add = New System.Windows.Forms.Button()
        Me.lblemployee_name = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.tabMember.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.grpPayrollStatus.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage17.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.others.SuspendLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.dgvemail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvphone, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvcivil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvaddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvlastname, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.picEmpSignature, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEmpPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabMember
        '
        Me.tabMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabMember.Controls.Add(Me.TabPage5)
        Me.tabMember.Controls.Add(Me.TabPage1)
        Me.tabMember.Controls.Add(Me.TabPage2)
        Me.tabMember.Controls.Add(Me.TabPage4)
        Me.tabMember.Controls.Add(Me.TabPage17)
        Me.tabMember.Controls.Add(Me.TabPage3)
        Me.tabMember.Controls.Add(Me.others)
        Me.tabMember.Controls.Add(Me.TabPage6)
        Me.tabMember.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabMember.ItemSize = New System.Drawing.Size(120, 18)
        Me.tabMember.Location = New System.Drawing.Point(7, 352)
        Me.tabMember.Name = "tabMember"
        Me.tabMember.SelectedIndex = 0
        Me.tabMember.Size = New System.Drawing.Size(1017, 215)
        Me.tabMember.TabIndex = 16
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage5.Controls.Add(Me.dtBoardApproval)
        Me.TabPage5.Controls.Add(Me.Label58)
        Me.TabPage5.Controls.Add(Me.txtHDMF)
        Me.TabPage5.Controls.Add(Me.txtPhilhealth)
        Me.TabPage5.Controls.Add(Me.txtSSS)
        Me.TabPage5.Controls.Add(Me.txtTin)
        Me.TabPage5.Controls.Add(Me.Label57)
        Me.TabPage5.Controls.Add(Me.Label56)
        Me.TabPage5.Controls.Add(Me.Label55)
        Me.TabPage5.Controls.Add(Me.Label54)
        Me.TabPage5.Controls.Add(Me.grpPayrollStatus)
        Me.TabPage5.Controls.Add(Me.chkBereaveYes)
        Me.TabPage5.Controls.Add(Me.chkBereaveNo)
        Me.TabPage5.Controls.Add(Me.mem_WithdrawDate)
        Me.TabPage5.Controls.Add(Me.mem_PaycontDate)
        Me.TabPage5.Controls.Add(Me.chkNo)
        Me.TabPage5.Controls.Add(Me.Label110)
        Me.TabPage5.Controls.Add(Me.txtpayrollcontriamount)
        Me.TabPage5.Controls.Add(Me.chkyes)
        Me.TabPage5.Controls.Add(Me.Label109)
        Me.TabPage5.Controls.Add(Me.Label37)
        Me.TabPage5.Controls.Add(Me.txtwithdrawal)
        Me.TabPage5.Controls.Add(Me.mem_MemberDate)
        Me.TabPage5.Controls.Add(Me.Label111)
        Me.TabPage5.Controls.Add(Me.Label108)
        Me.TabPage5.Controls.Add(Me.Label65)
        Me.TabPage5.Controls.Add(Me.Label105)
        Me.TabPage5.Controls.Add(Me.cboemp_status)
        Me.TabPage5.Controls.Add(Me.Label11)
        Me.TabPage5.Controls.Add(Me.PictureBox8)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Client Information"
        '
        'dtBoardApproval
        '
        Me.dtBoardApproval.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBoardApproval.Location = New System.Drawing.Point(746, 144)
        Me.dtBoardApproval.Name = "dtBoardApproval"
        Me.dtBoardApproval.Size = New System.Drawing.Size(187, 21)
        Me.dtBoardApproval.TabIndex = 129
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label58.Location = New System.Drawing.Point(641, 149)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(95, 15)
        Me.Label58.TabIndex = 128
        Me.Label58.Text = "Board Approval:"
        '
        'txtHDMF
        '
        Me.txtHDMF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHDMF.Location = New System.Drawing.Point(746, 116)
        Me.txtHDMF.Name = "txtHDMF"
        Me.txtHDMF.Size = New System.Drawing.Size(187, 21)
        Me.txtHDMF.TabIndex = 127
        '
        'txtPhilhealth
        '
        Me.txtPhilhealth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPhilhealth.Location = New System.Drawing.Point(746, 89)
        Me.txtPhilhealth.Name = "txtPhilhealth"
        Me.txtPhilhealth.Size = New System.Drawing.Size(187, 21)
        Me.txtPhilhealth.TabIndex = 126
        '
        'txtSSS
        '
        Me.txtSSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSSS.Location = New System.Drawing.Point(746, 62)
        Me.txtSSS.Name = "txtSSS"
        Me.txtSSS.Size = New System.Drawing.Size(187, 21)
        Me.txtSSS.TabIndex = 125
        '
        'txtTin
        '
        Me.txtTin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTin.Location = New System.Drawing.Point(746, 35)
        Me.txtTin.Name = "txtTin"
        Me.txtTin.Size = New System.Drawing.Size(187, 21)
        Me.txtTin.TabIndex = 124
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label57.Location = New System.Drawing.Point(707, 65)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(29, 15)
        Me.Label57.TabIndex = 123
        Me.Label57.Text = "SSS:"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label56.Location = New System.Drawing.Point(656, 92)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(80, 15)
        Me.Label56.TabIndex = 122
        Me.Label56.Text = "PHIL-HEALTH:"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label55.Location = New System.Drawing.Point(693, 120)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(44, 15)
        Me.Label55.TabIndex = 121
        Me.Label55.Text = "HDMF:"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label54.Location = New System.Drawing.Point(708, 41)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(32, 15)
        Me.Label54.TabIndex = 120
        Me.Label54.Text = "TIN: "
        '
        'grpPayrollStatus
        '
        Me.grpPayrollStatus.Controls.Add(Me.rdoNonExempt)
        Me.grpPayrollStatus.Controls.Add(Me.rdoExempt)
        Me.grpPayrollStatus.Location = New System.Drawing.Point(332, 88)
        Me.grpPayrollStatus.Name = "grpPayrollStatus"
        Me.grpPayrollStatus.Size = New System.Drawing.Size(267, 49)
        Me.grpPayrollStatus.TabIndex = 118
        Me.grpPayrollStatus.TabStop = False
        Me.grpPayrollStatus.Text = "Payroll Status"
        '
        'rdoNonExempt
        '
        Me.rdoNonExempt.AutoSize = True
        Me.rdoNonExempt.Location = New System.Drawing.Point(99, 20)
        Me.rdoNonExempt.Name = "rdoNonExempt"
        Me.rdoNonExempt.Size = New System.Drawing.Size(94, 19)
        Me.rdoNonExempt.TabIndex = 1
        Me.rdoNonExempt.Text = "Non-Exempt"
        Me.rdoNonExempt.UseVisualStyleBackColor = True
        '
        'rdoExempt
        '
        Me.rdoExempt.AutoSize = True
        Me.rdoExempt.Checked = True
        Me.rdoExempt.Location = New System.Drawing.Point(10, 20)
        Me.rdoExempt.Name = "rdoExempt"
        Me.rdoExempt.Size = New System.Drawing.Size(67, 19)
        Me.rdoExempt.TabIndex = 0
        Me.rdoExempt.TabStop = True
        Me.rdoExempt.Text = "Exempt"
        Me.rdoExempt.UseVisualStyleBackColor = True
        '
        'chkBereaveYes
        '
        Me.chkBereaveYes.AutoSize = True
        Me.chkBereaveYes.Location = New System.Drawing.Point(153, 126)
        Me.chkBereaveYes.Name = "chkBereaveYes"
        Me.chkBereaveYes.Size = New System.Drawing.Size(46, 19)
        Me.chkBereaveYes.TabIndex = 117
        Me.chkBereaveYes.Text = "Yes"
        Me.chkBereaveYes.UseVisualStyleBackColor = True
        Me.chkBereaveYes.Visible = False
        '
        'chkBereaveNo
        '
        Me.chkBereaveNo.AutoSize = True
        Me.chkBereaveNo.Location = New System.Drawing.Point(211, 126)
        Me.chkBereaveNo.Name = "chkBereaveNo"
        Me.chkBereaveNo.Size = New System.Drawing.Size(42, 19)
        Me.chkBereaveNo.TabIndex = 116
        Me.chkBereaveNo.Text = "No"
        Me.chkBereaveNo.UseVisualStyleBackColor = True
        Me.chkBereaveNo.Visible = False
        '
        'mem_WithdrawDate
        '
        Me.mem_WithdrawDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_WithdrawDate.Location = New System.Drawing.Point(600, 36)
        Me.mem_WithdrawDate.Name = "mem_WithdrawDate"
        Me.mem_WithdrawDate.Size = New System.Drawing.Size(27, 21)
        Me.mem_WithdrawDate.TabIndex = 110
        '
        'mem_PaycontDate
        '
        Me.mem_PaycontDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_PaycontDate.Location = New System.Drawing.Point(492, 142)
        Me.mem_PaycontDate.Name = "mem_PaycontDate"
        Me.mem_PaycontDate.Size = New System.Drawing.Size(104, 21)
        Me.mem_PaycontDate.TabIndex = 1
        Me.mem_PaycontDate.Visible = False
        '
        'chkNo
        '
        Me.chkNo.AutoSize = True
        Me.chkNo.Location = New System.Drawing.Point(211, 38)
        Me.chkNo.Name = "chkNo"
        Me.chkNo.Size = New System.Drawing.Size(42, 19)
        Me.chkNo.TabIndex = 1
        Me.chkNo.Text = "No"
        Me.chkNo.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(453, 144)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(33, 15)
        Me.Label110.TabIndex = 109
        Me.Label110.Text = "Date"
        Me.Label110.Visible = False
        '
        'txtpayrollcontriamount
        '
        Me.txtpayrollcontriamount.AcceptsTab = True
        Me.txtpayrollcontriamount.Location = New System.Drawing.Point(495, 63)
        Me.txtpayrollcontriamount.Name = "txtpayrollcontriamount"
        Me.txtpayrollcontriamount.Size = New System.Drawing.Size(104, 21)
        Me.txtpayrollcontriamount.TabIndex = 0
        Me.txtpayrollcontriamount.Text = "0.00"
        Me.txtpayrollcontriamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtpayrollcontriamount.Visible = False
        '
        'chkyes
        '
        Me.chkyes.AutoSize = True
        Me.chkyes.Location = New System.Drawing.Point(152, 38)
        Me.chkyes.Name = "chkyes"
        Me.chkyes.Size = New System.Drawing.Size(46, 19)
        Me.chkyes.TabIndex = 0
        Me.chkyes.Text = "Yes"
        Me.chkyes.UseVisualStyleBackColor = True
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.Location = New System.Drawing.Point(329, 64)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(129, 15)
        Me.Label109.TabIndex = 109
        Me.Label109.Text = "Contribution Amount:"
        Me.Label109.Visible = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(13, 40)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(65, 15)
        Me.Label37.TabIndex = 115
        Me.Label37.Text = "Employed:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtwithdrawal
        '
        Me.txtwithdrawal.Location = New System.Drawing.Point(495, 36)
        Me.txtwithdrawal.Name = "txtwithdrawal"
        Me.txtwithdrawal.Size = New System.Drawing.Size(104, 21)
        Me.txtwithdrawal.TabIndex = 6
        '
        'mem_MemberDate
        '
        Me.mem_MemberDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_MemberDate.Location = New System.Drawing.Point(152, 64)
        Me.mem_MemberDate.Name = "mem_MemberDate"
        Me.mem_MemberDate.Size = New System.Drawing.Size(116, 21)
        Me.mem_MemberDate.TabIndex = 5
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label111.Location = New System.Drawing.Point(329, 39)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(144, 15)
        Me.Label111.TabIndex = 109
        Me.Label111.Text = "Clients Withdrawal Date:"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label108.Location = New System.Drawing.Point(13, 65)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(110, 15)
        Me.Label108.TabIndex = 102
        Me.Label108.Text = "Membership Date:"
        Me.Label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label65.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label65.Location = New System.Drawing.Point(6, 1)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(122, 18)
        Me.Label65.TabIndex = 9
        Me.Label65.Text = "Client Information"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.Location = New System.Drawing.Point(14, 125)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(86, 15)
        Me.Label105.TabIndex = 102
        Me.Label105.Text = "Bereavement:"
        Me.Label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label105.Visible = False
        '
        'cboemp_status
        '
        Me.cboemp_status.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_status.FormattingEnabled = True
        Me.cboemp_status.Location = New System.Drawing.Point(152, 91)
        Me.cboemp_status.Name = "cboemp_status"
        Me.cboemp_status.Size = New System.Drawing.Size(116, 23)
        Me.cboemp_status.TabIndex = 3
        Me.cboemp_status.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 94)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(125, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Employement Status:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label11.Visible = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox8.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox8.Location = New System.Drawing.Point(1, 1)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(1086, 22)
        Me.PictureBox8.TabIndex = 119
        Me.PictureBox8.TabStop = False
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.txtContractPrice)
        Me.TabPage1.Controls.Add(Me.Label61)
        Me.TabPage1.Controls.Add(Me.txtEcola)
        Me.TabPage1.Controls.Add(Me.Label63)
        Me.TabPage1.Controls.Add(Me.Label52)
        Me.TabPage1.Controls.Add(Me.Label51)
        Me.TabPage1.Controls.Add(Me.Label50)
        Me.TabPage1.Controls.Add(Me.cboPaycode)
        Me.TabPage1.Controls.Add(Me.Label38)
        Me.TabPage1.Controls.Add(Me.cbotaxcode)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.cboPositionLevel)
        Me.TabPage1.Controls.Add(Me.emp_company)
        Me.TabPage1.Controls.Add(Me.Label114)
        Me.TabPage1.Controls.Add(Me.emp_Ytenures)
        Me.TabPage1.Controls.Add(Me.Label100)
        Me.TabPage1.Controls.Add(Me.txtlocalofficenumber)
        Me.TabPage1.Controls.Add(Me.btnrestricted)
        Me.TabPage1.Controls.Add(Me.Label34)
        Me.TabPage1.Controls.Add(Me.txtofficenumber)
        Me.TabPage1.Controls.Add(Me.txtofficeadd)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.emp_Datehired)
        Me.TabPage1.Controls.Add(Me.cbopayroll)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.cborate)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.cboEmp_type)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label69)
        Me.TabPage1.Controls.Add(Me.cbotitledesignation)
        Me.TabPage1.Controls.Add(Me.txtbasicpay)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.PictureBox3)
        Me.TabPage1.Controls.Add(Me.Label49)
        Me.TabPage1.Controls.Add(Me.Label48)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Employment Information"
        '
        'txtContractPrice
        '
        Me.txtContractPrice.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtContractPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContractPrice.Location = New System.Drawing.Point(322, 154)
        Me.txtContractPrice.MaxLength = 255
        Me.txtContractPrice.Name = "txtContractPrice"
        Me.txtContractPrice.Size = New System.Drawing.Size(83, 21)
        Me.txtContractPrice.TabIndex = 122
        Me.txtContractPrice.Text = "0.00"
        Me.txtContractPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label61
        '
        Me.Label61.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(199, 157)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(117, 15)
        Me.Label61.TabIndex = 121
        Me.Label61.Text = "Basic Contract Price:"
        '
        'txtEcola
        '
        Me.txtEcola.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEcola.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEcola.Location = New System.Drawing.Point(567, 158)
        Me.txtEcola.MaxLength = 255
        Me.txtEcola.Name = "txtEcola"
        Me.txtEcola.Size = New System.Drawing.Size(94, 21)
        Me.txtEcola.TabIndex = 120
        Me.txtEcola.Text = "0.00"
        Me.txtEcola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label63
        '
        Me.Label63.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(481, 162)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(47, 15)
        Me.Label63.TabIndex = 119
        Me.Label63.Text = "ECOLA:"
        '
        'Label52
        '
        Me.Label52.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.ForeColor = System.Drawing.Color.Red
        Me.Label52.Location = New System.Drawing.Point(989, 88)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(14, 15)
        Me.Label52.TabIndex = 117
        Me.Label52.Text = "*"
        '
        'Label51
        '
        Me.Label51.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.Red
        Me.Label51.Location = New System.Drawing.Point(989, 62)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(14, 15)
        Me.Label51.TabIndex = 116
        Me.Label51.Text = "*"
        '
        'Label50
        '
        Me.Label50.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.Red
        Me.Label50.Location = New System.Drawing.Point(989, 36)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(14, 15)
        Me.Label50.TabIndex = 115
        Me.Label50.Text = "*"
        '
        'cboPaycode
        '
        Me.cboPaycode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaycode.BackColor = System.Drawing.Color.White
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(567, 107)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(208, 23)
        Me.cboPaycode.TabIndex = 112
        '
        'Label38
        '
        Me.Label38.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(481, 108)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(61, 15)
        Me.Label38.TabIndex = 111
        Me.Label38.Text = "Pay Code:"
        '
        'cbotaxcode
        '
        Me.cbotaxcode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotaxcode.Enabled = False
        Me.cbotaxcode.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbotaxcode.FormattingEnabled = True
        Me.cbotaxcode.Location = New System.Drawing.Point(871, 84)
        Me.cbotaxcode.Name = "cbotaxcode"
        Me.cbotaxcode.Size = New System.Drawing.Size(116, 23)
        Me.cbotaxcode.TabIndex = 107
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(791, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Tax Code:"
        '
        'cboPositionLevel
        '
        Me.cboPositionLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPositionLevel.FormattingEnabled = True
        Me.cboPositionLevel.Location = New System.Drawing.Point(567, 81)
        Me.cboPositionLevel.Name = "cboPositionLevel"
        Me.cboPositionLevel.Size = New System.Drawing.Size(208, 23)
        Me.cboPositionLevel.TabIndex = 50
        '
        'emp_company
        '
        Me.emp_company.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.emp_company.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_company.Location = New System.Drawing.Point(110, 33)
        Me.emp_company.Name = "emp_company"
        Me.emp_company.Size = New System.Drawing.Size(345, 21)
        Me.emp_company.TabIndex = 103
        '
        'Label114
        '
        Me.Label114.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label114.AutoSize = True
        Me.Label114.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label114.Location = New System.Drawing.Point(481, 85)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(87, 15)
        Me.Label114.TabIndex = 49
        Me.Label114.Text = "Position Level:"
        '
        'emp_Ytenures
        '
        Me.emp_Ytenures.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.emp_Ytenures.BackColor = System.Drawing.SystemColors.Window
        Me.emp_Ytenures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_Ytenures.Location = New System.Drawing.Point(110, 130)
        Me.emp_Ytenures.Name = "emp_Ytenures"
        Me.emp_Ytenures.ReadOnly = True
        Me.emp_Ytenures.Size = New System.Drawing.Size(295, 21)
        Me.emp_Ytenures.TabIndex = 106
        '
        'Label100
        '
        Me.Label100.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label100.AutoSize = True
        Me.Label100.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.Location = New System.Drawing.Point(3, 133)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(95, 15)
        Me.Label100.TabIndex = 45
        Me.Label100.Text = "Years of Tenure:"
        '
        'txtlocalofficenumber
        '
        Me.txtlocalofficenumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtlocalofficenumber.BackColor = System.Drawing.SystemColors.Window
        Me.txtlocalofficenumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlocalofficenumber.Location = New System.Drawing.Point(110, 106)
        Me.txtlocalofficenumber.MaxLength = 255
        Me.txtlocalofficenumber.Name = "txtlocalofficenumber"
        Me.txtlocalofficenumber.Size = New System.Drawing.Size(295, 21)
        Me.txtlocalofficenumber.TabIndex = 4
        '
        'btnrestricted
        '
        Me.btnrestricted.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnrestricted.Enabled = False
        Me.btnrestricted.ForeColor = System.Drawing.Color.Red
        Me.btnrestricted.Location = New System.Drawing.Point(110, 154)
        Me.btnrestricted.Name = "btnrestricted"
        Me.btnrestricted.Size = New System.Drawing.Size(85, 25)
        Me.btnrestricted.TabIndex = 8
        Me.btnrestricted.Text = "Confidential"
        Me.btnrestricted.UseVisualStyleBackColor = True
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label34.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label34.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label34.Location = New System.Drawing.Point(6, 3)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(164, 18)
        Me.Label34.TabIndex = 19
        Me.Label34.Text = "Employment Information"
        '
        'txtofficenumber
        '
        Me.txtofficenumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtofficenumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtofficenumber.Location = New System.Drawing.Point(110, 82)
        Me.txtofficenumber.MaxLength = 255
        Me.txtofficenumber.Name = "txtofficenumber"
        Me.txtofficenumber.Size = New System.Drawing.Size(295, 21)
        Me.txtofficenumber.TabIndex = 3
        '
        'txtofficeadd
        '
        Me.txtofficeadd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtofficeadd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtofficeadd.Location = New System.Drawing.Point(110, 57)
        Me.txtofficeadd.MaxLength = 50
        Me.txtofficeadd.Name = "txtofficeadd"
        Me.txtofficeadd.Size = New System.Drawing.Size(345, 21)
        Me.txtofficeadd.TabIndex = 2
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(3, 108)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 15)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Local No.:"
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(4, 85)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(65, 15)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Office No.:"
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(3, 60)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(90, 15)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Office Address:"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(4, 36)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 15)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Company:"
        '
        'emp_Datehired
        '
        Me.emp_Datehired.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_Datehired.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_Datehired.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.emp_Datehired.Location = New System.Drawing.Point(567, 133)
        Me.emp_Datehired.Name = "emp_Datehired"
        Me.emp_Datehired.Size = New System.Drawing.Size(94, 21)
        Me.emp_Datehired.TabIndex = 14
        Me.emp_Datehired.Value = New Date(2006, 5, 3, 0, 0, 0, 0)
        '
        'cbopayroll
        '
        Me.cbopayroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbopayroll.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbopayroll.FormattingEnabled = True
        Me.cbopayroll.Location = New System.Drawing.Point(871, 33)
        Me.cbopayroll.Name = "cbopayroll"
        Me.cbopayroll.Size = New System.Drawing.Size(116, 23)
        Me.cbopayroll.TabIndex = 11
        '
        'Label62
        '
        Me.Label62.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(791, 36)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(79, 15)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Payroll Code:"
        '
        'cborate
        '
        Me.cborate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cborate.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cborate.FormattingEnabled = True
        Me.cborate.Location = New System.Drawing.Point(871, 59)
        Me.cborate.Name = "cborate"
        Me.cborate.Size = New System.Drawing.Size(116, 23)
        Me.cborate.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(481, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Position Type:"
        '
        'cboEmp_type
        '
        Me.cboEmp_type.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboEmp_type.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboEmp_type.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmp_type.FormattingEnabled = True
        Me.cboEmp_type.Location = New System.Drawing.Point(567, 31)
        Me.cboEmp_type.Name = "cboEmp_type"
        Me.cboEmp_type.Size = New System.Drawing.Size(208, 23)
        Me.cboEmp_type.TabIndex = 5
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(481, 59)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 15)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Position Title:"
        '
        'Label69
        '
        Me.Label69.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(791, 62)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(65, 15)
        Me.Label69.TabIndex = 34
        Me.Label69.Text = "Rate Type:"
        '
        'cbotitledesignation
        '
        Me.cbotitledesignation.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotitledesignation.FormattingEnabled = True
        Me.cbotitledesignation.Location = New System.Drawing.Point(567, 56)
        Me.cbotitledesignation.Name = "cbotitledesignation"
        Me.cbotitledesignation.Size = New System.Drawing.Size(208, 23)
        Me.cbotitledesignation.TabIndex = 6
        '
        'txtbasicpay
        '
        Me.txtbasicpay.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtbasicpay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtbasicpay.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbasicpay.Location = New System.Drawing.Point(110, 155)
        Me.txtbasicpay.Name = "txtbasicpay"
        Me.txtbasicpay.Size = New System.Drawing.Size(85, 21)
        Me.txtbasicpay.TabIndex = 6
        Me.txtbasicpay.Text = "0.00"
        '
        'Label59
        '
        Me.Label59.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(3, 157)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(59, 15)
        Me.Label59.TabIndex = 12
        Me.Label59.Text = "Basic Pay:"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(481, 138)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 15)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Date Hired:"
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox3.Location = New System.Drawing.Point(1, 1)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(1065, 22)
        Me.PictureBox3.TabIndex = 18
        Me.PictureBox3.TabStop = False
        '
        'Label49
        '
        Me.Label49.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.Red
        Me.Label49.Location = New System.Drawing.Point(774, 111)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(14, 15)
        Me.Label49.TabIndex = 114
        Me.Label49.Text = "*"
        '
        'Label48
        '
        Me.Label48.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.Red
        Me.Label48.Location = New System.Drawing.Point(774, 60)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(14, 15)
        Me.Label48.TabIndex = 113
        Me.Label48.Text = "*"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.btnUpdateBA)
        Me.TabPage2.Controls.Add(Me.btnDeleteBA)
        Me.TabPage2.Controls.Add(Me.btnNewBA)
        Me.TabPage2.Controls.Add(Me.lvlBankInfo)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.PictureBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage2.TabIndex = 18
        Me.TabPage2.Text = "Bank Information"
        '
        'btnUpdateBA
        '
        Me.btnUpdateBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateBA.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateBA.Location = New System.Drawing.Point(683, 159)
        Me.btnUpdateBA.Name = "btnUpdateBA"
        Me.btnUpdateBA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnUpdateBA.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateBA.TabIndex = 24
        Me.btnUpdateBA.Text = "Edit"
        Me.btnUpdateBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateBA.UseVisualStyleBackColor = True
        '
        'btnDeleteBA
        '
        Me.btnDeleteBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteBA.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteBA.Location = New System.Drawing.Point(754, 159)
        Me.btnDeleteBA.Name = "btnDeleteBA"
        Me.btnDeleteBA.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteBA.TabIndex = 25
        Me.btnDeleteBA.Text = "Delete"
        Me.btnDeleteBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteBA.UseVisualStyleBackColor = True
        '
        'btnNewBA
        '
        Me.btnNewBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewBA.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNewBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewBA.Location = New System.Drawing.Point(612, 159)
        Me.btnNewBA.Name = "btnNewBA"
        Me.btnNewBA.Size = New System.Drawing.Size(68, 24)
        Me.btnNewBA.TabIndex = 23
        Me.btnNewBA.Text = "New"
        Me.btnNewBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewBA.UseVisualStyleBackColor = True
        '
        'lvlBankInfo
        '
        Me.lvlBankInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlBankInfo.Location = New System.Drawing.Point(3, 29)
        Me.lvlBankInfo.Name = "lvlBankInfo"
        Me.lvlBankInfo.Size = New System.Drawing.Size(819, 126)
        Me.lvlBankInfo.TabIndex = 22
        Me.lvlBankInfo.UseCompatibleStateImageBehavior = False
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label35.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label35.Location = New System.Drawing.Point(6, 2)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(115, 18)
        Me.Label35.TabIndex = 21
        Me.Label35.Text = "Bank Information"
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox4.Location = New System.Drawing.Point(-1, 0)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox4.TabIndex = 20
        Me.PictureBox4.TabStop = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage4.Controls.Add(Me.btnUpdateSInc)
        Me.TabPage4.Controls.Add(Me.btnDeleteSInc)
        Me.TabPage4.Controls.Add(Me.btnsaveSInc)
        Me.TabPage4.Controls.Add(Me.lvlSourceIncome)
        Me.TabPage4.Controls.Add(Me.Label36)
        Me.TabPage4.Controls.Add(Me.PictureBox6)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage4.TabIndex = 19
        Me.TabPage4.Text = "Source of Income"
        '
        'btnUpdateSInc
        '
        Me.btnUpdateSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateSInc.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSInc.Location = New System.Drawing.Point(683, 159)
        Me.btnUpdateSInc.Name = "btnUpdateSInc"
        Me.btnUpdateSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSInc.TabIndex = 28
        Me.btnUpdateSInc.Text = "Edit"
        Me.btnUpdateSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSInc.UseVisualStyleBackColor = True
        '
        'btnDeleteSInc
        '
        Me.btnDeleteSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteSInc.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteSInc.Location = New System.Drawing.Point(754, 159)
        Me.btnDeleteSInc.Name = "btnDeleteSInc"
        Me.btnDeleteSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteSInc.TabIndex = 27
        Me.btnDeleteSInc.Text = "Delete"
        Me.btnDeleteSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteSInc.UseVisualStyleBackColor = True
        '
        'btnsaveSInc
        '
        Me.btnsaveSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsaveSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsaveSInc.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsaveSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsaveSInc.Location = New System.Drawing.Point(612, 159)
        Me.btnsaveSInc.Name = "btnsaveSInc"
        Me.btnsaveSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnsaveSInc.TabIndex = 26
        Me.btnsaveSInc.Text = "New"
        Me.btnsaveSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsaveSInc.UseVisualStyleBackColor = True
        '
        'lvlSourceIncome
        '
        Me.lvlSourceIncome.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlSourceIncome.Location = New System.Drawing.Point(3, 29)
        Me.lvlSourceIncome.Name = "lvlSourceIncome"
        Me.lvlSourceIncome.Size = New System.Drawing.Size(819, 126)
        Me.lvlSourceIncome.TabIndex = 25
        Me.lvlSourceIncome.UseCompatibleStateImageBehavior = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label36.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label36.Location = New System.Drawing.Point(7, 2)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(115, 18)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "Source of Income"
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox6.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(1027, 22)
        Me.PictureBox6.TabIndex = 23
        Me.PictureBox6.TabStop = False
        '
        'TabPage17
        '
        Me.TabPage17.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage17.Controls.Add(Me.BtnEditContact)
        Me.TabPage17.Controls.Add(Me.BtnDeleteContact)
        Me.TabPage17.Controls.Add(Me.btnaddContact)
        Me.TabPage17.Controls.Add(Me.lvlNearestRelatives)
        Me.TabPage17.Controls.Add(Me.Label98)
        Me.TabPage17.Controls.Add(Me.PictureBox1)
        Me.TabPage17.Location = New System.Drawing.Point(4, 22)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage17.TabIndex = 17
        Me.TabPage17.Text = "Beneficiary"
        '
        'BtnEditContact
        '
        Me.BtnEditContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnEditContact.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnEditContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnEditContact.Location = New System.Drawing.Point(683, 159)
        Me.BtnEditContact.Name = "BtnEditContact"
        Me.BtnEditContact.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BtnEditContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnEditContact.TabIndex = 20
        Me.BtnEditContact.Text = "Edit"
        Me.BtnEditContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnEditContact.UseVisualStyleBackColor = True
        '
        'BtnDeleteContact
        '
        Me.BtnDeleteContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnDeleteContact.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDeleteContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeleteContact.Location = New System.Drawing.Point(754, 159)
        Me.BtnDeleteContact.Name = "BtnDeleteContact"
        Me.BtnDeleteContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnDeleteContact.TabIndex = 21
        Me.BtnDeleteContact.Text = "Delete"
        Me.BtnDeleteContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeleteContact.UseVisualStyleBackColor = True
        '
        'btnaddContact
        '
        Me.btnaddContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnaddContact.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnaddContact.Location = New System.Drawing.Point(612, 159)
        Me.btnaddContact.Name = "btnaddContact"
        Me.btnaddContact.Size = New System.Drawing.Size(68, 24)
        Me.btnaddContact.TabIndex = 19
        Me.btnaddContact.Text = "New"
        Me.btnaddContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnaddContact.UseVisualStyleBackColor = True
        '
        'lvlNearestRelatives
        '
        Me.lvlNearestRelatives.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlNearestRelatives.Location = New System.Drawing.Point(3, 29)
        Me.lvlNearestRelatives.Name = "lvlNearestRelatives"
        Me.lvlNearestRelatives.Size = New System.Drawing.Size(819, 126)
        Me.lvlNearestRelatives.TabIndex = 4
        Me.lvlNearestRelatives.UseCompatibleStateImageBehavior = False
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label98.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label98.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label98.Location = New System.Drawing.Point(5, 2)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(77, 18)
        Me.Label98.TabIndex = 3
        Me.Label98.Text = "Beneficiary"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox1.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1049, 21)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage3.Controls.Add(Me.LvlEmployeeDependent)
        Me.TabPage3.Controls.Add(Me.Label68)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.btnemp_editdepdts)
        Me.TabPage3.Controls.Add(Me.btndelete_dep)
        Me.TabPage3.Controls.Add(Me.btnadd_dep)
        Me.TabPage3.Controls.Add(Me.PictureBox5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Dependents"
        '
        'LvlEmployeeDependent
        '
        Me.LvlEmployeeDependent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LvlEmployeeDependent.Location = New System.Drawing.Point(9, 26)
        Me.LvlEmployeeDependent.Name = "LvlEmployeeDependent"
        Me.LvlEmployeeDependent.Size = New System.Drawing.Size(984, 132)
        Me.LvlEmployeeDependent.TabIndex = 68
        Me.LvlEmployeeDependent.UseCompatibleStateImageBehavior = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label68.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label68.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label68.Location = New System.Drawing.Point(3, 0)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(149, 18)
        Me.Label68.TabIndex = 8
        Me.Label68.Text = "Employee Dependents"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.emp_pic)
        Me.GroupBox3.Controls.Add(Me.temp_marks)
        Me.GroupBox3.Controls.Add(Me.txtitemno)
        Me.GroupBox3.Controls.Add(Me.txtremarks)
        Me.GroupBox3.Controls.Add(Me.temp_add3)
        Me.GroupBox3.Controls.Add(Me.txtparent_ID)
        Me.GroupBox3.Controls.Add(Me.txtmonthreqular)
        Me.GroupBox3.Controls.Add(Me.txtkeysection)
        Me.GroupBox3.Controls.Add(Me.temp_height)
        Me.GroupBox3.Controls.Add(Me.txtdescsection)
        Me.GroupBox3.Controls.Add(Me.txtsalarygrade)
        Me.GroupBox3.Controls.Add(Me.txtdescdepartment)
        Me.GroupBox3.Controls.Add(Me.temp_add2)
        Me.GroupBox3.Controls.Add(Me.txtdescdivision)
        Me.GroupBox3.Controls.Add(Me.txtweight)
        Me.GroupBox3.Controls.Add(Me.txtkeydepartment)
        Me.GroupBox3.Controls.Add(Me.txtdateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeydivision)
        Me.GroupBox3.Controls.Add(Me.Dateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeycompany)
        Me.GroupBox3.Controls.Add(Me.emp_extphone)
        Me.GroupBox3.Controls.Add(Me.cbosalarygrade)
        Me.GroupBox3.Controls.Add(Me.temp_citizen)
        Me.GroupBox3.Controls.Add(Me.temp_designation)
        Me.GroupBox3.Controls.Add(Me.txtType_employee)
        Me.GroupBox3.Controls.Add(Me.txtfxkeypositionlevel)
        Me.GroupBox3.Controls.Add(Me.txtfullname)
        Me.GroupBox3.Controls.Add(Me.TXTRECID)
        Me.GroupBox3.Controls.Add(Me.cbodept)
        Me.GroupBox3.Controls.Add(Me.Dateresigned)
        Me.GroupBox3.Controls.Add(Me.TXTKEYEMPLOYEEID)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Location = New System.Drawing.Point(326, 22)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(242, 161)
        Me.GroupBox3.TabIndex = 67
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Omitted items in Personnel New Version"
        Me.GroupBox3.Visible = False
        '
        'emp_pic
        '
        Me.emp_pic.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar
        Me.emp_pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_pic.Location = New System.Drawing.Point(96, 95)
        Me.emp_pic.Name = "emp_pic"
        Me.emp_pic.Size = New System.Drawing.Size(126, 124)
        Me.emp_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.emp_pic.TabIndex = 0
        Me.emp_pic.TabStop = False
        '
        'temp_marks
        '
        Me.temp_marks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_marks.Location = New System.Drawing.Point(104, 182)
        Me.temp_marks.MaxLength = 50
        Me.temp_marks.Name = "temp_marks"
        Me.temp_marks.Size = New System.Drawing.Size(137, 21)
        Me.temp_marks.TabIndex = 9
        '
        'txtitemno
        '
        Me.txtitemno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtitemno.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtitemno.Location = New System.Drawing.Point(57, 37)
        Me.txtitemno.Name = "txtitemno"
        Me.txtitemno.Size = New System.Drawing.Size(35, 21)
        Me.txtitemno.TabIndex = 19
        '
        'txtremarks
        '
        Me.txtremarks.Location = New System.Drawing.Point(66, 208)
        Me.txtremarks.Multiline = True
        Me.txtremarks.Name = "txtremarks"
        Me.txtremarks.Size = New System.Drawing.Size(39, 22)
        Me.txtremarks.TabIndex = 43
        '
        'temp_add3
        '
        Me.temp_add3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add3.Location = New System.Drawing.Point(104, 208)
        Me.temp_add3.MaxLength = 255
        Me.temp_add3.Name = "temp_add3"
        Me.temp_add3.Size = New System.Drawing.Size(156, 21)
        Me.temp_add3.TabIndex = 5
        '
        'txtparent_ID
        '
        Me.txtparent_ID.Location = New System.Drawing.Point(166, 183)
        Me.txtparent_ID.Name = "txtparent_ID"
        Me.txtparent_ID.Size = New System.Drawing.Size(28, 21)
        Me.txtparent_ID.TabIndex = 97
        Me.txtparent_ID.Visible = False
        '
        'txtmonthreqular
        '
        Me.txtmonthreqular.Location = New System.Drawing.Point(125, 60)
        Me.txtmonthreqular.MaxLength = 3
        Me.txtmonthreqular.Name = "txtmonthreqular"
        Me.txtmonthreqular.Size = New System.Drawing.Size(39, 21)
        Me.txtmonthreqular.TabIndex = 15
        '
        'txtkeysection
        '
        Me.txtkeysection.Location = New System.Drawing.Point(132, 183)
        Me.txtkeysection.Name = "txtkeysection"
        Me.txtkeysection.Size = New System.Drawing.Size(28, 21)
        Me.txtkeysection.TabIndex = 96
        Me.txtkeysection.Visible = False
        '
        'temp_height
        '
        Me.temp_height.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_height.Location = New System.Drawing.Point(78, 62)
        Me.temp_height.MaxLength = 10
        Me.temp_height.Name = "temp_height"
        Me.temp_height.Size = New System.Drawing.Size(51, 21)
        Me.temp_height.TabIndex = 7
        '
        'txtdescsection
        '
        Me.txtdescsection.Location = New System.Drawing.Point(166, 159)
        Me.txtdescsection.Name = "txtdescsection"
        Me.txtdescsection.Size = New System.Drawing.Size(59, 21)
        Me.txtdescsection.TabIndex = 95
        Me.txtdescsection.Visible = False
        '
        'txtsalarygrade
        '
        Me.txtsalarygrade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtsalarygrade.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsalarygrade.Location = New System.Drawing.Point(183, 32)
        Me.txtsalarygrade.Name = "txtsalarygrade"
        Me.txtsalarygrade.Size = New System.Drawing.Size(39, 21)
        Me.txtsalarygrade.TabIndex = 47
        Me.txtsalarygrade.Visible = False
        '
        'txtdescdepartment
        '
        Me.txtdescdepartment.Location = New System.Drawing.Point(166, 136)
        Me.txtdescdepartment.Name = "txtdescdepartment"
        Me.txtdescdepartment.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdepartment.TabIndex = 94
        Me.txtdescdepartment.Visible = False
        '
        'temp_add2
        '
        Me.temp_add2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add2.Location = New System.Drawing.Point(84, 136)
        Me.temp_add2.Name = "temp_add2"
        Me.temp_add2.Size = New System.Drawing.Size(25, 21)
        Me.temp_add2.TabIndex = 10
        Me.temp_add2.Visible = False
        '
        'txtdescdivision
        '
        Me.txtdescdivision.Location = New System.Drawing.Point(166, 113)
        Me.txtdescdivision.Name = "txtdescdivision"
        Me.txtdescdivision.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdivision.TabIndex = 93
        Me.txtdescdivision.Visible = False
        '
        'txtweight
        '
        Me.txtweight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtweight.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtweight.Location = New System.Drawing.Point(202, 134)
        Me.txtweight.MaxLength = 10
        Me.txtweight.Name = "txtweight"
        Me.txtweight.Size = New System.Drawing.Size(32, 21)
        Me.txtweight.TabIndex = 8
        '
        'txtkeydepartment
        '
        Me.txtkeydepartment.Location = New System.Drawing.Point(132, 159)
        Me.txtkeydepartment.Name = "txtkeydepartment"
        Me.txtkeydepartment.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydepartment.TabIndex = 92
        Me.txtkeydepartment.Visible = False
        '
        'txtdateregular
        '
        Me.txtdateregular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtdateregular.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdateregular.Location = New System.Drawing.Point(9, 91)
        Me.txtdateregular.Name = "txtdateregular"
        Me.txtdateregular.Size = New System.Drawing.Size(69, 21)
        Me.txtdateregular.TabIndex = 57
        Me.txtdateregular.Visible = False
        '
        'txtkeydivision
        '
        Me.txtkeydivision.Location = New System.Drawing.Point(132, 136)
        Me.txtkeydivision.Name = "txtkeydivision"
        Me.txtkeydivision.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydivision.TabIndex = 91
        Me.txtkeydivision.Visible = False
        '
        'Dateregular
        '
        Me.Dateregular.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateregular.Location = New System.Drawing.Point(84, 91)
        Me.Dateregular.Name = "Dateregular"
        Me.Dateregular.Size = New System.Drawing.Size(21, 21)
        Me.Dateregular.TabIndex = 49
        Me.Dateregular.Value = New Date(2006, 7, 17, 0, 0, 0, 0)
        Me.Dateregular.Visible = False
        '
        'txtkeycompany
        '
        Me.txtkeycompany.Location = New System.Drawing.Point(132, 113)
        Me.txtkeycompany.Name = "txtkeycompany"
        Me.txtkeycompany.Size = New System.Drawing.Size(28, 21)
        Me.txtkeycompany.TabIndex = 90
        Me.txtkeycompany.Visible = False
        '
        'emp_extphone
        '
        Me.emp_extphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_extphone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_extphone.Location = New System.Drawing.Point(191, 89)
        Me.emp_extphone.Name = "emp_extphone"
        Me.emp_extphone.Size = New System.Drawing.Size(45, 21)
        Me.emp_extphone.TabIndex = 7
        '
        'cbosalarygrade
        '
        Me.cbosalarygrade.FormattingEnabled = True
        Me.cbosalarygrade.Location = New System.Drawing.Point(84, 86)
        Me.cbosalarygrade.Name = "cbosalarygrade"
        Me.cbosalarygrade.Size = New System.Drawing.Size(156, 23)
        Me.cbosalarygrade.TabIndex = 7
        '
        'temp_citizen
        '
        Me.temp_citizen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_citizen.Location = New System.Drawing.Point(97, 110)
        Me.temp_citizen.MaxLength = 50
        Me.temp_citizen.Name = "temp_citizen"
        Me.temp_citizen.Size = New System.Drawing.Size(137, 21)
        Me.temp_citizen.TabIndex = 6
        '
        'temp_designation
        '
        Me.temp_designation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_designation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_designation.Location = New System.Drawing.Point(67, 95)
        Me.temp_designation.Name = "temp_designation"
        Me.temp_designation.Size = New System.Drawing.Size(25, 21)
        Me.temp_designation.TabIndex = 16
        Me.temp_designation.Visible = False
        '
        'txtType_employee
        '
        Me.txtType_employee.Location = New System.Drawing.Point(118, 53)
        Me.txtType_employee.Name = "txtType_employee"
        Me.txtType_employee.Size = New System.Drawing.Size(42, 21)
        Me.txtType_employee.TabIndex = 80
        Me.txtType_employee.Visible = False
        '
        'txtfxkeypositionlevel
        '
        Me.txtfxkeypositionlevel.Location = New System.Drawing.Point(118, 76)
        Me.txtfxkeypositionlevel.Name = "txtfxkeypositionlevel"
        Me.txtfxkeypositionlevel.Size = New System.Drawing.Size(42, 21)
        Me.txtfxkeypositionlevel.TabIndex = 81
        Me.txtfxkeypositionlevel.Visible = False
        '
        'txtfullname
        '
        Me.txtfullname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtfullname.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfullname.Location = New System.Drawing.Point(99, 95)
        Me.txtfullname.MaxLength = 50
        Me.txtfullname.Name = "txtfullname"
        Me.txtfullname.Size = New System.Drawing.Size(22, 21)
        Me.txtfullname.TabIndex = 62
        Me.txtfullname.Visible = False
        '
        'TXTRECID
        '
        Me.TXTRECID.Location = New System.Drawing.Point(145, 65)
        Me.TXTRECID.Name = "TXTRECID"
        Me.TXTRECID.Size = New System.Drawing.Size(72, 21)
        Me.TXTRECID.TabIndex = 52
        Me.TXTRECID.Visible = False
        '
        'cbodept
        '
        Me.cbodept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cbodept.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbodept.Location = New System.Drawing.Point(100, 123)
        Me.cbodept.MaxLength = 50
        Me.cbodept.Name = "cbodept"
        Me.cbodept.Size = New System.Drawing.Size(22, 21)
        Me.cbodept.TabIndex = 72
        Me.cbodept.Visible = False
        '
        'Dateresigned
        '
        Me.Dateresigned.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dateresigned.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateresigned.Location = New System.Drawing.Point(67, 122)
        Me.Dateresigned.Name = "Dateresigned"
        Me.Dateresigned.Size = New System.Drawing.Size(28, 21)
        Me.Dateresigned.TabIndex = 58
        Me.Dateresigned.Value = New Date(2006, 5, 30, 0, 0, 0, 0)
        Me.Dateresigned.Visible = False
        '
        'TXTKEYEMPLOYEEID
        '
        Me.TXTKEYEMPLOYEEID.Location = New System.Drawing.Point(145, 89)
        Me.TXTKEYEMPLOYEEID.Name = "TXTKEYEMPLOYEEID"
        Me.TXTKEYEMPLOYEEID.Size = New System.Drawing.Size(72, 21)
        Me.TXTKEYEMPLOYEEID.TabIndex = 51
        Me.TXTKEYEMPLOYEEID.Visible = False
        '
        'btnemp_editdepdts
        '
        Me.btnemp_editdepdts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_editdepdts.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_editdepdts.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_editdepdts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_editdepdts.Location = New System.Drawing.Point(854, 161)
        Me.btnemp_editdepdts.Name = "btnemp_editdepdts"
        Me.btnemp_editdepdts.Size = New System.Drawing.Size(68, 24)
        Me.btnemp_editdepdts.TabIndex = 10
        Me.btnemp_editdepdts.Text = "Edit"
        Me.btnemp_editdepdts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_editdepdts.UseVisualStyleBackColor = True
        '
        'btndelete_dep
        '
        Me.btndelete_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btndelete_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btndelete_dep.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete_dep.Location = New System.Drawing.Point(925, 161)
        Me.btndelete_dep.Name = "btndelete_dep"
        Me.btndelete_dep.Size = New System.Drawing.Size(68, 24)
        Me.btndelete_dep.TabIndex = 5
        Me.btndelete_dep.Text = "Delete"
        Me.btndelete_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete_dep.UseVisualStyleBackColor = True
        '
        'btnadd_dep
        '
        Me.btnadd_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnadd_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnadd_dep.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnadd_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnadd_dep.Location = New System.Drawing.Point(783, 161)
        Me.btnadd_dep.Name = "btnadd_dep"
        Me.btnadd_dep.Size = New System.Drawing.Size(68, 24)
        Me.btnadd_dep.TabIndex = 3
        Me.btnadd_dep.Text = "New"
        Me.btnadd_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnadd_dep.UseVisualStyleBackColor = True
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox5.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(1050, 18)
        Me.PictureBox5.TabIndex = 1
        Me.PictureBox5.TabStop = False
        '
        'others
        '
        Me.others.BackColor = System.Drawing.SystemColors.Control
        Me.others.Controls.Add(Me.btnEditMobile)
        Me.others.Controls.Add(Me.gridMobileNos)
        Me.others.Controls.Add(Me.txtEloadingLimit)
        Me.others.Controls.Add(Me.Label31)
        Me.others.Controls.Add(Me.txtEloaderPIN)
        Me.others.Controls.Add(Me.chkShowPINChar)
        Me.others.Controls.Add(Me.Label24)
        Me.others.Controls.Add(Me.Label15)
        Me.others.Controls.Add(Me.btnAddMobileNo)
        Me.others.Controls.Add(Me.chkIsEloader)
        Me.others.Controls.Add(Me.Label4)
        Me.others.Controls.Add(Me.txtMobileNo)
        Me.others.Controls.Add(Me.PictureBox9)
        Me.others.Controls.Add(Me.PictureBox7)
        Me.others.Location = New System.Drawing.Point(4, 22)
        Me.others.Name = "others"
        Me.others.Padding = New System.Windows.Forms.Padding(3)
        Me.others.Size = New System.Drawing.Size(1009, 189)
        Me.others.TabIndex = 20
        Me.others.Text = "Others"
        '
        'btnEditMobile
        '
        Me.btnEditMobile.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditMobile.Location = New System.Drawing.Point(423, 72)
        Me.btnEditMobile.Name = "btnEditMobile"
        Me.btnEditMobile.Size = New System.Drawing.Size(53, 22)
        Me.btnEditMobile.TabIndex = 45
        Me.btnEditMobile.Text = "Edit"
        Me.btnEditMobile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditMobile.UseVisualStyleBackColor = True
        '
        'gridMobileNos
        '
        Me.gridMobileNos.AllowUserToAddRows = False
        Me.gridMobileNos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.gridMobileNos.BackgroundColor = System.Drawing.Color.White
        Me.gridMobileNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gridMobileNos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.gridMobileNos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMobileNos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.gridMobileNos.Location = New System.Drawing.Point(203, 45)
        Me.gridMobileNos.Name = "gridMobileNos"
        Me.gridMobileNos.Size = New System.Drawing.Size(214, 125)
        Me.gridMobileNos.TabIndex = 44
        '
        'txtEloadingLimit
        '
        Me.txtEloadingLimit.Location = New System.Drawing.Point(571, 73)
        Me.txtEloadingLimit.Name = "txtEloadingLimit"
        Me.txtEloadingLimit.Size = New System.Drawing.Size(115, 21)
        Me.txtEloadingLimit.TabIndex = 43
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(484, 78)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(87, 15)
        Me.Label31.TabIndex = 42
        Me.Label31.Text = "Eloading Limit:"
        '
        'txtEloaderPIN
        '
        Me.txtEloaderPIN.Location = New System.Drawing.Point(571, 45)
        Me.txtEloaderPIN.MaxLength = 4
        Me.txtEloaderPIN.Name = "txtEloaderPIN"
        Me.txtEloaderPIN.Size = New System.Drawing.Size(115, 21)
        Me.txtEloaderPIN.TabIndex = 40
        Me.txtEloaderPIN.UseSystemPasswordChar = True
        '
        'chkShowPINChar
        '
        Me.chkShowPINChar.AutoSize = True
        Me.chkShowPINChar.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPINChar.Location = New System.Drawing.Point(692, 49)
        Me.chkShowPINChar.Name = "chkShowPINChar"
        Me.chkShowPINChar.Size = New System.Drawing.Size(95, 17)
        Me.chkShowPINChar.TabIndex = 39
        Me.chkShowPINChar.Text = "Show PIN Char."
        Me.chkShowPINChar.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(7, 29)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(101, 15)
        Me.Label24.TabIndex = 38
        Me.Label24.Text = "E-Loading Service"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(484, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 15)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "PIN:"
        '
        'btnAddMobileNo
        '
        Me.btnAddMobileNo.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddMobileNo.Location = New System.Drawing.Point(423, 45)
        Me.btnAddMobileNo.Name = "btnAddMobileNo"
        Me.btnAddMobileNo.Size = New System.Drawing.Size(53, 22)
        Me.btnAddMobileNo.TabIndex = 27
        Me.btnAddMobileNo.Text = "Add"
        Me.btnAddMobileNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAddMobileNo.UseVisualStyleBackColor = True
        '
        'chkIsEloader
        '
        Me.chkIsEloader.AutoSize = True
        Me.chkIsEloader.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsEloader.Location = New System.Drawing.Point(571, 98)
        Me.chkIsEloader.Name = "chkIsEloader"
        Me.chkIsEloader.Size = New System.Drawing.Size(131, 19)
        Me.chkIsEloader.TabIndex = 35
        Me.chkIsEloader.Text = "Registered Member"
        Me.chkIsEloader.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(5, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 18)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Apps Registrations"
        '
        'txtMobileNo
        '
        Me.txtMobileNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobileNo.Location = New System.Drawing.Point(487, 134)
        Me.txtMobileNo.Name = "txtMobileNo"
        Me.txtMobileNo.ReadOnly = True
        Me.txtMobileNo.Size = New System.Drawing.Size(199, 21)
        Me.txtMobileNo.TabIndex = 13
        Me.txtMobileNo.Visible = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox9.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox9.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(996, 19)
        Me.PictureBox9.TabIndex = 33
        Me.PictureBox9.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.Location = New System.Drawing.Point(9, 48)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(188, 124)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage6.Controls.Add(Me.dgvemail)
        Me.TabPage6.Controls.Add(Me.Label43)
        Me.TabPage6.Controls.Add(Me.dgvphone)
        Me.TabPage6.Controls.Add(Me.Label42)
        Me.TabPage6.Controls.Add(Me.dgvcivil)
        Me.TabPage6.Controls.Add(Me.Label41)
        Me.TabPage6.Controls.Add(Me.dgvaddress)
        Me.TabPage6.Controls.Add(Me.Label40)
        Me.TabPage6.Controls.Add(Me.dgvlastname)
        Me.TabPage6.Controls.Add(Me.Label39)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1009, 189)
        Me.TabPage6.TabIndex = 21
        Me.TabPage6.Text = "Information History"
        '
        'dgvemail
        '
        Me.dgvemail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvemail.BackgroundColor = System.Drawing.Color.White
        Me.dgvemail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvemail.Location = New System.Drawing.Point(603, 28)
        Me.dgvemail.Name = "dgvemail"
        Me.dgvemail.ReadOnly = True
        Me.dgvemail.RowHeadersVisible = False
        Me.dgvemail.Size = New System.Drawing.Size(302, 68)
        Me.dgvemail.TabIndex = 9
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(600, 12)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(92, 13)
        Me.Label43.TabIndex = 8
        Me.Label43.Text = "Previous Email:"
        '
        'dgvphone
        '
        Me.dgvphone.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvphone.BackgroundColor = System.Drawing.Color.White
        Me.dgvphone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvphone.Location = New System.Drawing.Point(396, 28)
        Me.dgvphone.Name = "dgvphone"
        Me.dgvphone.ReadOnly = True
        Me.dgvphone.RowHeadersVisible = False
        Me.dgvphone.Size = New System.Drawing.Size(198, 68)
        Me.dgvphone.TabIndex = 7
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(396, 12)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(158, 13)
        Me.Label42.TabIndex = 6
        Me.Label42.Text = "Previous Residence Phone:"
        '
        'dgvcivil
        '
        Me.dgvcivil.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvcivil.BackgroundColor = System.Drawing.Color.White
        Me.dgvcivil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcivil.Location = New System.Drawing.Point(235, 28)
        Me.dgvcivil.Name = "dgvcivil"
        Me.dgvcivil.ReadOnly = True
        Me.dgvcivil.RowHeadersVisible = False
        Me.dgvcivil.Size = New System.Drawing.Size(155, 68)
        Me.dgvcivil.TabIndex = 5
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(232, 12)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(128, 13)
        Me.Label41.TabIndex = 4
        Me.Label41.Text = "Previous Civil Status :"
        '
        'dgvaddress
        '
        Me.dgvaddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvaddress.BackgroundColor = System.Drawing.Color.White
        Me.dgvaddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvaddress.Location = New System.Drawing.Point(18, 115)
        Me.dgvaddress.Name = "dgvaddress"
        Me.dgvaddress.ReadOnly = True
        Me.dgvaddress.RowHeadersVisible = False
        Me.dgvaddress.Size = New System.Drawing.Size(674, 58)
        Me.dgvaddress.TabIndex = 3
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(15, 99)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(111, 13)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "Previous Address :"
        '
        'dgvlastname
        '
        Me.dgvlastname.AllowUserToAddRows = False
        Me.dgvlastname.AllowUserToDeleteRows = False
        Me.dgvlastname.AllowUserToResizeColumns = False
        Me.dgvlastname.AllowUserToResizeRows = False
        Me.dgvlastname.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvlastname.BackgroundColor = System.Drawing.Color.White
        Me.dgvlastname.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvlastname.Location = New System.Drawing.Point(18, 28)
        Me.dgvlastname.Name = "dgvlastname"
        Me.dgvlastname.ReadOnly = True
        Me.dgvlastname.RowHeadersVisible = False
        Me.dgvlastname.Size = New System.Drawing.Size(208, 68)
        Me.dgvlastname.TabIndex = 1
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(15, 12)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(121, 13)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Previous Lastname :"
        '
        'Cbomem_Status
        '
        Me.Cbomem_Status.FormattingEnabled = True
        Me.Cbomem_Status.Location = New System.Drawing.Point(390, 99)
        Me.Cbomem_Status.Name = "Cbomem_Status"
        Me.Cbomem_Status.Size = New System.Drawing.Size(153, 21)
        Me.Cbomem_Status.TabIndex = 4
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.BackColor = System.Drawing.Color.Transparent
        Me.Label107.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.Location = New System.Drawing.Point(283, 102)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(80, 15)
        Me.Label107.TabIndex = 102
        Me.Label107.Text = "Client Status:"
        '
        'txtMemberID
        '
        Me.txtMemberID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemberID.BackColor = System.Drawing.SystemColors.Window
        Me.txtMemberID.Location = New System.Drawing.Point(894, 100)
        Me.txtMemberID.MaxLength = 30
        Me.txtMemberID.Name = "txtMemberID"
        Me.txtMemberID.ReadOnly = True
        Me.txtMemberID.Size = New System.Drawing.Size(131, 20)
        Me.txtMemberID.TabIndex = 2
        Me.txtMemberID.Visible = False
        '
        'temp_placebirth
        '
        Me.temp_placebirth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_placebirth.Location = New System.Drawing.Point(115, 114)
        Me.temp_placebirth.MaxLength = 255
        Me.temp_placebirth.Name = "temp_placebirth"
        Me.temp_placebirth.Size = New System.Drawing.Size(661, 20)
        Me.temp_placebirth.TabIndex = 15
        '
        'txtage1
        '
        Me.txtage1.BackColor = System.Drawing.SystemColors.Window
        Me.txtage1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtage1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtage1.Location = New System.Drawing.Point(304, 80)
        Me.txtage1.MaxLength = 3
        Me.txtage1.Name = "txtage1"
        Me.txtage1.Size = New System.Drawing.Size(25, 14)
        Me.txtage1.TabIndex = 45
        Me.txtage1.Text = "0"
        '
        'lblage
        '
        Me.lblage.AutoSize = True
        Me.lblage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblage.Location = New System.Drawing.Point(278, 80)
        Me.lblage.Name = "lblage"
        Me.lblage.Size = New System.Drawing.Size(30, 14)
        Me.lblage.TabIndex = 44
        Me.lblage.Text = "Age "
        '
        'EMP_DATEbirth
        '
        Me.EMP_DATEbirth.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EMP_DATEbirth.Location = New System.Drawing.Point(115, 76)
        Me.EMP_DATEbirth.Name = "EMP_DATEbirth"
        Me.EMP_DATEbirth.Size = New System.Drawing.Size(150, 20)
        Me.EMP_DATEbirth.TabIndex = 10
        Me.EMP_DATEbirth.Value = New Date(2006, 3, 17, 0, 0, 0, 0)
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(3, 81)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(81, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Date of Birth:"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(92, 145)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(156, 20)
        Me.TextBox15.TabIndex = 11
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(92, 119)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(156, 20)
        Me.TextBox16.TabIndex = 10
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(92, 93)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(156, 20)
        Me.TextBox17.TabIndex = 9
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(92, 67)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(156, 20)
        Me.TextBox18.TabIndex = 8
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(92, 41)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(156, 20)
        Me.TextBox19.TabIndex = 7
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(17, 152)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(54, 13)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Address 3"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(17, 126)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(54, 13)
        Me.Label26.TabIndex = 5
        Me.Label26.Text = "Address 2"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(17, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(54, 13)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Address 1"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(17, 74)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Religion"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(70, 13)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "Place of Birth"
        '
        'MaskedTextBox5
        '
        Me.MaskedTextBox5.Location = New System.Drawing.Point(92, 15)
        Me.MaskedTextBox5.Mask = "LLLL,00,0000"
        Me.MaskedTextBox5.Name = "MaskedTextBox5"
        Me.MaskedTextBox5.Size = New System.Drawing.Size(79, 20)
        Me.MaskedTextBox5.TabIndex = 1
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(17, 22)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(66, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Date of Birth"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(803, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Client ID No.:"
        '
        'txtEmployeeNo
        '
        Me.txtEmployeeNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmployeeNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtEmployeeNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmployeeNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmployeeNo.Enabled = False
        Me.txtEmployeeNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeNo.Location = New System.Drawing.Point(894, 75)
        Me.txtEmployeeNo.MaxLength = 50
        Me.txtEmployeeNo.Name = "txtEmployeeNo"
        Me.txtEmployeeNo.Size = New System.Drawing.Size(130, 21)
        Me.txtEmployeeNo.TabIndex = 1
        '
        'temp_fname
        '
        Me.temp_fname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_fname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_fname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_fname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_fname.Enabled = False
        Me.temp_fname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_fname.Location = New System.Drawing.Point(68, 3)
        Me.temp_fname.MaxLength = 50
        Me.temp_fname.Name = "temp_fname"
        Me.temp_fname.Size = New System.Drawing.Size(288, 25)
        Me.temp_fname.TabIndex = 0
        '
        'temp_lname
        '
        Me.temp_lname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_lname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_lname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_lname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_lname.Enabled = False
        Me.temp_lname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_lname.Location = New System.Drawing.Point(67, 3)
        Me.temp_lname.MaxLength = 50
        Me.temp_lname.Name = "temp_lname"
        Me.temp_lname.Size = New System.Drawing.Size(267, 25)
        Me.temp_lname.TabIndex = 4
        '
        'temp_midname
        '
        Me.temp_midname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_midname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_midname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_midname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_midname.Enabled = False
        Me.temp_midname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_midname.Location = New System.Drawing.Point(78, 3)
        Me.temp_midname.MaxLength = 50
        Me.temp_midname.Name = "temp_midname"
        Me.temp_midname.Size = New System.Drawing.Size(225, 25)
        Me.temp_midname.TabIndex = 6
        '
        'txtprovincialadd
        '
        Me.txtprovincialadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtprovincialadd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtprovincialadd.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprovincialadd.Location = New System.Drawing.Point(115, 161)
        Me.txtprovincialadd.MaxLength = 1000
        Me.txtprovincialadd.Name = "txtprovincialadd"
        Me.txtprovincialadd.Size = New System.Drawing.Size(622, 21)
        Me.txtprovincialadd.TabIndex = 17
        '
        'cboemp_gender
        '
        Me.cboemp_gender.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_gender.BackColor = System.Drawing.Color.White
        Me.cboemp_gender.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_gender.FormattingEnabled = True
        Me.cboemp_gender.Items.AddRange(New Object() {"F", "M"})
        Me.cboemp_gender.Location = New System.Drawing.Point(115, 22)
        Me.cboemp_gender.Name = "cboemp_gender"
        Me.cboemp_gender.Size = New System.Drawing.Size(150, 23)
        Me.cboemp_gender.TabIndex = 3
        '
        'cboemp_civil
        '
        Me.cboemp_civil.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_civil.BackColor = System.Drawing.Color.White
        Me.cboemp_civil.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_civil.FormattingEnabled = True
        Me.cboemp_civil.Location = New System.Drawing.Point(115, 49)
        Me.cboemp_civil.Name = "cboemp_civil"
        Me.cboemp_civil.Size = New System.Drawing.Size(150, 23)
        Me.cboemp_civil.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "First Name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 14)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Last Name"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Middle Name"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 15)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Gender:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 15)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Civil Status:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 165)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(110, 15)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Provincial Address:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(19, 77)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 15)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "Organization:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(3, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(75, 15)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Contact No.:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(3, 49)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(40, 15)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "Email:"
        '
        'txtresidencephone
        '
        Me.txtresidencephone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtresidencephone.BackColor = System.Drawing.Color.White
        Me.txtresidencephone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtresidencephone.Location = New System.Drawing.Point(111, 22)
        Me.txtresidencephone.Name = "txtresidencephone"
        Me.txtresidencephone.Size = New System.Drawing.Size(201, 21)
        Me.txtresidencephone.TabIndex = 12
        '
        'txtEmailAddress
        '
        Me.txtEmailAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmailAddress.BackColor = System.Drawing.Color.White
        Me.txtEmailAddress.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailAddress.Location = New System.Drawing.Point(111, 45)
        Me.txtEmailAddress.MaxLength = 50
        Me.txtEmailAddress.Name = "txtEmailAddress"
        Me.txtEmailAddress.Size = New System.Drawing.Size(201, 21)
        Me.txtEmailAddress.TabIndex = 14
        '
        'txtorgchart
        '
        Me.txtorgchart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtorgchart.BackColor = System.Drawing.SystemColors.Window
        Me.txtorgchart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtorgchart.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtorgchart.Enabled = False
        Me.txtorgchart.Location = New System.Drawing.Point(112, 75)
        Me.txtorgchart.Name = "txtorgchart"
        Me.txtorgchart.Size = New System.Drawing.Size(655, 20)
        Me.txtorgchart.TabIndex = 0
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.Location = New System.Drawing.Point(3, 142)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(91, 15)
        Me.Label85.TabIndex = 74
        Me.Label85.Text = "Home Address:"
        '
        'txtResidenceadd
        '
        Me.txtResidenceadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtResidenceadd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtResidenceadd.Location = New System.Drawing.Point(115, 138)
        Me.txtResidenceadd.Name = "txtResidenceadd"
        Me.txtResidenceadd.Size = New System.Drawing.Size(622, 20)
        Me.txtResidenceadd.TabIndex = 16
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.BackColor = System.Drawing.Color.Transparent
        Me.Label86.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label86.Location = New System.Drawing.Point(19, 102)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(37, 15)
        Me.Label86.TabIndex = 76
        Me.Label86.Text = "Rank:"
        '
        'txtDepartment2
        '
        Me.txtDepartment2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartment2.Location = New System.Drawing.Point(605, 99)
        Me.txtDepartment2.Name = "txtDepartment2"
        Me.txtDepartment2.Size = New System.Drawing.Size(162, 20)
        Me.txtDepartment2.TabIndex = 13
        Me.txtDepartment2.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.SplitContainer2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 120)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1020, 40)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(1, 7)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.temp_lname)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label6)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer2.Size = New System.Drawing.Size(1018, 31)
        Me.SplitContainer2.SplitterDistance = 339
        Me.SplitContainer2.TabIndex = 46
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(675, 31)
        Me.Panel1.TabIndex = 0
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.IsSplitterFixed = True
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.temp_fname)
        Me.SplitContainer3.Panel1.Controls.Add(Me.Label5)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.temp_midname)
        Me.SplitContainer3.Panel2.Controls.Add(Me.Label7)
        Me.SplitContainer3.Size = New System.Drawing.Size(675, 31)
        Me.SplitContainer3.SplitterDistance = 362
        Me.SplitContainer3.TabIndex = 0
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.Black
        Me.Label66.Location = New System.Drawing.Point(3, 0)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(140, 14)
        Me.Label66.TabIndex = 11
        Me.Label66.Text = "PERSONAL INFORMATION"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(3, 118)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(69, 15)
        Me.Label67.TabIndex = 102
        Me.Label67.Text = "Birth Place:"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.btnSearchProvince)
        Me.Panel2.Controls.Add(Me.btnSearchHome)
        Me.Panel2.Controls.Add(Me.Label33)
        Me.Panel2.Controls.Add(Me.Label67)
        Me.Panel2.Controls.Add(Me.temp_placebirth)
        Me.Panel2.Controls.Add(Me.txtprovincialadd)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label85)
        Me.Panel2.Controls.Add(Me.txtResidenceadd)
        Me.Panel2.Location = New System.Drawing.Point(14, 160)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(783, 186)
        Me.Panel2.TabIndex = 14
        '
        'btnSearchProvince
        '
        Me.btnSearchProvince.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchProvince.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchProvince.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchProvince.Location = New System.Drawing.Point(741, 160)
        Me.btnSearchProvince.Name = "btnSearchProvince"
        Me.btnSearchProvince.Size = New System.Drawing.Size(35, 23)
        Me.btnSearchProvince.TabIndex = 109
        Me.btnSearchProvince.Text = "..."
        Me.btnSearchProvince.UseVisualStyleBackColor = True
        '
        'btnSearchHome
        '
        Me.btnSearchHome.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchHome.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchHome.Location = New System.Drawing.Point(741, 136)
        Me.btnSearchHome.Name = "btnSearchHome"
        Me.btnSearchHome.Size = New System.Drawing.Size(35, 23)
        Me.btnSearchHome.TabIndex = 108
        Me.btnSearchHome.Text = "..."
        Me.btnSearchHome.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(467, 86)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(63, 15)
        Me.Label33.TabIndex = 107
        Me.Label33.Text = "Signature:"
        '
        'txtpk_Employee
        '
        Me.txtpk_Employee.Location = New System.Drawing.Point(3, 0)
        Me.txtpk_Employee.Name = "txtpk_Employee"
        Me.txtpk_Employee.Size = New System.Drawing.Size(139, 20)
        Me.txtpk_Employee.TabIndex = 11
        Me.txtpk_Employee.Visible = False
        '
        'Label112
        '
        Me.Label112.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label112.AutoSize = True
        Me.Label112.BackColor = System.Drawing.Color.Transparent
        Me.Label112.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label112.Location = New System.Drawing.Point(802, 101)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(84, 15)
        Me.Label112.TabIndex = 107
        Me.Label112.Text = "Clients ID No.:"
        Me.Label112.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(14, 160)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label66)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtage1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblage)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EMP_DATEbirth)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label19)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboemp_gender)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboemp_civil)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtpk_Employee)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtresidencephone)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtEmailAddress)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label14)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label17)
        Me.SplitContainer1.Size = New System.Drawing.Size(765, 111)
        Me.SplitContainer1.SplitterDistance = 391
        Me.SplitContainer1.TabIndex = 103
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.GroupBox2)
        Me.Panel3.Location = New System.Drawing.Point(800, 161)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(227, 204)
        Me.Panel3.TabIndex = 108
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnMaxSignature)
        Me.GroupBox2.Controls.Add(Me.btnMaxPhoto)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.btnBrowseSignature)
        Me.GroupBox2.Controls.Add(Me.picEmpSignature)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.btnBrowsePicture)
        Me.GroupBox2.Controls.Add(Me.picEmpPhoto)
        Me.GroupBox2.Location = New System.Drawing.Point(2, -9)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(224, 212)
        Me.GroupBox2.TabIndex = 127
        Me.GroupBox2.TabStop = False
        '
        'btnMaxSignature
        '
        Me.btnMaxSignature.Location = New System.Drawing.Point(189, 118)
        Me.btnMaxSignature.Name = "btnMaxSignature"
        Me.btnMaxSignature.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxSignature.TabIndex = 128
        Me.btnMaxSignature.Text = "[*]"
        Me.btnMaxSignature.UseVisualStyleBackColor = True
        '
        'btnMaxPhoto
        '
        Me.btnMaxPhoto.Location = New System.Drawing.Point(191, 11)
        Me.btnMaxPhoto.Name = "btnMaxPhoto"
        Me.btnMaxPhoto.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxPhoto.TabIndex = 127
        Me.btnMaxPhoto.Text = "[*]"
        Me.btnMaxPhoto.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(6, 9)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(44, 15)
        Me.Label32.TabIndex = 126
        Me.Label32.Text = "Photo:"
        '
        'btnBrowseSignature
        '
        Me.btnBrowseSignature.Location = New System.Drawing.Point(189, 183)
        Me.btnBrowseSignature.Name = "btnBrowseSignature"
        Me.btnBrowseSignature.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowseSignature.TabIndex = 125
        Me.btnBrowseSignature.Text = "..."
        Me.btnBrowseSignature.UseVisualStyleBackColor = True
        Me.btnBrowseSignature.Visible = False
        '
        'picEmpSignature
        '
        Me.picEmpSignature.BackColor = System.Drawing.Color.DarkGray
        Me.picEmpSignature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEmpSignature.Image = CType(resources.GetObject("picEmpSignature.Image"), System.Drawing.Image)
        Me.picEmpSignature.Location = New System.Drawing.Point(65, 115)
        Me.picEmpSignature.Name = "picEmpSignature"
        Me.picEmpSignature.Size = New System.Drawing.Size(120, 91)
        Me.picEmpSignature.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEmpSignature.TabIndex = 124
        Me.picEmpSignature.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(3, 109)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(63, 15)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "Signature:"
        '
        'btnBrowsePicture
        '
        Me.btnBrowsePicture.Location = New System.Drawing.Point(189, 89)
        Me.btnBrowsePicture.Name = "btnBrowsePicture"
        Me.btnBrowsePicture.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowsePicture.TabIndex = 123
        Me.btnBrowsePicture.Text = "..."
        Me.btnBrowsePicture.UseVisualStyleBackColor = True
        Me.btnBrowsePicture.Visible = False
        '
        'picEmpPhoto
        '
        Me.picEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEmpPhoto.Image = CType(resources.GetObject("picEmpPhoto.Image"), System.Drawing.Image)
        Me.picEmpPhoto.Location = New System.Drawing.Point(65, 9)
        Me.picEmpPhoto.Name = "picEmpPhoto"
        Me.picEmpPhoto.Size = New System.Drawing.Size(120, 100)
        Me.picEmpPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEmpPhoto.TabIndex = 122
        Me.picEmpPhoto.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(772, 75)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 20)
        Me.PictureBox2.TabIndex = 45
        Me.PictureBox2.TabStop = False
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.BackColor = System.Drawing.Color.Transparent
        Me.Label45.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(19, 50)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(43, 14)
        Me.Label45.TabIndex = 109
        Me.Label45.Text = "Group :"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(524, 50)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(57, 14)
        Me.Label46.TabIndex = 110
        Me.Label46.Text = "Category :"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(760, 50)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(37, 14)
        Me.Label47.TabIndex = 111
        Me.Label47.Text = "Type :"
        '
        'cbofGroup
        '
        Me.cbofGroup.Enabled = False
        Me.cbofGroup.FormattingEnabled = True
        Me.cbofGroup.Location = New System.Drawing.Point(112, 47)
        Me.cbofGroup.Name = "cbofGroup"
        Me.cbofGroup.Size = New System.Drawing.Size(167, 21)
        Me.cbofGroup.TabIndex = 112
        '
        'cbofType
        '
        Me.cbofType.Enabled = False
        Me.cbofType.FormattingEnabled = True
        Me.cbofType.Location = New System.Drawing.Point(803, 47)
        Me.cbofType.Name = "cbofType"
        Me.cbofType.Size = New System.Drawing.Size(167, 21)
        Me.cbofType.TabIndex = 113
        '
        'cbofCategory
        '
        Me.cbofCategory.Enabled = False
        Me.cbofCategory.FormattingEnabled = True
        Me.cbofCategory.Location = New System.Drawing.Point(587, 47)
        Me.cbofCategory.Name = "cbofCategory"
        Me.cbofCategory.Size = New System.Drawing.Size(167, 21)
        Me.cbofCategory.TabIndex = 114
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.ForeColor = System.Drawing.Color.Red
        Me.Label53.Location = New System.Drawing.Point(542, 107)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(11, 13)
        Me.Label53.TabIndex = 115
        Me.Label53.Text = "*"
        '
        'cboRank
        '
        Me.cboRank.FormattingEnabled = True
        Me.cboRank.Location = New System.Drawing.Point(112, 98)
        Me.cboRank.Name = "cboRank"
        Me.cboRank.Size = New System.Drawing.Size(165, 21)
        Me.cboRank.TabIndex = 116
        '
        'txtCompanyChart
        '
        Me.txtCompanyChart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCompanyChart.Enabled = False
        Me.txtCompanyChart.Location = New System.Drawing.Point(112, 72)
        Me.txtCompanyChart.Name = "txtCompanyChart"
        Me.txtCompanyChart.Size = New System.Drawing.Size(655, 20)
        Me.txtCompanyChart.TabIndex = 117
        Me.txtCompanyChart.Visible = False
        '
        'cboSubgroup
        '
        Me.cboSubgroup.Enabled = False
        Me.cboSubgroup.FormattingEnabled = True
        Me.cboSubgroup.Location = New System.Drawing.Point(351, 47)
        Me.cboSubgroup.Name = "cboSubgroup"
        Me.cboSubgroup.Size = New System.Drawing.Size(167, 21)
        Me.cboSubgroup.TabIndex = 119
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.BackColor = System.Drawing.Color.Transparent
        Me.Label60.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(283, 50)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(62, 14)
        Me.Label60.TabIndex = 118
        Me.Label60.Text = "Sub-Group:"
        '
        'btnemp_close
        '
        Me.btnemp_close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_close.BackColor = System.Drawing.Color.Transparent
        Me.btnemp_close.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_close.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnemp_close.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_close.Location = New System.Drawing.Point(958, 4)
        Me.btnemp_close.Name = "btnemp_close"
        Me.btnemp_close.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_close.TabIndex = 7
        Me.btnemp_close.Text = "&Close"
        Me.btnemp_close.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_close.UseVisualStyleBackColor = False
        '
        'btnemp_next
        '
        Me.btnemp_next.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_next.BackColor = System.Drawing.Color.Transparent
        Me.btnemp_next.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_next.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_next.Location = New System.Drawing.Point(146, 4)
        Me.btnemp_next.Name = "btnemp_next"
        Me.btnemp_next.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_next.TabIndex = 3
        Me.btnemp_next.Text = ">>"
        Me.btnemp_next.UseVisualStyleBackColor = False
        '
        'btnemp_delete
        '
        Me.btnemp_delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_delete.BackColor = System.Drawing.Color.Transparent
        Me.btnemp_delete.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_delete.Location = New System.Drawing.Point(890, 4)
        Me.btnemp_delete.Name = "btnemp_delete"
        Me.btnemp_delete.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_delete.TabIndex = 6
        Me.btnemp_delete.Text = "&Delete"
        Me.btnemp_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_delete.UseVisualStyleBackColor = False
        '
        'BTNSEARCH
        '
        Me.BTNSEARCH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTNSEARCH.BackColor = System.Drawing.Color.Transparent
        Me.BTNSEARCH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTNSEARCH.Location = New System.Drawing.Point(76, 4)
        Me.BTNSEARCH.Name = "BTNSEARCH"
        Me.BTNSEARCH.Size = New System.Drawing.Size(66, 28)
        Me.BTNSEARCH.TabIndex = 2
        Me.BTNSEARCH.UseVisualStyleBackColor = False
        '
        'btnemp_edit
        '
        Me.btnemp_edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_edit.BackColor = System.Drawing.Color.Transparent
        Me.btnemp_edit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_edit.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_edit.Location = New System.Drawing.Point(821, 4)
        Me.btnemp_edit.Name = "btnemp_edit"
        Me.btnemp_edit.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_edit.TabIndex = 5
        Me.btnemp_edit.Text = "&Edit"
        Me.btnemp_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_edit.UseVisualStyleBackColor = False
        '
        'btnemp_previous
        '
        Me.btnemp_previous.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_previous.BackColor = System.Drawing.Color.Transparent
        Me.btnemp_previous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_previous.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_previous.Location = New System.Drawing.Point(7, 4)
        Me.btnemp_previous.Name = "btnemp_previous"
        Me.btnemp_previous.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_previous.TabIndex = 1
        Me.btnemp_previous.Text = "<<"
        Me.btnemp_previous.UseVisualStyleBackColor = False
        '
        'btnemp_add
        '
        Me.btnemp_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_add.BackColor = System.Drawing.Color.Transparent
        Me.btnemp_add.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_add.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_add.Location = New System.Drawing.Point(752, 4)
        Me.btnemp_add.Name = "btnemp_add"
        Me.btnemp_add.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_add.TabIndex = 4
        Me.btnemp_add.Text = "&New"
        Me.btnemp_add.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_add.UseVisualStyleBackColor = False
        '
        'lblemployee_name
        '
        Me.lblemployee_name.AutoSize = True
        Me.lblemployee_name.BackColor = System.Drawing.Color.Transparent
        Me.lblemployee_name.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblemployee_name.ForeColor = System.Drawing.Color.Black
        Me.lblemployee_name.Location = New System.Drawing.Point(12, 9)
        Me.lblemployee_name.Name = "lblemployee_name"
        Me.lblemployee_name.Size = New System.Drawing.Size(29, 19)
        Me.lblemployee_name.TabIndex = 13
        Me.lblemployee_name.Text = "....."
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel4.Controls.Add(Me.lblemployee_name)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1028, 33)
        Me.Panel4.TabIndex = 120
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel5.Controls.Add(Me.btnemp_close)
        Me.Panel5.Controls.Add(Me.btnemp_delete)
        Me.Panel5.Controls.Add(Me.btnemp_next)
        Me.Panel5.Controls.Add(Me.btnemp_edit)
        Me.Panel5.Controls.Add(Me.btnemp_add)
        Me.Panel5.Controls.Add(Me.BTNSEARCH)
        Me.Panel5.Controls.Add(Me.btnemp_previous)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 573)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1028, 35)
        Me.Panel5.TabIndex = 122
        '
        'frmMember_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.btnemp_close
        Me.ClientSize = New System.Drawing.Size(1028, 608)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.cboSubgroup)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.txtCompanyChart)
        Me.Controls.Add(Me.cboRank)
        Me.Controls.Add(Me.Label53)
        Me.Controls.Add(Me.cbofCategory)
        Me.Controls.Add(Me.cbofType)
        Me.Controls.Add(Me.cbofGroup)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.Label46)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label112)
        Me.Controls.Add(Me.txtMemberID)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Cbomem_Status)
        Me.Controls.Add(Me.Label107)
        Me.Controls.Add(Me.txtDepartment2)
        Me.Controls.Add(Me.Label86)
        Me.Controls.Add(Me.txtEmployeeNo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tabMember)
        Me.Controls.Add(Me.txtorgchart)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MinimumSize = New System.Drawing.Size(781, 581)
        Me.Name = "frmMember_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        Me.tabMember.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.grpPayrollStatus.ResumeLayout(False)
        Me.grpPayrollStatus.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.others.ResumeLayout(False)
        Me.others.PerformLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.dgvemail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvphone, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvcivil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvaddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvlastname, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.PerformLayout()
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.Panel2.PerformLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.picEmpSignature, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEmpPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMember As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents temp_placebirth As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents temp_add3 As System.Windows.Forms.TextBox
    Friend WithEvents temp_add2 As System.Windows.Forms.TextBox
    Friend WithEvents txtofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtofficeadd As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox5 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents temp_marks As System.Windows.Forms.TextBox
    Friend WithEvents temp_height As System.Windows.Forms.TextBox
    Friend WithEvents temp_citizen As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents btndelete_dep As System.Windows.Forms.Button
    Friend WithEvents btnadd_dep As System.Windows.Forms.Button
    Friend WithEvents btnemp_previous As System.Windows.Forms.Button
    Friend WithEvents btnemp_next As System.Windows.Forms.Button
    Friend WithEvents btnemp_add As System.Windows.Forms.Button
    Friend WithEvents btnemp_edit As System.Windows.Forms.Button
    Friend WithEvents btnemp_delete As System.Windows.Forms.Button
    Friend WithEvents btnemp_close As System.Windows.Forms.Button
    Friend WithEvents txtbasicpay As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cbopayroll As System.Windows.Forms.ComboBox
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents cborate As System.Windows.Forms.ComboBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents EMP_DATEbirth As System.Windows.Forms.DateTimePicker
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents Address2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnemp_editdepdts As System.Windows.Forms.Button
    Friend WithEvents emp_pic As System.Windows.Forms.PictureBox
    Friend WithEvents BTNSEARCH As System.Windows.Forms.Button
    Friend WithEvents temp_designation As System.Windows.Forms.TextBox
    Friend WithEvents txtEmailAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtresidencephone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents emp_Datehired As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboemp_civil As System.Windows.Forms.ComboBox
    Friend WithEvents cboemp_gender As System.Windows.Forms.ComboBox
    Friend WithEvents txtprovincialadd As System.Windows.Forms.TextBox
    Friend WithEvents temp_midname As System.Windows.Forms.TextBox
    Friend WithEvents temp_lname As System.Windows.Forms.TextBox
    Friend WithEvents temp_fname As System.Windows.Forms.TextBox
    Friend WithEvents txtEmployeeNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboEmp_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblemployee_name As System.Windows.Forms.Label
    Friend WithEvents txtlocalofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtweight As System.Windows.Forms.TextBox
    Friend WithEvents txtremarks As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents cbosalarygrade As System.Windows.Forms.ComboBox
    Friend WithEvents btnrestricted As System.Windows.Forms.Button
    Friend WithEvents TXTKEYEMPLOYEEID As System.Windows.Forms.TextBox
    Friend WithEvents TXTRECID As System.Windows.Forms.TextBox
    Friend WithEvents cbotitledesignation As System.Windows.Forms.ComboBox
    Friend WithEvents Dateresigned As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtorgchart As System.Windows.Forms.TextBox
    Friend WithEvents txtfullname As System.Windows.Forms.TextBox
    Friend WithEvents lblage As System.Windows.Forms.Label
    Friend WithEvents txtage1 As System.Windows.Forms.TextBox
    Friend WithEvents cbodept As System.Windows.Forms.TextBox
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents txtResidenceadd As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartment2 As System.Windows.Forms.TextBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtType_employee As System.Windows.Forms.TextBox
    Friend WithEvents txtfxkeypositionlevel As System.Windows.Forms.TextBox
    Friend WithEvents txtkeycompany As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydivision As System.Windows.Forms.TextBox
    Friend WithEvents txtdescsection As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdivision As System.Windows.Forms.TextBox
    Friend WithEvents txtkeysection As System.Windows.Forms.TextBox
    Friend WithEvents txtparent_ID As System.Windows.Forms.TextBox
    Friend WithEvents TabPage17 As System.Windows.Forms.TabPage
    Friend WithEvents lvlNearestRelatives As System.Windows.Forms.ListView
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents BtnEditContact As System.Windows.Forms.Button
    Friend WithEvents BtnDeleteContact As System.Windows.Forms.Button
    Friend WithEvents btnaddContact As System.Windows.Forms.Button
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents txtMemberID As System.Windows.Forms.TextBox
    Friend WithEvents emp_company As System.Windows.Forms.TextBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Cbomem_Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents emp_Ytenures As System.Windows.Forms.TextBox
    Friend WithEvents cboPositionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents cbotaxcode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlBankInfo As System.Windows.Forms.ListView
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents lvlSourceIncome As System.Windows.Forms.ListView
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents txtpk_Employee As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateBA As System.Windows.Forms.Button
    Friend WithEvents btnDeleteBA As System.Windows.Forms.Button
    Friend WithEvents btnNewBA As System.Windows.Forms.Button
    Friend WithEvents btnUpdateSInc As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSInc As System.Windows.Forms.Button
    Friend WithEvents btnsaveSInc As System.Windows.Forms.Button
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtitemno As System.Windows.Forms.TextBox
    Friend WithEvents txtmonthreqular As System.Windows.Forms.TextBox
    Friend WithEvents txtsalarygrade As System.Windows.Forms.TextBox
    Friend WithEvents txtdateregular As System.Windows.Forms.TextBox
    Friend WithEvents Dateregular As System.Windows.Forms.DateTimePicker
    Friend WithEvents emp_extphone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnAddMobileNo As System.Windows.Forms.Button
    Friend WithEvents others As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkIsEloader As System.Windows.Forms.CheckBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents chkShowPINChar As System.Windows.Forms.CheckBox
    Friend WithEvents txtEloaderPIN As System.Windows.Forms.TextBox
    Friend WithEvents txtEloadingLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents gridMobileNos As System.Windows.Forms.DataGridView
    Friend WithEvents txtMobileNo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnEditMobile As System.Windows.Forms.Button
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseSignature As System.Windows.Forms.Button
    Friend WithEvents picEmpSignature As System.Windows.Forms.PictureBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnBrowsePicture As System.Windows.Forms.Button
    Friend WithEvents picEmpPhoto As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents dgvemail As System.Windows.Forms.DataGridView
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents dgvphone As System.Windows.Forms.DataGridView
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents dgvcivil As System.Windows.Forms.DataGridView
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents dgvaddress As System.Windows.Forms.DataGridView
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents dgvlastname As System.Windows.Forms.DataGridView
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents cbofCategory As System.Windows.Forms.ComboBox
    Friend WithEvents cbofType As System.Windows.Forms.ComboBox
    Friend WithEvents cbofGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents btnMaxPhoto As System.Windows.Forms.Button
    Friend WithEvents btnMaxSignature As System.Windows.Forms.Button
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents grpPayrollStatus As System.Windows.Forms.GroupBox
    Friend WithEvents rdoNonExempt As System.Windows.Forms.RadioButton
    Friend WithEvents rdoExempt As System.Windows.Forms.RadioButton
    Friend WithEvents chkBereaveYes As System.Windows.Forms.CheckBox
    Friend WithEvents chkBereaveNo As System.Windows.Forms.CheckBox
    Friend WithEvents mem_WithdrawDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents mem_PaycontDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkNo As System.Windows.Forms.CheckBox
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents txtpayrollcontriamount As System.Windows.Forms.TextBox
    Friend WithEvents chkyes As System.Windows.Forms.CheckBox
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtwithdrawal As System.Windows.Forms.TextBox
    Friend WithEvents mem_MemberDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents cboemp_status As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents cboRank As System.Windows.Forms.ComboBox
    Friend WithEvents txtCompanyChart As System.Windows.Forms.TextBox
    Friend WithEvents LvlEmployeeDependent As System.Windows.Forms.ListView
    Friend WithEvents txtHDMF As System.Windows.Forms.TextBox
    Friend WithEvents txtPhilhealth As System.Windows.Forms.TextBox
    Friend WithEvents txtSSS As System.Windows.Forms.TextBox
    Friend WithEvents txtTin As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents dtBoardApproval As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents btnSearchProvince As System.Windows.Forms.Button
    Friend WithEvents btnSearchHome As System.Windows.Forms.Button
    Friend WithEvents cboSubgroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents txtEcola As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents txtContractPrice As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
End Class
