Option Strict Off
Option Explicit On
Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class clsPersonnel
    'database connection

    Protected sqlConn As New SqlConnection
    Protected sqlcmd As New SqlCommand
    Public cnstring As String
    Public Server, Database, Username, Password As String
    Public servername As String = ConfigurationSettings.AppSettings.Get("server").ToString
    Public databasename As String = ConfigurationSettings.AppSettings.Get("database").ToString
    Public username1 As String = ConfigurationSettings.AppSettings.Get("username").ToString
    Public password1 As String = ConfigurationSettings.AppSettings.Get("password").ToString

#Region "variables"
    Public myerrormessage As String
#End Region
#Region "get property"
    Public Property geterrormessage() As String
        Get
            Return myerrormessage
        End Get
        Set(ByVal value As String)
            myerrormessage = value
        End Set
    End Property
#End Region
    Public Sub getappconfig()
        getservername = servername
        getdatabasename = databasename
    End Sub
    Public Sub New()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration

        Server = appRdr.GetValue("Server", GetType(String))
        Database = appRdr.GetValue("Database", GetType(String))
        Username = appRdr.GetValue("Username", GetType(String))
        Password = appRdr.GetValue("Password", GetType(String))
        cnstring = "Data Source= '" & Server & "' ;Initial Catalog= '" & Database & "' ;User Id= '" & Username & "';Password='" & Password & "' ; Connection Timeout=0;"
        sqlConn = New SqlConnection(cnstring)


        sqlcmd = sqlConn.CreateCommand
        sqlcmd.CommandTimeout = 0
    End Sub

    'functions to use
    ' datatable
    Public Function DataQuery(ByVal sqlScript As String, ByVal cmdType As CommandType, Optional ByRef errMsg As String = "") As DataTable
        sqlcmd.CommandType = cmdType
        sqlcmd.CommandText = sqlScript
        Dim sqlDa As New SqlDataAdapter(sqlcmd)
        Dim DT As New DataTable
        Try
            sqlConn.Open()
            sqlDa.Fill(DT)
            Return DT
        Catch ex As Exception
            errMsg = ex.Message
        Finally
            sqlConn.Close()
        End Try
        sqlDa = Nothing
        DT = Nothing

    End Function
    'execute non query
    'no return
    Public Sub ExecuteNonQuery(ByVal sqlScript As String, ByVal cmdType As CommandType, Optional ByRef errMsg As String = "")
        Try
            sqlConn.Open()
            sqlcmd.CommandType = cmdType
            sqlcmd.CommandText = sqlScript
            sqlcmd.CommandTimeout = 0
            sqlcmd.ExecuteNonQuery()

        Catch ex As SqlException
            errMsg = ex.Message
        Catch ex As Exception
            errMsg = ex.Message
        Finally
            sqlConn.Close()
        End Try
    End Sub
    'clear parameters
    Public Sub Clear_SQLParameters()
        sqlcmd.Parameters.Clear()
    End Sub

    'add parameter for script or stored proc
    Public Sub Add_SQLParameter(ByVal Name As String, ByVal Datatype As SqlDbType, ByVal Value As Object, Optional ByVal paramDirection As System.Data.ParameterDirection = ParameterDirection.Input, Optional ByVal paramSize As Integer = 0)
        'check if name starts with an '@' sign
        If Name.Substring(0, 1) <> "@" Then
            Name = Name.Insert(0, "@")
        End If
        sqlcmd.Parameters.Add(Name, Datatype).Value = Value
        sqlcmd.Parameters(Name).Direction = paramDirection
        sqlcmd.Parameters(Name).Size = paramSize

    End Sub
    Public Function getParameterValue(ByVal paramName As String) As Object
        If sqlCmd.Parameters.Count <= 0 Then
            Return Nothing
        Else
            If paramName.Substring(0, 1) <> "@" Then
                paramName = paramName.Insert(0, "@")
            End If
            Return IIf(IsNothing(sqlCmd.Parameters(paramName)), Nothing, sqlCmd.Parameters(paramName).Value)
        End If
    End Function

    'my functions
    Public Function LoadAllLevelDefinition(Optional ByRef errmsg As String = "") As DataTable
        errmsg = ""
        Dim dt As DataTable
        Try
            Me.Clear_SQLParameters()
            dt = Me.DataQuery("dbo.usp_LoadAllLevelDefinition", CommandType.StoredProcedure, errmsg)
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            Else
                Return dt
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try
    End Function
    Public Function LoadParentCode(ByVal orglevel As Integer, Optional ByRef errmsg As String = "") As DataTable
        errmsg = ""
        Dim dt As DataTable
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@OrgLevel", SqlDbType.Int, orglevel)
            dt = Me.DataQuery("dbo.usp_LoadParentCode", CommandType.StoredProcedure, errmsg)

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            Else
                Return dt
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try
    End Function

    Public Sub LevelMaintenance(ByVal leveloption As Integer, ByVal level As Integer, ByVal code As String, ByVal desc As String, Optional ByRef errmsg As String = "")
        errmsg = ""
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@option", SqlDbType.Int, leveloption)
            Me.Add_SQLParameter("@level", SqlDbType.VarChar, level)
            Me.Add_SQLParameter("@code", SqlDbType.VarChar, code)
            Me.Add_SQLParameter("@description", SqlDbType.VarChar, desc)
            Me.Add_SQLParameter("@errormsg", SqlDbType.VarChar, errmsg, ParameterDirection.Output, 1000)
            Me.ExecuteNonQuery("dbo.usp_LevelMaintenance", CommandType.StoredProcedure, errmsg)
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            Else
                errmsg = CStr(IIf(IsDBNull(Me.getParameterValue("@ErrorMsg")), "", Me.getParameterValue("@ErrorMsg")))
                geterrormessage() = errmsg
            End If
        Catch ex As Exception
            errmsg = ex.Message
            'errmsg = MessageBox.Show("Record cannot be deleted", "Delete cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub UploadEmployeeMaster(ByVal EmployeeID As String, ByVal LastName As String, ByVal FirstName As String, ByVal middlename As String, _
    ByVal designation As String, ByVal department As String, ByVal datehired As String, ByVal datebirth As String, ByVal gender As String, _
    ByVal dateregular As String, ByVal sg As String, ByVal employeetype As String, ByVal civilstatus As String, ByVal status As String, ByVal basic_salary As String, _
    ByVal Philhealth As String, ByVal sss As String, ByVal tin As String, ByVal pagibig As String, _
    ByVal address As String, ByVal address2 As String, ByVal address3 As String, ByVal homephone As String, ByVal mobileno As String, ByVal MachineID As String, ByVal orgcompany As String, ByVal orgdivision As String, _
    ByVal orgdepartment As String, ByVal orgsection As String, ByVal orgunit As String, ByVal positioncode As String, ByVal departmentorg As String, _
    ByVal taxcode As String, ByVal ratetype As String, ByVal atm As String, ByVal dayspermonth As String, ByVal hours As String, ByVal pagibigamount As String, _
    ByVal payrollschedule As String, ByVal resigned As String, Optional ByVal errmsg As String = "") 'ByVal payresigned As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@EmployeeID", SqlDbType.VarChar, EmployeeID)
            Me.Add_SQLParameter("@LastName", SqlDbType.VarChar, LastName)
            Me.Add_SQLParameter("@FirstName", SqlDbType.VarChar, FirstName)
            Me.Add_SQLParameter("@MiddleName", SqlDbType.VarChar, middlename)
            Me.Add_SQLParameter("@Designation", SqlDbType.VarChar, designation)
            Me.Add_SQLParameter("@Department", SqlDbType.VarChar, department)
            Me.Add_SQLParameter("@Datehired", SqlDbType.VarChar, datehired)
            Me.Add_SQLParameter("@Datebirth", SqlDbType.VarChar, datebirth)
            Me.Add_SQLParameter("@Gender", SqlDbType.VarChar, gender)
            Me.Add_SQLParameter("@DateRegular", SqlDbType.VarChar, dateregular)
            Me.Add_SQLParameter("@SG", SqlDbType.VarChar, sg)
            Me.Add_SQLParameter("@EmployeeType", SqlDbType.VarChar, employeetype)
            Me.Add_SQLParameter("@CivilStatus", SqlDbType.VarChar, civilstatus)
            Me.Add_SQLParameter("@Status", SqlDbType.VarChar, status)
            Me.Add_SQLParameter("@Basic_Salary", SqlDbType.VarChar, basic_salary) 'encryp_decrpy.GetEncryptedData(basic_salary))
            Me.Add_SQLParameter("@fxKeyemployee", SqlDbType.VarChar, "") ' 10, ParameterDirection.Output, "")
            Me.Add_SQLParameter("@SSS", SqlDbType.VarChar, sss)
            Me.Add_SQLParameter("@TIN_no", SqlDbType.VarChar, tin)
            Me.Add_SQLParameter("@phil_health_no", SqlDbType.VarChar, Philhealth)
            Me.Add_SQLParameter("@pag_ibig_no", SqlDbType.VarChar, pagibig)
            Me.Add_SQLParameter("@address_1", SqlDbType.VarChar, address)
            Me.Add_SQLParameter("@address_2", SqlDbType.VarChar, address2)
            Me.Add_SQLParameter("@address_3", SqlDbType.VarChar, address3)
            Me.Add_SQLParameter("@Office_Phone", SqlDbType.VarChar, homephone)
            Me.Add_SQLParameter("@Mobile_Phone", SqlDbType.VarChar, mobileno)
            Me.Add_SQLParameter("@timemachineID", SqlDbType.VarChar, MachineID)

            Me.Add_SQLParameter("@OrgCompany", SqlDbType.Char, orgcompany)
            Me.Add_SQLParameter("@OrgDivision", SqlDbType.Char, orgdivision)
            Me.Add_SQLParameter("@OrgDepartment", SqlDbType.Char, orgdepartment)
            Me.Add_SQLParameter("@OrgSection", SqlDbType.Char, department)
            Me.Add_SQLParameter("@OrgUnit", SqlDbType.Char, orgunit)
            Me.Add_SQLParameter("@Positioncode", SqlDbType.Char, positioncode)
            Me.Add_SQLParameter("@DeptmentOrg", SqlDbType.VarChar, departmentorg)

            Me.Add_SQLParameter("@tax", SqlDbType.VarChar, taxcode)
            Me.Add_SQLParameter("@atm_no", SqlDbType.VarChar, atm)
            Me.Add_SQLParameter("@per_month", SqlDbType.VarChar, dayspermonth)
            Me.Add_SQLParameter("@rate", SqlDbType.VarChar, ratetype)
            Me.Add_SQLParameter("@Hours", SqlDbType.VarChar, hours)
            Me.Add_SQLParameter("@PagibigContribution", SqlDbType.VarChar, pagibigamount)
            Me.Add_SQLParameter("@Payroll", SqlDbType.VarChar, payrollschedule)
            Me.Add_SQLParameter("@Resigned", SqlDbType.VarChar, resigned)
            'Me.Add_SQLParameter("@PayResigned", SqlDbType.VarChar, payresigned)

            Me.ExecuteNonQuery("dbo.uspUploadEmployeeMasterFile", CommandType.StoredProcedure, errmsg)
            'Me.ExecuteNonQuery("uspUploadEmployeeMasterFile_Revised", CommandType.StoredProcedure, errmsg)
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try

    End Sub
#Region "UPLOAD APPLICANT MASTERFILE"
    Public Sub UploadApplicant(ByVal ApplicantID As String, ByVal lastname As String, ByVal firstname As String, ByVal middlename As String, _
                               ByVal dateapplied As String, ByVal PositionCode As String, ByVal SourcingCode As String, ByVal Category As String, _
                               ByVal cvlocation As String, ByVal residentialadd As String, ByVal mobile As String, ByVal landline As String, _
                               ByVal gender As String, ByVal height As String, ByVal weight As String, ByVal CivilStatus As String, _
                               ByVal birthdate As String, ByVal birthplace As String, ByVal sss As String, ByVal philhealth As String, ByVal pagibig As String, _
                               ByVal tin As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@IDnumber", SqlDbType.VarChar, ApplicantID)
            Me.Add_SQLParameter("@Surname", SqlDbType.VarChar, lastname)
            Me.Add_SQLParameter("@FirstName", SqlDbType.VarChar, firstname)
            Me.Add_SQLParameter("@Middlename", SqlDbType.VarChar, middlename)
            Me.Add_SQLParameter("@Dateapplied", SqlDbType.VarChar, dateapplied)
            Me.Add_SQLParameter("@Positioncode", SqlDbType.VarChar, PositionCode)
            Me.Add_SQLParameter("@Sourcingcode", SqlDbType.VarChar, SourcingCode)
            Me.Add_SQLParameter("@Category", SqlDbType.VarChar, Category)
            Me.Add_SQLParameter("@Cvlocation", SqlDbType.VarChar, cvlocation)
            Me.Add_SQLParameter("@address_2", SqlDbType.VarChar, residentialadd)
            Me.Add_SQLParameter("@Cell_No", SqlDbType.VarChar, mobile)
            Me.Add_SQLParameter("@Contact_No", SqlDbType.VarChar, landline)
            Me.Add_SQLParameter("@Gender", SqlDbType.VarChar, gender)
            Me.Add_SQLParameter("@Height", SqlDbType.VarChar, height)
            Me.Add_SQLParameter("@Weight", SqlDbType.VarChar, weight) 'encryp_decrpy.GetEncryptedData(basic_salary))
            Me.Add_SQLParameter("@Civil_Status", SqlDbType.VarChar, CivilStatus) ' 10, ParameterDirection.Output, "")
            Me.Add_SQLParameter("@Birthdate", SqlDbType.VarChar, birthdate)
            Me.Add_SQLParameter("@Birth_Place", SqlDbType.VarChar, birthplace)
            Me.Add_SQLParameter("@SSS", SqlDbType.VarChar, sss)
            Me.Add_SQLParameter("@PhilHealth", SqlDbType.VarChar, philhealth)
            Me.Add_SQLParameter("@PAG_IBIG", SqlDbType.VarChar, pagibig)
            Me.Add_SQLParameter("@TIN", SqlDbType.VarChar, tin)


            Me.ExecuteNonQuery("usp_ApplicantMasterfile_Upload", CommandType.StoredProcedure, errmsg)
            'Me.ExecuteNonQuery("uspUploadEmployeeMasterFile_Revised", CommandType.StoredProcedure, errmsg)
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try

    End Sub
#End Region
#Region "payroll division insert"
    Public Sub SavepayrollDivision1(ByVal divcode As String, ByVal descdiv As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_division_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                '.Add("@orgid", SqlDbType.Char, 2).Value = orgid
                .Add("@Code", SqlDbType.Char, 10).Value = divcode
                .Add("@Desc", SqlDbType.VarChar, 50).Value = descdiv
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check usp_per_division_insert", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Insert into Payroll Masterfile"
    'Public Sub InsertintoPayroll(ByVal EmployeeID As String, ByVal LastName As String, ByVal FirstName As String, ByVal middlename As String, _
    'ByVal designation As String, ByVal datehired As String, ByVal datebirth As String, ByVal gender As String, _
    'ByVal Philhealth As String, ByVal sss As String, ByVal tin As String, ByVal pagibig As String, _
    'ByVal address As String, ByVal MachineID As String, ByVal address2 As String, ByVal address3 As String, _
    'ByVal homephone As String, ByVal mobile As String, Optional ByVal errmsg As String = "")
    ' errmsg = ""
    ' Dim myconnection As New Clsappconfiguration
    ' Dim cmd As New SqlCommand("updload_payroll_masterfile", myconnection.sqlconn)
    ' cmd.CommandType = CommandType.StoredProcedure
    ' With cmd.Parameters
    '     .Add("@fcEmployeeID", SqlDbType.VarChar, 30).Value = EmployeeID
    '     .Add("@fcLastName", SqlDbType.VarChar, 60).Value = LastName
    '     .Add("@fcFirstName", SqlDbType.VarChar, 60).Value = FirstName
    '     .Add("@fcMiddleName", SqlDbType.VarChar, 60).Value = middlename
    '     .Add("@fcDesignation", SqlDbType.VarChar, 60).Value = designation
    '     .Add("@fdDateHired", SqlDbType.VarChar, 15).Value = datehired
    '     .Add("@fdDateBirth", SqlDbType.VarChar, 15).Value = datebirth
    '     .Add("@fcGender", SqlDbType.VarChar, 15).Value = gender
    '     '.Add("@fnBasicPay", SqlDbType.Decimal).Value = Decimal.Parse(basic_salary)
    '     .Add("@fcPhilHealthNo", SqlDbType.VarChar, 15).Value = Philhealth
    '     .Add("@fcSSSNo", SqlDbType.VarChar, 15).Value = sss
    '     .Add("@fcTINNo", SqlDbType.VarChar, 15).Value = tin
    '     .Add("@fcPagIBIGNo", SqlDbType.VarChar, 15).Value = pagibig
    '     .Add("@fcAddress1", SqlDbType.VarChar, 15).Value = address
    '     .Add("@fcEmployeeTKID", SqlDbType.VarChar, 15).Value = MachineID
    '     .Add("@fcAddress2", SqlDbType.VarChar, 15).Value = address2
    '     .Add("@fcAddress3", SqlDbType.VarChar, 15).Value = address3
    '     .Add("@fcOfficePhone", SqlDbType.VarChar, 15).Value = homephone
    '     .Add("@fcMobilePhone", SqlDbType.VarChar, 15).Value = mobile
    '     .Add("@fxKeyEmployee", SqlDbType.VarChar, 50, ParameterDirection.Output).Value = ""
    ' End With
    ' myconnection.sqlconn.Open()
    ' cmd.ExecuteNonQuery()
    ' myconnection.sqlconn.Close()
    'End Sub
#End Region
#Region "Company Insert/Update/Delete"
    Public Sub SaveCompany(ByVal Orglevel As Integer, ByVal compcode As String, ByVal compdesc As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_company_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@Orglevel", SqlDbType.Char, 3).Value = Orglevel
            .Add("@CompanyCode", SqlDbType.Char, 10).Value = compcode
            .Add("@CompanyDescription", SqlDbType.VarChar, 50).Value = compdesc
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
    Public Sub UpdateCompany(ByVal Orglevel As Integer, ByVal compcode As String, ByVal compdesc As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_company_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@Orglevel", SqlDbType.Char, 3).Value = Orglevel
            .Add("@CompanyCode", SqlDbType.Char, 10).Value = compcode
            .Add("@CompanyDescription", SqlDbType.VarChar, 50).Value = compdesc
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Division Insert/Update/Delete"
    Public Sub SaveDivision(ByVal orglevel As Integer, ByVal compcode As String, ByVal divcode As String, ByVal descdiv As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_division_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@Orglevel", SqlDbType.Char, 3).Value = Orglevel
            .Add("@Companycode", SqlDbType.Char, 6).Value = compcode
            .Add("@DivCode", SqlDbType.Char, 10).Value = divcode
            .Add("@DivDescription", SqlDbType.VarChar, 50).Value = descdiv
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub

    Public Sub UpdateDivision(ByVal orglevel As Integer, ByVal compcode As String, ByVal divcode As String, ByVal descdiv As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_division_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@Orglevel", SqlDbType.Char, 3).Value = orglevel
            .Add("@Companycode", SqlDbType.Char, 6).Value = compcode
            .Add("@DivCode", SqlDbType.Char, 10).Value = divcode
            .Add("@DivDescription", SqlDbType.VarChar, 50).Value = descdiv
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#Region "Department insert/update/delete"
    Public Sub saveDepartment(ByVal orglevel As String, ByVal deptcode As String, ByVal descdept As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_department_table", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@Orglevel", SqlDbType.Char, 3).Value = Orglevel
            .Add("@DeptCode", SqlDbType.Char, 10).Value = deptcode
            .Add("@DeptDescription", SqlDbType.VarChar, 50).Value = descdept
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Section insert/update/delete"
    Public Sub saveSection(ByVal orglevel As String, ByVal deptcode As String, ByVal seccode As String, ByVal descsec As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_section_table", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@Orglevel", SqlDbType.Char, 3).Value = orglevel
            '.Add("@deptcode", SqlDbType.Char, 10).Value = deptcode
            .Add("@SecCode", SqlDbType.Char, 10).Value = seccode
            .Add("@SecDescription", SqlDbType.VarChar, 50).Value = descsec
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Division Insert into payroll"
    Public Sub savepayrolldivision(ByVal divcode As String, ByVal descdive As String, ByVal parentid As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_division_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            '.Add("@fxKeyDivision", SqlDbType.Char, 2).Value = ""
            .Add("@fcDivisionCode", SqlDbType.VarChar, 12).Value = divcode
            .Add("@fcDivisionName", SqlDbType.VarChar, 30).Value = descdive
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#End Region
    Public Sub OrgMaintenance(ByVal orgoption As Integer, ByVal orglevel As String, ByVal code As String, ByVal desc As String, ByVal parentID As String, ByVal orgID As String, ByVal fxkeyall As String, Optional ByRef errmsg As String = "")
        'errmsg = ""
        Dim myconnection As New Clsappconfiguration
        Dim sSQLCmd As String = "usp_orgmaintainance_revised"
        sSQLCmd &= " @option='" & orgoption & "' "
        sSQLCmd &= ",@Orglevel='" & orglevel & "' "
        sSQLCmd &= ",@Code='" & code & "' "
        sSQLCmd &= ",@Desc='" & desc & "' "
        sSQLCmd &= ",@ParentID='" & parentID & "' "
        sSQLCmd &= ",@OrgID='" & orgID & "' "
        sSQLCmd &= ",@fxkeyall='" & fxkeyall & "' "
        sSQLCmd &= ",@errormsg='" & errmsg & "' "
        Try
            SqlHelper.ExecuteScalar(myconnection.sqlconn, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            errmsg = ex.Message
        End Try

        '    Try
        '        Me.Clear_SQLParameters()
        '        Me.Add_SQLParameter("@option", SqlDbType.Int, orgoption)
        '        Me.Add_SQLParameter("@Orglevel", SqlDbType.VarChar, orglevel)
        '        Me.Add_SQLParameter("@Code", SqlDbType.VarChar, code)
        '        Me.Add_SQLParameter("@Desc", SqlDbType.VarChar, desc)
        '        Me.Add_SQLParameter("@ParentID", SqlDbType.Char, parentID)
        '        Me.Add_SQLParameter("@OrgID", SqlDbType.Char, orgID)
        '        Me.Add_SQLParameter("@fxkeyall", SqlDbType.Char, ParameterDirection.Output)
        '        Me.Add_SQLParameter("@errormsg", SqlDbType.VarChar, errmsg, ParameterDirection.Output, 1000)
        '        Me.ExecuteNonQuery("dbo.uspOrgMaintenance", CommandType.StoredProcedure, errmsg)
        '        Me.ExecuteNonQuery("dbo.usp_orgmaintainance_revised", CommandType.StoredProcedure, errmsg)
        '        Me.ExecuteNonQuery("usp_orgmaintainance_revised", CommandType.StoredProcedure, errmsg)
        '        If errmsg <> "" Then
        '            Throw New Exception(errmsg)
        '        Else
        '            errmsg = CStr(IIf(IsDBNull(Me.getParameterValue("@ErrorMsg")), "", Me.getParameterValue("@ErrorMsg")))

        '        End If
        '    Catch ex As Exception
        '        errmsg = ex.Message

        '    End Try
    End Sub


    Public Function GetOrgDetails(ByVal OrgID As String, Optional ByVal errmsg As String = "") As DataTable
        errmsg = ""
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@OrgID", SqlDbType.VarChar, orgID)
            Dim dt As DataTable = Me.DataQuery("dbo.uspGetOrgDetails", CommandType.StoredProcedure, errmsg)

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            Else
                Return dt
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try
    End Function

    Public Function GetOrgChild(ByVal orgid As String, Optional ByVal errmsg As String = "") As DataTable
        errmsg = ""
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@ParentID", SqlDbType.VarChar, orgid)
            Dim dt As DataTable = Me.DataQuery("dbo.uspGetOrgChild", CommandType.StoredProcedure, errmsg)
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            Else
                Return dt
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try
    End Function
#Region "Get Employee"
    Public Function getEmployee(ByVal orgid As String, Optional ByVal errmsg As String = "") As DataTable
        errmsg = ""
        Try
            Me.Clear_SQLParameters()
            Me.Add_SQLParameter("@Parentid", SqlDbType.VarChar, orgid)
            Dim dt As DataTable = Me.DataQuery("dbo.usp_getemployee", CommandType.StoredProcedure, errmsg)
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            Else
                Return dt
            End If
        Catch ex As Exception
            errmsg = ex.Message
        End Try
    End Function
#End Region
    Public Shared Function getuserrights(ByVal username As String, ByVal moduletype As String) As DataTable

        username = gUserName
        moduletype = "Master File"

        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmd As New SqlCommand("usp_SYS_GetUserRights", myconnection.sqlconn)

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Clear()
        cmd.Parameters.Add("username", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = username
        cmd.Parameters.Add("moduletype", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = moduletype

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()

        Try

            Dim myadapter As New SqlDataAdapter
            Dim DT As New DataTable
            Dim sqlDa As New SqlDataAdapter(cmd)
            sqlDa.SelectCommand = New SqlCommand("usp_SYS_GetUserRights", myconnection.sqlconn)

            myadapter.SelectCommand = cmd
            myadapter.Fill(DT)

            DT.TableName = "dbo.tSysUser_Rights"
            Return DT

        Finally

            myconnection.sqlconn.Close()

        End Try

    End Function
#Region "variables"
    Public server1 As String
    Public database1 As String
#End Region
#Region "Get property"
    Public Property getservername() As String
        Get
            Return Server
        End Get
        Set(ByVal value As String)
            Server = value
        End Set
    End Property

    Public Property getdatabasename() As String
        Get
            Return Database
        End Get
        Set(ByVal value As String)
            Database = value
        End Set
    End Property
#End Region
#Region "Insert Department"
    Public Sub insertdeptpayroll(ByVal parentid As String, ByVal code As String, ByVal desc As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_orgchart_division_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                '.Add("@Orgid", SqlDbType.Char, 2).Value = orgid
                .Add("@fcDivisionCode", SqlDbType.VarChar, 12).Value = parentid
                .Add("@fcDeptCode", SqlDbType.VarChar, 12).Value = code
                .Add("@fcDeptName", SqlDbType.VarChar, 60).Value = desc
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check usp_orgchart_division_insert", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Insert SECTION"
    Public Sub insertsectionpayroll(ByVal parentid As String, ByVal code As String, ByVal desc As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_orgchart_section_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@fcdeptCode", SqlDbType.VarChar, 12).Value = parentid
                .Add("@fcSectionCode", SqlDbType.VarChar, 12).Value = code
                .Add("@fcSectionName", SqlDbType.VarChar, 60).Value = desc
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check your section code", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Department"
    Public Sub updatepayrolldepartment(ByVal orgid As String, ByVal code As String, ByVal desc As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_payrollDepartment_update", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@OrgID", SqlDbType.VarChar, 10).Value = orgid
                .Add("@fcDeptCode", SqlDbType.VarChar, 12).Value = code
                .Add("@fcDeptName", SqlDbType.VarChar, 60).Value = desc
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check usp_per_payrollDepartment_update", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Section"
    Public Sub updatepayrollsection(ByVal orgid As String, ByVal code As String, ByVal desc As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_orgchartsection_update", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@OrgID", SqlDbType.VarChar, 10).Value = orgid
                .Add("@fcSectionCode", SqlDbType.VarChar, 12).Value = code
                .Add("@fcSectionName", SqlDbType.VarChar, 60).Value = desc
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check usp_per_orgchartsection_update", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
End Class
