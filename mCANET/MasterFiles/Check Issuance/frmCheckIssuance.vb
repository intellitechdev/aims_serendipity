﻿Imports System.Data.SqlClient
Imports WPM_EDCRYPT
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCheckIssuance

#Region "Variables"
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim Task As String
    Dim companyID As String = gCompanyID()
    Dim PreceedingZero As Integer
    Dim CheckID As String
#End Region

    Private Sub frmCheckIssuance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call BankList()
        Call ChkPreceedStat()
        Call SelectedColumnVAR()
        Call CheckNumberList()
    End Sub

#Region "Events"
    '------------------------------Bank----------------------------'
    Public Sub BankList()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "bankfile_List",
                                          New SqlParameter("FK_CoId", gCompanyID))

            grdBankList.DataSource = ds.Tables(0)
            With grdBankList
                .Columns("acnt_code").HeaderText = "Code"
                .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("acnt_code").Width = 80
                .Columns("BankName").HeaderText = "Bank Name"
                .Columns("BankName").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("BankName").Width = 200
                .Columns("BankID").HeaderText = "Bank ID"
                .Columns("BankID").Visible = False
                .Columns("fbIsClosed").HeaderText = "Closed"
                .Columns("fbIsClosed").Width = 60
                .Columns("fbIsClosed").SortMode = DataGridViewColumnSortMode.NotSortable
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SelectedColumnVAR()
        Try
            gAccountName = grdBankList.SelectedRows(0).Cells(0).Value.ToString()
            Task = grdBankList.SelectedRows(0).Cells(2).Value.ToString()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub SearchID()
        'Dim rd As SqlDataReader
        'rd = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "bankfile_SelectID",
        '                           New SqlParameter("@acnt_name", gAccountName))
        'While rd.Read
        '    gAccountID = rd("acnt_id")
        'End While
    End Sub

    Private Sub DeleteBank()
        Try
            Dim mycon As New Clsappconfiguration
            SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "bankfile_Delete", _
                                       New SqlParameter("@PK_BankID", Task))
            Call BankList()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    '------------------------------Check----------------------------'
    Private Sub ChkPreceedStat()
        If chkPreceedingZero.Checked = False Then
            cboPreceedingZero.Enabled = False
            cboPreceedingZero.Text = ""
        Else
            cboPreceedingZero.Enabled = True
        End If
    End Sub

    Private Sub AddCheckNum(ByVal bankID As String, ByVal CheckNum As String)
        Dim bID As New Guid(Task)
        Dim mycon As New Clsappconfiguration
        SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "Check_Insert", _
                                   New SqlParameter("@FX_CheckNumber", CheckNum),
                                   New SqlParameter("@FK_BankId", bID),
                                   New SqlParameter("@Company", gCompanyID))
    End Sub

    Private Sub DeleteCheck()
        Try
            Dim mycon As New Clsappconfiguration
            SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "Check_Delete", _
                                       New SqlParameter("@FK_BankId", Task),
                                       New SqlParameter("@FX_CheckNumber", CheckID),
                                       New SqlParameter("@Company", gCompanyID))
            Call CheckNumberList()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub CheckNumberList()
        Try
            Dim IDbank As New Guid(grdBankList.SelectedRows(0).Cells(2).Value.ToString())


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "CheckNumber_List",
                                       New SqlParameter("FK_BankID", IDbank),
                                       New SqlParameter("@Company", gCompanyID))
            dgvCheckList.DataSource = ds.Tables(0)
            With dgvCheckList
                .Columns("CheckNumber").HeaderText = "Check Number"
                .Columns("CheckNumber").Width = 150
                .Columns("CheckNumber").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("TransID").Width = 100
                .Columns("TransID").HeaderText = "Reference"
                .Columns("TransID").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("CheckDate").HeaderText = "Date"
                .Columns("CheckDate").Width = 80
                .Columns("CheckDate").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("CheckDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Payee").Width = 200
                .Columns("Payee").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Amount").Width = 90
                .Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Amount").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Amount").DefaultCellStyle.Format = "N2"
                .Columns("Used").Width = 50
                .Columns("Used").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Used").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Cancelled").Width = 70
                .Columns("Cancelled").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Cancelled").SortMode = DataGridViewColumnSortMode.NotSortable
            End With
        Catch ex As Exception

        End Try
        SelectedColumnCheck()
    End Sub

    Private Sub SelectedColumnCheck()
        Try
            CheckID = dgvCheckList.SelectedRows(0).Cells(0).Value.ToString()
        Catch ex As Exception

        End Try

        'Task = dgvCheckList.SelectedRows(0).Cells(1).Value.ToString()
    End Sub

    Private Sub CheckEmptyFields()
        If txtCheckNum.Text <> "" And txtQuantity.Text = "" Then
            MsgBox("Please Set Check Quantity.", MsgBoxStyle.Exclamation, "Check Issuance")
            txtQuantity.Focus()
        ElseIf txtCheckNum.Text = "" And txtQuantity.Text <> "" Then
            MsgBox("Nothing to add.", MsgBoxStyle.Exclamation, "Check Issuance")
            txtCheckNum.Focus()
        ElseIf cboPreceedingZero.Text = "" Then
            PreceedingZero = "1"
        End If
    End Sub

#End Region

#Region "Objects"
    '------------------------------Bank----------------------------'
    Private Sub grdBankList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdBankList.Click
        'gAccountName = grdBankList.SelectedRows(0).Cells(0).Value.ToString()
        'Task = grdBankList.SelectedRows(0).Cells(1).Value.ToString()
        'SearchID()

        Call SelectedColumnVAR()
        Call CheckNumberList()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        frmAddBank.ShowDialog()
    End Sub

    Private Sub btnDeleteBank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteBank.Click
        Call DeleteBank()
    End Sub

    '------------------------------Check----------------------------'
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        CheckEmptyFields()

        Dim CheckNumber As String
        'Counters
        Dim CheckCounter As String
        Dim NumCounter As Integer


        CheckCounter = txtCheckNum.Text
        NumCounter = Val(txtQuantity.Text)
        PreceedingZero = Val(cboPreceedingZero.Text)

        Try
            While NumCounter <> 0
                CheckNumber = CheckCounter.ToString.PadLeft(PreceedingZero, "0"c)

                'Check if Duplicate Document Number
                Dim mycon As New Clsappconfiguration
                Dim rd As SqlDataReader
                rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "Check_Search", _
                                           New SqlParameter("@FX_CheckNumber", CheckNumber),
                                           New SqlParameter("@FK_BankId", Task),
                                           New SqlParameter("@Company", gCompanyID))
                If rd.Read = True Then
                    MsgBox("Duplicate Check Number")
                    CheckNumberList()
                    Exit While
                End If

                'Add Document Number
                Call AddCheckNum(Task, CheckNumber)
                CheckCounter = CheckCounter + 1
                NumCounter = NumCounter - 1

                'Display after successful adding of document number
                If NumCounter = 0 Then
                    MsgBox("Add Successful")
                    CheckNumberList()
                End If

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Call DeleteCheck()
    End Sub

    Private Sub chkPreceedingZero_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPreceedingZero.CheckedChanged
        Call ChkPreceedStat()
    End Sub

    Private Sub dgvCheckList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCheckList.Click
        Call SelectedColumnCheck()
    End Sub

    Private Sub txtCheckNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCheckNum.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

#End Region


    Private Sub txtSearchCheck_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchCheck.TextChanged
        Try
            Dim IDbank As New Guid(grdBankList.SelectedRows(0).Cells(2).Value.ToString())

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "CheckNumber_Filter",
                                       New SqlParameter("@FK_BankID", IDbank),
                                       New SqlParameter("@FX_CheckNumber", txtSearchCheck.Text),
                                       New SqlParameter("@Company", gCompanyID))

            dgvCheckList.DataSource = ds.Tables(0)

            With dgvCheckList
                .Columns("CheckNumber").HeaderText = "Check Number"
                .Columns("CheckNumber").Width = 150
                .Columns("CheckNumber").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("TransID").Width = 100
                .Columns("TransID").HeaderText = "Reference"
                .Columns("TransID").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("CheckDate").HeaderText = "Date"
                .Columns("CheckDate").Width = 80
                .Columns("CheckDate").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("CheckDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Payee").Width = 200
                .Columns("Payee").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Amount").Width = 90
                .Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Amount").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Used").Width = 50
                .Columns("Used").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Used").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Cancelled").Width = 70
                .Columns("Cancelled").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Cancelled").SortMode = DataGridViewColumnSortMode.NotSortable
            End With
        Catch ex As Exception

        End Try
        SelectedColumnCheck()
    End Sub


    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        frmCancelledChecksReport.Show()
    End Sub
End Class