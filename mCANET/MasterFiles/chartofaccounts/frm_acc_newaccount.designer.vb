<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_newaccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboAcntType = New System.Windows.Forms.ComboBox
        Me.grpActTypeDesc = New System.Windows.Forms.GroupBox
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnnew_continue = New System.Windows.Forms.Button
        Me.btnnew_cancel = New System.Windows.Forms.Button
        Me.grpActTypeDesc.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(217, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Choose one account type and click continue"
        '
        'cboAcntType
        '
        Me.cboAcntType.FormattingEnabled = True
        Me.cboAcntType.Location = New System.Drawing.Point(79, 38)
        Me.cboAcntType.Name = "cboAcntType"
        Me.cboAcntType.Size = New System.Drawing.Size(172, 21)
        Me.cboAcntType.Sorted = True
        Me.cboAcntType.TabIndex = 17
        '
        'grpActTypeDesc
        '
        Me.grpActTypeDesc.Controls.Add(Me.txtDescription)
        Me.grpActTypeDesc.Location = New System.Drawing.Point(5, 76)
        Me.grpActTypeDesc.Name = "grpActTypeDesc"
        Me.grpActTypeDesc.Size = New System.Drawing.Size(252, 109)
        Me.grpActTypeDesc.TabIndex = 18
        Me.grpActTypeDesc.TabStop = False
        Me.grpActTypeDesc.Text = "Account Type Description"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(6, 20)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(240, 81)
        Me.txtDescription.TabIndex = 0
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(4, 191)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(110, 23)
        Me.btnAdd.TabIndex = 19
        Me.btnAdd.Text = "Add Account Type"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnnew_continue
        '
        Me.btnnew_continue.Location = New System.Drawing.Point(111, 224)
        Me.btnnew_continue.Name = "btnnew_continue"
        Me.btnnew_continue.Size = New System.Drawing.Size(74, 23)
        Me.btnnew_continue.TabIndex = 1
        Me.btnnew_continue.Text = "Continue"
        Me.btnnew_continue.UseVisualStyleBackColor = True
        '
        'btnnew_cancel
        '
        Me.btnnew_cancel.Location = New System.Drawing.Point(188, 224)
        Me.btnnew_cancel.Name = "btnnew_cancel"
        Me.btnnew_cancel.Size = New System.Drawing.Size(75, 23)
        Me.btnnew_cancel.TabIndex = 2
        Me.btnnew_cancel.Text = "Cancel"
        Me.btnnew_cancel.UseVisualStyleBackColor = True
        '
        'frm_acc_newaccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(269, 252)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.grpActTypeDesc)
        Me.Controls.Add(Me.cboAcntType)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnnew_cancel)
        Me.Controls.Add(Me.btnnew_continue)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frm_acc_newaccount"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add New Account: Choose Account Type"
        Me.grpActTypeDesc.ResumeLayout(False)
        Me.grpActTypeDesc.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboAcntType As System.Windows.Forms.ComboBox
    Friend WithEvents grpActTypeDesc As System.Windows.Forms.GroupBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnnew_continue As System.Windows.Forms.Button
    Friend WithEvents btnnew_cancel As System.Windows.Forms.Button
End Class
