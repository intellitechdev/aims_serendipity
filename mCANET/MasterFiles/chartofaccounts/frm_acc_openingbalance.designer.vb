<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_openingbalance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpBank = New System.Windows.Forms.GroupBox
        Me.btnbOK = New System.Windows.Forms.Button
        Me.btnbCancel = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtendBal = New System.Windows.Forms.TextBox
        Me.dtEndDate = New System.Windows.Forms.DateTimePicker
        Me.grpAssets = New System.Windows.Forms.GroupBox
        Me.btnOka = New System.Windows.Forms.Button
        Me.btnCancela = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtOpeningBal = New System.Windows.Forms.TextBox
        Me.dtasOfdate = New System.Windows.Forms.DateTimePicker
        Me.grpBank.SuspendLayout()
        Me.grpAssets.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpBank
        '
        Me.grpBank.Controls.Add(Me.btnbOK)
        Me.grpBank.Controls.Add(Me.btnbCancel)
        Me.grpBank.Controls.Add(Me.Label3)
        Me.grpBank.Controls.Add(Me.Label2)
        Me.grpBank.Controls.Add(Me.txtendBal)
        Me.grpBank.Controls.Add(Me.dtEndDate)
        Me.grpBank.Location = New System.Drawing.Point(217, 3)
        Me.grpBank.Name = "grpBank"
        Me.grpBank.Size = New System.Drawing.Size(191, 110)
        Me.grpBank.TabIndex = 1
        Me.grpBank.TabStop = False
        Me.grpBank.Text = "Enter the ending date and balance"
        '
        'btnbOK
        '
        Me.btnbOK.Location = New System.Drawing.Point(156, 117)
        Me.btnbOK.Name = "btnbOK"
        Me.btnbOK.Size = New System.Drawing.Size(84, 23)
        Me.btnbOK.TabIndex = 6
        Me.btnbOK.Text = "OK"
        Me.btnbOK.UseVisualStyleBackColor = True
        '
        'btnbCancel
        '
        Me.btnbCancel.Location = New System.Drawing.Point(246, 117)
        Me.btnbCancel.Name = "btnbCancel"
        Me.btnbCancel.Size = New System.Drawing.Size(84, 23)
        Me.btnbCancel.TabIndex = 5
        Me.btnbCancel.Text = "Cancel"
        Me.btnbCancel.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(118, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Statement Ending Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(132, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Statement Ending Balance"
        '
        'txtendBal
        '
        Me.txtendBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtendBal.Location = New System.Drawing.Point(155, 30)
        Me.txtendBal.Name = "txtendBal"
        Me.txtendBal.Size = New System.Drawing.Size(174, 21)
        Me.txtendBal.TabIndex = 0
        Me.txtendBal.Text = "0.00"
        Me.txtendBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtEndDate
        '
        Me.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtEndDate.Location = New System.Drawing.Point(155, 66)
        Me.dtEndDate.Name = "dtEndDate"
        Me.dtEndDate.Size = New System.Drawing.Size(106, 21)
        Me.dtEndDate.TabIndex = 1
        '
        'grpAssets
        '
        Me.grpAssets.Controls.Add(Me.btnOka)
        Me.grpAssets.Controls.Add(Me.btnCancela)
        Me.grpAssets.Controls.Add(Me.Label1)
        Me.grpAssets.Controls.Add(Me.Label4)
        Me.grpAssets.Controls.Add(Me.txtOpeningBal)
        Me.grpAssets.Controls.Add(Me.dtasOfdate)
        Me.grpAssets.Location = New System.Drawing.Point(12, 3)
        Me.grpAssets.Name = "grpAssets"
        Me.grpAssets.Size = New System.Drawing.Size(198, 110)
        Me.grpAssets.TabIndex = 2
        Me.grpAssets.TabStop = False
        Me.grpAssets.Text = "Enter the Value of :"
        '
        'btnOka
        '
        Me.btnOka.Location = New System.Drawing.Point(156, 117)
        Me.btnOka.Name = "btnOka"
        Me.btnOka.Size = New System.Drawing.Size(84, 23)
        Me.btnOka.TabIndex = 6
        Me.btnOka.Text = "OK"
        Me.btnOka.UseVisualStyleBackColor = True
        '
        'btnCancela
        '
        Me.btnCancela.Location = New System.Drawing.Point(246, 117)
        Me.btnCancela.Name = "btnCancela"
        Me.btnCancela.Size = New System.Drawing.Size(84, 23)
        Me.btnCancela.TabIndex = 5
        Me.btnCancela.Text = "Cancel"
        Me.btnCancela.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "As of"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Opening Balance"
        '
        'txtOpeningBal
        '
        Me.txtOpeningBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpeningBal.Location = New System.Drawing.Point(144, 34)
        Me.txtOpeningBal.Name = "txtOpeningBal"
        Me.txtOpeningBal.Size = New System.Drawing.Size(174, 21)
        Me.txtOpeningBal.TabIndex = 0
        Me.txtOpeningBal.Text = "0.00"
        Me.txtOpeningBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtasOfdate
        '
        Me.dtasOfdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtasOfdate.Location = New System.Drawing.Point(144, 68)
        Me.dtasOfdate.Name = "dtasOfdate"
        Me.dtasOfdate.Size = New System.Drawing.Size(106, 21)
        Me.dtasOfdate.TabIndex = 1
        '
        'frm_acc_openingbalance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(438, 160)
        Me.Controls.Add(Me.grpAssets)
        Me.Controls.Add(Me.grpBank)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_acc_openingbalance"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enter Opening Balance"
        Me.grpBank.ResumeLayout(False)
        Me.grpBank.PerformLayout()
        Me.grpAssets.ResumeLayout(False)
        Me.grpAssets.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpBank As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtendBal As System.Windows.Forms.TextBox
    Friend WithEvents dtEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnbOK As System.Windows.Forms.Button
    Friend WithEvents btnbCancel As System.Windows.Forms.Button
    Friend WithEvents grpAssets As System.Windows.Forms.GroupBox
    Friend WithEvents btnOka As System.Windows.Forms.Button
    Friend WithEvents btnCancela As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtOpeningBal As System.Windows.Forms.TextBox
    Friend WithEvents dtasOfdate As System.Windows.Forms.DateTimePicker
End Class
