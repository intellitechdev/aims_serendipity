<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgBeginningBudget
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.grpBeginningBalance = New System.Windows.Forms.GroupBox
        Me.lblBeginningBudget = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.dteBeginningDate = New System.Windows.Forms.DateTimePicker
        Me.txtBeginningBudget = New System.Windows.Forms.TextBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.grpBeginningBalance.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(136, 110)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'grpBeginningBalance
        '
        Me.grpBeginningBalance.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpBeginningBalance.Controls.Add(Me.lblBeginningBudget)
        Me.grpBeginningBalance.Controls.Add(Me.lblDate)
        Me.grpBeginningBalance.Controls.Add(Me.dteBeginningDate)
        Me.grpBeginningBalance.Controls.Add(Me.txtBeginningBudget)
        Me.grpBeginningBalance.Location = New System.Drawing.Point(12, 12)
        Me.grpBeginningBalance.Name = "grpBeginningBalance"
        Me.grpBeginningBalance.Size = New System.Drawing.Size(270, 89)
        Me.grpBeginningBalance.TabIndex = 1
        Me.grpBeginningBalance.TabStop = False
        Me.grpBeginningBalance.Text = "Account's Beginning Balance"
        '
        'lblBeginningBudget
        '
        Me.lblBeginningBudget.AutoSize = True
        Me.lblBeginningBudget.Location = New System.Drawing.Point(6, 55)
        Me.lblBeginningBudget.Name = "lblBeginningBudget"
        Me.lblBeginningBudget.Size = New System.Drawing.Size(91, 13)
        Me.lblBeginningBudget.TabIndex = 3
        Me.lblBeginningBudget.Text = "Beginning Budget"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Location = New System.Drawing.Point(6, 29)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(80, 13)
        Me.lblDate.TabIndex = 2
        Me.lblDate.Text = "Beginning Date"
        '
        'dteBeginningDate
        '
        Me.dteBeginningDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteBeginningDate.CustomFormat = "MMM - yyyy"
        Me.dteBeginningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteBeginningDate.Location = New System.Drawing.Point(116, 29)
        Me.dteBeginningDate.Name = "dteBeginningDate"
        Me.dteBeginningDate.Size = New System.Drawing.Size(145, 20)
        Me.dteBeginningDate.TabIndex = 1
        '
        'txtBeginningBudget
        '
        Me.txtBeginningBudget.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBeginningBudget.Location = New System.Drawing.Point(116, 55)
        Me.txtBeginningBudget.Name = "txtBeginningBudget"
        Me.txtBeginningBudget.Size = New System.Drawing.Size(145, 20)
        Me.txtBeginningBudget.TabIndex = 0
        Me.txtBeginningBudget.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dlgBeginningBudget
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(294, 151)
        Me.Controls.Add(Me.grpBeginningBalance)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgBeginningBudget"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enter Beginning Budget"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.grpBeginningBalance.ResumeLayout(False)
        Me.grpBeginningBalance.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents grpBeginningBalance As System.Windows.Forms.GroupBox
    Friend WithEvents dteBeginningDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtBeginningBudget As System.Windows.Forms.TextBox
    Friend WithEvents lblBeginningBudget As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label

End Class
