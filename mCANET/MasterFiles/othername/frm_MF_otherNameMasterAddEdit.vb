Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_otherNameMasterAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeyOtherName As String
    Private sKeyAddress As String
    Private sKeyBillRate As String

    Public Property KeyOtherName() As String
        Get
            Return Me.sKeyOtherName
        End Get
        Set(ByVal value As String)
            sKeyOtherName = value
        End Set
    End Property

    Public Property KeyAddress() As String
        Get
            Return sKeyAddress
        End Get
        Set(ByVal value As String)
            sKeyAddress = value
        End Set
    End Property

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        updateOtherName()
    End Sub

    Private Sub updateOtherName()
        Dim sSQLCmd As String = "SPU_OtherName_Update "
        sSQLCmd &= "@fxKeyOtherName ='" & sKeyOtherName & "'"
        sSQLCmd &= ",@fcOtherName ='" & txtName.Text & "'"
        sSQLCmd &= ",@fcCompanyName ='" & txtCoName.Text & "'"
        sSQLCmd &= ",@fcSalutation ='" & txtSalutation.Text & "'"
        sSQLCmd &= ",@fcLastName ='" & txtLastName.Text & "'"
        sSQLCmd &= ",@fcFirstName ='" & txtFirstName.Text & "'"
        sSQLCmd &= ",@fcMidName ='" & txtMiddleInitial.Text & "'"
        sSQLCmd &= ",@fcAccountNo ='" & txtAcctNo.Text & "'"
        sSQLCmd &= ",@fcContactPerson1 ='" & txtContact.Text & "'"
        sSQLCmd &= ",@fcContactPerson2 ='" & txtContact2.Text & "'"
        sSQLCmd &= ",@fcTelNo1 ='" & txtPhone.Text & "'"
        sSQLCmd &= ",@fcTelNo2 ='" & txtPhone2.Text & "'"
        sSQLCmd &= ",@fcFaxNo ='" & txtFAX.Text & "'"
        sSQLCmd &= ",@fcEmail ='" & txtEmail.Text & "'"
        sSQLCmd &= ",@fcNote ='" & txtNotes.Text & "'"
        sSQLCmd &= ",@fbActive =" & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@fcAddress ='" & txtAddress.Text & "'"
        sSQLCmd &= ",@fxKeyAddress ='" & sKeyAddress & "'"
        If cboBillingRateLvlID.SelectedIndex <> 0 And cboBillingRateLvlID.SelectedIndex <> 1 Then
            sSQLCmd &= ",@fxKeyBillRate ='" & cboBillingRateLvlID.SelectedItem & "'"
        End If
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated", "Update Other Name")
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Other Name")
        End Try
    End Sub

    Private Sub frm_MF_otherNameMasterAddEdit_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        m_OtherNameList(frm_acc_otherNameMaster.chkIncludeInactive.CheckState, frm_acc_otherNameMaster.grdOtherName)
        m_chkSettings(frm_acc_otherNameMaster.chkIncludeInactive, frm_acc_otherNameMaster.SQLQuery)
        If frm_MF_salesRepAddEdit.KeySalesRep <> "" Then
            frm_MF_salesRepAddEdit.refreshForm()
        End If
    End Sub

    Private Sub frm_MF_otherNameMasterAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'm_loadBillingRate(cboBillingRateLvlName, cboBillingRateLvlID)
        If (sKeyOtherName = "" And sKeyAddress = "") Or sKeyOtherName = Nothing Then
            sKeyOtherName = Guid.NewGuid.ToString
            sKeyAddress = Guid.NewGuid.ToString
        End If
        loadOtherName()
    End Sub

    Public Sub loadOtherName()
        Dim sSQLCmd As String = "SPU_OtherName_InformationList @fxKeyOtherName = '" & sKeyOtherName & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtName.Text = rd.Item("fcOtherName")
                    txtCoName.Text = rd.Item("fcCompanyName")
                    txtSalutation.Text = rd.Item("fcSalutation")
                    txtLastName.Text = rd.Item("fcLastName")
                    txtFirstName.Text = rd.Item("fcFirstName")
                    txtMiddleInitial.Text = rd.Item("fcMidName")
                    txtAddress.Text = rd.Item("fcAddress")
                    txtAcctNo.Text = rd.Item("fcAccountNo")
                    txtContact.Text = rd.Item("fcContactPerson1")
                    txtContact2.Text = rd.Item("fcContactPerson2")
                    txtPhone.Text = rd.Item("fcTelNo1")
                    txtPhone2.Text = rd.Item("fcTelNo2")
                    txtFAX.Text = rd.Item("fcFaxNo")
                    txtEmail.Text = rd.Item("fcEmail")
                    txtNotes.Text = rd.Item("fcNote")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                    cboBillingRateLvlName.SelectedItem = rd.Item("fxKeyBillRate")
                End If
            End Using
            selectedBillRate()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Other Name")
        End Try
    End Sub

    Private Sub selectedBillRate()
        If Not sKeyBillRate = Nothing Or Not sKeyBillRate = "" Then
            cboBillingRateLvlID.SelectedText = sKeyBillRate
            cboBillingRateLvlID.SelectedItem = sKeyBillRate
            cboBillingRateLvlName.SelectedIndex = cboBillingRateLvlID.SelectedIndex
        Else
            cboBillingRateLvlName.SelectedIndex = 0
            cboBillingRateLvlID.SelectedIndex = 0
        End If
    End Sub
    
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub cboBillingRateLvlName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBillingRateLvlName.SelectedIndexChanged
        cboBillingRateLvlID.SelectedIndex = cboBillingRateLvlName.SelectedIndex

        If cboBillingRateLvlName.SelectedIndex = 1 Then
            frm_MF_billRateLevelAdd.KeyBillRate = Guid.NewGuid.ToString
            frm_MF_billRateLevelAdd.Show()
        End If
    End Sub

    Private Sub btnAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddress.Click
        frm_MF_otherNameMasterAddress.KeyAddress = sKeyAddress
        frm_MF_otherNameMasterAddress.KeyOtherName = sKeyOtherName
        frm_MF_otherNameMasterAddress.Show()
    End Sub
End Class