<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_otherNameMaster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_otherNameMaster))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_New = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Edit = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_MkInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_showInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.CustomizeColumnsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.UseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FindInTransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NotepadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.PrintListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_writeCheck = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_creditCard = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_changeType = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_genReport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_phoneList = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_ContactList = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.ReportsOnAllOtherNamesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_chkDetail = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_missingChk = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton
        Me.chkIncludeInactive = New System.Windows.Forms.CheckBox
        Me.grdOtherName = New System.Windows.Forms.DataGridView
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdOtherName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripSplitButton1, Me.ToolStripButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(361, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_New, Me.ts_Edit, Me.ts_Delete, Me.ToolStripSeparator1, Me.ts_MkInactive, Me.ts_showInactive, Me.CustomizeColumnsToolStripMenuItem, Me.ToolStripSeparator2, Me.UseToolStripMenuItem, Me.FindInTransactionToolStripMenuItem, Me.NotepadToolStripMenuItem, Me.ToolStripSeparator3, Me.PrintListToolStripMenuItem})
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(86, 22)
        Me.ToolStripButton1.Text = "Other &Names"
        '
        'ts_New
        '
        Me.ts_New.Name = "ts_New"
        Me.ts_New.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ts_New.Size = New System.Drawing.Size(180, 22)
        Me.ts_New.Text = "New"
        '
        'ts_Edit
        '
        Me.ts_Edit.Name = "ts_Edit"
        Me.ts_Edit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.ts_Edit.Size = New System.Drawing.Size(180, 22)
        Me.ts_Edit.Text = "Edit"
        '
        'ts_Delete
        '
        Me.ts_Delete.Name = "ts_Delete"
        Me.ts_Delete.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ts_Delete.Size = New System.Drawing.Size(180, 22)
        Me.ts_Delete.Text = "Delete"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(177, 6)
        '
        'ts_MkInactive
        '
        Me.ts_MkInactive.Name = "ts_MkInactive"
        Me.ts_MkInactive.Size = New System.Drawing.Size(180, 22)
        Me.ts_MkInactive.Text = "Make Inactive"
        '
        'ts_showInactive
        '
        Me.ts_showInactive.Name = "ts_showInactive"
        Me.ts_showInactive.Size = New System.Drawing.Size(180, 22)
        Me.ts_showInactive.Text = "Show Inactive"
        '
        'CustomizeColumnsToolStripMenuItem
        '
        Me.CustomizeColumnsToolStripMenuItem.Name = "CustomizeColumnsToolStripMenuItem"
        Me.CustomizeColumnsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.CustomizeColumnsToolStripMenuItem.Text = "Customize Columns"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(177, 6)
        '
        'UseToolStripMenuItem
        '
        Me.UseToolStripMenuItem.Name = "UseToolStripMenuItem"
        Me.UseToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.UseToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.UseToolStripMenuItem.Text = "Use"
        '
        'FindInTransactionToolStripMenuItem
        '
        Me.FindInTransactionToolStripMenuItem.Name = "FindInTransactionToolStripMenuItem"
        Me.FindInTransactionToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.FindInTransactionToolStripMenuItem.Text = "Find in Transactions"
        '
        'NotepadToolStripMenuItem
        '
        Me.NotepadToolStripMenuItem.Name = "NotepadToolStripMenuItem"
        Me.NotepadToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.NotepadToolStripMenuItem.Text = "Notepad"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(177, 6)
        '
        'PrintListToolStripMenuItem
        '
        Me.PrintListToolStripMenuItem.Name = "PrintListToolStripMenuItem"
        Me.PrintListToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintListToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PrintListToolStripMenuItem.Text = "Print List"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_writeCheck, Me.ts_creditCard, Me.ts_changeType})
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(66, 22)
        Me.ToolStripButton2.Text = "Ac&tivities"
        '
        'ts_writeCheck
        '
        Me.ts_writeCheck.Name = "ts_writeCheck"
        Me.ts_writeCheck.Size = New System.Drawing.Size(212, 22)
        Me.ts_writeCheck.Text = "Write Checks"
        '
        'ts_creditCard
        '
        Me.ts_creditCard.Name = "ts_creditCard"
        Me.ts_creditCard.Size = New System.Drawing.Size(212, 22)
        Me.ts_creditCard.Text = "Enter Credit Card Charges"
        '
        'ts_changeType
        '
        Me.ts_changeType.Name = "ts_changeType"
        Me.ts_changeType.Size = New System.Drawing.Size(212, 22)
        Me.ts_changeType.Text = "Change Other Name Type"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_genReport, Me.ToolStripSeparator4, Me.ts_phoneList, Me.ts_ContactList, Me.ToolStripSeparator5, Me.ReportsOnAllOtherNamesToolStripMenuItem})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(61, 22)
        Me.ToolStripSplitButton1.Text = "Re&ports"
        '
        'ts_genReport
        '
        Me.ts_genReport.Name = "ts_genReport"
        Me.ts_genReport.Size = New System.Drawing.Size(218, 22)
        Me.ts_genReport.Text = "General Report"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(215, 6)
        '
        'ts_phoneList
        '
        Me.ts_phoneList.Name = "ts_phoneList"
        Me.ts_phoneList.Size = New System.Drawing.Size(218, 22)
        Me.ts_phoneList.Text = "Phone List"
        '
        'ts_ContactList
        '
        Me.ts_ContactList.Name = "ts_ContactList"
        Me.ts_ContactList.Size = New System.Drawing.Size(218, 22)
        Me.ts_ContactList.Text = "Contact List"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(215, 6)
        '
        'ReportsOnAllOtherNamesToolStripMenuItem
        '
        Me.ReportsOnAllOtherNamesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_chkDetail, Me.ts_missingChk})
        Me.ReportsOnAllOtherNamesToolStripMenuItem.Name = "ReportsOnAllOtherNamesToolStripMenuItem"
        Me.ReportsOnAllOtherNamesToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.ReportsOnAllOtherNamesToolStripMenuItem.Text = "Reports on All Other Names"
        '
        'ts_chkDetail
        '
        Me.ts_chkDetail.Name = "ts_chkDetail"
        Me.ts_chkDetail.Size = New System.Drawing.Size(156, 22)
        Me.ts_chkDetail.Text = "Check Detail"
        '
        'ts_missingChk
        '
        Me.ts_missingChk.Name = "ts_missingChk"
        Me.ts_missingChk.Size = New System.Drawing.Size(156, 22)
        Me.ts_missingChk.Text = "Missing Checks"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "ToolStripButton3"
        '
        'chkIncludeInactive
        '
        Me.chkIncludeInactive.AutoSize = True
        Me.chkIncludeInactive.Location = New System.Drawing.Point(12, 28)
        Me.chkIncludeInactive.Name = "chkIncludeInactive"
        Me.chkIncludeInactive.Size = New System.Drawing.Size(101, 17)
        Me.chkIncludeInactive.TabIndex = 5
        Me.chkIncludeInactive.Text = "Include in&active"
        Me.chkIncludeInactive.UseVisualStyleBackColor = True
        '
        'grdOtherName
        '
        Me.grdOtherName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdOtherName.Location = New System.Drawing.Point(0, 48)
        Me.grdOtherName.Name = "grdOtherName"
        Me.grdOtherName.Size = New System.Drawing.Size(361, 150)
        Me.grdOtherName.TabIndex = 4
        '
        'frm_acc_otherNameMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(361, 198)
        Me.Controls.Add(Me.chkIncludeInactive)
        Me.Controls.Add(Me.grdOtherName)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_acc_otherNameMaster"
        Me.Text = "Other Names"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdOtherName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_New As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Edit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_MkInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_showInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomizeColumnsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents UseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FindInTransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotepadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrintListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_writeCheck As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_creditCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_changeType As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_genReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_phoneList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_ContactList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ReportsOnAllOtherNamesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_chkDetail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_missingChk As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkIncludeInactive As System.Windows.Forms.CheckBox
    Friend WithEvents grdOtherName As System.Windows.Forms.DataGridView
End Class
