Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports MTGCComboBox

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: July 07, 2009                      ###
'                   ### Website: www.ionflux.site50.net                  ###
'                   ########################################################

Public Class frmClasses
#Region "Declaration"
    Private gcon As New Clsappconfiguration
    Dim LevelDept As Integer = 0
    Dim detParent As String
    Dim detLevel As String
    Private ClassDesciptionForToolTip As String
    Private ClassParentForToolTip As String
    Private ClassNameForToolTip As String
#End Region
#Region "Function and sub"
    Private Function GetClasses() As DataSet
        Dim sSqlGetClassesCmd As String = "SELECT * FROM dbo.mClasses WHERE fxKeyCompany ='" & _
                                        gCompanyID() & "' ORDER BY classLevel"
        Try
            Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlGetClassesCmd)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function   
    Private Sub ClassLoadToGrid()
        With DGVClasses
            Dim DGVClasses As DataTable = GetClasses().Tables(0)
            .DataSource = DGVClasses.DefaultView
        End With
        'checkClassExistingStatus()
    End Sub
    Private Sub getLevelDept()
        DGVClasses.SelectAll()
        Dim xRow As Integer = 0
        For Each selectedRow As DataGridViewRow In DGVClasses.SelectedRows
            xRow = selectedRow.Index
            Dim RowLeveldept As Integer = DGVClasses.Item(3, xRow).Value
            'MsgBox(RowLeveldept)
            If RowLeveldept > LevelDept Then
                LevelDept = RowLeveldept
            End If
        Next
        DGVClasses.Refresh()
    End Sub
    Private Function GetNode(ByVal text As String, ByVal parentCollection As TreeNodeCollection) As TreeNode
        Dim ret As TreeNode
        Dim child As TreeNode
        For Each child In parentCollection 'step through the parentcollection
            If child.Text = text Then
                ret = child
            ElseIf child.GetNodeCount(False) > 0 Then ' if there is child items then call this function recursively
                ret = GetNode(text, child.Nodes)
            End If
            If Not ret Is Nothing Then Exit For 'if something was found, exit out of the for loop
        Next
        Return ret
    End Function
    Private Sub ClassFromGridToTree()
        Dim diggerDept As Integer = 1
        Dim xRow As Integer = 0
        TreeViewClass.Nodes.Clear()
        Do While diggerDept <= LevelDept
            If diggerDept = 1 Then
                'establishing first level
                DGVClasses.SelectAll()
                For Each selectedRow As DataGridViewRow In DGVClasses.SelectedRows
                    xRow = selectedRow.Index
                    If diggerDept = DGVClasses.Item(3, xRow).Value Then
                        TreeViewClass.Nodes.Add(DGVClasses.Item(2, xRow).Value)
                        TreeViewClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TreeViewClass.Nodes)
                        TreeViewClass.SelectedNode.Tag = DGVClasses.Item(0, xRow).Value
                    End If
                Next
                xRow = 0
                DGVClasses.Refresh()
                '#############################
            Else
                DGVClasses.SelectAll()
                For Each SelectedRow As DataGridViewRow In DGVClasses.SelectedRows
                    xRow = SelectedRow.Index
                    'MsgBox(DGVClasses.Item(2, xRow).Value)
                    If diggerDept = DGVClasses.Item(3, xRow).Value Then
                        TreeViewClass.SelectedNode = GetNode(DGVClasses.Item(4, xRow).Value, TreeViewClass.Nodes)
                        If TreeViewClass.SelectedNode Is Nothing Then
                            DeleteClass(DGVClasses.Item(0, xRow).Value)
                        Else
                            TreeViewClass.SelectedNode.Nodes.Add(DGVClasses.Item(2, xRow).Value)
                            TreeViewClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TreeViewClass.Nodes)
                            TreeViewClass.SelectedNode.Tag = DGVClasses.Item(0, xRow).Value
                            'MsgBox(DGVClasses.Item(0, xRow).Value & vbCr & DGVClasses.Item(2, xRow).Value)
                        End If
                    End If
                Next
                DGVClasses.Refresh()
            End If
            diggerDept += 1
        Loop
       
        EvaluateNodeName()
    End Sub
    Private Sub checkClassExistingStatus()
        Dim xRow As Integer = 0
        DGVClasses.Refresh()
        DGVClasses.SelectAll()
        For Each ExistingRows As DataGridViewRow In DGVClasses.SelectedRows
            xRow = ExistingRows.Index
            Dim sSqlChckExistingStatus As String = "SELECT * FROM dbo.mClasses WHERE classname = '" & _
                                        DGVClasses.Item(4, xRow).Value & "' AND fxKeyCompany ='" & gCompanyID() & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlChckExistingStatus)
                    If rd.Item(0).ToString Is Nothing Then
                        DeleteClass(DGVClasses.Item(0, xRow).Value)
                        DGVClasses.Refresh()
                        DGVClasses.SelectAll()
                    End If
                End Using
            Catch ex As Exception

            End Try
        Next
        DGVClasses.Refresh()
    End Sub
    Private Sub EvaluateNodeName()
        DGVClasses.SelectAll()
        Dim xRow As Integer = 0
        Dim UncleanName As String
        Dim ClassName() As String
        For Each selectedRow As DataGridViewRow In DGVClasses.SelectedRows
            xRow = selectedRow.Index
            TreeViewClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TreeViewClass.Nodes)
            If TreeViewClass.SelectedNode Is Nothing Then
                DeleteClass(DGVClasses.Item(0, xRow).Value)
            Else
                UncleanName = TreeViewClass.SelectedNode.Text
                ClassName = Split(UncleanName, "���")
                TreeViewClass.SelectedNode.Text = ClassName(0)
            End If
        Next
        TreeViewClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TreeViewClass.Nodes)
        DGVClasses.Refresh()
        TreeViewClass.CollapseAll()
    End Sub
    Private Sub DeleteClass(ByVal Refno As String)
        Dim SqlDeleteClass As String = "DELETE FROM dbo.mClasses where ClassRefnumber ='" & Refno & _
                                        "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, SqlDeleteClass)
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "frmClasses-delete class")
        End Try
    End Sub
    Private Function CheckIfHasChild(ByVal ClassName As String, ByVal Refno As String) As Boolean
        Dim ClassWholeName As String = ClassName & "���" & Refno
        Dim HasChild As String
        Dim sSqlCmd As String = "SELECT ClassRefnumber FROM dbo.mClasses WHERE ClassParentPath ='" & _
                                    ClassWholeName & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    HasChild = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
        If HasChild = "" Or HasChild Is Nothing Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub GetClassDetail(ByVal Refno As String)
        Dim classLevel As String
        Dim ScrapClassParent As String
        Dim className As String
        Dim sSqlCmd As String = "SELECT classLevel, ClassParentPath FROM dbo.mClasses WHERE ClassRefnumber='" & _
                            Refno & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                classLevel = rd.Item(0).ToString
                ScrapClassParent = rd.Item(1).ToString
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getClassDetailsForToolTip(ByVal ClassReferenceNumber As String)
        Dim dbParent As String
        Dim xParent(0) As String
        Dim dbName As String
        Dim xName(0) As String
        Dim sSqlGetDetail As String = _
                    "SELECT ClassDescription, ClassParentPath, classname FROM dbo.mClasses WHERE ClassRefnumber = '" & _
                    ClassReferenceNumber & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlGetDetail)
                While rd.Read
                    ClassDesciptionForToolTip = rd.Item(0).ToString
                    dbParent = rd.Item(1).ToString
                    If dbParent Is Nothing Or dbParent = "" Or dbParent = " " Then
                        ClassParentForToolTip = "N/A"
                    Else
                        xParent = Split(dbParent, "���")
                        ClassParentForToolTip = xParent(0)
                    End If
                    dbName = rd.Item(2).ToString
                    xName = Split(dbName, "���")
                    ClassNameForToolTip = xName(0)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Private Sub frmClasses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ClassLoadToGrid()
        getLevelDept()
        ClassFromGridToTree()
    End Sub
    Private Sub AddClassToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddClassToolStripMenuItem.Click
        FrmNewClass.Text = "New Class"
        FrmNewClass.ShowDialog()

        ClassLoadToGrid()
        getLevelDept()
        ClassFromGridToTree()
    End Sub
    Private Sub DeleteClaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteClaseToolStripMenuItem.Click
        If MsgBox("Are you want to delete class " & TreeViewClass.SelectedNode.Text & "?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Warning:") = MsgBoxResult.Yes Then
            DeleteClass(TreeViewClass.SelectedNode.Tag)
            GetClasses()
            ClassLoadToGrid()
            getLevelDept()
            ClassFromGridToTree()
        End If
    End Sub
    Private Sub EditClassToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditClassToolStripMenuItem.Click
        On Error GoTo EndSub
        If CheckIfHasChild(TreeViewClass.SelectedNode.Text, TreeViewClass.SelectedNode.Tag) = False Then
            MsgBox("To avoid data loses, you can't edit the selected class because there are existing subclasses on this class.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        Else
            'frmEditClasses.xRefno = TreeViewClass.SelectedNode.Tag
            frmEditClasses.SelectedClass = TreeViewClass.SelectedNode.Tag
            frmEditClasses.ShowDialog()
            ClassLoadToGrid()
            getLevelDept()
            ClassFromGridToTree()
        End If
EndSub:
    End Sub
    Private Sub TreeViewClass_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewClass.AfterSelect
        DeleteClaseToolStripMenuItem.Enabled = True
        EditClassToolStripMenuItem.Enabled = True
        DeleteToolStripMenuItem.Enabled = True
        EditClassToolStripMenuItem1.Enabled = True

        On Error GoTo subTerminated
        getClassDetailsForToolTip(TreeViewClass.SelectedNode.Tag)
        If TreeViewClass.SelectedNode.Tag <> "" Then
            TreeViewClass.SelectedNode.ToolTipText = "Class Name:  " & ClassNameForToolTip & _
                        vbCr & "Reference #:  " & TreeViewClass.SelectedNode.Tag & vbCr & _
                        "Subclass of:   " & ClassParentForToolTip & vbCr & _
                        "Description:   " & ClassDesciptionForToolTip
        End If
subTerminated:
    End Sub
    Private Sub TreeViewClass_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeViewClass.NodeMouseClick
        Dim selected As String = ""
        Dim i As Integer = 0
        selected = TreeViewClass.GetNodeAt(New Point(e.X, e.Y)).Text
        TreeViewClass.SelectedNode = GetNode(selected, TreeViewClass.Nodes)
    End Sub
    Private Sub TreeViewClass_NodeMouseHover(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseHoverEventArgs) Handles TreeViewClass.NodeMouseHover
        On Error GoTo subTerminated
        getClassDetailsForToolTip(TreeViewClass.SelectedNode.Tag)
        If TreeViewClass.SelectedNode.Tag <> "" Then
            TreeViewClass.SelectedNode.ToolTipText = "Class Name:  " & ClassNameForToolTip & _
                        vbCr & "Reference #:  " & TreeViewClass.SelectedNode.Tag & vbCr & _
                        "Subclass of:   " & ClassParentForToolTip & vbCr & _
                        "Description:   " & ClassDesciptionForToolTip
        End If
subTerminated:
    End Sub
    Private Sub AddClassToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddClassToolStripMenuItem1.Click
        FrmNewClass.Text = "New Class"
        FrmNewClass.TxtClassRefno.Text = TreeViewClass.SelectedNode.Tag
        FrmNewClass.txtParentName.Text = TreeViewClass.SelectedNode.Text
        FrmNewClass.ShowDialog()
        ClassLoadToGrid()
        getLevelDept()
        ClassFromGridToTree()
    End Sub
    Private Sub EditClassToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditClassToolStripMenuItem1.Click
        On Error GoTo EndSub
        If CheckIfHasChild(TreeViewClass.SelectedNode.Text, TreeViewClass.SelectedNode.Tag) = False Then
            MsgBox("To avoid data loses, you can't edit the selected class because there are existing subclasses on this class.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        Else
            frmEditClasses.SelectedClass = TreeViewClass.SelectedNode.Tag
            frmEditClasses.ShowDialog()
            ClassLoadToGrid()
            getLevelDept()
            ClassFromGridToTree()
        End If
EndSub:
    End Sub
    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        If MsgBox("Are you want to delete class " & TreeViewClass.SelectedNode.Text & "?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Warning:") = MsgBoxResult.Yes Then
            DeleteClass(TreeViewClass.SelectedNode.Tag)
            GetClasses()
            ClassLoadToGrid()
            getLevelDept()
            ClassFromGridToTree()
        End If
    End Sub
    Private Sub RefreshClassesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshClassesToolStripMenuItem.Click
        ClassLoadToGrid()
        getLevelDept()
        ClassFromGridToTree()
    End Sub

    Private Sub ContextMenuStrip1_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening
        On Error GoTo subTerminator
        AddClassToolStripMenuItem1.Enabled = False
        EditClassToolStripMenuItem1.Enabled = False
        DeleteToolStripMenuItem.Enabled = False

        If TreeViewClass.SelectedNode.Text Is Nothing Then
            AddClassToolStripMenuItem1.Enabled = False
            EditClassToolStripMenuItem1.Enabled = False
            DeleteToolStripMenuItem.Enabled = False
        Else
            AddClassToolStripMenuItem1.Enabled = True
            EditClassToolStripMenuItem1.Enabled = True
            DeleteToolStripMenuItem.Enabled = True
        End If
subTerminator:
    End Sub
End Class