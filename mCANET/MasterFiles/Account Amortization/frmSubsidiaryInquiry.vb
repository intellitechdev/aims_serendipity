﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports System.Threading, System.IO
Imports System.Data.OleDb

Public Class frmSubsidiaryInquiry
    Private gCon As New Clsappconfiguration()
    Dim memKey As String

#Region "Logic"

#End Region

#Region "Data Access Region"

    Private Sub LoadLedgers()
        grdSubsidiaryList.Rows.Clear()
        Dim memGUID As New Guid(memKey)
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiary_ListPerClientID",
                                                     New SqlParameter("@FkMember", memGUID))

        While rd.Read = True
            AddItem_Column(rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, rd.Item(4))
        End While
    End Sub

    Private Sub AddItem_Column(ByVal SubsidiaryHeaderID As String, ByVal FcDocnum As String, ByVal FcAccountName As String, ByVal FdDate As Date, ByVal FnAmount As Decimal)
        Try
            Dim row As String() =
             {SubsidiaryHeaderID, FcDocnum, FcAccountName, FdDate, FnAmount}

            Dim nRowIndex As Integer
            With grdSubsidiaryList
                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

#End Region

#Region "Event Handler"

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        frmMembersFilter.ShowDialog()
        If frmMembersFilter.DialogResult = Windows.Forms.DialogResult.OK Then
            lblClientname.Text = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
            lblIDno.Text = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
            memKey = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
            LoadLedgers()
        End If
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmSubsidiaryInquiry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
#End Region


End Class