﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountAmortizationSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.grdColumn = New System.Windows.Forms.DataGridView()
        Me.FbSelected = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnClearParameters = New System.Windows.Forms.Button()
        Me.btnCreateAmortization = New System.Windows.Forms.Button()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtFilter = New System.Windows.Forms.TextBox()
        Me.grdClientList = New System.Windows.Forms.DataGridView()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.txtNoOfPayment = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboModeOfPayment = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboTerms = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboAccount = New System.Windows.Forms.ComboBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnSearchAcntRef = New System.Windows.Forms.Button()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.txtRefNo = New System.Windows.Forms.TextBox()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.btnGenerateTemplate = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.grdAmortization = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSaveUploader = New System.Windows.Forms.Button()
        Me.btnBatchUploader = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnUploaderTemplate = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdUploader = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboAccountList = New System.Windows.Forms.ComboBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PkColumnID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reference = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeductionType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Principal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateStart = New CSAcctg.GridDateControl()
        Me.PayNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmortizationDate = New CSAcctg.GridDateControl()
        Me.FnTerms = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnAmortization = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fbSSS = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdColumn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdClientList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdAmortization, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdUploader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.InitialDirectory = "C:\"
        Me.SaveFileDialog1.Title = "Save Template"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.InitialDirectory = "C:\"
        Me.OpenFileDialog1.Title = "Open File"
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TabPage1.Controls.Add(Me.grdColumn)
        Me.TabPage1.Controls.Add(Me.btnClearParameters)
        Me.TabPage1.Controls.Add(Me.btnCreateAmortization)
        Me.TabPage1.Controls.Add(Me.txtAmount)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.txtFilter)
        Me.TabPage1.Controls.Add(Me.grdClientList)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.dtpDate)
        Me.TabPage1.Controls.Add(Me.txtNoOfPayment)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.cboModeOfPayment)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.cboTerms)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.cboAccount)
        Me.TabPage1.Controls.Add(Me.btnSave)
        Me.TabPage1.Controls.Add(Me.btnSearchAcntRef)
        Me.TabPage1.Controls.Add(Me.btnUpload)
        Me.TabPage1.Controls.Add(Me.txtRefNo)
        Me.TabPage1.Controls.Add(Me.txtPath)
        Me.TabPage1.Controls.Add(Me.btnGenerateTemplate)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.grdAmortization)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1135, 447)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Account Amortization Setup"
        '
        'grdColumn
        '
        Me.grdColumn.AllowUserToAddRows = False
        Me.grdColumn.AllowUserToDeleteRows = False
        Me.grdColumn.AllowUserToResizeColumns = False
        Me.grdColumn.AllowUserToResizeRows = False
        Me.grdColumn.BackgroundColor = System.Drawing.Color.White
        Me.grdColumn.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdColumn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdColumn.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FbSelected, Me.PkColumnID, Me.ColumnName})
        Me.grdColumn.Location = New System.Drawing.Point(614, 53)
        Me.grdColumn.Name = "grdColumn"
        Me.grdColumn.RowHeadersVisible = False
        Me.grdColumn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdColumn.Size = New System.Drawing.Size(274, 148)
        Me.grdColumn.TabIndex = 105
        '
        'FbSelected
        '
        Me.FbSelected.HeaderText = ""
        Me.FbSelected.Name = "FbSelected"
        Me.FbSelected.Width = 40
        '
        'btnClearParameters
        '
        Me.btnClearParameters.Location = New System.Drawing.Point(695, 206)
        Me.btnClearParameters.Name = "btnClearParameters"
        Me.btnClearParameters.Size = New System.Drawing.Size(193, 21)
        Me.btnClearParameters.TabIndex = 104
        Me.btnClearParameters.Text = "Clear Parameters"
        Me.btnClearParameters.UseVisualStyleBackColor = True
        '
        'btnCreateAmortization
        '
        Me.btnCreateAmortization.Location = New System.Drawing.Point(489, 206)
        Me.btnCreateAmortization.Name = "btnCreateAmortization"
        Me.btnCreateAmortization.Size = New System.Drawing.Size(193, 21)
        Me.btnCreateAmortization.TabIndex = 103
        Me.btnCreateAmortization.Text = "Create Amortization Schedule"
        Me.btnCreateAmortization.UseVisualStyleBackColor = True
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(415, 102)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(153, 20)
        Me.txtAmount.TabIndex = 101
        Me.txtAmount.Text = "0.00"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(314, 105)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(43, 13)
        Me.Label13.TabIndex = 102
        Me.Label13.Text = "Amount"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(5, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 13)
        Me.Label12.TabIndex = 100
        Me.Label12.Text = "Search"
        '
        'txtFilter
        '
        Me.txtFilter.Location = New System.Drawing.Point(52, 24)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(243, 20)
        Me.txtFilter.TabIndex = 99
        '
        'grdClientList
        '
        Me.grdClientList.AllowUserToAddRows = False
        Me.grdClientList.AllowUserToDeleteRows = False
        Me.grdClientList.AllowUserToResizeColumns = False
        Me.grdClientList.AllowUserToResizeRows = False
        Me.grdClientList.BackgroundColor = System.Drawing.Color.White
        Me.grdClientList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdClientList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdClientList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdClientList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdClientList.Location = New System.Drawing.Point(7, 50)
        Me.grdClientList.Name = "grdClientList"
        Me.grdClientList.RowHeadersVisible = False
        Me.grdClientList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdClientList.Size = New System.Drawing.Size(288, 380)
        Me.grdClientList.TabIndex = 98
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(314, 80)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 97
        Me.Label11.Text = "Date"
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(415, 76)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(153, 20)
        Me.dtpDate.TabIndex = 96
        '
        'txtNoOfPayment
        '
        Me.txtNoOfPayment.Location = New System.Drawing.Point(415, 180)
        Me.txtNoOfPayment.Name = "txtNoOfPayment"
        Me.txtNoOfPayment.ReadOnly = True
        Me.txtNoOfPayment.Size = New System.Drawing.Size(153, 20)
        Me.txtNoOfPayment.TabIndex = 94
        Me.txtNoOfPayment.Text = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(314, 183)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 13)
        Me.Label10.TabIndex = 95
        Me.Label10.Text = "No. of Payment"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(314, 157)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 13)
        Me.Label9.TabIndex = 93
        Me.Label9.Text = "Mode of Payment"
        '
        'cboModeOfPayment
        '
        Me.cboModeOfPayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModeOfPayment.FormattingEnabled = True
        Me.cboModeOfPayment.Items.AddRange(New Object() {"DAILY", "WEEKLY", "SEMI-MONTHLY", "MONTHLY", "QUARTERLY", "ANNUALLY", "LUMP SUM"})
        Me.cboModeOfPayment.Location = New System.Drawing.Point(415, 154)
        Me.cboModeOfPayment.Name = "cboModeOfPayment"
        Me.cboModeOfPayment.Size = New System.Drawing.Size(153, 21)
        Me.cboModeOfPayment.TabIndex = 92
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(314, 131)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(36, 13)
        Me.Label8.TabIndex = 91
        Me.Label8.Text = "Terms"
        '
        'cboTerms
        '
        Me.cboTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTerms.FormattingEnabled = True
        Me.cboTerms.Location = New System.Drawing.Point(415, 128)
        Me.cboTerms.Name = "cboTerms"
        Me.cboTerms.Size = New System.Drawing.Size(153, 21)
        Me.cboTerms.TabIndex = 90
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(314, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 89
        Me.Label7.Text = "Account"
        '
        'cboAccount
        '
        Me.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccount.FormattingEnabled = True
        Me.cboAccount.Location = New System.Drawing.Point(415, 24)
        Me.cboAccount.Name = "cboAccount"
        Me.cboAccount.Size = New System.Drawing.Size(474, 21)
        Me.cboAccount.TabIndex = 88
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(794, 406)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(94, 21)
        Me.btnSave.TabIndex = 87
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnSearchAcntRef
        '
        Me.btnSearchAcntRef.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchAcntRef.Location = New System.Drawing.Point(573, 47)
        Me.btnSearchAcntRef.Name = "btnSearchAcntRef"
        Me.btnSearchAcntRef.Size = New System.Drawing.Size(23, 25)
        Me.btnSearchAcntRef.TabIndex = 10
        Me.btnSearchAcntRef.UseVisualStyleBackColor = True
        '
        'btnUpload
        '
        Me.btnUpload.Location = New System.Drawing.Point(671, 406)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(117, 21)
        Me.btnUpload.TabIndex = 86
        Me.btnUpload.Text = "Upload Template"
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'txtRefNo
        '
        Me.txtRefNo.Location = New System.Drawing.Point(415, 50)
        Me.txtRefNo.Name = "txtRefNo"
        Me.txtRefNo.Size = New System.Drawing.Size(153, 20)
        Me.txtRefNo.TabIndex = 8
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(345, 407)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(199, 20)
        Me.txtPath.TabIndex = 14
        '
        'btnGenerateTemplate
        '
        Me.btnGenerateTemplate.Location = New System.Drawing.Point(549, 406)
        Me.btnGenerateTemplate.Name = "btnGenerateTemplate"
        Me.btnGenerateTemplate.Size = New System.Drawing.Size(117, 21)
        Me.btnGenerateTemplate.TabIndex = 85
        Me.btnGenerateTemplate.Text = "Generate Template"
        Me.btnGenerateTemplate.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(314, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Doc. Number"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(309, 409)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Path"
        '
        'grdAmortization
        '
        Me.grdAmortization.AllowUserToAddRows = False
        Me.grdAmortization.AllowUserToDeleteRows = False
        Me.grdAmortization.BackgroundColor = System.Drawing.Color.White
        Me.grdAmortization.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdAmortization.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdAmortization.Location = New System.Drawing.Point(309, 233)
        Me.grdAmortization.Name = "grdAmortization"
        Me.grdAmortization.ReadOnly = True
        Me.grdAmortization.Size = New System.Drawing.Size(579, 164)
        Me.grdAmortization.TabIndex = 12
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1143, 473)
        Me.TabControl1.TabIndex = 88
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TabPage2.Controls.Add(Me.btnClose)
        Me.TabPage2.Controls.Add(Me.btnSaveUploader)
        Me.TabPage2.Controls.Add(Me.btnBatchUploader)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.btnUploaderTemplate)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.grdUploader)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.cboAccountList)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1135, 447)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Batch Uploader"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(1031, 409)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(94, 21)
        Me.btnClose.TabIndex = 96
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveUploader
        '
        Me.btnSaveUploader.Location = New System.Drawing.Point(932, 409)
        Me.btnSaveUploader.Name = "btnSaveUploader"
        Me.btnSaveUploader.Size = New System.Drawing.Size(94, 21)
        Me.btnSaveUploader.TabIndex = 92
        Me.btnSaveUploader.Text = "Save"
        Me.btnSaveUploader.UseVisualStyleBackColor = True
        '
        'btnBatchUploader
        '
        Me.btnBatchUploader.Location = New System.Drawing.Point(809, 409)
        Me.btnBatchUploader.Name = "btnBatchUploader"
        Me.btnBatchUploader.Size = New System.Drawing.Size(117, 21)
        Me.btnBatchUploader.TabIndex = 91
        Me.btnBatchUploader.Text = "Upload Template"
        Me.btnBatchUploader.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(42, 409)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(639, 20)
        Me.TextBox1.TabIndex = 88
        '
        'btnUploaderTemplate
        '
        Me.btnUploaderTemplate.Location = New System.Drawing.Point(687, 409)
        Me.btnUploaderTemplate.Name = "btnUploaderTemplate"
        Me.btnUploaderTemplate.Size = New System.Drawing.Size(117, 21)
        Me.btnUploaderTemplate.TabIndex = 90
        Me.btnUploaderTemplate.Text = "Generate Template"
        Me.btnUploaderTemplate.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 411)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Path"
        '
        'grdUploader
        '
        Me.grdUploader.AllowUserToAddRows = False
        Me.grdUploader.AllowUserToDeleteRows = False
        Me.grdUploader.AllowUserToResizeRows = False
        Me.grdUploader.BackgroundColor = System.Drawing.Color.White
        Me.grdUploader.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdUploader.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.grdUploader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdUploader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Reference, Me.idno, Me.memName, Me.DeductionType, Me.Principal, Me.DateStart, Me.PayNo, Me.AmortizationDate, Me.FnTerms, Me.FnAmortization, Me.fbSSS})
        Me.grdUploader.Location = New System.Drawing.Point(3, 49)
        Me.grdUploader.Name = "grdUploader"
        Me.grdUploader.RowHeadersVisible = False
        Me.grdUploader.Size = New System.Drawing.Size(1129, 340)
        Me.grdUploader.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(10, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 94
        Me.Label3.Text = "Account"
        '
        'cboAccountList
        '
        Me.cboAccountList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAccountList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccountList.FormattingEnabled = True
        Me.cboAccountList.Location = New System.Drawing.Point(63, 17)
        Me.cboAccountList.Name = "cboAccountList"
        Me.cboAccountList.Size = New System.Drawing.Size(498, 21)
        Me.cboAccountList.TabIndex = 93
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "ClientID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "ID No."
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Principal"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Date Start"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Amortization Date"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 120
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Pay No."
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 120
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Amortization Date"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 120
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Amortization Date"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 120
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Amortization Date"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 120
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Terms"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Width = 40
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Amortization"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'PkColumnID
        '
        Me.PkColumnID.HeaderText = "PkColumnID"
        Me.PkColumnID.Name = "PkColumnID"
        Me.PkColumnID.ReadOnly = True
        Me.PkColumnID.Visible = False
        '
        'ColumnName
        '
        Me.ColumnName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColumnName.HeaderText = "Column"
        Me.ColumnName.Name = "ColumnName"
        Me.ColumnName.ReadOnly = True
        Me.ColumnName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColumnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Reference
        '
        Me.Reference.HeaderText = "Reference"
        Me.Reference.Name = "Reference"
        Me.Reference.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'idno
        '
        Me.idno.HeaderText = "ID"
        Me.idno.Name = "idno"
        Me.idno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.idno.Width = 120
        '
        'memName
        '
        Me.memName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.memName.HeaderText = "Name"
        Me.memName.Name = "memName"
        Me.memName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DeductionType
        '
        Me.DeductionType.HeaderText = "Deduction Type"
        Me.DeductionType.Name = "DeductionType"
        Me.DeductionType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DeductionType.Width = 130
        '
        'Principal
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Principal.DefaultCellStyle = DataGridViewCellStyle3
        Me.Principal.HeaderText = "Principal"
        Me.Principal.Name = "Principal"
        Me.Principal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DateStart
        '
        Me.DateStart.HeaderText = "Date Granted"
        Me.DateStart.Name = "DateStart"
        Me.DateStart.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DateStart.Width = 120
        '
        'PayNo
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.PayNo.DefaultCellStyle = DataGridViewCellStyle4
        Me.PayNo.HeaderText = "Pay No."
        Me.PayNo.Name = "PayNo"
        Me.PayNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PayNo.Width = 53
        '
        'AmortizationDate
        '
        Me.AmortizationDate.HeaderText = "Amortization Date"
        Me.AmortizationDate.Name = "AmortizationDate"
        Me.AmortizationDate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AmortizationDate.Width = 120
        '
        'FnTerms
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.FnTerms.DefaultCellStyle = DataGridViewCellStyle5
        Me.FnTerms.HeaderText = "Terms"
        Me.FnTerms.Name = "FnTerms"
        Me.FnTerms.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FnTerms.Width = 40
        '
        'FnAmortization
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.FnAmortization.DefaultCellStyle = DataGridViewCellStyle6
        Me.FnAmortization.HeaderText = "Amortization"
        Me.FnAmortization.Name = "FnAmortization"
        Me.FnAmortization.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'fbSSS
        '
        Me.fbSSS.HeaderText = "SSS"
        Me.fbSSS.Name = "fbSSS"
        Me.fbSSS.Width = 30
        '
        'frmAccountAmortizationSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(1143, 473)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Name = "frmAccountAmortizationSetup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdColumn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdClientList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdAmortization, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.grdUploader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnSearchAcntRef As System.Windows.Forms.Button
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents txtRefNo As System.Windows.Forms.TextBox
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents btnGenerateTemplate As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents grdAmortization As System.Windows.Forms.DataGridView
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboModeOfPayment As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboTerms As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboAccount As System.Windows.Forms.ComboBox
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNoOfPayment As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents grdClientList As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnClearParameters As System.Windows.Forms.Button
    Friend WithEvents btnCreateAmortization As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdUploader As System.Windows.Forms.DataGridView
    Friend WithEvents btnSaveUploader As System.Windows.Forms.Button
    Friend WithEvents btnBatchUploader As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnUploaderTemplate As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboAccountList As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents grdColumn As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FbSelected As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents PkColumnID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Reference As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents memName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DeductionType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Principal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateStart As CSAcctg.GridDateControl
    Friend WithEvents PayNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AmortizationDate As CSAcctg.GridDateControl
    Friend WithEvents FnTerms As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnAmortization As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fbSSS As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
