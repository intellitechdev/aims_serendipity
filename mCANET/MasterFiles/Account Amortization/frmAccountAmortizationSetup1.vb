﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports System.Threading, System.IO
Imports System.Data.OleDb

Public Class frmAccountAmortizationSetup

    Private gCon As New Clsappconfiguration()
    Dim acntKey As String
    Dim empKey As String
    Dim PkID As Integer = 0

    Dim filenym As String
    Dim fpath As String
    Private UploaderType As String
    Private xFilePath As String = ""

    Dim Col As String
    Dim ColVal As String
    Dim ColorCount As Integer

    Dim dv As New DataView
    Dim ds As New DataSet

#Region "Logic"
    Private Sub ComputeNumberOfPayments()
        Try
            Select Case cboModeOfPayment.Text
                Case "DAILY"
                    txtNoOfPayment.Text = cboTerms.Text * 30
                Case "WEEKLY"
                    txtNoOfPayment.Text = cboTerms.Text * 4
                Case "SEMI-MONTHLY"
                    txtNoOfPayment.Text = cboTerms.Text * 2
                Case "MONTHLY"
                    txtNoOfPayment.Text = cboTerms.Text
                Case "QUARTERLY"
                    txtNoOfPayment.Text = cboTerms.Text / 4
                Case "ANNUALLY"
                    txtNoOfPayment.Text = cboTerms.Text / 12
                Case "LUMP SUM"
                    txtNoOfPayment.Text = 1
            End Select
        Catch ex As Exception
            MsgBox("Please Select Term.", MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function ErrMsg() As String
        Dim msg As String

        If cboAccount.Text = "Select" Then
            msg = "Please Select Account." + vbNewLine
        End If

        If txtRefNo.Text = "" Then
            msg += "Please Document Number." + vbNewLine
        End If

        If txtAmount.Text = "" Then
            msg += "Please Input Amount." + vbNewLine
        End If

        If cboTerms.Text = "" Then
            msg += "Please Select Terms." + vbNewLine
        End If

        If cboModeOfPayment.Text = "" Then
            msg += "Please Select Mode of Payment." + vbNewLine
        End If

        Return msg
    End Function

#End Region

#Region "Data Access Layer"

    Private Sub AddColumn(ByVal ColumnID As String, ByVal ColumnName As String)
        Try
            'Dim column As String() =
            ' {PkAccount, FkAccount, FcAcntCode, FcAcntTitle}

            'Dim nColumnIndex As Integer
            With grdAmortization

                .Columns.Add(ColumnID, ColumnName)
                '.ClearSelection()
                'nColumnIndex = .Columns.Count - 1
                '.FirstDisplayedScrollingRowIndex = nColumnIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SelectColumns()
        grdAmortization.Columns.Clear()
        With grdAmortization
            .Columns.Add("FnPayNo", "Pay No.")
            .Columns.Add("FdDate", "Date")
        End With
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_Temp_AccountAmortizationColumn_ListByAccountID",
                                                     New SqlParameter("@acntID", GetAccountKey))
        While rd.Read = True
            AddColumn(rd.Item(0).ToString, rd.Item(1).ToString)
        End While
    End Sub

    Private Sub ClearTemp()
        Dim sSQLCmdDeleteTemp As String = "delete from tbl_Temp_DeductionColumn where FkUserID = " & intSysCurrentId & ""
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmdDeleteTemp)
    End Sub

    Private Sub GenerateTemplate()
        Try
            Dim xlApp As Microsoft.Office.Interop.Excel.Application
            Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
            Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            Dim i As Integer
            Dim j As Integer

            xlApp = New Microsoft.Office.Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            With xlWorkSheet
                For Each column As DataGridViewColumn In grdAmortization.Columns
                    .Cells(1, column.Index + 1) = column.HeaderText
                Next
                For i = 0 To grdAmortization.RowCount - 1
                    For j = 0 To grdAmortization.ColumnCount - 1
                        For k As Integer = 1 To grdAmortization.Columns.Count
                            xlWorkSheet.Cells(1, k) = grdAmortization.Columns(k - 1).HeaderText
                            xlWorkSheet.Cells(i + 2, j + 1) = grdAmortization(j, i).Value
                        Next
                    Next
                Next
            End With

            xlWorkSheet.SaveAs(Path.GetFullPath(SaveFileDialog1.FileName))
            xlWorkBook.Close()
            xlApp.Quit()

            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            Dim res As MsgBoxResult
            res = MsgBox("Process completed, Would you like to open file?", MsgBoxStyle.YesNo)
            If (res = MsgBoxResult.Yes) Then
                Process.Start(Path.GetFullPath(SaveFileDialog1.FileName))
            End If
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub GenerateBatchUploaderTemplate()
        Try
            Dim xlApp As Microsoft.Office.Interop.Excel.Application
            Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
            Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            Dim i As Integer
            Dim j As Integer

            xlApp = New Microsoft.Office.Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            With xlWorkSheet
                For Each column As DataGridViewColumn In grdUploader.Columns
                    .Cells(1, column.Index + 1) = column.HeaderText
                Next
                For i = 0 To grdUploader.RowCount - 1
                    For j = 0 To grdUploader.ColumnCount - 1
                        For k As Integer = 1 To grdUploader.Columns.Count
                            xlWorkSheet.Cells(1, k) = grdUploader.Columns(k - 1).HeaderText
                            xlWorkSheet.Cells(i + 2, j + 1) = grdUploader(j, i).Value
                        Next
                    Next
                Next
            End With

            xlWorkSheet.SaveAs(Path.GetFullPath(SaveFileDialog1.FileName))
            xlWorkBook.Close()
            xlApp.Quit()

            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            Dim res As MsgBoxResult
            res = MsgBox("Process completed, Would you like to open file?", MsgBoxStyle.YesNo)
            If (res = MsgBoxResult.Yes) Then
                Process.Start(Path.GetFullPath(SaveFileDialog1.FileName))
            End If
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Import()
        grdAmortization.Rows.Clear()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        Dim xCol As Integer

        rd = dbCommand.ExecuteReader
        Try
            Dim xRow As Integer = 0
            While rd.Read = True
                grdAmortization.Rows.Add()
                xCol = rd.FieldCount
                'Dim row(xCol) As String
                For i As Integer = 0 To xCol - 1
                    grdAmortization.Item(i, xRow).Value = rd.Item(i).ToString
                Next
                xRow += 1
                'grdList.Rows.Add(row)
            End While
            rd.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ImportBatchUploading()
        grdUploader.Rows.Clear()
        ColorCount = 0
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim xRowIndex As Integer = 0
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + TextBox1.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader
        Try
            While rd.Read = True
                Dim row As String() = New String() {rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, Format(CDec(rd.Item(4).ToString), "##,##0.00"), rd.Item(5).ToString, rd.Item(6).ToString, rd.Item(7).ToString, rd.Item(8).ToString, Format(CDec(rd.Item(9).ToString), "##,##0.00"), CBool(rd.Item(10).ToString)}
                grdUploader.Rows.Add(row)
                If CheckClientID(rd.Item(1).ToString) = False Then
                    grdUploader.Rows(xRowIndex).DefaultCellStyle.BackColor = Color.Red
                    ColorCount += 1
                End If
                xRowIndex += 1
            End While
            rd.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function CheckClientID(ByVal idno As String)
        Dim str As Boolean
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryBatch_CheckClientID",
                                                         New SqlParameter("@fcEmployeeNo", idno))
        If rd.Read = True Then
            str = True
        Else
            str = False
        End If
        Return str
        rd.Close()
    End Function

    Private Function CheckRefNo(ByVal refno As String)
        Dim str As Boolean
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryBatch_CheckClientID",
                                                         New SqlParameter("@fcEmployeeNo", refno))
        If rd.Read = True Then
            str = True
        Else
            str = False
        End If
        Return str
        rd.Close()
    End Function

    Private Sub CreateAmortization()
        Dim myid As New Guid(cboAccount.SelectedValue.ToString)

        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_CreateAmortization",
                                                          New SqlParameter("@date", dtpDate.Text),
                                                          New SqlParameter("@amount", CDbl(txtAmount.Text)),
                                                          New SqlParameter("@noofpayment", CInt(txtNoOfPayment.Text)),
                                                          New SqlParameter("@paymenttype", cboModeOfPayment.Text),
                                                          New SqlParameter("@acntKey", myid))
        Dim xCol As Integer
        Dim payno As Integer = 1
        Dim columnid As Integer = 2
        Dim xCount As Integer


        Try
            Dim xRow As Integer = 0
            'If rd.Read = True Then
            '    payno = rd.Item(0)
            grdAmortization.Rows.Add()

            While rd.Read = True
                If payno <> rd.Item(0) Then
                    grdAmortization.Rows.Add()
                    xRow += 1
                    columnid = 2

                    grdAmortization.Item(0, xRow).Value = rd.Item(0)
                    grdAmortization.Item(1, xRow).Value = rd.Item(1)

                    grdAmortization.Item(columnid, xRow).Value = rd.Item(2)
                    columnid += 1
                Else
                    grdAmortization.Item(0, xRow).Value = rd.Item(0)
                    grdAmortization.Item(1, xRow).Value = rd.Item(1)

                    grdAmortization.Item(columnid, xRow).Value = rd.Item(2)
                    columnid += 1

                End If
                'xCol = rd.Item(3)
                'Dim row(xCol) As String
                'grdAmortization.Item(0, xRow).Value = rd.Item(0)
                'grdAmortization.Item(1, xRow).Value = rd.Item(1)
                'For i As Integer = 2 To xCol - 1
                '    grdAmortization.Item(i, xRow).Value = rd.Item(2).ToString
                'Next
                'xRow += 1
                'grdList.Rows.Add(row)
                payno = rd.Item(0)
            End While
            rd.Close()
            'End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub Save(ByVal subsidiaryID As Integer, ByVal FnPayNo As Integer, ByVal FdDate As Date, ByVal ColID As String, ByVal ColValue As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryDetails_Insert",
                                                         New SqlParameter("@FkSubsidiaryHeaderID", subsidiaryID),
                                                         New SqlParameter("@FnPayNo", FnPayNo),
                                                         New SqlParameter("@FdDate", FdDate),
                                                         New SqlParameter("@FcColumnID", ColID),
                                                         New SqlParameter("@FcAmount", ColValue))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub LoadDebitCredit()
    '    'KeyAccountID = Nothing
    '    Dim myid As New Guid(gCompanyID)
    '    Dim ds As New DataSet
    '    Dim ad As New SqlDataAdapter
    '    Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
    '    Try
    '        ad.SelectCommand = cmd
    '        ad.Fill(ds, "Accounts")
    '        With cboAccount
    '            .ValueMember = "FkAccount"
    '            .DisplayMember = "acntwithcode"
    '            .DataSource = ds.Tables(0)
    '            .SelectedIndex = -1
    '            .Text = "Select"
    '        End With
    '        gCon.sqlconn.Close()
    '    Catch ex As Exception

    '    End Try

    'End Sub

    Private Sub loadClientList()
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_MasterList")

        'grdSAList.DataSource = ds.Tables(0)
        dv.Table = ds.Tables(0)
    End Sub

    Private Sub LoadGridDetails()
        dv = New DataView(ds.Tables(0))
        dv.RowFilter = "Fullname LIKE '%" & txtFilter.Text & "%' OR fcEmployeeNo LIKE '%" & txtFilter.Text & "%'"
        grdClientList.DataSource = dv

        With grdClientList
            .Columns("pk_Employee").Visible = False
            .Columns("fcEmployeeNo").Width = 100
            .Columns("fcEmployeeNo").ReadOnly = True
            .Columns("fcEmployeeNo").HeaderText = "ID"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Fullname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("Fullname").ReadOnly = True
            .Columns("Fullname").HeaderText = "Name"
            .Columns("Fullname").SortMode = DataGridViewColumnSortMode.NotSortable
        End With
        dv.Sort = "Fullname"
    End Sub

    Private Sub LoadAccountsDropdown()
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccount
                .ValueMember = "FkAccount"
                .DisplayMember = "acntwithcode"
                .Tag = "PkAccount"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadAccountsDropdownTab2()
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccountList
                .ValueMember = "FkAccount"
                .DisplayMember = "acntwithcode"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Function GetAccountKey() As Guid
        Dim myid As New Guid(cboAccount.SelectedValue.ToString)
        Return myid
    End Function

    Private Sub LoadTerms()
        Try
            Dim ds As New DataSet
            Dim ad As New SqlDataAdapter
            Dim cmd As New SqlCommand("SELECT PkTermID, FnTerms FROM [dbo].[tbl_AccountAmortization_Terms] T LEFT JOIN [dbo].[tbl_AccountAmortization] A ON A.PkAccount = T.FkAccountID WHERE A.FkAccount = '" & cboAccount.SelectedValue.ToString & "' ORDER BY FnTerms", gCon.sqlconn)
            cmd.CommandType = CommandType.Text

            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboTerms
                .ValueMember = "PkTermID"
                .DisplayMember = "FnTerms"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveSubsidiaryDetails(ByVal headerID As Integer)
        For xRow As Integer = 0 To grdAmortization.RowCount - 1
            For i As Integer = 0 To grdAmortization.ColumnCount - 1
                Dim CellValue As String = grdAmortization.Item(i, xRow).Value.ToString()
                Dim CellHeader As String = grdAmortization.Columns(i).Name.ToString
                If i > 1 Then
                    If Col = "" Then
                        Col = Col + CellHeader
                    Else
                        Col = Col + ", " + CellHeader
                    End If
                End If

                If i > 1 Then
                    If ColVal = "" Then
                        ColVal = ColVal + CellValue
                    Else
                        ColVal = ColVal + ", " + CellValue
                    End If
                End If

            Next
            Save(headerID, grdAmortization.Item(0, xRow).Value, grdAmortization.Item(1, xRow).Value, Col, ColVal)
            Col = ""
            ColVal = ""
        Next
        MsgBox("Saved!", MsgBoxStyle.Information, "Account Amortization Setup")
    End Sub

    Private Sub SaveSubsidiaryHeader()
        Dim myid As New Guid(cboAccount.SelectedValue.ToString)
        Dim memID As New Guid(grdClientList.Item(0, grdClientList.CurrentRow.Index).Value.ToString)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryHeader_Insert",
                                                         New SqlParameter("@FkAmortization", myid),
                                                         New SqlParameter("@FcDocNumber", txtRefNo.Text),
                                                         New SqlParameter("@FnAmount", CDec(txtAmount.Text)),
                                                         New SqlParameter("@FdDate", dtpDate.Value),
                                                         New SqlParameter("@FkMember", memID))
            Call UpdateDocDate()
            Call SaveSubsidiaryDetails(GetNewHeaderID)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function GetNewHeaderID() As Integer
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryHeader_GetNewID")
        If rd.Read = True Then
            GetNewHeaderID = rd.Item(0)
            Return GetNewHeaderID
        Else
            GetNewHeaderID = 0
            Return GetNewHeaderID
        End If
    End Function

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtRefNo.Text),
                    New SqlParameter("@fcDoctype", "RECEIVABLES"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception
            MsgBox("Error on updating document number " & txtRefNo.Text & ". Please notify the system developer if you seen this message.")
        End Try
    End Sub

    Private Sub SaveBatchAmortization()
        If ColorCount > 0 Then
            MsgBox("Please fix or remove red lines to continue.", MsgBoxStyle.Exclamation, "Batch Upload")
            Exit Sub
        Else
            Dim myid As New Guid(cboAccountList.SelectedValue.ToString)
            For xRow As Integer = 0 To grdUploader.RowCount - 1
                If CBool(grdUploader.Item(10, xRow).Value) = False Then
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryBatch_Insert",
                                                                     New SqlParameter("@FcDocNumber", grdUploader.Item(0, xRow).Value.ToString),
                                                                     New SqlParameter("@FcEmployeeNo", grdUploader.Item(1, xRow).Value.ToString),
                                                                     New SqlParameter("@FkAmortization", myid),
                                                                     New SqlParameter("@FcDeductionType", grdUploader.Item(3, xRow).Value.ToString),
                                                                     New SqlParameter("@FnAmount", CDec(grdUploader.Item(4, xRow).Value)),
                                                                     New SqlParameter("@FdDateStart", grdUploader.Item(5, xRow).Value.ToString),
                                                                     New SqlParameter("@FnNoofPayment", CInt(grdUploader.Item(6, xRow).Value)),
                                                                     New SqlParameter("@FdAmortizationDate", grdUploader.Item(7, xRow).Value.ToString),
                                                                     New SqlParameter("@FnTerms", CInt(grdUploader.Item(8, xRow).Value.ToString)),
                                                                     New SqlParameter("@FnAmortization", CDec(grdUploader.Item(9, xRow).Value.ToString)),
                                                                     New SqlParameter("@FbSSS", CBool(grdUploader.Item(10, xRow).Value)))
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                Else
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiaryBatch_Insert_SSS",
                                                                     New SqlParameter("@FcDocNumber", grdUploader.Item(0, xRow).Value.ToString),
                                                                     New SqlParameter("@FcEmployeeNo", grdUploader.Item(1, xRow).Value.ToString),
                                                                     New SqlParameter("@FkAmortization", myid),
                                                                     New SqlParameter("@FcDeductionType", grdUploader.Item(3, xRow).Value.ToString),
                                                                     New SqlParameter("@FnAmount", CDec(grdUploader.Item(4, xRow).Value)),
                                                                     New SqlParameter("@FdDateStart", grdUploader.Item(5, xRow).Value.ToString),
                                                                     New SqlParameter("@FnNoofPayment", CInt(grdUploader.Item(6, xRow).Value)),
                                                                     New SqlParameter("@FdAmortizationDate", grdUploader.Item(7, xRow).Value.ToString),
                                                                     New SqlParameter("@FnTerms", CInt(grdUploader.Item(8, xRow).Value.ToString)),
                                                                     New SqlParameter("@FnAmortization", CDec(grdUploader.Item(9, xRow).Value.ToString)),
                                                                     New SqlParameter("@FbSSS", CBool(grdUploader.Item(10, xRow).Value)))
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                End If

            Next
            MsgBox("Saved!", MsgBoxStyle.Information, "Account Amortization Setup")
            grdUploader.Rows.Clear()
        End If
    End Sub

    Private Sub InsertAccountToTemp(ByVal chkStatus As Boolean, ByVal xRow As Integer)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_Temp_DeductionColumn_InsertDelete",
                                                                            New SqlParameter("@PkColumnID", GetValuesInDataGridView(grdColumn, "PkColumnID", xRow)),
                                                                            New SqlParameter("@FcColumnName", GetValuesInDataGridView(grdColumn, "ColumnName", xRow)),
                                                                            New SqlParameter("@FkUserID", intSysCurrentId),
                                                                            New SqlParameter("@FkAccountHeader", GetAccountKey),
                                                                            New SqlParameter("@xType", chkStatus))
    End Sub

    Private Sub LoadColumns()
        grdColumn.Rows.Clear()
        Dim myid As New Guid(cboAccount.SelectedValue.ToString)
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_ListByAccountID",
                                                     New SqlParameter("@acntID", myid))
        While rd.Read = True
            AddItem_Column(rd.Item(0).ToString, rd.Item(1).ToString)
        End While
    End Sub

#End Region

#Region "Event Handler"

    Private Sub btnSearchAcntRef_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchAcntRef.Click
        frmJVno.xDoctype = "RECEIVABLES"
        frmJVno.ShowDialog()
        If frmJVno.DialogResult = DialogResult.OK Then
            txtRefNo.Text = frmJVno.listDocNumber.SelectedItems.Item(0).Text
        End If
    End Sub

    Private Sub btnGenerateTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateTemplate.Click
        SaveFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        SaveFileDialog1.ShowDialog()
        If DialogResult.OK Then
            GenerateTemplate()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnUpload_Click(sender As System.Object, e As System.EventArgs) Handles btnUpload.Click
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath
            xFilePath = txtPath.Text
            UploaderType = IO.Path.GetExtension(xFilePath)
            If DialogResult.OK Then
                Import()
            Else
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Call SaveSubsidiaryHeader()
    End Sub

    Private Sub frmAccountAmortizationSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        loadClientList()
        LoadGridDetails()
        LoadAccountsDropdown()
        LoadTerms()
        LoadAccountsDropdownTab2()
        ClearTemp()
    End Sub

    Private Sub txtFilter_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtFilter.TextChanged
        LoadGridDetails()
    End Sub

    Private Sub btnCreateAmortization_Click(sender As System.Object, e As System.EventArgs) Handles btnCreateAmortization.Click
        If ErrMsg() = "" Then
            SelectColumns()
            CreateAmortization()
        Else
            MsgBox(ErrMsg, MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub cboAccount_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboAccount.DropDownClosed
        LoadTerms()
        LoadColumns()
        ClearTemp()
    End Sub

    Private Sub AddItem_Column(ByVal PkColumnID As String, ByVal FcColumnName As String)
        Try
            Dim row As String() =
             {False, PkColumnID, FcColumnName}

            Dim nRowIndex As Integer
            With grdColumn

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub cboModeOfPayment_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboModeOfPayment.DropDownClosed
        ComputeNumberOfPayments()
    End Sub

    Private Sub txtAmount_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub txtAmount_LostFocus(sender As Object, e As System.EventArgs) Handles txtAmount.LostFocus
        Try
            Dim temp As Double = txtAmount.Text
            txtAmount.Text = Format(temp, "N2")
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub cboTerms_TextChanged(sender As Object, e As System.EventArgs) Handles cboTerms.TextChanged
        cboModeOfPayment.Text = ""
        txtNoOfPayment.Text = "0"
    End Sub

    Private Sub btnUploaderTemplate_Click(sender As System.Object, e As System.EventArgs) Handles btnUploaderTemplate.Click
        SaveFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        SaveFileDialog1.ShowDialog()
        If DialogResult.OK Then
            GenerateBatchUploaderTemplate()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnBatchUploader_Click(sender As System.Object, e As System.EventArgs) Handles btnBatchUploader.Click
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            TextBox1.Text = fpath
            xFilePath = TextBox1.Text
            UploaderType = IO.Path.GetExtension(xFilePath)
            If DialogResult.OK Then
                ImportBatchUploading()
            Else
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnSaveUploader_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveUploader.Click
        Call SaveBatchAmortization()
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnClearParameters_Click(sender As System.Object, e As System.EventArgs) Handles btnClearParameters.Click
        cboAccount.SelectedValue = -1
        txtRefNo.Text = ""
        txtAmount.Text = "0.00"
        cboTerms.SelectedValue = -1
        cboModeOfPayment.Text = ""
        txtNoOfPayment.Text = "0"
        grdAmortization.Rows.Clear()
        grdAmortization.Columns.Clear()
    End Sub

    Private Sub grdUploader_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdUploader.CellEndEdit
        If CheckClientID(grdUploader.Item(1, grdUploader.CurrentRow.Index).Value.ToString) = False Then
            grdUploader.Rows(grdUploader.CurrentRow.Index).DefaultCellStyle.BackColor = Color.Red
            ColorCount += 1
        Else
            grdUploader.Rows(grdUploader.CurrentRow.Index).DefaultCellStyle.BackColor = Color.White
            ColorCount -= 1
        End If
    End Sub

    Private Sub grdColumn_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdColumn.CellClick
        Try
            If grdColumn.Item(0, e.RowIndex).Value = True Then
                grdColumn.Item(0, e.RowIndex).Value = False
                InsertAccountToTemp(0, e.RowIndex)
            Else
                grdColumn.Item(0, e.RowIndex).Value = True
                InsertAccountToTemp(1, e.RowIndex)
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        'InsertAccountToTemp(GetValuesInDataGridView(grdColumn, "FbSelected", e.RowIndex), e.RowIndex)
    End Sub

    Private Sub frmAccountAmortizationSetup_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        ClearTemp()
    End Sub

#End Region

End Class