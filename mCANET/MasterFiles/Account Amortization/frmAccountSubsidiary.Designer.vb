﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountSubsidiary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.miniToolStrip = New System.Windows.Forms.MenuStrip()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.grdColumns = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.txtRate = New System.Windows.Forms.TextBox()
        Me.rbInAmount = New System.Windows.Forms.RadioButton()
        Me.rbInPercent = New System.Windows.Forms.RadioButton()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtSubAccount = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtColumn = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboAccountList = New System.Windows.Forms.ComboBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grdAccount = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboAccount = New System.Windows.Forms.ComboBox()
        Me.tab1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.grdPriority = New System.Windows.Forms.DataGridView()
        Me.grdTerms = New System.Windows.Forms.DataGridView()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.MenuStrip3 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteTermToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddTermToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtTerm = New System.Windows.Forms.ToolStripTextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboOtherAccountList = New System.Windows.Forms.ComboBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PkAccountID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FkAccountKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcAccountCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcAccountTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PkAcntID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcColumnName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FkSubAcntID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcAcntCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcAcntTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FbInPercent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnRate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriorityColumnID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnPriority = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PkTermID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnTerms = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage3.SuspendLayout()
        CType(Me.grdColumns, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.MenuStrip2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.tab1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTerms, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.MenuStrip3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'miniToolStrip
        '
        Me.miniToolStrip.AutoSize = False
        Me.miniToolStrip.BackColor = System.Drawing.Color.Transparent
        Me.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me.miniToolStrip.Location = New System.Drawing.Point(204, 9)
        Me.miniToolStrip.Name = "miniToolStrip"
        Me.miniToolStrip.Size = New System.Drawing.Size(748, 36)
        Me.miniToolStrip.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.grdColumns)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 23)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(710, 427)
        Me.TabPage3.TabIndex = 4
        Me.TabPage3.Text = "Column Setup"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'grdColumns
        '
        Me.grdColumns.AllowUserToAddRows = False
        Me.grdColumns.AllowUserToDeleteRows = False
        Me.grdColumns.AllowUserToResizeColumns = False
        Me.grdColumns.AllowUserToResizeRows = False
        Me.grdColumns.BackgroundColor = System.Drawing.Color.White
        Me.grdColumns.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdColumns.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdColumns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdColumns.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PkAcntID, Me.FcColumnName, Me.FkSubAcntID, Me.FcAcntCode, Me.FcAcntTitle, Me.FbInPercent, Me.FnRate, Me.FnAmount})
        Me.grdColumns.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdColumns.Location = New System.Drawing.Point(3, 114)
        Me.grdColumns.Name = "grdColumns"
        Me.grdColumns.ReadOnly = True
        Me.grdColumns.RowHeadersVisible = False
        Me.grdColumns.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdColumns.Size = New System.Drawing.Size(704, 253)
        Me.grdColumns.TabIndex = 5
        '
        'GroupBox1
        '
        Me.GroupBox1.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox1.Controls.Add(Me.MenuStrip1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox1.Location = New System.Drawing.Point(3, 367)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(704, 57)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.EditToolStripMenuItem, Me.ToolStripMenuItem3, Me.CancelToolStripMenuItem, Me.ToolStripMenuItem4})
        Me.MenuStrip1.Location = New System.Drawing.Point(3, 18)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(698, 36)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripMenuItem1.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(64, 32)
        Me.ToolStripMenuItem1.Text = "Close"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(57, 32)
        Me.ToolStripMenuItem2.Text = "Add"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(55, 32)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Image = Global.CSAcctg.My.Resources.Resources.Save
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(59, 32)
        Me.ToolStripMenuItem3.Text = "Save"
        '
        'CancelToolStripMenuItem
        '
        Me.CancelToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.CancelToolStripMenuItem.Name = "CancelToolStripMenuItem"
        Me.CancelToolStripMenuItem.Size = New System.Drawing.Size(71, 32)
        Me.CancelToolStripMenuItem.Text = "Cancel"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(68, 32)
        Me.ToolStripMenuItem4.Text = "Delete"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtAmount)
        Me.GroupBox2.Controls.Add(Me.txtRate)
        Me.GroupBox2.Controls.Add(Me.rbInAmount)
        Me.GroupBox2.Controls.Add(Me.rbInPercent)
        Me.GroupBox2.Controls.Add(Me.btnSearch)
        Me.GroupBox2.Controls.Add(Me.txtSubAccount)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtColumn)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cboAccountList)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(704, 111)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(493, 80)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 14)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Amount"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(493, 51)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 14)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Rate"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(651, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 14)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "%"
        '
        'txtAmount
        '
        Me.txtAmount.Enabled = False
        Me.txtAmount.Location = New System.Drawing.Point(548, 77)
        Me.txtAmount.MaxLength = 18
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(100, 22)
        Me.txtAmount.TabIndex = 13
        '
        'txtRate
        '
        Me.txtRate.Location = New System.Drawing.Point(548, 48)
        Me.txtRate.MaxLength = 18
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Size = New System.Drawing.Size(100, 22)
        Me.txtRate.TabIndex = 12
        '
        'rbInAmount
        '
        Me.rbInAmount.AutoSize = True
        Me.rbInAmount.Location = New System.Drawing.Point(590, 17)
        Me.rbInAmount.Name = "rbInAmount"
        Me.rbInAmount.Size = New System.Drawing.Size(88, 18)
        Me.rbInAmount.TabIndex = 11
        Me.rbInAmount.Text = "In Amount"
        Me.rbInAmount.UseVisualStyleBackColor = True
        '
        'rbInPercent
        '
        Me.rbInPercent.AutoSize = True
        Me.rbInPercent.Checked = True
        Me.rbInPercent.Location = New System.Drawing.Point(474, 17)
        Me.rbInPercent.Name = "rbInPercent"
        Me.rbInPercent.Size = New System.Drawing.Size(95, 18)
        Me.rbInPercent.TabIndex = 10
        Me.rbInPercent.TabStop = True
        Me.rbInPercent.Text = "In Percent"
        Me.rbInPercent.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Enabled = False
        Me.btnSearch.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearch.Location = New System.Drawing.Point(427, 71)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(26, 26)
        Me.btnSearch.TabIndex = 9
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'txtSubAccount
        '
        Me.txtSubAccount.BackColor = System.Drawing.Color.White
        Me.txtSubAccount.Enabled = False
        Me.txtSubAccount.Location = New System.Drawing.Point(99, 73)
        Me.txtSubAccount.Name = "txtSubAccount"
        Me.txtSubAccount.Size = New System.Drawing.Size(322, 22)
        Me.txtSubAccount.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(9, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 14)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Sub-Account"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(9, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 14)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Column"
        '
        'txtColumn
        '
        Me.txtColumn.BackColor = System.Drawing.Color.White
        Me.txtColumn.Enabled = False
        Me.txtColumn.Location = New System.Drawing.Point(99, 45)
        Me.txtColumn.MaxLength = 100
        Me.txtColumn.Name = "txtColumn"
        Me.txtColumn.Size = New System.Drawing.Size(354, 22)
        Me.txtColumn.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Location = New System.Drawing.Point(9, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 14)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Account"
        '
        'cboAccountList
        '
        Me.cboAccountList.FormattingEnabled = True
        Me.cboAccountList.Location = New System.Drawing.Point(99, 16)
        Me.cboAccountList.Name = "cboAccountList"
        Me.cboAccountList.Size = New System.Drawing.Size(354, 22)
        Me.cboAccountList.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grdAccount)
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(710, 427)
        Me.TabPage2.TabIndex = 2
        Me.TabPage2.Text = "Account Setup"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'grdAccount
        '
        Me.grdAccount.AllowUserToAddRows = False
        Me.grdAccount.AllowUserToDeleteRows = False
        Me.grdAccount.AllowUserToResizeColumns = False
        Me.grdAccount.AllowUserToResizeRows = False
        Me.grdAccount.BackgroundColor = System.Drawing.Color.White
        Me.grdAccount.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdAccount.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.grdAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdAccount.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PkAccountID, Me.FkAccountKey, Me.FcAccountCode, Me.FcAccountTitle})
        Me.grdAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdAccount.Location = New System.Drawing.Point(3, 60)
        Me.grdAccount.Name = "grdAccount"
        Me.grdAccount.ReadOnly = True
        Me.grdAccount.RowHeadersVisible = False
        Me.grdAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdAccount.Size = New System.Drawing.Size(704, 307)
        Me.grdAccount.TabIndex = 5
        '
        'GroupBox3
        '
        Me.GroupBox3.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox3.Controls.Add(Me.MenuStrip2)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox3.Location = New System.Drawing.Point(3, 367)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(704, 57)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        '
        'MenuStrip2
        '
        Me.MenuStrip2.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem, Me.AddToolStripMenuItem, Me.DeleteToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(3, 18)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Size = New System.Drawing.Size(698, 36)
        Me.MenuStrip2.TabIndex = 0
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CloseToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(64, 32)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'AddToolStripMenuItem
        '
        Me.AddToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
        Me.AddToolStripMenuItem.Size = New System.Drawing.Size(57, 32)
        Me.AddToolStripMenuItem.Text = "Add"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(68, 32)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.cboAccount)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox4.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(704, 57)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(10, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 14)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Account"
        '
        'cboAccount
        '
        Me.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccount.FormattingEnabled = True
        Me.cboAccount.Location = New System.Drawing.Point(72, 21)
        Me.cboAccount.Name = "cboAccount"
        Me.cboAccount.Size = New System.Drawing.Size(399, 22)
        Me.cboAccount.TabIndex = 0
        '
        'tab1
        '
        Me.tab1.Controls.Add(Me.TabPage2)
        Me.tab1.Controls.Add(Me.TabPage3)
        Me.tab1.Controls.Add(Me.TabPage1)
        Me.tab1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab1.Location = New System.Drawing.Point(0, 0)
        Me.tab1.Name = "tab1"
        Me.tab1.SelectedIndex = 0
        Me.tab1.Size = New System.Drawing.Size(718, 454)
        Me.tab1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TabPage1.Controls.Add(Me.grdPriority)
        Me.TabPage1.Controls.Add(Me.grdTerms)
        Me.TabPage1.Controls.Add(Me.GroupBox5)
        Me.TabPage1.Controls.Add(Me.GroupBox6)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(710, 427)
        Me.TabPage1.TabIndex = 5
        Me.TabPage1.Text = "Other Setup"
        '
        'grdPriority
        '
        Me.grdPriority.AllowUserToAddRows = False
        Me.grdPriority.AllowUserToDeleteRows = False
        Me.grdPriority.AllowUserToResizeColumns = False
        Me.grdPriority.AllowUserToResizeRows = False
        Me.grdPriority.BackgroundColor = System.Drawing.Color.White
        Me.grdPriority.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdPriority.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.grdPriority.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPriority.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PriorityColumnID, Me.DataGridViewTextBoxColumn11, Me.FnPriority})
        Me.grdPriority.Dock = System.Windows.Forms.DockStyle.Left
        Me.grdPriority.Location = New System.Drawing.Point(3, 83)
        Me.grdPriority.Name = "grdPriority"
        Me.grdPriority.ReadOnly = True
        Me.grdPriority.RowHeadersVisible = False
        Me.grdPriority.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdPriority.Size = New System.Drawing.Size(333, 284)
        Me.grdPriority.TabIndex = 9
        '
        'grdTerms
        '
        Me.grdTerms.AllowUserToAddRows = False
        Me.grdTerms.AllowUserToDeleteRows = False
        Me.grdTerms.AllowUserToResizeColumns = False
        Me.grdTerms.AllowUserToResizeRows = False
        Me.grdTerms.BackgroundColor = System.Drawing.Color.White
        Me.grdTerms.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdTerms.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.grdTerms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTerms.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PkTermID, Me.FnTerms})
        Me.grdTerms.Dock = System.Windows.Forms.DockStyle.Right
        Me.grdTerms.Location = New System.Drawing.Point(372, 83)
        Me.grdTerms.Name = "grdTerms"
        Me.grdTerms.ReadOnly = True
        Me.grdTerms.RowHeadersVisible = False
        Me.grdTerms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdTerms.Size = New System.Drawing.Size(335, 284)
        Me.grdTerms.TabIndex = 8
        '
        'GroupBox5
        '
        Me.GroupBox5.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox5.Controls.Add(Me.MenuStrip3)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox5.Location = New System.Drawing.Point(3, 367)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(704, 57)
        Me.GroupBox5.TabIndex = 7
        Me.GroupBox5.TabStop = False
        '
        'MenuStrip3
        '
        Me.MenuStrip3.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem5, Me.ToolStripMenuItem6, Me.ToolStripMenuItem7, Me.DeleteTermToolStripMenuItem, Me.AddTermToolStripMenuItem, Me.txtTerm})
        Me.MenuStrip3.Location = New System.Drawing.Point(3, 18)
        Me.MenuStrip3.Name = "MenuStrip3"
        Me.MenuStrip3.Size = New System.Drawing.Size(698, 36)
        Me.MenuStrip3.TabIndex = 0
        Me.MenuStrip3.Text = "MenuStrip3"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripMenuItem5.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(64, 32)
        Me.ToolStripMenuItem5.Text = "Close"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Image = Global.CSAcctg.My.Resources.Resources.Up
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(83, 32)
        Me.ToolStripMenuItem6.Text = "Move Up"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Image = Global.CSAcctg.My.Resources.Resources.Down
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(99, 32)
        Me.ToolStripMenuItem7.Text = "Move Down"
        '
        'DeleteTermToolStripMenuItem
        '
        Me.DeleteTermToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.DeleteTermToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.DeleteTermToolStripMenuItem.Name = "DeleteTermToolStripMenuItem"
        Me.DeleteTermToolStripMenuItem.Size = New System.Drawing.Size(99, 32)
        Me.DeleteTermToolStripMenuItem.Text = "Delete Term"
        '
        'AddTermToolStripMenuItem
        '
        Me.AddTermToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.AddTermToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.AddTermToolStripMenuItem.Name = "AddTermToolStripMenuItem"
        Me.AddTermToolStripMenuItem.Size = New System.Drawing.Size(88, 32)
        Me.AddTermToolStripMenuItem.Text = "Add Term"
        '
        'txtTerm
        '
        Me.txtTerm.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.txtTerm.MaxLength = 4
        Me.txtTerm.Name = "txtTerm"
        Me.txtTerm.Size = New System.Drawing.Size(50, 32)
        '
        'GroupBox6
        '
        Me.GroupBox6.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Controls.Add(Me.Label9)
        Me.GroupBox6.Controls.Add(Me.Label8)
        Me.GroupBox6.Controls.Add(Me.cboOtherAccountList)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(704, 80)
        Me.GroupBox6.TabIndex = 6
        Me.GroupBox6.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(513, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 19)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Terms"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(117, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(81, 19)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Priority"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Location = New System.Drawing.Point(10, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 14)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Account"
        '
        'cboOtherAccountList
        '
        Me.cboOtherAccountList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboOtherAccountList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOtherAccountList.FormattingEnabled = True
        Me.cboOtherAccountList.Location = New System.Drawing.Point(72, 21)
        Me.cboOtherAccountList.Name = "cboOtherAccountList"
        Me.cboOtherAccountList.Size = New System.Drawing.Size(399, 22)
        Me.cboOtherAccountList.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "PkAccountID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "FkAccountKey"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.FillWeight = 194.9239!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Column Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.FillWeight = 5.076141!
        Me.DataGridViewTextBoxColumn4.HeaderText = "Account Title"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "PkAccountID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "Column Name"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "FkAccountKey"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.HeaderText = "Account Title"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "PkAccountID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn11.HeaderText = "Column Name"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn12.FillWeight = 5.076141!
        Me.DataGridViewTextBoxColumn12.HeaderText = "PkTermID"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn13.FillWeight = 5.076141!
        Me.DataGridViewTextBoxColumn13.HeaderText = "PkTermID"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn14.FillWeight = 5.076141!
        Me.DataGridViewTextBoxColumn14.HeaderText = "Terms in Month"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "FnPriority"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn16.FillWeight = 5.076141!
        Me.DataGridViewTextBoxColumn16.HeaderText = "PkTermID"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn17.FillWeight = 5.076141!
        Me.DataGridViewTextBoxColumn17.HeaderText = "Terms in Month"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        '
        'PkAccountID
        '
        Me.PkAccountID.HeaderText = "PkAccountID"
        Me.PkAccountID.Name = "PkAccountID"
        Me.PkAccountID.ReadOnly = True
        Me.PkAccountID.Visible = False
        '
        'FkAccountKey
        '
        Me.FkAccountKey.HeaderText = "FkAccountKey"
        Me.FkAccountKey.Name = "FkAccountKey"
        Me.FkAccountKey.ReadOnly = True
        Me.FkAccountKey.Visible = False
        '
        'FcAccountCode
        '
        Me.FcAccountCode.FillWeight = 194.9239!
        Me.FcAccountCode.HeaderText = "Account Code"
        Me.FcAccountCode.Name = "FcAccountCode"
        Me.FcAccountCode.ReadOnly = True
        Me.FcAccountCode.Width = 200
        '
        'FcAccountTitle
        '
        Me.FcAccountTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FcAccountTitle.FillWeight = 5.076141!
        Me.FcAccountTitle.HeaderText = "Account Title"
        Me.FcAccountTitle.Name = "FcAccountTitle"
        Me.FcAccountTitle.ReadOnly = True
        '
        'PkAcntID
        '
        Me.PkAcntID.HeaderText = "PkAccountID"
        Me.PkAcntID.Name = "PkAcntID"
        Me.PkAcntID.ReadOnly = True
        Me.PkAcntID.Visible = False
        '
        'FcColumnName
        '
        Me.FcColumnName.HeaderText = "Column Name"
        Me.FcColumnName.Name = "FcColumnName"
        Me.FcColumnName.ReadOnly = True
        Me.FcColumnName.Width = 300
        '
        'FkSubAcntID
        '
        Me.FkSubAcntID.HeaderText = "FkAccountKey"
        Me.FkSubAcntID.Name = "FkSubAcntID"
        Me.FkSubAcntID.ReadOnly = True
        Me.FkSubAcntID.Visible = False
        '
        'FcAcntCode
        '
        Me.FcAcntCode.HeaderText = "Code"
        Me.FcAcntCode.Name = "FcAcntCode"
        Me.FcAcntCode.ReadOnly = True
        '
        'FcAcntTitle
        '
        Me.FcAcntTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FcAcntTitle.HeaderText = "Account Title"
        Me.FcAcntTitle.Name = "FcAcntTitle"
        Me.FcAcntTitle.ReadOnly = True
        '
        'FbInPercent
        '
        Me.FbInPercent.HeaderText = "FbInPercent"
        Me.FbInPercent.Name = "FbInPercent"
        Me.FbInPercent.ReadOnly = True
        Me.FbInPercent.Visible = False
        '
        'FnRate
        '
        Me.FnRate.HeaderText = "FnRate"
        Me.FnRate.Name = "FnRate"
        Me.FnRate.ReadOnly = True
        Me.FnRate.Visible = False
        '
        'FnAmount
        '
        Me.FnAmount.HeaderText = "FnAmount"
        Me.FnAmount.Name = "FnAmount"
        Me.FnAmount.ReadOnly = True
        Me.FnAmount.Visible = False
        '
        'PriorityColumnID
        '
        Me.PriorityColumnID.HeaderText = "PkAccountID"
        Me.PriorityColumnID.Name = "PriorityColumnID"
        Me.PriorityColumnID.ReadOnly = True
        Me.PriorityColumnID.Visible = False
        '
        'FnPriority
        '
        Me.FnPriority.HeaderText = "FnPriority"
        Me.FnPriority.Name = "FnPriority"
        Me.FnPriority.ReadOnly = True
        Me.FnPriority.Visible = False
        '
        'PkTermID
        '
        Me.PkTermID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.PkTermID.FillWeight = 5.076141!
        Me.PkTermID.HeaderText = "PkTermID"
        Me.PkTermID.Name = "PkTermID"
        Me.PkTermID.ReadOnly = True
        Me.PkTermID.Visible = False
        '
        'FnTerms
        '
        Me.FnTerms.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FnTerms.FillWeight = 5.076141!
        Me.FnTerms.HeaderText = "Terms in Month"
        Me.FnTerms.Name = "FnTerms"
        Me.FnTerms.ReadOnly = True
        '
        'frmAccountSubsidiary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(718, 454)
        Me.Controls.Add(Me.tab1)
        Me.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmAccountSubsidiary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Subsidiary Account Setup"
        Me.TabPage3.ResumeLayout(False)
        CType(Me.grdColumns, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdAccount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.tab1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.grdPriority, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTerms, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.MenuStrip3.ResumeLayout(False)
        Me.MenuStrip3.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents miniToolStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents grdColumns As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents txtSubAccount As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtColumn As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboAccountList As System.Windows.Forms.ComboBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdAccount As System.Windows.Forms.DataGridView
    Friend WithEvents PkAccountID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FkAccountKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcAccountCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcAccountTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboAccount As System.Windows.Forms.ComboBox
    Friend WithEvents tab1 As System.Windows.Forms.TabControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents txtRate As System.Windows.Forms.TextBox
    Friend WithEvents rbInAmount As System.Windows.Forms.RadioButton
    Friend WithEvents rbInPercent As System.Windows.Forms.RadioButton
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grdTerms As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip3 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboOtherAccountList As System.Windows.Forms.ComboBox
    Friend WithEvents grdPriority As System.Windows.Forms.DataGridView
    Friend WithEvents DeleteTermToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddTermToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtTerm As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PkTermID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnTerms As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PriorityColumnID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PkAcntID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcColumnName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FkSubAcntID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcAcntCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcAcntTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FbInPercent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
