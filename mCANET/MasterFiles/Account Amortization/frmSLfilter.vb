﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes

Public Class frmSLfilter
    Private gCon As New Clsappconfiguration()
    Dim KeyAccountID As String
    Dim dv As New DataView
    Dim ds As New DataSet

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Sub frmSLfilter_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadDebitCredit()
        loadSubsidiaryAccounts()
        LoadGridDetails()
    End Sub

    Private Sub LoadDebitCredit()
        KeyAccountID = Nothing
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccountType
                .ValueMember = "FkAccount"
                .DisplayMember = "acntwithcode"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                '.Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub loadSubsidiaryAccounts()
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_SLFilter",
                                      New SqlParameter("@coid", gCompanyID),
                                      New SqlParameter("@acntKey", cboAccountType.SelectedValue))

        'grdSAList.DataSource = ds.Tables(0)
        dv.Table = ds.Tables(0)
    End Sub

    Private Sub LoadGridDetails()
        dv = New DataView(ds.Tables(0))
        dv.RowFilter = "fcFullname LIKE '%" & txtSearch.Text & "%' OR fcEmployeeNo LIKE '%" & txtSearch.Text & "%' OR fcDocNumber LIKE '%" & txtSearch.Text & "%'"
        grdSAList.DataSource = dv

        With grdSAList
            .Columns("fxKeyAccountRegister").Visible = False
            .Columns("fcDocNumber").Width = 100
            .Columns("fcDocNumber").ReadOnly = True
            .Columns("fcDocNumber").HeaderText = "Account Ref."
            .Columns("fcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcEmployeeNo").Width = 100
            .Columns("fcEmployeeNo").ReadOnly = True
            .Columns("fcEmployeeNo").HeaderText = "ID"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcFullname").Width = 250
            .Columns("fcFullname").ReadOnly = True
            .Columns("fcFullname").HeaderText = "Name"
            .Columns("fcFullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcAccountName").Width = 290
            .Columns("fcAccountName").ReadOnly = True
            .Columns("fcAccountName").HeaderText = "Account Title"
            .Columns("fcAccountName").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fxKey_Account").Visible = False
            .Columns("pk_Employee").Visible = False
            .Columns("fcAccountCode").ReadOnly = True
            .Columns("fcAccountCode").HeaderText = "Code"
            .Columns("fcAccountCode").Width = 70
            .Columns("fcAccountCode").SortMode = DataGridViewColumnSortMode.NotSortable
        End With
        dv.Sort = "fcFullname"
    End Sub

    Private Sub cboAccountType_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboAccountType.DropDownClosed
        loadSubsidiaryAccounts()
        LoadGridDetails()
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        LoadGridDetails()
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub cboAccountType_SelectedValueChanged(sender As System.Object, e As System.EventArgs) Handles cboAccountType.SelectedValueChanged
        loadSubsidiaryAccounts()
        LoadGridDetails()
    End Sub
End Class