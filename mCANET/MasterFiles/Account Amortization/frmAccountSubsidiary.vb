﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes

Public Class frmAccountSubsidiary
    Private gCon As New Clsappconfiguration()
    Dim KeyAccountID As String
    Dim KeyID As Integer
    Dim ColumnID As Integer
    Dim SubAccountKey As String

#Region "Other Setup Declaration"
    Dim PriorityColID1 As Integer
    Dim Priority1 As Integer
    Dim PriorityColID2 As Integer
    Dim Priority2 As Integer
    Dim CurrentSelectedRow As Integer
#End Region

#Region "Logic"
    Private Function Msg()
        Dim str As String

        If txtColumn.Text = "" Then
            str = "Please Input Column Name." + vbNewLine
        End If

        If SubAccountKey = "" Then
            str += "Please Select Sub-Account." + vbNewLine
        End If

        If rbInPercent.Checked = True And txtRate.Text = "" Then
            str += "Please Input Rate." + vbNewLine
        End If

        If rbInAmount.Checked = True And txtAmount.Text = "" Then
            str += "Please Input Amount." + vbNewLine
        End If

        Return str
    End Function

    Private Sub rbLogic(ByVal InPercent As Boolean)
        If rbInPercent.Checked = True Then
            txtAmount.Text = "0.00"
            txtAmount.Enabled = False
            txtRate.Enabled = True
        Else
            txtRate.Text = "0.00"
            txtAmount.Enabled = True
            txtRate.Enabled = False
        End If
    End Sub

    Private Sub PriorityLogic(ByVal ActionCondition As String)
        If ActionCondition = "MoveUp" Then
            PriorityColID1 = grdPriority.Item("PriorityColumnID", grdPriority.CurrentRow.Index).Value
            Priority1 = grdPriority.Item("FnPriority", grdPriority.CurrentRow.Index).Value
            Try
                PriorityColID2 = grdPriority.Item("PriorityColumnID", grdPriority.CurrentRow.Index - 1).Value
                Priority2 = grdPriority.Item("FnPriority", grdPriority.CurrentRow.Index - 1).Value
                CurrentSelectedRow = grdPriority.CurrentRow.Index - 1
            Catch ex As Exception
                PriorityColID2 = grdPriority.Item("PriorityColumnID", grdPriority.CurrentRow.Index).Value
                Priority2 = grdPriority.Item("FnPriority", grdPriority.CurrentRow.Index).Value
                CurrentSelectedRow = grdPriority.CurrentRow.Index
            End Try

        ElseIf ActionCondition = "MoveDown" Then
            PriorityColID1 = grdPriority.Item("PriorityColumnID", grdPriority.CurrentRow.Index).Value
            Priority1 = grdPriority.Item("FnPriority", grdPriority.CurrentRow.Index).Value
            Try
                PriorityColID2 = grdPriority.Item("PriorityColumnID", grdPriority.CurrentRow.Index + 1).Value
                Priority2 = grdPriority.Item("FnPriority", grdPriority.CurrentRow.Index + 1).Value
                CurrentSelectedRow = grdPriority.CurrentRow.Index + 1
            Catch ex As Exception
                PriorityColID2 = grdPriority.Item("PriorityColumnID", grdPriority.CurrentRow.Index).Value
                Priority2 = grdPriority.Item("FnPriority", grdPriority.CurrentRow.Index).Value
                CurrentSelectedRow = grdPriority.CurrentRow.Index
            End Try
        End If
    End Sub
#End Region

#Region "Data Access Layer"
    '===================================================================================================================
    'ACCOUNT SETUP
    '===================================================================================================================
    Private Sub LoadDebitCredit()
        KeyAccountID = Nothing
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_ListDebitCredit", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccount
                .ValueMember = "acnt_id"
                .DisplayMember = "AccountName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadAccountsDropdown()
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboAccountList
                .ValueMember = "PkAccount"
                .DisplayMember = "acnt_name"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadAccounts()
        grdAccount.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_View",
                                                     New SqlParameter("@coid", gCompanyID))
        While rd.Read = True
            AddItem(rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString)
        End While
    End Sub

    Private Sub AddItem(ByVal PkAccount As String,
                              ByVal FkAccount As String,
                              ByVal FcAcntCode As String,
                              ByVal FcAcntTitle As String)
        Try
            Dim row As String() =
             {PkAccount, FkAccount, FcAcntCode, FcAcntTitle}

            Dim nRowIndex As Integer
            With grdAccount

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Save()
        Dim myid As New Guid(KeyAccountID)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_InsertUpdate",
                                                     New SqlParameter("@FkAccount", myid))
    End Sub

    Private Sub Delete()
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_Delete",
                                                     New SqlParameter("@PkAccount", KeyID))
    End Sub

    '====================================================================================================================================================
    'COLUMN SETUP
    '===================================================================================================================

    Private Sub LoadColumns()
        grdColumns.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_ListByAccount",
                                                     New SqlParameter("@FkAccountID", Convert.ToInt32(cboAccountList.SelectedValue)))
        While rd.Read = True
            AddItem_Column(rd.Item(0), rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, rd.Item(4).ToString, Convert.ToBoolean(rd.Item(5)), Convert.ToDecimal(rd.Item(6)), Convert.ToDecimal(rd.Item(7)))
        End While
    End Sub

    Private Sub AddItem_Column(ByVal PkColumnID As Integer,
                              ByVal FcColumnName As String,
                              ByVal FkSubAccountKey As String,
                              ByVal FcAcntCode As String,
                              ByVal FcAcntTitle As String,
                              ByVal FbInPercent As Boolean,
                              ByVal FnRate As Decimal,
                              ByVal FnAmount As Decimal)
        Try
            Dim row As String() =
             {PkColumnID, FcColumnName, FkSubAccountKey, FcAcntCode, FcAcntTitle, FbInPercent, FnRate, FnAmount}

            Dim nRowIndex As Integer
            With grdColumns

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SaveColumn()
        Try
            Dim myid As New Guid(SubAccountKey)
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_InsertUpdate",
                                                        New SqlParameter("@PkColumnID", ColumnID),
                                                        New SqlParameter("@FkAccountID", CInt(cboAccountList.SelectedValue)),
                                                        New SqlParameter("@FcColumnName", txtColumn.Text),
                                                        New SqlParameter("@FkSubAccountKey", myid),
                                                        New SqlParameter("@FbInPercent", rbInPercent.Checked),
                                                        New SqlParameter("@FnRate", CDec(txtRate.Text)),
                                                        New SqlParameter("@FnAmount", CDec(txtAmount.Text)))
            MsgBox("Saved.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox("Command failed.", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub DeleteColumn(ByVal ColumnID As Integer)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_Delete",
                                                     New SqlParameter("@PkColumnID", ColumnID))
    End Sub

    Private Sub ColumnDetails()
        Try
            ColumnID = grdColumns.Item("PkAcntID", grdColumns.CurrentRow.Index).Value.ToString
            SubAccountKey = grdColumns.Item("FkSubAcntID", grdColumns.CurrentRow.Index).Value.ToString
            txtColumn.Text = grdColumns.Item("FcColumnName", grdColumns.CurrentRow.Index).Value.ToString
            txtSubAccount.Text = grdColumns.Item("FcAcntTitle", grdColumns.CurrentRow.Index).Value.ToString
            rbInPercent.Checked = grdColumns.Item("FbInPercent", grdColumns.CurrentRow.Index).Value
            txtRate.Text = grdColumns.Item("FnRate", grdColumns.CurrentRow.Index).Value
            txtAmount.Text = grdColumns.Item("FnAmount", grdColumns.CurrentRow.Index).Value
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    '===================================================================================================================
    'OTHER SETUP
    '===================================================================================================================
    Private Sub LoadOtherAccountsDropdown()
        Dim myid As New Guid(gCompanyID)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Accounts")
            With cboOtherAccountList
                .ValueMember = "PkAccount"
                .DisplayMember = "acnt_name"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadOtherColumns()
        grdPriority.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationPriority_ListByAccount",
                                                     New SqlParameter("@FkAccountID", Convert.ToInt32(cboOtherAccountList.SelectedValue)))
        While rd.Read = True
            AddItem_OtherColumn(rd.Item(0), rd.Item(1).ToString, rd.Item(5))
        End While
    End Sub

    Private Sub AddItem_OtherColumn(ByVal PkColumnID As Integer,
                              ByVal FcColumnName As String,
                              ByVal FnPriority As Integer)
        Try
            Dim row As String() =
             {PkColumnID, FcColumnName, FnPriority}

            Dim nRowIndex As Integer
            With grdPriority

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub LoadTerms()
        grdTerms.Rows.Clear()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_Terms_ListByAccount",
                                                     New SqlParameter("@FkAccountID", Convert.ToInt32(cboOtherAccountList.SelectedValue)))
        While rd.Read = True
            AddItem_Terms(rd.Item(0), rd.Item(1))
        End While
    End Sub

    Private Sub AddItem_Terms(ByVal PkTermID As Integer,
                              ByVal FnTerm As Decimal)
        Try
            Dim row As String() =
             {PkTermID, FnTerm}

            Dim nRowIndex As Integer
            With grdTerms

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SaveTerm()
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_Terms_Insert",
                                                     New SqlParameter("@FkAccountID", Convert.ToInt32(cboOtherAccountList.SelectedValue)),
                                                     New SqlParameter("@FnTerms", txtTerm.Text))
    End Sub

    Private Sub DeleteTerm(ByVal TermID As Integer)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortization_Terms_Delete",
                                                     New SqlParameter("@PkTermID", TermID))
    End Sub

    Private Sub UpdatePiority()
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationPriority_Update",
                                                     New SqlParameter("@PkColumnID", PriorityColID1),
                                                     New SqlParameter("@FnPriority", Priority2))

        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationPriority_Update",
                                                     New SqlParameter("@PkColumnID", PriorityColID2),
                                                     New SqlParameter("@FnPriority", Priority1))
    End Sub

#End Region

#Region "Event Handler"

    Private Sub frmAccountSubsidiary_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadDebitCredit()
        LoadAccounts()
        LoadAccountsDropdown()
        LoadColumns()
        ColumnDetails()
        LoadOtherAccountsDropdown()
        LoadOtherColumns()
        LoadTerms()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        If cboAccount.Text <> "Select" Then
            Save()
            LoadAccounts()
            LoadDebitCredit()
            LoadAccountsDropdown()
        Else
            MsgBox("Please Select Account", MsgBoxStyle.Exclamation, "Account Setup")
            Exit Sub
        End If
    End Sub

    Private Sub cboAccount_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboAccount.DropDownClosed
        Try
            KeyAccountID = cboAccount.SelectedValue.ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub grdAccount_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccount.CellClick
        KeyID = grdAccount.Item("PkAccountID", grdAccount.CurrentRow.Index).Value
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Delete()
        LoadAccounts()
        LoadDebitCredit()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Me.Close()
    End Sub

    Private Sub ToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem2.Click
        txtColumn.Clear()
        txtSubAccount.Clear()
        ColumnID = 0
        SubAccountKey = ""

        txtColumn.Enabled = True
        btnSearch.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtSubAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            SubAccountKey = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub ToolStripMenuItem3_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem3.Click
        If Msg() = "" Then
            SaveColumn()
            LoadColumns()

            txtColumn.Enabled = False
            btnSearch.Enabled = False

            txtColumn.Clear()
            txtSubAccount.Clear()
            SubAccountKey = ""
        Else
            MsgBox(Msg, MsgBoxStyle.Exclamation)
            Exit Sub
        End If
    End Sub

    Private Sub cboAccountList_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboAccountList.DropDownClosed
        LoadColumns()
        ColumnDetails()
        txtColumn.Enabled = False
        btnSearch.Enabled = False

        txtColumn.Clear()
        txtSubAccount.Clear()
        SubAccountKey = ""
    End Sub

    Private Sub ToolStripMenuItem4_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem4.Click
        If MsgBox("Delete this row?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Account Amortization") = MsgBoxResult.Yes Then
            DeleteColumn(grdColumns.Item("PkAcntID", grdColumns.CurrentRow.Index).Value)
            LoadColumns()

            txtColumn.Enabled = False
            btnSearch.Enabled = False

            txtColumn.Clear()
            txtSubAccount.Clear()
            SubAccountKey = ""
        Else
            Exit Sub
        End If
    End Sub

    Private Sub EditToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EditToolStripMenuItem.Click
        txtColumn.Enabled = True
    End Sub

    Private Sub CancelToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CancelToolStripMenuItem.Click
        txtColumn.Enabled = False
        btnSearch.Enabled = False
    End Sub

    Private Sub cboOtherAccountList_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles cboOtherAccountList.DropDownClosed
        LoadOtherColumns()
        LoadTerms()
    End Sub

    Private Sub ToolStripMenuItem5_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem5.Click
        Me.Close()
    End Sub

    Private Sub AddTermToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddTermToolStripMenuItem.Click
        SaveTerm()
        LoadTerms()
    End Sub

    Private Sub DeleteTermToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DeleteTermToolStripMenuItem.Click
        DeleteTerm(grdTerms.Item("PkTermID", grdTerms.CurrentRow.Index).Value)
        LoadTerms()
    End Sub

    Private Sub rbInPercent_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbInPercent.CheckedChanged
        rbLogic(rbInPercent.Checked)
    End Sub

    Private Sub rbInAmount_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbInAmount.CheckedChanged
        rbLogic(rbInPercent.Checked)
    End Sub

    Private Sub ToolStripMenuItem6_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem6.Click
        PriorityLogic("MoveUp")
        UpdatePiority()
        LoadOtherColumns()
        grdPriority.ClearSelection()
        grdPriority.Rows(CurrentSelectedRow).Selected = True
    End Sub

    Private Sub ToolStripMenuItem7_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripMenuItem7.Click
        PriorityLogic("MoveDown")
        UpdatePiority()
        LoadOtherColumns()
        grdPriority.ClearSelection()
        grdPriority.Rows(CurrentSelectedRow).Selected = True
    End Sub

    Private Sub txtRate_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtRate.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub txtAmount_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub grdColumns_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles grdColumns.SelectionChanged
        ColumnDetails()
    End Sub

    Private Sub grdColumns_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdColumns.CellClick
        ColumnDetails()
    End Sub

#End Region
End Class