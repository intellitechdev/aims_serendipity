Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmChartOfAccounts_Edit

    Private gCon As New Clsappconfiguration()

    Private accountID As String
    Private accountSub As String = "NULL"
    Private isDebit As Boolean

    Public Property GetAccountID() As String
        Get
            Return accountID
        End Get
        Set(ByVal value As String)
            accountID = value
        End Set
    End Property

    'Mode:  1."New'
    '       2."Edit"
    Private mode As String

    Public Property getMode() As String
        Get
            Return mode
        End Get
        Set(ByVal value As String)
            mode = value
        End Set
    End Property

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub LoadAccountTypesToCombobox()
        Dim dtAccountType As New DataTable("Account Types")
        Dim dRow As DataRow

        Dim storedProcedure As String = "Masterfiles_AccountType"

        dtAccountType.Columns.Add("acnt_type", System.Type.GetType("System.String"))
        dtAccountType.Columns.Add("actType_id", System.Type.GetType("System.String"))

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                        New SqlParameter("@companyID", gCompanyID()))
                While rd.Read
                    dRow = dtAccountType.NewRow

                    dRow("acnt_type") = rd.Item(1).ToString
                    dRow("actType_id") = rd.Item(0).ToString

                    dtAccountType.Rows.Add(dRow)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at modGlobalListing at m_loadCustomerToMultiComboBox." + vbCr + vbCr + ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        cboAccountTypes.Items.Clear()
        cboAccountTypes.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        cboAccountTypes.SourceDataString = New String(1) {"acnt_type", "actType_id"}
        cboAccountTypes.SourceDataTable = dtAccountType
    End Sub

    Private Sub LoadAllChartOfAccountsToComboBox()
        Dim dtAccountType As New DataTable("Chart Of Accounts")
        Dim dRow As DataRow

        Dim storedProcedure As String = "Masterfile_ChartOfAccounts_ActiveAndInactive"

        dtAccountType.Columns.Add("acnt_name", System.Type.GetType("System.String"))
        dtAccountType.Columns.Add("acnt_type", System.Type.GetType("System.String"))
        dtAccountType.Columns.Add("acnt_id", System.Type.GetType("System.String"))

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                        New SqlParameter("@companyID", gCompanyID()))
                While rd.Read
                    dRow = dtAccountType.NewRow

                    dRow("acnt_name") = rd.Item(1).ToString
                    dRow("acnt_type") = rd.Item(2).ToString
                    dRow("acnt_id") = rd.Item(0).ToString

                    dtAccountType.Rows.Add(dRow)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        With cboSubAccountOf
            .Items.Clear()
            .LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
            .SourceDataString = New String(2) {"acnt_name", "acnt_type", "acnt_id"}
            .SourceDataTable = dtAccountType
        End With

    End Sub
    Private Function LoadAccountTypeDescription(ByVal companyID As String, ByVal accountTypeID As String) As String
        Dim description As String = ""
        Dim storedProc As String = "Masterfiles_AccountType_PerType"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storedProc, _
                    New SqlParameter("@companyID", companyID), _
                    New SqlParameter("@accountTypeID", accountTypeID))
                If rd.Read() Then
                    description = rd.Item(2).ToString()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return description
    End Function
    Private Sub LoadAccountForEditing(ByVal accountID As String)
        Dim storeProc As String = "Masterfile_ChartOfAccounts_PerAccount"
        Dim co_id As New Guid(gCompanyID)
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storeProc, _
                            New SqlParameter("@compID", co_id), _
                            New SqlParameter("@accountID", GetAccountID()))
                If rd.Read() Then
                    cboLevel.Text = rd.Item(3)
                    txtAccountCode.Text = rd.Item(4).ToString()
                    txtAccountName.Text = rd.Item(5).ToString()
                    txtAccountDesc.Text = rd.Item(6).ToString()
                    cboAccountTypes.SelectedValue = rd.Item(7).ToString()
                    chkIsActive.Checked = rd.Item(8)
                    chkIsSubAccountOf.Checked = rd.Item(9)
                    cboSubAccountOf.SelectedValue = rd.Item(10).ToString().Trim()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UpdateChartOfAccount(ByVal keyID As String, ByVal aCode As String, ByVal aName As String)
        Dim gID As New Guid(keyID)
        Dim co_id As New Guid(gCompanyID)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, "spu_ChartOfAccounts_Update",
                          New SqlParameter("@accountID", gID),
                          New SqlParameter("@acnt_code", aCode),
                          New SqlParameter("@acnt_name", aName),
                          New SqlParameter("@co_id", co_id))
    End Sub

    Private Sub UpdateAccount()
        Dim compID As String = gCompanyID()
        Dim accountID As String
        Dim accountLevel As String = cboLevel.Text
        Dim accountCode As String = txtAccountCode.Text
        Dim accountName As String = txtAccountName.Text
        Dim accountDesc As String = txtAccountDesc.Text
        Dim accountType As String = cboAccountTypes.SelectedItem.Col2
        Dim isActive As Int16 = IIf(chkIsActive.Checked.ToString() = "True", 1, 0)
        Dim isAccountSub As Int16 = IIf(chkIsSubAccountOf.Checked.ToString() = "True", 1, 0)
        Dim isdebit As Int16 = IIf(chDebit.Checked.ToString() = "True", 1, 0)
        Dim sSqlCmd As String
        Dim user As String = gUserName

        If GetAccountID() <> "" Then
            accountID = GetAccountID()
        Else
            accountID = Guid.NewGuid.ToString
        End If


        Try
            sSqlCmd = "Masterfile_ChartOfAccounts_Edit " & _
                      "@compID = '" & compID & "', " & _
                      "@accountID = '" & accountID & "', " & _
                      "@fcLevel = '" & accountLevel & "', " & _
                      "@acnt_code = '" & accountCode & "', " & _
                      "@acnt_name = '" & accountName & "', " & _
                      "@acnt_desc = '" & accountDesc & "', " & _
                      "@acnt_type = '" & accountType & "', " & _
                      "@fbActive = '" & isActive & "', " & _
                      "@acnt_sub = '" & isAccountSub & "', " & _
                      "@isdebit = '" & isdebit & "', "

            If chkIsSubAccountOf.Checked = True Then
                If accountSub <> "NULL" Then
                    sSqlCmd &= "@acnt_subof = '" & accountSub & "', "
                End If
            End If

            sSqlCmd &= "@user =' " & user & "'"

            UpdateChartOfAccount(accountID, accountCode, accountName)

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
            MessageBox.Show("Account Update Successful!", getMode().ToString() + " Account", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Dim sSQLCmd1 As String = "update mBankList Set fcBankName = '" & accountName & "' where FK_AcntId= '" & accountID & "'"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd1)

            Call Close()
            frmChartOfAccounts.LoadChartOfAccountsToGrid()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub DisableEnableControl(ByVal control As Control, ByVal status As Boolean)
        control.Enabled = status
    End Sub

    Private Sub frmChartOfAccounts_Edit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadAccountTypesToCombobox()
        Call LoadAllChartOfAccountsToComboBox()

        isDebit = frmChartOfAccounts.gridChartOfAccounts.SelectedRows(0).Cells(9).Value.ToString
        If isDebit = True Then
            chDebit.Checked = True
        Else
            chDebit.Checked = False
        End If
        If GetAccountID() <> "" Then
            Call LoadAccountForEditing(GetAccountID())
        Else
            Me.Text = "Create Account"
        End If
        Call DisableEnableControl(cboSubAccountOf, chkIsSubAccountOf.Checked)
    End Sub

    Private Sub cboAccountTypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccountTypes.SelectedIndexChanged
        Dim accountTypeID As String
        Dim companyID As String

        accountTypeID = cboAccountTypes.SelectedItem.Col2.ToString()
        companyID = gCompanyID()

        txtAccountTypeDesc.Text = LoadAccountTypeDescription(companyID, accountTypeID)
        If Me.getMode = "New" Then
            DebitCreditAccount(accountTypeID)
        End If
    End Sub

    Private Sub frmChartOfAccounts_Edit_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Opacity = 0.5
    End Sub

    Private Sub frmChartOfAccounts_Edit_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Opacity = 1
    End Sub
    Private Function IsAccountCodeUnique() As Boolean
        Dim sSqlCmd As String = "select acnt_name from dbo.mAccounts where acnt_code = '" & txtAccountCode.Text & "' and co_id = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            ShowErrorMessage("IsAccountCodeUnique", Me.Name, ex.Message)
            Return False
        End Try
    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Select Case getMode()
            Case "New"
                NewModeTransaction()
            Case "Edit"
                EditModeTransaction()
        End Select

    End Sub

    Private Sub chkIsSubAccountOf_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIsSubAccountOf.CheckedChanged
        DisableEnableControl(cboSubAccountOf, chkIsSubAccountOf.Checked)
    End Sub

    Private Sub cboSubAccountOf_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSubAccountOf.SelectedIndexChanged
        If cboSubAccountOf.SelectedItem.Col3 <> Nothing Then
            accountSub = cboSubAccountOf.SelectedItem.Col3
        End If
    End Sub

    Private Sub frmChartOfAccounts_Edit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'Resets the forms mode
        getMode() = ""
    End Sub

    Private Sub NewModeTransaction()
        If IsAccountCodeUnique() = True Then
            If GetAccountID() <> "" Then
                If MessageBox.Show("Are you sure you want to apply changes?", getMode().ToString() + " Account", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                    Call UpdateAccount()
                End If
            Else
                ' This is for new account
                UpdateAccount()
            End If
        End If
    End Sub

    Private Sub EditModeTransaction()
        If GetAccountID() <> "" Then
            If MessageBox.Show("Are you sure you want to apply changes?", getMode().ToString() + " Account", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Call UpdateAccount()
                AuditTrail_Save("CHART OF ACCOUNTS", "Update " & txtAccountCode.Text & " | " & txtAccountCode.Text)
            End If
        Else
            ' This is for new account
            UpdateAccount()
            AuditTrail_Save("CHART OF ACCOUNTS", "Add new account " & txtAccountCode.Text & " | " & txtAccountCode.Text)
        End If
    End Sub

    Private Sub DebitCreditAccount(ByVal AccntId As String)
        Dim isDebitCredit As String
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "ChartOfAccounts_DebitCredit",
                                New SqlParameter("@compID", gCompanyID),
                                New SqlParameter("@Acnt_Type", AccntId))
        With rd.Read
            isDebitCredit = rd.Item("fcMotherAccount")
        End With
        If isDebitCredit = "Assets" Or isDebitCredit = "Expenses" Then
            chDebit.Checked = True
        Else
            chDebit.Checked = False
        End If
        rd.Close()
    End Sub
End Class