Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class frmChartOfAccounts

    Private gCon As New Clsappconfiguration
    Private ds As New DataSet

    Private rowIndex As Integer
    Private _AcntToBeZoom As String = ""
    Private ZoomIndex As Integer = 0
    Private _DisplayCOAVerificationForm As Boolean = False
    Private COAVerificationForm As frmCOAVerification

    Public Property GetRowNumber() As Integer
        Get
            Return rowIndex
        End Get
        Set(ByVal value As Integer)
            rowIndex = value
        End Set
    End Property
    Public Property AcntToBeZoom() As String
        Get
            Return _AcntToBeZoom
        End Get
        Set(ByVal value As String)
            _AcntToBeZoom = value
        End Set
    End Property
    Public Property DisplayCOAVerificationForm As Boolean
        Get
            Return _DisplayCOAVerificationForm
        End Get
        Set(ByVal value As Boolean)
            _DisplayCOAVerificationForm = value
            If _DisplayCOAVerificationForm = True Then
                Dim xForm As New frmCOAVerification
                xForm.StartPosition = FormStartPosition.CenterScreen
                xForm.TopMost = True
                xForm.IsLoadByCOA = True
                xForm.ParentCOAForm = Me
                COAVerificationForm = xForm
                xForm.Show()
            End If
        End Set
    End Property

    Public Sub ZoomToAccount(ByVal KeyAcnt As String)
        For Each xRow As DataGridViewRow In gridChartOfAccounts.Rows
            If xRow.Cells("acnt_id").Value.ToString() = KeyAcnt Then
                ZoomIndex = xRow.Index
                AcntToBeZoom = ""
                gridChartOfAccounts.FirstDisplayedScrollingRowIndex = 0
                TimZoomer.Enabled = True
                Exit For
            End If
        Next
    End Sub

    Public Sub gDeleteAccounts(ByVal AccountID As String)
        Dim sSqlCmd As String = "usp_t_CheckAccountUsage @fxKeyCompany = '" & gCompanyID() & "', @fxKeyAccount = '" & AccountID & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    MsgBox("Account is in use and cannot be deleted to avoid data errors and lost.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Action cancelled")
                Else
                    Dim Response As String
                    Response = MsgBox("Are you sure you want to delete this account?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Warning")
                    If Response = vbYes Then
                        Try
                            Dim sSQL As String = "delete from dbo.mAccounts where acnt_id = '" & AccountID & "' and co_id = '" & gCompanyID() & "'"
                            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQL)
                            MsgBox("Account successfully deleted!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted")
                            AuditTrail_Save("CHART OF ACCOUNTS", "Add new account " & frmChartOfAccounts_Edit.txtAccountCode.Text & " | " & frmChartOfAccounts_Edit.txtAccountCode.Text)
                            LoadChartOfAccountsToGrid()
                        Catch ex As Exception
                            MsgBox("Error at gDeleteAccounts in frmChartOfAccounts" & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Action terminated")
                        End Try
                    End If
                End If
            End Using
        Catch ex As Exception
            MsgBox("Error at gDeleteAccounts in frmChartOfAccounts" & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Action terminated")
        End Try
    End Sub

    Private Sub frmChartOfAccounts_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If COAVerificationForm IsNot Nothing Then
            COAVerificationForm.Close()
        End If
    End Sub

    Private Sub frmChartOfAccounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadChartOfAccountsToGrid()
        GetRowNumber() = 0

        If AcntToBeZoom <> "" Then
            ZoomToAccount(AcntToBeZoom)
        End If
    End Sub

    Private Sub SelectPreviousAccount(ByVal rowNumber As Integer)
        Try
            With gridChartOfAccounts
                .FirstDisplayedScrollingRowIndex = gridChartOfAccounts.Rows(rowNumber).Index
                .Refresh()
                .CurrentCell = gridChartOfAccounts.Rows(rowNumber).Cells(5)
                .Rows(rowNumber).Selected = True
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Function GetChartOfAccountsFromSQL() As DataSet
        Dim storedProcedure As String = "Masterfile_ChartOfAccounts"
        Dim companyID As String = gCompanyID()
        Dim ChartOfAccounts As New DataSet()

        Try
            ChartOfAccounts = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                    New SqlParameter("@compID", companyID))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Masterfile: Chart of Accounts")
        End Try

        Return ChartOfAccounts
    End Function

    Public Sub LoadChartOfAccountsToGrid()
        With gridChartOfAccounts
            ds = GetChartOfAccountsFromSQL()
            .DataSource = ds.Tables(0)
        End With

        Call FormatChartOfAccountDataGrid()
    End Sub

    Private Sub FormatChartOfAccountDataGrid()
        With gridChartOfAccounts
            .Columns("acnt_id").Visible = False
            .Columns("compID_Accounts").Visible = False
            .Columns("compID_type").Visible = False
            .Columns("IsAccountSub").Visible = False
            .Columns("Account SubOf").Visible = False

            .Columns("Account Code").Width = 120
            .Columns("Account Name").Width = 300
            .Columns("Account Name").HeaderText = "Account Title"
            .Columns("acnt_type").Width = 200
            .Columns("Level").Width = 45
            '.Columns("Active").Width = 45
            .Columns("Debit").Width = 45

            .Columns("Level").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Account Code").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("acnt_type").HeaderText = "Account Type"

            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub

    Private Sub gridChartOfAccounts_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridChartOfAccounts.CellClick
        GetRowNumber() = gridChartOfAccounts.CurrentRow.Index

        'Dim filePath As String = "C:\test.txt"
        'GenerateListToTextFile(filePath)
        'OpenTextFile(filePath)
    End Sub

    Private Sub GenerateListToTextFile(ByVal filePath As String)
        Dim TextFile As New StreamWriter(filePath)
        TextFile.Flush()

        For Each row As DataGridViewRow In gridChartOfAccounts.Rows
            TextFile.WriteLine(row.Cells("Account Name").Value.ToString())
        Next

        TextFile.Close()
    End Sub

    Private Sub OpenTextFile(ByVal filePath As String)
        If (System.IO.File.Exists(filePath)) Then
            System.Diagnostics.Process.Start(filePath)
        End If
    End Sub

    Private Sub EditAccount()
        Dim xForm As New frmChartOfAccounts_Edit
        xForm.getMode() = "Edit"
        xForm.GetAccountID() = gridChartOfAccounts.CurrentRow.Cells("acnt_id").Value.ToString()
        xForm.ShowDialog()

        If COAVerificationForm IsNot Nothing Then
            COAVerificationForm.LoadAccounts()
        End If
    End Sub

    Private Sub EditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditToolStripMenuItem.Click
        Call EditAccount()
    End Sub

    Private Sub gridChartOfAccounts_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridChartOfAccounts.CellDoubleClick
        Call EditAccount()
        GetRowNumber() = gridChartOfAccounts.CurrentRow.Index
    End Sub

    Private Sub EditAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditAccountToolStripMenuItem.Click
        Call EditAccount()
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        Dim xForm As New frmChartOfAccounts_Edit
        xForm.getMode() = "New"
        xForm.ShowDialog()
    End Sub

    Private Sub NewAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewAccountToolStripMenuItem.Click
        Dim xForm As New frmChartOfAccounts_Edit
        xForm.getMode = "New"
        xForm.ShowDialog()
    End Sub

    Private Sub DeleteAccountsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteAccountsToolStripMenuItem.Click
        DeleteAccounts()
    End Sub

    Private Sub DeleteAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteAccountToolStripMenuItem.Click
        DeleteAccounts()
    End Sub

    Private Sub DeleteAccounts()
        Dim accountID As String = gridChartOfAccounts.Item("acnt_id", gridChartOfAccounts.CurrentRow.Index).Value.ToString
        'If ValidateAccount(accountID) = True Then
        '    MsgBox("We cannot delete this account." & vbCrLf & _
        '        "Account is still active." & vbCrLf & _
        '        "Please re-configure account in Accounts Configuration.", _
        '        MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Warning")
        'Else
        If gridChartOfAccounts.SelectedRows.Count = 1 Then
            gDeleteAccounts(accountID)
        End If
        'End Ifs
    End Sub

    Private Function ValidateAccount(ByVal accountID As String) As Boolean
        Try
            Dim IsAccountExistInConfig As Boolean
            Dim sSQLCmd As String = "usp_Integ_ValidateAccount "
            sSQLCmd &= "@accountID='" & accountID & "'"

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    IsAccountExistInConfig = rd.Item(0).ToString
                End If
            End Using
            Return IsAccountExistInConfig
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Error")
        End Try
    End Function

    Private Sub gridChartOfAccounts_DataSourceChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridChartOfAccounts.DataSourceChanged
        SelectPreviousAccount(GetRowNumber())
    End Sub

   
    Private Sub txtFind_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFind.KeyPress
        Dim dv As DataView = New DataView()

        dv.Table = ds.Tables(0)
        dv.RowFilter = "[Account Name] like '%" & txtFind.Text & "%'"

        gridChartOfAccounts.DataSource = dv
        Call FormatChartOfAccountDataGrid()
    End Sub
    Private Sub TimZoomer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimZoomer.Tick
        If gridChartOfAccounts.FirstDisplayedScrollingRowIndex < ZoomIndex Then
            gridChartOfAccounts.FirstDisplayedScrollingRowIndex = gridChartOfAccounts.FirstDisplayedScrollingRowIndex + 1
            gridChartOfAccounts.Rows((gridChartOfAccounts.FirstDisplayedScrollingRowIndex)).Selected = True
        Else
            TimZoomer.Enabled = False
        End If
    End Sub

    Private Sub PrintToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintToolStripMenuItem.Click
        pCallCompany(frmChartofAccountList)
    End Sub

    Private Sub gridChartOfAccounts_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridChartOfAccounts.CellContentClick

    End Sub
End Class