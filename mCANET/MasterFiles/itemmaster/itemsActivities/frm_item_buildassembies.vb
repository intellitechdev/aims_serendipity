Public Class frm_item_buildassembies

    Private Sub frm_item_buildassembies_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadItemtobeAssembled()
    End Sub

    Private Sub loadItemtobeAssembled()
        With grdItemAssemble
            .Columns.Add("cComponent", "Componenet Item")
            .Columns.Add("cDesc", "Description")
            .Columns.Add("cType", "Type")
            .Columns.Add("cQtyonHand", "Qty On Hand")
            .Columns.Add("cQtyNeed", "Qty On Hand")

            .Columns(0).Width = 80
            .Columns(1).Width = 130
            .Columns(2).Width = 50
            .Columns(3).Width = 100
            .Columns(4).Width = 100
        End With
    End Sub

End Class