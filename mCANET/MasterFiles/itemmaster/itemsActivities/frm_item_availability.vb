Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_item_availability
    Private gCon As New Clsappconfiguration

    Private sKeyItem As String
    Private sMode As String
    Private sKey As String = ""

    Private pLocShow As New Point(539, 455)
    Private pLocHide As New Point(539, 236)
    Private pSizeShow As New Size(643, 518)
    Private pSizeHide As New Size(643, 298)
    
    Public Property KeyItem() As String
        Get
            Return sKeyItem
        End Get
        Set(ByVal value As String)
            sKeyItem = value
        End Set
    End Property

    Private Sub frm_item_availability_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadItem()
        With cboDetails
            .Items.Clear()
            .Items.Add("Purchase Orders")
            .Items.Add("Sales Orders")
            .SelectedIndex = 1
        End With
        btnShow.Text = "Show Details >>"
        showHide(False)
    End Sub

    Private Sub unitSettings(ByVal bVisible As Boolean)
        If bVisible Then
            lblUnit.Visible = True
            lblUnitAssemblies.Visible = True
            lblUnitAvailable.Visible = True
            lblUnitOnHand.Visible = True
            lblUnitSO.Visible = True
            lblUnitPO.Visible = True
        Else
            lblUnit.Visible = False
            lblUnitAssemblies.Visible = False
            lblUnitAvailable.Visible = False
            lblUnitOnHand.Visible = False
            lblUnitSO.Visible = False
            lblUnitPO.Visible = False
        End If
    End Sub

    Private Sub loadItem()
        Dim dtItem As DataTable = m_GetItem().Tables(0)
        cboItem.DataSource = dtItem.DefaultView
        cboItem.DisplayMember = "fcItemName"
        cboItem.ValueMember = "fxKeyItem"
        cboItem.SelectedItem = sKeyItem

        loadItemIfo()
    End Sub

    Private Sub loadItemIfo()
        Dim dtItem As DataTable
        Dim sUnit As String = ""
        Try

            If sKeyItem <> "" Then
                dtItem = m_GetItem(sKeyItem).Tables(0)
                cboItem.SelectedValue = dtItem.Rows(0)("fxKeyItem")

                txtDescription.Text = dtItem.Rows(0)("fcItemDescription")
                txtOnHand.Text = dtItem.Rows(0)("fdOnHand")
                txtOnSO.Text = dtItem.Rows(0)("fdQuantityOnSO")
                txtOnPO.Text = dtItem.Rows(0)("fdQuantityOnPO")
                sUnit = dtItem.Rows(0)("fcUnitAbbreviation")
            Else
                txtDescription.Text = ""
                txtOnHand.Text = "0.00"
                txtOnSO.Text = "0.00"
                txtOnPO.Text = "0.00"
                sUnit = ""
            End If

            If sUnit <> "" Then
                lblUnit.Text = sUnit
                lblUnitAssemblies.Text = sUnit
                lblUnitAvailable.Text = sUnit
                lblUnitOnHand.Text = sUnit
                lblUnitSO.Text = sUnit
                lblUnitPO.Text = sUnit

                unitSettings(True)
            Else
                unitSettings(False)
            End If


            txtQuantity.Text = FormatNumber(txtOnHand.Text - txtOnSO.Text, 2, , , TriState.False)

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Item Info.")
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cboItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboItem.SelectedIndexChanged
        If cboItem.Text <> "" Then
            If Not cboItem.SelectedValue.ToString = "System.Data.DataRowView" Then
                sKeyItem = cboItem.SelectedValue.ToString
                If sKeyItem <> "System.Data.DataRowView" Then
                    loadItemIfo()
                    loadDetails(sMode)
                End If
            End If
        End If
    End Sub

    Private Sub cboDetails_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDetails.SelectedIndexChanged
        sMode = cboDetails.SelectedItem
        loadDetails(sMode)
    End Sub

    Public Sub loadDetails(ByVal sModes As String)
        If sKeyItem <> "" Then
            If sModes = "Purchase Orders" Then
                With grdDetails
                    .DataSource = loadPO.Tables(0).DefaultView
                    .Columns(0).Visible = False
                    .Columns(1).HeaderText = "P.O. No."
                    .Columns(2).HeaderText = "Date"
                    .Columns(2).DefaultCellStyle.Format = "MM/dd/yyyy"
                    .Columns(3).HeaderText = "Expected Date"
                    .Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                    .Columns(4).HeaderText = "Supplier Name"
                    '.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns(5).HeaderText = "Quantity"
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns(6).HeaderText = "Unit"

                End With
            End If
            If sModes = "Sales Orders" Then
                With grdDetails
                    .DataSource = loadSO.Tables(0).DefaultView
                    .Columns(0).Visible = False
                    .Columns(1).HeaderText = "S.O. No."
                    .Columns(2).HeaderText = "Date"
                    .Columns(2).DefaultCellStyle.Format = "MM/dd/yyyy"
                    .Columns(3).HeaderText = "Customer Name"
                    .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns(4).HeaderText = "Quanity"
                    .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns(5).HeaderText = "Unit"
                End With
            End If
        End If
    End Sub

    Private Function loadPO() As DataSet
        Dim sSQlCmd As String = "SELECT tPurchaseOrder.fxKeyPO,fcPoNo, fdDateTransact, fdDateDeliver, "
        sSQlCmd &= "fcSupplierName, tPurchaseOrder_item.fdQuantity, fcUnitAbbreviation "
        sSQlCmd &= "FROM tPurchaseOrder INNER JOIN tPurchaseOrder_item ON "
        sSQlCmd &= "tPurchaseOrder.fxKeyPO = tPurchaseOrder_item.fxKeyPO "
        sSQlCmd &= "LEFT JOIN mSupplier00Master ON "
        sSQlCmd &= "mSupplier00Master.fxKeySupplier = tPurchaseOrder.fxKeySupplier "
        sSQlCmd &= "INNER JOIN mItem00Master ON "
        sSQlCmd &= "mItem00Master.fxKeyItem = tPurchaseOrder_item.fxKeyItem "
        sSQlCmd &= "LEFT JOIN mItme04MeasurementUsed ON "
        sSQlCmd &= "mItme04MeasurementUsed.fxKeyUnitUsed = mItem00Master.fxKeyUnitUsed "
        sSQlCmd &= "WHERE tPurchaseOrder_item.fxKeyItem = '" & sKeyItem & "' "
        sSQlCmd &= "ORDER BY fcPoNo"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQlCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load P.O.")
            Return Nothing
        End Try
    End Function

    Private Function loadSO() As DataSet
        Dim sSQLCmd As String = "SELECT tSalesOrder.fxKeySO, fcSONo , fdDateTransact, fcCustomerName, "
        sSQLCmd &= "tSalesOrder_Item.fdQuantity, fcUnitAbbreviation "
        sSQLCmd &= "FROM tSalesOrder INNER JOIN tSalesOrder_Item ON "
        sSQLCmd &= "tSalesOrder.fxKeySO = tSalesOrder_Item.fxKeySO "
        sSQLCmd &= "INNER JOIN mCustomer00Master ON "
        sSQLCmd &= "mCustomer00Master.fxKeyCustomer = tSalesOrder.fxKeyCustomer "
        sSQLCmd &= "INNER JOIN mItem00Master "
        sSQLCmd &= "ON mItem00Master.fxKeyItem = tSalesOrder_Item.fxKeyItem "
        sSQLCmd &= "LEFT JOIN mItme04MeasurementUsed ON "
        sSQLCmd &= "mItme04MeasurementUsed.fxKeyUnitUsed = mItem00Master.fxKeyUnitUsed "
        sSQLCmd &= "WHERE tSalesOrder_Item.fxKeyItem = '" & sKeyItem & "' "
        sSQLCmd &= "ORDER BY fcSONo"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load S.O.")
            Return Nothing
        End Try
    End Function

    Private Sub grdDetails_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDetails.CellClick
        If grdDetails.SelectedCells.Count > 0 Then
            sKey = grdDetails.CurrentRow.Cells(0).Value.ToString
        End If
    End Sub

    Private Sub grdDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDetails.DoubleClick
        If grdDetails.SelectedCells.Count > 0 Then
            sKey = grdDetails.CurrentRow.Cells(0).Value.ToString

            If sMode = "Purchase Orders" Then
                frm_vend_CreatePurchaseOrder.KeyPO = sKey
                frm_vend_CreatePurchaseOrder.Text = "Edit Purchase Order"
                frm_vend_CreatePurchaseOrder.ShowDialog()
            ElseIf sMode = "Sales Orders" Then
                frm_cust_CreateSalesOrder.KeySO = sKey
                frm_cust_CreateSalesOrder.Text = "Edit Sales Order"
                frm_cust_CreateSalesOrder.ShowDialog()
            End If
        End If
    End Sub

    Private Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        If btnShow.Text = "Show Details >>" Then
            btnShow.Text = "Hide Details <<"
            showHide(True)
        Else
            btnShow.Text = "Show Details >>"
            showHide(False)
        End If
    End Sub

    Private Sub showHide(ByVal bSHow As Boolean)

        If bSHow Then
            Me.Size = pSizeShow
            btnClose.Location = pLocShow
        Else
            Me.Size = pSizeHide
            btnClose.Location = pLocHide
        End If
    End Sub
End Class