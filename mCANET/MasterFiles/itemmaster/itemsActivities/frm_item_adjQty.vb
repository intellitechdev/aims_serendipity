Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_item_adjQty
    Private gCon As New Clsappconfiguration

    Const cKeyInventoryItem = 0
    Const cKeyItem = 1
    Const cName = 2
    Const cDescription = 3
    Const cOnHand = 4
    Const cOnHandNew = 5
    Const cDifferenceQty = 6
    Const cDifferenceValue = 7
    Const cValue = 8
    Const cValueNew = 9
    Const cAvgCost = 10
    Const cUnit = 11
    Const cUpdate = 12

    Private sKeyInventory As String
    Private sKeyCustomer As String
    Private sKeyAcct As String
    Private bUpdate As Boolean

    Public Property KeyInventory()
        Get
            Return sKeyInventory
        End Get
        Set(ByVal value)
            sKeyInventory = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_item_adjQty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm
    End Sub

    Private Sub refreshForm()
        If sKeyInventory = "" Or sKeyInventory = Nothing Then
            sKeyInventory = Guid.NewGuid.ToString
        End If

        m_loadCustomer(cboCustomerName, cboCustomerID)
        loadInventory()
        grdSettings()
        chkSettings()

    End Sub

    Private Sub loadInventory()
        Dim dtInventory As DataSet = m_GetInventory(sKeyInventory)

        Try
            Using rd As DataTableReader = dtInventory.CreateDataReader

                If rd.Read Then
                    sKeyCustomer = rd.Item("fxKeyCustomer").ToString
                    sKeyAcct = rd.Item("fxKeyAccount").ToString
                    txtRefNo.Text = rd.Item("fcRefNo")
                    dteAdjustment.Value = rd.Item("fdDate")
                    txtMemo.Text = rd.Item("fcMemo")
                    chkAdj.Checked = rd.Item("fbValueAdj")
                    txtValue.Text = rd.Item("fdTotalValueAdj")
                Else
                    sKeyCustomer = ""
                    sKeyAcct = ""
                    txtRefNo.Text = defaultInvNo()
                    dteAdjustment.Value = Now
                    txtMemo.Text = ""
                    chkAdj.Checked = False
                    txtValue.Text = "0.00"
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    Private Sub grdSettings()
        With grdAdj
            .DataSource = loadItems().Tables(0).DefaultView

            .Columns(cName).ReadOnly = True
            .Columns(cDescription).ReadOnly = True
            .Columns(cOnHand).ReadOnly = True
            .Columns(cValue).ReadOnly = True
            .Columns(cUnit).ReadOnly = True

            .Columns(cName).DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns(cDescription).DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns(cOnHand).DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns(cValue).DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns(cAvgCost).DefaultCellStyle.BackColor = Color.Gainsboro
            .Columns(cUnit).DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns(cOnHandNew).ReadOnly = False
            .Columns(cDifferenceQty).ReadOnly = False
            .Columns(cValueNew).ReadOnly = False

            .Columns(cName).HeaderText = "Item"
            .Columns(cDescription).HeaderText = "Description"
            .Columns(cOnHand).HeaderText = "Current Qty"
            .Columns(cOnHandNew).HeaderText = "New Qty"
            .Columns(cDifferenceQty).HeaderText = "Qty Difference"
            .Columns(cValue).HeaderText = "Current Value"
            .Columns(cValueNew).HeaderText = "New Value"
            .Columns(cAvgCost).HeaderText = "Avg Cost"
            .Columns(cUnit).HeaderText = "Unit"

            .Columns(cKeyItem).Visible = False
            .Columns(cKeyInventoryItem).Visible = False
            .Columns(cAvgCost).Visible = False
            .Columns(cDifferenceValue).Visible = False
            .Columns(cUpdate).Visible = False
        End With
    End Sub

    Private Sub chkSettings()
        With grdAdj
            If chkAdj.Checked Then
                .Columns(cDifferenceQty).Visible = False
                .Columns(cValue).Visible = True
                .Columns(cValueNew).Visible = True
            Else
                .Columns(cDifferenceQty).Visible = True
                .Columns(cValue).Visible = False
                .Columns(cValueNew).Visible = False
            End If
        End With
    End Sub

    Private Function defaultInvNo() As String
        Dim sDefault As String = "1"
        Dim sInvNoFetch As String = ""
        Dim sInvNo() As String = {""}
        Dim sINvNosTemp As String = ""
        Dim i As Integer = 0

        Dim sSCLCmd As String = "SELECT fcRefNo FROM tInventory ORDER BY fcRefNo"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    If IsNumeric(rd.Item("fcRefNo")) Then
                        sInvNoFetch = rd.Item("fcRefNo")
                        sINvNosTemp &= sInvNoFetch & " "
                    End If
                End While
            End Using

            sInvNo = sINvNosTemp.Split
            If UBound(sInvNo) > 0 Then
                For i = 0 To UBound(sInvNo)
                    If CStr(i + 1) <> sInvNo(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sInvNo(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next

            End If
            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Inventory No.")
            Return sDefault
        End Try
    End Function

    Private Function loadItems() As DataSet
        Dim sSQLCmd As String = "SELECT * FROM ("
        sSQLCmd &= "SELECT tInventory_item.fxKeyInventoryItem, "
        sSQLCmd &= "mItem00Master.fxKeyItem, "
        sSQLCmd &= "mItem00Master.fcItemName, "
        sSQLCmd &= "mItem00Master.fcItemDescription, "
        sSQLCmd &= "mItem00Master.fdOnHand, "
        sSQLCmd &= "mItem00Master.fdOnHand as fdOnHandNew, 0.00 as fdDiffQty ,0.00 as fdDiffVal, "
        sSQLCmd &= "mItem00Master.fdTotalValue, "
        sSQLCmd &= "mItem00Master.fdTotalValue as fdTotalValueNew, "
        sSQLCmd &= "mItem00Master.fdAvgCost, "
        sSQLCmd &= "fcUnitAbbreviation, 0 as fbUpdate "
        sSQLCmd &= "FROM mItem00Master "
        sSQLCmd &= "LEFT JOIN mItme04MeasurementUsed "
        sSQLCmd &= "ON mItem00Master.fxKeyUnitUsed = mItme04MeasurementUsed.fxKeyUnitUsed "
        sSQLCmd &= "LEFT JOIN tInventory_item "
        sSQLCmd &= "ON tInventory_item.fxKeyItem = mItem00Master.fxKeyItem "
        sSQLCmd &= "WHERE fxKeyInventory = '" & sKeyInventory & "'"
        sSQLCmd &= "UNION "
        sSQLCmd &= "SELECT null as fxKey, "
        sSQLCmd &= "mItem00Master.fxKeyItem, "
        sSQLCmd &= "mItem00Master.fcItemName, "
        sSQLCmd &= "mItem00Master.fcItemDescription, "
        sSQLCmd &= "mItem00Master.fdOnHand, "
        sSQLCmd &= "mItem00Master.fdOnHand, 0,0, "
        sSQLCmd &= "mItem00Master.fdTotalValue, "
        sSQLCmd &= "mItem00Master.fdTotalValue, "
        sSQLCmd &= "mItem00Master.fdAvgCost, "
        sSQLCmd &= "fcUnitAbbreviation, 0 "
        sSQLCmd &= "FROM mItem00Master "
        sSQLCmd &= "LEFT JOIN mItme04MeasurementUsed "
        sSQLCmd &= "ON mItem00Master.fxKeyUnitUsed = mItme04MeasurementUsed.fxKeyUnitUsed "
        sSQLCmd &= "WHERE fxKeyItem NOT IN (SELECT fxKeyItem "
        sSQLCmd &= "FROM tInventory_item "
        sSQLCmd &= "WHERE fxKeyInventory = '" & sKeyInventory & "')) as q "
        sSQLCmd &= "ORDER BY q.fcItemName"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Items")
            Return Nothing
        End Try
    End Function


    'Private Sub loadItemQtyAdj()
    '    With grdAdj
    '        .Columns.Add("cItem", "Item")
    '        .Columns.Add("cDesc", "Description")
    '        .Columns.Add("cCurQty", "Current Qty")
    '        .Columns.Add("cNewQty", "New Qty")
    '        .Columns.Add("cQtyDif", "Qty Difference")

    '        .Columns(0).Width = 100
    '        .Columns(1).Width = 100
    '        .Columns(2).Width = 80
    '        .Columns(3).Width = 80
    '        .Columns(4).Width = 80
    '    End With
    'End Sub


    Private Sub chkAdj_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAdj.CheckedChanged
        chkSettings()
    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedIndex
        If cboCustomerName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddEdit.Show()
        End If
    End Sub

    Private Sub cboAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccount.SelectedIndexChanged
        sKeyAcct = cboAccount.SelectedItem.ToString
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        updateInventory()
    End Sub

    Private Sub updateInventory()
        Dim sSQLCmd As String = "usp_t_inventory_update "
        sSQLCmd &= "@fxKeyInventory = '" & sKeyInventory & "'"
        sSQLCmd &= ",@fcRefNo = '" & txtRefNo.Text & "'"
        sSQLCmd &= ",@fdDate = '" & dteAdjustment.Value & "'"
        sSQLCmd &= ",@fcMemo = '" & txtMemo.Text & "'"
        sSQLCmd &= ",@fdTotalValueAdj = " & txtValue.Text
        sSQLCmd &= ",@fbValueAdj = " & IIf(chkAdj.Checked = True, 1, 0)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"

        If sKeyAcct <> "" Then
            sSQLCmd &= ",@fxKeyAccount = '" & sKeyAcct & "'"
        End If

        If sKeyCustomer <> "" Then
            sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"
        End If

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            If updateInventoryItem() Then
                If bUpdate Then
                    m_ItemList(frm_item_masterItem.chkIncludeInactive.CheckState, frm_item_masterItem.grdItems)
                    MessageBox.Show("Record successfully Updated.", "Adjust Inventory")
                Else
                    sSQLCmd = "DELETE FROM tInventory WHERE fxKeyInventory = '" & sKeyInventory & "'"
                    SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                End If
                Me.Close()
            Else
                sSQLCmd = "DELETE FROM tInventory WHERE fxKeyInventory = '" & sKeyInventory & "'"
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Cannot update this record!" & vbCrLf & "Please check the Inventory Items.", "Adjust Inventory")
                Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Adjust Inventory")
        End Try
    End Sub

    Private Function updateInventoryItem() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Dim sKeyInventoryItem As String
        Dim sItem As String
        Dim dOnHand As Decimal
        Dim dOnHandNew As Decimal
        Dim dValue As Decimal
        Dim dValueNew As Decimal
        Dim dAvgCost As Decimal
        Dim bUpdateTemp As Boolean

        Try
            bUpdate = False
            If Me.grdAdj.Rows.Count >= 1 Then
                For iRow = 0 To (grdAdj.RowCount - 2)
                    If mkDefaultValues(iRow, cKeyInventoryItem) <> Nothing Then
                        sKeyInventoryItem = grdAdj.Rows(iRow).Cells(cKeyInventoryItem).Value.ToString
                    Else
                        sKeyInventoryItem = Guid.NewGuid.ToString
                    End If

                    If mkDefaultValues(iRow, cKeyItem) <> Nothing Then
                        sItem = grdAdj.Rows(iRow).Cells(cKeyItem).Value.ToString
                    Else
                        sItem = ""
                    End If

                    If mkDefaultValues(iRow, cOnHand) <> Nothing Then
                        dOnHand = grdAdj.Rows(iRow).Cells(cOnHand).Value
                    Else
                        dOnHand = 0
                    End If

                    If mkDefaultValues(iRow, cOnHandNew) <> Nothing Then
                        dOnHandNew = grdAdj.Rows(iRow).Cells(cOnHandNew).Value
                    End If

                    If mkDefaultValues(iRow, cValue) <> Nothing Then
                        dValue = grdAdj.Rows(iRow).Cells(cValue).Value
                    Else
                        dValue = 0
                    End If

                    If mkDefaultValues(iRow, cValueNew) <> Nothing Then
                        dValueNew = grdAdj.Rows(iRow).Cells(cValueNew).Value
                    Else
                        dValueNew = 0
                    End If

                    If mkDefaultValues(iRow, cAvgCost) <> Nothing Then
                        dAvgCost = grdAdj.Rows(iRow).Cells(cAvgCost).Value
                    Else
                        dAvgCost = 0
                    End If

                    If mkDefaultValues(iRow, cUpdate) <> Nothing Then
                        bUpdateTemp = grdAdj.Rows(iRow).Cells(cUpdate).Value
                    Else
                        bUpdateTemp = False
                    End If

                    If bUpdateTemp = True Then
                        bUpdate = True
                        sSQLCmd = "usp_t_inventoryItem_update "
                        sSQLCmd &= "@fxKeyInventoryItem = '" & sKeyInventoryItem & "'"
                        sSQLCmd &= ",@fxKeyInventory = '" & sKeyInventory & "'"
                        sSQLCmd &= ",@fxKeyItem = '" & sItem & "'"
                        sSQLCmd &= ",@fdOnHand = " & dOnHand
                        sSQLCmd &= ",@fdOnHandAdj = " & dOnHandNew
                        sSQLCmd &= ",@fdTotalValue = " & dValue
                        sSQLCmd &= ",@fdTotalValueAdj = " & dValueNew
                        sSQLCmd &= ",@fdAvgCost = " & dAvgCost

                        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

                        sSQLCmd = "UPDATE mItem00Master SET fdOnHand = " & dOnHandNew
                        sSQLCmd &= ",fdAvgCost = " & dAvgCost
                        sSQLCmd &= ",fdTotalValue = " & dValueNew
                        sSQLCmd &= " WHERE fxKeyItem = '" & sItem & "'"

                        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

                    End If
                Next
                Return True
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Adjust Inventory Item")
            Return False
        End Try

    End Function

    Private Sub grdAdj_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAdj.CellValueChanged
        If e.RowIndex >= 0 Then
            Try
                Dim dQuantity As Decimal = 0
                Dim dQuantityNew As Decimal = 0
                Dim dValue As Decimal = 0
                Dim dValueNew As Decimal = 0
                Dim dAvgCost As Decimal = 0
                Dim dDifferenceQty As Decimal = 0
                Dim dDifferenceVal As Decimal = 0

                If mkDefaultValues(grdAdj.CurrentRow.Index, cOnHand) <> Nothing Then
                    dQuantity = grdAdj.CurrentRow.Cells(cOnHand).Value
                End If

                If mkDefaultValues(grdAdj.CurrentRow.Index, cValue) <> Nothing Then
                    dValue = grdAdj.CurrentRow.Cells(cValue).Value
                End If

                If mkDefaultValues(grdAdj.CurrentRow.Index, cOnHandNew) <> Nothing Then
                    dQuantityNew = grdAdj.CurrentRow.Cells(cOnHandNew).Value
                End If

                If dQuantity <> 0 Then
                    dAvgCost = dValue / dQuantity
                End If

                If e.ColumnIndex = cOnHandNew Then
                    'dQuantityNew = grdAdj.CurrentRow.Cells(cOnHandNew).Value
                    dValueNew = FormatNumber(dQuantityNew * dAvgCost, 2, , , False)
                    dDifferenceVal = FormatNumber(dValueNew - dValue, 2, , , False)
                    dDifferenceQty = FormatNumber(dQuantityNew - dQuantity, 2, , , False)

                    grdAdj.CurrentRow.Cells(cValueNew).Value = dValueNew
                    grdAdj.CurrentRow.Cells(cDifferenceQty).Value = dDifferenceQty
                    grdAdj.CurrentRow.Cells(cDifferenceValue).Value = dDifferenceVal
                    grdAdj.CurrentRow.Cells(cAvgCost).Value = dAvgCost
                    If dValue <> dValueNew Or dQuantity <> dQuantityNew Then
                        grdAdj.CurrentRow.Cells(cUpdate).Value = True
                    Else
                        grdAdj.CurrentRow.Cells(cUpdate).Value = False
                    End If
                End If

                If e.ColumnIndex = cValueNew Then
                    'dQuantityNew = grdAdj.CurrentRow.Cells(cOnHandNew).Value
                    dValueNew = grdAdj.CurrentRow.Cells(cValueNew).Value
                    dAvgCost = FormatNumber(dValueNew / dQuantityNew, 2, , , False)
                    dDifferenceVal = FormatNumber(dValueNew - dValue, 2, , , False)

                    grdAdj.CurrentRow.Cells(cDifferenceValue).Value = dDifferenceVal
                    grdAdj.CurrentRow.Cells(cAvgCost).Value = dAvgCost
                    If dValue <> dValueNew Or dQuantity <> dQuantityNew Then
                        grdAdj.CurrentRow.Cells(cUpdate).Value = True
                    Else
                        grdAdj.CurrentRow.Cells(cUpdate).Value = False
                    End If
                End If

                If e.ColumnIndex = cDifferenceQty Then
                    dQuantityNew = FormatNumber(dQuantity + grdAdj.CurrentRow.Cells(cDifferenceQty).Value, 2, , , False)
                    dValueNew = FormatNumber(dQuantityNew * dAvgCost, 2, , , False)
                    dDifferenceVal = FormatNumber(dValueNew - dValue, 2, , , False)

                    grdAdj.CurrentRow.Cells(cValueNew).Value = dValueNew
                    grdAdj.CurrentRow.Cells(cDifferenceValue).Value = dDifferenceVal
                    grdAdj.CurrentRow.Cells(cOnHandNew).Value = dQuantityNew
                    If dValue <> dValueNew Or dQuantity <> dQuantityNew Then
                        grdAdj.CurrentRow.Cells(cUpdate).Value = True
                    Else
                        grdAdj.CurrentRow.Cells(cUpdate).Value = False
                    End If
                End If

                getTotal()
            Catch ex As Exception
                MessageBox.Show(Err.Description, "Inventory Item")
            End Try
        End If
    End Sub

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdAdj.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdAdj.Rows(irow).Cells(iCol).Value)

    End Function

   
    Private Sub getTotal()
        Dim iRow As Integer
        Dim dTotal As Decimal = 0
        For iRow = 0 To grdAdj.Rows.Count - 2
            dTotal = dTotal + grdAdj.Item(cDifferenceValue, iRow).Value
        Next
        txtValue.Text = Format(dTotal, "0.00")
    End Sub

End Class