Public Class frmBrand

    Private Sub btn_dep_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_saveClose.Click
        If m_saveitembrand(txtCode.Text, txtdescription.Text, Me) = "save successful..." Then
            MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
            clear()
            Me.Close()
        Else
            MsgBox("encounter an error, pls report this to your sytem administrator...", MsgBoxStyle.Information, Me.Text)
            clear()
        End If
    End Sub

    Private Sub btn_item_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_item_cancel.Click
        clear()
        Me.Close()
    End Sub

    Private Sub clear()
        txtCode.Text = ""
        txtdescription.Text = ""
    End Sub

End Class