Public Class frmItemCategory

    Private svalidate As Integer = 0

    Private Sub frmItemCategory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadgrid()
    End Sub

    Private Sub grdCategory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCategory.CellClick
        svalidate = 1
        With grdCategory
            txtCode.Text = .CurrentRow.Cells(0).Value.ToString
            txtdescription.Text = .CurrentRow.Cells(1).Value.ToString
        End With
        Call editvalidation()
    End Sub

    Private Sub btn_dep_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_saveClose.Click
        svalidate = 0
        If m_saveitemcategory(txtCode.Text, txtdescription.Text, Me) = "save successful..." Then
            MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
            clear()
            Me.Close()
            Me.Dispose()
        Else
            MsgBox("encounter an error, pls report this to your sytem administrator...", MsgBoxStyle.Information, Me.Text)
            clear()
        End If
    End Sub

    Private Sub btn_item_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_item_cancel.Click
        clear()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub loadgrid()
        With grdCategory
            .DataSource = m_displayitemcategory().Tables(0)
            .Columns(0).Width = 100
            .Columns(1).Width = 150
        End With
    End Sub

    Private Sub clear()
        svalidate = 0
        txtCode.Text = ""
        txtdescription.Text = ""
    End Sub

    Private Sub editvalidation()
        If svalidate = 0 Then
            txtCode.Enabled = True
        ElseIf svalidate = 1 Then
            txtCode.Enabled = False
        End If
    End Sub

End Class