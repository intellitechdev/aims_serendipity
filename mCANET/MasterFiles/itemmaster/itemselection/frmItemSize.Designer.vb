<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemSize
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmItemSize))
        Me.btn_item_cancel = New System.Windows.Forms.Button
        Me.btn_dep_saveClose = New System.Windows.Forms.Button
        Me.txtdescription = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtCode = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'btn_item_cancel
        '
        Me.btn_item_cancel.Location = New System.Drawing.Point(265, 117)
        Me.btn_item_cancel.Name = "btn_item_cancel"
        Me.btn_item_cancel.Size = New System.Drawing.Size(93, 23)
        Me.btn_item_cancel.TabIndex = 36
        Me.btn_item_cancel.Text = "Cancel"
        Me.btn_item_cancel.UseVisualStyleBackColor = True
        '
        'btn_dep_saveClose
        '
        Me.btn_dep_saveClose.Location = New System.Drawing.Point(164, 117)
        Me.btn_dep_saveClose.Name = "btn_dep_saveClose"
        Me.btn_dep_saveClose.Size = New System.Drawing.Size(93, 23)
        Me.btn_dep_saveClose.TabIndex = 35
        Me.btn_dep_saveClose.Text = "OK"
        Me.btn_dep_saveClose.UseVisualStyleBackColor = True
        '
        'txtdescription
        '
        Me.txtdescription.Location = New System.Drawing.Point(92, 58)
        Me.txtdescription.Multiline = True
        Me.txtdescription.Name = "txtdescription"
        Me.txtdescription.Size = New System.Drawing.Size(265, 40)
        Me.txtdescription.TabIndex = 34
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Description"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Item Code"
        '
        'txtCode
        '
        Me.txtCode.Location = New System.Drawing.Point(92, 16)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(144, 21)
        Me.txtCode.TabIndex = 31
        '
        'frmItemSize
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(373, 156)
        Me.Controls.Add(Me.btn_item_cancel)
        Me.Controls.Add(Me.btn_dep_saveClose)
        Me.Controls.Add(Me.txtdescription)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCode)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmItemSize"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Item Size"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_item_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_dep_saveClose As System.Windows.Forms.Button
    Friend WithEvents txtdescription As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
End Class
