Imports Microsoft.ApplicationBlocks.Data

Public Class frm_item_group
    Private sKeyGroup As String
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mItem01Group WHERE fbActive <> 1 AND fxKeyCompany = '" & gCompanyID() & "'"

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_item_groupAddEdit.MdiParent = Me.MdiParent
        frm_item_groupAddEdit.KeyGroup = Guid.NewGuid.ToString
        frm_item_groupAddEdit.Text = "Add Item Group"
        frm_item_groupAddEdit.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_item_groupAddEdit.MdiParent = Me.MdiParent
        frm_item_groupAddEdit.KeyGroup = sKeyGroup
        frm_item_groupAddEdit.Text = "Edit Item Group"
        frm_item_groupAddEdit.Show()
    End Sub

    Private Sub frm_item_group_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Private Sub refreshForm()
        m_itemGroupList(chkIncludeInactive.CheckState, grdItemGroup)
        grdSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub
    Private Sub grdSettings()
        With grdItemGroup
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns.Item(1).HeaderText = "Active"
            .Columns.Item(2).HeaderText = "Group Name"
            If chkIncludeInactive.CheckState = CheckState.Checked Then
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Visible = True
            End If
        End With
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_itemGroupList(chkIncludeInactive.CheckState, grdItemGroup)
        grdSettings()
    End Sub

    Private Sub grdTerms_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdItemGroup.CellClick
        If Me.grdItemGroup.SelectedRows.Count > 0 AndAlso _
                           Not Me.grdItemGroup.SelectedRows(0).Index = _
                           Me.grdItemGroup.Rows.Count - 1 Then
            sKeyGroup = grdItemGroup.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mItem01Group", "fxKeyItemGroup", sKeyGroup) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdItemGroup_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdItemGroup.DoubleClick
        If Me.grdItemGroup.SelectedRows.Count > 0 AndAlso _
                           Not Me.grdItemGroup.SelectedRows(0).Index = _
                           Me.grdItemGroup.Rows.Count - 1 Then
            frm_item_groupAddEdit.MdiParent = Me.MdiParent
            frm_item_groupAddEdit.KeyGroup = sKeyGroup
            frm_item_groupAddEdit.Text = "Edit Terms"
            frm_item_groupAddEdit.Show()
        End If
    End Sub

    Private Sub deleteTerms()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mItem01Group WHERE fxKeyItemGroup = '" & sKeyGroup & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        deleteTerms()
        refreshForm()
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_showInactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mItem01Group", "fxKeyItemGroup", sKeyGroup, True)
        Else
            m_mkActive("mItem01Group", "fxKeyItemGroup", sKeyGroup, False)
        End If
        refreshForm()
    End Sub
End Class