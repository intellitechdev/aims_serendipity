<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_item_masterItemAddEdit
    Inherits System.Windows.Forms.Form



    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_item_masterItemAddEdit))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbo_item_itemGroupName = New System.Windows.Forms.ComboBox
        Me.txtitemdescription = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbo_item_itemGroupID = New System.Windows.Forms.ComboBox
        Me.chkInactive = New System.Windows.Forms.CheckBox
        Me.btn_dep_saveClose = New System.Windows.Forms.Button
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.txtAvgCost = New System.Windows.Forms.TextBox
        Me.txtSO = New System.Windows.Forms.TextBox
        Me.lblOnSo = New System.Windows.Forms.Label
        Me.txtPO = New System.Windows.Forms.TextBox
        Me.lblOnPo = New System.Windows.Forms.Label
        Me.cboAssetAcnt = New System.Windows.Forms.ComboBox
        Me.Label64 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtReorder = New System.Windows.Forms.TextBox
        Me.txtOnhand = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.lblTotalVal = New System.Windows.Forms.Label
        Me.txtManPartNo = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.cboIncomeAcnt = New System.Windows.Forms.ComboBox
        Me.cboTax = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtSalesprice = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtItemdescsales = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cboSupplier = New System.Windows.Forms.ComboBox
        Me.cboCOGs = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtItemdescpurchase = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.TxtItemCode = New System.Windows.Forms.TextBox
        Me.cboModel = New System.Windows.Forms.ComboBox
        Me.cboBrand = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.Label78 = New System.Windows.Forms.Label
        Me.cboSizeCode = New System.Windows.Forms.ComboBox
        Me.cboColorCode = New System.Windows.Forms.ComboBox
        Me.Label77 = New System.Windows.Forms.Label
        Me.Label76 = New System.Windows.Forms.Label
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtdescription = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.cboItemCode = New System.Windows.Forms.ComboBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.btn_item_cancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbo_item_itemGroupName)
        Me.GroupBox1.Controls.Add(Me.txtitemdescription)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbo_item_itemGroupID)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 17)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(260, 139)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Group"
        '
        'cbo_item_itemGroupName
        '
        Me.cbo_item_itemGroupName.FormattingEnabled = True
        Me.cbo_item_itemGroupName.Location = New System.Drawing.Point(12, 22)
        Me.cbo_item_itemGroupName.Name = "cbo_item_itemGroupName"
        Me.cbo_item_itemGroupName.Size = New System.Drawing.Size(234, 21)
        Me.cbo_item_itemGroupName.TabIndex = 4
        '
        'txtitemdescription
        '
        Me.txtitemdescription.BackColor = System.Drawing.Color.White
        Me.txtitemdescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtitemdescription.Location = New System.Drawing.Point(12, 52)
        Me.txtitemdescription.Multiline = True
        Me.txtitemdescription.Name = "txtitemdescription"
        Me.txtitemdescription.Size = New System.Drawing.Size(234, 71)
        Me.txtitemdescription.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(395, 192)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Label1"
        '
        'cbo_item_itemGroupID
        '
        Me.cbo_item_itemGroupID.FormattingEnabled = True
        Me.cbo_item_itemGroupID.Items.AddRange(New Object() {"Discount", "Group", "Inventory Part", "Inventory Assembly", "Non-Inventory Part", "Other Charge", "Payment", "Sales Tax Item", "Sales Tax Group", "Service", "Subtotal"})
        Me.cbo_item_itemGroupID.Location = New System.Drawing.Point(12, 22)
        Me.cbo_item_itemGroupID.Name = "cbo_item_itemGroupID"
        Me.cbo_item_itemGroupID.Size = New System.Drawing.Size(188, 21)
        Me.cbo_item_itemGroupID.TabIndex = 0
        '
        'chkInactive
        '
        Me.chkInactive.AutoSize = True
        Me.chkInactive.Location = New System.Drawing.Point(811, 3)
        Me.chkInactive.Name = "chkInactive"
        Me.chkInactive.Size = New System.Drawing.Size(116, 17)
        Me.chkInactive.TabIndex = 6
        Me.chkInactive.Text = "Item is Inactive"
        Me.chkInactive.UseVisualStyleBackColor = True
        '
        'btn_dep_saveClose
        '
        Me.btn_dep_saveClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_dep_saveClose.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btn_dep_saveClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_dep_saveClose.Location = New System.Drawing.Point(759, 441)
        Me.btn_dep_saveClose.Name = "btn_dep_saveClose"
        Me.btn_dep_saveClose.Size = New System.Drawing.Size(85, 29)
        Me.btn_dep_saveClose.TabIndex = 27
        Me.btn_dep_saveClose.Text = "Save"
        Me.btn_dep_saveClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_dep_saveClose.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtAvgCost)
        Me.GroupBox6.Controls.Add(Me.txtSO)
        Me.GroupBox6.Controls.Add(Me.lblOnSo)
        Me.GroupBox6.Controls.Add(Me.txtPO)
        Me.GroupBox6.Controls.Add(Me.lblOnPo)
        Me.GroupBox6.Controls.Add(Me.cboAssetAcnt)
        Me.GroupBox6.Controls.Add(Me.Label64)
        Me.GroupBox6.Controls.Add(Me.Label18)
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Controls.Add(Me.txtReorder)
        Me.GroupBox6.Controls.Add(Me.txtOnhand)
        Me.GroupBox6.Controls.Add(Me.Label20)
        Me.GroupBox6.Controls.Add(Me.lblTotalVal)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 373)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(927, 62)
        Me.GroupBox6.TabIndex = 40
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Inventory Information"
        '
        'txtAvgCost
        '
        Me.txtAvgCost.Location = New System.Drawing.Point(446, 29)
        Me.txtAvgCost.Name = "txtAvgCost"
        Me.txtAvgCost.ReadOnly = True
        Me.txtAvgCost.Size = New System.Drawing.Size(145, 21)
        Me.txtAvgCost.TabIndex = 40
        Me.txtAvgCost.Text = "0.00"
        Me.txtAvgCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAvgCost.Visible = False
        '
        'txtSO
        '
        Me.txtSO.Location = New System.Drawing.Point(758, 29)
        Me.txtSO.Name = "txtSO"
        Me.txtSO.ReadOnly = True
        Me.txtSO.Size = New System.Drawing.Size(158, 21)
        Me.txtSO.TabIndex = 42
        Me.txtSO.Text = "0.00"
        Me.txtSO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSO.Visible = False
        '
        'lblOnSo
        '
        Me.lblOnSo.AutoSize = True
        Me.lblOnSo.Location = New System.Drawing.Point(755, 14)
        Me.lblOnSo.Name = "lblOnSo"
        Me.lblOnSo.Size = New System.Drawing.Size(95, 13)
        Me.lblOnSo.TabIndex = 41
        Me.lblOnSo.Text = "On Sales Order"
        Me.lblOnSo.Visible = False
        '
        'txtPO
        '
        Me.txtPO.Location = New System.Drawing.Point(597, 29)
        Me.txtPO.Name = "txtPO"
        Me.txtPO.ReadOnly = True
        Me.txtPO.Size = New System.Drawing.Size(156, 21)
        Me.txtPO.TabIndex = 42
        Me.txtPO.Text = "0.00"
        Me.txtPO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPO.Visible = False
        '
        'lblOnPo
        '
        Me.lblOnPo.AutoSize = True
        Me.lblOnPo.Location = New System.Drawing.Point(594, 14)
        Me.lblOnPo.Name = "lblOnPo"
        Me.lblOnPo.Size = New System.Drawing.Size(51, 13)
        Me.lblOnPo.TabIndex = 41
        Me.lblOnPo.Text = "On P.O."
        Me.lblOnPo.Visible = False
        '
        'cboAssetAcnt
        '
        Me.cboAssetAcnt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAssetAcnt.DropDownWidth = 250
        Me.cboAssetAcnt.FormattingEnabled = True
        Me.cboAssetAcnt.Location = New System.Drawing.Point(6, 29)
        Me.cboAssetAcnt.Name = "cboAssetAcnt"
        Me.cboAssetAcnt.Size = New System.Drawing.Size(265, 21)
        Me.cboAssetAcnt.TabIndex = 40
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(395, 192)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(51, 13)
        Me.Label64.TabIndex = 1
        Me.Label64.Text = "Label64"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 16)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(87, 13)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Asset Account"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(278, 14)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(85, 13)
        Me.Label19.TabIndex = 29
        Me.Label19.Text = "Reorder Point"
        '
        'txtReorder
        '
        Me.txtReorder.Location = New System.Drawing.Point(279, 29)
        Me.txtReorder.Name = "txtReorder"
        Me.txtReorder.Size = New System.Drawing.Size(76, 21)
        Me.txtReorder.TabIndex = 30
        '
        'txtOnhand
        '
        Me.txtOnhand.Location = New System.Drawing.Point(364, 29)
        Me.txtOnhand.Name = "txtOnhand"
        Me.txtOnhand.Size = New System.Drawing.Size(76, 21)
        Me.txtOnhand.TabIndex = 34
        Me.txtOnhand.Text = "0"
        Me.txtOnhand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(366, 14)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(56, 13)
        Me.Label20.TabIndex = 31
        Me.Label20.Text = "On Hand"
        '
        'lblTotalVal
        '
        Me.lblTotalVal.AutoSize = True
        Me.lblTotalVal.Location = New System.Drawing.Point(443, 14)
        Me.lblTotalVal.Name = "lblTotalVal"
        Me.lblTotalVal.Size = New System.Drawing.Size(71, 13)
        Me.lblTotalVal.TabIndex = 33
        Me.lblTotalVal.Text = "Total Value"
        Me.lblTotalVal.Visible = False
        '
        'txtManPartNo
        '
        Me.txtManPartNo.Location = New System.Drawing.Point(766, 163)
        Me.txtManPartNo.Name = "txtManPartNo"
        Me.txtManPartNo.Size = New System.Drawing.Size(158, 21)
        Me.txtManPartNo.TabIndex = 39
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(600, 166)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(142, 13)
        Me.Label23.TabIndex = 38
        Me.Label23.Text = "Manufacturer's Part Number"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cboIncomeAcnt)
        Me.GroupBox5.Controls.Add(Me.cboTax)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.txtSalesprice)
        Me.GroupBox5.Controls.Add(Me.Label16)
        Me.GroupBox5.Controls.Add(Me.txtItemdescsales)
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Location = New System.Drawing.Point(469, 188)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(466, 178)
        Me.GroupBox5.TabIndex = 18
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Sales Information"
        '
        'cboIncomeAcnt
        '
        Me.cboIncomeAcnt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboIncomeAcnt.DropDownWidth = 250
        Me.cboIncomeAcnt.FormattingEnabled = True
        Me.cboIncomeAcnt.Location = New System.Drawing.Point(115, 146)
        Me.cboIncomeAcnt.Name = "cboIncomeAcnt"
        Me.cboIncomeAcnt.Size = New System.Drawing.Size(340, 21)
        Me.cboIncomeAcnt.TabIndex = 40
        '
        'cboTax
        '
        Me.cboTax.FormattingEnabled = True
        Me.cboTax.Location = New System.Drawing.Point(115, 119)
        Me.cboTax.Name = "cboTax"
        Me.cboTax.Size = New System.Drawing.Size(340, 21)
        Me.cboTax.TabIndex = 40
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 149)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(99, 13)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Income Account"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(7, 123)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "Tax Code"
        '
        'txtSalesprice
        '
        Me.txtSalesprice.AcceptsReturn = True
        Me.txtSalesprice.Location = New System.Drawing.Point(297, 92)
        Me.txtSalesprice.Name = "txtSalesprice"
        Me.txtSalesprice.Size = New System.Drawing.Size(158, 21)
        Me.txtSalesprice.TabIndex = 28
        Me.txtSalesprice.Text = "0.00"
        Me.txtSalesprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(203, 95)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(70, 13)
        Me.Label16.TabIndex = 27
        Me.Label16.Text = "Sales Price"
        '
        'txtItemdescsales
        '
        Me.txtItemdescsales.Location = New System.Drawing.Point(18, 35)
        Me.txtItemdescsales.Multiline = True
        Me.txtItemdescsales.Name = "txtItemdescsales"
        Me.txtItemdescsales.Size = New System.Drawing.Size(437, 51)
        Me.txtItemdescsales.TabIndex = 22
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(200, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Description on Sales Transactions"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboSupplier)
        Me.GroupBox3.Controls.Add(Me.cboCOGs)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txtCost)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtItemdescpurchase)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 188)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(455, 178)
        Me.GroupBox3.TabIndex = 17
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Purchase Information"
        '
        'cboSupplier
        '
        Me.cboSupplier.FormattingEnabled = True
        Me.cboSupplier.Location = New System.Drawing.Point(122, 146)
        Me.cboSupplier.Name = "cboSupplier"
        Me.cboSupplier.Size = New System.Drawing.Size(324, 21)
        Me.cboSupplier.TabIndex = 28
        '
        'cboCOGs
        '
        Me.cboCOGs.FormattingEnabled = True
        Me.cboCOGs.Location = New System.Drawing.Point(122, 121)
        Me.cboCOGs.Name = "cboCOGs"
        Me.cboCOGs.Size = New System.Drawing.Size(324, 21)
        Me.cboCOGs.TabIndex = 27
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 149)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(112, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Preferred Supplier"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 123)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(91, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "COGS Account"
        '
        'txtCost
        '
        Me.txtCost.Location = New System.Drawing.Point(262, 95)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.Size = New System.Drawing.Size(184, 21)
        Me.txtCost.TabIndex = 22
        Me.txtCost.Text = "0.00"
        Me.txtCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(199, 98)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(33, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Cost"
        '
        'txtItemdescpurchase
        '
        Me.txtItemdescpurchase.Location = New System.Drawing.Point(6, 35)
        Me.txtItemdescpurchase.Multiline = True
        Me.txtItemdescpurchase.Name = "txtItemdescpurchase"
        Me.txtItemdescpurchase.Size = New System.Drawing.Size(440, 51)
        Me.txtItemdescpurchase.TabIndex = 20
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(221, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Description on Purchase Transactions"
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.TxtItemCode)
        Me.GroupBox12.Controls.Add(Me.cboModel)
        Me.GroupBox12.Controls.Add(Me.cboBrand)
        Me.GroupBox12.Controls.Add(Me.Label3)
        Me.GroupBox12.Controls.Add(Me.Label2)
        Me.GroupBox12.Controls.Add(Me.cboUnit)
        Me.GroupBox12.Controls.Add(Me.Label78)
        Me.GroupBox12.Controls.Add(Me.cboSizeCode)
        Me.GroupBox12.Controls.Add(Me.cboColorCode)
        Me.GroupBox12.Controls.Add(Me.Label77)
        Me.GroupBox12.Controls.Add(Me.Label76)
        Me.GroupBox12.Controls.Add(Me.cboCategory)
        Me.GroupBox12.Controls.Add(Me.Label22)
        Me.GroupBox12.Controls.Add(Me.txtdescription)
        Me.GroupBox12.Controls.Add(Me.Label21)
        Me.GroupBox12.Controls.Add(Me.cboItemCode)
        Me.GroupBox12.Controls.Add(Me.Label17)
        Me.GroupBox12.Location = New System.Drawing.Point(275, 17)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(660, 139)
        Me.GroupBox12.TabIndex = 46
        Me.GroupBox12.TabStop = False
        '
        'TxtItemCode
        '
        Me.TxtItemCode.Location = New System.Drawing.Point(118, 22)
        Me.TxtItemCode.Name = "TxtItemCode"
        Me.TxtItemCode.Size = New System.Drawing.Size(213, 21)
        Me.TxtItemCode.TabIndex = 54
        '
        'cboModel
        '
        Me.cboModel.FormattingEnabled = True
        Me.cboModel.Items.AddRange(New Object() {"Discount", "Group", "Inventory Part", "Inventory Assembly", "Non-Inventory Part", "Other Charge", "Payment", "Sales Tax Item", "Sales Tax Group", "Service", "Subtotal"})
        Me.cboModel.Location = New System.Drawing.Point(436, 107)
        Me.cboModel.Name = "cboModel"
        Me.cboModel.Size = New System.Drawing.Size(213, 21)
        Me.cboModel.TabIndex = 53
        '
        'cboBrand
        '
        Me.cboBrand.FormattingEnabled = True
        Me.cboBrand.Location = New System.Drawing.Point(118, 107)
        Me.cboBrand.Name = "cboBrand"
        Me.cboBrand.Size = New System.Drawing.Size(213, 21)
        Me.cboBrand.TabIndex = 52
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(363, 110)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 51
        Me.Label3.Text = "Item Model"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "Item Brand"
        '
        'cboUnit
        '
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(491, 78)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(158, 21)
        Me.cboUnit.TabIndex = 45
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(363, 81)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(125, 13)
        Me.Label78.TabIndex = 49
        Me.Label78.Text = "Unit of Measurement"
        '
        'cboSizeCode
        '
        Me.cboSizeCode.FormattingEnabled = True
        Me.cboSizeCode.Items.AddRange(New Object() {"Discount", "Group", "Inventory Part", "Inventory Assembly", "Non-Inventory Part", "Other Charge", "Payment", "Sales Tax Item", "Sales Tax Group", "Service", "Subtotal"})
        Me.cboSizeCode.Location = New System.Drawing.Point(436, 49)
        Me.cboSizeCode.Name = "cboSizeCode"
        Me.cboSizeCode.Size = New System.Drawing.Size(213, 21)
        Me.cboSizeCode.TabIndex = 48
        '
        'cboColorCode
        '
        Me.cboColorCode.FormattingEnabled = True
        Me.cboColorCode.Items.AddRange(New Object() {"Discount", "Group", "Inventory Part", "Inventory Assembly", "Non-Inventory Part", "Other Charge", "Payment", "Sales Tax Item", "Sales Tax Group", "Service", "Subtotal"})
        Me.cboColorCode.Location = New System.Drawing.Point(436, 22)
        Me.cboColorCode.Name = "cboColorCode"
        Me.cboColorCode.Size = New System.Drawing.Size(213, 21)
        Me.cboColorCode.TabIndex = 47
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(363, 52)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(31, 13)
        Me.Label77.TabIndex = 46
        Me.Label77.Text = "Size"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(363, 25)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(38, 13)
        Me.Label76.TabIndex = 45
        Me.Label76.Text = "Color"
        '
        'cboCategory
        '
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Items.AddRange(New Object() {"Discount", "Group", "Inventory Part", "Inventory Assembly", "Non-Inventory Part", "Other Charge", "Payment", "Sales Tax Item", "Sales Tax Group", "Service", "Subtotal"})
        Me.cboCategory.Location = New System.Drawing.Point(118, 78)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(213, 21)
        Me.cboCategory.TabIndex = 44
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(9, 81)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(91, 13)
        Me.Label22.TabIndex = 43
        Me.Label22.Text = "Item Category"
        '
        'txtdescription
        '
        Me.txtdescription.Location = New System.Drawing.Point(118, 49)
        Me.txtdescription.Name = "txtdescription"
        Me.txtdescription.Size = New System.Drawing.Size(213, 21)
        Me.txtdescription.TabIndex = 41
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(9, 52)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(102, 13)
        Me.Label21.TabIndex = 42
        Me.Label21.Text = "Item Description"
        '
        'cboItemCode
        '
        Me.cboItemCode.FormattingEnabled = True
        Me.cboItemCode.Items.AddRange(New Object() {"Discount", "Group", "Inventory Part", "Inventory Assembly", "Non-Inventory Part", "Other Charge", "Payment", "Sales Tax Item", "Sales Tax Group", "Service", "Subtotal"})
        Me.cboItemCode.Location = New System.Drawing.Point(144, 25)
        Me.cboItemCode.Name = "cboItemCode"
        Me.cboItemCode.Size = New System.Drawing.Size(213, 21)
        Me.cboItemCode.TabIndex = 5
        Me.cboItemCode.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 25)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(68, 13)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Item Code"
        '
        'btn_item_cancel
        '
        Me.btn_item_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_item_cancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_item_cancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel2
        Me.btn_item_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_item_cancel.Location = New System.Drawing.Point(850, 441)
        Me.btn_item_cancel.Name = "btn_item_cancel"
        Me.btn_item_cancel.Size = New System.Drawing.Size(85, 29)
        Me.btn_item_cancel.TabIndex = 28
        Me.btn_item_cancel.Text = "Cancel"
        Me.btn_item_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_item_cancel.UseVisualStyleBackColor = True
        '
        'frm_item_masterItemAddEdit
        '
        Me.AcceptButton = Me.btn_dep_saveClose
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_item_cancel
        Me.ClientSize = New System.Drawing.Size(941, 473)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.txtManPartNo)
        Me.Controls.Add(Me.GroupBox12)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btn_item_cancel)
        Me.Controls.Add(Me.btn_dep_saveClose)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkInactive)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(949, 504)
        Me.Name = "frm_item_masterItemAddEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Item"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbo_item_itemGroupID As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtitemdescription As System.Windows.Forms.TextBox
    Friend WithEvents chkInactive As System.Windows.Forms.CheckBox
    Friend WithEvents btn_dep_saveClose As System.Windows.Forms.Button
    Friend WithEvents btn_item_cancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtItemdescsales As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtItemdescpurchase As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtSalesprice As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtOnhand As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalVal As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtReorder As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtManPartNo As System.Windows.Forms.TextBox
    Friend WithEvents cboCOGs As System.Windows.Forms.ComboBox
    Friend WithEvents cboSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents cboIncomeAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents cboTax As System.Windows.Forms.ComboBox
    Friend WithEvents cboAssetAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents cbo_item_itemGroupName As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents txtSO As System.Windows.Forms.TextBox
    Friend WithEvents lblOnSo As System.Windows.Forms.Label
    Friend WithEvents txtPO As System.Windows.Forms.TextBox
    Friend WithEvents lblOnPo As System.Windows.Forms.Label
    Friend WithEvents txtAvgCost As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtdescription As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboItemCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents cboSizeCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboColorCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboModel As System.Windows.Forms.ComboBox
    Friend WithEvents cboBrand As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtItemCode As System.Windows.Forms.TextBox
End Class
