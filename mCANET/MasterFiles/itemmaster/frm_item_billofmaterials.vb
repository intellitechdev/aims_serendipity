Public Class frm_item_billofmaterials

    Private Sub frm_item_billofmaterials_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_fullview_billofmaterials()
    End Sub

    Private Sub btn_item_fvBMedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_item_fvBMedit.Click
        Dim frmtmp As New frm_item_masterItemAddEdit
        frmtmp.ShowDialog()
    End Sub

    Private Sub load_fullview_billofmaterials()
        With lst_item_fvBM
            .Clear()
            .View = View.Details
            .Columns.Add("Item", 70, HorizontalAlignment.Left)
            .Columns.Add("Description", 150, HorizontalAlignment.Left)
            .Columns.Add("Type", 70, HorizontalAlignment.Left)
            .Columns.Add("Qty", 50, HorizontalAlignment.Left)
            .Columns.Add("Total", 100, HorizontalAlignment.Left)
        End With
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

    End Sub

End Class