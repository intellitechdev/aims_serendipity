﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblClientname = New System.Windows.Forms.Label()
        Me.lblIDno = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.tabVerification = New System.Windows.Forms.TabControl()
        Me.CurrentLoan = New System.Windows.Forms.TabPage()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnPrintStatement = New System.Windows.Forms.Button()
        Me.btnPrintLoanLedger = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtBalPrincipal = New System.Windows.Forms.TextBox()
        Me.txtBalInterest = New System.Windows.Forms.TextBox()
        Me.txtBalServicefee = New System.Windows.Forms.TextBox()
        Me.txtBalCreditAcct = New System.Windows.Forms.TextBox()
        Me.txtTotalPayment = New System.Windows.Forms.TextBox()
        Me.dgvSubsidiaryCurrent = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvLoans = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.LoanHistory = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvLoanHistorySubsidiary = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgvLoanHistory = New System.Windows.Forms.DataGridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Debit = New System.Windows.Forms.TabPage()
        Me.TBCDebitAccounts = New System.Windows.Forms.TabControl()
        Me.TBListAccounts = New System.Windows.Forms.TabPage()
        Me.dgvDebitList = New System.Windows.Forms.DataGridView()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TBSoaAccounts = New System.Windows.Forms.TabPage()
        Me.dgvSoaList = New System.Windows.Forms.DataGridView()
        Me.dgvDebitList1 = New System.Windows.Forms.DataGridView()
        Me.dgvDebitDetails = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Credit = New System.Windows.Forms.TabPage()
        Me.txtLine = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dgvCreditDetails = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgvCredit = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnPrintPassbook = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Amortization = New System.Windows.Forms.TabPage()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.btnSelectedLoanPrint = New System.Windows.Forms.Button()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtPrincipal = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblLoanType = New System.Windows.Forms.Label()
        Me.lblLoanRef = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvAmortizationBalances = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgvAmortizationSchedule = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SOA = New System.Windows.Forms.TabPage()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.grdReceivables = New System.Windows.Forms.DataGridView()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnPrintSSS = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.grdSSSDetails = New System.Windows.Forms.DataGridView()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.grdSSS = New System.Windows.Forms.DataGridView()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.tabVerification.SuspendLayout()
        Me.CurrentLoan.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvSubsidiaryCurrent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.LoanHistory.SuspendLayout()
        CType(Me.dgvLoanHistorySubsidiary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLoanHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Debit.SuspendLayout()
        Me.TBCDebitAccounts.SuspendLayout()
        Me.TBListAccounts.SuspendLayout()
        CType(Me.dgvDebitList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TBSoaAccounts.SuspendLayout()
        CType(Me.dgvSoaList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebitList1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebitDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Credit.SuspendLayout()
        CType(Me.txtLine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCreditDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Amortization.SuspendLayout()
        CType(Me.dgvAmortizationBalances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAmortizationSchedule, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdReceivables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdSSSDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdSSS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(896, 62)
        Me.Panel1.TabIndex = 57
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(321, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Client Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID No."
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.lblClientname)
        Me.GroupBox1.Controls.Add(Me.lblIDno)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(870, 45)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'lblClientname
        '
        Me.lblClientname.AutoSize = True
        Me.lblClientname.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientname.ForeColor = System.Drawing.Color.Black
        Me.lblClientname.Location = New System.Drawing.Point(396, 19)
        Me.lblClientname.Name = "lblClientname"
        Me.lblClientname.Size = New System.Drawing.Size(138, 16)
        Me.lblClientname.TabIndex = 1
        Me.lblClientname.Text = "CLIENT, CLIENT C."
        '
        'lblIDno
        '
        Me.lblIDno.AutoSize = True
        Me.lblIDno.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDno.ForeColor = System.Drawing.Color.Black
        Me.lblIDno.Location = New System.Drawing.Point(64, 19)
        Me.lblIDno.Name = "lblIDno"
        Me.lblIDno.Size = New System.Drawing.Size(94, 14)
        Me.lblIDno.TabIndex = 0
        Me.lblIDno.Text = "SAMPLE-123"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.tabVerification)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 62)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(896, 603)
        Me.Panel2.TabIndex = 58
        '
        'tabVerification
        '
        Me.tabVerification.Controls.Add(Me.CurrentLoan)
        Me.tabVerification.Controls.Add(Me.LoanHistory)
        Me.tabVerification.Controls.Add(Me.Debit)
        Me.tabVerification.Controls.Add(Me.Credit)
        Me.tabVerification.Controls.Add(Me.Amortization)
        Me.tabVerification.Controls.Add(Me.SOA)
        Me.tabVerification.Controls.Add(Me.TabPage1)
        Me.tabVerification.Controls.Add(Me.TabPage2)
        Me.tabVerification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabVerification.Location = New System.Drawing.Point(0, 0)
        Me.tabVerification.Name = "tabVerification"
        Me.tabVerification.SelectedIndex = 0
        Me.tabVerification.Size = New System.Drawing.Size(896, 603)
        Me.tabVerification.TabIndex = 0
        '
        'CurrentLoan
        '
        Me.CurrentLoan.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.CurrentLoan.Controls.Add(Me.btnRefresh)
        Me.CurrentLoan.Controls.Add(Me.btnPrintStatement)
        Me.CurrentLoan.Controls.Add(Me.btnPrintLoanLedger)
        Me.CurrentLoan.Controls.Add(Me.btnSearch)
        Me.CurrentLoan.Controls.Add(Me.btnClose)
        Me.CurrentLoan.Controls.Add(Me.Panel5)
        Me.CurrentLoan.Controls.Add(Me.dgvSubsidiaryCurrent)
        Me.CurrentLoan.Controls.Add(Me.Panel3)
        Me.CurrentLoan.Controls.Add(Me.dgvLoans)
        Me.CurrentLoan.Controls.Add(Me.Panel4)
        Me.CurrentLoan.Location = New System.Drawing.Point(4, 24)
        Me.CurrentLoan.Name = "CurrentLoan"
        Me.CurrentLoan.Padding = New System.Windows.Forms.Padding(3)
        Me.CurrentLoan.Size = New System.Drawing.Size(888, 575)
        Me.CurrentLoan.TabIndex = 1
        Me.CurrentLoan.Text = "Active Loan"
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.btnRefresh.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(604, 488)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(86, 30)
        Me.btnRefresh.TabIndex = 24
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnPrintStatement
        '
        Me.btnPrintStatement.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintStatement.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.btnPrintStatement.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPrintStatement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintStatement.Location = New System.Drawing.Point(157, 488)
        Me.btnPrintStatement.Name = "btnPrintStatement"
        Me.btnPrintStatement.Size = New System.Drawing.Size(199, 30)
        Me.btnPrintStatement.TabIndex = 23
        Me.btnPrintStatement.Text = "Print Statement of Accounts"
        Me.btnPrintStatement.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintStatement.UseVisualStyleBackColor = True
        Me.btnPrintStatement.Visible = False
        '
        'btnPrintLoanLedger
        '
        Me.btnPrintLoanLedger.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintLoanLedger.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.btnPrintLoanLedger.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPrintLoanLedger.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintLoanLedger.Location = New System.Drawing.Point(10, 488)
        Me.btnPrintLoanLedger.Name = "btnPrintLoanLedger"
        Me.btnPrintLoanLedger.Size = New System.Drawing.Size(141, 30)
        Me.btnPrintLoanLedger.TabIndex = 22
        Me.btnPrintLoanLedger.Text = "Print Loan Ledger"
        Me.btnPrintLoanLedger.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintLoanLedger.UseVisualStyleBackColor = True
        Me.btnPrintLoanLedger.Visible = False
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.btnSearch.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(696, 488)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(86, 30)
        Me.btnSearch.TabIndex = 2
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(788, 488)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(86, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.txtBalPrincipal)
        Me.Panel5.Controls.Add(Me.txtBalInterest)
        Me.Panel5.Controls.Add(Me.txtBalServicefee)
        Me.Panel5.Controls.Add(Me.txtBalCreditAcct)
        Me.Panel5.Controls.Add(Me.txtTotalPayment)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(3, 439)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(882, 22)
        Me.Panel5.TabIndex = 24
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label17.Location = New System.Drawing.Point(375, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 15)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Balances :"
        '
        'txtBalPrincipal
        '
        Me.txtBalPrincipal.BackColor = System.Drawing.Color.White
        Me.txtBalPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalPrincipal.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalPrincipal.Location = New System.Drawing.Point(442, 0)
        Me.txtBalPrincipal.Name = "txtBalPrincipal"
        Me.txtBalPrincipal.ReadOnly = True
        Me.txtBalPrincipal.Size = New System.Drawing.Size(88, 14)
        Me.txtBalPrincipal.TabIndex = 24
        Me.txtBalPrincipal.Text = "0.00"
        Me.txtBalPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalInterest
        '
        Me.txtBalInterest.BackColor = System.Drawing.Color.White
        Me.txtBalInterest.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalInterest.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalInterest.Location = New System.Drawing.Point(530, 0)
        Me.txtBalInterest.Name = "txtBalInterest"
        Me.txtBalInterest.ReadOnly = True
        Me.txtBalInterest.Size = New System.Drawing.Size(88, 14)
        Me.txtBalInterest.TabIndex = 23
        Me.txtBalInterest.Text = "0.00"
        Me.txtBalInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalServicefee
        '
        Me.txtBalServicefee.BackColor = System.Drawing.Color.White
        Me.txtBalServicefee.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalServicefee.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalServicefee.Location = New System.Drawing.Point(618, 0)
        Me.txtBalServicefee.Name = "txtBalServicefee"
        Me.txtBalServicefee.ReadOnly = True
        Me.txtBalServicefee.Size = New System.Drawing.Size(88, 14)
        Me.txtBalServicefee.TabIndex = 25
        Me.txtBalServicefee.Text = "0.00"
        Me.txtBalServicefee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalCreditAcct
        '
        Me.txtBalCreditAcct.BackColor = System.Drawing.Color.White
        Me.txtBalCreditAcct.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalCreditAcct.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalCreditAcct.Location = New System.Drawing.Point(706, 0)
        Me.txtBalCreditAcct.Name = "txtBalCreditAcct"
        Me.txtBalCreditAcct.ReadOnly = True
        Me.txtBalCreditAcct.Size = New System.Drawing.Size(88, 14)
        Me.txtBalCreditAcct.TabIndex = 27
        Me.txtBalCreditAcct.Text = "0.00"
        Me.txtBalCreditAcct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPayment
        '
        Me.txtTotalPayment.BackColor = System.Drawing.Color.White
        Me.txtTotalPayment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalPayment.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtTotalPayment.Location = New System.Drawing.Point(794, 0)
        Me.txtTotalPayment.Name = "txtTotalPayment"
        Me.txtTotalPayment.ReadOnly = True
        Me.txtTotalPayment.Size = New System.Drawing.Size(88, 14)
        Me.txtTotalPayment.TabIndex = 26
        Me.txtTotalPayment.Text = "0.00"
        Me.txtTotalPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvSubsidiaryCurrent
        '
        Me.dgvSubsidiaryCurrent.AllowUserToAddRows = False
        Me.dgvSubsidiaryCurrent.AllowUserToDeleteRows = False
        Me.dgvSubsidiaryCurrent.AllowUserToResizeColumns = False
        Me.dgvSubsidiaryCurrent.AllowUserToResizeRows = False
        Me.dgvSubsidiaryCurrent.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSubsidiaryCurrent.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSubsidiaryCurrent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSubsidiaryCurrent.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvSubsidiaryCurrent.Location = New System.Drawing.Point(3, 271)
        Me.dgvSubsidiaryCurrent.Name = "dgvSubsidiaryCurrent"
        Me.dgvSubsidiaryCurrent.ReadOnly = True
        Me.dgvSubsidiaryCurrent.RowHeadersVisible = False
        Me.dgvSubsidiaryCurrent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSubsidiaryCurrent.Size = New System.Drawing.Size(882, 168)
        Me.dgvSubsidiaryCurrent.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(3, 231)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(882, 40)
        Me.Panel3.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Label3.Location = New System.Drawing.Point(5, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Subsidiary Ledger"
        '
        'dgvLoans
        '
        Me.dgvLoans.AllowUserToAddRows = False
        Me.dgvLoans.AllowUserToDeleteRows = False
        Me.dgvLoans.AllowUserToResizeColumns = False
        Me.dgvLoans.AllowUserToResizeRows = False
        Me.dgvLoans.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLoans.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvLoans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoans.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvLoans.Location = New System.Drawing.Point(3, 43)
        Me.dgvLoans.Name = "dgvLoans"
        Me.dgvLoans.ReadOnly = True
        Me.dgvLoans.RowHeadersVisible = False
        Me.dgvLoans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoans.Size = New System.Drawing.Size(882, 188)
        Me.dgvLoans.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(3, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(882, 40)
        Me.Panel4.TabIndex = 23
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Label16.Location = New System.Drawing.Point(4, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(126, 15)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "List of Current Loans"
        '
        'LoanHistory
        '
        Me.LoanHistory.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.LoanHistory.Controls.Add(Me.Button2)
        Me.LoanHistory.Controls.Add(Me.Button3)
        Me.LoanHistory.Controls.Add(Me.Button1)
        Me.LoanHistory.Controls.Add(Me.dgvLoanHistorySubsidiary)
        Me.LoanHistory.Controls.Add(Me.Label9)
        Me.LoanHistory.Controls.Add(Me.dgvLoanHistory)
        Me.LoanHistory.Controls.Add(Me.Label12)
        Me.LoanHistory.Location = New System.Drawing.Point(4, 24)
        Me.LoanHistory.Name = "LoanHistory"
        Me.LoanHistory.Size = New System.Drawing.Size(888, 575)
        Me.LoanHistory.TabIndex = 2
        Me.LoanHistory.Text = "Loan History"
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button2.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(696, 496)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 30)
        Me.Button2.TabIndex = 27
        Me.Button2.Text = "Search"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button3.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(788, 496)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 30)
        Me.Button3.TabIndex = 26
        Me.Button3.Text = "Close"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button1.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(604, 496)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 30)
        Me.Button1.TabIndex = 25
        Me.Button1.Text = "Refresh"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgvLoanHistorySubsidiary
        '
        Me.dgvLoanHistorySubsidiary.AllowUserToAddRows = False
        Me.dgvLoanHistorySubsidiary.AllowUserToDeleteRows = False
        Me.dgvLoanHistorySubsidiary.AllowUserToResizeColumns = False
        Me.dgvLoanHistorySubsidiary.AllowUserToResizeRows = False
        Me.dgvLoanHistorySubsidiary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoanHistorySubsidiary.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanHistorySubsidiary.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLoanHistorySubsidiary.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvLoanHistorySubsidiary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanHistorySubsidiary.Location = New System.Drawing.Point(15, 268)
        Me.dgvLoanHistorySubsidiary.Name = "dgvLoanHistorySubsidiary"
        Me.dgvLoanHistorySubsidiary.ReadOnly = True
        Me.dgvLoanHistorySubsidiary.RowHeadersVisible = False
        Me.dgvLoanHistorySubsidiary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanHistorySubsidiary.Size = New System.Drawing.Size(859, 200)
        Me.dgvLoanHistorySubsidiary.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 250)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(119, 15)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Subsidiary Ledger :"
        '
        'dgvLoanHistory
        '
        Me.dgvLoanHistory.AllowUserToAddRows = False
        Me.dgvLoanHistory.AllowUserToDeleteRows = False
        Me.dgvLoanHistory.AllowUserToResizeColumns = False
        Me.dgvLoanHistory.AllowUserToResizeRows = False
        Me.dgvLoanHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoanHistory.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLoanHistory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvLoanHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanHistory.Location = New System.Drawing.Point(15, 30)
        Me.dgvLoanHistory.Name = "dgvLoanHistory"
        Me.dgvLoanHistory.ReadOnly = True
        Me.dgvLoanHistory.RowHeadersVisible = False
        Me.dgvLoanHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanHistory.Size = New System.Drawing.Size(859, 200)
        Me.dgvLoanHistory.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(12, 12)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(115, 15)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "List of Paid Loans :"
        '
        'Debit
        '
        Me.Debit.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Debit.Controls.Add(Me.TBCDebitAccounts)
        Me.Debit.Controls.Add(Me.dgvDebitList1)
        Me.Debit.Controls.Add(Me.dgvDebitDetails)
        Me.Debit.Controls.Add(Me.Label6)
        Me.Debit.Controls.Add(Me.Label5)
        Me.Debit.Controls.Add(Me.Button4)
        Me.Debit.Controls.Add(Me.Button5)
        Me.Debit.Controls.Add(Me.Button6)
        Me.Debit.Location = New System.Drawing.Point(4, 24)
        Me.Debit.Name = "Debit"
        Me.Debit.Size = New System.Drawing.Size(888, 575)
        Me.Debit.TabIndex = 3
        Me.Debit.Text = "Debit Accounts"
        '
        'TBCDebitAccounts
        '
        Me.TBCDebitAccounts.Controls.Add(Me.TBListAccounts)
        Me.TBCDebitAccounts.Controls.Add(Me.TBSoaAccounts)
        Me.TBCDebitAccounts.Location = New System.Drawing.Point(15, 18)
        Me.TBCDebitAccounts.Name = "TBCDebitAccounts"
        Me.TBCDebitAccounts.SelectedIndex = 0
        Me.TBCDebitAccounts.Size = New System.Drawing.Size(859, 229)
        Me.TBCDebitAccounts.TabIndex = 32
        '
        'TBListAccounts
        '
        Me.TBListAccounts.Controls.Add(Me.dgvDebitList)
        Me.TBListAccounts.Controls.Add(Me.Label18)
        Me.TBListAccounts.Location = New System.Drawing.Point(4, 24)
        Me.TBListAccounts.Name = "TBListAccounts"
        Me.TBListAccounts.Padding = New System.Windows.Forms.Padding(3)
        Me.TBListAccounts.Size = New System.Drawing.Size(851, 201)
        Me.TBListAccounts.TabIndex = 0
        Me.TBListAccounts.Text = "Debit Accounts"
        Me.TBListAccounts.UseVisualStyleBackColor = True
        '
        'dgvDebitList
        '
        Me.dgvDebitList.AllowUserToAddRows = False
        Me.dgvDebitList.AllowUserToDeleteRows = False
        Me.dgvDebitList.AllowUserToResizeColumns = False
        Me.dgvDebitList.AllowUserToResizeRows = False
        Me.dgvDebitList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitList.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitList.Location = New System.Drawing.Point(6, 17)
        Me.dgvDebitList.Name = "dgvDebitList"
        Me.dgvDebitList.ReadOnly = True
        Me.dgvDebitList.RowHeadersVisible = False
        Me.dgvDebitList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebitList.Size = New System.Drawing.Size(839, 178)
        Me.dgvDebitList.TabIndex = 1
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(12, 16)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(139, 15)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "List of Debit Accounts :"
        Me.Label18.Visible = False
        '
        'TBSoaAccounts
        '
        Me.TBSoaAccounts.Controls.Add(Me.dgvSoaList)
        Me.TBSoaAccounts.Location = New System.Drawing.Point(4, 24)
        Me.TBSoaAccounts.Name = "TBSoaAccounts"
        Me.TBSoaAccounts.Padding = New System.Windows.Forms.Padding(3)
        Me.TBSoaAccounts.Size = New System.Drawing.Size(851, 201)
        Me.TBSoaAccounts.TabIndex = 1
        Me.TBSoaAccounts.Text = "Soa Accounts"
        Me.TBSoaAccounts.UseVisualStyleBackColor = True
        '
        'dgvSoaList
        '
        Me.dgvSoaList.AllowUserToAddRows = False
        Me.dgvSoaList.AllowUserToDeleteRows = False
        Me.dgvSoaList.AllowUserToResizeColumns = False
        Me.dgvSoaList.AllowUserToResizeRows = False
        Me.dgvSoaList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvSoaList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvSoaList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvSoaList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSoaList.Location = New System.Drawing.Point(12, 17)
        Me.dgvSoaList.Name = "dgvSoaList"
        Me.dgvSoaList.ReadOnly = True
        Me.dgvSoaList.RowHeadersVisible = False
        Me.dgvSoaList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSoaList.Size = New System.Drawing.Size(840, 178)
        Me.dgvSoaList.TabIndex = 0
        '
        'dgvDebitList1
        '
        Me.dgvDebitList1.AllowUserToAddRows = False
        Me.dgvDebitList1.AllowUserToDeleteRows = False
        Me.dgvDebitList1.AllowUserToResizeColumns = False
        Me.dgvDebitList1.AllowUserToResizeRows = False
        Me.dgvDebitList1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitList1.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitList1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDebitList1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvDebitList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitList1.Location = New System.Drawing.Point(213, 302)
        Me.dgvDebitList1.Name = "dgvDebitList1"
        Me.dgvDebitList1.ReadOnly = True
        Me.dgvDebitList1.RowHeadersVisible = False
        Me.dgvDebitList1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebitList1.Size = New System.Drawing.Size(242, 182)
        Me.dgvDebitList1.TabIndex = 31
        Me.dgvDebitList1.Visible = False
        '
        'dgvDebitDetails
        '
        Me.dgvDebitDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDebitDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvDebitDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitDetails.Location = New System.Drawing.Point(15, 268)
        Me.dgvDebitDetails.Name = "dgvDebitDetails"
        Me.dgvDebitDetails.ReadOnly = True
        Me.dgvDebitDetails.RowHeadersVisible = False
        Me.dgvDebitDetails.Size = New System.Drawing.Size(859, 179)
        Me.dgvDebitDetails.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 250)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Details :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(139, 15)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "List of Debit Accounts :"
        Me.Label5.Visible = False
        '
        'Button4
        '
        Me.Button4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button4.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button4.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.Location = New System.Drawing.Point(700, 497)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(86, 30)
        Me.Button4.TabIndex = 30
        Me.Button4.Text = "Search"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button5.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button5.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(792, 497)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(86, 30)
        Me.Button5.TabIndex = 29
        Me.Button5.Text = "Close"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button6.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button6.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.Location = New System.Drawing.Point(608, 497)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(86, 30)
        Me.Button6.TabIndex = 28
        Me.Button6.Text = "Refresh"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Credit
        '
        Me.Credit.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Credit.Controls.Add(Me.txtLine)
        Me.Credit.Controls.Add(Me.Label11)
        Me.Credit.Controls.Add(Me.dgvCreditDetails)
        Me.Credit.Controls.Add(Me.Label8)
        Me.Credit.Controls.Add(Me.dgvCredit)
        Me.Credit.Controls.Add(Me.Label7)
        Me.Credit.Controls.Add(Me.btnPrintPassbook)
        Me.Credit.Controls.Add(Me.Button7)
        Me.Credit.Controls.Add(Me.Button8)
        Me.Credit.Controls.Add(Me.Button9)
        Me.Credit.Location = New System.Drawing.Point(4, 24)
        Me.Credit.Name = "Credit"
        Me.Credit.Size = New System.Drawing.Size(888, 575)
        Me.Credit.TabIndex = 4
        Me.Credit.Text = "Credit Accounts"
        '
        'txtLine
        '
        Me.txtLine.Location = New System.Drawing.Point(104, 501)
        Me.txtLine.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.txtLine.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtLine.Name = "txtLine"
        Me.txtLine.Size = New System.Drawing.Size(57, 21)
        Me.txtLine.TabIndex = 37
        Me.txtLine.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(19, 504)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 15)
        Me.Label11.TabIndex = 36
        Me.Label11.Text = "Line Number"
        '
        'dgvCreditDetails
        '
        Me.dgvCreditDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCreditDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvCreditDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCreditDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvCreditDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCreditDetails.Location = New System.Drawing.Point(15, 268)
        Me.dgvCreditDetails.Name = "dgvCreditDetails"
        Me.dgvCreditDetails.ReadOnly = True
        Me.dgvCreditDetails.RowHeadersVisible = False
        Me.dgvCreditDetails.Size = New System.Drawing.Size(859, 200)
        Me.dgvCreditDetails.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 250)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 15)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Details :"
        '
        'dgvCredit
        '
        Me.dgvCredit.AllowUserToAddRows = False
        Me.dgvCredit.AllowUserToDeleteRows = False
        Me.dgvCredit.AllowUserToResizeColumns = False
        Me.dgvCredit.AllowUserToResizeRows = False
        Me.dgvCredit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCredit.BackgroundColor = System.Drawing.Color.White
        Me.dgvCredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCredit.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvCredit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCredit.Location = New System.Drawing.Point(15, 30)
        Me.dgvCredit.Name = "dgvCredit"
        Me.dgvCredit.ReadOnly = True
        Me.dgvCredit.RowHeadersVisible = False
        Me.dgvCredit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCredit.Size = New System.Drawing.Size(859, 200)
        Me.dgvCredit.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 15)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "List of Credit Accounts :"
        '
        'btnPrintPassbook
        '
        Me.btnPrintPassbook.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPrintPassbook.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintPassbook.Location = New System.Drawing.Point(176, 496)
        Me.btnPrintPassbook.Name = "btnPrintPassbook"
        Me.btnPrintPassbook.Size = New System.Drawing.Size(119, 28)
        Me.btnPrintPassbook.TabIndex = 35
        Me.btnPrintPassbook.Text = "Print Preview"
        Me.btnPrintPassbook.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintPassbook.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button7.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button7.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button7.Location = New System.Drawing.Point(696, 496)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(86, 30)
        Me.Button7.TabIndex = 30
        Me.Button7.Text = "Search"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button8.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button8.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button8.Location = New System.Drawing.Point(788, 496)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(86, 30)
        Me.Button8.TabIndex = 29
        Me.Button8.Text = "Close"
        Me.Button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button9.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button9.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.Button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button9.Location = New System.Drawing.Point(604, 496)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(86, 30)
        Me.Button9.TabIndex = 28
        Me.Button9.Text = "Refresh"
        Me.Button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Amortization
        '
        Me.Amortization.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Amortization.Controls.Add(Me.Button10)
        Me.Amortization.Controls.Add(Me.Button11)
        Me.Amortization.Controls.Add(Me.Button12)
        Me.Amortization.Controls.Add(Me.btnSelectedLoanPrint)
        Me.Amortization.Controls.Add(Me.txtServiceFee)
        Me.Amortization.Controls.Add(Me.txtPrincipal)
        Me.Amortization.Controls.Add(Me.txtInterest)
        Me.Amortization.Controls.Add(Me.Label15)
        Me.Amortization.Controls.Add(Me.lblLoanType)
        Me.Amortization.Controls.Add(Me.lblLoanRef)
        Me.Amortization.Controls.Add(Me.Label14)
        Me.Amortization.Controls.Add(Me.Label13)
        Me.Amortization.Controls.Add(Me.dgvAmortizationBalances)
        Me.Amortization.Controls.Add(Me.Label10)
        Me.Amortization.Controls.Add(Me.dgvAmortizationSchedule)
        Me.Amortization.Controls.Add(Me.Label4)
        Me.Amortization.Location = New System.Drawing.Point(4, 24)
        Me.Amortization.Name = "Amortization"
        Me.Amortization.Size = New System.Drawing.Size(888, 575)
        Me.Amortization.TabIndex = 5
        Me.Amortization.Text = "Amortization"
        '
        'Button10
        '
        Me.Button10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button10.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button10.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button10.Location = New System.Drawing.Point(696, 496)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(86, 30)
        Me.Button10.TabIndex = 33
        Me.Button10.Text = "Search"
        Me.Button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button11.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button11.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button11.Location = New System.Drawing.Point(788, 496)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(86, 30)
        Me.Button11.TabIndex = 32
        Me.Button11.Text = "Close"
        Me.Button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button12.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button12.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.Button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button12.Location = New System.Drawing.Point(604, 496)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(86, 30)
        Me.Button12.TabIndex = 31
        Me.Button12.Text = "Refresh"
        Me.Button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button12.UseVisualStyleBackColor = True
        '
        'btnSelectedLoanPrint
        '
        Me.btnSelectedLoanPrint.BackColor = System.Drawing.SystemColors.Control
        Me.btnSelectedLoanPrint.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnSelectedLoanPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSelectedLoanPrint.Location = New System.Drawing.Point(10, 410)
        Me.btnSelectedLoanPrint.Name = "btnSelectedLoanPrint"
        Me.btnSelectedLoanPrint.Size = New System.Drawing.Size(215, 33)
        Me.btnSelectedLoanPrint.TabIndex = 22
        Me.btnSelectedLoanPrint.Text = "Preview Loan Amortization"
        Me.btnSelectedLoanPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelectedLoanPrint.UseVisualStyleBackColor = False
        Me.btnSelectedLoanPrint.Visible = False
        '
        'txtServiceFee
        '
        Me.txtServiceFee.BackColor = System.Drawing.Color.White
        Me.txtServiceFee.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtServiceFee.Location = New System.Drawing.Point(561, 215)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.ReadOnly = True
        Me.txtServiceFee.Size = New System.Drawing.Size(88, 14)
        Me.txtServiceFee.TabIndex = 15
        Me.txtServiceFee.Text = "0.00"
        Me.txtServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipal
        '
        Me.txtPrincipal.BackColor = System.Drawing.Color.White
        Me.txtPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPrincipal.Location = New System.Drawing.Point(287, 216)
        Me.txtPrincipal.Name = "txtPrincipal"
        Me.txtPrincipal.ReadOnly = True
        Me.txtPrincipal.Size = New System.Drawing.Size(88, 14)
        Me.txtPrincipal.TabIndex = 14
        Me.txtPrincipal.Text = "0.00"
        Me.txtPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterest
        '
        Me.txtInterest.BackColor = System.Drawing.Color.White
        Me.txtInterest.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtInterest.Location = New System.Drawing.Point(430, 215)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.ReadOnly = True
        Me.txtInterest.Size = New System.Drawing.Size(88, 14)
        Me.txtInterest.TabIndex = 13
        Me.txtInterest.Text = "0.00"
        Me.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(234, 217)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(50, 15)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Totals: "
        '
        'lblLoanType
        '
        Me.lblLoanType.AutoSize = True
        Me.lblLoanType.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanType.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblLoanType.Location = New System.Drawing.Point(456, 11)
        Me.lblLoanType.Name = "lblLoanType"
        Me.lblLoanType.Size = New System.Drawing.Size(13, 14)
        Me.lblLoanType.TabIndex = 11
        Me.lblLoanType.Text = "-"
        '
        'lblLoanRef
        '
        Me.lblLoanRef.AutoSize = True
        Me.lblLoanRef.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanRef.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblLoanRef.Location = New System.Drawing.Point(234, 11)
        Me.lblLoanRef.Name = "lblLoanRef"
        Me.lblLoanRef.Size = New System.Drawing.Size(13, 14)
        Me.lblLoanRef.TabIndex = 10
        Me.lblLoanRef.Text = "-"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(380, 11)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(75, 15)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Loan Type :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(171, 11)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 15)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Loan Ref:"
        '
        'dgvAmortizationBalances
        '
        Me.dgvAmortizationBalances.AllowUserToAddRows = False
        Me.dgvAmortizationBalances.AllowUserToDeleteRows = False
        Me.dgvAmortizationBalances.AllowUserToResizeColumns = False
        Me.dgvAmortizationBalances.AllowUserToResizeRows = False
        Me.dgvAmortizationBalances.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortizationBalances.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortizationBalances.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAmortizationBalances.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvAmortizationBalances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortizationBalances.Location = New System.Drawing.Point(10, 255)
        Me.dgvAmortizationBalances.Name = "dgvAmortizationBalances"
        Me.dgvAmortizationBalances.ReadOnly = True
        Me.dgvAmortizationBalances.RowHeadersVisible = False
        Me.dgvAmortizationBalances.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortizationBalances.Size = New System.Drawing.Size(859, 149)
        Me.dgvAmortizationBalances.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 237)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 15)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Balance "
        '
        'dgvAmortizationSchedule
        '
        Me.dgvAmortizationSchedule.AllowUserToAddRows = False
        Me.dgvAmortizationSchedule.AllowUserToDeleteRows = False
        Me.dgvAmortizationSchedule.AllowUserToResizeColumns = False
        Me.dgvAmortizationSchedule.AllowUserToResizeRows = False
        Me.dgvAmortizationSchedule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortizationSchedule.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortizationSchedule.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAmortizationSchedule.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvAmortizationSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortizationSchedule.Location = New System.Drawing.Point(10, 57)
        Me.dgvAmortizationSchedule.Name = "dgvAmortizationSchedule"
        Me.dgvAmortizationSchedule.ReadOnly = True
        Me.dgvAmortizationSchedule.RowHeadersVisible = False
        Me.dgvAmortizationSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortizationSchedule.Size = New System.Drawing.Size(859, 146)
        Me.dgvAmortizationSchedule.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(139, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Amortization Schedule "
        '
        'SOA
        '
        Me.SOA.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.SOA.Location = New System.Drawing.Point(4, 24)
        Me.SOA.Name = "SOA"
        Me.SOA.Size = New System.Drawing.Size(888, 575)
        Me.SOA.TabIndex = 6
        Me.SOA.Text = "SOA"
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TabPage1.Controls.Add(Me.Button19)
        Me.TabPage1.Controls.Add(Me.Button13)
        Me.TabPage1.Controls.Add(Me.Button14)
        Me.TabPage1.Controls.Add(Me.Button15)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.grdReceivables)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(888, 575)
        Me.TabPage1.TabIndex = 7
        Me.TabPage1.Text = "Receivables"
        '
        'Button19
        '
        Me.Button19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button19.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button19.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.Button19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button19.Location = New System.Drawing.Point(9, 496)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(141, 30)
        Me.Button19.TabIndex = 41
        Me.Button19.Text = "Print Amortization"
        Me.Button19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button13.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button13.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button13.Location = New System.Drawing.Point(696, 496)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(86, 30)
        Me.Button13.TabIndex = 36
        Me.Button13.Text = "Search"
        Me.Button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button14.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button14.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button14.Location = New System.Drawing.Point(788, 496)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(86, 30)
        Me.Button14.TabIndex = 35
        Me.Button14.Text = "Close"
        Me.Button14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button15.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button15.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.Button15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button15.Location = New System.Drawing.Point(604, 496)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(86, 30)
        Me.Button15.TabIndex = 34
        Me.Button15.Text = "Refresh"
        Me.Button15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button15.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(9, 268)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(871, 200)
        Me.DataGridView1.TabIndex = 7
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 250)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(55, 15)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "Details :"
        '
        'grdReceivables
        '
        Me.grdReceivables.AllowUserToAddRows = False
        Me.grdReceivables.AllowUserToDeleteRows = False
        Me.grdReceivables.AllowUserToResizeColumns = False
        Me.grdReceivables.AllowUserToResizeRows = False
        Me.grdReceivables.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdReceivables.BackgroundColor = System.Drawing.Color.White
        Me.grdReceivables.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdReceivables.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.grdReceivables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdReceivables.Location = New System.Drawing.Point(9, 30)
        Me.grdReceivables.Name = "grdReceivables"
        Me.grdReceivables.ReadOnly = True
        Me.grdReceivables.RowHeadersVisible = False
        Me.grdReceivables.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdReceivables.Size = New System.Drawing.Size(871, 200)
        Me.grdReceivables.TabIndex = 5
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 12)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(122, 15)
        Me.Label20.TabIndex = 4
        Me.Label20.Text = "List of Receivables :"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TabPage2.Controls.Add(Me.btnPrintSSS)
        Me.TabPage2.Controls.Add(Me.Button16)
        Me.TabPage2.Controls.Add(Me.Button17)
        Me.TabPage2.Controls.Add(Me.Button18)
        Me.TabPage2.Controls.Add(Me.grdSSSDetails)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.grdSSS)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(888, 575)
        Me.TabPage2.TabIndex = 8
        Me.TabPage2.Text = "Government Loans"
        '
        'btnPrintSSS
        '
        Me.btnPrintSSS.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintSSS.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.btnPrintSSS.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPrintSSS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintSSS.Location = New System.Drawing.Point(11, 496)
        Me.btnPrintSSS.Name = "btnPrintSSS"
        Me.btnPrintSSS.Size = New System.Drawing.Size(141, 30)
        Me.btnPrintSSS.TabIndex = 40
        Me.btnPrintSSS.Text = "Print Amortization"
        Me.btnPrintSSS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintSSS.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button16.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button16.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button16.Location = New System.Drawing.Point(696, 496)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(86, 30)
        Me.Button16.TabIndex = 39
        Me.Button16.Text = "Search"
        Me.Button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button17.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button17.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Button17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button17.Location = New System.Drawing.Point(788, 496)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(86, 30)
        Me.Button17.TabIndex = 38
        Me.Button17.Text = "Close"
        Me.Button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button18.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Button18.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.Button18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button18.Location = New System.Drawing.Point(604, 496)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(86, 30)
        Me.Button18.TabIndex = 37
        Me.Button18.Text = "Refresh"
        Me.Button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button18.UseVisualStyleBackColor = True
        '
        'grdSSSDetails
        '
        Me.grdSSSDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdSSSDetails.BackgroundColor = System.Drawing.Color.White
        Me.grdSSSDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdSSSDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.grdSSSDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSSSDetails.Location = New System.Drawing.Point(11, 270)
        Me.grdSSSDetails.Name = "grdSSSDetails"
        Me.grdSSSDetails.ReadOnly = True
        Me.grdSSSDetails.RowHeadersVisible = False
        Me.grdSSSDetails.Size = New System.Drawing.Size(871, 200)
        Me.grdSSSDetails.TabIndex = 11
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(8, 252)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(55, 15)
        Me.Label21.TabIndex = 10
        Me.Label21.Text = "Details :"
        '
        'grdSSS
        '
        Me.grdSSS.AllowUserToAddRows = False
        Me.grdSSS.AllowUserToDeleteRows = False
        Me.grdSSS.AllowUserToResizeColumns = False
        Me.grdSSS.AllowUserToResizeRows = False
        Me.grdSSS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdSSS.BackgroundColor = System.Drawing.Color.White
        Me.grdSSS.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle14.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdSSS.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.grdSSS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSSS.Location = New System.Drawing.Point(11, 32)
        Me.grdSSS.Name = "grdSSS"
        Me.grdSSS.ReadOnly = True
        Me.grdSSS.RowHeadersVisible = False
        Me.grdSSS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdSSS.Size = New System.Drawing.Size(871, 200)
        Me.grdSSS.TabIndex = 9
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(8, 14)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(115, 15)
        Me.Label22.TabIndex = 8
        Me.Label22.Text = "List of SSS Loans :"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 109
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Doc No."
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 109
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 108
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Debit"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 109
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Credit"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 109
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Principal"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 109
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Interest"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 108
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Service Fee"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 109
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Check"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 109
        '
        'frmVerification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(896, 665)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmVerification"
        Me.Text = "Account Inquiry"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.tabVerification.ResumeLayout(False)
        Me.CurrentLoan.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvSubsidiaryCurrent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.LoanHistory.ResumeLayout(False)
        Me.LoanHistory.PerformLayout()
        CType(Me.dgvLoanHistorySubsidiary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLoanHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Debit.ResumeLayout(False)
        Me.Debit.PerformLayout()
        Me.TBCDebitAccounts.ResumeLayout(False)
        Me.TBListAccounts.ResumeLayout(False)
        Me.TBListAccounts.PerformLayout()
        CType(Me.dgvDebitList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TBSoaAccounts.ResumeLayout(False)
        CType(Me.dgvSoaList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebitList1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebitDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Credit.ResumeLayout(False)
        Me.Credit.PerformLayout()
        CType(Me.txtLine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCreditDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCredit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Amortization.ResumeLayout(False)
        Me.Amortization.PerformLayout()
        CType(Me.dgvAmortizationBalances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAmortizationSchedule, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdReceivables, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.grdSSSDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdSSS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblClientname As System.Windows.Forms.Label
    Friend WithEvents lblIDno As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tabVerification As System.Windows.Forms.TabControl
    Friend WithEvents CurrentLoan As System.Windows.Forms.TabPage
    Friend WithEvents LoanHistory As System.Windows.Forms.TabPage
    Friend WithEvents Debit As System.Windows.Forms.TabPage
    Friend WithEvents Credit As System.Windows.Forms.TabPage
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dgvDebitDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgvCreditDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dgvCredit As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dgvLoans As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Amortization As System.Windows.Forms.TabPage
    Friend WithEvents dgvAmortizationBalances As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgvAmortizationSchedule As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvSubsidiaryCurrent As System.Windows.Forms.DataGridView
    Friend WithEvents dgvLoanHistorySubsidiary As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgvLoanHistory As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents SOA As System.Windows.Forms.TabPage
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblLoanType As System.Windows.Forms.Label
    Friend WithEvents lblLoanRef As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSelectedLoanPrint As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtBalCreditAcct As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalPayment As System.Windows.Forms.TextBox
    Friend WithEvents txtBalServicefee As System.Windows.Forms.TextBox
    Friend WithEvents txtBalPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtBalInterest As System.Windows.Forms.TextBox
    Friend WithEvents btnPrintStatement As System.Windows.Forms.Button
    Friend WithEvents btnPrintLoanLedger As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents txtLine As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPrintPassbook As System.Windows.Forms.Button
    Friend WithEvents TBCDebitAccounts As System.Windows.Forms.TabControl
    Friend WithEvents TBListAccounts As System.Windows.Forms.TabPage
    Friend WithEvents dgvDebitList As System.Windows.Forms.DataGridView
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TBSoaAccounts As System.Windows.Forms.TabPage
    Friend WithEvents dgvSoaList As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDebitList1 As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents grdReceivables As System.Windows.Forms.DataGridView
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdSSSDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents grdSSS As System.Windows.Forms.DataGridView
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents btnPrintSSS As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
End Class
