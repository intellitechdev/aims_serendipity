﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmPrint

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private Sub frmPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptsummary.Close()
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Sub LoadREport(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\StatementofAccounts.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@IDNo", empno, "soa_debit")
            rptsummary.SetParameterValue("@IDNo", empno, "soa_credit")
            rptsummary.SetParameterValue("@IDNo", empno, "soa_general")
            rptsummary.SetParameterValue("@IDNo", empno, "SOA_Comakers.rpt")
            rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_LoanHistory.rpt")
        Catch ex As Exception
            MsgBox("Error : " + ex.ToString)
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Public Sub LoadLoanAmortization(ByVal empno As String, ByVal loanno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\LoanAmortization.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@LoanNo", loanno)
        Catch ex As Exception
            MsgBox("Error : " + ex.ToString)
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub

    Public Sub LoadLoanLedger(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\LoanLedger.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_SummaryAmountsDue.rpt")
            rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_LoanHistory.rpt")
        Catch ex As Exception
            MsgBox("Error : " + ex.ToString)
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub PrintToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintToolStripMenuItem.Click
        CrvRpt.PrintReport()
    End Sub
End Class