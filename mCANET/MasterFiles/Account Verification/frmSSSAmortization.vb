﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmSSSAmortization
    Public xRef As String
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private Sub frmSSSAmortization_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Sub LoadREport(ByVal refno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\ViewSSSAmortization.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@FcDocNumber", refno)
        Catch ex As Exception
            MsgBox("Error : " + ex.ToString)
        End Try

        'CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadREport(xRef)
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        If Err.Number <> 0 Then Exit Sub
        Me.CrvRpt.Visible = True
        Me.CrvRpt.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub PrintToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintToolStripMenuItem.Click
        CrvRpt.PrintReport()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class