'----------------------------------------------------------------------------
'Author: Dadulla, Joshua
'Date: 2/3/2011
'Description: Masterfile: Insert, Update, Delete of Statutory Allocations
'----------------------------------------------------------------------------

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmStatutoryAllocations
    Private gCon As New Clsappconfiguration()

    Const AddAllocation As String = "Insert"
    Const EditAllocation As String = "Update"

#Region "Properties"
    Private allocationID As String
    Public Property GetAllocationID() As String
        Get
            Return allocationID
        End Get
        Set(ByVal value As String)
            allocationID = value
        End Set
    End Property
#End Region

#Region "Events"
    'Insert Allocation
    Private Sub toolStripNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStripNew.Click
        frmStatutoryAllocations_AddEdit.GetMode() = AddAllocation
        frmStatutoryAllocations_AddEdit.MdiParent = frmMain
        frmStatutoryAllocations_AddEdit.Show()
    End Sub
    Private Sub contextNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles contextNew.Click
        frmStatutoryAllocations_AddEdit.GetMode() = AddAllocation
        frmStatutoryAllocations_AddEdit.MdiParent = frmMain
        frmStatutoryAllocations_AddEdit.Show()
    End Sub

    'Edit Allocation
    Private Sub toolStripEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStripEdit.Click
        Call EditCurrentAllocation()
    End Sub
    Private Sub contextEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles contextEdit.Click
        Call EditCurrentAllocation()
    End Sub
    Private Sub grdStatutoryAlloc_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdStatutoryAlloc.DoubleClick
        Call EditCurrentAllocation()
    End Sub

    'Delete Allocation
    Private Sub toolStripDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStripDelete.Click
        Call DeleteCurrentAllocation()
        Call LoadStatutoryAllocations()
        Call FormatGridColumns()
    End Sub
    Private Sub contextDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles contextDelete.Click
        Call DeleteCurrentAllocation()
        Call LoadStatutoryAllocations()
        Call FormatGridColumns()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub frmStatutoryAllocations_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadStatutoryAllocations()
        Call FormatGridColumns()
    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Close()
    End Sub

    Private Sub SystemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SystemToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub frmStatutoryAllocations_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Call LoadStatutoryAllocations()
        Call FormatGridColumns()
    End Sub
#End Region

#Region "Functions and Subroutines"
    Private Sub LoadStatutoryAllocations()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_StatutoryAllocations", _
                New SqlParameter("@companyID", gCompanyID()))

        grdStatutoryAlloc.DataSource = ds.Tables(0)
    End Sub

    Private Sub FormatGridColumns()
        With grdStatutoryAlloc
            .Columns("pkStatutory").Visible = False

            .Columns("fcStatutoryAllocations").HeaderText = "Allocation"
            .Columns("fnPercentAllocation").HeaderText = "Percent"
            .Columns("fcCategory").HeaderText = "Category"

            .Columns("fnPercentAllocation").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub

    Private Sub EditCurrentAllocation()
        Dim rowIndex As Integer = grdStatutoryAlloc.CurrentCell.RowIndex
        GetAllocationID() = grdStatutoryAlloc.Item("pkStatutory", rowIndex).Value.ToString()

        If GetAllocationID() IsNot Nothing Then
            With frmStatutoryAllocations_AddEdit
                .GetAllocationID() = Me.GetAllocationID()
                .GetAllocationName() = grdStatutoryAlloc.Item("fcStatutoryAllocations", rowIndex).Value.ToString()
                .GetPercentage() = grdStatutoryAlloc.Item("fnPercentAllocation", rowIndex).Value
                .GetCategory() = grdStatutoryAlloc.Item("fcCategory", rowIndex).Value

                .GetMode() = EditAllocation
                .txtAllocation.Text = .GetAllocationName()
                .txtPercentage.Text = .GetPercentage()
                .cboCategory.Text = .GetCategory()

                .MdiParent = frmMain
                .Show()
            End With
        Else
            MessageBox.Show("User Error: Please select an allocation to edit first.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub DeleteCurrentAllocation()

        Try
            If MessageBox.Show("Are you sure you want to delete allocation?", "Delete Allocation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim rowIndex As Integer = grdStatutoryAlloc.CurrentCell.RowIndex
                GetAllocationID() = grdStatutoryAlloc.Item("pkStatutory", rowIndex).Value.ToString()

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_StatutoryAllocations_Delete", _
                        New SqlParameter("@allocationID", GetAllocationID()))

                MessageBox.Show("Delete Successful!", "Delete Allocation", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
#End Region

End Class