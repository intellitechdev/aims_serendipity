Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_vendorTypeAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeySuplierType As String

    Public Property KeySupplierType() As String
        Get
            Return Me.sKeySuplierType
        End Get
        Set(ByVal value As String)
            sKeySuplierType = value
        End Set
    End Property

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        updateSupplierType()
        m_supplierTypeList(frm_acc_vendorType.chkIncludeInactive.CheckState, frm_acc_vendorType.grdSupplierType)
        m_loadSupplierType(frm_vend_masterVendorAddEdit.cboTypeName, frm_vend_masterVendorAddEdit.cboTypeID)
        m_chkSettings(frm_acc_vendorType.chkIncludeInactive, frm_acc_vendorType.SQLQuery)
    End Sub

    Private Sub updateSupplierType()
        Dim sSQLCmd As String = "SPU_SupplierType_Update "
        sSQLCmd &= "@fxKeySupplierType='" & sKeySuplierType & "',"
        sSQLCmd &= "@fcTypeName='" & txtSupplierType.Text & "',"
        sSQLCmd &= "@fbActive='" & IIf(chkInactive.Checked = True, 0, 1) & "'"
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Supplier Type successfully updated!")
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Supplier Type")
        End Try
    End Sub

    Private Sub frm_MF_vendorTypeAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadType()
    End Sub

    Private Sub loadType()
        Dim sSQLCmd As String = "SELECT fxKeySupplierType,fcTypeName,fbActive FROM mSupplier01Type WHERE fxKeySupplierType = '" & sKeySuplierType & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtSupplierType.Text = rd.Item("fcTypeName")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Supplier Type Master File")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class