﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterFile_Accounts
    Public Active As Integer

    Public Property GetActive() As Integer
        Get
            Return Active
        End Get
        Set(ByVal value As Integer)
            Active = value
        End Set
    End Property
#Region "DebitCreditLIST"
    Private Sub ListDebitCredit()
        Dim mycon As New Clsappconfiguration
        Dim co_id As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_DebitCredit_SelectAll", _
                                                        New SqlParameter("@fxkey_coid", co_id))

            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.Columns(0).Visible = False
            GrdAccounts.Columns(1).Width = 100
            GrdAccounts.Columns(1).HeaderText = "Account Code"
            GrdAccounts.Columns(1).ReadOnly = True
            GrdAccounts.Columns(2).HeaderText = "Description"
            GrdAccounts.Columns(2).ReadOnly = True
            GrdAccounts.Columns(2).Width = 270
            GrdAccounts.Columns(3).HeaderText = "Type"
            GrdAccounts.Columns(3).ReadOnly = True
            GrdAccounts.Columns(3).Width = 50
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ListDebitCreatebyID(ByVal Id As String)
        Dim mycon As New Clsappconfiguration
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_DebitCreate_ListByID",
                                                         New SqlParameter("@fcAccountCode", Id))
            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.Columns(0).Visible = False
            GrdAccounts.Columns(1).Width = 100
            GrdAccounts.Columns(1).HeaderText = "Account Code"
            GrdAccounts.Columns(1).ReadOnly = True
            GrdAccounts.Columns(2).HeaderText = "Description"
            GrdAccounts.Columns(2).ReadOnly = True
            GrdAccounts.Columns(2).Width = 270
            GrdAccounts.Columns(3).HeaderText = "Type"
            GrdAccounts.Columns(3).ReadOnly = True
            GrdAccounts.Columns(3).Width = 50
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List by ID", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ListDebitCreditByDescription(ByVal des As String)
        Dim mycon As New Clsappconfiguration
        Dim co_id As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_DebitCreate_ListByDescription",
                                                         New SqlParameter("@fcAccountName", des), _
                                                         New SqlParameter("@co_id", co_id))
            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.Columns(0).Visible = False
            GrdAccounts.Columns(1).Width = 100
            GrdAccounts.Columns(1).HeaderText = "Account Code"
            GrdAccounts.Columns(1).ReadOnly = True
            GrdAccounts.Columns(2).HeaderText = "Description"
            GrdAccounts.Columns(2).ReadOnly = True
            GrdAccounts.Columns(2).Width = 270
            GrdAccounts.Columns(3).HeaderText = "Type"
            GrdAccounts.Columns(3).ReadOnly = True
            GrdAccounts.Columns(3).Width = 50
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List Description", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
#Region "ACCOUNTLIST"
    Private Sub AccountCode()
        Dim mycon As New Clsappconfiguration
        Dim co_id As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_Load",
                                                     New SqlParameter("@accountID", co_id))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).HeaderText = "Account Code and Description"
                .Columns(3).Width = 406
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "AccountCode", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AccountCodeByID(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadByID",
                                                     New SqlParameter("@acnt_code", ID))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).HeaderText = "Account Code and Description"
                .Columns(3).Width = 406
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "AccountCode", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AccountCodeByDescription(ByVal Des As String)
        Dim mycon As New Clsappconfiguration
        Dim co_id As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadByDescription",
                                                     New SqlParameter("@acnt_name", Des),
                                                     New SqlParameter("@coid", co_id))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).HeaderText = "Account Code and Description"
                .Columns(3).Width = 406
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "AccountCode", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

    Private Sub frmMasterFile_Accounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Active <> 2 Then
            AccountCode()
        ElseIf Active = 2 Then
            ListDebitCredit()
        End If
    End Sub
    Private Sub GrdAccounts_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrdAccounts.DoubleClick
        If Active = 1 Then
            If GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterFile_DebitCredit.fxid = row.Cells(0).Value.ToString
                    frmMasterFile_DebitCredit.txtaccountcode.Text = row.Cells(1).Value.ToString
                    frmMasterFile_DebitCredit.txtaccountname.Text = row.Cells(2).Value.ToString
                Next
                Me.Close()
            End If
        ElseIf Active = 2 Then
            If GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterfile_AccountRegister.GetAccountID = row.Cells(0).Value.ToString
                    frmMasterfile_AccountRegister.txtaccountcode.Text = row.Cells(1).Value.ToString
                    frmMasterfile_AccountRegister.txtaccountname.Text = row.Cells(2).Value.ToString
                Next
                Me.Close()
            End If
        End If
    End Sub

    Private Sub txtAccountNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAccountNo.TextChanged
        If Active <> 2 Then
            AccountCodeByID(txtAccountNo.Text)
        ElseIf Active = 2 Then
            ListDebitCreatebyID(txtAccountNo.Text)
        End If
    End Sub

    Private Sub txtDescription_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescription.TextChanged
        If Active <> 2 Then
            AccountCodeByDescription(txtDescription.Text)
        ElseIf Active = 2 Then
            ListDebitCreditByDescription(txtDescription.Text)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        'If Active = 1 Then
        '    If GrdAccounts.SelectedCells.Count <> 0 Then
        '        For Each row As DataGridViewRow In GrdAccounts.SelectedRows
        '            frmMasterFile_DebitCredit.fxid = row.Cells(0).Value.ToString
        '            frmMasterFile_DebitCredit.txtaccountcode.Text = row.Cells(1).Value.ToString
        '            frmMasterFile_DebitCredit.txtaccountname.Text = row.Cells(2).Value.ToString
        '        Next
        '        Me.Close()
        '    End If
        'ElseIf Active = 2 Then
        '    If GrdAccounts.SelectedCells.Count <> 0 Then
        '        For Each row As DataGridViewRow In GrdAccounts.SelectedRows
        '            frmMasterfile_AccountRegister.GetAccountID = row.Cells(0).Value.ToString
        '            frmMasterfile_AccountRegister.txtaccountcode.Text = row.Cells(1).Value.ToString
        '            frmMasterfile_AccountRegister.txtaccountname.Text = row.Cells(2).Value.ToString
        '            frmMasterfile_AccountRegister.TextBox1.Text = row.Cells(0).Value.ToString()
        '        Next
        '        Me.Close()
        '    End If
        'End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class