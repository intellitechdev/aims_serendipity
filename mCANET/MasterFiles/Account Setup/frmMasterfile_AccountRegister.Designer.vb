﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterfile_AccountRegister
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterfile_AccountRegister))
        Me.txtsearch = New System.Windows.Forms.TextBox()
        Me.btnsearch = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.txtAccountNumber = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnBrowseEmployee = New System.Windows.Forms.Button()
        Me.txtSchedule = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.btnBrowseAccount = New System.Windows.Forms.Button()
        Me.txtaccountname = New System.Windows.Forms.TextBox()
        Me.txtaccountcode = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvListAccountRegister = New System.Windows.Forms.DataGridView()
        Me.btnBrowseAccountNumber = New System.Windows.Forms.Button()
        Me.CBClosedList = New System.Windows.Forms.CheckBox()
        Me.CBclose = New System.Windows.Forms.CheckBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.cbDeducted = New System.Windows.Forms.CheckBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccountNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Schedule = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcClosed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcDeducted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvListAccountRegister, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtsearch
        '
        Me.txtsearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtsearch.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearch.Location = New System.Drawing.Point(468, 436)
        Me.txtsearch.Name = "txtsearch"
        Me.txtsearch.Size = New System.Drawing.Size(153, 27)
        Me.txtsearch.TabIndex = 139
        '
        'btnsearch
        '
        Me.btnsearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnsearch.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch.Location = New System.Drawing.Point(397, 435)
        Me.btnsearch.Name = "btnsearch"
        Me.btnsearch.Size = New System.Drawing.Size(66, 28)
        Me.btnsearch.TabIndex = 133
        Me.btnsearch.Text = "Search"
        Me.btnsearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch.UseVisualStyleBackColor = True
        '
        'btnclose
        '
        Me.btnclose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnclose.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(771, 435)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 132
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btndelete.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(12, 435)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(66, 28)
        Me.btndelete.TabIndex = 131
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnupdate.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(699, 435)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 130
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnsave.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(627, 435)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(66, 28)
        Me.btnsave.TabIndex = 129
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'txtAccountNumber
        '
        Me.txtAccountNumber.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNumber.Location = New System.Drawing.Point(146, 13)
        Me.txtAccountNumber.Name = "txtAccountNumber"
        Me.txtAccountNumber.ReadOnly = True
        Me.txtAccountNumber.Size = New System.Drawing.Size(153, 27)
        Me.txtAccountNumber.TabIndex = 141
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(16, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(124, 19)
        Me.Label5.TabIndex = 140
        Me.Label5.Text = "Account Number :"
        '
        'btnBrowseEmployee
        '
        Me.btnBrowseEmployee.Enabled = False
        Me.btnBrowseEmployee.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowseEmployee.Location = New System.Drawing.Point(499, 80)
        Me.btnBrowseEmployee.Name = "btnBrowseEmployee"
        Me.btnBrowseEmployee.Size = New System.Drawing.Size(42, 28)
        Me.btnBrowseEmployee.TabIndex = 139
        Me.btnBrowseEmployee.Text = "...."
        Me.btnBrowseEmployee.UseVisualStyleBackColor = True
        '
        'txtSchedule
        '
        Me.txtSchedule.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSchedule.Location = New System.Drawing.Point(145, 113)
        Me.txtSchedule.Name = "txtSchedule"
        Me.txtSchedule.Size = New System.Drawing.Size(348, 27)
        Me.txtSchedule.TabIndex = 138
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 19)
        Me.Label4.TabIndex = 137
        Me.Label4.Text = "Schedule:"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(145, 80)
        Me.txtName.Name = "txtName"
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(348, 27)
        Me.txtName.TabIndex = 136
        '
        'btnBrowseAccount
        '
        Me.btnBrowseAccount.Enabled = False
        Me.btnBrowseAccount.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseAccount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowseAccount.Location = New System.Drawing.Point(499, 49)
        Me.btnBrowseAccount.Name = "btnBrowseAccount"
        Me.btnBrowseAccount.Size = New System.Drawing.Size(42, 28)
        Me.btnBrowseAccount.TabIndex = 135
        Me.btnBrowseAccount.Text = "...."
        Me.btnBrowseAccount.UseVisualStyleBackColor = True
        '
        'txtaccountname
        '
        Me.txtaccountname.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaccountname.Location = New System.Drawing.Point(145, 48)
        Me.txtaccountname.Name = "txtaccountname"
        Me.txtaccountname.ReadOnly = True
        Me.txtaccountname.Size = New System.Drawing.Size(348, 27)
        Me.txtaccountname.TabIndex = 134
        '
        'txtaccountcode
        '
        Me.txtaccountcode.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaccountcode.Location = New System.Drawing.Point(487, 15)
        Me.txtaccountcode.Name = "txtaccountcode"
        Me.txtaccountcode.ReadOnly = True
        Me.txtaccountcode.Size = New System.Drawing.Size(153, 27)
        Me.txtaccountcode.TabIndex = 133
        Me.txtaccountcode.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 19)
        Me.Label3.TabIndex = 132
        Me.Label3.Text = "Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 19)
        Me.Label2.TabIndex = 131
        Me.Label2.Text = "Account Title :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(357, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(106, 19)
        Me.Label6.TabIndex = 130
        Me.Label6.Text = "Account Code :"
        Me.Label6.Visible = False
        '
        'dgvListAccountRegister
        '
        Me.dgvListAccountRegister.AllowUserToAddRows = False
        Me.dgvListAccountRegister.AllowUserToDeleteRows = False
        Me.dgvListAccountRegister.AllowUserToResizeColumns = False
        Me.dgvListAccountRegister.AllowUserToResizeRows = False
        Me.dgvListAccountRegister.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvListAccountRegister.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvListAccountRegister.BackgroundColor = System.Drawing.Color.White
        Me.dgvListAccountRegister.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvListAccountRegister.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AccountNumber, Me.fcID, Me.fcName, Me.Account, Me.Schedule, Me.fcClosed, Me.fcDeducted})
        Me.dgvListAccountRegister.Location = New System.Drawing.Point(7, 176)
        Me.dgvListAccountRegister.Name = "dgvListAccountRegister"
        Me.dgvListAccountRegister.RowHeadersVisible = False
        Me.dgvListAccountRegister.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvListAccountRegister.Size = New System.Drawing.Size(830, 253)
        Me.dgvListAccountRegister.TabIndex = 129
        '
        'btnBrowseAccountNumber
        '
        Me.btnBrowseAccountNumber.Enabled = False
        Me.btnBrowseAccountNumber.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseAccountNumber.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowseAccountNumber.Location = New System.Drawing.Point(305, 13)
        Me.btnBrowseAccountNumber.Name = "btnBrowseAccountNumber"
        Me.btnBrowseAccountNumber.Size = New System.Drawing.Size(42, 28)
        Me.btnBrowseAccountNumber.TabIndex = 142
        Me.btnBrowseAccountNumber.Text = "...."
        Me.btnBrowseAccountNumber.UseVisualStyleBackColor = True
        '
        'CBClosedList
        '
        Me.CBClosedList.AutoSize = True
        Me.CBClosedList.BackColor = System.Drawing.Color.Transparent
        Me.CBClosedList.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBClosedList.Location = New System.Drawing.Point(764, 15)
        Me.CBClosedList.Name = "CBClosedList"
        Me.CBClosedList.Size = New System.Drawing.Size(69, 22)
        Me.CBClosedList.TabIndex = 143
        Me.CBClosedList.Text = "Closed"
        Me.CBClosedList.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBClosedList.UseVisualStyleBackColor = False
        '
        'CBclose
        '
        Me.CBclose.BackColor = System.Drawing.Color.Transparent
        Me.CBclose.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBclose.Enabled = False
        Me.CBclose.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBclose.Location = New System.Drawing.Point(20, 146)
        Me.CBclose.Name = "CBclose"
        Me.CBclose.Size = New System.Drawing.Size(97, 24)
        Me.CBclose.TabIndex = 144
        Me.CBclose.Text = "Closed"
        Me.CBclose.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(512, 117)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(181, 20)
        Me.TextBox1.TabIndex = 145
        Me.TextBox1.Visible = False
        '
        'cbDeducted
        '
        Me.cbDeducted.BackColor = System.Drawing.Color.Transparent
        Me.cbDeducted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbDeducted.Enabled = False
        Me.cbDeducted.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDeducted.Location = New System.Drawing.Point(134, 149)
        Me.cbDeducted.Name = "cbDeducted"
        Me.cbDeducted.Size = New System.Drawing.Size(116, 19)
        Me.cbDeducted.TabIndex = 146
        Me.cbDeducted.Text = "Deducted"
        Me.cbDeducted.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Account Number"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 138
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 138
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 137
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Account"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 138
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Schedule"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 138
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Closed"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 138
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Deducted"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 118
        '
        'AccountNumber
        '
        Me.AccountNumber.HeaderText = "Account Number"
        Me.AccountNumber.Name = "AccountNumber"
        Me.AccountNumber.ReadOnly = True
        '
        'fcID
        '
        Me.fcID.HeaderText = "ID"
        Me.fcID.Name = "fcID"
        Me.fcID.ReadOnly = True
        '
        'fcName
        '
        Me.fcName.HeaderText = "Name"
        Me.fcName.Name = "fcName"
        Me.fcName.ReadOnly = True
        '
        'Account
        '
        Me.Account.HeaderText = "Account"
        Me.Account.Name = "Account"
        Me.Account.ReadOnly = True
        '
        'Schedule
        '
        Me.Schedule.HeaderText = "Schedule"
        Me.Schedule.Name = "Schedule"
        Me.Schedule.ReadOnly = True
        '
        'fcClosed
        '
        Me.fcClosed.HeaderText = "Closed"
        Me.fcClosed.Name = "fcClosed"
        Me.fcClosed.ReadOnly = True
        '
        'fcDeducted
        '
        Me.fcDeducted.HeaderText = "Deducted"
        Me.fcDeducted.Name = "fcDeducted"
        '
        'frmMasterfile_AccountRegister
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.CSAcctg.My.Resources.Resources.Project_Name_Panel
        Me.ClientSize = New System.Drawing.Size(845, 471)
        Me.Controls.Add(Me.cbDeducted)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.txtsearch)
        Me.Controls.Add(Me.btnsearch)
        Me.Controls.Add(Me.btnclose)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.CBclose)
        Me.Controls.Add(Me.btnupdate)
        Me.Controls.Add(Me.CBClosedList)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.txtAccountNumber)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnBrowseEmployee)
        Me.Controls.Add(Me.txtSchedule)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.btnBrowseAccount)
        Me.Controls.Add(Me.txtaccountname)
        Me.Controls.Add(Me.txtaccountcode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dgvListAccountRegister)
        Me.Controls.Add(Me.btnBrowseAccountNumber)
        Me.Name = "frmMasterfile_AccountRegister"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Account Register"
        CType(Me.dgvListAccountRegister, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents txtAccountNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseEmployee As System.Windows.Forms.Button
    Friend WithEvents txtSchedule As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowseAccount As System.Windows.Forms.Button
    Friend WithEvents txtaccountname As System.Windows.Forms.TextBox
    Friend WithEvents txtaccountcode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgvListAccountRegister As System.Windows.Forms.DataGridView
    Friend WithEvents btnBrowseAccountNumber As System.Windows.Forms.Button
    Friend WithEvents CBClosedList As System.Windows.Forms.CheckBox
    Friend WithEvents CBclose As System.Windows.Forms.CheckBox
    Friend WithEvents btnsearch As System.Windows.Forms.Button
    Friend WithEvents txtsearch As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents AccountNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Schedule As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcClosed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcDeducted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbDeducted As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
