﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterFile_DebitCredit
    Private fxKeyAccount As String
    Dim mycon As New Clsappconfiguration
    Public fxid As String
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        frmMasterFile_Accounts.GetActive = 1
        frmMasterFile_Accounts.ShowDialog()
        With frmMasterFile_Accounts
            If .Active = 1 Then
                If .GrdAccounts.SelectedCells.Count <> 0 Then
                    If frmMasterFile_Accounts.DialogResult = DialogResult.OK Then
                        For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
                            fxid = row.Cells(0).Value.ToString
                            txtaccountcode.Text = row.Cells(1).Value.ToString
                            txtaccountname.Text = row.Cells(2).Value.ToString
                            TextBox1.Text = fxid
                        Next
                    End If
                End If
            End If
        End with
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If btnsave.Text = "New" Then
            ControlSetup("New")
        Else
            ControlSetup("Save")
        End If
    End Sub
    Private Sub ControlSetup(ByVal mode As String)

        If mode = "Close" Then
            Me.Close()
        End If
        If mode = "New" Then
            btnupdate.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Text = "Save"
            GroupBox1.Enabled = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            Exit Sub
        End If
        If mode = "Save" Then
            AddNewDebitCreditAccount(fxid, txtaccountcode.Text, txtaccountname.Text, cbotype.Text, gCompanyID)
            AuditTrail_Save("DEBIT/CREDIT ACCOUNTS", "Add " & txtaccountname.Text & "' as new " & cbotype.Text & " account")
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            GroupBox1.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            ListDebitCredit()
            Exit Sub
        End If
        'If mode = "Close" Then
        '    Me.Close()
        'End If
        If mode = "Cancel" Then
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnsave.Enabled = True
            GroupBox1.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            Exit Sub
        End If
        If mode = "Edit" Then
            btnupdate.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Enabled = False
            GroupBox1.Enabled = True
            Exit Sub
        End If
        If mode = "Update" Then
            UpdateDebitCreditAccount(fxid, txtaccountcode.Text, txtaccountname.Text, cbotype.Text)
            AuditTrail_Save("DEBIT/CREDIT ACCOUNTS", "Change '" & txtaccountname.Text & "' type to " & cbotype.Text)
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            GroupBox1.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            ListDebitCredit()
            Exit Sub
        End If
        If mode = "Delete" Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                DeleteDebitCreditAccount(fxid)
                AuditTrail_Save("DEBIT/CREDIT ACCOUNTS", "Delete '" & txtaccountname.Text & "'")
                txtaccountcode.Clear()
                txtaccountname.Clear()
                cbotype.Text = ""
                ListDebitCredit()
            Else
                Exit Sub
            End If
        End If
    End Sub
    Private Sub AddNewDebitCreditAccount(ByVal Key_Account As String,
                                         ByVal code As String,
                                         ByVal name As String,
                                         ByVal type As String,
                                         ByVal Key_coid As String)
        Dim MyGuid As New Guid(Key_Account)
        Dim MyGuids As New Guid(Key_coid)
        If txtaccountcode.Text <> "" And txtaccountname.Text <> "" Then
            Try
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_DebitCredit_Addnew",
                                          New SqlParameter("@fxKeyAccountID", MyGuid),
                                          New SqlParameter("@fxKey_coid", MyGuids),
                                          New SqlParameter("@fcAccountCode", code),
                                          New SqlParameter("@fcAccountName", name),
                                          New SqlParameter("@fcAccountType", type))
                MessageBox.Show("Record Succesfully Added!")
            Catch ex As Exception
                MessageBox.Show("Error! " + ex.Message)
            End Try
        End If
    End Sub
    Private Sub UpdateDebitCreditAccount(ByVal Key_Account As String,
                                         ByVal code As String,
                                         ByVal name As String,
                                         ByVal type As String)
        Dim MyGuid As New Guid(Key_Account)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_DebitCredit_Edit",
                                      New SqlParameter("@fxKeyAccountID", MyGuid),
                                      New SqlParameter("@fcAccountCode", code),
                                      New SqlParameter("@fcAccountName", name),
                                      New SqlParameter("@fcAccountType", type))
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString)
        End Try
    End Sub

    Private Sub DeleteDebitCreditAccount(ByVal Key_Account As String)
        Dim MyGuid As New Guid(Key_Account)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_DebitCredit_Delete",
                                      New SqlParameter("@fxKeyAccountID", MyGuid))
            MessageBox.Show("Record Succesfully Deleted!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString)
        End Try
    End Sub

    Private Sub ListDebitCredit()
        Dim MyGuid As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_DebitCredit_SelectAll", _
                                                     New SqlParameter("@fxkey_coid", MyGuid))

        With dgvlist
            .DataSource = ds.Tables(0)
            .Columns(0).Visible = False

            .Columns(1).HeaderText = "Account Code"
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 50
            .Columns(2).HeaderText = "Account Name"
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 200
            .Columns(3).HeaderText = "Account Type"
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 80
        End With
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If btnupdate.Text = "Edit" Then
            fxid = dgvlist.SelectedRows(0).Cells(0).Value.ToString
            txtaccountcode.Text = dgvlist.SelectedRows(0).Cells(1).Value.ToString
            txtaccountname.Text = dgvlist.SelectedRows(0).Cells(2).Value.ToString
            cbotype.Text = dgvlist.SelectedRows(0).Cells(3).Value.ToString
            ControlSetup("Edit")
            Exit Sub
        End If
        If btnupdate.Text = "Update" Then
            ControlSetup("Update")
            Exit Sub
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        ControlSetup("Delete")
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub

    Private Sub FormAccountSetupDebitCredit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListDebitCredit()
        GroupBox1.Enabled = False
    End Sub

    Private Sub dgvlist_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvlist.CellClick
        fxid = dgvlist.Rows(0).Cells(0).Value.ToString()
    End Sub

    Private Sub txtaccountname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtaccountname.TextChanged

    End Sub
End Class