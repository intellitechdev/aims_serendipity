﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports CrystalDecisions.Shared

Public Class frmMasterfile_AccountRegister
    Private AccountKey As String
    Private EmployeeId As String
    Private fxAccountKeyRegister As String
    Private mycon As New Clsappconfiguration
    Dim Isclosed As Boolean
#Region "Property"
    Public Property GetAccountID() As String
        Get
            Return AccountKey
        End Get
        Set(ByVal value As String)
            AccountKey = value
        End Set
    End Property

    Public Property GetfxAccountKeyRegister() As String
        Get
            Return fxAccountKeyRegister
        End Get
        Set(ByVal value As String)
            fxAccountKeyRegister = value
        End Set
    End Property
    Public Property GetEmployeeID() As String
        Get
            Return EmployeeId
        End Get
        Set(ByVal value As String)
            EmployeeId = value
        End Set
    End Property
#End Region
    Private Sub ControlSetup(ByVal mode As String)
      
        If mode = "Cancel" Then
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnsave.Enabled = True
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            CBclose.Checked = False
            CBclose.Enabled = False
            cbDeducted.Checked = False
            cbDeducted.Enabled = False
            Exit Sub
        End If
        If mode = "Close" Then
            Me.Close()
        End If
        If btnupdate.Text = "Update" Then
            Update_AccountRegister(fxAccountKeyRegister, txtAccountNumber.Text, AccountKey, EmployeeId, txtName.Text, Convert.ToDecimal(txtSchedule.Text), CBclose.Checked, gCompanyID(), cbDeducted.Checked)
            UpdateAccountInAccounting(txtAccountNumber.Text, Date.Now)
            Load_AccountRegister(CBClosedList.Checked, gCompanyID())
            AuditTrail_Save("ACCOUNT REGISTER", "Update account account register " & txtAccountNumber.Text & " | " & txtaccountname.Text & " | " & txtName.Text)
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.Enabled = False
            txtSchedule.Clear()
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            CBclose.Checked = False
            CBclose.Enabled = False
            Exit Sub
        End If
        If btnupdate.Text = "Edit" Then
            btnupdate.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            txtSchedule.Enabled = True
            btnsave.Enabled = False
            btnBrowseAccount.Enabled = True
            btnBrowseEmployee.Enabled = True
            btnBrowseAccountNumber.Enabled = True

            'txtSchedule.ReadOnly = False
            CBclose.Enabled = True
            cbDeducted.Enabled = True
            Exit Sub
        End If
    End Sub

    Private Sub btnBrowseAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccount.Click
        frmMasterFile_Accounts.GetActive = 2
        frmMasterFile_Accounts.ShowDialog()
        With frmMasterFile_Accounts
            'If .Active = 1 Then
            '    If .GrdAccounts.SelectedCells.Count <> 0 Then
            '        For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
            '            frmMasterFile_DebitCredit.fxid = row.Cells(0).Value.ToString
            '            frmMasterFile_DebitCredit.txtaccountcode.Text = row.Cells(1).Value.ToString
            '            frmMasterFile_DebitCredit.txtaccountname.Text = row.Cells(2).Value.ToString
            '        Next
            '        Me.Close()
            '    End If
            'ElseIf 
            If .Active = 2 Then
                If .GrdAccounts.SelectedCells.Count <> 0 Then
                    If frmMasterfile_AccountNumber.DialogResult = DialogResult.OK Then
                        For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
                            GetAccountID = row.Cells(0).Value.ToString
                            txtaccountcode.Text = row.Cells(1).Value.ToString
                            txtaccountname.Text = row.Cells(2).Value.ToString
                            TextBox1.Text = row.Cells(0).Value.ToString()
                        Next
                    End If
                End If
            End If
        End With
    End Sub

    Private Sub btnBrowseEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseEmployee.Click
        'frmMember_List.ShowDialog()
        frmMember_List.ShowDialog()
        With frmMember_List
            If .GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
                    GetEmployeeID = row.Cells(1).Value.ToString
                    txtName.Text = row.Cells(2).Value.ToString
                Next
            End If
        End With
    End Sub

    Private Sub btnBrowseAccountNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccountNumber.Click
        frmMasterfile_AccountNumber.GetID = 3
        frmMasterfile_AccountNumber.ShowDialog()
        If frmMasterfile_AccountNumber.GrdAccounts.SelectedCells.Count <> 0 Then
            If frmMasterfile_AccountNumber.DialogResult = DialogResult.OK Then
                If frmMasterfile_AccountNumber.ID = 3 Then
                    For Each row As DataGridViewRow In frmMasterfile_AccountNumber.GrdAccounts.SelectedRows
                        Me.DialogResult = DialogResult.OK
                        txtAccountNumber.Text = row.Cells(0).Value.ToString
                    Next
                End If
            End If
        End If
        'frmMasterfile_AccountNumber.GetID = 3
        'frmMasterfile_AccountNumber.ShowDialog()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim AccountKey As String

        If GetAccountID <> "" Then
            AccountKey = GetAccountID
        Else
            AccountKey = Guid.NewGuid.ToString
        End If

        If txtSchedule.Text = "" Then
            txtSchedule.Text = "0.00"
        End If
        If btnsave.Text = "New" Then
            btnupdate.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Text = "Save"
            btnBrowseAccount.Enabled = True
            btnBrowseEmployee.Enabled = True
            btnBrowseAccountNumber.Enabled = True

            txtSchedule.Enabled = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            'txtSchedule.Clear()
            CBclose.Checked = False
            CBclose.Enabled = True
            cbDeducted.Checked = False
            cbDeducted.Enabled = True
            Exit Sub
        End If
        If btnsave.Text = "Save" Then
            Add_AccountRegister(txtAccountNumber.Text, EmployeeId, Convert.ToDecimal(txtSchedule.Text), CBclose.Checked, gCompanyID(), cbDeducted.Checked)
            UpdateAccountInAccounting(txtAccountNumber.Text, Date.Now)
            AuditTrail_Save("ACCOUNT REGISTER", "Create new account account register " & txtAccountNumber.Text & " | " & txtaccountname.Text & " | " & txtName.Text)
            Load_AccountRegister(CBClosedList.Checked, gCompanyID())
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            CBclose.Checked = False
            CBclose.Enabled = False
            cbDeducted.Checked = False
            cbDeducted.Enabled = False
            Exit Sub

            'If btnsave.Text = "New" Then
            '    ControlSetup("New")
            'Else
            '    If txtaccountcode.Text <> "" And txtAccountNumber.Text <> "" And txtName.Text <> "" And txtSchedule.Text <> "" Then
            '        ControlSetup("Save")
        Else
            MessageBox.Show("Fill up form!", "Save Account Register", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        'End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If btnupdate.Text = "Edit" Then
            If dgvListAccountRegister.SelectedCells.Count <> 0 Then
                ControlSetup("Edit")
                Exit Sub
            End If
        End If
        On Error Resume Next
        If btnupdate.Text = "Update" Then
            ControlSetup("Update")
            Exit Sub
        End If
    End Sub
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
            Delete_AccountRegister(fxAccountKeyRegister, gCompanyID())
            Dim sSQLCmd As String = "update mDocNumber Set fdDateUsed= Null where fcDocNumber='" & txtAccountNumber.Text & "' "
            SqlHelper.ExecuteScalar(mycon.cnstring, CommandType.Text, sSQLCmd)
            AuditTrail_Save("ACCOUNT REGISTER", "Delete " & txtAccountNumber.Text & " | " & txtaccountname.Text & " | " & txtName.Text)
            Load_AccountRegister(CBClosedList.Checked, gCompanyID())
            btnBrowseAccount.Enabled = True
            btnBrowseEmployee.Enabled = True

            txtSchedule.Enabled = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            CBclose.Checked = False
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Update_AccountRegister(ByVal fxKeyAccountRegister As String, ByVal fcDocNumber As String, ByVal fxKey_Account As String,
                                        ByVal fk_Employee As String, ByVal EmpName As String, ByVal fnSchedule As Decimal, ByVal fbClosed As Boolean, ByVal fxkeyco_id As String, ByVal fbDeducted As Boolean)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim MyGuids As New Guid(fxAccountKeyRegister)
        Dim co_id As New Guid(fxkeyco_id)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Update",
                                      New SqlParameter("@fxKeyAccountRegister", MyGuids),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fcEmployeeNo", fk_Employee),
                                      New SqlParameter("@fcFullname", EmpName),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed),
                                      New SqlParameter("@co_id", co_id),
                                      New SqlParameter("@fbDeducted", fbDeducted))
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Add_AccountRegister(ByVal fcDocNumber As String, ByVal fk_Employee As String, ByVal fnSchedule As Double, ByVal fbClosed As Boolean, ByVal fxkey_coid As String, ByVal fbDeducted As Boolean)
        'Dim MyGuids As New Guid(fk_Employee)
        Dim MyGuid As New Guid(GetAccountID())
        Dim MyGuids As New Guid(fxkey_coid)
        'Dim key_account As New Guid(fxkeyAccount)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Insert",
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fxKey_coid", MyGuids),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fcEmployeeNo", fk_Employee),
                                      New SqlParameter("@fcFullname", txtName.Text),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed),
                                      New SqlParameter("@fbDeducted", fbDeducted))
            MessageBox.Show("Record Succesfully Added!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Add Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Delete_AccountRegister(ByVal fxKey_Account As String, ByVal fxkeyco_id As String)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim co_id As New Guid(fxkeyco_id)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Delete",
                                      New SqlParameter("@fxAccountKeyRegister", MyGuid),
                                      New SqlParameter("@co_id", co_id))
            MessageBox.Show("Record Succesfully Deleted!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Delete Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub UpdateAccountInAccounting(ByVal fcDocNumber As String, ByVal fdDateUsed As Date)
        Dim co_id As New Guid(gCompanyID)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_UpdateAccount",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fdDateUsed", fdDateUsed),
                                      New SqlParameter("@co_id", co_id))
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Document Number....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmMasterfile_AccountRegister_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvListAccountRegister.Columns.Clear()
        Load_AccountRegister(CBClosedList.Checked, gCompanyID())
    End Sub

    Private Sub dgvListAccountRegister_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListAccountRegister.CellClick
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
            cbDeducted.Checked = Row.Cells(8).Value
            TextBox1.Text = AccountKey
        Next
    End Sub

    Private Sub dgvListAccountRegister_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListAccountRegister.KeyUp
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
            cbDeducted.Checked = Row.Cells(8).Value
        Next
    End Sub
    Private Sub dgvListAccountRegister_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListAccountRegister.KeyDown
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
            cbDeducted.Checked = Row.Cells(8).Value
        Next
    End Sub

    Private Sub CBClosedList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBClosedList.CheckedChanged
        Load_AccountRegister(CBClosedList.Checked, gCompanyID)
    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        SearchList(txtsearch.Text, gCompanyID)
    End Sub

#Region "List"
    Public Sub ClosedList(ByVal Active As Boolean)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_ListClosed")
        dgvListAccountRegister.DataSource = ds.Tables(0)
    End Sub
    Public Sub SearchList(ByVal Search As String, ByVal fxkeyco_id As String)
        Dim co_id As New Guid(fxkeyco_id)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_Search", _
                                      New SqlParameter("@Search", txtsearch.Text), _
                                      New SqlParameter("@co_id", co_id))
        With dgvListAccountRegister
            .DataSource = ds.Tables(0)
            .Columns(0).Visible = False
            .Columns(1).Visible = False

            .Columns(2).HeaderText = "Account Number"
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(2).Width = 100
            .Columns(2).ReadOnly = True
            .Columns(3).HeaderText = "ID"
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(3).Width = 150
            .Columns(3).ReadOnly = True
            .Columns(4).HeaderText = "Name"
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(4).ReadOnly = True
            .Columns(5).HeaderText = "Account Title"
            .Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(5).ReadOnly = True
            .Columns(6).HeaderText = "Schedule"
            .Columns(6).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(6).ReadOnly = True
            .Columns(6).Width = 80
            .Columns(7).HeaderText = "Closed"
            .Columns(7).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(7).Width = 80
            .Columns(8).HeaderText = "Deducted"
            .Columns(8).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(8).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(8).Width = 80
        End With
    End Sub
    Private Sub Load_AccountRegister(ByVal Active As Boolean, ByVal co_id As String)

        Try
            Dim MyGuid As New Guid(co_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_ListClosed", _
                                                         New SqlParameter("@Closed", Active), _
                                                         New SqlParameter("@co_id", MyGuid))

            With dgvListAccountRegister
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False

                .Columns(2).HeaderText = "Account Number"
                .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(2).Width = 100
                .Columns(2).ReadOnly = True
                .Columns(3).HeaderText = "ID"
                .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(3).Width = 150
                .Columns(3).ReadOnly = True
                .Columns(4).HeaderText = "Name"
                .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(4).ReadOnly = True
                .Columns(5).HeaderText = "Account Title"
                .Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(5).ReadOnly = True
                .Columns(6).HeaderText = "Schedule"
                .Columns(6).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(6).ReadOnly = True
                .Columns(6).Width = 80
                .Columns(7).HeaderText = "Closed"
                .Columns(7).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(7).Width = 80
                .Columns(8).HeaderText = "Deducted"
                .Columns(8).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(8).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(8).Width = 80
            End With
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Load Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

    Private Sub txtsearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                SearchList(txtsearch.Text, gCompanyID)
        End Select
        'If e.KeyCode = Keys.Enter Then
        '    SearchList(txtsearch.Text)
        'End If
    End Sub

    Private Sub txtsearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsearch.TextChanged
        SearchList(txtsearch.Text, gCompanyID)
    End Sub

    Private Sub dgvListAccountRegister_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListAccountRegister.CellContentClick

    End Sub
End Class