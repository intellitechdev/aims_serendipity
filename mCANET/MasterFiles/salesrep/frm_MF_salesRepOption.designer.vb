<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MF_salesRepOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.rbtnCustomer = New System.Windows.Forms.RadioButton
        Me.rbtnSupplier = New System.Windows.Forms.RadioButton
        Me.rbtnOther = New System.Windows.Forms.RadioButton
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select which type of name to add then click OK"
        '
        'rbtnCustomer
        '
        Me.rbtnCustomer.AutoSize = True
        Me.rbtnCustomer.Location = New System.Drawing.Point(99, 68)
        Me.rbtnCustomer.Name = "rbtnCustomer"
        Me.rbtnCustomer.Size = New System.Drawing.Size(69, 17)
        Me.rbtnCustomer.TabIndex = 1
        Me.rbtnCustomer.TabStop = True
        Me.rbtnCustomer.Text = "Customer"
        Me.rbtnCustomer.UseVisualStyleBackColor = True
        '
        'rbtnSupplier
        '
        Me.rbtnSupplier.AutoSize = True
        Me.rbtnSupplier.Location = New System.Drawing.Point(99, 102)
        Me.rbtnSupplier.Name = "rbtnSupplier"
        Me.rbtnSupplier.Size = New System.Drawing.Size(63, 17)
        Me.rbtnSupplier.TabIndex = 2
        Me.rbtnSupplier.TabStop = True
        Me.rbtnSupplier.Text = "Supplier"
        Me.rbtnSupplier.UseVisualStyleBackColor = True
        '
        'rbtnOther
        '
        Me.rbtnOther.AutoSize = True
        Me.rbtnOther.Location = New System.Drawing.Point(99, 136)
        Me.rbtnOther.Name = "rbtnOther"
        Me.rbtnOther.Size = New System.Drawing.Size(51, 17)
        Me.rbtnOther.TabIndex = 3
        Me.rbtnOther.TabStop = True
        Me.rbtnOther.Text = "Other"
        Me.rbtnOther.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(48, 222)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 4
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(129, 222)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frm_MF_salesRepOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.rbtnOther)
        Me.Controls.Add(Me.rbtnSupplier)
        Me.Controls.Add(Me.rbtnCustomer)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_MF_salesRepOption"
        Me.Text = "Select Name Type"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rbtnCustomer As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSupplier As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnOther As System.Windows.Forms.RadioButton
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
