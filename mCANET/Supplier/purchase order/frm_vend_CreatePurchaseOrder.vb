Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_CreatePurchaseOrder
    Private gCon As New Clsappconfiguration

    Private bIsNew As Boolean = False
    Private sKeyPO As String
    Private sKeySupplier As String
    Private sKeyShipTo As String
    Private sKeyShipAdd As String
    Private sShipAddInitial As String
    Private sKeyShipAddInitial As String
    Private sSupplierAddInitial As String
    Private dAmountInitial As Long = 0
    Private sType As String
    Private NewAddress, OldAddress As String
    Private bClosed As Boolean
    Private bReceived As Boolean

    Private sKeyToDel() As String = {""}
    Private iDel As Integer = 0

    Private sCoAddress As String
    Private fcFormReady As Boolean = False
    ' Variable storage of Customer name result in PO
    Private sNameShipto As String = "" ' GELO

    Dim checkDate As New clsRestrictedDate

    Public Property IsNew() As Boolean
        Get
            Return bIsNew
        End Get
        Set(ByVal value As Boolean)
            bIsNew = value
        End Set
    End Property

    Public Property KeyPO() As String
        Get
            Return sKeyPO
        End Get
        Set(ByVal value As String)
            sKeyPO = value
            'refreshForm()
        End Set
    End Property

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If cboSupplierID.SelectedIndex <> 0 Then
            updatePO()
        Else
            MessageBox.Show("Please choose a supplier and ship address", "Add/Edit P.O.")
        End If
    End Sub
  

    Private Sub updatePO()
        Dim msgWarning As DialogResult
        Dim sWarning As String
        Dim dtAddress As DataTable

        Dim sSQLCmd As String = "usp_t_purchaseOrder_update "
        sSQLCmd &= "@fxKeyPO = '" & sKeyPO & "'"
        sSQLCmd &= ",@fcPOno = '" & txtPONo.Text & "'"
        sSQLCmd &= ",@fdDateTransact = '" & dtDate.Value & "'"
        sSQLCmd &= ",@fcSupplierMessage = '" & txtSupplierMessage.Text & "'"
        sSQLCmd &= ",@fcMemo = '" & txtMemo.Text & "'"
        sSQLCmd &= ",@fbToBePrinted = " & IIf(chkPrint.Checked = True, 1, 0)
        sSQLCmd &= ",@fbToBeEmailed = " & IIf(chkEmail.Checked = True, 1, 0)
        sSQLCmd &= ",@fdAmount = " & Decimal.Parse(lblTotal.Text)
        sSQLCmd &= ",@fbClosed = " & IIf(chkClose.Checked = True, 1, 0)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@fcSupplierAddress = '" & txtSupplierAddress.Text & "'"

        If sKeySupplier <> "" Then
            dtAddress = m_getAddress(sKeySupplier, "Supplier").Tables(0)
            Dim sAddressTemp As String

            If dtAddress.Rows.Count <> 0 Then
                sAddressTemp = dtAddress.Rows(0)("fcAddress").ToString
            Else
                sAddressTemp = ""
            End If

            If Trim(txtSupplierAddress.Text) <> Trim(sAddressTemp) Then
                sWarning = "Shipping address for Supplier: "
                sWarning &= cboSupplierName.SelectedItem
                sWarning &= " is different from the master file." & vbCrLf
                sWarning &= "Would you like to have this new information appear next time?"

                msgWarning = MessageBox.Show(sWarning, "New Information Changed", MessageBoxButtons.YesNoCancel)

                If msgWarning = Windows.Forms.DialogResult.Yes Then
                    updateAddress(True)
                ElseIf msgWarning = Windows.Forms.DialogResult.Cancel Then
                    Exit Sub
                End If
            End If
        End If

            sSQLCmd &= ",@fcShipToAddress = '" & txtShipAddress.Text & "'"

            If sKeySupplier <> "" Then
                sSQLCmd &= ",@fxKeySupplier = '" & sKeySupplier & "'"
            End If

            If sKeyShipTo <> "" Then
                sSQLCmd &= ",@fxKeyShipTo = '" & sKeyShipTo & "'"
            End If

            If sKeyShipAdd <> "" Then
                sSQLCmd &= ",@fxKeyShipAdd = '" & sKeyShipAdd & "'"
            If sKeyShipTo <> "" Then
                NewAddress = txtShipAddress.Text
                OldAddress = MtcboShipAddress.SelectedItem.Col3
                sWarning = "Shipping address for " & sType & ": "
                sWarning &= sNameShipto
                sWarning &= " is different from the master file." & vbCrLf
                sWarning &= "Would you like to have this new information appear next time?"
                Dim xResult As MsgBoxResult
                If NewAddress <> OldAddress Then
                    xResult = MsgBox(sWarning, MsgBoxStyle.Exclamation + MsgBoxStyle.YesNoCancel, "Information changed!")
                    Select Case xResult
                        Case MsgBoxResult.Yes
                            Dim sSqlCmdAddress As String = "update dbo.mAddress set fcAddress = '" & txtShipAddress.Text & "' where fxKeyAddress = '" & sKeyShipAdd & "'"
                            Try
                                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)
                            Catch ex As Exception
                                ShowErrorMessage("updatePO", Me.Name, ex.Message)
                            End Try
                        Case MsgBoxResult.Cancel
                            Exit Sub
                    End Select
                End If
            End If
        End If

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            If updatePOItem() Then
                MessageBox.Show("Record successfully Updated.", "Add/Edit P.O.")
                'frm_vend_masterVendor.loadGrdSupplier2("Purchase Orders")
                frm_item_availability.loadDetails("Purchase Orders")
                'Me.Close()
            Else
                sSQLCmd = "DELETE FROM tPurchaseOrder WHERE fxKeyPO = '" & sKeyPO & "'"
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Cannot update this record!" & vbCrLf & "Please check the P.O. Items.", "P.O. Item")
                Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit P.O.")
        End Try
    End Sub

    Private Sub updateAddress(Optional ByVal bSupplier As Boolean = False)
        Dim sSQLCmd As String
        If bSupplier Then
            sSQLCmd = "UPDATE mAddress "
            sSQLCmd &= "SET fcAddress = '" & txtSupplierAddress.Text & "' "
            sSQLCmd &= "WHERE fxKeyID = '" & sKeySupplier & "' "
            sSQLCmd &= "AND fbSupplier = 1"
        Else
            Select Case sType
                Case "Customer"
                    sSQLCmd = "UPDATE mCustomer00Master "
                    sSQLCmd &= "SET fxKeyShippingAddress = '" & sKeyShipAdd & "' "
                    sSQLCmd &= "WHERE fxKeyCustomer = '" & sKeyShipTo & "'"
                    SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

                    sSQLCmd = "UPDATE mAddress "
                    sSQLCmd &= "SET fcAddress = '" & txtShipAddress.Text & "' "
                    sSQLCmd &= "WHERE fxKeyAddress = '" & sKeyShipAdd & "'"
                Case Else
                    sSQLCmd = "UPDATE mAddress "
                    sSQLCmd &= "SET fcAddress = '" & txtShipAddress.Text & "' "
                    sSQLCmd &= "WHERE fxKeyAddress = '" & sKeyShipAdd & "'"
            End Select
        End If

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Address")
        End Try
    End Sub

    Private Function updatePOItem() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Dim sKeyPOItem As String
        Dim sItem As String = ""
        Dim sDescription As String = ""
        Dim dQuantity As Long = 0
        Dim dRate As Long = 0
        Dim dRcv As Long = 0
        Dim sCustomer As String = ""
        Dim dAmount As Long = 0
        Dim bClose As Boolean = False

        Try

            If sKeyToDel(0) <> "" Then
                Dim i As Integer
                For i = 0 To UBound(sKeyToDel)
                    If sKeyToDel(i) <> "" Then
                        sSQLCmd = "DELETE FROM tPurchaseOrder_item "
                        sSQLCmd &= "WHERE fxKeyPOItem = '" & sKeyToDel(i) & "'"
                        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                    End If
                Next
            End If

            If grdPO.AllowUserToAddRows = True Then
                grdPO.AllowUserToAddRows = False
            End If
            If Me.grdPO.Rows.Count >= 1 Then
                For Each xItem As DataGridViewRow In grdPO.Rows
                    iRow = xItem.Index
                    If grdPO.Item("cfxKeyPOItem", iRow).Value Is Nothing Then
                        sKeyPOItem = Guid.NewGuid.ToString
                    Else
                        sKeyPOItem = grdPO.Item("cfxKeyPOItem", iRow).Value.ToString
                    End If
                    'sKeyPOItem = IIf(IsDBNull(grdPO.Item("cfxKeyPOItem", iRow).Value) = True, Guid.NewGuid.ToString, grdPO.Item("cfxKeyPOItem", iRow).Value.ToString)
                    sItem = grdPO.Item("cItem", iRow).Value.ToString
                    sDescription = grdPO.Item("cDescription", iRow).Value.ToString
                    dQuantity = grdPO.Item("cQuantity", iRow).Value
                    dRate = grdPO.Item("cRate", iRow).Value
                    dAmount = Format(CDec(grdPO.Item("cAmount", iRow).Value), "####0.00")
                    dRcv = grdPO.Item("cRcvd", iRow).Value

                    sSQLCmd = "usp_t_purchaseOrderItem_update "
                    sSQLCmd &= "@fxKeyPOItem = '" & sKeyPOItem & "'"
                    sSQLCmd &= ",@fxKeyPO = '" & sKeyPO & "'"
                    sSQLCmd &= ",@fxKeyItem = '" & sItem & "'"
                    sSQLCmd &= ",@fcPODescription = '" & sDescription & "'"
                    sSQLCmd &= ",@fdQuantity = " & dQuantity
                    sSQLCmd &= ",@fdRate = " & dRate
                    sSQLCmd &= ",@fdAmount = " & dAmount
                    sSQLCmd &= ",@fdReceived = " & dRcv ' GELO
                    sSQLCmd &= ",@fbClosed = " & IIf(grdPO.Item("cClosed", iRow).Value = "True", 1, 0)
                    If sCustomer <> "" Then
                        sSQLCmd &= ",@fxKeyCustomer = '" & sCustomer & "'"
                    End If

                    SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                Next
                If grdPO.AllowUserToAddRows = False Then
                    grdPO.AllowUserToAddRows = True
                End If
                Return True
            End If
            
        Catch ex As Exception
            ShowErrorMessage("updatePOItem", Me.Name, ex.Message)
            Return False
        End Try
       
    End Function

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdPO.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdPO.Rows(irow).Cells(iCol).Value)
    End Function

    Private Sub frm_vend_CreatePurchaseOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sKeyPO = "" Or sKeyPO = Nothing Then
            sKeyPO = Guid.NewGuid.ToString
            bIsNew = True
            Me.Text = "Create Purchase Order"
        End If
        IsDateRestricted()
        refreshForm()

    End Sub

    Private Sub refreshForm()
        iDel = 0
        Dim dtComp As DataRow = gCompanyInfo().Tables(0).Rows(0)

        sCoAddress = dtComp("co_name").ToString + _
                         vbCrLf + dtComp("co_street").ToString + _
                        " " + dtComp("co_city").ToString + _
                        vbCrLf + dtComp("co_zip").ToString
        If bIsNew Then
            chkClose.Visible = False
        Else
            chkClose.Visible = True
        End If

        m_loadSupplier(cboSupplierName, cboSupplierID)
        M_ShipToCompanyNameToMtcbo(MtcboShipname, True)
        MtcboShipname.SelectedIndex = 0
        loadPO()

        m_selectedValuesForMtCboNames(MtcboShipname, sKeyShipTo)

        m_selectedCboValue(cboSupplierName, cboSupplierID, sKeySupplier)

        If sKeyShipTo <> "" And sKeyShipAddInitial <> "" Then
            m_loadShippingAddressToMtcbo(MtcboShipAddress, sKeyShipTo)
            MtcboShipAddress.SelectedValue = GetValueForMTcboShippingAddress(sKeyShipAddInitial, sKeyShipTo)
        End If

        'If sType = "Customer" Then
        '    MtcboShipAddress.Enabled = True
        'Else
        '    MtcboShipAddress.Enabled = False
        'End If

   
        txtSupplierAddress.Text = sSupplierAddInitial
        txtShipAddress.Text = sShipAddInitial
        lblTotal.Text = Format(dAmountInitial, "0.00")

        If bClosed Then
            TransLbl.Text = "Closed"
            ShowStatus()
        Else
            If bReceived Then
                TransLbl.Text = "Fully Received"
                ShowStatus()
            Else
                TransLbl.Visible = False
            End If
        End If
        createColumn()
        loadPOItems(sKeyPO)
        fcFormReady = True
    End Sub
    Private Sub ShowStatus()
        cboSupplierName.Enabled = False
        txtSupplierAddress.Enabled = False
        MtcboShipname.Enabled = False
        dtDate.Enabled = False
        txtPONo.Enabled = False
        MtcboShipAddress.Enabled = False
        txtShipAddress.Enabled = False
        grdPO.Enabled = False
        txtSupplierMessage.Enabled = False
        txtMemo.Enabled = False
        chkClose.Enabled = False
        chkEmail.Enabled = False
        chkPrint.Enabled = False
        btnTrigger.Enabled = False
        btnSave.Enabled = False
        TransLbl.Visible = True
        TransLbl.Select()
    End Sub
    Private Sub createColumn()

        Dim cboColKeyPOItem As New DataGridViewTextBoxColumn
        Dim cboColItem As New DataGridViewComboBoxColumn

        Dim cboColDescription As New DataGridViewTextBoxColumn
        Dim cboColQty As New DataGridViewTextBoxColumn
        Dim cboColUnit As New DataGridViewTextBoxColumn
        Dim cboColRate As New DataGridViewTextBoxColumn
        Dim cboColCustomer As New DataGridViewComboBoxColumn
        Dim cboColAmount As New DataGridViewTextBoxColumn
        Dim cboColBkOrder As New DataGridViewTextBoxColumn
        Dim cboColRcv As New DataGridViewTextBoxColumn
        Dim cboColClose As New DataGridViewCheckBoxColumn

        Dim dtItem As DataTable = m_GetItem().Tables(0)
        Dim dtCustomer As DataTable = m_GetCustomer().Tables(0)

        Try
            With cboColKeyPOItem
                .DataPropertyName = "fxKeyPOItem"
                .Name = "cfxKeyPOItem"
                .Visible = False
            End With

            With cboColItem                                     ' GELO
                .HeaderText = "Item"
                .Name = "cItem"
                .DataPropertyName = "fxKeyItem"
                .DataSource = dtItem.DefaultView
                .DisplayMember = "fcDescription"
                .ValueMember = "fxKeyItem"
                .DropDownWidth = 200
            End With

            With cboColDescription
                .HeaderText = "Description"
                .Name = "cDescription"
                .DataPropertyName = "fcPODescription"
            End With

            With cboColQty
                .HeaderText = "Qty"
                .Name = "cQuantity"
                .DataPropertyName = "fdQuantity"
            End With

            With cboColUnit
                .HeaderText = "Unit"
                .Name = "cUnit"
                .DataPropertyName = "fcUnitAbbreviation"
                .ReadOnly = True
            End With

            With cboColRate
                .HeaderText = "Rate"
                .Name = "cRate"
                .DataPropertyName = "fdRate"
                .DefaultCellStyle.Format = "###,###,##0.00"
            End With

            With cboColCustomer
                .HeaderText = "Customer"
                .Name = "cCustomer"
                .DataPropertyName = "fxKeyCustomer"
                .DataSource = dtCustomer.DefaultView
                .DisplayMember = "fcCustomerName"
                .ValueMember = "fxKeyCustomer"
                .DropDownWidth = 200
            End With

            With cboColAmount
                .HeaderText = "Amount"
                .Name = "cAmount"
                .DataPropertyName = "fdAmount"
                .DefaultCellStyle.Format = "##,##0.00"
            End With

            With cboColBkOrder
                .HeaderText = "Backordered"
                .Name = "cBackOrdered"
                .DataPropertyName = "fdBackorder"
            End With

            With cboColRcv
                .HeaderText = "Rcvd"
                .Name = "cRcvd"
                .DataPropertyName = "fdReceived"
                .ReadOnly = True
            End With

            With cboColClose
                .HeaderText = "Approved"
                .Name = "cClosed"
                .DataPropertyName = "fbClosed"
            End With

            With grdPO
                .Columns.Clear()
                .Columns.Add(cboColKeyPOItem)
                .Columns.Add(cboColItem)
                .Columns.Add(cboColDescription)
                .Columns.Add(cboColQty)
                .Columns.Add(cboColUnit)
                .Columns.Add(cboColRate)
                .Columns.Add(cboColCustomer)
                .Columns.Add(cboColAmount)
                .Columns.Add(cboColBkOrder)
                .Columns.Add(cboColRcv)
                .Columns.Add(cboColClose)
            End With

        Catch
            MessageBox.Show(Err.Description, "datagrid")
        End Try
    End Sub
    ' GELO
    ' Loading Purchase Order Items
    Private Sub loadPOItems(ByVal sKey)
        Dim xRow As Integer = 0
        Dim sSQLCmd As String = "usp_t_loadPOItems "
        sSQLCmd &= "@fxKeyPO='" & sKeyPO & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    With grdPO
                        .Rows.Add()
                        .Item("cfxKeyPOItem", xRow).Value = rd.Item("fxKeyPOItem")
                        .Item("cItem", xRow).Value = rd.Item("fxKeyItem")
                        .Item("cDescription", xRow).Value = rd.Item("fcPODescription").ToString
                        .Item("cQuantity", xRow).Value = rd.Item("fdQuantity")
                        .Item("cUnit", xRow).Value = rd.Item("fcUnitAbbreviation").ToString
                        .Item("cRate", xRow).Value = rd.Item("fdRate")
                        .Item("cCustomer", xRow).Value = rd.Item("fxKeyCustomer")
                        .Item("cAmount", xRow).Value = Format(rd.Item("fdAmount"), "##,##0.00")
                        .Item("cBackOrdered", xRow).Value = rd.Item("fdBackorder")
                        .Item("cRcvd", xRow).Value = rd.Item("fdReceived")
                        .Item("cClosed", xRow).Value = rd.Item("fbClosed")
                    End With
                    xRow += 1
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load PO Items")
        End Try

    End Sub

    Private Sub grdPOSettings()
        With grdPO()
            .Columns.Item("cfxKeyPOItem").HeaderText = "PO Item Key"
            .Columns.Item("cItem").HeaderText = "Item"
            .Columns.Item("cDescription").HeaderText = "Description"
            .Columns.Item("cQuantity").HeaderText = "Qty"
            .Columns.Item("cUnit").HeaderText = "Unit"
            .Columns.Item("cRate").HeaderText = "Rate"
            .Columns.Item("cCustomer").HeaderText = "Customer"
            .Columns.Item("cAmount").HeaderText = "Amount"
            .Columns.Item("cBackOrdered").HeaderText = "Backordered"
            .Columns.Item("cRcvd").HeaderText = "Rcv"
            .Columns.Item("cClosed").HeaderText = "Approved"


            .Columns("cfxKeyPOItem").Visible = False
            .Columns("cItem").Width = 100
            .Columns("cItemDescription").Width = 100
            .Columns("cQuantity").Width = 150
            .Columns("cUnit").Width = 100
            .Columns("cRate").Width = 100
            .Columns("cCustomer").Width = 100
            .Columns("cAmount").Width = 100
            .Columns("cBackordered").Width = 100
            .Columns("cRcvd").Width = 100
            .Columns("cClosed").Width = 100

        End With
    End Sub


    Private Sub loadPO()
        Dim dtPO As DataSet = m_GetPO(sKeyPO)


        Try
            Using rd As DataTableReader = dtPO.CreateDataReader
                If rd.Read Then
                    dtDate.Value = rd.Item("fdDateTransact")
                    txtPONo.Text = rd.Item("fcPOno")
                    sSupplierAddInitial = rd.Item("fcSupplierAddress")
                    'txtShipAddress.Text = rd.Item("fcShiptoAddress")
                    sShipAddInitial = rd.Item("fcShiptoAddress")
                    txtSupplierMessage.Text = rd.Item("fcSupplierMessage")
                    txtMemo.Text = rd.Item("fcMemo")
                    chkPrint.Checked = rd.Item("fbToBePrinted") 'IIf(rd.Item("fbToBePrinted") = 1, True, False)
                    chkEmail.Checked = rd.Item("fbToBeEmailed") 'IIf(rd.Item("fbToBeEmailed") = 1, True, False)
                    chkClose.Checked = rd.Item("fbClosed") 'IIf(rd.Item("fbClosed") = 1, True, False)
                    sKeySupplier = rd.Item("fxKeySupplier").ToString
                    sKeyShipTo = rd.Item("fxKeyShipTo").ToString
                    sKeyShipAddInitial = rd.Item("fxKeyShipAdd").ToString
                    dAmountInitial = rd.Item("fdAmount")
                    bClosed = rd.Item("fbClosed")
                    bReceived = rd.Item("fbReceived")
                    If sKeyShipTo <> "" Then
                        MtcboShipname.SelectedValue = GetMtcboShipToValue(sKeyShipTo, gCompanyID())
                    End If

                Else
                    dtDate.Value = Now

                    sSupplierAddInitial = ""
                    txtShipAddress.Text = ""
                    txtSupplierMessage.Text = ""
                    txtMemo.Text = ""
                    chkPrint.Checked = True
                    chkEmail.Checked = False
                    chkClose.Checked = False
                    sKeySupplier = ""
                    sKeyShipTo = ""
                    sKeyShipAddInitial = ""
                    dAmountInitial = 0

                    txtPONo.Text = defaultPONo()
                    txtShipAddress.Text = sCoAddress

                    bClosed = False
                    bReceived = False

                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load P.O.")
        End Try
    End Sub

    Private Function defaultPONo() As String
        Dim sDefault As String = "1"
        Dim sPONoFetch As String = ""
        Dim sPONo() As String = {""}
        Dim sPONosTemp As String = ""
        Dim i As Integer = 0

        Dim sSCLCmd As String = "SELECT fcPOno FROM tPurchaseOrder ORDER BY fcPOno"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    If IsNumeric(rd.Item("fcPOno")) Then
                        sPONoFetch = rd.Item("fcPOno")
                        sPONosTemp &= sPONoFetch & " "
                    End If
                End While
            End Using

            sPONo = sPONosTemp.Split
            If UBound(sPONo) > 0 Then
                For i = 0 To UBound(sPONo)
                    If CStr(i + 1) <> sPONo(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sPONo(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next

            End If
            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "P.O. No.")
            Return sDefault
        End Try
    End Function


    Private Sub cboSupplierName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSupplierName.SelectedIndexChanged
        cboSupplierID.SelectedIndex = cboSupplierName.SelectedIndex
        sKeySupplier = cboSupplierID.SelectedItem

        If cboSupplierName.SelectedIndex = 1 Then
            Dim x As New frm_vend_masterVendorAddEdit
            x.Text = "Add Supplier"
            x.ShowDialog()
            m_loadSupplier(cboSupplierName, cboSupplierID)
            Exit Sub
        End If

        If cboSupplierName.SelectedIndex <> 1 And cboSupplierName.SelectedIndex <> 0 Then
            Dim dtAddress As DataTable = m_getAddress(sKeySupplier, "Supplier").Tables(0)
            If dtAddress.Rows.Count > 0 Then
                txtSupplierAddress.Text = dtAddress.Rows(0)("fcAddress").ToString
            End If
        End If
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub grdPO_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdPO.CellValueChanged
        If fcFormReady = True Then
            If e.RowIndex >= 0 Then
                Dim sKeyItemTemp As String = grdPO.CurrentRow.Cells("cItem").Value.ToString
                Dim dtItemTemp As DataTable = m_GetItem(sKeyItemTemp).Tables(0)

                Dim dQuantity As Long = 0
                Dim dAmount As Decimal = 0
                Dim dRate As Decimal = 0
                Dim xRow As Integer = grdPO.CurrentRow.Index

                dQuantity = IIf(grdPO.Item("cQuantity", xRow).Value Is Nothing, "0", grdPO.Item("cQuantity", xRow).Value)

                dRate = IIf(grdPO.Item("cRate", xRow).Value Is Nothing, "0", grdPO.Item("cRate", xRow).Value)


                If grdPO.CurrentCell.OwningColumn.Name = "cRate" Then
                    If sKeyItemTemp <> "" Then
                        Try
                            If Math.Round(dRate, 2) <> Math.Round(dtItemTemp.Rows(0)("fdPrice"), 2) Then
                                Dim sWarn As String
                                Dim msgWarn As DialogResult
                                sWarn = "You have changed the cost for: "
                                sWarn &= grdPO.Item("cItem", xRow).Value.ToString & vbCrLf & "Do you want to update the item with the new cost?"
                                msgWarn = MessageBox.Show(sWarn, "Item's Cost Changed", MessageBoxButtons.YesNo)
                                If msgWarn = Windows.Forms.DialogResult.Yes Then
                                    updateItem(sKeyItemTemp, dRate)
                                End If
                            End If
                            dAmount = dRate * dQuantity
                            grdPO.CurrentRow.Cells("cAmount").Value = dAmount
                        Catch ex As Exception

                        End Try
                    End If
                End If

                If grdPO.CurrentCell.OwningColumn.Name = "cItem" Then
                    If sKeyItemTemp <> "" Then
                        Try
                            grdPO.CurrentRow.Cells("cDescription").Value = dtItemTemp.Rows(0)("fcItemDescription")
                            grdPO.CurrentRow.Cells("cUnit").Value = dtItemTemp.Rows(0)("fcUnitName")
                            grdPO.CurrentRow.Cells("cRate").Value = Format(CDec(dtItemTemp.Rows(0)("fdPrice")), "##,##0.00")
                        Catch
                            MessageBox.Show(Err.Description, "P.O. Item")
                        End Try
                    End If
                End If

                If grdPO.CurrentCell.OwningColumn.Name = "cQuantity" Then
                    Try
                        dAmount = dQuantity * dRate
                        grdPO.CurrentRow.Cells("cAmount").Value = Format(dAmount, "##,##0.00")
                    Catch
                        MessageBox.Show(Err.Description, "Data Validation")
                    End Try

                End If
                getTotal()
            End If
        End If
    End Sub

    Private Sub getTotal()
        Try
            Dim iRow As Integer
            Dim dTotal As Long = 0
            For iRow = 0 To grdPO.Rows.Count - 2
                dTotal = dTotal + IIf(grdPO.Item("cAmount", iRow).Value Is Nothing, "0.00", Format(CDec(grdPO.Item("cAmount", iRow).Value), "####0.00"))
            Next
            lblTotal.Text = Format(dTotal, "##,##0.00")
        Catch
            MessageBox.Show(Err.Description, "Total")
        End Try
    End Sub

    Private Sub updateItem(ByVal sKey As String, ByVal nRate As Long)
        Dim sSQLCmd As String = "UPDATE mItem00Master "
        sSQLCmd &= "SET fdCost = " & nRate
        sSQLCmd &= " WHERE fxKeyItem = '" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Item Cost")
        End Try
    End Sub

    Private Sub btnTrigger_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTrigger.Click
        createColumn()
        iDel = 0
        sKeyToDel(iDel) = ""
    End Sub

    Private Sub grdPO_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdPO.UserDeletingRow
        If grdPO.Rows.Count > 0 Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                ReDim Preserve sKeyToDel(iDel)
                sKeyToDel(iDel) = e.Row.Cells("cfxKeyPOItem").Value.ToString
                iDel += 1
                getTotal()
                lblTotal.Text = lblTotal.Text - e.Row.Cells("cAmount").Value
                lblTotal.Text = Format(CLng(lblTotal.Text), "0.00")
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dtDate.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
        End If
    End Sub

    Private Sub dtDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtDate.ValueChanged
        IsDateRestricted()
    End Sub

 
    Private Sub MtcboShipname_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MtcboShipname.SelectedIndexChanged
        sKeyShipTo = MtcboShipname.SelectedItem.Col2
        sNameShipto = MtcboShipname.SelectedItem.Col1
        m_loadShippingAddressToMtcbo(MtcboShipAddress, sKeyShipTo)

    End Sub

    Private Sub MtcboShipAddress_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MtcboShipAddress.SelectedIndexChanged
        If MtcboShipAddress.SelectedItem.Col1 = "Create New" Then
            Dim xForm As New frm_cust_masterCustomerAddress
            xForm.SKeyName = sKeyShipTo
            xForm.fcAddressType = "shipping"
            xForm.ShowDialog()
            m_loadShippingAddressToMtcbo(MtcboShipAddress, sKeyShipTo)
            txtShipAddress.Text = ""
        Else
            txtShipAddress.Text = MtcboShipAddress.SelectedItem.Col3
            sKeyShipAdd = MtcboShipAddress.SelectedItem.Col2
        End If
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim sSqlCmd As String = "select * from dbo.tPurchaseOrder where fxKeyPO = '" & sKeyPO & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    Dim xForm As New FrmPODetailReport
                    xForm.xfxKeyPO = sKeyPO
                    xForm.ShowDialog()
                Else
                    MsgBox("Please save first before printing!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
                End If
            End Using
        Catch ex As Exception
            ShowErrorMessage("btnPreview_Click", Me.Name, ex.Message)
        End Try
    End Sub

    Private Sub frm_vend_CreatePurchaseOrder_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frm_vend_CreatePurchaseOrder_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub


End Class