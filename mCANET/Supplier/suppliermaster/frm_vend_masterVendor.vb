Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_masterVendor
#Region "Declaration"
    Private gcon As New Clsappconfiguration
    Private gTrans As New clsTransactionFunctions
    Private sKeySupplier As String
    Private sKeyAddress As String
    Private sKeyRate As String
    Private sMode As String = ""
    Private sKeyPO As String
    Private sShow As String
    Private dteDateFrom As String
    Private dteDateTo As String
    Private dteFrom As String
    Private dteTo As String

    Private xsType As String
#End Region

    Private Sub frm_vend_masterVendor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        With grdTransaction
            .Rows.Clear()
            '.Rows.Add("Purchase Orders")
            .Rows.Add("Bills")
            .Rows.Add("Checks")
        End With

        secPanelSettings()
        cboViewSettings()
        cboShowSettings()

        TabSuppliernTransaction.TabPages.Remove(TabSuppliernTransaction.TabPages(2))

        Call loadcustType()
        Dim filter As String
        If cbofilter.SelectedValue IsNot DBNull.Value Then
            filter = cbofilter.SelectedValue.ToString
        Else
            filter = ""
        End If

        m_supplierList(grdSupplier1, cboView.SelectedItem, filter)

        cboFilterBy_Load("Purchase Orders", cboFilterPO)
        m_loaddteRange(dteRange)
        m_loaddteRange(dteDateRange)

    End Sub
    Private Sub cboShowSettings()
        With cboShow.Items

            .Add("All Transactions")
            .Add("Bills")
            '.Add("Bill Payments")
            '.Add("Checks")
            .Add("Journal Entries")
        End With
        If cboShow.SelectedIndex < 0 Then
            cboShow.SelectedIndex = 0
        End If
    End Sub
    Private Sub cboViewSettings()
        With cboView.Items
            .Add("All Members")
            .Add("Active Members")
            .Add("With Open Balances")
        End With

        If cboView.SelectedIndex < 0 Then
            cboView.SelectedIndex = 1
        End If
    End Sub
    Private Sub cboView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboView.SelectedIndexChanged


        m_supplierList(grdSupplier1, cboView.SelectedItem, cbofilter.SelectedValue)
    End Sub
    Private Sub TabControl_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabSuppliernTransaction.SelectedIndexChanged
        secPanelSettings()
        If TabSuppliernTransaction.SelectedIndex = 1 Then
            grdTransaction.SelectedCells.Item(0).Selected = True
            LoadGridSupplier2(sMode)
        End If
    End Sub
    Private Sub secPanelSettings()
        If TabSuppliernTransaction.SelectedIndex = 0 Then
            'btnCancelBill_allSupplier.Visible = False
            PanelVendorChild.Visible = True
            sizeSettings()
        Else
            PanelVendorChild.Visible = False
        End If
    End Sub
    Private Sub frm_vend_masterVendor_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        sizeSettings()
    End Sub

    Private Sub SplitContainer1_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SplitContainer1.SizeChanged
        sizeSettings()
    End Sub
    Private Sub SplitContainer2_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SplitContainer1.SizeChanged
        sizeSettings()
    End Sub
    Private Sub sizeSettings()
        TabPage1.Size = New Size(TabSuppliernTransaction.Size)
        TabPage2.Size = New Size(TabSuppliernTransaction.Size)
        TabPage3.Size = New Size(TabSuppliernTransaction.Size)

        TabSuppliernTransaction.Size = New Size(SplitContainer1.Panel1.Size)
        'grdSupplier1.Size = New Size(TabSuppliernTransaction.Size - 10)
        grdTransaction.Size = New Size(TabSuppliernTransaction.Size)

        PanelVendorChild.Size = New Size(SplitContainer1.Panel2.Size)
        PanelVendorChild.Location = New Point(-1, -1)

        grdSupplier2.Size = New Size(SplitContainer1.Panel2.Width - 10, SplitContainer1.Panel2.Height - 40)
        grdSupplier3.Size = New Size(spltSupplierInfo.Width - 10, spltSupplierInfo.Height - 400)

        grdRate.Size = New Size(TabSuppliernTransaction.Width, 318)
    End Sub
    Private Sub btnEditSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSupplier.Click
        Dim xForm As New frm_vend_masterVendorAddEdit
        xForm.Text = "Edit " & cbofilter.Text
        xForm.KeySupplier = sKeySupplier
        xForm.KeyAddress = sKeyAddress
        xForm.ShowDialog()
        loadcustType()

        Dim filter As String

        If cbofilter.SelectedValue IsNot DBNull.Value Then
            filter = cbofilter.SelectedValue.ToString()
        Else
            filter = ""
        End If

        Call m_supplierList(grdSupplier1, cboView.SelectedItem, filter)
    End Sub
    Private Sub btnEditNotes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditNotes.Click
        sKeySupplier = grdSupplier1.CurrentRow.Cells(0).Value.ToString
        frm_vend_masterVendorNote.KeySupplier = sKeySupplier
        frm_vend_masterVendorNote.ShowDialog()
    End Sub
    Private Sub ts_NewSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_NewSupplier.Click
        Dim x As New frm_vend_masterVendorAddEdit
        x.ShowDialog()

        Dim filter As String

        If cbofilter.SelectedValue IsNot DBNull.Value Then
            filter = cbofilter.SelectedValue.ToString()
        Else
            filter = ""
        End If

        Call m_supplierList(grdSupplier1, cboView.SelectedItem, filter)

    End Sub
    Private Sub ts_EnterBills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_EnterBills.Click
        Dim xForm As New frm_vend_EnterBills
        xForm.TabControl1.SelectedIndex = 0
        xForm.Text = "Enter Bills"
        xForm.chkBillRcv.Checked = True
        xForm.Show()
    End Sub
    Private Sub ts_PayBills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_PayBills.Click
        frm_vend_PayBills.MdiParent = Me.MdiParent
        frm_vend_PayBills.Show()
    End Sub
    Private Sub ts_PurchaseOrders_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_PurchaseOrders.Click
        frm_vend_CreatePurchaseOrder.KeyPO = Guid.NewGuid.ToString
        frm_vend_CreatePurchaseOrder.IsNew = True
        frm_vend_CreatePurchaseOrder.Text = "Create Purchase Order"
        frm_vend_CreatePurchaseOrder.MdiParent = Me.MdiParent
        frm_vend_CreatePurchaseOrder.Show()
    End Sub
    Private Sub ts_RcvItemAndBill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_RcvItemAndBill.Click
        Dim xForm As New frm_vend_EnterBills
        xForm.MdiParent = Me.MdiParent
        xForm.TabControl1.SelectedIndex = 1
        xForm.Text = "Enter Bills"
        xForm.chkBillRcv.Checked = True
        xForm.Show()
    End Sub
    Private Sub ts_RcvItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_RcvItem.Click
        Dim xForm As New frm_vend_EnterBills
        xForm.MdiParent = Me.MdiParent
        xForm.TabControl1.SelectedIndex = 1
        xForm.Text = "Create Item Receipts"
        xForm.chkBillRcv.Checked = False
        xForm.Show()
    End Sub
    Private Sub ts_EnterBillforRcvItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_EnterBillforRcvItem.Click
        frm_vend_EnterBillsForItems.Show()
    End Sub
    Private Sub SplitContainer1_SplitterMoved(ByVal sender As Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles SplitContainer1.SplitterMoved
        sizeSettings()
    End Sub
    Private Sub spltSupplierInfo_SplitterMoved(ByVal sender As Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles spltSupplierInfo.SplitterMoved
        sizeSettings()
    End Sub
    Private Sub showsupplier()
        If Me.grdSupplier1.SelectedRows.Count > 0 AndAlso _
                                 Not Me.grdSupplier1.SelectedRows(0).Index = _
                                 Me.grdSupplier1.Rows.Count - 1 Then
            sKeySupplier = grdSupplier1.CurrentRow.Cells(0).Value.ToString 'insert a value to sKeySupplier
            gKeySupplier = sKeySupplier
            If Not grdSupplier1.CurrentRow.Cells(1).Value.ToString Is Nothing Then
                sKeyAddress = grdSupplier1.CurrentRow.Cells(1).Value.ToString
            End If
            loadSuppplierInfo()
        End If
    End Sub
    Private Sub showTransaction()
        Me.Cursor = Cursors.WaitCursor
        If sKeySupplier = "" Then
            Exit Sub
        Else
            sKeySupplier = grdSupplier1.CurrentRow.Cells(0).Value.ToString
            gKeySupplier = sKeySupplier
        End If
        gTransactionType = cboShow.SelectedItem 'define transaction type
        sShow = gTransactionType
        Select_Date_Range() 'define date ranges
        Dim dtTransaction As DataSet
        If sShow = "All Transactions" Then
            dtTransaction = loadTransaction()
        Else
            dtTransaction = loadTransaction(sShow)
        End If
        Try
            With grdSupplier3
                .DataSource = dtTransaction.Tables(0).DefaultView
                .ScrollBars = ScrollBars.Both
                .Columns(0).Visible = False
                .Columns(1).HeaderText = "Type"
                .Columns(2).HeaderText = "Reference Number"
                .Columns(3).HeaderText = "Date"
                .Columns(4).HeaderText = "Memo"
                .Columns(5).HeaderText = "Account"
                .Columns(6).HeaderText = "Amount"
                .Columns(7).HeaderText = "Paid/Posted"
                .Columns(7).Name = "Paid"
                .Columns(8).HeaderText = "Cancelled"
                .Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                .Columns(6).DefaultCellStyle.Format = "##,##0.00"
                .Columns(7).ReadOnly = True
                .Columns(8).ReadOnly = True
                If sShow = "Bills" Then
                    .Columns(8).Visible = True
                End If
            End With
            Arrange_GridSupplier3()
        Catch
            MessageBox.Show(Err.Description, "Show Transactions")
        End Try
        If cboShow.SelectedItem = "Bills" Then 'filter the bill if it is open bills or cancelled bills or all bill must show.
            filter_bills()
        End If
        btnEditNotes.Enabled = True
        btnEditSupplier.Enabled = True
        Me.Cursor = Cursors.Default
    End Sub
    Private Function loadTransaction(Optional ByVal sType As String = "") As DataSet
        xsType = sType
        Dim sSQLCmd As String = "usp_t_transaction_listBySupplier "
        Dim TDateFrom As String
        Dim TDateTo As String
        sSQLCmd &= "@fxKeySupplier = '" & sKeySupplier & "' "
        If sType <> "" Then
            sSQLCmd &= ",@fcType = '" & sType & "'"
        End If
        If dteDateFrom Is Nothing Then
            TDateTo = ""
            TDateFrom = ""
        Else
            TDateFrom = gDateFrom
            TDateTo = gDateTo
        End If
        sSQLCmd &= ",@fdDateFrom = '" & TDateFrom & "'"
        sSQLCmd &= ",@fdDateTo = '" & TDateTo & "'"
        Try
            Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Transactions")
            Return Nothing
        End Try
    End Function
    Public Sub loadSuppplierInfo()
        Dim psSupName As String = ""
        Dim psCoName As String = ""
        Dim psAddress As String = ""
        Dim psAcntNo As String = "0"
        Dim psContact As String = ""
        Dim psNote As String = ""
        Dim psPhone1 As String = ""
        Dim psPhone2 As String = ""
        Dim psFax As String = ""
        Dim psEmail As String = ""
        Dim psSupType As String = ""
        Dim psTerms As String = ""
        Dim psBillRate As String = ""

        Dim sSQLCmd As String = "SPU_Supplier_InformationList @fxKeySupplier ='" & sKeySupplier & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    psSupName = rd.Item("fcSupplierName")
                    psCoName = rd.Item("fcCompanyName")
                    psAddress = rd.Item("fcAddress").ToString
                    psAcntNo = rd.Item("fcAccountNo").ToString
                    psContact = rd.Item("fcContactPerson1").ToString
                    psNote = rd.Item("fcNote").ToString
                    psPhone1 = rd.Item("fcTelNo1").ToString
                    psPhone2 = rd.Item("fcTelNo2").ToString
                    psFax = rd.Item("fcFaxNo").ToString
                    psEmail = rd.Item("fcEmail").ToString
                    psSupType = rd.Item("fcTypeName").ToString
                    psTerms = rd.Item("fcTermsName").ToString
                    psBillRate = rd.Item("fcBillRateName").ToString
                End While
            End Using

            txtSuppplierName.Text = psSupName
            txtCompName.Text = psCoName
            txtAddress.Text = psAddress
            txtAccountNo.Text = psAcntNo
            txtContact.Text = psContact
            txtNotes.Text = psNote
            txtPhone.Text = psPhone1
            txtPhone2.Text = psPhone2
            txtFax.Text = psFax
            txtEmail.Text = psEmail
            txtSupplierType.Text = psSupType
            txtTerms.Text = psTerms
            txtBillingRateLvl.Text = psBillRate

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Supplier List")
        End Try
    End Sub
    Private Sub grdRate_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRate.CellClick
        If Me.grdRate.SelectedRows.Count > 0 AndAlso _
          Not Me.grdRate.SelectedRows(0).Index = _
          Me.grdRate.Rows.Count - 1 Then
            sKeyRate = grdRate.CurrentRow.Cells(0).Value.ToString
            rateInfoList()
        End If
    End Sub
    Private Sub grdRate_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdRate.DoubleClick
        If Me.grdRate.SelectedRows.Count > 0 AndAlso _
                  Not Me.grdRate.SelectedRows(0).Index = _
                  Me.grdRate.Rows.Count - 1 Then
            sKeyRate = grdRate.CurrentRow.Cells(0).Value.ToString

            frm_vend_masterVendorRateAddEdit.KeyRate = sKeyRate
            frm_vend_masterVendorRateAddEdit.Text = "Edit Supplier Rate Category"
            frm_vend_masterVendorRateAddEdit.ShowDialog()
        End If
    End Sub
    Private Sub btnRate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateAdd.Click
        frm_vend_masterVendorRateAddEdit.KeyRate = Guid.NewGuid.ToString
        frm_vend_masterVendorRateAddEdit.Text = "Add Supplier Rate Category"
        frm_vend_masterVendorRateAddEdit.ShowDialog()
    End Sub
    Private Sub btnRateEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateEdit.Click
        frm_vend_masterVendorRateAddEdit.KeyRate = sKeyRate
        frm_vend_masterVendorRateAddEdit.Text = "Edit Supplier Rate Category"
        frm_vend_masterVendorRateAddEdit.ShowDialog()
    End Sub
    Private Sub btnRateDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateDelete.Click
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete Supplier Rate Category", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            deleteRate()
            m_supplierRate(grdRate)
        End If
    End Sub
    Private Sub deleteRate()
        Dim sSQLCmd As String = "DELETE FROM mSupplier02RateMaster "
        sSQLCmd &= " WHERE fxKeyRate = '" & sKeyRate & "'"
        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully deleted.", "Delete Supplier Rate Category")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Supplier Rate Category")
        End Try
    End Sub
    Private Sub rateInfoList()

        Dim sSQLCmd As String = "SELECT fcRateName, fcRateDescription FROM mSupplier02RateMaster "
        sSQLCmd &= " WHERE fxKeyRate = '" & sKeyRate & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If (rd.Read) Then
                    lblRateName.Visible = True
                    lblRateDescription.Visible = True
                    lblRateName.Text = rd.Item("fcRateName")
                    lblRateDescription.Text = rd.Item("fcRateDescription")
                Else
                    lblRateName.Visible = False
                    lblRateDescription.Visible = False
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Rate Category Info.")
        End Try
    End Sub
    Public Sub LoadGridSupplier2(ByVal sModes As String)

        Dim dtTableTransaction As DataTable

        Dim col1 As New DataGridViewTextBoxColumn
        Dim col2 As New DataGridViewTextBoxColumn
        Dim col3 As New DataGridViewTextBoxColumn
        Dim col4 As New DataGridViewTextBoxColumn
        Dim col5 As New DataGridViewTextBoxColumn
        Dim col6 As New DataGridViewTextBoxColumn
        Dim col7 As New DataGridViewTextBoxColumn
        Dim col8 As New DataGridViewCheckBoxColumn
        Dim col9 As New DataGridViewCheckBoxColumn
        Dim GridSupplier2_bindingsource As New BindingSource

        'If sMode = "Bills" Then
        '    ' btnCancelBill_allSupplier.Visible = True
        'Else
        '    ' btnCancelBill_allSupplier.Visible = False
        'End If
        Select Case sMode
            Case "Purchase Orders"
                dtTableTransaction = m_GetPO("", "All Purchase Orders", dteFrom, dteTo).Tables(0)

                grdSupplier2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeyPO"
                col1.HeaderText = "KeyPO"

                col2.DataPropertyName = "fcSupplierName"
                col2.HeaderText = "Supplier"

                col3.DataPropertyName = "fcPOno"
                col3.HeaderText = "PO No."

                col4.DataPropertyName = "fdDateTransact"
                col4.HeaderText = "Date"

                col5.DataPropertyName = "fdDateDeliver"
                col5.HeaderText = "Delivery Date"

                col6.DataPropertyName = "fdAmount"
                col6.HeaderText = "Amount"
                col6.DefaultCellStyle.Format = "##,##0.00"

                With grdSupplier2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                    .Add(col7)
                    grdSupplier2.Columns(0).Visible = False
                    grdSupplier2.Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdSupplier2.Columns(4).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdSupplier2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End With

            Case "Item Receipts"
                MessageBox.Show("Item Receipts")
            Case "Bills"
                GridSupplier2_bindingsource.DataSource = m_GetBillsInformationList(dteFrom, dteTo)
                GridSupplier2_bindingsource.DataMember = m_GetBillsInformationList(dteFrom, dteTo).Tables(0).TableName

                grdSupplier2.DataSource = GridSupplier2_bindingsource

                DisplayBills_to_Grid(grdSupplier2)
                Arrange_Bills_on_GridSupplier2()


            Case "Bills Payment"
                col1.DataPropertyName = "fxKeyBillPayment"
                col1.HeaderText = "KeyBillPayment"

                col2.DataPropertyName = "fcSupplierName"
                col2.HeaderText = "Member"

                col3.DataPropertyName = "fcType"
                col3.HeaderText = "Type"

                col4.DataPropertyName = "fcNumber"
                col4.HeaderText = "Number"

                col5.DataPropertyName = "fdDate"
                col5.HeaderText = "Date"

                col6.DataPropertyName = "fdAmount"
                col6.HeaderText = "Amount"

                With grdSupplier2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                    grdSupplier2.Columns(0).Visible = False
                    grdSupplier2.Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdSupplier2.Columns(4).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdSupplier2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End With
            Case "Checks"
                'dtTableTransaction = m_GetCheck().Tables(0)

                With grdSupplier2
                    .Columns.Clear()
                    .DataSource = m_GetCheck().Tables(0)
                    .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

                    .Columns("fdCheckDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("fdCheckAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    .Columns("fcSupplierName").HeaderText = "Member"
                    .Columns("acnt_desc").HeaderText = "Bank Account"
                    '.Columns("fcAPVno").HeaderText = "Reference No."
                    .Columns("fcCheckNo").HeaderText = "Check No."
                    .Columns("fdCheckDate").HeaderText = "Check Date"
                    .Columns("fdCheckAmount").HeaderText = "Amount in Check"
                    ''.Columns("fnAmountDue").HeaderText = "Amount in Bill"

                    '.Columns("fnAmountDue").DefaultCellStyle.Format = "n2"
                    .Columns("fdCheckAmount").DefaultCellStyle.Format = "n2"

                    .Columns("pk_WriteCheck").Visible = False
                    .Columns("fxKeyPayBills").Visible = False
                    .Columns("co_legalname").Visible = False
                    .Columns("fcEmployeeNo").Visible = False
                    .Columns("fcAmountInWords").Visible = False
                    .Columns("fcMemo").Visible = False
                End With
            Case "Sales Tax Payments"
                MessageBox.Show("Sales Tax Payments")
            Case Else
                Exit Sub
        End Select


    End Sub
    Public Sub DisplayBills_to_Grid(ByVal datagrid As System.Windows.Forms.DataGridView)
        Dim fxKeyBill As New DataGridViewTextBoxColumn
        Dim supplierName As New DataGridViewTextBoxColumn
        Dim referenceNo As New DataGridViewTextBoxColumn
        Dim transactionDate As New DataGridViewTextBoxColumn
        Dim dueDate As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        Dim paid As New DataGridViewCheckBoxColumn
        Dim OriginalAmnt As New DataGridViewTextBoxColumn
        Dim Void As New DataGridViewCheckBoxColumn

        fxKeyBill.DataPropertyName = "fxKeyBill"
        fxKeyBill.HeaderText = "KeyBill"

        supplierName.DataPropertyName = "fcSupplierName"
        supplierName.HeaderText = "Member"

        referenceNo.DataPropertyName = "fcRefNo"
        referenceNo.HeaderText = "APV No."
        referenceNo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        referenceNo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

        transactionDate.DataPropertyName = "fdtransDate"
        transactionDate.HeaderText = "Date"
        transactionDate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        dueDate.DataPropertyName = "fdDueDate"
        dueDate.HeaderText = "Due Date"
        dueDate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        OriginalAmnt.DataPropertyName = "fnAmountDue"
        OriginalAmnt.HeaderText = "Original Amount"
        OriginalAmnt.Name = "OriginalAmnt"
        OriginalAmnt.ReadOnly = True

        amount.DataPropertyName = "fdBalance"
        amount.HeaderText = "Balance"
        amount.DefaultCellStyle.Format = "n2"
        amount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        paid.DataPropertyName = "fbPaid"
        paid.HeaderText = "Paid"
        paid.Name = "Paid"
        paid.ReadOnly = True

        Void.DataPropertyName = "fbvoid"
        Void.HeaderText = "Voided"
        Void.Name = "Void"
        Void.ReadOnly = True

        With datagrid.Columns
            .Clear()
            .Add(fxKeyBill)
            .Add(supplierName)
            .Add(referenceNo)
            .Add(transactionDate)
            .Add(dueDate)
            .Add(OriginalAmnt)
            .Add(amount)
            .Add(paid)
            .Add(Void)
        End With
    End Sub
    Private Sub DisplayBills_to_Grdsupplier3(ByVal datagrid As System.Windows.Forms.DataGridView)
        Dim fcKey As New DataGridViewTextBoxColumn
        Dim fcType As New DataGridViewTextBoxColumn
        Dim referenceNo As New DataGridViewTextBoxColumn
        Dim transactionDate As New DataGridViewTextBoxColumn
        Dim Memo As New DataGridViewTextBoxColumn
        Dim Account As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        Dim paid As New DataGridViewCheckBoxColumn
        Dim cancelled As New DataGridViewCheckBoxColumn

        fcKey.DataPropertyName = "fcKey"
        fcKey.HeaderText = "fcKey"
        fcKey.Visible = True

        fcType.DataPropertyName = "fcType"
        fcType.HeaderText = "Type"

        referenceNo.DataPropertyName = "fcRefNo"
        referenceNo.HeaderText = "APV Number"
        referenceNo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        transactionDate.DataPropertyName = "fdtransDate"
        transactionDate.HeaderText = "Date"
        transactionDate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        Memo.DataPropertyName = "fcMemo"
        Memo.HeaderText = "Memo"
        Memo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        Account.DataPropertyName = "fcAccount"
        Account.HeaderText = "Account"
        Account.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        amount.DataPropertyName = "fcAmount"
        amount.HeaderText = "Amount"
        amount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        paid.DataPropertyName = "fdPosted"
        paid.HeaderText = "Paid/Posted"
        paid.Name = "Paid"
        paid.ReadOnly = True

        cancelled.DataPropertyName = "fbCancelled"
        cancelled.HeaderText = "Cancelled"
        cancelled.ReadOnly = True

        With datagrid.Columns
            .Clear()
            .Add(fcKey)
            .Add(fcType)
            .Add(referenceNo)
            .Add(transactionDate)
            .Add(Memo)
            .Add(Account)
            .Add(amount)
            .Add(paid)
            .Add(cancelled)
        End With
    End Sub

    Private Sub grdTransaction_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTransaction.SelectionChanged
        If Not Me.grdTransaction.CurrentRow.Index = _
                       Me.grdTransaction.Rows.Count - 1 Then

            sMode = grdTransaction.CurrentRow.Cells(0).Value
            LoadGridSupplier2(sMode)
            cboFilterBy_Load(sMode, cboFilterPO)
        End If
    End Sub
    Private Sub grdTransaction_cellClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTransaction.Click
        If Not Me.grdTransaction.CurrentRow.Index = _
                Me.grdTransaction.Rows.Count - 1 Then

            sMode = grdTransaction.CurrentRow.Cells(0).Value

            LoadGridSupplier2(sMode)
            cboFilterBy_Load(sMode, cboFilterPO)
        End If
    End Sub
    Private Sub grdSupplier1_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdSupplier1.CellPainting
        'Dim sf As New StringFormat
        'sf.Alignment = StringAlignment.Center
        'If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdSupplier1.Rows.Count Then
        '    e.PaintBackground(e.ClipBounds, True)
        '    e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
        '    e.Handled = True

        'End If
    End Sub
    Private Sub grdSupplier1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSupplier1.DoubleClick
        'If Me.grdSupplier1.SelectedRows.Count > 0 AndAlso _
        '                   Not Me.grdSupplier1.SelectedRows(0).Index = _
        '                   Me.grdSupplier1.Rows.Count - 1 Then

        If grdSupplier1.Rows.Count > 0 Then
            sKeySupplier = grdSupplier1.CurrentRow.Cells(0).Value.ToString

            sKeyAddress = grdSupplier1.CurrentRow.Cells(1).Value.ToString
            frm_vend_masterVendorAddEdit.KeySupplier = sKeySupplier
            frm_vend_masterVendorAddEdit.KeyAddress = sKeyAddress
            frm_vend_masterVendorAddEdit.Text = "Edit Member"
            frm_vend_masterVendorAddEdit.ShowDialog()

            Dim filter As String

            If cbofilter.SelectedValue IsNot DBNull.Value Then
                filter = cbofilter.SelectedValue.ToString()
            Else
                filter = ""
            End If

            Call m_supplierList(grdSupplier1, cboView.SelectedItem, filter)
        End If

        'End If
    End Sub
    Private Sub grdSupplier1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSupplier1.Click
        Try
            If grdSupplier1.Rows.Count > 0 Then
                Call showsupplier()
                Call showTransaction()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub grdSupplier1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdSupplier1.KeyUp
        If Me.grdSupplier1.SelectedRows.Count > 0 AndAlso _
                                   Not Me.grdSupplier1.SelectedRows(0).Index = _
                                   Me.grdSupplier1.Rows.Count - 1 Then
            sKeySupplier = grdSupplier1.CurrentRow.Cells(0).Value.ToString
            sKeyAddress = grdSupplier1.CurrentRow.Cells(1).Value.ToString
            loadSuppplierInfo()
        End If
    End Sub
   
    Private Sub grdSupplier2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSupplier2.DoubleClick
        If Me.grdSupplier2.SelectedCells.Count > 0 Then
            Select Case sMode
                Case "Purchase Orders"
                    sKeyPO = grdSupplier2.CurrentRow.Cells(0).Value.ToString
                    Dim xForm As New frm_vend_CreatePurchaseOrder
                    xForm.KeyPO = sKeyPO
                    xForm.Text = "Edit Purchase Order"
                    xForm.ShowDialog()
                Case "Bills"
                    Dim xForm As New frm_vend_EnterBills
                    xForm.MdiParent = frmMain
                    xForm.Text = " Voucher Register (Bills Entry)"
                    xForm.Mode = 2
                    xForm.KeyBill = grdSupplier2.Item(0, grdSupplier2.CurrentRow.Index).Value.ToString
                    xForm.GetSupplierID() = grdSupplier2.Item(0, grdSupplier2.CurrentRow.Index).Value.ToString
                    xForm.Show()
                Case "Bills Payment"
                    frm_vend_PayBills.Text = "Edit Bills Payment"
                    frm_vend_PayBills.ShowDialog()
                Case "Checks"
                    With frm_acc_writeChecks_v2
                        .GetWriteChecksID() = grdSupplier2.CurrentRow.Cells("pk_WriteCheck").Value.ToString()
                        .GetPayBillsID() = grdSupplier2.CurrentRow.Cells("fxKeyPayBills").Value.ToString()
                        .ShowDialog()
                    End With
            End Select
        End If
    End Sub
    Private Sub grdSupplier3_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdSupplier3.CellPainting
        Dim sf As New StringFormat
        On Error Resume Next
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdSupplier3.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
        Me.Cursor = Cursors.WaitCursor
    End Sub
    Private Sub grdSupplier3_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSupplier3.DoubleClick

        Dim sType As String = grdSupplier3.Item(1, grdSupplier3.CurrentRow.Index).Value
        If Me.grdSupplier3.SelectedCells.Count > 0 Then
            Select Case sType
                Case "Purchase Orders"
                    sKeyPO = grdSupplier3.CurrentRow.Cells(9).Value.ToString
                    Dim xForm As New frm_vend_CreatePurchaseOrder
                    xForm.KeyPO = sKeyPO
                    xForm.Text = "Edit Purchase Order"
                    xForm.ShowDialog()
                Case "Bills"
                    Dim xForm As New frm_vend_EnterBills
                    xForm.Text = " Voucher Register (Bills Entry)"
                    xForm.Mode = 2
                    xForm.MdiParent = frmMain
                    xForm.KeyBill = grdSupplier3.Item(9, grdSupplier3.CurrentRow.Index).Value.ToString
                    xForm.Show()
                Case "Bills Payment"
                    frm_vend_PayBills.Text = "Edit Bills Payment"
                    frm_vend_PayBills.ShowDialog()
                Case "Checks"
                    frm_acc_writechecks.Text = "Edit Check"
                    frm_acc_writechecks.ShowDialog()
            End Select
        End If

    End Sub
    Private Sub cboFilterPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterPO.SelectedIndexChanged
        If Not grdTransaction.CurrentRow() Is Nothing Then
            If grdTransaction.CurrentCell.Value.ToString = "Bills" Then
                Dim Bills_status As String = cboFilterPO.SelectedItem
                Dim gridSupplier2_bindingSource As New BindingSource
                Dim transaction_Type As String = grdTransaction.CurrentRow.Cells(0).Value
                Dim SupplierTransactions As DataSet = m_GetBillsInformationList(dteDateFrom, dteDateTo)

                gridSupplier2_bindingSource.DataSource = SupplierTransactions
                gridSupplier2_bindingSource.DataMember = SupplierTransactions.Tables(0).TableName

                Select Case Bills_status
                    Case "Open Bills"
                        gridSupplier2_bindingSource.Filter = "fbCancelled = 0"
                    Case "Cancelled Bills"
                        gridSupplier2_bindingSource.Filter = "fbCancelled = 1"
                End Select

                grdSupplier2.DataSource = gridSupplier2_bindingSource
                DisplayBills_to_Grid(grdSupplier2)
                Arrange_Bills_on_GridSupplier2()

            End If
        End If

    End Sub
    Private Sub cboFilterBy_Load(ByVal sMode As String, ByVal combobox As System.Windows.Forms.ComboBox)
        With combobox
            Select Case sMode
                Case "All Transactions"
                    .Items.Clear()
                    .Items.Add("All")
                Case "Balance Details"
                    .Items.Clear()
                    .Items.Add("All")
                    .Items.Add("Open")
                Case "All Payments Issued"
                    .Items.Clear()
                    .Items.Add("All")
                Case "Bills"
                    .Items.Clear()
                    .Items.Add("All Bills")
                    .Items.Add("Open Bills")
                    .Items.Add("Cancelled Bills")
                Case "Purchase Orders"
                    .Items.Clear()
                    .Items.Add("All Purchase Orders")
                Case "Bill Payments"
                    .Items.Clear()
                    .Items.Add("All")
                Case "Checks"
                    '.Items.Clear()
                    '.Items.Add("All Bank Accounts")
                Case "Credit Card Activities"
                    .Items.Clear()
                    .Items.Add("All Credit Card Activities")
                Case "Journal Entries"
                    .Items.Clear()
                    .Items.Add("All")
                Case Else
            End Select

            .SelectedIndex = 0
        End With
    End Sub
    Private Sub cboShow_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboShow.SelectedIndexChanged
        If cboShow.SelectedItem = "Bills" Then
            cboFilterBills.Enabled = True
            Label21.Enabled = True
            cboFilterBills.Items.Clear()
            cboFilterBills.Items.Add("All Bills")
            cboFilterBills.Items.Add("Open Bills")
            cboFilterBills.Items.Add("Cancelled Bills")
            cboFilterBills.Text = "All Bills"
            'btnCancelBill_perSupplier.Visible = True
        Else
            cboFilterBills.Enabled = False
            cboFilterBills.Enabled = False
            'btnCancelBill_perSupplier.Enabled = False
        End If
        showTransaction()
    End Sub
    Private Sub cboFilterBills_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterBills.SelectedIndexChanged
        showTransaction()
    End Sub
    Private Function filter_bills()
        If sKeySupplier <> "" Then
            Dim Bill_status As String = cboFilterBills.SelectedItem
            Dim Bills_BindingSource As New BindingSource
            Bills_BindingSource.DataSource = loadTransaction("Bills")
            Bills_BindingSource.DataMember = loadTransaction("Bills").Tables(0).TableName

            Select Case Bill_status
                Case "Open Bills"
                    Bills_BindingSource.Filter = ("fbCancelled = 0")
                Case "Cancelled Bills"
                    Bills_BindingSource.Filter = ("fbCancelled = 1")

            End Select

            grdSupplier3.Columns.Clear()
            grdSupplier3.DataSource = Bills_BindingSource
            Arrange_GridSupplier3()

        End If

    End Function

    Private Sub dteRange_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteRange.SelectedIndexChanged
        Call DisplayCustomerTransactionsPerPeriod()
    End Sub

    Private Sub DisplayCustomerTransactionsPerPeriod()
        getDateRange(lblRange, dteRange)
        gtransactionsCoveredDate = dteRange.SelectedItem


        'If dteRange.SelectedIndex <> 0 Then
        '    If InStr(lblRange.Text, "-", CompareMethod.Binary) <> 0 Then
        '        'dteFrom = Convert.ToDateTime(Mid(lblRange.Text, 1, InStr(lblRange.Text, "-", CompareMethod.Binary) - 1))
        '        dteFrom = dtpFrom.Value.Date
        '        'dteTo = Convert.ToDateTime(Mid(lblRange.Text, InStr(lblRange.Text, "-", CompareMethod.Binary) + 1, lblRange.Text.Length - InStr(lblRange.Text, "-", CompareMethod.Binary)))
        '        dteTo = dtpEnd.Value.Date
        '    Else
        '        'dteFrom = Convert.ToDateTime(IIf(lblRange.Text = "", Nothing, lblRange.Text))
        '        dteFrom = dtpFrom.Value.Date

        '        'dteTo = Convert.ToDateTime(IIf(lblRange.Text = "", Nothing, lblRange.Text))
        '        dteTo = dtpEnd.Value.Date

        '        If IsDate(dteTo) Then
        '            Dim x As Date = DateAdd(DateInterval.Day, 1, Convert.ToDateTime(dteTo))
        '            dteTo = FormatDateTime(x.ToString, DateFormat.ShortDate)
        '        End If
        '    End If
        'Else
        '    dteFrom = Nothing
        '    dteTo = Nothing
        'End If


        dteFrom = dtpFrom.Value.Date

        'dteTo = Convert.ToDateTime(IIf(lblRange.Text = "", Nothing, lblRange.Text))
        dteTo = dtpEnd.Value.Date
        LoadGridSupplier2(sMode)
    End Sub

    Private Function Select_Date_Range()
        getDateRange(lbldteRange, dteDateRange)
        If dteDateRange.SelectedItem <> "All" Then

            If InStr(lbldteRange.Text, "-", CompareMethod.Binary) <> 0 Then 'IF TWO DATES NEEDS TO BE SEPARATED
                dteDateFrom = Convert.ToDateTime(Mid(lbldteRange.Text, 1, InStr(lbldteRange.Text, "-", CompareMethod.Binary) - 1))
                dteDateTo = Convert.ToDateTime(Mid(lbldteRange.Text, InStr(lbldteRange.Text, "-", CompareMethod.Binary) + 1, lbldteRange.Text.Length - InStr(lbldteRange.Text, "-", CompareMethod.Binary)))
            Else
                dteDateFrom = Convert.ToDateTime(IIf(lbldteRange.Text = "", Nothing, lbldteRange.Text))
                dteDateTo = Convert.ToDateTime(IIf(lbldteRange.Text = "", Nothing, lbldteRange.Text))

                If IsDate(dteDateTo) Then
                    Dim x As Date = DateAdd(DateInterval.Day, 1, Convert.ToDateTime(dteDateTo))
                    dteDateTo = x.ToString
                End If
            End If
        Else
            dteDateFrom = Nothing
            dteDateTo = Nothing
        End If
        gDateFrom = dteDateFrom
        gDateTo = dteDateTo
    End Function
    Public Sub Arrange_Bills_on_GridSupplier2()
        With grdSupplier2
            .Columns(0).Visible = False
            .Columns(1).HeaderText = "Member"
            .Columns(2).HeaderText = "Reference Number"
            .Columns(3).HeaderText = "Date"
            .Columns(4).HeaderText = "Due Date"
            .Columns(5).HeaderText = "Original Amount"
            .Columns(6).HeaderText = "Amount"
            .Columns(7).HeaderText = "Paid"
            .Columns(7).Name = "Paid"
            .Columns(8).HeaderText = "Void"
            '.Columns(7).HeaderText = "Cancelled"
            '.Columns(7).Name = "fbCancelled"

            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            .Columns(5).DefaultCellStyle.Format = "n2"
            .Columns(1).Width = 200
            .Columns(2).Width = 60
            .Columns(3).Width = 80
            .Columns(5).Width = 85
            .Columns(4).Width = 80
            .Columns(7).Width = 35
            .Columns(8).Width = 35
            '.Columns(7).Width = 70
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
        'btnCancelBill_allSupplier.Enabled = False
    End Sub
    Private Sub Arrange_GridSupplier3()
        With grdSupplier3
            .Columns(0).Visible = False
            .Columns(1).HeaderText = "Type"
            .Columns(2).HeaderText = "Reference Number"
            .Columns(3).HeaderText = "Date"
            .Columns(4).HeaderText = "Memo"
            .Columns(5).HeaderText = "Account"
            .Columns(6).HeaderText = "Amount"
            .Columns(7).HeaderText = "Paid/Posted"
            .Columns(7).Name = "Paid"
            .Columns(7).ReadOnly = True
            .Columns(8).HeaderText = "Cancelled"
            .Columns(8).Name = "fbCancelled"
            .Columns(8).Visible = True
            .Columns(8).ReadOnly = True

            If grdSupplier3.ColumnCount > 8 Then
                .Columns(10).Visible = False
                .Columns(9).Visible = False
            End If

            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(3).Width = 70
            .Columns(4).Width = 200
            .Columns(5).Width = 80
            .Columns(6).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(7).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(8).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With

    End Sub
#Region "Cancel Bill Entry"
    Private Sub btnCancelBill_perSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        frm_Cancel_Bills.LstBills.Items.Clear()
        Dim DataMedium As String
        Dim ExportingRow As Integer = 0
        For Each selectedRows As DataGridViewRow In grdSupplier3.SelectedRows
            ExportingRow = selectedRows.Index
            DataMedium = grdSupplier3.Item(2, ExportingRow).Value
            frm_Cancel_Bills.LstBills.Items.Add(DataMedium)
        Next
        frm_Cancel_Bills.ShowDialog()
        If Response = "Cancel Bills" Then
            Dim selectedBill As System.Windows.Forms.DataGridView = grdSupplier3
            Dim rowNumber As Integer = 0
            Dim xRow As Integer = 0
            Dim CellKeyBill As String
            Dim CellKeyCompany As String

            For Each selectedRows As DataGridViewRow In grdSupplier3.SelectedRows
                xRow = selectedRows.Index
                rowNumber = selectedRows.Index
                If grdSupplier3.Item("Paid", rowNumber).Value = True Then
                    MsgBox("The Selected Bill has already been paid. This cannot be cancelled.", MsgBoxStyle.Exclamation)
                    Exit Sub
                Else
                    If CheckIf_isCancelled(selectedBill, rowNumber) = True Then
                        MsgBox("There are Selected Bill that has already been cancelled.", MsgBoxStyle.Exclamation)
                        Exit Sub
                    Else
                        If grdSupplier3.SelectedRows.Count = 0 Then
                            Exit Sub
                        End If
                        Dim sKeyBill As String = ""
                        sKeyBill = grdSupplier3.Item(0, selectedRows.Index).Value.ToString
                        CellKeyBill = grdSupplier3.Item(9, xRow).Value.ToString
                        CellKeyCompany = grdSupplier3.Item(10, xRow).Value.ToString
                        Dim sSqlCmd As String = " usp_t_CancelBill "
                        Try
                            sSqlCmd &= " @fxKeybill ='" & CellKeyBill & "'"
                            sSqlCmd &= ", @fxkeyCompany ='" & CellKeyCompany & "'"
                            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
                        Catch ex As Exception
                            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
                        End Try
                    End If
                End If
            Next
            Response = ""
            MsgBox("All selected bill(s) successfully deleted.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access Granted")
            'btnCancelBill_perSupplier.Enabled = False
            showTransaction()
        End If
    End Sub
    Private Sub btnCancelBill_allSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim selectedBill As System.Windows.Forms.DataGridView = grdSupplier2
        Dim rowNumber As Integer = 0

        For Each selectedRows As DataGridViewRow In grdSupplier2.SelectedRows

            rowNumber = selectedRows.Index
            If CheckIfPaidBills(selectedBill, rowNumber) = True Then
                MsgBox("The Selected Bill has already been paid. This cannot be cancelled.", MsgBoxStyle.Exclamation)
                Exit Sub
            ElseIf CheckIf_isCancelled(selectedBill, rowNumber) = True Then
                MsgBox("There are Selected Bill that has already been cancelled.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If grdSupplier2.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            Dim sKeyBill As String = ""
            sKeyBill = grdSupplier2.Item(0, selectedRows.Index).Value.ToString

            Dim BillreferenceNUmber As String
            BillreferenceNUmber = SelectReferenceNofromBill(sKeyBill)
            If MessageBox.Show("Are you sure you want to cancel Bill No. " & BillreferenceNUmber & "?", "Cancel Bill", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.OK Then
                CancelBillEntry(sKeyBill)
            End If
        Next
        LoadGridSupplier2("Bills")
    End Sub
    Private Function SelectReferenceNofromBill(ByVal sKeyBill As String) As String
        Dim referenceNumber As String = "<n/a>"
        Dim commandString As String = "SELECT fcRefNo "
        commandString &= "FROM dbo.tBills "
        commandString &= "WHERE fxKeyBill = '" & sKeyBill & "'"

        Try
            Using sqlReader As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, commandString)
                sqlReader.Read()
                If sqlReader.Read() Then
                    referenceNumber = sqlReader.Item(0).ToString
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Select Reference Number")
        End Try

        Return referenceNumber
    End Function
    Private Function CheckIfPaidBills(ByVal datagrid As DataGridView, ByVal RowNumber As Integer) As Boolean
        Dim IsBillPaid As Boolean = True
        IsBillPaid = datagrid.Item("Paid", RowNumber).Value
        Return IsBillPaid
    End Function
    Private Function CheckIf_isCancelled(ByVal datagrid As DataGridView, ByVal rowNumber As Integer) As Boolean
        Dim IsBillCancelled As Boolean = True
        'IsBillCancelled = datagrid.Item("fbCancelled", rowNumber).Value
        Return IsBillCancelled
    End Function
    Private Sub CancelBillEntry(ByVal sKeyBill As String)

        Dim cancelBillCommand As String = "UPDATE tBills SET fbCancelled = 1 "
        cancelBillCommand &= "FROM dbo.tBills WHERE fxKeyBill = '" & sKeyBill & "'"
        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, cancelBillCommand)
            MessageBox.Show("Bill No. " & SelectReferenceNofromBill(sKeyBill) & " has been Successfully Cancelled!", "Cancel Bill", MessageBoxButtons.OK)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Cancel Bill")
        End Try
    End Sub
#End Region
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Me.Cursor = Cursors.WaitCursor
        Dim MedType As String
        Dim MedRefNo As String
        Dim MedDate As String
        Dim MedMemo As String
        Dim MedAccount As String
        Dim MedAmmount As String
        Dim ExportingRow As Integer = 0

        Dim sSqlCmdDel As String = "DELETE FROM dbo.tsupplierTemporaryData "
        SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmdDel)

        Dim sSqlCmd As String = " SPU_Record_temporarydata "
        grdSupplier3.SelectAll()
        For Each selectedRows As DataGridViewRow In grdSupplier3.SelectedRows
            ExportingRow = selectedRows.Index
            MedType = grdSupplier3.Item(1, ExportingRow).Value
            sSqlCmd &= " @fcType ='" & MedType & "'"
            MedRefNo = grdSupplier3.Item(2, ExportingRow).Value
            sSqlCmd &= ", @ReferenceNo ='" & MedRefNo & "'"
            MedDate = grdSupplier3.Item(3, ExportingRow).Value
            sSqlCmd &= ", @Date ='" & MedDate & "'"
            MedMemo = grdSupplier3.Item(4, ExportingRow).Value
            sSqlCmd &= ", @Memo ='" & MedMemo & "'"
            MedAccount = grdSupplier3.Item(5, ExportingRow).Value
            sSqlCmd &= ", @Account ='" & MedAccount & "'"
            MedAmmount = grdSupplier3.Item(6, ExportingRow).Value
            sSqlCmd &= ", @Amount ='" & MedAmmount & "'"
            Try
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
                sSqlCmd = " SPU_Record_temporarydata "
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Printing Session Aborted!")
            End Try
        Next
        grdSupplier3.MultiSelect = False
        grdSupplier3.MultiSelect = True
        frmsupplierMasterReport.Show()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub grdSupplier3_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles grdSupplier3.RowsAdded
        btnPreview.Enabled = True
    End Sub
    Private Sub grdSupplier3_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles grdSupplier3.Paint
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub grdSupplier3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSupplier3.Click
        If grdSupplier3.RowCount <> 0 Then
            Dim selectedBill As System.Windows.Forms.DataGridView = grdSupplier3
            Dim rowNumber As Integer = grdSupplier3.CurrentRow.Index
            Dim CheckRow As Integer = 0

            For Each selectedRows As DataGridViewRow In grdSupplier3.SelectedRows
                CheckRow = selectedRows.Index
                If cboShow.SelectedItem = "Bills" Then
                    If grdSupplier3.Item("Paid", rowNumber).Value = True Then
                        ' btnCancelBill_perSupplier.Enabled = False
                        Exit Sub
                        'Else
                        '    If CheckIf_isCancelled(selectedBill, rowNumber) = True Then
                        '        btnCancelBill_perSupplier.Enabled = False
                        '        Exit Sub
                        '    Else
                        '        btnCancelBill_perSupplier.Enabled = True
                        '    End If
                    End If
                End If
            Next
        End If
       
    End Sub
    Private Sub dteDateRange_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteDateRange.SelectedIndexChanged
        showTransaction()
    End Sub
    Private Sub grdSupplier2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSupplier2.Click

        Select Case grdTransaction.CurrentCell.Value.ToString()
            Case "Checks"

            Case "Bills"
                Dim selectedBill As System.Windows.Forms.DataGridView = grdSupplier2
                Dim rowNumber As Integer = 0

                For Each selectedRows As DataGridViewRow In grdSupplier2.SelectedRows
                    rowNumber = selectedRows.Index
                    If CheckIfPaidBills(selectedBill, rowNumber) = True Then
                        'btnCancelBill_allSupplier.Enabled = False
                        Exit Sub
                    ElseIf CheckIf_isCancelled(selectedBill, rowNumber) = True Then
                        'btnCancelBill_allSupplier.Enabled = False
                        Exit Sub
                        'Else
                        'btnCancelBill_allSupplier.Enabled = True
                    End If

                    If grdSupplier2.SelectedRows.Count = 0 Then
                        Exit Sub
                    End If
                Next
        End Select
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        Call DisplayCustomerTransactionsPerPeriod()
    End Sub
    Private Sub dtpEnd_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEnd.ValueChanged
        Call DisplayCustomerTransactionsPerPeriod()
    End Sub
    Private Sub loadcustType()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("Customer_Type", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "filter")
            With cbofilter
                .ValueMember = "fxKeySupplierType"
                .DisplayMember = "fcTypeName"
                .DataSource = ds.Tables("filter")
                '.SelectedIndex = -1
            End With
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View filter")
        End Try

    End Sub
    Private Sub cbofilter_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbofilter.SelectedValueChanged
        If cbofilter.Text <> "" Then
            Dim filter As String
            If cbofilter.SelectedValue IsNot DBNull.Value Then
                filter = cbofilter.SelectedValue.ToString
            Else
                filter = ""
            End If

            Call m_supplierList(grdSupplier1, cboView.SelectedItem, filter)
            Me.Label4.Text = cbofilter.Text & " Information"
            Me.btnEditSupplier.Text = "Edit " & cbofilter.Text
            Me.TabPage1.Text = cbofilter.Text
            Me.ts_NewSupplier.Text = "New " & cbofilter.Text
            'xfrm.Text = cbofilter.Text
        ElseIf cbofilter.SelectedValue Is DBNull.Value Then
            Dim filter As String

            If cbofilter.SelectedValue IsNot DBNull.Value Then
                filter = cbofilter.SelectedValue.ToString()
            Else
                filter = ""
            End If

            Call m_supplierList(grdSupplier1, cboView.SelectedItem, filter)
            Me.Label4.Text = "Member Information"
            Me.btnEditSupplier.Text = "Edit Member"
            Me.TabPage1.Text = "Member"
            Me.ts_NewSupplier.Text = "New Member"

        End If
    End Sub
    Private Sub grdSupplier2_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles grdTransaction.KeyPress, grdSupplier2.KeyPress
        HandleKeyPress(e)
    End Sub
    Private Sub HandleKeyPress(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If (e.KeyChar = Microsoft.VisualBasic.ChrW(6)) And sMode = "Bills" Then
            frmSearchBills.Show()
        End If
    End Sub
    Private Sub BIllsDelete_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles grdSupplier2.KeyPress
        billsDelete(e)
    End Sub
    Private Sub billsDelete(ByVal e As System.Windows.Forms.KeyPressEventArgs)

        If (e.KeyChar = Microsoft.VisualBasic.ChrW(4)) And sMode = "Bills" Then
            If gUserName = "admin" Then
                frmDeleteBills.pkID = grdSupplier2.CurrentRow.Cells(0).Value.ToString
                frmDeleteBills.ShowDialog()
                Dim GridSupplier2_bindingsource As New BindingSource
                GridSupplier2_bindingsource.DataSource = m_GetBillsInformationList(dteFrom, dteTo)
                GridSupplier2_bindingsource.DataMember = m_GetBillsInformationList(dteFrom, dteTo).Tables(0).TableName

                grdSupplier2.DataSource = GridSupplier2_bindingsource

                DisplayBills_to_Grid(grdSupplier2)
                Arrange_Bills_on_GridSupplier2()
            Else
                MessageBox.Show("You Need to be admin to proceed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "spu_MemberList_ByID",
                                          New SqlParameter("@fcEmployeeNo", txtID.Text))
            With grdSupplier1
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = True
                '.Columns(3).Visible = True
                .Columns("fcMainAddress").Visible = False

                .Columns(4).Visible = False
                .Columns.Item(2).HeaderText = "Active"
                .Columns.Item(3).HeaderText = "Member Name"
                .Columns.Item(4).HeaderText = "Balance Total"
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End With
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE")
        End Try
        
    End Sub
End Class