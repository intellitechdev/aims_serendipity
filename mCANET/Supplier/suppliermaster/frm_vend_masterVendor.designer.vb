<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_masterVendor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_masterVendor))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_NewSupplier = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ts_EnterBills = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_PayBills = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ts_PurchaseOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_RcvItemAndBill = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_RcvItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_EnterBillforRcvItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ts_supplierList = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_Info = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_transList = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ts_ExportSupplierList = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_ExportTransactions = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_ImportFromExcel = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ts_PrepareLetterto = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_PrepareLetter = New System.Windows.Forms.ToolStripMenuItem()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TabSuppliernTransaction = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.cbofilter = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.grdSupplier1 = New System.Windows.Forms.DataGridView()
        Me.cboView = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grdTransaction = New System.Windows.Forms.DataGridView()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblRateDescription = New System.Windows.Forms.Label()
        Me.lblRateName = New System.Windows.Forms.Label()
        Me.btnRateDelete = New System.Windows.Forms.Button()
        Me.btnRateEdit = New System.Windows.Forms.Button()
        Me.btnRateAdd = New System.Windows.Forms.Button()
        Me.grdRate = New System.Windows.Forms.DataGridView()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.lblRange = New System.Windows.Forms.Label()
        Me.PanelVendorChild = New System.Windows.Forms.Panel()
        Me.spltSupplierInfo = New System.Windows.Forms.SplitContainer()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.lnkOpeningBal = New System.Windows.Forms.LinkLabel()
        Me.lnkReport = New System.Windows.Forms.LinkLabel()
        Me.btnEditNotes = New System.Windows.Forms.Button()
        Me.txtBillingRateLvl = New System.Windows.Forms.TextBox()
        Me.txtTerms = New System.Windows.Forms.TextBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.txtFax = New System.Windows.Forms.TextBox()
        Me.txtPhone2 = New System.Windows.Forms.TextBox()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.txtContact = New System.Windows.Forms.TextBox()
        Me.txtAccountNo = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtCompName = New System.Windows.Forms.TextBox()
        Me.txtSupplierType = New System.Windows.Forms.TextBox()
        Me.txtSuppplierName = New System.Windows.Forms.TextBox()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnEditSupplier = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbldteRange = New System.Windows.Forms.Label()
        Me.grdSupplier3 = New System.Windows.Forms.DataGridView()
        Me.dteDateRange = New System.Windows.Forms.ComboBox()
        Me.cboFilterBills = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cboShow = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.grdSupplier2 = New System.Windows.Forms.DataGridView()
        Me.dteRange = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboFilterPO = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolStrip1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TabSuppliernTransaction.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdSupplier1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdTransaction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.grdRate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelVendorChild.SuspendLayout()
        Me.spltSupplierInfo.Panel1.SuspendLayout()
        Me.spltSupplierInfo.Panel2.SuspendLayout()
        Me.spltSupplierInfo.SuspendLayout()
        CType(Me.grdSupplier3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdSupplier2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_NewSupplier, Me.ToolStripButton2, Me.ToolStripButton3, Me.ToolStripButton4, Me.ToolStripButton5})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1118, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_NewSupplier
        '
        Me.ts_NewSupplier.Image = CType(resources.GetObject("ts_NewSupplier.Image"), System.Drawing.Image)
        Me.ts_NewSupplier.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_NewSupplier.Name = "ts_NewSupplier"
        Me.ts_NewSupplier.Size = New System.Drawing.Size(101, 22)
        Me.ts_NewSupplier.Text = "New Member"
        Me.ts_NewSupplier.Visible = False
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_EnterBills, Me.ts_PayBills, Me.ToolStripSeparator1, Me.ts_PurchaseOrders, Me.ts_RcvItemAndBill, Me.ts_RcvItem, Me.ts_EnterBillforRcvItem})
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(133, 22)
        Me.ToolStripButton2.Text = "New Transaction"
        Me.ToolStripButton2.Visible = False
        '
        'ts_EnterBills
        '
        Me.ts_EnterBills.Name = "ts_EnterBills"
        Me.ts_EnterBills.Size = New System.Drawing.Size(232, 22)
        Me.ts_EnterBills.Text = "Enter Bills"
        '
        'ts_PayBills
        '
        Me.ts_PayBills.Name = "ts_PayBills"
        Me.ts_PayBills.Size = New System.Drawing.Size(232, 22)
        Me.ts_PayBills.Text = "Pay Bills"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(229, 6)
        '
        'ts_PurchaseOrders
        '
        Me.ts_PurchaseOrders.Name = "ts_PurchaseOrders"
        Me.ts_PurchaseOrders.Size = New System.Drawing.Size(232, 22)
        Me.ts_PurchaseOrders.Text = "Purchase Orders"
        '
        'ts_RcvItemAndBill
        '
        Me.ts_RcvItemAndBill.Name = "ts_RcvItemAndBill"
        Me.ts_RcvItemAndBill.Size = New System.Drawing.Size(232, 22)
        Me.ts_RcvItemAndBill.Text = "Receive Item and Bill"
        '
        'ts_RcvItem
        '
        Me.ts_RcvItem.Name = "ts_RcvItem"
        Me.ts_RcvItem.Size = New System.Drawing.Size(232, 22)
        Me.ts_RcvItem.Text = "Receive Item"
        '
        'ts_EnterBillforRcvItem
        '
        Me.ts_EnterBillforRcvItem.Name = "ts_EnterBillforRcvItem"
        Me.ts_EnterBillforRcvItem.Size = New System.Drawing.Size(232, 22)
        Me.ts_EnterBillforRcvItem.Text = "Enter Bill for Received Item"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_supplierList, Me.ts_Info, Me.ts_transList})
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(65, 22)
        Me.ToolStripButton3.Text = "Print"
        Me.ToolStripButton3.Visible = False
        '
        'ts_supplierList
        '
        Me.ts_supplierList.Name = "ts_supplierList"
        Me.ts_supplierList.Size = New System.Drawing.Size(214, 22)
        Me.ts_supplierList.Text = "Supplier List"
        '
        'ts_Info
        '
        Me.ts_Info.Name = "ts_Info"
        Me.ts_Info.Size = New System.Drawing.Size(214, 22)
        Me.ts_Info.Text = "Supplier Information"
        '
        'ts_transList
        '
        Me.ts_transList.Name = "ts_transList"
        Me.ts_transList.Size = New System.Drawing.Size(214, 22)
        Me.ts_transList.Text = "Supplier Transaction List"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_ExportSupplierList, Me.ts_ExportTransactions, Me.ts_ImportFromExcel})
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(69, 22)
        Me.ToolStripButton4.Text = "Excel"
        Me.ToolStripButton4.Visible = False
        '
        'ts_ExportSupplierList
        '
        Me.ts_ExportSupplierList.Name = "ts_ExportSupplierList"
        Me.ts_ExportSupplierList.Size = New System.Drawing.Size(187, 22)
        Me.ts_ExportSupplierList.Text = "Export Supplier List"
        '
        'ts_ExportTransactions
        '
        Me.ts_ExportTransactions.Name = "ts_ExportTransactions"
        Me.ts_ExportTransactions.Size = New System.Drawing.Size(187, 22)
        Me.ts_ExportTransactions.Text = "Export Transactions"
        '
        'ts_ImportFromExcel
        '
        Me.ts_ImportFromExcel.Name = "ts_ImportFromExcel"
        Me.ts_ImportFromExcel.Size = New System.Drawing.Size(187, 22)
        Me.ts_ImportFromExcel.Text = "Import from Excel"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_PrepareLetterto, Me.ts_PrepareLetter})
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(69, 22)
        Me.ToolStripButton5.Text = "Word"
        Me.ToolStripButton5.Visible = False
        '
        'ts_PrepareLetterto
        '
        Me.ts_PrepareLetterto.Name = "ts_PrepareLetterto"
        Me.ts_PrepareLetterto.Size = New System.Drawing.Size(207, 22)
        Me.ts_PrepareLetterto.Text = "Prepare Letter to ..."
        '
        'ts_PrepareLetter
        '
        Me.ts_PrepareLetter.Name = "ts_PrepareLetter"
        Me.ts_PrepareLetter.Size = New System.Drawing.Size(207, 22)
        Me.ts_PrepareLetter.Text = "Prepare Supplier Letter"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 25)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel1.Controls.Add(Me.TabSuppliernTransaction)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label26)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label23)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dtpEnd)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dtpFrom)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblRange)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PanelVendorChild)
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdSupplier2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dteRange)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cboFilterPO)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1118, 584)
        Me.SplitContainer1.SplitterDistance = 330
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 1
        '
        'TabSuppliernTransaction
        '
        Me.TabSuppliernTransaction.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabSuppliernTransaction.Controls.Add(Me.TabPage1)
        Me.TabSuppliernTransaction.Controls.Add(Me.TabPage2)
        Me.TabSuppliernTransaction.Controls.Add(Me.TabPage3)
        Me.TabSuppliernTransaction.Location = New System.Drawing.Point(0, 0)
        Me.TabSuppliernTransaction.Name = "TabSuppliernTransaction"
        Me.TabSuppliernTransaction.SelectedIndex = 0
        Me.TabSuppliernTransaction.Size = New System.Drawing.Size(324, 582)
        Me.TabSuppliernTransaction.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.Label28)
        Me.TabPage1.Controls.Add(Me.btnSearch)
        Me.TabPage1.Controls.Add(Me.txtID)
        Me.TabPage1.Controls.Add(Me.cbofilter)
        Me.TabPage1.Controls.Add(Me.Label27)
        Me.TabPage1.Controls.Add(Me.grdSupplier1)
        Me.TabPage1.Controls.Add(Me.cboView)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(316, 556)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Member"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(49, 9)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(26, 13)
        Me.Label28.TabIndex = 22
        Me.Label28.Text = "ID:"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(222, 4)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 21
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID.Location = New System.Drawing.Point(81, 6)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(135, 21)
        Me.txtID.TabIndex = 20
        '
        'cbofilter
        '
        Me.cbofilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbofilter.FormattingEnabled = True
        Me.cbofilter.Location = New System.Drawing.Point(81, 30)
        Me.cbofilter.Name = "cbofilter"
        Me.cbofilter.Size = New System.Drawing.Size(216, 21)
        Me.cbofilter.TabIndex = 4
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(18, 33)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(58, 13)
        Me.Label27.TabIndex = 3
        Me.Label27.Text = "Filter by:"
        '
        'grdSupplier1
        '
        Me.grdSupplier1.AllowUserToAddRows = False
        Me.grdSupplier1.AllowUserToDeleteRows = False
        Me.grdSupplier1.AllowUserToResizeRows = False
        Me.grdSupplier1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdSupplier1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdSupplier1.BackgroundColor = System.Drawing.Color.White
        Me.grdSupplier1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSupplier1.GridColor = System.Drawing.Color.Black
        Me.grdSupplier1.Location = New System.Drawing.Point(0, 57)
        Me.grdSupplier1.MultiSelect = False
        Me.grdSupplier1.Name = "grdSupplier1"
        Me.grdSupplier1.ReadOnly = True
        Me.grdSupplier1.RowTemplate.Height = 24
        Me.grdSupplier1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdSupplier1.Size = New System.Drawing.Size(316, 503)
        Me.grdSupplier1.TabIndex = 2
        '
        'cboView
        '
        Me.cboView.FormattingEnabled = True
        Me.cboView.Location = New System.Drawing.Point(81, 30)
        Me.cboView.Name = "cboView"
        Me.cboView.Size = New System.Drawing.Size(216, 21)
        Me.cboView.TabIndex = 1
        Me.cboView.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "View:"
        Me.Label1.Visible = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.grdTransaction)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(316, 556)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Transaction"
        '
        'grdTransaction
        '
        Me.grdTransaction.AllowUserToDeleteRows = False
        Me.grdTransaction.AllowUserToResizeColumns = False
        Me.grdTransaction.AllowUserToResizeRows = False
        Me.grdTransaction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTransaction.BackgroundColor = System.Drawing.Color.White
        Me.grdTransaction.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdTransaction.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grdTransaction.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdTransaction.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTransaction.ColumnHeadersVisible = False
        Me.grdTransaction.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column3})
        Me.grdTransaction.Location = New System.Drawing.Point(-1, 6)
        Me.grdTransaction.Name = "grdTransaction"
        Me.grdTransaction.ReadOnly = True
        Me.grdTransaction.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grdTransaction.RowHeadersVisible = False
        Me.grdTransaction.RowTemplate.Height = 24
        Me.grdTransaction.Size = New System.Drawing.Size(294, 341)
        Me.grdTransaction.TabIndex = 0
        '
        'Column3
        '
        Me.Column3.HeaderText = ""
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.Label25)
        Me.TabPage3.Controls.Add(Me.Label24)
        Me.TabPage3.Controls.Add(Me.lblRateDescription)
        Me.TabPage3.Controls.Add(Me.lblRateName)
        Me.TabPage3.Controls.Add(Me.btnRateDelete)
        Me.TabPage3.Controls.Add(Me.btnRateEdit)
        Me.TabPage3.Controls.Add(Me.btnRateAdd)
        Me.TabPage3.Controls.Add(Me.grdRate)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(316, 556)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Rate"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(7, 424)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(112, 13)
        Me.Label25.TabIndex = 34
        Me.Label25.Text = "Category Description:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(7, 384)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(86, 13)
        Me.Label24.TabIndex = 33
        Me.Label24.Text = "Category Name:"
        '
        'lblRateDescription
        '
        Me.lblRateDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRateDescription.Location = New System.Drawing.Point(27, 439)
        Me.lblRateDescription.Name = "lblRateDescription"
        Me.lblRateDescription.Size = New System.Drawing.Size(231, 105)
        Me.lblRateDescription.TabIndex = 32
        Me.lblRateDescription.Text = "Rate Description"
        Me.lblRateDescription.Visible = False
        '
        'lblRateName
        '
        Me.lblRateName.AutoSize = True
        Me.lblRateName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRateName.Location = New System.Drawing.Point(27, 400)
        Me.lblRateName.Name = "lblRateName"
        Me.lblRateName.Size = New System.Drawing.Size(69, 13)
        Me.lblRateName.TabIndex = 31
        Me.lblRateName.Text = "Rate Name"
        Me.lblRateName.Visible = False
        '
        'btnRateDelete
        '
        Me.btnRateDelete.Location = New System.Drawing.Point(183, 326)
        Me.btnRateDelete.Name = "btnRateDelete"
        Me.btnRateDelete.Size = New System.Drawing.Size(85, 21)
        Me.btnRateDelete.TabIndex = 30
        Me.btnRateDelete.Text = "&Delete"
        Me.btnRateDelete.UseVisualStyleBackColor = True
        '
        'btnRateEdit
        '
        Me.btnRateEdit.Location = New System.Drawing.Point(98, 326)
        Me.btnRateEdit.Name = "btnRateEdit"
        Me.btnRateEdit.Size = New System.Drawing.Size(85, 21)
        Me.btnRateEdit.TabIndex = 29
        Me.btnRateEdit.Text = "&Edit"
        Me.btnRateEdit.UseVisualStyleBackColor = True
        '
        'btnRateAdd
        '
        Me.btnRateAdd.Location = New System.Drawing.Point(13, 326)
        Me.btnRateAdd.Name = "btnRateAdd"
        Me.btnRateAdd.Size = New System.Drawing.Size(85, 21)
        Me.btnRateAdd.TabIndex = 28
        Me.btnRateAdd.Text = "&Add"
        Me.btnRateAdd.UseVisualStyleBackColor = True
        '
        'grdRate
        '
        Me.grdRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRate.Location = New System.Drawing.Point(3, 2)
        Me.grdRate.Name = "grdRate"
        Me.grdRate.RowTemplate.Height = 24
        Me.grdRate.Size = New System.Drawing.Size(286, 318)
        Me.grdRate.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(604, 14)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(33, 13)
        Me.Label26.TabIndex = 9
        Me.Label26.Text = "End:"
        Me.Label26.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(416, 14)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(40, 13)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Start:"
        Me.Label23.Visible = False
        '
        'dtpEnd
        '
        Me.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnd.Location = New System.Drawing.Point(644, 11)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(125, 21)
        Me.dtpEnd.TabIndex = 7
        Me.dtpEnd.Visible = False
        '
        'dtpFrom
        '
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFrom.Location = New System.Drawing.Point(473, 11)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(125, 21)
        Me.dtpFrom.TabIndex = 6
        Me.dtpFrom.Value = New Date(2010, 1, 1, 0, 0, 0, 0)
        Me.dtpFrom.Visible = False
        '
        'lblRange
        '
        Me.lblRange.AutoSize = True
        Me.lblRange.Location = New System.Drawing.Point(566, 14)
        Me.lblRange.Name = "lblRange"
        Me.lblRange.Size = New System.Drawing.Size(0, 13)
        Me.lblRange.TabIndex = 5
        '
        'PanelVendorChild
        '
        Me.PanelVendorChild.Controls.Add(Me.spltSupplierInfo)
        Me.PanelVendorChild.Location = New System.Drawing.Point(18, 48)
        Me.PanelVendorChild.Name = "PanelVendorChild"
        Me.PanelVendorChild.Size = New System.Drawing.Size(758, 529)
        Me.PanelVendorChild.TabIndex = 0
        '
        'spltSupplierInfo
        '
        Me.spltSupplierInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spltSupplierInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.spltSupplierInfo.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.spltSupplierInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spltSupplierInfo.Location = New System.Drawing.Point(0, 0)
        Me.spltSupplierInfo.MinimumSize = New System.Drawing.Size(0, 500)
        Me.spltSupplierInfo.Name = "spltSupplierInfo"
        Me.spltSupplierInfo.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'spltSupplierInfo.Panel1
        '
        Me.spltSupplierInfo.Panel1.AutoScroll = True
        Me.spltSupplierInfo.Panel1.AutoScrollMinSize = New System.Drawing.Size(0, 250)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.btnPreview)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.lnkOpeningBal)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.lnkReport)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.btnEditNotes)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtBillingRateLvl)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtTerms)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtEmail)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtFax)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtPhone2)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtPhone)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtContact)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtAccountNo)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtAddress)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtCompName)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtSupplierType)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtSuppplierName)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.txtNotes)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label20)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label11)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label18)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label19)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.btnEditSupplier)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label17)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label16)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label15)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label14)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label13)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label12)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label4)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label10)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label9)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label8)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label7)
        Me.spltSupplierInfo.Panel1.Controls.Add(Me.Label6)
        Me.spltSupplierInfo.Panel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'spltSupplierInfo.Panel2
        '
        Me.spltSupplierInfo.Panel2.AutoScroll = True
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.lbldteRange)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.grdSupplier3)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.dteDateRange)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.cboFilterBills)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.Label22)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.Label21)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.cboShow)
        Me.spltSupplierInfo.Panel2.Controls.Add(Me.Label5)
        Me.spltSupplierInfo.Panel2MinSize = 100
        Me.spltSupplierInfo.Size = New System.Drawing.Size(758, 578)
        Me.spltSupplierInfo.SplitterDistance = 294
        Me.spltSupplierInfo.TabIndex = 0
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPreview.Enabled = False
        Me.btnPreview.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = CType(resources.GetObject("btnPreview.Image"), System.Drawing.Image)
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(611, 257)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(115, 32)
        Me.btnPreview.TabIndex = 34
        Me.btnPreview.Text = "Print Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'lnkOpeningBal
        '
        Me.lnkOpeningBal.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lnkOpeningBal.AutoSize = True
        Me.lnkOpeningBal.Location = New System.Drawing.Point(487, 266)
        Me.lnkOpeningBal.Name = "lnkOpeningBal"
        Me.lnkOpeningBal.Size = New System.Drawing.Size(103, 13)
        Me.lnkOpeningBal.TabIndex = 33
        Me.lnkOpeningBal.TabStop = True
        Me.lnkOpeningBal.Text = "Opening Balance"
        Me.lnkOpeningBal.Visible = False
        '
        'lnkReport
        '
        Me.lnkReport.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lnkReport.AutoSize = True
        Me.lnkReport.Location = New System.Drawing.Point(393, 266)
        Me.lnkReport.Name = "lnkReport"
        Me.lnkReport.Size = New System.Drawing.Size(81, 13)
        Me.lnkReport.TabIndex = 32
        Me.lnkReport.TabStop = True
        Me.lnkReport.Text = "Quick Report"
        Me.lnkReport.Visible = False
        '
        'btnEditNotes
        '
        Me.btnEditNotes.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnEditNotes.Enabled = False
        Me.btnEditNotes.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditNotes.Image = CType(resources.GetObject("btnEditNotes.Image"), System.Drawing.Image)
        Me.btnEditNotes.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditNotes.Location = New System.Drawing.Point(264, 257)
        Me.btnEditNotes.Name = "btnEditNotes"
        Me.btnEditNotes.Size = New System.Drawing.Size(102, 32)
        Me.btnEditNotes.TabIndex = 31
        Me.btnEditNotes.Text = "Edit Note"
        Me.btnEditNotes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditNotes.UseVisualStyleBackColor = True
        Me.btnEditNotes.Visible = False
        '
        'txtBillingRateLvl
        '
        Me.txtBillingRateLvl.BackColor = System.Drawing.Color.White
        Me.txtBillingRateLvl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBillingRateLvl.Location = New System.Drawing.Point(498, 164)
        Me.txtBillingRateLvl.Name = "txtBillingRateLvl"
        Me.txtBillingRateLvl.ReadOnly = True
        Me.txtBillingRateLvl.Size = New System.Drawing.Size(245, 21)
        Me.txtBillingRateLvl.TabIndex = 30
        '
        'txtTerms
        '
        Me.txtTerms.BackColor = System.Drawing.Color.White
        Me.txtTerms.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTerms.Location = New System.Drawing.Point(498, 141)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.ReadOnly = True
        Me.txtTerms.Size = New System.Drawing.Size(245, 21)
        Me.txtTerms.TabIndex = 29
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Location = New System.Drawing.Point(498, 118)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.ReadOnly = True
        Me.txtEmail.Size = New System.Drawing.Size(245, 21)
        Me.txtEmail.TabIndex = 28
        '
        'txtFax
        '
        Me.txtFax.BackColor = System.Drawing.Color.White
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.Location = New System.Drawing.Point(498, 95)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.ReadOnly = True
        Me.txtFax.Size = New System.Drawing.Size(245, 21)
        Me.txtFax.TabIndex = 27
        '
        'txtPhone2
        '
        Me.txtPhone2.BackColor = System.Drawing.Color.White
        Me.txtPhone2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone2.Location = New System.Drawing.Point(498, 72)
        Me.txtPhone2.Name = "txtPhone2"
        Me.txtPhone2.ReadOnly = True
        Me.txtPhone2.Size = New System.Drawing.Size(245, 21)
        Me.txtPhone2.TabIndex = 26
        '
        'txtPhone
        '
        Me.txtPhone.BackColor = System.Drawing.Color.White
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.Location = New System.Drawing.Point(498, 49)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.ReadOnly = True
        Me.txtPhone.Size = New System.Drawing.Size(245, 21)
        Me.txtPhone.TabIndex = 25
        '
        'txtContact
        '
        Me.txtContact.BackColor = System.Drawing.Color.White
        Me.txtContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContact.Location = New System.Drawing.Point(498, 26)
        Me.txtContact.Name = "txtContact"
        Me.txtContact.ReadOnly = True
        Me.txtContact.Size = New System.Drawing.Size(245, 21)
        Me.txtContact.TabIndex = 24
        '
        'txtAccountNo
        '
        Me.txtAccountNo.BackColor = System.Drawing.Color.White
        Me.txtAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNo.Location = New System.Drawing.Point(498, 187)
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.ReadOnly = True
        Me.txtAccountNo.Size = New System.Drawing.Size(245, 21)
        Me.txtAccountNo.TabIndex = 23
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.Color.White
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(126, 118)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ReadOnly = True
        Me.txtAddress.Size = New System.Drawing.Size(245, 90)
        Me.txtAddress.TabIndex = 22
        '
        'txtCompName
        '
        Me.txtCompName.BackColor = System.Drawing.Color.White
        Me.txtCompName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompName.Location = New System.Drawing.Point(126, 72)
        Me.txtCompName.Multiline = True
        Me.txtCompName.Name = "txtCompName"
        Me.txtCompName.ReadOnly = True
        Me.txtCompName.Size = New System.Drawing.Size(245, 44)
        Me.txtCompName.TabIndex = 21
        '
        'txtSupplierType
        '
        Me.txtSupplierType.BackColor = System.Drawing.Color.White
        Me.txtSupplierType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSupplierType.Location = New System.Drawing.Point(126, 49)
        Me.txtSupplierType.Name = "txtSupplierType"
        Me.txtSupplierType.ReadOnly = True
        Me.txtSupplierType.Size = New System.Drawing.Size(245, 21)
        Me.txtSupplierType.TabIndex = 20
        '
        'txtSuppplierName
        '
        Me.txtSuppplierName.BackColor = System.Drawing.Color.White
        Me.txtSuppplierName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSuppplierName.Location = New System.Drawing.Point(126, 26)
        Me.txtSuppplierName.Name = "txtSuppplierName"
        Me.txtSuppplierName.ReadOnly = True
        Me.txtSuppplierName.Size = New System.Drawing.Size(245, 21)
        Me.txtSuppplierName.TabIndex = 19
        '
        'txtNotes
        '
        Me.txtNotes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.txtNotes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.txtNotes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl
        Me.txtNotes.BackColor = System.Drawing.Color.White
        Me.txtNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.Location = New System.Drawing.Point(126, 218)
        Me.txtNotes.MaxLength = 100
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ReadOnly = True
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtNotes.Size = New System.Drawing.Size(618, 33)
        Me.txtNotes.TabIndex = 18
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(15, 221)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(39, 13)
        Me.Label20.TabIndex = 17
        Me.Label20.Text = "Notes"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(393, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Contact"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(393, 168)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(105, 13)
        Me.Label18.TabIndex = 14
        Me.Label18.Text = "Billing Rate Level"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(16, 202)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(735, 13)
        Me.Label19.TabIndex = 16
        Me.Label19.Text = "_________________________________________________________________________________" & _
            "_______________________"
        '
        'btnEditSupplier
        '
        Me.btnEditSupplier.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnEditSupplier.Enabled = False
        Me.btnEditSupplier.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditSupplier.Image = CType(resources.GetObject("btnEditSupplier.Image"), System.Drawing.Image)
        Me.btnEditSupplier.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditSupplier.Location = New System.Drawing.Point(126, 257)
        Me.btnEditSupplier.Name = "btnEditSupplier"
        Me.btnEditSupplier.Size = New System.Drawing.Size(132, 32)
        Me.btnEditSupplier.TabIndex = 15
        Me.btnEditSupplier.Text = "Edit Member"
        Me.btnEditSupplier.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditSupplier.UseVisualStyleBackColor = True
        Me.btnEditSupplier.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(393, 145)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(43, 13)
        Me.Label17.TabIndex = 13
        Me.Label17.Text = "Terms"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(393, 190)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(75, 13)
        Me.Label16.TabIndex = 12
        Me.Label16.Text = "Account No."
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(393, 122)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(38, 13)
        Me.Label15.TabIndex = 11
        Me.Label15.Text = "Email"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(393, 99)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(27, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Fax"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(393, 76)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Alt. Phone"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(393, 53)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Phone"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Member Information"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 10)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(735, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "_________________________________________________________________________________" & _
            "_______________________"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 118)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Address"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 75)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Company Name"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 52)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Type"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Name"
        '
        'lbldteRange
        '
        Me.lbldteRange.AutoEllipsis = True
        Me.lbldteRange.AutoSize = True
        Me.lbldteRange.Location = New System.Drawing.Point(660, 17)
        Me.lbldteRange.Name = "lbldteRange"
        Me.lbldteRange.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lbldteRange.Size = New System.Drawing.Size(51, 13)
        Me.lbldteRange.TabIndex = 9
        Me.lbldteRange.Text = "1/1/1900"
        '
        'grdSupplier3
        '
        Me.grdSupplier3.AllowUserToAddRows = False
        Me.grdSupplier3.AllowUserToDeleteRows = False
        Me.grdSupplier3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdSupplier3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdSupplier3.BackgroundColor = System.Drawing.Color.White
        Me.grdSupplier3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSupplier3.GridColor = System.Drawing.Color.Black
        Me.grdSupplier3.Location = New System.Drawing.Point(-2, 41)
        Me.grdSupplier3.MinimumSize = New System.Drawing.Size(0, 190)
        Me.grdSupplier3.Name = "grdSupplier3"
        Me.grdSupplier3.RowTemplate.Height = 24
        Me.grdSupplier3.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdSupplier3.Size = New System.Drawing.Size(775, 511)
        Me.grdSupplier3.TabIndex = 8
        '
        'dteDateRange
        '
        Me.dteDateRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.dteDateRange.DropDownWidth = 155
        Me.dteDateRange.FormattingEnabled = True
        Me.dteDateRange.Location = New System.Drawing.Point(202, 14)
        Me.dteDateRange.Name = "dteDateRange"
        Me.dteDateRange.Size = New System.Drawing.Size(143, 21)
        Me.dteDateRange.TabIndex = 7
        '
        'cboFilterBills
        '
        Me.cboFilterBills.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterBills.Enabled = False
        Me.cboFilterBills.FormattingEnabled = True
        Me.cboFilterBills.Location = New System.Drawing.Point(415, 14)
        Me.cboFilterBills.Name = "cboFilterBills"
        Me.cboFilterBills.Size = New System.Drawing.Size(128, 21)
        Me.cboFilterBills.TabIndex = 6
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(168, 17)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(30, 13)
        Me.Label22.TabIndex = 5
        Me.Label22.Text = "Date"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Enabled = False
        Me.Label21.Location = New System.Drawing.Point(363, 17)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(46, 13)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Filter By"
        '
        'cboShow
        '
        Me.cboShow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShow.FormattingEnabled = True
        Me.cboShow.Location = New System.Drawing.Point(45, 14)
        Me.cboShow.Name = "cboShow"
        Me.cboShow.Size = New System.Drawing.Size(115, 21)
        Me.cboShow.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Show"
        '
        'grdSupplier2
        '
        Me.grdSupplier2.AllowUserToAddRows = False
        Me.grdSupplier2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdSupplier2.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdSupplier2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.grdSupplier2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSupplier2.GridColor = System.Drawing.Color.Black
        Me.grdSupplier2.Location = New System.Drawing.Point(-5, 47)
        Me.grdSupplier2.Name = "grdSupplier2"
        Me.grdSupplier2.ReadOnly = True
        Me.grdSupplier2.RowTemplate.Height = 24
        Me.grdSupplier2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdSupplier2.Size = New System.Drawing.Size(506, 338)
        Me.grdSupplier2.TabIndex = 4
        '
        'dteRange
        '
        Me.dteRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.dteRange.DropDownWidth = 140
        Me.dteRange.FormattingEnabled = True
        Me.dteRange.Location = New System.Drawing.Point(275, 11)
        Me.dteRange.Name = "dteRange"
        Me.dteRange.Size = New System.Drawing.Size(140, 21)
        Me.dteRange.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(234, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Date"
        '
        'cboFilterPO
        '
        Me.cboFilterPO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterPO.DropDownWidth = 140
        Me.cboFilterPO.FormattingEnabled = True
        Me.cboFilterPO.Location = New System.Drawing.Point(80, 11)
        Me.cboFilterPO.Name = "cboFilterPO"
        Me.cboFilterPO.Size = New System.Drawing.Size(140, 21)
        Me.cboFilterPO.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Filter By"
        '
        'frm_vend_masterVendor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1118, 609)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(602, 478)
        Me.Name = "frm_vend_masterVendor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Member Master"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TabSuppliernTransaction.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdSupplier1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdTransaction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.grdRate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelVendorChild.ResumeLayout(False)
        Me.spltSupplierInfo.Panel1.ResumeLayout(False)
        Me.spltSupplierInfo.Panel1.PerformLayout()
        Me.spltSupplierInfo.Panel2.ResumeLayout(False)
        Me.spltSupplierInfo.Panel2.PerformLayout()
        Me.spltSupplierInfo.ResumeLayout(False)
        CType(Me.grdSupplier3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdSupplier2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_NewSupplier As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_EnterBills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PayBills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_PurchaseOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_RcvItemAndBill As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_RcvItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_EnterBillforRcvItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_supplierList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Info As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_transList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_ExportSupplierList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_ExportTransactions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_ImportFromExcel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_PrepareLetterto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PrepareLetter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TabSuppliernTransaction As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdSupplier2 As System.Windows.Forms.DataGridView
    Friend WithEvents dteRange As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboFilterPO As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grdSupplier1 As System.Windows.Forms.DataGridView
    Friend WithEvents cboView As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanelVendorChild As System.Windows.Forms.Panel
    Friend WithEvents spltSupplierInfo As System.Windows.Forms.SplitContainer
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnEditSupplier As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCompName As System.Windows.Forms.TextBox
    Friend WithEvents txtSupplierType As System.Windows.Forms.TextBox
    Friend WithEvents txtSuppplierName As System.Windows.Forms.TextBox
    Friend WithEvents txtContact As System.Windows.Forms.TextBox
    Friend WithEvents txtAccountNo As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtBillingRateLvl As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents txtFax As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents btnEditNotes As System.Windows.Forms.Button
    Friend WithEvents lnkOpeningBal As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkReport As System.Windows.Forms.LinkLabel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboShow As System.Windows.Forms.ComboBox
    Friend WithEvents grdSupplier3 As System.Windows.Forms.DataGridView
    Friend WithEvents dteDateRange As System.Windows.Forms.ComboBox
    Friend WithEvents cboFilterBills As System.Windows.Forms.ComboBox
    Friend WithEvents lbldteRange As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents grdRate As System.Windows.Forms.DataGridView
    Friend WithEvents btnRateAdd As System.Windows.Forms.Button
    Friend WithEvents btnRateDelete As System.Windows.Forms.Button
    Friend WithEvents btnRateEdit As System.Windows.Forms.Button
    Friend WithEvents lblRateName As System.Windows.Forms.Label
    Friend WithEvents lblRateDescription As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lblRange As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents grdTransaction As System.Windows.Forms.DataGridView
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cbofilter As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents txtID As System.Windows.Forms.TextBox
End Class
