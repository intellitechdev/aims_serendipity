Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_masterVendorDefineFields
    Private gCon As New Clsappconfiguration

    Private sKeyOtherInfo As String

    Private Sub frm_vend_masterVendorDefinedFields_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Settings("")
        m_otherInfoList(grdDefineFields)
        grdSettings()
    End Sub

    Private Sub grdSettings()
        With grdDefineFields
            .Columns(0).Visible = False
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns.Item(1).HeaderText = "Field Name"
            .Columns.Item(2).HeaderText = "Customer"
            .Columns.Item(3).HeaderText = "Supplier"
        End With
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Settings("Add")
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Settings("Edit")
    End Sub

    Private Sub btnDeleteSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSave.Click
        If btnDeleteSave.Text = "Delete" Then
            Settings("Delete")
        Else
            Settings("Save")
        End If
    End Sub

    Private Sub Settings(ByVal sMode As String)
        Select Case sMode
            Case "Add"
                clearText()
                grdDefineFields.Visible = False
                btnDeleteSave.Text = "Save"

                btnAdd.Enabled = False
                btnEdit.Enabled = False
                sKeyOtherInfo = Guid.NewGuid.ToString
            Case "Edit"
                grdDefineFields.Visible = False
                btnDeleteSave.Text = "Save"

                btnAdd.Enabled = False
                btnEdit.Enabled = False
                
            Case "Delete"
                deleteOtherInfo()
                grdDefineFields.Visible = True
                btnAdd.Enabled = True
                btnEdit.Enabled = True
                btnDeleteSave.Text = "Delete"
                Settings("")
                m_otherInfoList(grdDefineFields)
                grdSettings()
                formSettings()            
            Case "Save"
                If Trim(txtFieldName.Text) = "" Then
                    MessageBox.Show("Field Name cannot be empty!")
                    Exit Select
                End If
                updateOtherInfo()

                grdDefineFields.Visible = True
                btnAdd.Enabled = True
                btnEdit.Enabled = True
                btnDeleteSave.Text = "Delete"
                Settings("")
                m_otherInfoList(grdDefineFields)
                grdSettings()
                formSettings()
            Case Else
                grdDefineFields.Visible = True
                btnAdd.Enabled = True
                btnEdit.Enabled = True
                btnDeleteSave.Text = "Delete"
        End Select

    End Sub

    Private Sub formSettings()
        m_otherInfoValueGetVal(frm_vend_masterVendorAddEdit.KeySupplier, True)
        m_otherInfoValList(frm_vend_masterVendorAddEdit.grdDefineFields)

        m_otherInfoValueGetVal(frm_cust_masterCustomerAddEdit.KeyCustomer, False)
        m_otherInfoValList(frm_cust_masterCustomerAddEdit.grdDefinedField)
    End Sub

    Private Sub clearText()
        txtFieldName.Text = ""
        chkCustomer.Checked = False
        chkSupplier.Checked = False
    End Sub

    Private Sub updateOtherInfo()
        Dim sSQLCmd As String = "usp_m_otherInfoMaster_Update "
        sSQLCmd &= "@fxKeyOtherInfo = '" & sKeyOtherInfo & "'"
        sSQLCmd &= ",@fcOtherInfoName = '" & txtFieldName.Text & "'"
        sSQLCmd &= ",@fbCustomer = " & IIf(chkCustomer.Checked = True, 1, 0)
        sSQLCmd &= ",@fbSupplier = " & IIf(chkSupplier.Checked = True, 1, 0)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated!", "Update Other Info.")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Other Info.")
        End Try
    End Sub

    Private Sub deleteOtherInfo()
        Dim sSQLCmd As String = "usp_m_otherInfoMaster_delete @fxKeyOtherInfo = '" & sKeyOtherInfo & "'"
        Try
            If MessageBox.Show("Are you sure you want to delete this record?", "Delete Other Info.", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted!", "Delete Other Info.")
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Other Info.")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub grdDefineFields_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDefineFields.CellClick
        If Me.grdDefineFields.SelectedRows.Count > 0 AndAlso _
                                  Not Me.grdDefineFields.SelectedRows(0).Index = _
                                  Me.grdDefineFields.Rows.Count - 1 Then
            sKeyOtherInfo = grdDefineFields.CurrentRow.Cells(0).Value.ToString
            loadOtherInfo()
        End If
    End Sub

    Private Sub loadOtherInfo()
        Dim sSQLCmd As String = "SELECT * FROM mOtherInfo00Master WHERE fxKeyOtherInfo = '" & sKeyOtherInfo & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtFieldName.Text = rd.Item("fcOtherInfoName")
                    chkCustomer.Checked = rd.Item("fbCustomer")
                    chkSupplier.Checked = rd.Item("fbSupplier")
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Define Fields")
        End Try
    End Sub

    Private Sub grdDefineFields_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDefineFields.DoubleClick
        If Me.grdDefineFields.SelectedRows.Count > 0 AndAlso _
                                 Not Me.grdDefineFields.SelectedRows(0).Index = _
                                 Me.grdDefineFields.Rows.Count - 1 Then
            sKeyOtherInfo = grdDefineFields.CurrentRow.Cells(0).Value.ToString
            loadOtherInfo()
            Settings("Edit")
        End If
    End Sub
End Class