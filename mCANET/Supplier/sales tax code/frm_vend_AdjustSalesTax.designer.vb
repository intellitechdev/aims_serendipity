<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_AdjustSalesTax
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.dteAdjustmentDate = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtEntryNo = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboTaxSupplier = New System.Windows.Forms.ComboBox
        Me.cboAdjustment = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.rbtnReduce = New System.Windows.Forms.RadioButton
        Me.rbtnIncrease = New System.Windows.Forms.RadioButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Adjustment &Date"
        '
        'dteAdjustmentDate
        '
        Me.dteAdjustmentDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteAdjustmentDate.Location = New System.Drawing.Point(120, 9)
        Me.dteAdjustmentDate.Name = "dteAdjustmentDate"
        Me.dteAdjustmentDate.Size = New System.Drawing.Size(93, 20)
        Me.dteAdjustmentDate.TabIndex = 1
        Me.dteAdjustmentDate.Value = New Date(2007, 8, 28, 0, 0, 0, 0)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Entry No."
        '
        'txtEntryNo
        '
        Me.txtEntryNo.Location = New System.Drawing.Point(120, 39)
        Me.txtEntryNo.Name = "txtEntryNo"
        Me.txtEntryNo.Size = New System.Drawing.Size(93, 20)
        Me.txtEntryNo.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Sales Tax &Supplier"
        '
        'cboTaxSupplier
        '
        Me.cboTaxSupplier.FormattingEnabled = True
        Me.cboTaxSupplier.Location = New System.Drawing.Point(120, 66)
        Me.cboTaxSupplier.Name = "cboTaxSupplier"
        Me.cboTaxSupplier.Size = New System.Drawing.Size(194, 21)
        Me.cboTaxSupplier.TabIndex = 5
        '
        'cboAdjustment
        '
        Me.cboAdjustment.FormattingEnabled = True
        Me.cboAdjustment.Location = New System.Drawing.Point(120, 93)
        Me.cboAdjustment.Name = "cboAdjustment"
        Me.cboAdjustment.Size = New System.Drawing.Size(194, 21)
        Me.cboAdjustment.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "&Adjustment Account"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtAmount)
        Me.GroupBox1.Controls.Add(Me.rbtnReduce)
        Me.GroupBox1.Controls.Add(Me.rbtnIncrease)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 134)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(298, 79)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Adjustment"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(187, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Amount"
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(190, 43)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(80, 20)
        Me.txtAmount.TabIndex = 4
        '
        'rbtnReduce
        '
        Me.rbtnReduce.AutoSize = True
        Me.rbtnReduce.Location = New System.Drawing.Point(30, 46)
        Me.rbtnReduce.Name = "rbtnReduce"
        Me.rbtnReduce.Size = New System.Drawing.Size(128, 17)
        Me.rbtnReduce.TabIndex = 1
        Me.rbtnReduce.TabStop = True
        Me.rbtnReduce.Text = "Reduce Sales Tax By"
        Me.rbtnReduce.UseVisualStyleBackColor = True
        '
        'rbtnIncrease
        '
        Me.rbtnIncrease.AutoSize = True
        Me.rbtnIncrease.Location = New System.Drawing.Point(30, 23)
        Me.rbtnIncrease.Name = "rbtnIncrease"
        Me.rbtnIncrease.Size = New System.Drawing.Size(131, 17)
        Me.rbtnIncrease.TabIndex = 0
        Me.rbtnIncrease.TabStop = True
        Me.rbtnIncrease.Text = "Increase Sales Tax By"
        Me.rbtnIncrease.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 227)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Memo"
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(64, 227)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(249, 20)
        Me.txtMemo.TabIndex = 10
        Me.txtMemo.Text = "Sales Tax Adjustment"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(164, 257)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 11
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(241, 257)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frm_vend_AdjustSalesTax
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(325, 292)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboAdjustment)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboTaxSupplier)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtEntryNo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dteAdjustmentDate)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_vend_AdjustSalesTax"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sales Tax Adjust"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteAdjustmentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEntryNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboTaxSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents cboAdjustment As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents rbtnReduce As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnIncrease As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
