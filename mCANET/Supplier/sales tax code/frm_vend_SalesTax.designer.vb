<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_SalesTax
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_SalesTax))
        Me.grd_SalesTax = New System.Windows.Forms.DataGridView
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_New = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Edit = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Delete = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_MkInactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Inactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_CustomizeCol = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Print = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripDropDownButton2 = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_createInvoice = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_salesReceipt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_createCredit = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_paySalesTax = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_AdjustSales = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripDropDownButton3 = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_GenReport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_SalesTaxLiabilities = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_SalesTaxRevenue = New System.Windows.Forms.ToolStripMenuItem
        Me.chkIncludeInactive = New System.Windows.Forms.CheckBox
        CType(Me.grd_SalesTax, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grd_SalesTax
        '
        Me.grd_SalesTax.AllowUserToAddRows = False
        Me.grd_SalesTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd_SalesTax.Location = New System.Drawing.Point(0, 48)
        Me.grd_SalesTax.Name = "grd_SalesTax"
        Me.grd_SalesTax.Size = New System.Drawing.Size(361, 150)
        Me.grd_SalesTax.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripDropDownButton1, Me.ToolStripDropDownButton2, Me.ToolStripDropDownButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(361, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_New, Me.ts_Edit, Me.ts_Delete, Me.ToolStripSeparator1, Me.ts_MkInactive, Me.ts_Inactive, Me.ts_CustomizeCol, Me.ToolStripSeparator2, Me.ts_Print})
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(94, 22)
        Me.ToolStripDropDownButton1.Text = "&Sales Tax Code"
        '
        'ts_New
        '
        Me.ts_New.Name = "ts_New"
        Me.ts_New.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ts_New.Size = New System.Drawing.Size(232, 22)
        Me.ts_New.Text = "New"
        '
        'ts_Edit
        '
        Me.ts_Edit.Name = "ts_Edit"
        Me.ts_Edit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.ts_Edit.Size = New System.Drawing.Size(232, 22)
        Me.ts_Edit.Text = "Edit Sales Tax Code"
        '
        'ts_Delete
        '
        Me.ts_Delete.Name = "ts_Delete"
        Me.ts_Delete.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ts_Delete.Size = New System.Drawing.Size(232, 22)
        Me.ts_Delete.Text = "Delete Sales Tax Code"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(229, 6)
        '
        'ts_MkInactive
        '
        Me.ts_MkInactive.Name = "ts_MkInactive"
        Me.ts_MkInactive.Size = New System.Drawing.Size(232, 22)
        Me.ts_MkInactive.Text = "Make Sales Tax Code Inactive "
        '
        'ts_Inactive
        '
        Me.ts_Inactive.Name = "ts_Inactive"
        Me.ts_Inactive.Size = New System.Drawing.Size(232, 22)
        Me.ts_Inactive.Text = "Show Inactive Sales Tax Code"
        '
        'ts_CustomizeCol
        '
        Me.ts_CustomizeCol.Name = "ts_CustomizeCol"
        Me.ts_CustomizeCol.Size = New System.Drawing.Size(232, 22)
        Me.ts_CustomizeCol.Text = "Customize Columns..."
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(229, 6)
        '
        'ts_Print
        '
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.ts_Print.Size = New System.Drawing.Size(232, 22)
        Me.ts_Print.Text = "Print List..."
        '
        'ToolStripDropDownButton2
        '
        Me.ToolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_createInvoice, Me.ts_salesReceipt, Me.ts_createCredit, Me.ToolStripSeparator3, Me.ts_paySalesTax, Me.ts_AdjustSales})
        Me.ToolStripDropDownButton2.Image = CType(resources.GetObject("ToolStripDropDownButton2.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton2.Name = "ToolStripDropDownButton2"
        Me.ToolStripDropDownButton2.Size = New System.Drawing.Size(63, 22)
        Me.ToolStripDropDownButton2.Text = "Activities"
        '
        'ts_createInvoice
        '
        Me.ts_createInvoice.Name = "ts_createInvoice"
        Me.ts_createInvoice.Size = New System.Drawing.Size(230, 22)
        Me.ts_createInvoice.Text = "Create Invoices"
        '
        'ts_salesReceipt
        '
        Me.ts_salesReceipt.Name = "ts_salesReceipt"
        Me.ts_salesReceipt.Size = New System.Drawing.Size(230, 22)
        Me.ts_salesReceipt.Text = "Enter Sales Receipt"
        '
        'ts_createCredit
        '
        Me.ts_createCredit.Name = "ts_createCredit"
        Me.ts_createCredit.Size = New System.Drawing.Size(230, 22)
        Me.ts_createCredit.Text = "Create Credit Memos/Refunds"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(227, 6)
        '
        'ts_paySalesTax
        '
        Me.ts_paySalesTax.Name = "ts_paySalesTax"
        Me.ts_paySalesTax.Size = New System.Drawing.Size(230, 22)
        Me.ts_paySalesTax.Text = "Pay Sales Tax..."
        '
        'ts_AdjustSales
        '
        Me.ts_AdjustSales.Name = "ts_AdjustSales"
        Me.ts_AdjustSales.Size = New System.Drawing.Size(230, 22)
        Me.ts_AdjustSales.Text = "Adjust Sales Tax Due..."
        '
        'ToolStripDropDownButton3
        '
        Me.ToolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_GenReport, Me.ToolStripSeparator4, Me.ts_SalesTaxLiabilities, Me.ts_SalesTaxRevenue})
        Me.ToolStripDropDownButton3.Image = CType(resources.GetObject("ToolStripDropDownButton3.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton3.Name = "ToolStripDropDownButton3"
        Me.ToolStripDropDownButton3.Size = New System.Drawing.Size(58, 22)
        Me.ToolStripDropDownButton3.Text = "Reports"
        '
        'ts_GenReport
        '
        Me.ts_GenReport.Name = "ts_GenReport"
        Me.ts_GenReport.Size = New System.Drawing.Size(224, 22)
        Me.ts_GenReport.Text = "General Report"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(221, 6)
        '
        'ts_SalesTaxLiabilities
        '
        Me.ts_SalesTaxLiabilities.Name = "ts_SalesTaxLiabilities"
        Me.ts_SalesTaxLiabilities.Size = New System.Drawing.Size(224, 22)
        Me.ts_SalesTaxLiabilities.Text = "Sales Tax Liability"
        '
        'ts_SalesTaxRevenue
        '
        Me.ts_SalesTaxRevenue.Name = "ts_SalesTaxRevenue"
        Me.ts_SalesTaxRevenue.Size = New System.Drawing.Size(224, 22)
        Me.ts_SalesTaxRevenue.Text = "Sales Tax Revenue Summary"
        '
        'chkIncludeInactive
        '
        Me.chkIncludeInactive.AutoSize = True
        Me.chkIncludeInactive.Location = New System.Drawing.Point(12, 28)
        Me.chkIncludeInactive.Name = "chkIncludeInactive"
        Me.chkIncludeInactive.Size = New System.Drawing.Size(101, 17)
        Me.chkIncludeInactive.TabIndex = 3
        Me.chkIncludeInactive.Text = "Include in&active"
        Me.chkIncludeInactive.UseVisualStyleBackColor = True
        '
        'frm_vend_SalesTax
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(361, 198)
        Me.Controls.Add(Me.chkIncludeInactive)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.grd_SalesTax)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_vend_SalesTax"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Sales Tax Code"
        CType(Me.grd_SalesTax, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grd_SalesTax As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_New As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Edit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_MkInactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Inactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_CustomizeCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripDropDownButton2 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_createInvoice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_salesReceipt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_createCredit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_paySalesTax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_AdjustSales As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripDropDownButton3 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_SalesTaxLiabilities As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_SalesTaxRevenue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_GenReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents chkIncludeInactive As System.Windows.Forms.CheckBox
End Class
