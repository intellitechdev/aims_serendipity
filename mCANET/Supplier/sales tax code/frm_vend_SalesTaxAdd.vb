Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_SalesTaxAdd
    Private gCon As New Clsappconfiguration
    Private sSalesTaxCode As String

    Public Property KeySalesTaxCode() As String
        Get
            Return sSalesTaxCode
        End Get
        Set(ByVal value As String)
            sSalesTaxCode = value
        End Set
    End Property

    Private Sub frm_vend_SalesTaxAdd_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rbtnNoTax.Checked = True
        loadSalesTaxCode()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        addEditSalesTaxCode()
        m_chkSettings(frm_vend_SalesTax.chkIncludeInactive, frm_vend_SalesTax.SQLQuery)
        m_SalesTaxCodeList(frm_vend_SalesTax.chkIncludeInactive.CheckState, frm_vend_SalesTax.grd_SalesTax)
        m_loadSalesTaxCode(frm_cust_masterCustomerAddEdit.cboTaxCodeName, frm_cust_masterCustomerAddEdit.cboTaxItemID)
        'm_loadSalesTaxCode(frm_item_masterItemAddEdit.cboInventoryPart_taxCodeName, frm_item_masterItemAddEdit.cboInventoryPart_taxCodeID)
        m_loadSalesTaxCode(frm_cust_CreateInvoice.cboTaxCodeName, frm_cust_CreateInvoice.cboTaxCodeID)
        m_loadSalesTaxCode(frm_cust_CreateSalesOrder.cboTaxCodeName, frm_cust_CreateSalesOrder.cboTaxCodeID)
        Me.Close()
    End Sub


    Private Sub loadSalesTaxCode()
        Dim sSQLCmd As String = "SELECT * FROM mSalesTaxCode WHERE fxKeySalesTaxCode ='" & sSalesTaxCode & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtSalesCode.Text = rd.Item("fcSalesTaxCodeName")
                    txtDescription.Text = rd.Item("fcSalesTaxCodeDescription")
                    If rd.Item("fbTaxable") = True Then
                        rbtnTax.Checked = True
                    Else
                        rbtnNoTax.Checked = True
                    End If
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                End If
            End Using
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Load Sales Tax Code")
        End Try

    End Sub


    Private Sub addEditSalesTaxCode()
        Dim sSQLCmd As String = "SPU_SalesTaxCode_Update "
        sSQLCmd &= "@fxKeySalesTaxCode = '" & sSalesTaxCode & "'"
        sSQLCmd &= ",@fcSalesTaxCodeName = '" & txtSalesCode.Text & "'"
        sSQLCmd &= ",@fcSalesTaxCodeDescription = '" & txtDescription.Text & "'"
        sSQLCmd &= ",@fbTaxable =" & IIf(rbtnTax.Checked = True, 1, 0)
        sSQLCmd &= ",@fbActive =" & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ", @fxKeyCompany = '" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MsgBox("Record successfully updated", MsgBoxStyle.Information, "Add/Edit Sales Tax Code")
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Add/Edit Sales Tax Code")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class