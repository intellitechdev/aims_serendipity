Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient

Public Class frm_vend_EnterBills

#Region "Private Declarations"
    Private gTrans As New clsTransactionFunctions
    Private gCon As New Clsappconfiguration

    Private ExpenseAmount As Decimal
    Private ItemAmount As Decimal
    Private sKeyBill As String

    Private sKeyClear As Integer
    Public _SupplierBill As Boolean
    Public _Mode As Integer ' 0 = add ' 1=edit 
    Private _Type As String
    Private checkDate As New clsRestrictedDate
    Public ClassRefno As String = ""
    Private xfxKeyApAcnt As String = ""
#End Region

#Region "Properties"
    Public Property KeyBill() As String
        Get
            Return sKeyBill
        End Get
        Set(ByVal value As String)
            sKeyBill = value
        End Set
    End Property
    Public Property SupplierBill() As Boolean
        Get
            Return _SupplierBill
        End Get
        Set(ByVal value As Boolean)
            _SupplierBill = value
        End Set
    End Property
    Public Property Mode() As Integer
        Get
            Return _Mode
        End Get
        Set(ByVal value As Integer)
            _Mode = value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property
    Private saveMode As String
    Public Property GetSaveMode() As String
        Get
            Return saveMode
        End Get
        Set(ByVal value As String)
            saveMode = value
        End Set
    End Property

    Private supplierID As String
    Public Property GetSupplierID() As String
        Get
            Return supplierID
        End Get
        Set(ByVal value As String)
            supplierID = value
        End Set
    End Property

    Private terms As String
    Public Property GetTerms_ID() As String
        Get
            Return terms
        End Get
        Set(ByVal value As String)
            terms = value
        End Set
    End Property

    Private accountsPayable As String
    Public Property GetAccountsPayable() As String
        Get
            Return accountsPayable
        End Get
        Set(ByVal value As String)
            accountsPayable = value
        End Set
    End Property


#End Region

#Region "Events"
    Private Sub frm_vend_EnterBills_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        sKeyBill = ""
        sKeyClear = 0
        _Mode = 0
        ListDelete.Items.Clear()
    End Sub
    Private Sub frm_vend_EnterBills_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MessageBox.Show("Are you sure you want to close your current transaction?", "Bills", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
            ListDelete.Items.Clear()

            Dim sSqlCmd As String = "Delete from dbo.TempRefNo where RefNo = '" & txtRefNo.Text & "' and fxKeyCompany = '" & gCompanyID() & "'"
            Try
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
            Catch ex As Exception
                MsgBox("Error at frm_vend_EnterBills_FormClosed in frm_vend_EnterBills module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            End Try

            sKeyBill = ""
            sKeyClear = 0
            _Mode = 0
        Else
            e.Cancel = True
        End If


    End Sub
    Private Sub HideTab()
        Dim TempTab As TabPage
        Try

       
        TempTab = TabControl1.TabPages(1)
            TabControl1.TabPages.RemoveAt(1)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub frm_vend_EnterBills_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboTerms_Multicombo.Text = "NET30"
        HideTab()


        IsDateRestricted()

        'Load Supplier List
        Call LoadSupplierToMulticombo()
        'Load Terms
        'Call LoadTermsToMulticombobox()
        'Load Accounts Payable
        Call GetSpecifiedAcnt(MtcboAPAcnt, "Accounts Payable")


        'Classing
        If frmMain.LblClassActivator.Visible = True Then
            Label9.Visible = True
            txtClassName.Visible = True
            txtClassName.Enabled = True
            BtnBrowse.Enabled = True
            BtnBrowse.Visible = True
        Else
            Label9.Visible = False
            txtClassName.Visible = False
            txtClassName.Enabled = False
            BtnBrowse.Enabled = False
            BtnBrowse.Visible = False
        End If

        Select Case _Mode
            Case 0 'add
                If _SupplierBill Then
                    chkBillRcv.Checked = True

                    Call panelSettings()

                    ExpenseAmount = 0
                    ItemAmount = 0
                    txtAmtDue.Text = "0.00"
                    'txtRefNo.Text = gTrans.getBillReferenceNo()
                    txtLoanNo.Text = defaultReferenceNo()

                    Call CreateDataGridview_ExpensesTab()
                    Call CreateDataGridView_Items()
                Else
                    chkBillRcv.Checked = False
                    Call panelSettings()

                    txtAmtDue.Text = "0.00"
                    'txtRefNo.Text = gTrans.getBillReferenceNo()
                    txtLoanNo.Text = defaultReferenceNo()

                    Call CreateDataGridview_ExpensesTab()
                    Call CreateDataGridView_Items()

                    txtLoanNo.Text = defaultReferenceNo()
                End If

                chkBillRcv.Enabled = False

            Case 1 'edit
                If _SupplierBill Then

                    chkBillRcv.Checked = True
                    Call panelSettings()

                    txtLoanNo.Text = defaultReferenceNo()
                Else

                    chkBillRcv.Checked = False
                    Call panelSettings()
                    txtLoanNo.Text = defaultReferenceNo()
                    Call LoadBillDetails()
                End If
            Case 2 'edit not just the receipt only

                chkBillRcv.Checked = True
                Call panelSettings()
                txtLoanNo.Text = defaultReferenceNo()
                LoadBillDetails()

            Case Else
        End Select

        'For Special Transactions (e.g. Loans)
        Dim IsTrue As Boolean
        IsTrue = IsSpecialTransaction("Loan")
        lblLoanNo.Visible = IsTrue
        txtLoanNo.Visible = IsTrue

    End Sub
    Public Sub LoadTermsToMulticombobox()
        Dim DTAccounts As New DataTable("Terms")
        Dim DR As DataRow

        DTAccounts.Columns.Add("fxKeyTerms")
        DTAccounts.Columns.Add("fcTermsName")

        DR = DTAccounts.NewRow()
        DR("fcTermsName") = ""
        DTAccounts.Rows.Add(DR)

        DR = DTAccounts.NewRow()
        DR("fcTermsName") = "New"
        DTAccounts.Rows.Add(DR)

        DR = DTAccounts.NewRow()
        DR("fcTermsName") = "------"
        DTAccounts.Rows.Add(DR)

        Dim storedProc As String = "usp_m_mterms_getname"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storedProc, _
                        New SqlParameter("@coid", gCompanyID()))
                While rd.Read
                    DR = DTAccounts.NewRow()
                    DR("fxKeyTerms") = rd.Item("fxKeyTerms").ToString
                    DR("fcTermsName") = rd.Item("fcTermsName").ToString
                    DTAccounts.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        With cboTerms_Multicombo
            .Items.Clear()
            .LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
            .SourceDataString = New String(1) {"fcTermsName", "fxKeyTerms"}
            .SourceDataTable = DTAccounts
            If .Items.Count <> 0 Then
                .SelectedIndex = 0
            End If
        End With
    End Sub
    Public Sub LoadSupplierToMulticombo()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "usp_m_msupplier_getname", _
                                                     New SqlParameter("@coid", gCompanyID()))
        cboMember.ValueMember = "fxKeySupplier"
        cboMember.DisplayMember = "fcSupplierName"
        cboMember.DataSource = ds.Tables(0)
    End Sub
    Private Sub LoadBillHeader()
        Dim sSQLCmd As String = "usp_t_tBills_loaddetails "
        sSQLCmd &= " @fxKeyBill='" & sKeyBill & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then

                    cboMember.SelectedValue = rd.Item("fxKeySupplier").ToString
                    txtAddress.Text = m_getSpecificAddress(rd.Item("fxKeySupplier").ToString())
                    txtMemo.Text = rd.Item("fcMemo")
                    dteTransDate.Value = rd.Item("fdTransDate")
                    txtRefNo.Text = rd.Item("fcRefNo")
                    txtAmtDue.Text = rd.Item("fnAmountDue")
                    dteBillDue.Text = rd.Item("fdDueDate")
                    lblDateDisc.Text = rd.Item("fdDiscDate")
                    txtLoanNo.Text = rd.Item("fcReferenceNo")
                    cboTerms_Multicombo.SelectedValue = rd.Item("fcTerms").ToString()
                    txtClassRefNo.Text = rd.Item("fxClassRefNo").ToString
                    ClassRefno = rd.Item("fxClassRefNo").ToString
                    txtClassName.Text = GetClassName(ClassRefno)

                    'Get Term ID
                    If cboTerms_Multicombo.Text <> "" Then
                        GetTerms_ID() = cboTerms_Multicombo.SelectedItem.Col2
                    End If

                    If rd.Item("fxKeyAPAcnt").ToString <> "" Then
                        MtcboAPAcnt.SelectedValue = GetAccountValueForMtcbo(rd.Item("fxKeyAPAcnt").ToString)
                    End If

                    If rd.Item("fbPaid").ToString = "True" Then
                        btnSave.Enabled = False
                        LblTransStatus.Text = "Paid"
                        LblTransStatus.Visible = True
                        chkBillRcv.Enabled = False

                        txtClassName.TabStop = False
                        txtAddress.TabStop = False
                        txtMemo.TabStop = False
                        dteTransDate.TabStop = False
                        txtAmtDue.TabStop = False
                        dteBillDue.TabStop = False
                        txtLoanNo.TabStop = False
                        BtnDelete.Enabled = False
                        btnSaveOnly.Enabled = False

                        grdExpenses.ReadOnly = True
                        grdItems.ReadOnly = True
                        chkRecurring.Enabled = False
                        MtcboAPAcnt.TabStop = False
                    End If
                    If rd.Item("fbCancelled").ToString = "True" Then
                        btnSave.Enabled = False
                        LblTransStatus.Text = "Canceled"
                        LblTransStatus.Visible = True
                        chkBillRcv.Enabled = False


                        txtClassName.TabStop = False
                        txtAddress.TabStop = False
                        txtMemo.TabStop = False
                        dteTransDate.TabStop = False
                        txtAmtDue.TabStop = False
                        dteBillDue.TabStop = False
                        txtLoanNo.TabStop = False
                        btnSaveOnly.Enabled = False

                        grdExpenses.ReadOnly = True
                        grdItems.ReadOnly = True
                        BtnDelete.Enabled = False
                        chkRecurring.Enabled = False

                        MtcboAPAcnt.TabStop = False
                    End If
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub rbtnBill_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnBill.CheckedChanged
        Call gRbtnEvents()
    End Sub
    Private Sub rbtnCredit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCredit.CheckedChanged
        Call gRbtnEvents()
    End Sub
    Private Sub chkBillRcv_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkBillRcv.CheckedChanged
        Call panelSettings()
    End Sub
    Private Sub grdExpenses_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdExpenses.CellPainting
        Dim sf As New StringFormat
        Dim xDebit As Double = 0.0
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdExpenses.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If

        Try
            With Me.grdExpenses
                Dim xColCredit As Double = 0.0
                Dim xRow As Integer
                For xRow = 0 To .RowCount - 1
                    If .Rows(xRow).Cells("cCredit").Value IsNot Nothing Then
                        xColCredit += Format(CDec(.Rows(xRow).Cells("cCredit").Value), "##,##0.00")
                        .Rows(xRow).Cells("cCredit").Value = Format(CDec(.Rows(xRow).Cells("cCredit").Value), "##,##0.00")
                    Else
                        xColCredit += 0
                    End If
                Next
                txtCredittotal.Text = Format(CDec(xColCredit), "##,##0.00")
            End With
        Catch ex As Exception
        End Try

        Try
            With Me.grdExpenses
                Dim xColDebit As Double = 0.0
                Dim xRow As Integer
                For xRow = 0 To .RowCount - 1
                    If .Rows(xRow).Cells("cDebit").Value IsNot Nothing Then
                        xColDebit += Format(CDec(.Rows(xRow).Cells("cDebit").Value), "##,##0.00")
                        .Rows(xRow).Cells("cDebit").Value = Format(CDec(.Rows(xRow).Cells("cDebit").Value), "##,##0.00")
                    Else
                        xColDebit += 0
                    End If
                Next
                txtdebittotal.Text = Format(CDec(xColDebit), "##,##0.00")
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub grdExpenses_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdExpenses.EditingControlShowing
        Dim comboBoxColumn As DataGridViewComboBoxColumn = grdExpenses.Columns("cAccounts")
        If (grdExpenses.CurrentCellAddress.X = comboBoxColumn.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If

    End Sub
    Private Sub txtAmt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtAmtDue.Validating
        txtAmtDue.Text = Format(CDec(txtAmtDue.Text), "##,##0.00")
    End Sub
    Private Sub grdExpenses_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdExpenses.CellValidated
        Call gCalculateExpense()
        Call pDebit()
        Call pCredit()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If frmClosingEntries.IsPeriodClosed(dteTransDate.Value.Date, btnSave) = False Then
            SaveBills("SaveAndNew")
        End If
    End Sub
    Private Sub SaveBills(ByVal saveMode As String)
        GetSaveMode() = saveMode
        ClassRefno = txtClassRefNo.Text

        '1. Validation: Required Supplier Name
        If getSupplierID() Is Nothing Then
            MsgBox("Select a supplier name first.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
            Exit Sub
        End If

        '2. Validation: Required Terms
        'If GetTerms_ID() Is Nothing Then
        '    MsgBox("Select a term for this bill.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        '    Exit Sub
        'End If

        '3. Validation: Required Accounts Payable
        If GetAccountsPayable() Is Nothing Then
            MessageBox.Show("Select an Account Payable First.", "Access Denied")
            Exit Sub
        End If

        '4. Validation: User Prompted to confirm
        If MessageBox.Show("Are you sure you want to proceed to save?", "Bills Entry", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
            Try
                'Record Bills
                Call SaveBillsHeader()
                Call SaveBillsExpensesTab()
                MessageBox.Show("Saving of Bills Successful!", "Bills Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

        '5. Clear Form
        If GetSaveMode() = "SaveAndNew" Then
            ClearForm()
        End If

    End Sub
    Private Sub SaveBillsHeader()
        If KeyBill() Is Nothing Then
            KeyBill() = System.Guid.NewGuid.ToString()
        End If

        Try
            Dim sSQLCmd As String = "usp_t_tbillentry_save "
            sSQLCmd &= " @fxKeyBill='" & KeyBill() & "' "
            sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
            sSQLCmd &= ",@fxKeySupplier='" & GetSupplierID() & "' "
            'sSQLCmd &= ",@fxKeyTerms='" & GetTerms_ID() & "' "
            sSQLCmd &= ",@fcMemo='" & txtMemo.Text & "' "
            sSQLCmd &= ",@fdTransDate='" & dteTransDate.Value.Date & "' "
            sSQLCmd &= ",@fnAmountDue='" & CDec(txtAmtDue.Text) & "' "
            sSQLCmd &= ",@fdDueDate='" & dteBillDue.Value.Date & "' "
            sSQLCmd &= ",@fcRefNo='" & txtRefNo.Text & "' "
            sSQLCmd &= ",@fuCreatedby='" & gUserName & "' "
            sSQLCmd &= ",@fdDiscDate='" & lblDateDisc.Text & "' "
            sSQLCmd &= ",@fcAPVno='" & txtRefNo.Text & "' "
            sSQLCmd &= ",@fcReferenceNo='" & txtLoanNo.Text & "' "
            sSQLCmd &= ",@fxKeyAcnt = '" & GetAccountsPayable() & "'"


            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub SaveBillsExpensesTab()
        Dim psbol As Boolean = False
        Try

            With Me.grdExpenses
                Dim xRow As Integer

                For xRow = 0 To .Rows.Count - 2
                    Dim tbillsExpensesRowID As String = .Item(0, xRow).Value
                    If tbillsExpensesRowID <> "" Then
                        Dim sAcnt As String = ""
                        Dim sMemo As String = ""
                        Dim sDebit As Decimal = 0
                        Dim sCredit As Decimal = 0
                        Dim sName As String = ""
                        Dim bBillable As Boolean = False
                        Dim sKeyExpense As String = ""

                        If gTrans.gmkDefaultValues(xRow, 0, grdExpenses) <> Nothing Or gTrans.gmkDefaultValues(xRow, 0, grdExpenses) = "" Then
                            sKeyExpense = .Rows(xRow).Cells("cKeyExpense").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 1, grdExpenses) <> Nothing Then
                            sAcnt = .Rows(xRow).Cells("cAccounts").Value.ToString()
                        End If
                        If gTrans.gmkDefaultValues(xRow, 2, grdExpenses) <> Nothing Then
                            sDebit = .Rows(xRow).Cells("cDebit").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 3, grdExpenses) <> Nothing Then
                            sCredit = .Rows(xRow).Cells("cCredit").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 4, grdExpenses) <> Nothing Then
                            sMemo = .Rows(xRow).Cells("cMemo").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 5, grdExpenses) <> Nothing Then
                            sName = .Rows(xRow).Cells("cCustomer").Value.ToString()

                        End If

                        'Monitoring Deleted Entries'
                        For Each CollectedBills As ListViewItem In ListDelete.Items
                            Save_to_dumpfile_Collected_Bills(CollectedBills.Text) '<=======save to dump file before deletion
                            Delete_Collected_Bills(CollectedBills.Text) '<==================delete Expenses
                        Next
                        ListDelete.Items.Clear()

                        'Record Expenses
                        gTrans.gSaveBillExpenses(sKeyBill, sKeyExpense, sAcnt, sDebit, sCredit, sMemo, sName, Me)
                    End If
                Next
            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Sub recurringsetup()
        If gRecurringPeriod <> "" Then
            Select Case gRecurringPeriod
                Case "Daily"
                    Call psdailysettings()
                Case "Weekly"
                    Call psweeklysettings()
                Case "Monthly"
                    Call psmonthlysettings()
                Case "Quarterly"
                    Call psquarterlysettings()
                Case "Yearly"
                    Call psyearlysettings()
            End Select
        Else
            Dim xVal As Boolean
            xVal = psSave()
            If xVal = True Then
                MsgBox("Save Successful! ", MsgBoxStyle.Information, Me.Text)
            End If
        End If
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    
        Me.Close()
    End Sub
    Private Sub ClearForm()
        KeyBill() = Nothing
        getSupplierID() = Nothing
        txtAddress.Text = ""
        cboMember.SelectedIndex = 0
        cboTerms_Multicombo.SelectedIndex = 0
        txtMemo.Text = ""
        dteTransDate.Value = Now
        txtAmtDue.Text = "0.00"
        txtAmtDue.Text = "0.00"
        dteBillDue.Value = Now
        txtLoanNo.Text = defaultReferenceNo()

        Dim sSqlCmd As String = "Delete from dbo.TempRefNo where RefNo = '" & txtRefNo.Text & "' and fxKeyCompany = '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox("Error at frm_vend_EnterBills_FormClosed in frm_vend_EnterBills module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        txtRefNo.Text = gTrans.getBillReferenceNo()
        Call gTrans.gGridClear(Me.grdExpenses)
        Call gTrans.gGridClear(Me.grdItems)
        If grdExpenses.AllowUserToAddRows = True Then
            grdExpenses.AllowUserToAddRows = False
        End If
        grdExpenses.Rows.Clear()
        If grdExpenses.AllowUserToAddRows = False Then
            grdExpenses.AllowUserToAddRows = True
        End If
        If grdItems.AllowUserToAddRows = True Then
            grdItems.AllowUserToAddRows = False
        End If
        grdItems.Rows.Clear()
        If grdItems.AllowUserToAddRows = False Then
            grdItems.AllowUserToAddRows = True
        End If
        tb_Expenses.Text = "Expenses Php" + "0.00"
        txtdebittotal.Text = "0.00"
        txtCredittotal.Text = "0.00"
        _Mode = 0
    End Sub

#End Region

#Region "Functions/ Procedure"
    Private Sub CreateDataGridview_ExpensesTab()

        With grdExpenses
            .Columns.Clear()

            Dim cKeyExpense As New DataGridViewTextBoxColumn
            Dim cboAccounts As New DataGridViewComboBoxColumn
            Dim txtMemo As New DataGridViewTextBoxColumn
            Dim cboCustomer As New DataGridViewComboBoxColumn
            Dim txtDebit As New DataGridViewTextBoxColumn
            Dim txtCredit As New DataGridViewTextBoxColumn

            Dim dtsAccount As DataTable = m_displayAccountsDS.Tables(0)
            Dim dtsCustomer As DataTable = m_displaycustomerDS.Tables(0)
            Dim dtsSalesTax As DataTable = m_displaySalesTax.Tables(0)

            With cboAccounts
                .FlatStyle = FlatStyle.Flat
                .HeaderText = "Account Title"
                .DataPropertyName = "acnt_id"
                .Name = "cAccounts"
                .DataSource = dtsAccount
                .DisplayMember = "code_description"
                .ValueMember = "acnt_id"
                .DropDownWidth = 380
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True
            End With

            With cboCustomer
                .FlatStyle = FlatStyle.Flat
                .DropDownWidth = 200
                .HeaderText = "Subsidiary"
                .DataPropertyName = "fxKeyID"
                .Name = "cCustomer"
                .DataSource = dtsCustomer
                .DisplayMember = "Name"
                .ValueMember = "fxKeyID"
                .DropDownWidth = 300
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True
            End With

            With txtDebit
                .HeaderText = "Debit"
                .Name = "cDebit"
                .Width = 115
                .DefaultCellStyle.Format = "n2"
            End With

            With txtCredit
                .HeaderText = "Credit"
                .Name = "cCredit"
                .Width = 115
                .DefaultCellStyle.Format = "n2"
            End With

            With txtMemo
                .HeaderText = "Memo"
                .Name = "cMemo"
                .Width = 150
                .Visible = False
            End With

            With cKeyExpense
                .Name = "cKeyExpense"
            End With

            .Columns.Add(cKeyExpense)
            .Columns.Add(cboAccounts)
            .Columns.Add(txtDebit)
            .Columns.Add(txtCredit)
            .Columns.Add(txtMemo)
            .Columns.Add(cboCustomer)

            .Columns(0).Visible = False
            .Columns(1).Width = 250
            .Columns(2).Width = 100
            .Columns(3).Width = 100
            .Columns(4).Width = 150
            .Columns(5).Width = 180
        End With
    End Sub
    Private Sub CreateDataGridView_Items()

        Dim dtItem As New DataTable

        If gCompanyID() <> "aed74f4e-c5bb-4b48-9a0c-ef9210762948" Then
            dtItem = m_GetItem.Tables(0)
        ElseIf gCompanyID() = "aed74f4e-c5bb-4b48-9a0c-ef9210762948" Then
            dtItem = m_GetItemMossimo.Tables(0)
        End If

        With grdItems
            .Columns.Clear()
            Dim cboItems As New DataGridViewComboBoxColumn
            Dim txtdescription As New DataGridViewTextBoxColumn
            Dim txtquantity As New DataGridViewTextBoxColumn
            Dim txtunit As New DataGridViewTextBoxColumn
            Dim txtcost As New DataGridViewTextBoxColumn
            Dim txtamount As New DataGridViewTextBoxColumn
            Dim cboCustomer As New DataGridViewComboBoxColumn
            Dim chkBillable As New DataGridViewCheckBoxColumn
            Dim txtponum As New DataGridViewTextBoxColumn

            ' Dim dtItem As DataTable = m_GetItem.Tables(0)
            Dim dtCustomer As DataTable = m_GetCustomer.Tables(0)

            With cboItems
                .Items.Clear()
                .HeaderText = "Item"
                .DataPropertyName = "fcItemName"
                .Name = "cItem"
                .DataSource = dtItem.DefaultView
                .DisplayMember = "fcItemName"
                .ValueMember = "fcItemName"
                .DropDownWidth = 200
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True
            End With

            With txtdescription
                .HeaderText = "Description"
                .Name = "cDescriptions"
                .Width = 100
            End With

            With txtquantity
                .HeaderText = "Quantity"
                .Name = "cQuantity"
                .Width = 80
            End With

            With txtunit
                .HeaderText = "U/M"
                .Name = "cUnit"
                .Width = 30
            End With

            With txtcost
                .HeaderText = "Cost"
                .Name = "cCost"
                .Width = 100
                .DefaultCellStyle.Format = "###,###,##0.00"
            End With

            With txtamount
                .HeaderText = "Amount"
                .Name = "cAmount"
                .Width = 90
                .DefaultCellStyle.Format = "###,###,##0.00"
            End With

            With cboCustomer
                .Items.Clear()
                .HeaderText = "Customer"
                .DataPropertyName = "Name"
                .Name = "cCustomer"
                .DataSource = dtCustomer.DefaultView
                .DisplayMember = "Name"
                .ValueMember = "Name"
                .DropDownWidth = 200
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True
            End With

            With chkBillable
                .FlatStyle = FlatStyle.Standard
                .HeaderText = "Billable"
                .Name = "cBillable"
                .Width = 50
            End With

            With txtponum
                .HeaderText = "P.O.#"
                .Name = "cPOnum"
                .Width = 50
            End With

            .AllowUserToAddRows = True
            .Columns.Add(cboItems)
            .Columns.Add(txtdescription)
            .Columns.Add(txtunit)
            .Columns.Add(txtquantity)
            .Columns.Add(txtcost)
            .Columns.Add(txtamount)
            .Columns.Add(cboCustomer)
            .Columns.Add(chkBillable)
            .Columns.Add(txtponum)

        End With
    End Sub
    Private Sub updateItem(ByVal sKey As String, ByVal nPrice As Long)
        Dim sSQLCmd As String = "UPDATE mItem00Master "
        sSQLCmd &= "SET fdPrice = " & nPrice
        sSQLCmd &= " WHERE fxKeyItem = '" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Item Price")
        End Try
    End Sub
    Private Sub gCalculateExpense()
        Try
            Dim xVal As Decimal = 0
            For Each xRow As DataGridViewRow In grdExpenses.Rows
                xVal += grdExpenses.Item("cDebit", xRow.Index).Value
                xVal -= grdExpenses.Item("cCredit", xRow.Index).Value
            Next
            ExpenseAmount = Format(CDec(xVal), "##,##0.00")
            tb_Expenses.Text = "Expenses Php" + Format(CDec(xVal), "##,##0.00")
            Call gCheckAmount()
        Catch ex As Exception
        End Try
    End Sub


    Private Sub gCalculateItem()
        If sKeyClear <> 1 Then
            Dim xRow As Integer
            Dim xVal As Double = 0.0
            With grdItems
                If .Item(0, .CurrentRow.Index).Value IsNot Nothing Then
                    If Not IsDBNull(.Item(0, .CurrentRow.Index).Value) Then
                        If Not .Item(3, .CurrentRow.Index).Value = Nothing Or .Item(3, .CurrentRow.Index).Value <> 0 Then
                            If .Item(4, .CurrentRow.Index).Value IsNot Nothing Then
                                .Item(4, .CurrentRow.Index).Value = Convert.ToDecimal(.Item(4, .CurrentRow.Index).Value)
                                .Item(.Columns("cAmount").Index, .CurrentRow.Index).Value = Convert.ToDecimal(Convert.ToDecimal(.Item(.Columns("cCost").Index, .CurrentRow.Index).Value) * Convert.ToDecimal(.Item(.Columns("cQuantity").Index, .CurrentRow.Index).Value))
                            Else
                                .Item(.Columns("cAmount").Index, .CurrentRow.Index).Value = "0.00"
                            End If
                        ElseIf Not .Item(3, .CurrentRow.Index).Value = Nothing Or .Item(3, .CurrentRow.Index).Value = "0" Or Not IsDBNull(.Item(3, .CurrentRow.Index).Value) Then
                            If Not .Item(4, .CurrentRow.Index).Value = Nothing Or .Item(4, .CurrentRow.Index).Value = "0" Or Not IsDBNull(.Item(4, .CurrentRow.Index).Value) Then
                                .Item(.Columns("cAmount").Index, .CurrentRow.Index).Value = "0.00"
                            Else
                                .Item(4, .CurrentRow.Index).Value = Format(CDec(.Item(4, .CurrentRow.Index).Value), "##,##0.00")
                                .Item(5, .CurrentRow.Index).Value = Format(CDec(CDec(.Item(4, .CurrentRow.Index).Value) * 1), "##,##0.00")
                            End If
                        End If
                        For xRow = 0 To .Rows.Count - 1
                            xVal += .Item(5, xRow).Value
                        Next
                    End If
                End If
            End With
            ItemAmount = Format(CDec(xVal), "##,##0.00")
            tb_Items.Text = "Items" + "     " + "P " + Format(CDec(xVal), "##,##0.00")
            Call gCheckAmount()
            sKeyClear = 0
        End If
    End Sub
    Private Sub gCheckAmount()
        txtAmtDue.Text = Format(CDec(ExpenseAmount + ItemAmount), "##,##0.00")
    End Sub
    Private Sub gRbtnEvents()
        lblDiscDate.Text = ""
        lblDateDisc.Text = ""
        lblDiscDate.Visible = False
        lblDateDisc.Visible = False
        If rbtnBill.Checked = True Then
            lblbill.Text = "Bill(s)"
            lblamtdue.Text = "Amt.Due"
            lbladd.Visible = True
            txtAddress.Visible = True
            lblReceiptItem.Visible = False
            txtMemo.Text = ""
            dteBillDue.Visible = True
            lblBilldue.Visible = True
            'cboTerms_Multicombo.Visible = True
            'lblTerms.Visible = True
        ElseIf rbtnCredit.Checked = True Then
            lblbill.Text = "Credit"
            lblamtdue.Text = "Credit Amount"
            lbladd.Visible = False
            txtAddress.Visible = False
            lblReceiptItem.Visible = False
            txtMemo.Text = ""
            dteBillDue.Visible = False
            lblBilldue.Visible = False
            cboTerms_Multicombo.Visible = False
            lblTerms.Visible = False
        End If
    End Sub
    Private Sub panelSettings()
        lblDiscDate.Text = ""
        lblDateDisc.Text = ""
        lblDiscDate.Visible = False
        lblDateDisc.Visible = False
        If chkBillRcv.Checked = True Then
            rbtnBill.Enabled = True
            rbtnCredit.Enabled = True
            rbtnBill.Checked = True
            rbtnCredit.Checked = False
            lblReceiptItem.Visible = False
            lbladd.Visible = True
            txtAddress.Visible = True
            lblamtdue.Text = "Amt.Due"
            lblBilldue.Visible = True
            dteBillDue.Visible = True
            'lblTerms.Visible = True
            'cboTerms_Multicombo.Visible = True
            txtMemo.Text = ""
        ElseIf chkBillRcv.Checked = False Then
            rbtnBill.Enabled = False
            rbtnCredit.Enabled = False
            rbtnBill.Checked = True
            rbtnCredit.Checked = False
            lblReceiptItem.Visible = True
            lbladd.Visible = False
            txtAddress.Visible = False
            lblamtdue.Text = "Total"
            lblBilldue.Visible = False
            dteBillDue.Visible = False
            lblTerms.Visible = False
            cboTerms_Multicombo.Visible = False
            txtMemo.Text = "Received items (bill to follow)"
        End If
    End Sub
    Private Function pEvent() As Boolean
        'Dim psbol As Boolean = False
        'Dim xAPAcnt As String = "NULL"
        'Try
        '    Dim xBill, xReceived As Integer
        '    If rbtnBill.Checked = True Then
        '        xBill = 1
        '    Else
        '        xBill = 0
        '    End If

        '    If chkBillRcv.Checked = True Then
        '        xReceived = 1
        '    Else
        '        xReceived = 0
        '    End If

        '    If MtcboAPAcnt.SelectedItem IsNot Nothing Then
        '        xAPAcnt = "'" & MtcboAPAcnt.SelectedItem.Col2 & "'"
        '    End If
        '    'If SaveBill(sKeyBill, m_GetNameID(cboSupplier.SelectedItem, " Supplier"), _
        '    '                         m_GetTermsID(cboTerms.SelectedItem), txtMemo.Text, _
        '    '                         Microsoft.VisualBasic.FormatDateTime(gDate, DateFormat.ShortDate), _
        '    '                         Format(CDec(txtAmt.Text), "##,##0.00"), _
        '    '                         Microsoft.VisualBasic.FormatDateTime(gDateBillDue, DateFormat.ShortDate), _
        '    '                         txtRefNo.Text, xReceived, xBill, gDiscDate, _Type, "", txtLoanNo.Text, xAPAcnt) = "Save Successful...." Then
        '    psbol = True
        '    Else
        '    psbol = False
        '    End If
        '    Return psbol
        'Catch ex As Exception
        '    psbol = False
        '    Return psbol
        'End Try
    End Function
    Private Sub Save_to_dumpfile_Collected_Bills(ByVal fxkeybillexpenses As String)
        Dim xfxkeybillexpenses As String = ""
        Dim xfxKeyBill As String = ""
        Dim xfxKeyCompany As String = ""
        Dim xacnt_id As String = ""
        Dim xfdDebit As String = ""
        Dim xfdCredit As String = ""
        Dim xfcMemo As String = ""
        Dim xfxKeyCustomer As String = ""
        Dim xfuCreatedby As String = ""
        Dim xfdDateCreated As String = ""
        Dim xfuUpdatedby As String = ""
        Dim xfdDateUpdated As String = ""
        Dim xfdCount As String = ""
        Dim xfdDeletedBy As String = ""
        Dim sSqlCmd As String = "SELECT * FROM dbo.tBillsExpenses WHERE fxKeyBillExpenses ='" & fxkeybillexpenses & "' " & _
                                             "AND fxKeyBill ='" & sKeyBill & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xfxkeybillexpenses = rd.Item(0).ToString
                    xfxKeyBill = rd.Item(1).ToString
                    xfxKeyCompany = rd.Item(2).ToString
                    xacnt_id = rd.Item(3).ToString
                    xfdDebit = rd.Item(4).ToString
                    xfdCredit = rd.Item(5).ToString
                    xfcMemo = rd.Item(6).ToString
                    xfxKeyCustomer = rd.Item(7).ToString
                    xfuCreatedby = rd.Item(8).ToString
                    xfdDateCreated = rd.Item(9).ToString
                    xfuUpdatedby = rd.Item(10).ToString
                    xfdDateUpdated = rd.Item(11).ToString
                    xfdCount = rd.Item(12).ToString
                    xfdDeletedBy = gUserName
                End While
            End Using
        Catch ex As Exception
        End Try

        Dim sSqlSave As String = "INSERT INTO dbo.tBillsExpensesDeleted "
        sSqlSave &= "(fxKeyBillExpenses, fxKeyBill, fxKeyCompany, acnt_id, fdDebit, fdCredit, fcMemo, fxKeyCustomer, fuCreatedby, fdDateCreated, "
        sSqlSave &= "fuUpdatedby, fdDateUpdated, fdCount, fdDeletedBy) "
        sSqlSave &= "VALUES ('" & xfxkeybillexpenses & "', '" & xfxKeyBill & "', '" & xfxKeyCompany & "', '" & xacnt_id & "', '" & xfdDebit & _
                    "', '" & xfdCredit & "', '" & xfcMemo & "', '" & xfxKeyCustomer & "', '" & xfuCreatedby & "', '" & _
                      xfdDateCreated & "', '" & xfuUpdatedby & "', '" & xfdDateUpdated & "', '" & xfdCount & "', '" & xfdDeletedBy & "')"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlSave)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Delete_Collected_Bills(ByVal fKeyBillsExpenses As String)
        Dim sSqlDeleteCmd As String = "DELETE FROM dbo.tBillsExpenses WHERE fxKeyBillExpenses ='" & fKeyBillsExpenses & "' " & _
                                    "AND fxKeyBill ='" & sKeyBill & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlDeleteCmd)
        Catch ex As Exception
        End Try
    End Sub
    Private Function pSave_gExpense() As Boolean
        Dim psbol As Boolean = False
        Try

            'Dim sAccount() As String
            'Dim sNames() As String
            With Me.grdExpenses
                Dim xRow As Integer
                'MsgBox(.Item(1, xRow).Value)
                For xRow = 0 To .Rows.Count - 1
                    If .Item(1, xRow).Value.ToString() <> "" Then
                        Dim sAcnt As String = ""
                        Dim sMemo As String = ""
                        Dim sDebit As Decimal = 0
                        Dim sCredit As Decimal = 0
                        Dim sName As String = ""
                        Dim bBillable As Boolean = False
                        Dim sKeyExpense As String = ""

                        If gTrans.gmkDefaultValues(xRow, 0, grdExpenses) <> Nothing Or gTrans.gmkDefaultValues(xRow, 0, grdExpenses) = "" Then
                            sKeyExpense = .Rows(xRow).Cells("cKeyExpense").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 1, grdExpenses) <> Nothing Then
                            sAcnt = .Rows(xRow).Cells("cAccounts").Value.ToString()
                        End If
                        If gTrans.gmkDefaultValues(xRow, 2, grdExpenses) <> Nothing Then
                            sDebit = .Rows(xRow).Cells("cDebit").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 3, grdExpenses) <> Nothing Then
                            sCredit = .Rows(xRow).Cells("cCredit").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 4, grdExpenses) <> Nothing Then
                            sMemo = .Rows(xRow).Cells("cMemo").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 5, grdExpenses) <> Nothing Then
                            sName = .Rows(xRow).Cells("cCustomer").Value.ToString()


                        End If

                        For Each CollectedBills As ListViewItem In ListDelete.Items
                            Save_to_dumpfile_Collected_Bills(CollectedBills.Text) '<=======save to dump file before deletion
                            Delete_Collected_Bills(CollectedBills.Text) '<==================delete Expenses
                        Next

                        ListDelete.Items.Clear()

                        gTrans.gSaveBillExpenses(sKeyBill, sKeyExpense, sAcnt, sDebit, sCredit, sMemo, sName, Me)
                    End If
                Next
            End With

            psbol = True
            Return psbol
        Catch ex As Exception
            psbol = False
            Return psbol
        End Try
    End Function
    Private Function pSave_gItems() As Boolean
        Dim psbol As Boolean = False
        Try
            If tb_Items.Text <> "Items     P 1.00" Then
                Dim sItems() As String
                Dim sNames() As String
                Dim x As Integer
                With Me.grdItems
                    Dim xRow As Integer
                    For xRow = 0 To .Rows.Count - 1
                        If .Item(0, xRow).Value <> "" Then
                            Dim sItem As String = ""
                            Dim sDescription As String = ""
                            Dim sQty As String = ""
                            Dim sUnit As String = ""
                            Dim sCost As Long = 0
                            Dim sAmount As Long = 0
                            Dim sName As String = ""
                            Dim bBillable As Boolean = False
                            Dim sPONumber As String = ""
                            Dim sPOID As String = ""

                            If gTrans.gmkDefaultValues(xRow, 0, grdItems) <> Nothing Or gTrans.gmkDefaultValues(xRow, 0, grdItems) = "" Then
                                sItem = .Rows(xRow).Cells("cItems").Value
                                sItems = Split(sItem, "-")
                                sItem = m_GetItemByName(sItems(0))
                            End If
                            If gTrans.gmkDefaultValues(xRow, 1, grdItems) <> Nothing Then
                                sDescription = .Rows(xRow).Cells("cDescriptions").Value
                            End If
                            If gTrans.gmkDefaultValues(xRow, 2, grdItems) <> Nothing Then
                                sQty = .Rows(xRow).Cells("cQuantity").Value
                            End If
                            If gTrans.gmkDefaultValues(xRow, 3, grdItems) <> Nothing Then
                                sUnit = .Rows(xRow).Cells("cUnit").Value.ToString
                            End If
                            If gTrans.gmkDefaultValues(xRow, 4, grdItems) <> Nothing Then
                                sCost = .Rows(xRow).Cells("cCost").Value
                            End If
                            If gTrans.gmkDefaultValues(xRow, 5, grdItems) <> Nothing Then
                                sAmount = .Rows(xRow).Cells("cAmount").Value
                            End If
                            If gTrans.gmkDefaultValues(xRow, 6, grdItems) <> Nothing Then
                                sName = .Rows(xRow).Cells("cCustomer").Value
                                sNames = Split(sName, "-")
                                sName = m_GetNameID(sNames(0), sNames(1))
                            End If
                            If gTrans.gmkDefaultValues(xRow, 7, grdItems) <> Nothing Then
                                bBillable = .Rows(xRow).Cells("cBillable").Value
                                If bBillable = False Then
                                    x = 0
                                Else
                                    x = 1
                                End If
                            End If
                            If gTrans.gmkDefaultValues(xRow, 8, grdItems) <> Nothing Then
                                sPONumber = .Rows(xRow).Cells("cPOnum").Value
                                sPOID = m_GetPO_byPONumber(sPONumber)
                            End If
                            Call gTrans.gSaveBillItems(sKeyBill, sItem, sDescription, _
                                                        sUnit, sQty, sCost, sAmount, sName, _
                                                        x, sPOID, Me)
                        End If
                    Next
                End With
            End If
            psbol = True
            Return psbol
        Catch ex As Exception
            psbol = False
            Return psbol
        End Try
    End Function
    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdItems.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdItems.Rows(irow).Cells(iCol).Value)
    End Function
    Private Sub LoadBillDetails()
        Call LoadBillHeader()
        Call CreateDataGridView_Items()
        Call CreateDataGridview_ExpensesTab()

        Call LoadTabData_Items()
        Call LoadTabData_Expenses()
    End Sub
    Private Sub LoadTabData_Items()
        Dim recCount As Integer = 0
        Dim sSQLCmd As String
        Dim row As Integer = 0
        If sKeyBill <> "" Or sKeyBill <> Nothing Then
            If m_billitemvalidation(sKeyBill, Me) <> 0 Then
                sSQLCmd = "usp_t_billItems_List @fxKeyBill='" & sKeyBill & "'"
                sKeyClear = 1
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                        While rd.Read
                            recCount += 1
                        End While
                    End Using


                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                        grdItems.Rows.Add(recCount - 1)
                        While rd.Read
                            With grdItems
                                '  .Item("cItems", row).Value = IIf(IsDBNull(rd.Item("fcItemName")), " ", rd.Item("fcItemName") & " - Item")
                                .Item("cCustomer", row).Value = IIf(IsDBNull(rd.Item("cCustomer")), " ", rd.Item("cCustomer") & " - Customer")
                                .Item("cDescriptions", row).Value = rd.Item("cDescriptions")
                                .Item("cQuantity", row).Value = rd.Item("cQuantity")
                                .Item("cUnit", row).Value = rd.Item("cUnit")
                                .Item("cCost", row).Value = rd.Item("cCost")
                                .Item("cAmount", row).Value = rd.Item("cAmount")
                                .Item("cBillable", row).Value = rd.Item("cBillable")
                                .Item("cPONum", row).Value = rd.Item("cPONum")
                            End With
                            row += 1
                        End While
                    End Using

                    sKeyClear = 0
                Catch ex As Exception
                    ' MessageBox.Show(Err.Description, "CLICKSOFTWARE:ListItem Receipts")
                End Try
            End If
        End If

    End Sub
    Private Sub LoadTabData_Expenses()
        Dim sSQLCmd As String
        Dim row As Integer = 0
        If sKeyBill <> "" Or sKeyBill <> Nothing Then
            If m_billexpensevalidation(sKeyBill, Me) <> 0 Then
                sSQLCmd = "usp_t_billExpenses_List @fxKeyBill='" & sKeyBill & "'"
                sKeyClear = 1
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                        Dim xRow As Integer = 0
                        While rd.Read
                            With grdExpenses
                                .Rows.Add()
                                .Item("cKeyExpense", xRow).Value = rd.Item("cKeyExpense").ToString
                                .Item("cDebit", xRow).Value = rd.Item("cDebit")
                                .Item("cCredit", xRow).Value = rd.Item("cCredit")
                                .Item("cMemo", xRow).Value = rd.Item("cMemo").ToString
                                .Item("cAccounts", xRow).Value = rd.Item("cAccount")
                                .Item("cCustomer", xRow).Value = rd.Item("cCustomer")
                            End With
                            xRow += 1
                        End While
                    End Using
                    sKeyClear = 0
                Catch ex As Exception
                    MessageBox.Show(Err.Description, "CLICKSOFTWARE:List Expenses")
                End Try
            End If
        End If
    End Sub
    Private Sub pDebit()
        Try
            With Me.grdExpenses
                Dim xDebit As Double = 0.0
                Dim xRow As Integer
                For xRow = 0 To .RowCount - 1
                    If .Rows(xRow).Cells("cDebit").Value IsNot Nothing Then
                        xDebit += Format(CDec(.Rows(xRow).Cells("cDebit").Value), "##,##0.00")
                        .Rows(xRow).Cells("cDebit").Value = Format(CDec(.Rows(xRow).Cells("cDebit").Value), "##,##0.00")
                    Else
                        xDebit += 0
                    End If
                Next
                txtdebittotal.Text = Format(CDec(xDebit), "##,##0.00")
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub pCredit()
        Try
            With Me.grdExpenses
                Dim xCredit As Double = 0.0
                Dim xRow As Integer
                For xRow = 0 To .RowCount - 1
                    If .Rows(xRow).Cells("cCredit").Value IsNot Nothing Then
                        xCredit += Format(CDec(.Rows(xRow).Cells("cCredit").Value), "##,##0.00")
                        .Rows(xRow).Cells("cCredit").Value = Format(CDec(.Rows(xRow).Cells("cCredit").Value), "##,##0.00")
                    Else
                        xCredit += 0
                    End If
                Next
                txtCredittotal.Text = Format(CDec(xCredit), "##,##0.00")
            End With
        Catch ex As Exception
        End Try
    End Sub

    Public Function GenerateAPVNo() As String
        Dim apvNo As String
        Dim random As String = Guid.NewGuid.ToString

        apvNo = "APV-" & DateTime.Now.Year & "-" & DateTime.Now.Month & "-" & random.Substring(random.Length - 6, 6)

        Return apvNo
    End Function


#End Region

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmviewallbills.Show()
        Me.Close()
    End Sub
    Private Sub chkRecurring_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRecurring.CheckedChanged
        If chkRecurring.Checked = True Then
            frmRecurringEntries.Show()
        End If
    End Sub
#Region "Trush"
    Private Sub psdailysettings()
        'Dim xVal As Boolean
        'Dim nday As Integer = DateDiff(DateInterval.Day, gRecurringStartDate, gRecurringEndDate)
        'Dim xCnt As Integer
        'For xCnt = 0 To nday
        '    gDate = DateAdd(DateInterval.Day, xCnt, gRecurringStartDate)
        '    If cboTerms.SelectedIndex > 1 Then
        '        Call gTrans.getTermsDetailsrecurring(getTermID(cboTerms.SelectedItem))
        '    End If
        '    xVal = psSave()
        'Next
        'If xVal = True Then
        '    MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
        'End If
    End Sub
    Private Sub psweeklysettings()
        'Dim xVal As Boolean
        'Dim nweek As Integer = DateDiff(DateInterval.Weekday, gRecurringStartDate, gRecurringEndDate)
        'Dim xCnt As Integer
        'For xCnt = 0 To nweek - 1
        '    If cboTerms.SelectedIndex > 2 Then
        '        Call gTrans.getTermsDetailsrecurring(getTermID(cboTerms.SelectedItem))
        '    End If
        '    xVal = psSave()
        'Next
        'If xVal = True Then
        '    MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
        'End If
    End Sub
    Private Sub psmonthlysettings()
        'Dim xVal As Boolean
        'Dim nmnth As Integer = DateDiff(DateInterval.Month, gRecurringStartDate, gRecurringEndDate)
        'Dim xCnt As Integer
        'For xCnt = 0 To nmnth - 1
        '    If cboTerms.SelectedIndex > 2 Then
        '        Call gTrans.getTermsDetailsrecurring(getTermID(cboTerms.SelectedItem))
        '    End If
        '    xVal = psSave()
        'Next
        'If xVal = True Then
        '    MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
        'End If
    End Sub
    Private Sub psquarterlysettings()
        'Dim xVal As Boolean
        'Dim nQuarter As Integer = DateDiff(DateInterval.Quarter, gRecurringStartDate, gRecurringEndDate)
        'Dim xCnt As Integer
        'For xCnt = 0 To nQuarter - 1
        '    If cboTerms.SelectedIndex > 2 Then
        '        Call gTrans.getTermsDetailsrecurring(getTermID(cboTerms.SelectedItem))
        '    End If
        '    xVal = psSave()
        'Next
        'If xVal = True Then
        '    MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
        'End If
    End Sub
    Private Sub psyearlysettings()
        'Dim xVal As Boolean
        'Dim nyearly As Integer = DateDiff(DateInterval.Year, gRecurringStartDate, gRecurringEndDate)
        'Dim xCnt As Integer
        'For xCnt = 0 To nyearly - 1
        '    If cboTerms.SelectedIndex > 2 Then
        '        Call gTrans.getTermsDetailsrecurring(getTermID(cboTerms.SelectedItem))
        '    End If
        '    xVal = psSave()
        'Next
        'If xVal = True Then
        '    MsgBox("save successful...", MsgBoxStyle.Information, Me.Text)
        'End If
    End Sub
#End Region
    Private Function psSave() As Boolean
        Dim saved As Boolean
        Try
            If sKeyBill <> "" Then
                If pEvent() = True And pSave_gExpense() = True And pSave_gItems() = True Then
                    saved = True
                ElseIf pEvent() = False And pSave_gExpense() = True And pSave_gItems() = True Then
                    saved = True
                ElseIf pEvent() = True And pSave_gExpense() = False And pSave_gItems() = True Then
                    saved = True
                ElseIf pEvent() = True And pSave_gExpense() = True And pSave_gItems() = False Then
                    saved = True
                End If
            Else
                sKeyBill = Guid.NewGuid.ToString
                KeyBill() = sKeyBill
                If pEvent() = True And pSave_gExpense() = True And pSave_gItems() = True Then
                    saved = True
                ElseIf pEvent() = False And pSave_gExpense() = True And pSave_gItems() = True Then
                    saved = True
                ElseIf pEvent() = True And pSave_gExpense() = False And pSave_gItems() = True Then
                    saved = True
                ElseIf pEvent() = True And pSave_gExpense() = True And pSave_gItems() = False Then
                    saved = True
                End If
            End If

            Return saved
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dteTransDate.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
        End If
    End Sub
    Private Sub dteTransDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteTransDate.ValueChanged
        IsDateRestricted()
    End Sub
    Private Sub grdExpenses_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdExpenses.CellValueChanged
        gCalculateExpense()
    End Sub
    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        txtClassName.Text = FrmBrowseClass.Data1
        txtClassRefNo.Text = FrmBrowseClass.Data2
        ClassRefno = FrmBrowseClass.Data2
    End Sub
    Private Sub BtnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
        If grdExpenses.SelectedRows.Count <> 0 Then
            Dim xRow As Integer = 0
            For Each SelectedRow As DataGridViewRow In grdExpenses.SelectedRows
                xRow = SelectedRow.Index
                If grdExpenses.Item(0, xRow).Value <> "" Or grdExpenses.Item(0, xRow).Value <> Nothing Then
                    ListDelete.Items.Add(grdExpenses.Item(0, xRow).Value)
                End If
                grdExpenses.Rows.Remove(grdExpenses.Rows(xRow))
            Next
            MsgBox("The data that you've deleted was not totally deleted unless you save the changes.", _
                    MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Notice of deletion")
        Else
            MsgBox("Please, select the bill/s you want to delete.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        End If
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim sSqlCmd As String = "Select * from dbo.tBills where fxKeyBill = '" & KeyBill() & "' and fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    Dim xForm As New frmAccountsPayableVoucher
                    xForm.GetbillID() = sKeyBill
                    xForm.MdiParent = frmMain
                    xForm.Show()
                Else
                    MsgBox("User Error! Please save the Bill First.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Access denied")
                End If
            End Using
        Catch ex As Exception
            ShowErrorMessage("btnPreview_Click", Me.Name, ex.Message)
        End Try
    End Sub
    Private Sub btnSaveOnly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveOnly.Click
        If frmClosingEntries.IsPeriodClosed(dteTransDate.Value.Date, btnSaveOnly) = False Then
            cboTerms_Multicombo.Text = "NET30"
            SaveBills("SaveOnly")
        End If
    End Sub
    Private Function IsSpecialTransaction(ByVal transaction As String) As Boolean
        Dim specialTransaction As Boolean = False

        Select Case transaction
            Case "Loan"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "Validation_IsBillALoanTransaction", _
                            New SqlParameter("@billID", KeyBill()))
                        If rd.HasRows Then
                            specialTransaction = True
                        Else
                            specialTransaction = False
                        End If
                    End Using
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case Else
                specialTransaction = False
        End Select

        Return specialTransaction
    End Function
    Private Sub toolStripViewPaymentHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStripViewPaymentHistory.Click
        If KeyBill() IsNot Nothing Then
            frmBillsPaymentHistory.GetBillID() = KeyBill()
            frmBillsPaymentHistory.ShowDialog()
        Else
            MessageBox.Show("User Error! You should save first before proceeding.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub cboSupplierMultiCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMember.SelectedIndexChanged
        If cboMember.SelectedIndex = 1 Then
            frm_vend_masterVendorAddEdit.KeySupplier = Guid.NewGuid.ToString
            frm_vend_masterVendorAddEdit.KeyFormOrigin = "frm_Vend_EnterBills"
            frm_vend_masterVendorAddEdit.Show()
            cboMember.SelectedIndex = 0
        ElseIf cboMember.SelectedIndex = 2 Then
            cboMember.SelectedIndex = 0
        ElseIf cboMember.SelectedIndex > 2 Then
            Dim APAccount As String = ""
            GetSupplierID() = cboMember.SelectedValue.ToString()
            txtAddress.Text = m_getSpecificAddress(GetSupplierID())
            'cboTerms_Multicombo.Text = m_getTermsss(GetSupplierID())
            APAccount = GetAPAcntOfSupplier(supplierID)

            If APAccount <> "" Then
                MtcboAPAcnt.SelectedValue = APAccount
            Else
                LoadDefinedAcntsToMtcbo(MtcboAPAcnt, "Accounts Payable")
                MtcboAPAcnt.Text = ""
                xfxKeyApAcnt = ""
            End If
        End If
    End Sub
    Private Function GetAPAcntOfSupplier(ByVal xfxKeySupplier As String) As String
        Dim sSqlCmd As String = "select t2.acnt_desc " & _
                                "from dbo.mSupplier00Master t1 " & _
                                "inner join dbo.mAccounts t2 " & _
                                "on t1.fxKeyAPAccnt = t2.acnt_id " & _
                                "where fxKeySupplier = '" & xfxKeySupplier & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            ShowErrorMessage("GetAPAcntOfSupplier", Me.Name, ex.Message)
        End Try
    End Function
    Private Sub cboTerms_Multicombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTerms_Multicombo.SelectedIndexChanged

        lblDiscDate.Text = ""
        lblDateDisc.Text = ""
        lblDiscDate.Visible = False
        lblDateDisc.Visible = False
        If cboTerms_Multicombo.SelectedIndex = 1 Then
            frm_MF_termsAddEdit.Keyterms = Guid.NewGuid.ToString
            frm_MF_termsAddEdit.MdiParent = Me.MdiParent
            frm_MF_termsAddEdit.KeyFormOrigin = "frm_vend_EnterBills"
            frm_MF_termsAddEdit.Text = "Add Terms"
            frm_MF_termsAddEdit.Show()
            cboTerms_Multicombo.SelectedIndex = 0
        ElseIf cboTerms_Multicombo.SelectedIndex = 2 Then
            cboTerms_Multicombo.SelectedIndex = 0
        ElseIf cboTerms_Multicombo.SelectedIndex > 2 Then
            'Computes Due Date
            gTrans.getTermsDetails(cboTerms_Multicombo.SelectedItem.Col2, _
                                      lblDiscDate, lblDateDisc, _
                                      Microsoft.VisualBasic.FormatDateTime(dteTransDate.Value, DateFormat.ShortDate), _
                                      dteBillDue, Me, cboTerms_Multicombo.SelectedItem.Col1)
            GetTerms_ID() = cboTerms_Multicombo.SelectedItem.Col2
        End If
    End Sub
    Private Sub MtcboAPAcnt_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MtcboAPAcnt.SelectedIndexChanged
        Try
            GetAccountsPayable() = MtcboAPAcnt.SelectedItem.Col2
        Catch ex As Exception

        End Try
    End Sub
    Private Sub grdExpenses_RowsAdded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles grdExpenses.RowsAdded
        If grdExpenses.Item("cKeyExpense", grdExpenses.NewRowIndex).Value = "" Then
            grdExpenses.Item("cKeyExpense", grdExpenses.NewRowIndex).Value = System.Guid.NewGuid.ToString()
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPO.Click
        Dim xForm As New frm_vend_OpenPO
        xForm.ShowDialog()
        calculateAPandItems()
    End Sub
    Private Sub calculateAPandItems()
        Dim sSqlCmd As String
        Dim xfxKeyAccount As String = ""
        Dim xTotalExpensesAmt As Decimal = 0
        Dim xTotalAPAmt As Decimal = 0
        Dim xTotalItemAmt As Decimal = 0
        Dim xAPDebit As Decimal = 0
        Dim xAPCredit As Decimal = 0
        Dim xDebit As Decimal = 0
        Dim xCredit As Decimal = 0
        Dim xRow As Integer = 0

        'getting total amounts of accounts payables in grdExpenses
        If grdExpenses.AllowUserToAddRows = True Then
            grdExpenses.AllowUserToAddRows = False
        End If
        For Each xItem As DataGridViewRow In grdExpenses.Rows

            xRow = xItem.Index

            xTotalExpensesAmt += IIf(grdExpenses.Item("cCredit", xRow).Value Is Nothing, 0, grdExpenses.Item("cCredit", xRow).Value)
            xDebit += IIf(grdExpenses.Item("cDebit", xRow).Value Is Nothing, 0, grdExpenses.Item("cDebit", xRow).Value)
            xCredit += IIf(grdExpenses.Item("cCredit", xRow).Value Is Nothing, 0, grdExpenses.Item("cCredit", xRow).Value)

            xfxKeyAccount = IIf(IsDBNull(grdExpenses.Item("cAccounts", xRow).Value) = True, "", grdExpenses.Item("cAccounts", xRow).Value.ToString)
            If xfxKeyAccount <> "" Then
                sSqlCmd = "select t2.acnt_type from dbo.mAccounts t1 " & _
                            "inner join dbo.mAccountType t2 on t1.acnt_type = t2.actType_id " & _
                            "where t1.acnt_id = '" & xfxKeyAccount & "'"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        While rd.Read
                            If rd.Item(0).ToString = "Account Payables" Then
                                xAPDebit += IIf(grdExpenses.Item("cDebit", xRow).Value Is Nothing, 0, grdExpenses.Item("cDebit", xRow).Value)
                                xAPCredit += IIf(grdExpenses.Item("cCredit", xRow).Value Is Nothing, 0, grdExpenses.Item("cCredit", xRow).Value)
                            End If
                        End While
                    End Using
                Catch ex As Exception
                    ShowErrorMessage("calculateAPandItems", Me.Name, ex.Message)
                End Try
            End If

        Next
        xTotalAPAmt = xAPDebit - xAPCredit
        txtdebittotal.Text = Format(CDec(xDebit), "##,##0.00")
        txtCredittotal.Text = Format(CDec(xCredit), "##,##0.00")
        tb_Expenses.Text = "Expenses Php " & Format(xTotalExpensesAmt, "##,##0.00")

        If grdExpenses.AllowUserToAddRows = False Then
            grdExpenses.AllowUserToAddRows = True
        End If

        'getting total amounts of items in grdItems
        For Each xItem2 As DataGridViewRow In grdItems.Rows
            xRow = xItem2.Index
            xTotalItemAmt += grdItems.Item("cAmount", xRow).Value
        Next
        tb_Items.Text = "Items Php " & Format(xTotalItemAmt, "##,##0.00")

        txtAmtDue.Text = Format(CDec(xTotalAPAmt + xTotalItemAmt), "##,##0.00")
    End Sub
    Public Sub loadPurchaseItem(ByVal KeyPO As String)
        If KeyPO <> "" Or KeyPO <> Nothing Then
            Dim sSQLCmd As String = "usp_t_getPurchaseOrder_Items "
            sSQLCmd &= "@fxKeyPO='" & KeyPO & "'"

            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                    Dim xRow As Integer = grdItems.Rows.GetLastRow(DataGridViewElementStates.Visible)
                    While rd.Read
                        With grdItems
                            .Rows.Add()
                            .Item("cItem", xRow).Value = rd.Item(0)
                            .Item("cDescriptions", xRow).Value = rd.Item(1).ToString
                            .Item("cUnit", xRow).Value = rd.Item(2).ToString
                            .Item("cQuantity", xRow).Value = rd.Item(3).ToString
                            .Item("cCost", xRow).Value = Format(rd.Item(4), "##,##0.00")
                            .Item("cAmount", xRow).Value = Format(rd.Item(5), "##,##0.00")
                            .Item("cCustomer", xRow).Value = rd.Item(6).ToString
                            '.Item("cBillable", xRow).Value = rd.Item(7).ToString
                            .Item("cPOnum", xRow).Value = rd.Item(7).ToString
                            .Item("cPOItem", xRow).Value = rd.Item(8).ToString
                        End With
                        xRow += 1
                    End While
                End Using

            Catch ex As Exception
                MessageBox.Show(Err.Description, "Load Transactions")
            End Try
        End If

    End Sub

    Private Sub btnDocNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocNo.Click
        frmSelectDocNum1.ShowDialog()
    End Sub
End Class
