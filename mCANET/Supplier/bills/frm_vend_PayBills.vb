Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frm_vend_PayBills

    Private gTrans As New clsTransactionFunctions
    Private gcon As New Clsappconfiguration
    Dim checkDate As New clsRestrictedDate

    Private paybillID As String
    Public Property GetPaybillID() As String
        Get
            Return paybillID
        End Get
        Set(ByVal value As String)
            paybillID = value
        End Set
    End Property
    Private writeCheckID As String
    Public Property GetWriteCheckID() As String
        Get
            Return writeCheckID
        End Get
        Set(ByVal value As String)
            writeCheckID = value
        End Set
    End Property
    Private bankAccountID As String
    Public Property GetBankAccountID() As String
        Get
            Return bankAccountID
        End Get
        Set(ByVal value As String)
            bankAccountID = value
        End Set
    End Property
    Private bankAccountBalance As Decimal
    Public Property GetBankBalance() As Decimal
        Get
            Return bankAccountBalance
        End Get
        Set(ByVal value As Decimal)
            bankAccountBalance = value
        End Set
    End Property
    Private amountToPay As Decimal
    Public Property GetAmountToPay() As Decimal
        Get
            Return amountToPay
        End Get
        Set(ByVal value As Decimal)
            amountToPay = value
        End Set
    End Property
    Private selectedIndexBankAccount As Integer
    Public Property GetSelectedIndexBankAccount() As Integer
        Get
            Return selectedIndexBankAccount
        End Get
        Set(ByVal value As Integer)
            selectedIndexBankAccount = value
        End Set
    End Property
    Private user As String
    Public Property GetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property
    Private SupplierID As String
    Public Property getSupplierID() As String
        Get
            Return SupplierID
        End Get
        Set(ByVal value As String)
            SupplierID = value
        End Set
    End Property
    Private xSQLHelper As New NewSQLHelper

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub frm_vend_PayBills_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtePaymentDate.Value = Now
        dteDueBefore.Value = Now
        rbtnShwAll.Checked = True
        cboPaymentMethod.SelectedIndex = 0

        GetSpecifiedAcnt(cboBankAccount, "Bank Account")
        LoadBillList()
        ComputeEndingBalance()
        IsDateRestricted()
    End Sub
    Private Sub dteDueBefore_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteDueBefore.ValueChanged
        Try
            grdBills.Columns.Clear()
            Call LoadBillList(False)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub rbtnDue_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnDue.CheckedChanged
        Try
            If rbtnDue.Checked = True Then
                dteDueBefore.Enabled = True
                Call LoadBillList()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub rbtnShwAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnShwAll.CheckedChanged
        Try
            If rbtnShwAll.Checked = True Then
                dteDueBefore.Enabled = False
                grdBills.Columns.Clear()
                Call LoadBillList()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub grdBills_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdBills.CellValueChanged
        For Each xRow As DataGridViewRow In grdBills.Rows
            If xRow.Cells("Select").Value = True Then
                getSupplierID() = xRow.Cells("fxKeySupplier").Value.ToString()
                Exit For
            End If
        Next

        dtePaymentDate.Value = grdBills.CurrentRow.Cells("Due Date").Value.ToString()

        If e.ColumnIndex = 1 Or e.ColumnIndex = 2 Then
            UpdateAmountToPay()
            CheckIfMultipleCustomer()
        Else
            calculatetotals()
        End If

    End Sub
    Private Sub UpdateAmountToPay()
        Try
            Dim bSelected As Boolean = False
            With grdBills
                If .RowCount > 0 And .Item("Select", .CurrentRow.Index).Value <> False Then
                    .Rows(.CurrentRow.Index).Cells("cAmttoPay").Value = .Rows(.CurrentRow.Index).Cells("amt Due").Value

                    If gTrans.gmkDefaultValues(.CurrentRow.Index, 2, grdBills) <> Nothing Then
                        bSelected = .Rows(.CurrentRow.Index).Cells("Select").Value

                        If bSelected = False Then
                            grpDiscount.Enabled = False
                            btnPay.Enabled = False
                            btnSelectAll.Text = "Select All Bills"
                            Call cleartotalamount()
                        Else
                            grpDiscount.Enabled = True
                            btnPay.Enabled = True
                            btnSelectAll.Text = "Clear Selections"
                            Call calculatetotals()
                            Call getcurrentselectedbill()
                        End If
                    Else
                        grpDiscount.Enabled = False
                        btnPay.Enabled = False
                        btnSelectAll.Text = "Select All Bills"
                        Call cleartotalamount()
                    End If
                Else
                    .Rows(.CurrentRow.Index).Cells("cAmttoPay").Value = 0
                    Call calculatetotals()
                End If

            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click
        Try
            If btnSelectAll.Text = "Select All Bills" Then
                Call selectall()
            ElseIf btnSelectAll.Text = "Clear Selections" Then
                Call clearselection()
                Call ClearAmountToPay()
                Call cleartotalamount()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub ClearAmountToPay()
        Dim xCnt As Integer
        With grdBills
            For xCnt = 0 To .RowCount - 1
                .Rows(xCnt).Cells("cAmttoPay").Value = 0
            Next
        End With
    End Sub
    Private Sub btnSetDiscount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetDiscount.Click
        frmDiscountCredit.ShowDialog()
    End Sub
    Private Sub btnSetCredits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetCredits.Click
        frmDiscountCredit.ShowDialog()
    End Sub
    Private Sub btnPay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPay.Click
        If frmClosingEntries.IsPeriodClosed(dtePaymentDate.Value.Date, btnPay) = False And getSupplierID() <> "" Then
            Try
                If IsCheckNoValid() Then
                    Call PaySelectedBills()
                    Call LoadBillList()
                    Call RecordCheckPayment()
                    Call ResetPayBill()
                    Call ComputeEndingBalance()
                    Call OpenCheckPayment()

                    MessageBox.Show("Payment of Billing Successful!", "Payment of Bill", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

#Region "Functions/Procedures"
    Private Sub CheckIfMultipleCustomer()
        Dim FirstSupplierKey As String = ""

        lblWarning.Visible = False
        btnPay.Enabled = True

        For Each xRow As DataGridViewRow In grdBills.Rows
            If xRow.Cells("Select").Value = True Then
                If FirstSupplierKey = "" Then
                    FirstSupplierKey = grdBills.Item("fxKeySupplier", xRow.Index).Value.ToString()
                Else
                    If FirstSupplierKey <> grdBills.Item("fxKeySupplier", xRow.Index).Value.ToString() Then
                        lblWarning.Visible = True
                        btnPay.Enabled = False
                        Exit For
                    End If
                End If
            End If
        Next

    End Sub
    Private Sub OpenCheckPayment()
        If chkCheckPrint.Checked = True Then
            With frm_acc_writeChecks_v2
                .GetWriteChecksID() = Me.GetWriteCheckID()
                .GetPayBillsID() = Me.GetPaybillID()
                .panelTop.Visible = False
                .ShowDialog()
            End With
        End If
    End Sub
    Private Sub RecordCheckPayment()
        If cboPaymentMethod.Text = "Check" Then
            GetWriteCheckID() = System.Guid.NewGuid.ToString()
            Dim storedProc As String = "usp_t_WriteChecks_Insert"
            Dim amountInWords As String
            Dim moneyConvertToWords As New WPMs
            Dim amountToPay As String = Format(GetAmountToPay(), "##,##0.00")
            amountInWords = moneyConvertToWords.gGenerateCurrencyInWords(amountToPay)
            Dim particulars As String = "Check " + txtCheckNo.Text + dtePaymentDate.Value.Date

            Try
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, storedProc, _
                        New SqlParameter("@pk_WriteCheck", GetWriteCheckID()), _
                        New SqlParameter("@compID", gCompanyID()), _
                        New SqlParameter("@PaybillsID", GetPaybillID()), _
                        New SqlParameter("@subdiaryID", getSupplierID()), _
                        New SqlParameter("@checkNo", txtCheckNo.Text), _
                        New SqlParameter("@checkDate", dtePaymentDate.Value.Date), _
                        New SqlParameter("@Amount", GetAmountToPay()), _
                        New SqlParameter("@AmountInWords", amountInWords), _
                        New SqlParameter("@particulars", particulars), _
                        New SqlParameter("@user", GetUser()))
            Catch ex As Exception
                Throw ex
            End Try
        End If

    End Sub
    Private Function IsCheckNoValid() As Boolean
        Dim isCheckNoIncluded As Boolean = True

        If cboPaymentMethod.Text = "Check" Then
            If txtCheckNo.Text.Length = 0 Then
                isCheckNoIncluded = False
                MessageBox.Show("Please enter the check No. first before proceeding.", "Pay Bills", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Else
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CAS_t_CheckIfCheckNoExist", _
                                New SqlParameter("@checkNo", txtCheckNo.Text.Trim))
                    If rd.HasRows = True Then
                        isCheckNoIncluded = False
                        MessageBox.Show("The Check No. is already use.", "Pay Bills", MessageBoxButtons.OK, MessageBoxIcon.Question)
                    End If
                End Using

            End If
        End If

        Return isCheckNoIncluded
    End Function
    Private Sub ResetPayBill()
        lblendingbal.Text = "0.00"
        lblAmt2pay.Text = "0.00"
        lblAmtdue.Text = "0.00"
    End Sub
    Private Sub PaySelectedBills()
        Try
            GetPaybillID() = Guid.NewGuid.ToString

            Call RecordPaymentOfBills(GetPaybillID())
            Call UpdateBillsBasedFromPayment(GetPaybillID())


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub RecordPaymentOfBills(ByVal paybillID As String)
        Dim amountToPay As Decimal = lblAmt2pay.Text
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "usp_t_RecordBillsPayment", _
                    New SqlParameter("@paybillID", paybillID), _
                    New SqlParameter("@compID", gCompanyID()), _
                    New SqlParameter("@bankAccountID", GetBankAccountID()), _
                    New SqlParameter("@paymentMethod", cboPaymentMethod.Text), _
                    New SqlParameter("@fcCheckNo", txtCheckNo.Text), _
                    New SqlParameter("@paymentDate", dtePaymentDate.Value.Date), _
                    New SqlParameter("@fbPrinted", chkCheckPrint.Checked), _
                    New SqlParameter("@fdAmountToPay", amountToPay), _
                    New SqlParameter("@fuCreatedBy", GetUser()))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UpdateBillsBasedFromPayment(ByVal paybillID As String)
        Try
            With grdBills
                Dim xCnt As Integer
                For xCnt = 0 To .RowCount - 1
                    If .Rows(xCnt).Cells("Select").Value = True Then

                        Dim billID As String = .Rows(xCnt).Cells("fxKeyBill").Value.ToString()
                        Dim amountToPay As Decimal = 0


                        If .Rows(xCnt).Cells("cAmttoPay").Value IsNot Nothing Then
                            amountToPay = .Rows(xCnt).Cells("cAmttoPay").Value

                            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "usp_t_UpdateBillsBasedFromPayment", _
                                    New SqlParameter("@payBillID", paybillID), _
                                    New SqlParameter("@billID", billID), _
                                    New SqlParameter("@amountPaid", amountToPay), _
                                    New SqlParameter("@user", GetUser()))
                        End If
                    End If
                Next
            End With
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Sub paybillitems(ByVal skey As String, ByVal sCV As String)

        With grdBills
            Dim xCnt As Integer
            For xCnt = 0 To .RowCount - 1
                If .Rows(xCnt).Cells(1).Value = True Then
                    If .Rows(xCnt).Cells(2).Value IsNot Nothing Then
                        Dim pdDiscount As Decimal = 0
                        Dim pdCredit As Decimal = 0
                        Dim pdAmtPay As Decimal = 0

                        If .Rows(xCnt).Cells("Disc.Used").Value IsNot Nothing Then
                            pdDiscount = .Rows(xCnt).Cells("Disc.Used").Value
                        End If
                        If .Rows(xCnt).Cells("Credit Used").Value IsNot Nothing Then
                            pdCredit = .Rows(xCnt).Cells("Credit Used").Value
                        End If
                        If .Rows(xCnt).Cells("cAmttoPay").Value IsNot Nothing Then
                            pdAmtPay = .Rows(xCnt).Cells("cAmttoPay").Value
                        End If

                        Call m_savepaybillsitems(skey, 1, .Rows(xCnt).Cells(0).Value.ToString, pdDiscount, _
                                            pdCredit, pdAmtPay, sCV, Me)
                    End If
                End If
            Next
        End With
    End Sub
    Private Function tobeprinted() As Integer
        'Dim xVal As Integer = 0
        'If rbtnPrint.Checked = True Then
        '    xVal = 1
        'Else
        '    xVal = 0
        'End If
        'Return xVal
    End Function
    Private Function assignedcheckno() As Integer
        'Dim xVal As Integer = 0
        'If rbtnAssignChkNo.Checked = True Then
        '    xVal = 1
        'Else
        '    xVal = 0
        'End If
        'Return xVal
    End Function
    Private Sub selectall()
        Dim xCnt As Integer
        With grdBills
            For xCnt = 0 To .RowCount - 1
                If .Rows(xCnt).Cells(2).Value IsNot Nothing Then
                    .Rows(xCnt).Cells(1).Value = True
                End If
            Next
        End With
    End Sub
    Private Sub clearselection()
        Dim xCnt As Integer
        With grdBills
            For xCnt = 0 To .RowCount - 1
                'If .Rows(xCnt).Cells(2).Value IsNot Nothing Then
                If .Rows(xCnt).Cells("Select").Value = True Then
                    .Rows(xCnt).Cells("Select").Value = False
                End If
                'End If
            Next
        End With
    End Sub
    Private Sub enableselectallbutton()
        With grdBills
            If .RowCount > 0 Then
                btnSelectAll.Enabled = True
            Else
                btnSelectAll.Enabled = False
            End If
        End With
    End Sub
    Private Sub getcurrentselectedbill()
        With grdBills
            If .RowCount > 0 Then
                If gTrans.gmkDefaultValues(.CurrentRow.Index, 1, grdBills) <> Nothing Then
                    If .Rows(.CurrentRow.Index).Cells(1).Value = True Then
                        Dim xAmtDue As Long = 0
                        xAmtDue = CDec(.Rows(.CurrentRow.Index).Cells(6).Value)
                        lblsupplier.Text = .Rows(.CurrentRow.Index).Cells(3).Value
                        lblrefno.Text = .Rows(.CurrentRow.Index).Cells(4).Value
                        lblTerms.Text = m_getTermsss(m_GetNameID(lblsupplier.Text, " Supplier"))
                        lblDiscount.Text = Format(CDec(xAmtDue * (m_getDiscount(m_GetNameID(lblsupplier.Text, " Supplier")) / 100)), "##,##0.00")
                    End If
                End If
            End If
        End With
    End Sub
    Private Sub calculatetotals()
        Try
            With grdBills
                Dim xCnt As Integer
                Dim xAmtDue As Decimal = 0.0
                Dim xDiscUsed As Decimal = 0.0
                Dim xCredUsed As Decimal = 0.0
                Dim xAmt2Pay As Decimal = 0.0
                For xCnt = 0 To .RowCount - 1

                    If .Rows(xCnt).Cells("Select").Value = True Then
                        xAmtDue += CDec(.Rows(xCnt).Cells("cAmttoPay").Value)
                        xAmt2Pay += CDec(.Rows(xCnt).Cells("cAmttoPay").Value)
                    End If

                Next
                lblAmtdue.Text = Format(CDec(xAmtDue), "##,##0.00")
                lblAmt2pay.Text = Format(CDec(xAmt2Pay), "##,##0.00")
            End With

        Catch ex As Exception

        End Try
    End Sub
    Private Sub cleartotalamount()
        Dim xAmtDue As Decimal = 0.0
        Dim xDiscUsed As Decimal = 0.0
        Dim xCredUsed As Decimal = 0.0
        Dim xAmt2Pay As Decimal = 0.0
        lblAmtdue.Text = Format(CDec(xAmtDue), "##,##0.00")
        lblDiscUsed.Text = Format(CDec(xDiscUsed), "##,##0.00")
        lblCredUsed.Text = Format(CDec(xCredUsed), "##,##0.00")
        lblAmt2pay.Text = Format(CDec(xAmt2Pay), "##,##0.00")
        lblTerms.Text = ""
        lblDiscount.Text = "0.00"
        lblCredits.Text = "0.00"
        lblCreditno.Text = "0"
        lblsupplier.Text = "Supplier Name"
        lblrefno.Text = "Reference no."
    End Sub
    Private Sub LoadBillList(Optional ByVal pbViewall As Boolean = True)
        grdBills.Columns.Clear()
        With grdBills
            Dim cDiscUsed As New DataGridViewTextBoxColumn
            Dim cCreditUsed As New DataGridViewTextBoxColumn
            Dim cAmttoPay As New DataGridViewTextBoxColumn
            Dim dtBill As DataTable = m_GetListofBills(dteDueBefore.Value.Date, rbtnShwAll.Checked).Tables(0)
            .DataSource = dtBill.DefaultView

            With cAmttoPay
                .Name = "cAmttoPay"
                .HeaderText = "Amount To Pay"

                '.DataPropertyName = "amount to pay"

                .Width = 100
            End With

            .Columns("fxKeyBill").Visible = False
            .Columns("fxKeySupplier").Visible = False
            .Columns.Add(cAmttoPay)

        End With

        Call enableselectallbutton()

        Call FormatDatagrid()
    End Sub
    Private Sub FormatDatagrid()
        With grdBills
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("fxKeySupplier").Visible = False

            .Columns("Select").Width = 45
            .Columns("Due Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("APV #").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Amt Due").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("cAmttoPay").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns("Amt Due").DefaultCellStyle.Format = "n2"
            .Columns("cAmttoPay").DefaultCellStyle.Format = "n2"

            .Columns("Due Date").ReadOnly = True
            .Columns("Supplier").ReadOnly = True
            .Columns("APV #").ReadOnly = True
            .Columns("Amt Due").ReadOnly = True

            .Columns("Due Date").DefaultCellStyle.BackColor = Color.LightBlue
            .Columns("APV #").DefaultCellStyle.BackColor = Color.LightBlue
            .Columns("Supplier").DefaultCellStyle.BackColor = Color.LightBlue
            .Columns("Amt Due").DefaultCellStyle.BackColor = Color.LightBlue
        End With

    End Sub

#End Region

    Private Sub LoadBills()
        If tmpEditBills() = True Then
            With Me.grdBills
                frm_vend_EnterBills.SupplierBill = False
                frm_vend_EnterBills.Mode = 2
                frm_vend_EnterBills.KeyBill = .Item(0, .CurrentRow.Index).Value.ToString
                frm_vend_EnterBills.ShowDialog()
            End With
        End If
    End Sub
    Private Function tmpEditBills() As Boolean
        Dim sSQLCmd As String = "select fxKeyGroup from dbo.tUsers where fcLogName='" & gUserName & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    If rd.Item(0) = 3 Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function
    Private Sub IsDateRestricted()
        'Validate Date
        If btnPay.Enabled = True Then
            If checkDate.checkIfRestricted(dtePaymentDate.Value) Then
                btnPay.Enabled = False 'true if date is NOT within range of period restricted
            Else
                btnPay.Enabled = True 'false if date is within range of period restricted
            End If
        End If
    End Sub
    Private Sub dtePaymentDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtePaymentDate.ValueChanged
        IsDateRestricted()
    End Sub
    Private Sub grdBills_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdBills.CurrentCellDirtyStateChanged
        If grdBills.IsCurrentCellDirty Then
            grdBills.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub
    Private Sub cboBankAccount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankAccount.SelectedIndexChanged
        If bgwLoadBalance.IsBusy = False Then
            Dim isError As Boolean = False
            Try
                GetBankAccountID() = cboBankAccount.SelectedItem.Col2
            Catch ex As Exception
                isError = True
            End Try

            If isError = False Then
                GetBankAccountID() = cboBankAccount.SelectedItem.Col2

                picLoading.Visible = True
                lblendingbal.Visible = False
                bgwLoadBalance.RunWorkerAsync()
            Else
                MessageBox.Show("User Error! The Bank account you selected is not found in our records.", "User Error!")
                cboBankAccount.Focus()
            End If
        Else
            MessageBox.Show("Bank Account Balance is still Loading. Please wait.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            cboBankAccount.SelectedIndex = GetSelectedIndexBankAccount()
        End If

        GetSelectedIndexBankAccount() = cboBankAccount.SelectedIndex
    End Sub
    Private Sub LoadAccountBalance(ByVal accountID As String)
        Try
            Using rd As SqlDataReader = (xSQLHelper.ExecuteReader("Accounts_GetBalance", New SqlParameter("@acnt_Id", accountID)))
                If rd.Read() Then
                    GetBankBalance() = rd.Item("Balance").ToString()
                Else
                    GetBankBalance() = 0
                End If
            End Using
        Catch ex As Exception
            ShowErrorMessage("LoadAccountBalance", Me.Name, ex.Message)
        End Try
    End Sub
    Private Sub frm_vend_PayBills_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If MessageBox.Show("Are you sure you want to close this transaction?", "Payment of Bills", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            e.Cancel() = True
        End If
    End Sub
    Private Sub grdBills_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdBills.EditingControlShowing
        If grdBills.CurrentCell.ColumnIndex = 0 And Not e.Control Is Nothing Then
            Dim tb As TextBox = CType(e.Control, TextBox)

            '---add an event handler to the TextBox control---
            AddHandler tb.KeyPress, AddressOf TextBox_KeyPress
        End If
    End Sub
    Private Sub TextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 46 Then
            If Asc(e.KeyChar) = 46 Then
                If CType(sender, TextBox).Text.Contains(Chr(46)) Then
                    e.Handled = True
                Else
                    e.Handled = False
                End If
            Else
                e.Handled = False
            End If
        Else
            e.Handled = True
        End If
    End Sub
    Private Sub lblendingbal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblendingbal.TextChanged
        lblendingbal.Text = Format(CDec(lblendingbal.Text), "##,##0.00")
    End Sub
    Private Sub bgwLoadBalance_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadBalance.DoWork
        LoadAccountBalance(GetBankAccountID())
    End Sub
    Private Sub bgwLoadBalance_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadBalance.RunWorkerCompleted
        picLoading.Visible = False
        lblendingbal.Visible = True

        Try
            lblendingbal.Text = GetBankBalance() - GetAmountToPay()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub lblAmt2pay_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblAmt2pay.TextChanged
        ComputeEndingBalance()
    End Sub
    Private Sub ComputeEndingBalance()
        Dim amountToPay As Decimal
        Dim endingBalance As Decimal

        Try
            amountToPay = IIf(Decimal.TryParse(lblAmt2pay.Text, 0), Decimal.Parse(lblAmt2pay.Text), 0)
            GetAmountToPay() = amountToPay
            endingBalance = GetBankBalance() - amountToPay
            lblendingbal.Text = endingBalance
        Catch ex As Exception
            Throw ex
        End Try

        Call AmountToPayValidation(amountToPay)
    End Sub
    Private Sub AmountToPayValidation(ByVal amountToPay As Decimal)
        If amountToPay = 0 Then
            btnPay.Enabled = False
        Else
            btnPay.Enabled = True
        End If

    End Sub
    Private Sub cboPaymentMethod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymentMethod.SelectedIndexChanged
        If cboPaymentMethod.Text = "Check" Then
            chkCheckPrint.Enabled = True
            txtCheckNo.Enabled = True
            chkCheckPrint.CheckState = CheckState.Checked
        Else
            chkCheckPrint.Enabled = False
            txtCheckNo.Enabled = False
            txtCheckNo.Text = ""
            chkCheckPrint.CheckState = CheckState.Unchecked
        End If
    End Sub
End Class