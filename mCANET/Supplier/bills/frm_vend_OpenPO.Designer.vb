<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_OpenPO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_OpenPO))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.gridPOItems = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MtcboSupplier = New MTGCComboBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.gridPOItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Supplier"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Select a Purchase Order to receive"
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.Location = New System.Drawing.Point(350, 12)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(84, 29)
        Me.btnOK.TabIndex = 4
        Me.btnOK.Text = "OK"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(350, 46)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(84, 29)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'gridPOItems
        '
        Me.gridPOItems.AllowUserToAddRows = False
        Me.gridPOItems.AllowUserToDeleteRows = False
        Me.gridPOItems.BackgroundColor = System.Drawing.SystemColors.Window
        Me.gridPOItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gridPOItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridPOItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5})
        Me.gridPOItems.GridColor = System.Drawing.Color.Black
        Me.gridPOItems.Location = New System.Drawing.Point(4, 46)
        Me.gridPOItems.Name = "gridPOItems"
        Me.gridPOItems.Size = New System.Drawing.Size(337, 151)
        Me.gridPOItems.TabIndex = 3
        '
        'Column1
        '
        Me.Column1.HeaderText = ""
        Me.Column1.Name = "Column1"
        Me.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column1.Width = 20
        '
        'Column2
        '
        Me.Column2.HeaderText = "Date"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 75
        '
        'Column3
        '
        Me.Column3.HeaderText = "PO No."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 75
        '
        'Column4
        '
        Me.Column4.HeaderText = "Memo"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 105
        '
        'Column5
        '
        Me.Column5.HeaderText = "Key"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Visible = False
        '
        'MtcboSupplier
        '
        Me.MtcboSupplier.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboSupplier.ArrowColor = System.Drawing.Color.Black
        Me.MtcboSupplier.BindedControl = CType(resources.GetObject("MtcboSupplier.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboSupplier.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboSupplier.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboSupplier.ColumnNum = 2
        Me.MtcboSupplier.ColumnWidth = "200; 0"
        Me.MtcboSupplier.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboSupplier.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboSupplier.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboSupplier.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboSupplier.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboSupplier.DisplayMember = "Text"
        Me.MtcboSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboSupplier.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboSupplier.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboSupplier.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboSupplier.DropDownWidth = 320
        Me.MtcboSupplier.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboSupplier.GridLineHorizontal = False
        Me.MtcboSupplier.GridLineVertical = False
        Me.MtcboSupplier.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboSupplier.Location = New System.Drawing.Point(45, 6)
        Me.MtcboSupplier.ManagingFastMouseMoving = True
        Me.MtcboSupplier.ManagingFastMouseMovingInterval = 30
        Me.MtcboSupplier.MaxDropDownItems = 25
        Me.MtcboSupplier.Name = "MtcboSupplier"
        Me.MtcboSupplier.SelectedItem = Nothing
        Me.MtcboSupplier.SelectedValue = Nothing
        Me.MtcboSupplier.Size = New System.Drawing.Size(266, 21)
        Me.MtcboSupplier.TabIndex = 1
        '
        'Timer1
        '
        Me.Timer1.Interval = 10
        '
        'frm_vend_OpenPO
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(438, 200)
        Me.Controls.Add(Me.MtcboSupplier)
        Me.Controls.Add(Me.gridPOItems)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frm_vend_OpenPO"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Open Purchase Orders"
        CType(Me.gridPOItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents gridPOItems As System.Windows.Forms.DataGridView
    Friend WithEvents MtcboSupplier As MTGCComboBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
