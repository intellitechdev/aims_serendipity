Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmBillsPaymentHistory
    Private gCon As New Clsappconfiguration

    Private billID As String
    Public Property GetBillID() As String
        Get
            Return billID
        End Get
        Set(ByVal value As String)
            billID = value
        End Set
    End Property

    Private Sub frmBillsPaymentHistory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadBillsPaymentHistory()
    End Sub

    Private Sub LoadBillsPaymentHistory()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "Supplier_BillsPaymentHistory", _
                    New SqlParameter("@billID", GetBillID()), _
                    New SqlParameter("@compID", gCompanyID()))
        grdPaymentHistory.DataSource = ds.Tables(0)

        Call LoadHeader()
        Call FormatGrid()
    End Sub

    Private Sub LoadHeader()
        Try
            txtSupplier.Text = grdPaymentHistory.Rows(0).Cells("fcSupplierName").Value.ToString()
            txtRefNo.Text = grdPaymentHistory.Rows(0).Cells("fcRefNo").Value.ToString()
            txtAmountDue.Text = Format(CDec(grdPaymentHistory.Rows(0).Cells("fnAmountDue").Value.ToString()), "##,##0.00")
            txtbalance.Text = Format(CDec(grdPaymentHistory.Rows(0).Cells("fdBalance").Value.ToString()), "##,##0.00")
        Catch ex As Exception
            MessageBox.Show("There has been no payment for this bill yet.", "Payment History", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        End Try

    End Sub

    Private Sub FormatGrid()
        With grdPaymentHistory
            .Columns("fcSupplierName").Visible = False
            .Columns("fcRefNo").Visible = False
            .Columns("fnAmountDue").Visible = False
            .Columns("fdBalance").Visible = False

            .Columns("fcAmountPaid").DefaultCellStyle.Format = "n2"

            .Columns("fcAmountPaid").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("fdPaymentDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("fcChkNo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Check Voucher No.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("fdPaymentDate").HeaderText = "Payment Date"
            .Columns("fcChkNo").HeaderText = "Check No."
            .Columns("Check Voucher No.").HeaderText = "CV No."
            .Columns("fcAmountPaid").HeaderText = "Amount"

            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
    End Sub



    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub
End Class