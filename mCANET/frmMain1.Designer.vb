<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim databaseInUseIcon As System.Windows.Forms.ToolStripSplitButton
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.msMainMenu = New System.Windows.Forms.MenuStrip()
        Me.ms_main_file = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_file_newcompany = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_file_openCompany = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_file_closecompany = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_file_LogOff = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_file_exit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_supplier = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sup_supmaster = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_sup_enterbills = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sup_billsapproval = New System.Windows.Forms.ToolStripMenuItem()
        Me.BankFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sup_paybills = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_sup_saleTax = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_tax_managetax = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_tax_paysalestax = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_tax_adjtaxdue = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_tax_taxliability = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_tax_salestax = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_tax_revenuesumm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_sup_createPO = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sup_rcvItembill = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sup_rcveItm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sup_entRcvBill = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientList1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientList2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitCreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountRegisterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_accountant = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepositAccountingEntriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateClosingEntriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_accountant_setclosdte = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_accountant_wrktrialbal = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_accountant_genjourentry = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotBalanceEntriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_accountinquiry = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountInquiryToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatementOfAccountToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_reports = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountantsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralLedgerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrialBalanceToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReceivablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CollectionReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatementOfAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountsPayablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountsPayableListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BillsPaymentAccountsReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsSalesreport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BudgetMonitoringReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostumerTransactionReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomerTransactionListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditMemoListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitMemoListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.JournalToolStripMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.JournalVoucherSummary = New System.Windows.Forms.ToolStripMenuItem()
        Me.JournalVoucherPerAccountToolStripMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CashReceiptsJournalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CRJMonthlySummarizedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CRJMonthlySpreadsheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByCollectorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByCashierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CombinedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByDocTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MonthlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CDJMonthlySpreadsheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JournalListingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralJournalToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MonthlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GJMonthlySpreadsheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PurchaseOderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PurchaseReturnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesReturnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinancialStatementsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceSheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IncomeStatementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IncomeStatementDrillDownToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IncomeStatementPrevYearComparisonToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IncomeStatementWithStatutoryAllocationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceSheetToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.IncomeStatementToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralLedgerToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrialBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WorkingTrialBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrialBalanceToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailyToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MonthlyToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CashPositionReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CashFlowStatementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitAccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountsReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfAccountReceivablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfAdvancesToOEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditAccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountsPayableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfAccountsPayableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoansReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScheduleOfLRByCategoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScheduleOfLRAllTypesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfLRByCategoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByCategoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllTypesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountAnalysisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BankReconciliationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AuditTrailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PESOReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_accountschedules = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitAccountsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfAccountsReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreditAcountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfAccountsPayableToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByCategoryToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllTypesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgingOfLoansReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountAnalysisToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnearnedInterestLapsingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_billing = New System.Windows.Forms.ToolStripMenuItem()
        Me.BillingReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BillingListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CollectionsIntegratorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_masterfile = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChartOfAccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentTypeSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckIssuanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_itemmaster = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_itemGroup = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_pricelevel = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_billingrate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_tax = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_salestax = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_salestaxcode = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_salesrep = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClassToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_master_customertype = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_vendortype = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_master_terms = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_paymentMethod = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.StatutoryAllocationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_master_other = New System.Windows.Forms.ToolStripMenuItem()
        Me.CostCenterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColumnInterfaceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecurringEntrySetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.statutoryMaster = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_computation = New System.Windows.Forms.ToolStripMenuItem()
        Me.SavingsInterestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DividendToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PatronageRefundToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BereavementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatutoryFundToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_database = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackUpDatabaseToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataMigrationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountRegisterToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountForwardBalanceToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerateTemplateToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetForwardBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientUploadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_systemsettings = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sett_userList1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sett_userList = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sett_createuser = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserAccesibilityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sett_changepass = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_sett_preferences = New System.Windows.Forms.ToolStripMenuItem()
        Me.PeriodRestrictionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JournalEntriesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChartOfAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupplierMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountRegisterToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompanyProfileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStripToolsOptions = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_banking = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_banking_writechecks = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_banking_makedeposit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_banking_transferfunds = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_banking_reconcile = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_customer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_cust_custmaster = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_cust_createSO = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_cust_createInvoice = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_cust_enterSR = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_cust_enterSC = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_cust_createS = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_cust_assesFC = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.ms_cust_recvPayment = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_cust_createCM = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateDebitMemosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_windows = New System.Windows.Forms.ToolStripMenuItem()
        Me.ms_main_help = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LblClassActivator = New System.Windows.Forms.Label()
        Me.btnbar = New System.Windows.Forms.Button()
        Me.lblCompanyName = New System.Windows.Forms.Label()
        Me.axbar = New vbAccelerator.Components.Controls.acclExplorerBar()
        Me.sidebarImages = New System.Windows.Forms.ImageList(Me.components)
        Me.statusBar = New System.Windows.Forms.StatusStrip()
        Me.lblCurrentDate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.currentDate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblCurrentUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.currentUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.databaseInUse = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toolStripUsername = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cpics5 = New System.Windows.Forms.PictureBox()
        Me.cpics4 = New System.Windows.Forms.PictureBox()
        Me.cpics3 = New System.Windows.Forms.PictureBox()
        Me.cpics2 = New System.Windows.Forms.PictureBox()
        Me.cpics1 = New System.Windows.Forms.PictureBox()
        Me.cpics = New System.Windows.Forms.PictureBox()
        Me.DynamicColumnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        databaseInUseIcon = New System.Windows.Forms.ToolStripSplitButton()
        Me.msMainMenu.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.statusBar.SuspendLayout()
        CType(Me.cpics5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cpics4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cpics3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cpics2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cpics1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cpics, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'databaseInUseIcon
        '
        databaseInUseIcon.AutoSize = False
        databaseInUseIcon.AutoToolTip = False
        databaseInUseIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        databaseInUseIcon.ImageTransparentColor = System.Drawing.Color.Magenta
        databaseInUseIcon.Name = "databaseInUseIcon"
        databaseInUseIcon.Size = New System.Drawing.Size(32, 22)
        '
        'msMainMenu
        '
        Me.msMainMenu.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.msMainMenu.BackColor = System.Drawing.Color.Transparent
        Me.msMainMenu.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.msMainMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.msMainMenu.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.msMainMenu.GripMargin = New System.Windows.Forms.Padding(0)
        Me.msMainMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible
        Me.msMainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_main_file, Me.ms_main_supplier, Me.ms_main_accountant, Me.ms_main_accountinquiry, Me.ms_main_reports, Me.ms_main_accountschedules, Me.ms_main_billing, Me.ms_main_masterfile, Me.ms_main_computation, Me.ms_main_database, Me.ms_main_systemsettings, Me.toolStripToolsOptions, Me.ms_main_banking, Me.ms_main_customer, Me.ms_main_windows, Me.ms_main_help})
        Me.msMainMenu.Location = New System.Drawing.Point(0, 0)
        Me.msMainMenu.Name = "msMainMenu"
        Me.msMainMenu.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.msMainMenu.Size = New System.Drawing.Size(1018, 24)
        Me.msMainMenu.TabIndex = 1
        '
        'ms_main_file
        '
        Me.ms_main_file.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_file_newcompany, Me.ms_file_openCompany, Me.ms_file_closecompany, Me.ToolStripSeparator3, Me.ms_file_LogOff, Me.ms_file_exit})
        Me.ms_main_file.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_file.Image = CType(resources.GetObject("ms_main_file.Image"), System.Drawing.Image)
        Me.ms_main_file.Name = "ms_main_file"
        Me.ms_main_file.Size = New System.Drawing.Size(55, 20)
        Me.ms_main_file.Text = "&File"
        '
        'ms_file_newcompany
        '
        Me.ms_file_newcompany.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_file_newcompany.Name = "ms_file_newcompany"
        Me.ms_file_newcompany.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ms_file_newcompany.Size = New System.Drawing.Size(231, 22)
        Me.ms_file_newcompany.Text = "&New Project"
        '
        'ms_file_openCompany
        '
        Me.ms_file_openCompany.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_file_openCompany.Name = "ms_file_openCompany"
        Me.ms_file_openCompany.ShortcutKeyDisplayString = "Ctrl+O"
        Me.ms_file_openCompany.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.ms_file_openCompany.Size = New System.Drawing.Size(231, 22)
        Me.ms_file_openCompany.Text = "&Open Current Project"
        '
        'ms_file_closecompany
        '
        Me.ms_file_closecompany.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_file_closecompany.Name = "ms_file_closecompany"
        Me.ms_file_closecompany.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ms_file_closecompany.Size = New System.Drawing.Size(231, 22)
        Me.ms_file_closecompany.Text = "Close Project"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(228, 6)
        '
        'ms_file_LogOff
        '
        Me.ms_file_LogOff.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_file_LogOff.Name = "ms_file_LogOff"
        Me.ms_file_LogOff.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.ms_file_LogOff.Size = New System.Drawing.Size(231, 22)
        Me.ms_file_LogOff.Text = "&Log Off"
        Me.ms_file_LogOff.Visible = False
        '
        'ms_file_exit
        '
        Me.ms_file_exit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_file_exit.Name = "ms_file_exit"
        Me.ms_file_exit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.ms_file_exit.Size = New System.Drawing.Size(231, 22)
        Me.ms_file_exit.Text = "&Exit"
        '
        'ms_main_supplier
        '
        Me.ms_main_supplier.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_sup_supmaster, Me.ToolStripSeparator13, Me.ms_sup_enterbills, Me.ms_sup_billsapproval, Me.BankFileToolStripMenuItem, Me.ms_sup_paybills, Me.ToolStripSeparator1, Me.ms_sup_saleTax, Me.ToolStripSeparator2, Me.ms_sup_createPO, Me.ms_sup_rcvItembill, Me.ms_sup_rcveItm, Me.ms_sup_entRcvBill, Me.ClientMasterToolStripMenuItem, Me.ClientListToolStripMenuItem, Me.AccountSetupToolStripMenuItem})
        Me.ms_main_supplier.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_supplier.Image = CType(resources.GetObject("ms_main_supplier.Image"), System.Drawing.Image)
        Me.ms_main_supplier.Name = "ms_main_supplier"
        Me.ms_main_supplier.Size = New System.Drawing.Size(119, 20)
        Me.ms_main_supplier.Text = "Client Manager"
        Me.ms_main_supplier.Visible = False
        '
        'ms_sup_supmaster
        '
        Me.ms_sup_supmaster.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_supmaster.Name = "ms_sup_supmaster"
        Me.ms_sup_supmaster.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_supmaster.Text = "Member Master"
        Me.ms_sup_supmaster.Visible = False
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(229, 6)
        Me.ToolStripSeparator13.Visible = False
        '
        'ms_sup_enterbills
        '
        Me.ms_sup_enterbills.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_enterbills.Name = "ms_sup_enterbills"
        Me.ms_sup_enterbills.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_enterbills.Text = "Voucher Register (Bills Entry)"
        Me.ms_sup_enterbills.Visible = False
        '
        'ms_sup_billsapproval
        '
        Me.ms_sup_billsapproval.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_billsapproval.Name = "ms_sup_billsapproval"
        Me.ms_sup_billsapproval.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_billsapproval.Text = "Bills for Approval"
        Me.ms_sup_billsapproval.Visible = False
        '
        'BankFileToolStripMenuItem
        '
        Me.BankFileToolStripMenuItem.Name = "BankFileToolStripMenuItem"
        Me.BankFileToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.BankFileToolStripMenuItem.Text = "Bank File"
        Me.BankFileToolStripMenuItem.Visible = False
        '
        'ms_sup_paybills
        '
        Me.ms_sup_paybills.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_paybills.Name = "ms_sup_paybills"
        Me.ms_sup_paybills.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_paybills.Text = "Check Disbursement"
        Me.ms_sup_paybills.Visible = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(229, 6)
        Me.ToolStripSeparator1.Visible = False
        '
        'ms_sup_saleTax
        '
        Me.ms_sup_saleTax.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_tax_managetax, Me.ToolStripSeparator9, Me.ms_tax_paysalestax, Me.ms_tax_adjtaxdue, Me.ToolStripSeparator10, Me.ms_tax_taxliability, Me.ms_tax_salestax, Me.ms_tax_revenuesumm})
        Me.ms_sup_saleTax.Enabled = False
        Me.ms_sup_saleTax.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_saleTax.Name = "ms_sup_saleTax"
        Me.ms_sup_saleTax.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_saleTax.Text = "Sales Tax"
        Me.ms_sup_saleTax.Visible = False
        '
        'ms_tax_managetax
        '
        Me.ms_tax_managetax.Name = "ms_tax_managetax"
        Me.ms_tax_managetax.Size = New System.Drawing.Size(226, 22)
        Me.ms_tax_managetax.Text = "Manage Tax"
        Me.ms_tax_managetax.Visible = False
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(223, 6)
        Me.ToolStripSeparator9.Visible = False
        '
        'ms_tax_paysalestax
        '
        Me.ms_tax_paysalestax.Name = "ms_tax_paysalestax"
        Me.ms_tax_paysalestax.Size = New System.Drawing.Size(226, 22)
        Me.ms_tax_paysalestax.Text = "Pay Sales Tax"
        Me.ms_tax_paysalestax.Visible = False
        '
        'ms_tax_adjtaxdue
        '
        Me.ms_tax_adjtaxdue.Name = "ms_tax_adjtaxdue"
        Me.ms_tax_adjtaxdue.Size = New System.Drawing.Size(226, 22)
        Me.ms_tax_adjtaxdue.Text = "Adjust Sales Tax Due"
        Me.ms_tax_adjtaxdue.Visible = False
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(223, 6)
        Me.ToolStripSeparator10.Visible = False
        '
        'ms_tax_taxliability
        '
        Me.ms_tax_taxliability.Name = "ms_tax_taxliability"
        Me.ms_tax_taxliability.Size = New System.Drawing.Size(226, 22)
        Me.ms_tax_taxliability.Text = "Sales Tax Liability"
        Me.ms_tax_taxliability.Visible = False
        '
        'ms_tax_salestax
        '
        Me.ms_tax_salestax.Name = "ms_tax_salestax"
        Me.ms_tax_salestax.Size = New System.Drawing.Size(226, 22)
        Me.ms_tax_salestax.Text = "Sales Tax"
        Me.ms_tax_salestax.Visible = False
        '
        'ms_tax_revenuesumm
        '
        Me.ms_tax_revenuesumm.Name = "ms_tax_revenuesumm"
        Me.ms_tax_revenuesumm.Size = New System.Drawing.Size(226, 22)
        Me.ms_tax_revenuesumm.Text = "Sales Tax Revenue Summary"
        Me.ms_tax_revenuesumm.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(229, 6)
        Me.ToolStripSeparator2.Visible = False
        '
        'ms_sup_createPO
        '
        Me.ms_sup_createPO.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_createPO.Name = "ms_sup_createPO"
        Me.ms_sup_createPO.ShowShortcutKeys = False
        Me.ms_sup_createPO.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_createPO.Text = "Purchase Orders"
        Me.ms_sup_createPO.Visible = False
        '
        'ms_sup_rcvItembill
        '
        Me.ms_sup_rcvItembill.Enabled = False
        Me.ms_sup_rcvItembill.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_rcvItembill.Name = "ms_sup_rcvItembill"
        Me.ms_sup_rcvItembill.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_rcvItembill.Text = "Receive Item and Bill"
        Me.ms_sup_rcvItembill.Visible = False
        '
        'ms_sup_rcveItm
        '
        Me.ms_sup_rcveItm.Enabled = False
        Me.ms_sup_rcveItm.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_rcveItm.Name = "ms_sup_rcveItm"
        Me.ms_sup_rcveItm.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_rcveItm.Text = "Receive Item"
        Me.ms_sup_rcveItm.Visible = False
        '
        'ms_sup_entRcvBill
        '
        Me.ms_sup_entRcvBill.Enabled = False
        Me.ms_sup_entRcvBill.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sup_entRcvBill.Name = "ms_sup_entRcvBill"
        Me.ms_sup_entRcvBill.Size = New System.Drawing.Size(232, 22)
        Me.ms_sup_entRcvBill.Text = "Enter Bill for Received Item"
        Me.ms_sup_entRcvBill.Visible = False
        '
        'ClientMasterToolStripMenuItem
        '
        Me.ClientMasterToolStripMenuItem.Name = "ClientMasterToolStripMenuItem"
        Me.ClientMasterToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.ClientMasterToolStripMenuItem.Text = "Client Master"
        Me.ClientMasterToolStripMenuItem.Visible = False
        '
        'ClientListToolStripMenuItem
        '
        Me.ClientListToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientList1ToolStripMenuItem, Me.ClientList2ToolStripMenuItem})
        Me.ClientListToolStripMenuItem.Name = "ClientListToolStripMenuItem"
        Me.ClientListToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.ClientListToolStripMenuItem.Text = "Client List"
        Me.ClientListToolStripMenuItem.Visible = False
        '
        'ClientList1ToolStripMenuItem
        '
        Me.ClientList1ToolStripMenuItem.Name = "ClientList1ToolStripMenuItem"
        Me.ClientList1ToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ClientList1ToolStripMenuItem.Text = "Client List 1"
        '
        'ClientList2ToolStripMenuItem
        '
        Me.ClientList2ToolStripMenuItem.Name = "ClientList2ToolStripMenuItem"
        Me.ClientList2ToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ClientList2ToolStripMenuItem.Text = "Client List 2"
        '
        'AccountSetupToolStripMenuItem
        '
        Me.AccountSetupToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebitCreditToolStripMenuItem, Me.AccountRegisterToolStripMenuItem})
        Me.AccountSetupToolStripMenuItem.Name = "AccountSetupToolStripMenuItem"
        Me.AccountSetupToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.AccountSetupToolStripMenuItem.Text = "Account Setup"
        Me.AccountSetupToolStripMenuItem.Visible = False
        '
        'DebitCreditToolStripMenuItem
        '
        Me.DebitCreditToolStripMenuItem.Name = "DebitCreditToolStripMenuItem"
        Me.DebitCreditToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.DebitCreditToolStripMenuItem.Text = "Debit/Credit Accounts"
        Me.DebitCreditToolStripMenuItem.Visible = False
        '
        'AccountRegisterToolStripMenuItem
        '
        Me.AccountRegisterToolStripMenuItem.Name = "AccountRegisterToolStripMenuItem"
        Me.AccountRegisterToolStripMenuItem.Size = New System.Drawing.Size(194, 22)
        Me.AccountRegisterToolStripMenuItem.Text = "Account Register"
        Me.AccountRegisterToolStripMenuItem.Visible = False
        '
        'ms_main_accountant
        '
        Me.ms_main_accountant.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DepositAccountingEntriesToolStripMenuItem, Me.CreateClosingEntriesToolStripMenuItem, Me.ms_accountant_setclosdte, Me.ms_accountant_wrktrialbal, Me.ms_accountant_genjourentry, Me.NotBalanceEntriesToolStripMenuItem})
        Me.ms_main_accountant.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_accountant.Image = CType(resources.GetObject("ms_main_accountant.Image"), System.Drawing.Image)
        Me.ms_main_accountant.Name = "ms_main_accountant"
        Me.ms_main_accountant.Size = New System.Drawing.Size(105, 20)
        Me.ms_main_accountant.Text = "&Transactions"
        Me.ms_main_accountant.Visible = False
        '
        'DepositAccountingEntriesToolStripMenuItem
        '
        Me.DepositAccountingEntriesToolStripMenuItem.Name = "DepositAccountingEntriesToolStripMenuItem"
        Me.DepositAccountingEntriesToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.DepositAccountingEntriesToolStripMenuItem.Text = "Deposit Accounting Entries"
        Me.DepositAccountingEntriesToolStripMenuItem.Visible = False
        '
        'CreateClosingEntriesToolStripMenuItem
        '
        Me.CreateClosingEntriesToolStripMenuItem.Name = "CreateClosingEntriesToolStripMenuItem"
        Me.CreateClosingEntriesToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.CreateClosingEntriesToolStripMenuItem.Text = "Create Closing Entries"
        Me.CreateClosingEntriesToolStripMenuItem.Visible = False
        '
        'ms_accountant_setclosdte
        '
        Me.ms_accountant_setclosdte.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_accountant_setclosdte.Name = "ms_accountant_setclosdte"
        Me.ms_accountant_setclosdte.Size = New System.Drawing.Size(220, 22)
        Me.ms_accountant_setclosdte.Text = "Set Closing Date"
        Me.ms_accountant_setclosdte.Visible = False
        '
        'ms_accountant_wrktrialbal
        '
        Me.ms_accountant_wrktrialbal.Enabled = False
        Me.ms_accountant_wrktrialbal.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_accountant_wrktrialbal.Name = "ms_accountant_wrktrialbal"
        Me.ms_accountant_wrktrialbal.Size = New System.Drawing.Size(220, 22)
        Me.ms_accountant_wrktrialbal.Text = "Working Trial Balance"
        Me.ms_accountant_wrktrialbal.Visible = False
        '
        'ms_accountant_genjourentry
        '
        Me.ms_accountant_genjourentry.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_accountant_genjourentry.Name = "ms_accountant_genjourentry"
        Me.ms_accountant_genjourentry.Size = New System.Drawing.Size(220, 22)
        Me.ms_accountant_genjourentry.Text = "Transaction Entries"
        Me.ms_accountant_genjourentry.Visible = False
        '
        'NotBalanceEntriesToolStripMenuItem
        '
        Me.NotBalanceEntriesToolStripMenuItem.Name = "NotBalanceEntriesToolStripMenuItem"
        Me.NotBalanceEntriesToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.NotBalanceEntriesToolStripMenuItem.Text = "Not Balance Entries"
        Me.NotBalanceEntriesToolStripMenuItem.Visible = False
        '
        'ms_main_accountinquiry
        '
        Me.ms_main_accountinquiry.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountInquiryToolStripMenuItem1, Me.StatementOfAccountToolStripMenuItem1})
        Me.ms_main_accountinquiry.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_accountinquiry.Image = Global.CSAcctg.My.Resources.Resources.Help_book
        Me.ms_main_accountinquiry.Name = "ms_main_accountinquiry"
        Me.ms_main_accountinquiry.Size = New System.Drawing.Size(125, 20)
        Me.ms_main_accountinquiry.Text = "Account Inquiry"
        Me.ms_main_accountinquiry.Visible = False
        '
        'AccountInquiryToolStripMenuItem1
        '
        Me.AccountInquiryToolStripMenuItem1.Name = "AccountInquiryToolStripMenuItem1"
        Me.AccountInquiryToolStripMenuItem1.Size = New System.Drawing.Size(197, 22)
        Me.AccountInquiryToolStripMenuItem1.Text = "Account Inquiry"
        Me.AccountInquiryToolStripMenuItem1.Visible = False
        '
        'StatementOfAccountToolStripMenuItem1
        '
        Me.StatementOfAccountToolStripMenuItem1.Name = "StatementOfAccountToolStripMenuItem1"
        Me.StatementOfAccountToolStripMenuItem1.Size = New System.Drawing.Size(197, 22)
        Me.StatementOfAccountToolStripMenuItem1.Text = "Statement of Account"
        Me.StatementOfAccountToolStripMenuItem1.Visible = False
        '
        'ms_main_reports
        '
        Me.ms_main_reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountantsToolStripMenuItem, Me.ReceivablesToolStripMenuItem, Me.PayablesToolStripMenuItem, Me.SalesToolStripMenuItem, Me.ToolStripMenuItem6, Me.ToolStripMenuItem2, Me.BudgetMonitoringReportToolStripMenuItem, Me.CostumerTransactionReportsToolStripMenuItem, Me.CustomerTransactionListToolStripMenuItem, Me.ToolStripMenuItem4, Me.ToolStripMenuItem10, Me.ToolStripMenuItem11, Me.ToolStripMenuItem3, Me.JournalToolStripMenu, Me.FinancialStatementsToolStripMenuItem, Me.TrialBalanceToolStripMenuItem, Me.CashPositionReportToolStripMenuItem, Me.CashFlowStatementToolStripMenuItem, Me.AccountBalanceToolStripMenuItem, Me.LoansReceivableToolStripMenuItem, Me.AccountAnalysisToolStripMenuItem, Me.BankReconciliationToolStripMenuItem, Me.AuditTrailToolStripMenuItem, Me.PESOReportsToolStripMenuItem})
        Me.ms_main_reports.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_reports.Image = CType(resources.GetObject("ms_main_reports.Image"), System.Drawing.Image)
        Me.ms_main_reports.Name = "ms_main_reports"
        Me.ms_main_reports.Size = New System.Drawing.Size(77, 20)
        Me.ms_main_reports.Text = "&Reports"
        Me.ms_main_reports.Visible = False
        '
        'AccountantsToolStripMenuItem
        '
        Me.AccountantsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GeneralLedgerToolStripMenuItem, Me.TrialBalanceToolStripMenuItem1})
        Me.AccountantsToolStripMenuItem.Name = "AccountantsToolStripMenuItem"
        Me.AccountantsToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.AccountantsToolStripMenuItem.Text = "Accountants"
        Me.AccountantsToolStripMenuItem.Visible = False
        '
        'GeneralLedgerToolStripMenuItem
        '
        Me.GeneralLedgerToolStripMenuItem.Name = "GeneralLedgerToolStripMenuItem"
        Me.GeneralLedgerToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.GeneralLedgerToolStripMenuItem.Text = "General Ledger"
        '
        'TrialBalanceToolStripMenuItem1
        '
        Me.TrialBalanceToolStripMenuItem1.Name = "TrialBalanceToolStripMenuItem1"
        Me.TrialBalanceToolStripMenuItem1.Size = New System.Drawing.Size(155, 22)
        Me.TrialBalanceToolStripMenuItem1.Text = "Trial Balance"
        '
        'ReceivablesToolStripMenuItem
        '
        Me.ReceivablesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CollectionReportToolStripMenuItem, Me.StatementOfAccountToolStripMenuItem})
        Me.ReceivablesToolStripMenuItem.Enabled = False
        Me.ReceivablesToolStripMenuItem.Name = "ReceivablesToolStripMenuItem"
        Me.ReceivablesToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.ReceivablesToolStripMenuItem.Text = "Company Receivables"
        Me.ReceivablesToolStripMenuItem.Visible = False
        '
        'CollectionReportToolStripMenuItem
        '
        Me.CollectionReportToolStripMenuItem.Enabled = False
        Me.CollectionReportToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CollectionReportToolStripMenuItem.Name = "CollectionReportToolStripMenuItem"
        Me.CollectionReportToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.CollectionReportToolStripMenuItem.Text = "Collection Report"
        Me.CollectionReportToolStripMenuItem.Visible = False
        '
        'StatementOfAccountToolStripMenuItem
        '
        Me.StatementOfAccountToolStripMenuItem.Enabled = False
        Me.StatementOfAccountToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.StatementOfAccountToolStripMenuItem.Name = "StatementOfAccountToolStripMenuItem"
        Me.StatementOfAccountToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.StatementOfAccountToolStripMenuItem.Text = "Statement of Account"
        Me.StatementOfAccountToolStripMenuItem.Visible = False
        '
        'PayablesToolStripMenuItem
        '
        Me.PayablesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountsPayablesToolStripMenuItem, Me.AccountsPayableListToolStripMenuItem, Me.BillsPaymentAccountsReportToolStripMenuItem, Me.ToolStripMenuItem16})
        Me.PayablesToolStripMenuItem.Name = "PayablesToolStripMenuItem"
        Me.PayablesToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.PayablesToolStripMenuItem.Text = "Supplier and Payables"
        Me.PayablesToolStripMenuItem.Visible = False
        '
        'AccountsPayablesToolStripMenuItem
        '
        Me.AccountsPayablesToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.AccountsPayablesToolStripMenuItem.Name = "AccountsPayablesToolStripMenuItem"
        Me.AccountsPayablesToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.AccountsPayablesToolStripMenuItem.Text = "Accounts Payable Summary"
        '
        'AccountsPayableListToolStripMenuItem
        '
        Me.AccountsPayableListToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.AccountsPayableListToolStripMenuItem.Name = "AccountsPayableListToolStripMenuItem"
        Me.AccountsPayableListToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.AccountsPayableListToolStripMenuItem.Text = "Accounts Payable && Check Voucher List"
        '
        'BillsPaymentAccountsReportToolStripMenuItem
        '
        Me.BillsPaymentAccountsReportToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.BillsPaymentAccountsReportToolStripMenuItem.Name = "BillsPaymentAccountsReportToolStripMenuItem"
        Me.BillsPaymentAccountsReportToolStripMenuItem.Size = New System.Drawing.Size(287, 22)
        Me.BillsPaymentAccountsReportToolStripMenuItem.Text = "Bills Payment Accounts Report"
        Me.BillsPaymentAccountsReportToolStripMenuItem.Visible = False
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(287, 22)
        Me.ToolStripMenuItem16.Tag = "CVHistory"
        Me.ToolStripMenuItem16.Text = "Check Voucher History"
        '
        'SalesToolStripMenuItem
        '
        Me.SalesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsSalesreport})
        Me.SalesToolStripMenuItem.Enabled = False
        Me.SalesToolStripMenuItem.Name = "SalesToolStripMenuItem"
        Me.SalesToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.SalesToolStripMenuItem.Text = "Sales"
        Me.SalesToolStripMenuItem.Visible = False
        '
        'tsSalesreport
        '
        Me.tsSalesreport.Enabled = False
        Me.tsSalesreport.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.tsSalesreport.Name = "tsSalesreport"
        Me.tsSalesreport.Size = New System.Drawing.Size(142, 22)
        Me.tsSalesreport.Text = "Sales Report"
        Me.tsSalesreport.Visible = False
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem7, Me.ToolStripMenuItem8, Me.ToolStripMenuItem9})
        Me.ToolStripMenuItem6.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem6.Text = "Master List"
        Me.ToolStripMenuItem6.Visible = False
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(197, 22)
        Me.ToolStripMenuItem7.Text = "Customer List"
        Me.ToolStripMenuItem7.Visible = False
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(197, 22)
        Me.ToolStripMenuItem8.Text = "Item Master/Inventory"
        Me.ToolStripMenuItem8.Visible = False
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(197, 22)
        Me.ToolStripMenuItem9.Text = "Supplier Master"
        Me.ToolStripMenuItem9.Visible = False
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Enabled = False
        Me.ToolStripMenuItem2.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem2.Text = "Aging Report"
        Me.ToolStripMenuItem2.Visible = False
        '
        'BudgetMonitoringReportToolStripMenuItem
        '
        Me.BudgetMonitoringReportToolStripMenuItem.Enabled = False
        Me.BudgetMonitoringReportToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.BudgetMonitoringReportToolStripMenuItem.Name = "BudgetMonitoringReportToolStripMenuItem"
        Me.BudgetMonitoringReportToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.BudgetMonitoringReportToolStripMenuItem.Text = "Budget Monitoring Report"
        Me.BudgetMonitoringReportToolStripMenuItem.Visible = False
        '
        'CostumerTransactionReportsToolStripMenuItem
        '
        Me.CostumerTransactionReportsToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CostumerTransactionReportsToolStripMenuItem.Name = "CostumerTransactionReportsToolStripMenuItem"
        Me.CostumerTransactionReportsToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.CostumerTransactionReportsToolStripMenuItem.Text = "Customer Transaction  Reports"
        Me.CostumerTransactionReportsToolStripMenuItem.Visible = False
        '
        'CustomerTransactionListToolStripMenuItem
        '
        Me.CustomerTransactionListToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InvoiceListToolStripMenuItem, Me.CreditMemoListToolStripMenuItem, Me.DebitMemoListToolStripMenuItem})
        Me.CustomerTransactionListToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CustomerTransactionListToolStripMenuItem.Name = "CustomerTransactionListToolStripMenuItem"
        Me.CustomerTransactionListToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.CustomerTransactionListToolStripMenuItem.Text = "Customers' Transaction List"
        Me.CustomerTransactionListToolStripMenuItem.Visible = False
        '
        'InvoiceListToolStripMenuItem
        '
        Me.InvoiceListToolStripMenuItem.Name = "InvoiceListToolStripMenuItem"
        Me.InvoiceListToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.InvoiceListToolStripMenuItem.Text = "Invoice List"
        '
        'CreditMemoListToolStripMenuItem
        '
        Me.CreditMemoListToolStripMenuItem.Name = "CreditMemoListToolStripMenuItem"
        Me.CreditMemoListToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.CreditMemoListToolStripMenuItem.Text = "Credit Memo List"
        '
        'DebitMemoListToolStripMenuItem
        '
        Me.DebitMemoListToolStripMenuItem.Name = "DebitMemoListToolStripMenuItem"
        Me.DebitMemoListToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.DebitMemoListToolStripMenuItem.Text = "Debit Memo List"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Enabled = False
        Me.ToolStripMenuItem4.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem4.Text = "Finance Charge"
        Me.ToolStripMenuItem4.Visible = False
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Enabled = False
        Me.ToolStripMenuItem10.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem10.Text = "Reimbursable Expenses"
        Me.ToolStripMenuItem10.Visible = False
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Enabled = False
        Me.ToolStripMenuItem11.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem11.Text = "Tax Report"
        Me.ToolStripMenuItem11.Visible = False
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Enabled = False
        Me.ToolStripMenuItem3.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(241, 22)
        Me.ToolStripMenuItem3.Text = "Undeposited Funds"
        Me.ToolStripMenuItem3.Visible = False
        '
        'JournalToolStripMenu
        '
        Me.JournalToolStripMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.JournalVoucherSummary, Me.JournalVoucherPerAccountToolStripMenu, Me.CashReceiptsJournalToolStripMenuItem, Me.ByDocTypeToolStripMenuItem, Me.JournalListingToolStripMenuItem, Me.GeneralJournalToolStripMenuItem1, Me.PurchaseOderToolStripMenuItem, Me.PurchaseReturnToolStripMenuItem, Me.SalesInvoiceToolStripMenuItem, Me.SalesReturnToolStripMenuItem})
        Me.JournalToolStripMenu.Name = "JournalToolStripMenu"
        Me.JournalToolStripMenu.Size = New System.Drawing.Size(241, 22)
        Me.JournalToolStripMenu.Text = "Journal"
        '
        'JournalVoucherSummary
        '
        Me.JournalVoucherSummary.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.JournalVoucherSummary.Name = "JournalVoucherSummary"
        Me.JournalVoucherSummary.Size = New System.Drawing.Size(284, 22)
        Me.JournalVoucherSummary.Text = "Journal Voucher Summary"
        Me.JournalVoucherSummary.Visible = False
        '
        'JournalVoucherPerAccountToolStripMenu
        '
        Me.JournalVoucherPerAccountToolStripMenu.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.JournalVoucherPerAccountToolStripMenu.Name = "JournalVoucherPerAccountToolStripMenu"
        Me.JournalVoucherPerAccountToolStripMenu.Size = New System.Drawing.Size(284, 22)
        Me.JournalVoucherPerAccountToolStripMenu.Text = "Journal Voucher Summary per Account"
        Me.JournalVoucherPerAccountToolStripMenu.Visible = False
        '
        'CashReceiptsJournalToolStripMenuItem
        '
        Me.CashReceiptsJournalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CRJMonthlySummarizedToolStripMenuItem, Me.CRJMonthlySpreadsheetToolStripMenuItem, Me.ByCollectorToolStripMenuItem, Me.ByCashierToolStripMenuItem, Me.CombinedToolStripMenuItem})
        Me.CashReceiptsJournalToolStripMenuItem.Name = "CashReceiptsJournalToolStripMenuItem"
        Me.CashReceiptsJournalToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.CashReceiptsJournalToolStripMenuItem.Text = "Cash Receipts Journal"
        Me.CashReceiptsJournalToolStripMenuItem.Visible = False
        '
        'CRJMonthlySummarizedToolStripMenuItem
        '
        Me.CRJMonthlySummarizedToolStripMenuItem.Name = "CRJMonthlySummarizedToolStripMenuItem"
        Me.CRJMonthlySummarizedToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.CRJMonthlySummarizedToolStripMenuItem.Text = "CRJ - Monthly (Summarized)"
        Me.CRJMonthlySummarizedToolStripMenuItem.Visible = False
        '
        'CRJMonthlySpreadsheetToolStripMenuItem
        '
        Me.CRJMonthlySpreadsheetToolStripMenuItem.Name = "CRJMonthlySpreadsheetToolStripMenuItem"
        Me.CRJMonthlySpreadsheetToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.CRJMonthlySpreadsheetToolStripMenuItem.Text = "CRJ - Monthly (Spreadsheet)"
        Me.CRJMonthlySpreadsheetToolStripMenuItem.Visible = False
        '
        'ByCollectorToolStripMenuItem
        '
        Me.ByCollectorToolStripMenuItem.Name = "ByCollectorToolStripMenuItem"
        Me.ByCollectorToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ByCollectorToolStripMenuItem.Text = "CCR - Per Collector"
        Me.ByCollectorToolStripMenuItem.Visible = False
        '
        'ByCashierToolStripMenuItem
        '
        Me.ByCashierToolStripMenuItem.Name = "ByCashierToolStripMenuItem"
        Me.ByCashierToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ByCashierToolStripMenuItem.Text = "CCR - Per Cashier"
        Me.ByCashierToolStripMenuItem.Visible = False
        '
        'CombinedToolStripMenuItem
        '
        Me.CombinedToolStripMenuItem.Name = "CombinedToolStripMenuItem"
        Me.CombinedToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.CombinedToolStripMenuItem.Text = "CCR - Combined"
        Me.CombinedToolStripMenuItem.Visible = False
        '
        'ByDocTypeToolStripMenuItem
        '
        Me.ByDocTypeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DailyToolStripMenuItem, Me.MonthlyToolStripMenuItem, Me.CDJMonthlySpreadsheetToolStripMenuItem})
        Me.ByDocTypeToolStripMenuItem.Name = "ByDocTypeToolStripMenuItem"
        Me.ByDocTypeToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.ByDocTypeToolStripMenuItem.Text = "Cash Disbursement Journal"
        Me.ByDocTypeToolStripMenuItem.Visible = False
        '
        'DailyToolStripMenuItem
        '
        Me.DailyToolStripMenuItem.Name = "DailyToolStripMenuItem"
        Me.DailyToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.DailyToolStripMenuItem.Text = "CDJ - Daily"
        '
        'MonthlyToolStripMenuItem
        '
        Me.MonthlyToolStripMenuItem.Name = "MonthlyToolStripMenuItem"
        Me.MonthlyToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.MonthlyToolStripMenuItem.Text = "CDJ - Monthly (Summarized)"
        '
        'CDJMonthlySpreadsheetToolStripMenuItem
        '
        Me.CDJMonthlySpreadsheetToolStripMenuItem.Name = "CDJMonthlySpreadsheetToolStripMenuItem"
        Me.CDJMonthlySpreadsheetToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.CDJMonthlySpreadsheetToolStripMenuItem.Text = "CDJ - Monthly (Spreadsheet)"
        '
        'JournalListingToolStripMenuItem
        '
        Me.JournalListingToolStripMenuItem.Name = "JournalListingToolStripMenuItem"
        Me.JournalListingToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.JournalListingToolStripMenuItem.Text = "Journal Listings"
        Me.JournalListingToolStripMenuItem.Visible = False
        '
        'GeneralJournalToolStripMenuItem1
        '
        Me.GeneralJournalToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DailyToolStripMenuItem1, Me.MonthlyToolStripMenuItem1, Me.GJMonthlySpreadsheetToolStripMenuItem})
        Me.GeneralJournalToolStripMenuItem1.Name = "GeneralJournalToolStripMenuItem1"
        Me.GeneralJournalToolStripMenuItem1.Size = New System.Drawing.Size(284, 22)
        Me.GeneralJournalToolStripMenuItem1.Text = "General Journal"
        Me.GeneralJournalToolStripMenuItem1.Visible = False
        '
        'DailyToolStripMenuItem1
        '
        Me.DailyToolStripMenuItem1.Name = "DailyToolStripMenuItem1"
        Me.DailyToolStripMenuItem1.Size = New System.Drawing.Size(220, 22)
        Me.DailyToolStripMenuItem1.Text = "GJ - Daily"
        '
        'MonthlyToolStripMenuItem1
        '
        Me.MonthlyToolStripMenuItem1.Name = "MonthlyToolStripMenuItem1"
        Me.MonthlyToolStripMenuItem1.Size = New System.Drawing.Size(220, 22)
        Me.MonthlyToolStripMenuItem1.Text = "GJ - Monthly (Summarized)"
        '
        'GJMonthlySpreadsheetToolStripMenuItem
        '
        Me.GJMonthlySpreadsheetToolStripMenuItem.Name = "GJMonthlySpreadsheetToolStripMenuItem"
        Me.GJMonthlySpreadsheetToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.GJMonthlySpreadsheetToolStripMenuItem.Text = "GJ - Monthly (Spreadsheet)"
        '
        'PurchaseOderToolStripMenuItem
        '
        Me.PurchaseOderToolStripMenuItem.Name = "PurchaseOderToolStripMenuItem"
        Me.PurchaseOderToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.PurchaseOderToolStripMenuItem.Text = "Purchase Journal"
        '
        'PurchaseReturnToolStripMenuItem
        '
        Me.PurchaseReturnToolStripMenuItem.Name = "PurchaseReturnToolStripMenuItem"
        Me.PurchaseReturnToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.PurchaseReturnToolStripMenuItem.Text = "Purchase Returns"
        Me.PurchaseReturnToolStripMenuItem.Visible = False
        '
        'SalesInvoiceToolStripMenuItem
        '
        Me.SalesInvoiceToolStripMenuItem.Name = "SalesInvoiceToolStripMenuItem"
        Me.SalesInvoiceToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.SalesInvoiceToolStripMenuItem.Text = "Sales Journal"
        '
        'SalesReturnToolStripMenuItem
        '
        Me.SalesReturnToolStripMenuItem.Name = "SalesReturnToolStripMenuItem"
        Me.SalesReturnToolStripMenuItem.Size = New System.Drawing.Size(284, 22)
        Me.SalesReturnToolStripMenuItem.Text = "Sales Returns"
        Me.SalesReturnToolStripMenuItem.Visible = False
        '
        'FinancialStatementsToolStripMenuItem
        '
        Me.FinancialStatementsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BalanceSheetToolStripMenuItem, Me.IncomeStatementToolStripMenuItem, Me.IncomeStatementDrillDownToolStripMenuItem, Me.IncomeStatementPrevYearComparisonToolStripMenuItem, Me.IncomeStatementWithStatutoryAllocationsToolStripMenuItem, Me.BalanceSheetToolStripMenuItem1, Me.IncomeStatementToolStripMenuItem1, Me.GeneralLedgerToolStripMenuItem2})
        Me.FinancialStatementsToolStripMenuItem.Name = "FinancialStatementsToolStripMenuItem"
        Me.FinancialStatementsToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.FinancialStatementsToolStripMenuItem.Text = "Financial Statements"
        Me.FinancialStatementsToolStripMenuItem.Visible = False
        '
        'BalanceSheetToolStripMenuItem
        '
        Me.BalanceSheetToolStripMenuItem.Name = "BalanceSheetToolStripMenuItem"
        Me.BalanceSheetToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.BalanceSheetToolStripMenuItem.Text = "Balance Sheet Standard"
        Me.BalanceSheetToolStripMenuItem.Visible = False
        '
        'IncomeStatementToolStripMenuItem
        '
        Me.IncomeStatementToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IncomeStatementToolStripMenuItem.Name = "IncomeStatementToolStripMenuItem"
        Me.IncomeStatementToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.IncomeStatementToolStripMenuItem.Text = "Income Statement Standard"
        Me.IncomeStatementToolStripMenuItem.Visible = False
        '
        'IncomeStatementDrillDownToolStripMenuItem
        '
        Me.IncomeStatementDrillDownToolStripMenuItem.Name = "IncomeStatementDrillDownToolStripMenuItem"
        Me.IncomeStatementDrillDownToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.IncomeStatementDrillDownToolStripMenuItem.Text = "Income Statement (Drill Down)"
        Me.IncomeStatementDrillDownToolStripMenuItem.Visible = False
        '
        'IncomeStatementPrevYearComparisonToolStripMenuItem
        '
        Me.IncomeStatementPrevYearComparisonToolStripMenuItem.Name = "IncomeStatementPrevYearComparisonToolStripMenuItem"
        Me.IncomeStatementPrevYearComparisonToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.IncomeStatementPrevYearComparisonToolStripMenuItem.Text = "Income Statement Prev Year Comparison"
        Me.IncomeStatementPrevYearComparisonToolStripMenuItem.Visible = False
        '
        'IncomeStatementWithStatutoryAllocationsToolStripMenuItem
        '
        Me.IncomeStatementWithStatutoryAllocationsToolStripMenuItem.Name = "IncomeStatementWithStatutoryAllocationsToolStripMenuItem"
        Me.IncomeStatementWithStatutoryAllocationsToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.IncomeStatementWithStatutoryAllocationsToolStripMenuItem.Text = "Income Statement with Statutory Allocations"
        Me.IncomeStatementWithStatutoryAllocationsToolStripMenuItem.Visible = False
        '
        'BalanceSheetToolStripMenuItem1
        '
        Me.BalanceSheetToolStripMenuItem1.Name = "BalanceSheetToolStripMenuItem1"
        Me.BalanceSheetToolStripMenuItem1.Size = New System.Drawing.Size(316, 22)
        Me.BalanceSheetToolStripMenuItem1.Text = "Statement of Financial Condition"
        Me.BalanceSheetToolStripMenuItem1.Visible = False
        '
        'IncomeStatementToolStripMenuItem1
        '
        Me.IncomeStatementToolStripMenuItem1.Name = "IncomeStatementToolStripMenuItem1"
        Me.IncomeStatementToolStripMenuItem1.Size = New System.Drawing.Size(316, 22)
        Me.IncomeStatementToolStripMenuItem1.Text = "Statement of Operation"
        Me.IncomeStatementToolStripMenuItem1.Visible = False
        '
        'GeneralLedgerToolStripMenuItem2
        '
        Me.GeneralLedgerToolStripMenuItem2.Name = "GeneralLedgerToolStripMenuItem2"
        Me.GeneralLedgerToolStripMenuItem2.Size = New System.Drawing.Size(316, 22)
        Me.GeneralLedgerToolStripMenuItem2.Text = "General Ledger"
        Me.GeneralLedgerToolStripMenuItem2.Visible = False
        '
        'TrialBalanceToolStripMenuItem
        '
        Me.TrialBalanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WorkingTrialBalanceToolStripMenuItem, Me.TrialBalanceToolStripMenuItem2})
        Me.TrialBalanceToolStripMenuItem.Name = "TrialBalanceToolStripMenuItem"
        Me.TrialBalanceToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.TrialBalanceToolStripMenuItem.Text = "Trial Balance"
        Me.TrialBalanceToolStripMenuItem.Visible = False
        '
        'WorkingTrialBalanceToolStripMenuItem
        '
        Me.WorkingTrialBalanceToolStripMenuItem.Name = "WorkingTrialBalanceToolStripMenuItem"
        Me.WorkingTrialBalanceToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.WorkingTrialBalanceToolStripMenuItem.Text = "Working Trial Balance"
        Me.WorkingTrialBalanceToolStripMenuItem.Visible = False
        '
        'TrialBalanceToolStripMenuItem2
        '
        Me.TrialBalanceToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DailyToolStripMenuItem10, Me.MonthlyToolStripMenuItem9})
        Me.TrialBalanceToolStripMenuItem2.Name = "TrialBalanceToolStripMenuItem2"
        Me.TrialBalanceToolStripMenuItem2.Size = New System.Drawing.Size(220, 22)
        Me.TrialBalanceToolStripMenuItem2.Text = "Trial Balance - Single Form"
        Me.TrialBalanceToolStripMenuItem2.Visible = False
        '
        'DailyToolStripMenuItem10
        '
        Me.DailyToolStripMenuItem10.Name = "DailyToolStripMenuItem10"
        Me.DailyToolStripMenuItem10.Size = New System.Drawing.Size(120, 22)
        Me.DailyToolStripMenuItem10.Text = "Daily"
        '
        'MonthlyToolStripMenuItem9
        '
        Me.MonthlyToolStripMenuItem9.Name = "MonthlyToolStripMenuItem9"
        Me.MonthlyToolStripMenuItem9.Size = New System.Drawing.Size(120, 22)
        Me.MonthlyToolStripMenuItem9.Text = "Monthly"
        '
        'CashPositionReportToolStripMenuItem
        '
        Me.CashPositionReportToolStripMenuItem.Name = "CashPositionReportToolStripMenuItem"
        Me.CashPositionReportToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.CashPositionReportToolStripMenuItem.Text = "Cash Position Report"
        Me.CashPositionReportToolStripMenuItem.Visible = False
        '
        'CashFlowStatementToolStripMenuItem
        '
        Me.CashFlowStatementToolStripMenuItem.Name = "CashFlowStatementToolStripMenuItem"
        Me.CashFlowStatementToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.CashFlowStatementToolStripMenuItem.Text = "Cash Flow Statement"
        Me.CashFlowStatementToolStripMenuItem.Visible = False
        '
        'AccountBalanceToolStripMenuItem
        '
        Me.AccountBalanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebitAccountsToolStripMenuItem, Me.CreditAccountsToolStripMenuItem})
        Me.AccountBalanceToolStripMenuItem.Name = "AccountBalanceToolStripMenuItem"
        Me.AccountBalanceToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.AccountBalanceToolStripMenuItem.Text = "Account Balance"
        Me.AccountBalanceToolStripMenuItem.Visible = False
        '
        'DebitAccountsToolStripMenuItem
        '
        Me.DebitAccountsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountsReceivableToolStripMenuItem, Me.AgingOfAccountReceivablesToolStripMenuItem, Me.AgingOfAdvancesToOEToolStripMenuItem})
        Me.DebitAccountsToolStripMenuItem.Name = "DebitAccountsToolStripMenuItem"
        Me.DebitAccountsToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.DebitAccountsToolStripMenuItem.Text = "Debit Accounts"
        '
        'AccountsReceivableToolStripMenuItem
        '
        Me.AccountsReceivableToolStripMenuItem.Name = "AccountsReceivableToolStripMenuItem"
        Me.AccountsReceivableToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.AccountsReceivableToolStripMenuItem.Text = "All Debit Accounts with SLs"
        '
        'AgingOfAccountReceivablesToolStripMenuItem
        '
        Me.AgingOfAccountReceivablesToolStripMenuItem.Name = "AgingOfAccountReceivablesToolStripMenuItem"
        Me.AgingOfAccountReceivablesToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.AgingOfAccountReceivablesToolStripMenuItem.Text = "Aging of Account Receivables"
        '
        'AgingOfAdvancesToOEToolStripMenuItem
        '
        Me.AgingOfAdvancesToOEToolStripMenuItem.Name = "AgingOfAdvancesToOEToolStripMenuItem"
        Me.AgingOfAdvancesToOEToolStripMenuItem.Size = New System.Drawing.Size(233, 22)
        Me.AgingOfAdvancesToOEToolStripMenuItem.Text = "Aging of Advances to O && E"
        '
        'CreditAccountsToolStripMenuItem
        '
        Me.CreditAccountsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountsPayableToolStripMenuItem, Me.AgingOfAccountsPayableToolStripMenuItem})
        Me.CreditAccountsToolStripMenuItem.Name = "CreditAccountsToolStripMenuItem"
        Me.CreditAccountsToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.CreditAccountsToolStripMenuItem.Text = "Credit Accounts"
        '
        'AccountsPayableToolStripMenuItem
        '
        Me.AccountsPayableToolStripMenuItem.Name = "AccountsPayableToolStripMenuItem"
        Me.AccountsPayableToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.AccountsPayableToolStripMenuItem.Text = "All Credit Accounts with SLs"
        '
        'AgingOfAccountsPayableToolStripMenuItem
        '
        Me.AgingOfAccountsPayableToolStripMenuItem.Name = "AgingOfAccountsPayableToolStripMenuItem"
        Me.AgingOfAccountsPayableToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.AgingOfAccountsPayableToolStripMenuItem.Text = "Aging of Accounts Payable"
        '
        'LoansReceivableToolStripMenuItem
        '
        Me.LoansReceivableToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ScheduleOfLRByCategoryToolStripMenuItem, Me.ScheduleOfLRAllTypesToolStripMenuItem, Me.AgingOfLRByCategoryToolStripMenuItem})
        Me.LoansReceivableToolStripMenuItem.Name = "LoansReceivableToolStripMenuItem"
        Me.LoansReceivableToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.LoansReceivableToolStripMenuItem.Text = "Loans Receivable"
        Me.LoansReceivableToolStripMenuItem.Visible = False
        '
        'ScheduleOfLRByCategoryToolStripMenuItem
        '
        Me.ScheduleOfLRByCategoryToolStripMenuItem.Name = "ScheduleOfLRByCategoryToolStripMenuItem"
        Me.ScheduleOfLRByCategoryToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.ScheduleOfLRByCategoryToolStripMenuItem.Text = "By Category"
        '
        'ScheduleOfLRAllTypesToolStripMenuItem
        '
        Me.ScheduleOfLRAllTypesToolStripMenuItem.Name = "ScheduleOfLRAllTypesToolStripMenuItem"
        Me.ScheduleOfLRAllTypesToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.ScheduleOfLRAllTypesToolStripMenuItem.Text = "All Types"
        '
        'AgingOfLRByCategoryToolStripMenuItem
        '
        Me.AgingOfLRByCategoryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ByCategoryToolStripMenuItem, Me.AllTypesToolStripMenuItem})
        Me.AgingOfLRByCategoryToolStripMenuItem.Name = "AgingOfLRByCategoryToolStripMenuItem"
        Me.AgingOfLRByCategoryToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.AgingOfLRByCategoryToolStripMenuItem.Text = "Aging of Loans Receivable"
        '
        'ByCategoryToolStripMenuItem
        '
        Me.ByCategoryToolStripMenuItem.Name = "ByCategoryToolStripMenuItem"
        Me.ByCategoryToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.ByCategoryToolStripMenuItem.Text = "By Category"
        '
        'AllTypesToolStripMenuItem
        '
        Me.AllTypesToolStripMenuItem.Name = "AllTypesToolStripMenuItem"
        Me.AllTypesToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.AllTypesToolStripMenuItem.Text = "All Types"
        '
        'AccountAnalysisToolStripMenuItem
        '
        Me.AccountAnalysisToolStripMenuItem.Name = "AccountAnalysisToolStripMenuItem"
        Me.AccountAnalysisToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.AccountAnalysisToolStripMenuItem.Text = "Account Analysis"
        Me.AccountAnalysisToolStripMenuItem.Visible = False
        '
        'BankReconciliationToolStripMenuItem
        '
        Me.BankReconciliationToolStripMenuItem.Name = "BankReconciliationToolStripMenuItem"
        Me.BankReconciliationToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.BankReconciliationToolStripMenuItem.Text = "Bank Reconciliation"
        Me.BankReconciliationToolStripMenuItem.Visible = False
        '
        'AuditTrailToolStripMenuItem
        '
        Me.AuditTrailToolStripMenuItem.Name = "AuditTrailToolStripMenuItem"
        Me.AuditTrailToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.AuditTrailToolStripMenuItem.Text = "Audit Trail"
        Me.AuditTrailToolStripMenuItem.Visible = False
        '
        'PESOReportsToolStripMenuItem
        '
        Me.PESOReportsToolStripMenuItem.Name = "PESOReportsToolStripMenuItem"
        Me.PESOReportsToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.PESOReportsToolStripMenuItem.Text = "PESO Reports"
        Me.PESOReportsToolStripMenuItem.Visible = False
        '
        'ms_main_accountschedules
        '
        Me.ms_main_accountschedules.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebitsToolStripMenuItem, Me.CreditsToolStripMenuItem, Me.LoanReceivableToolStripMenuItem, Me.AccountAnalysisToolStripMenuItem1, Me.UnearnedInterestLapsingToolStripMenuItem})
        Me.ms_main_accountschedules.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_accountschedules.Image = Global.CSAcctg.My.Resources.Resources.Calendar
        Me.ms_main_accountschedules.Name = "ms_main_accountschedules"
        Me.ms_main_accountschedules.Size = New System.Drawing.Size(142, 20)
        Me.ms_main_accountschedules.Text = "Account Schedules"
        Me.ms_main_accountschedules.Visible = False
        '
        'DebitsToolStripMenuItem
        '
        Me.DebitsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebitAccountsToolStripMenuItem1, Me.AgingOfAccountsReceivableToolStripMenuItem, Me.AgingOfToolStripMenuItem})
        Me.DebitsToolStripMenuItem.Name = "DebitsToolStripMenuItem"
        Me.DebitsToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.DebitsToolStripMenuItem.Text = "Debits"
        Me.DebitsToolStripMenuItem.Visible = False
        '
        'DebitAccountsToolStripMenuItem1
        '
        Me.DebitAccountsToolStripMenuItem1.Name = "DebitAccountsToolStripMenuItem1"
        Me.DebitAccountsToolStripMenuItem1.Size = New System.Drawing.Size(242, 22)
        Me.DebitAccountsToolStripMenuItem1.Text = "Debit Accounts"
        '
        'AgingOfAccountsReceivableToolStripMenuItem
        '
        Me.AgingOfAccountsReceivableToolStripMenuItem.Name = "AgingOfAccountsReceivableToolStripMenuItem"
        Me.AgingOfAccountsReceivableToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AgingOfAccountsReceivableToolStripMenuItem.Text = "Aging of Accounts Receivable"
        '
        'AgingOfToolStripMenuItem
        '
        Me.AgingOfToolStripMenuItem.Name = "AgingOfToolStripMenuItem"
        Me.AgingOfToolStripMenuItem.Size = New System.Drawing.Size(242, 22)
        Me.AgingOfToolStripMenuItem.Text = "Aging of Advances to O && E"
        '
        'CreditsToolStripMenuItem
        '
        Me.CreditsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CreditAcountToolStripMenuItem, Me.AgingOfAccountsPayableToolStripMenuItem1})
        Me.CreditsToolStripMenuItem.Name = "CreditsToolStripMenuItem"
        Me.CreditsToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.CreditsToolStripMenuItem.Text = "Credits"
        Me.CreditsToolStripMenuItem.Visible = False
        '
        'CreditAcountToolStripMenuItem
        '
        Me.CreditAcountToolStripMenuItem.Name = "CreditAcountToolStripMenuItem"
        Me.CreditAcountToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.CreditAcountToolStripMenuItem.Text = "Credit Account"
        '
        'AgingOfAccountsPayableToolStripMenuItem1
        '
        Me.AgingOfAccountsPayableToolStripMenuItem1.Name = "AgingOfAccountsPayableToolStripMenuItem1"
        Me.AgingOfAccountsPayableToolStripMenuItem1.Size = New System.Drawing.Size(225, 22)
        Me.AgingOfAccountsPayableToolStripMenuItem1.Text = "Aging of Accounts Payable"
        '
        'LoanReceivableToolStripMenuItem
        '
        Me.LoanReceivableToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ByCategoryToolStripMenuItem1, Me.AllTypesToolStripMenuItem1, Me.AgingOfLoansReceivableToolStripMenuItem})
        Me.LoanReceivableToolStripMenuItem.Name = "LoanReceivableToolStripMenuItem"
        Me.LoanReceivableToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.LoanReceivableToolStripMenuItem.Text = "Loan Receivable"
        Me.LoanReceivableToolStripMenuItem.Visible = False
        '
        'ByCategoryToolStripMenuItem1
        '
        Me.ByCategoryToolStripMenuItem1.Name = "ByCategoryToolStripMenuItem1"
        Me.ByCategoryToolStripMenuItem1.Size = New System.Drawing.Size(224, 22)
        Me.ByCategoryToolStripMenuItem1.Text = "By Category"
        '
        'AllTypesToolStripMenuItem1
        '
        Me.AllTypesToolStripMenuItem1.Name = "AllTypesToolStripMenuItem1"
        Me.AllTypesToolStripMenuItem1.Size = New System.Drawing.Size(224, 22)
        Me.AllTypesToolStripMenuItem1.Text = "All Types"
        '
        'AgingOfLoansReceivableToolStripMenuItem
        '
        Me.AgingOfLoansReceivableToolStripMenuItem.Name = "AgingOfLoansReceivableToolStripMenuItem"
        Me.AgingOfLoansReceivableToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.AgingOfLoansReceivableToolStripMenuItem.Text = "Aging of Loans Receivable"
        '
        'AccountAnalysisToolStripMenuItem1
        '
        Me.AccountAnalysisToolStripMenuItem1.Name = "AccountAnalysisToolStripMenuItem1"
        Me.AccountAnalysisToolStripMenuItem1.Size = New System.Drawing.Size(225, 22)
        Me.AccountAnalysisToolStripMenuItem1.Text = "Account Analysis"
        Me.AccountAnalysisToolStripMenuItem1.Visible = False
        '
        'UnearnedInterestLapsingToolStripMenuItem
        '
        Me.UnearnedInterestLapsingToolStripMenuItem.Name = "UnearnedInterestLapsingToolStripMenuItem"
        Me.UnearnedInterestLapsingToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.UnearnedInterestLapsingToolStripMenuItem.Text = "Unearned Interest Lapsing"
        Me.UnearnedInterestLapsingToolStripMenuItem.Visible = False
        '
        'ms_main_billing
        '
        Me.ms_main_billing.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BillingReportsToolStripMenuItem, Me.BillingListToolStripMenuItem, Me.CollectionsIntegratorToolStripMenuItem})
        Me.ms_main_billing.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_billing.Image = Global.CSAcctg.My.Resources.Resources.Accounting
        Me.ms_main_billing.Name = "ms_main_billing"
        Me.ms_main_billing.Size = New System.Drawing.Size(69, 20)
        Me.ms_main_billing.Text = "Billing"
        Me.ms_main_billing.Visible = False
        '
        'BillingReportsToolStripMenuItem
        '
        Me.BillingReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ByGroupToolStripMenuItem, Me.AllToolStripMenuItem})
        Me.BillingReportsToolStripMenuItem.Name = "BillingReportsToolStripMenuItem"
        Me.BillingReportsToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.BillingReportsToolStripMenuItem.Text = "Billing Reports"
        Me.BillingReportsToolStripMenuItem.Visible = False
        '
        'ByGroupToolStripMenuItem
        '
        Me.ByGroupToolStripMenuItem.Name = "ByGroupToolStripMenuItem"
        Me.ByGroupToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.ByGroupToolStripMenuItem.Text = "By Group"
        '
        'AllToolStripMenuItem
        '
        Me.AllToolStripMenuItem.Name = "AllToolStripMenuItem"
        Me.AllToolStripMenuItem.Size = New System.Drawing.Size(128, 22)
        Me.AllToolStripMenuItem.Text = "All"
        '
        'BillingListToolStripMenuItem
        '
        Me.BillingListToolStripMenuItem.Name = "BillingListToolStripMenuItem"
        Me.BillingListToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.BillingListToolStripMenuItem.Text = "Billing Summary"
        Me.BillingListToolStripMenuItem.Visible = False
        '
        'CollectionsIntegratorToolStripMenuItem
        '
        Me.CollectionsIntegratorToolStripMenuItem.Name = "CollectionsIntegratorToolStripMenuItem"
        Me.CollectionsIntegratorToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.CollectionsIntegratorToolStripMenuItem.Text = "Collections Integrator"
        Me.CollectionsIntegratorToolStripMenuItem.Visible = False
        '
        'ms_main_masterfile
        '
        Me.ms_main_masterfile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChartOfAccountsToolStripMenuItem, Me.DocumentTypeSettingsToolStripMenuItem, Me.DocumentTypeToolStripMenuItem, Me.CheckIssuanceToolStripMenuItem, Me.ms_master, Me.ms_master_pricelevel, Me.ms_master_billingrate, Me.ms_master_tax, Me.ms_master_salesrep, Me.ClassToolStripMenuItem, Me.ToolStripSeparator4, Me.ms_master_customertype, Me.ms_master_vendortype, Me.ToolStripSeparator5, Me.ms_master_terms, Me.ms_master_paymentMethod, Me.ToolStripSeparator11, Me.StatutoryAllocationsToolStripMenuItem, Me.ms_master_other, Me.CostCenterToolStripMenuItem, Me.ColumnInterfaceToolStripMenuItem, Me.RecurringEntrySetupToolStripMenuItem, Me.statutoryMaster, Me.DynamicColumnToolStripMenuItem})
        Me.ms_main_masterfile.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_masterfile.Image = CType(resources.GetObject("ms_main_masterfile.Image"), System.Drawing.Image)
        Me.ms_main_masterfile.Name = "ms_main_masterfile"
        Me.ms_main_masterfile.Size = New System.Drawing.Size(75, 20)
        Me.ms_main_masterfile.Text = "&Set Ups"
        Me.ms_main_masterfile.Visible = False
        '
        'ChartOfAccountsToolStripMenuItem
        '
        Me.ChartOfAccountsToolStripMenuItem.Image = CType(resources.GetObject("ChartOfAccountsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ChartOfAccountsToolStripMenuItem.Name = "ChartOfAccountsToolStripMenuItem"
        Me.ChartOfAccountsToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.ChartOfAccountsToolStripMenuItem.Text = "Chart Of Accounts"
        Me.ChartOfAccountsToolStripMenuItem.Visible = False
        '
        'DocumentTypeSettingsToolStripMenuItem
        '
        Me.DocumentTypeSettingsToolStripMenuItem.Name = "DocumentTypeSettingsToolStripMenuItem"
        Me.DocumentTypeSettingsToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.DocumentTypeSettingsToolStripMenuItem.Text = "Document Type Settings"
        Me.DocumentTypeSettingsToolStripMenuItem.Visible = False
        '
        'DocumentTypeToolStripMenuItem
        '
        Me.DocumentTypeToolStripMenuItem.Name = "DocumentTypeToolStripMenuItem"
        Me.DocumentTypeToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.DocumentTypeToolStripMenuItem.Text = "Document Number Issuance"
        Me.DocumentTypeToolStripMenuItem.Visible = False
        '
        'CheckIssuanceToolStripMenuItem
        '
        Me.CheckIssuanceToolStripMenuItem.Name = "CheckIssuanceToolStripMenuItem"
        Me.CheckIssuanceToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.CheckIssuanceToolStripMenuItem.Text = "Check Issuance"
        Me.CheckIssuanceToolStripMenuItem.Visible = False
        '
        'ms_master
        '
        Me.ms_master.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_master_itemmaster, Me.ms_master_itemGroup})
        Me.ms_master.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master.Name = "ms_master"
        Me.ms_master.Size = New System.Drawing.Size(227, 22)
        Me.ms_master.Text = "Items"
        Me.ms_master.Visible = False
        '
        'ms_master_itemmaster
        '
        Me.ms_master_itemmaster.Name = "ms_master_itemmaster"
        Me.ms_master_itemmaster.Size = New System.Drawing.Size(140, 22)
        Me.ms_master_itemmaster.Text = "Item Master"
        '
        'ms_master_itemGroup
        '
        Me.ms_master_itemGroup.Name = "ms_master_itemGroup"
        Me.ms_master_itemGroup.Size = New System.Drawing.Size(140, 22)
        Me.ms_master_itemGroup.Text = "Item Group"
        '
        'ms_master_pricelevel
        '
        Me.ms_master_pricelevel.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_pricelevel.Name = "ms_master_pricelevel"
        Me.ms_master_pricelevel.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_pricelevel.Text = "Price Level"
        Me.ms_master_pricelevel.Visible = False
        '
        'ms_master_billingrate
        '
        Me.ms_master_billingrate.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_billingrate.Name = "ms_master_billingrate"
        Me.ms_master_billingrate.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_billingrate.Text = "Billing Rate"
        Me.ms_master_billingrate.Visible = False
        '
        'ms_master_tax
        '
        Me.ms_master_tax.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_master_salestax, Me.ms_master_salestaxcode})
        Me.ms_master_tax.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_tax.Name = "ms_master_tax"
        Me.ms_master_tax.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_tax.Text = "Sales Tax"
        Me.ms_master_tax.Visible = False
        '
        'ms_master_salestax
        '
        Me.ms_master_salestax.Name = "ms_master_salestax"
        Me.ms_master_salestax.Size = New System.Drawing.Size(154, 22)
        Me.ms_master_salestax.Text = "Sales Tax"
        Me.ms_master_salestax.Visible = False
        '
        'ms_master_salestaxcode
        '
        Me.ms_master_salestaxcode.Name = "ms_master_salestaxcode"
        Me.ms_master_salestaxcode.Size = New System.Drawing.Size(154, 22)
        Me.ms_master_salestaxcode.Text = "Sales Tax Code"
        Me.ms_master_salestaxcode.Visible = False
        '
        'ms_master_salesrep
        '
        Me.ms_master_salesrep.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_salesrep.Name = "ms_master_salesrep"
        Me.ms_master_salesrep.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_salesrep.Text = "Sales Rep"
        Me.ms_master_salesrep.Visible = False
        '
        'ClassToolStripMenuItem
        '
        Me.ClassToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ClassToolStripMenuItem.Name = "ClassToolStripMenuItem"
        Me.ClassToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.ClassToolStripMenuItem.Text = "Classes"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(224, 6)
        Me.ToolStripSeparator4.Visible = False
        '
        'ms_master_customertype
        '
        Me.ms_master_customertype.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_customertype.Name = "ms_master_customertype"
        Me.ms_master_customertype.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_customertype.Text = "Customer Type"
        Me.ms_master_customertype.Visible = False
        '
        'ms_master_vendortype
        '
        Me.ms_master_vendortype.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_vendortype.Name = "ms_master_vendortype"
        Me.ms_master_vendortype.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_vendortype.Text = "Supplier Type"
        Me.ms_master_vendortype.Visible = False
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(224, 6)
        Me.ToolStripSeparator5.Visible = False
        '
        'ms_master_terms
        '
        Me.ms_master_terms.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_terms.Name = "ms_master_terms"
        Me.ms_master_terms.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_terms.Text = "Terms"
        Me.ms_master_terms.Visible = False
        '
        'ms_master_paymentMethod
        '
        Me.ms_master_paymentMethod.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_paymentMethod.Name = "ms_master_paymentMethod"
        Me.ms_master_paymentMethod.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_paymentMethod.Text = "Payment Method"
        Me.ms_master_paymentMethod.Visible = False
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(224, 6)
        Me.ToolStripSeparator11.Visible = False
        '
        'StatutoryAllocationsToolStripMenuItem
        '
        Me.StatutoryAllocationsToolStripMenuItem.Name = "StatutoryAllocationsToolStripMenuItem"
        Me.StatutoryAllocationsToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.StatutoryAllocationsToolStripMenuItem.Text = "Statutory Allocations"
        Me.StatutoryAllocationsToolStripMenuItem.Visible = False
        '
        'ms_master_other
        '
        Me.ms_master_other.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_master_other.Name = "ms_master_other"
        Me.ms_master_other.Size = New System.Drawing.Size(227, 22)
        Me.ms_master_other.Text = "Other Name"
        Me.ms_master_other.Visible = False
        '
        'CostCenterToolStripMenuItem
        '
        Me.CostCenterToolStripMenuItem.Name = "CostCenterToolStripMenuItem"
        Me.CostCenterToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.CostCenterToolStripMenuItem.Text = "Cost Center"
        Me.CostCenterToolStripMenuItem.Visible = False
        '
        'ColumnInterfaceToolStripMenuItem
        '
        Me.ColumnInterfaceToolStripMenuItem.Name = "ColumnInterfaceToolStripMenuItem"
        Me.ColumnInterfaceToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.ColumnInterfaceToolStripMenuItem.Text = "Report Column Setup"
        Me.ColumnInterfaceToolStripMenuItem.Visible = False
        '
        'RecurringEntrySetupToolStripMenuItem
        '
        Me.RecurringEntrySetupToolStripMenuItem.Name = "RecurringEntrySetupToolStripMenuItem"
        Me.RecurringEntrySetupToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.RecurringEntrySetupToolStripMenuItem.Text = "Recurring Entry Setup"
        '
        'statutoryMaster
        '
        Me.statutoryMaster.Name = "statutoryMaster"
        Me.statutoryMaster.Size = New System.Drawing.Size(227, 22)
        Me.statutoryMaster.Text = "Statutory Master"
        '
        'ms_main_computation
        '
        Me.ms_main_computation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SavingsInterestToolStripMenuItem, Me.DividendToolStripMenuItem, Me.PatronageRefundToolStripMenuItem, Me.BereavementToolStripMenuItem, Me.StatutoryFundToolStripMenuItem})
        Me.ms_main_computation.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_computation.Image = Global.CSAcctg.My.Resources.Resources.Calculator
        Me.ms_main_computation.Name = "ms_main_computation"
        Me.ms_main_computation.Size = New System.Drawing.Size(114, 20)
        Me.ms_main_computation.Text = "Computations"
        Me.ms_main_computation.Visible = False
        '
        'SavingsInterestToolStripMenuItem
        '
        Me.SavingsInterestToolStripMenuItem.Name = "SavingsInterestToolStripMenuItem"
        Me.SavingsInterestToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.SavingsInterestToolStripMenuItem.Text = "Savings Interest"
        Me.SavingsInterestToolStripMenuItem.Visible = False
        '
        'DividendToolStripMenuItem
        '
        Me.DividendToolStripMenuItem.Name = "DividendToolStripMenuItem"
        Me.DividendToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.DividendToolStripMenuItem.Text = "Dividend"
        Me.DividendToolStripMenuItem.Visible = False
        '
        'PatronageRefundToolStripMenuItem
        '
        Me.PatronageRefundToolStripMenuItem.Name = "PatronageRefundToolStripMenuItem"
        Me.PatronageRefundToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.PatronageRefundToolStripMenuItem.Text = "Patronage Refund"
        Me.PatronageRefundToolStripMenuItem.Visible = False
        '
        'BereavementToolStripMenuItem
        '
        Me.BereavementToolStripMenuItem.Name = "BereavementToolStripMenuItem"
        Me.BereavementToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.BereavementToolStripMenuItem.Text = "Bereavement"
        Me.BereavementToolStripMenuItem.Visible = False
        '
        'StatutoryFundToolStripMenuItem
        '
        Me.StatutoryFundToolStripMenuItem.Name = "StatutoryFundToolStripMenuItem"
        Me.StatutoryFundToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.StatutoryFundToolStripMenuItem.Text = "Statutory Fund"
        '
        'ms_main_database
        '
        Me.ms_main_database.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BackUpDatabaseToolStripMenuItem1, Me.ResoreToolStripMenuItem, Me.DataMigrationToolStripMenuItem})
        Me.ms_main_database.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_database.Image = Global.CSAcctg.My.Resources.Resources.Database
        Me.ms_main_database.Name = "ms_main_database"
        Me.ms_main_database.Size = New System.Drawing.Size(89, 20)
        Me.ms_main_database.Text = "Database"
        Me.ms_main_database.Visible = False
        '
        'BackUpDatabaseToolStripMenuItem1
        '
        Me.BackUpDatabaseToolStripMenuItem1.Name = "BackUpDatabaseToolStripMenuItem1"
        Me.BackUpDatabaseToolStripMenuItem1.Size = New System.Drawing.Size(191, 22)
        Me.BackUpDatabaseToolStripMenuItem1.Text = "Back up Database"
        Me.BackUpDatabaseToolStripMenuItem1.Visible = False
        '
        'ResoreToolStripMenuItem
        '
        Me.ResoreToolStripMenuItem.Name = "ResoreToolStripMenuItem"
        Me.ResoreToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.ResoreToolStripMenuItem.Text = "Restore Database"
        Me.ResoreToolStripMenuItem.Visible = False
        '
        'DataMigrationToolStripMenuItem
        '
        Me.DataMigrationToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountRegisterToolStripMenuItem2, Me.AccountForwardBalanceToolStripMenuItem1, Me.GenerateTemplateToolStripMenuItem1, Me.ResetForwardBalanceToolStripMenuItem, Me.ClientUploadToolStripMenuItem})
        Me.DataMigrationToolStripMenuItem.Name = "DataMigrationToolStripMenuItem"
        Me.DataMigrationToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.DataMigrationToolStripMenuItem.Text = "Data Migration Tools"
        Me.DataMigrationToolStripMenuItem.Visible = False
        '
        'AccountRegisterToolStripMenuItem2
        '
        Me.AccountRegisterToolStripMenuItem2.Name = "AccountRegisterToolStripMenuItem2"
        Me.AccountRegisterToolStripMenuItem2.Size = New System.Drawing.Size(218, 22)
        Me.AccountRegisterToolStripMenuItem2.Text = "Account Register"
        Me.AccountRegisterToolStripMenuItem2.Visible = False
        '
        'AccountForwardBalanceToolStripMenuItem1
        '
        Me.AccountForwardBalanceToolStripMenuItem1.Name = "AccountForwardBalanceToolStripMenuItem1"
        Me.AccountForwardBalanceToolStripMenuItem1.Size = New System.Drawing.Size(218, 22)
        Me.AccountForwardBalanceToolStripMenuItem1.Text = "Account Forward Balance"
        Me.AccountForwardBalanceToolStripMenuItem1.Visible = False
        '
        'GenerateTemplateToolStripMenuItem1
        '
        Me.GenerateTemplateToolStripMenuItem1.Name = "GenerateTemplateToolStripMenuItem1"
        Me.GenerateTemplateToolStripMenuItem1.Size = New System.Drawing.Size(218, 22)
        Me.GenerateTemplateToolStripMenuItem1.Text = "Generate Template"
        Me.GenerateTemplateToolStripMenuItem1.Visible = False
        '
        'ResetForwardBalanceToolStripMenuItem
        '
        Me.ResetForwardBalanceToolStripMenuItem.Name = "ResetForwardBalanceToolStripMenuItem"
        Me.ResetForwardBalanceToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.ResetForwardBalanceToolStripMenuItem.Text = "Reset Forward Balance"
        Me.ResetForwardBalanceToolStripMenuItem.Visible = False
        '
        'ClientUploadToolStripMenuItem
        '
        Me.ClientUploadToolStripMenuItem.Name = "ClientUploadToolStripMenuItem"
        Me.ClientUploadToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.ClientUploadToolStripMenuItem.Text = "Client Upload"
        Me.ClientUploadToolStripMenuItem.Visible = False
        '
        'ms_main_systemsettings
        '
        Me.ms_main_systemsettings.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_sett_userList1, Me.ms_sett_changepass, Me.ms_sett_preferences, Me.PeriodRestrictionsToolStripMenuItem, Me.ImportingToolStripMenuItem, Me.CompanyProfileToolStripMenuItem})
        Me.ms_main_systemsettings.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_systemsettings.Image = CType(resources.GetObject("ms_main_systemsettings.Image"), System.Drawing.Image)
        Me.ms_main_systemsettings.Name = "ms_main_systemsettings"
        Me.ms_main_systemsettings.Size = New System.Drawing.Size(118, 20)
        Me.ms_main_systemsettings.Text = "S&ystem Settings"
        Me.ms_main_systemsettings.Visible = False
        '
        'ms_sett_userList1
        '
        Me.ms_sett_userList1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_sett_userList, Me.ms_sett_createuser, Me.UserAccesibilityToolStripMenuItem})
        Me.ms_sett_userList1.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sett_userList1.Name = "ms_sett_userList1"
        Me.ms_sett_userList1.Size = New System.Drawing.Size(189, 22)
        Me.ms_sett_userList1.Text = "User Option"
        Me.ms_sett_userList1.Visible = False
        '
        'ms_sett_userList
        '
        Me.ms_sett_userList.Name = "ms_sett_userList"
        Me.ms_sett_userList.Size = New System.Drawing.Size(176, 22)
        Me.ms_sett_userList.Text = "User List"
        Me.ms_sett_userList.Visible = False
        '
        'ms_sett_createuser
        '
        Me.ms_sett_createuser.Name = "ms_sett_createuser"
        Me.ms_sett_createuser.Size = New System.Drawing.Size(176, 22)
        Me.ms_sett_createuser.Text = "Create Users"
        Me.ms_sett_createuser.Visible = False
        '
        'UserAccesibilityToolStripMenuItem
        '
        Me.UserAccesibilityToolStripMenuItem.Name = "UserAccesibilityToolStripMenuItem"
        Me.UserAccesibilityToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.UserAccesibilityToolStripMenuItem.Text = "User Access Rights"
        Me.UserAccesibilityToolStripMenuItem.Visible = False
        '
        'ms_sett_changepass
        '
        Me.ms_sett_changepass.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sett_changepass.Name = "ms_sett_changepass"
        Me.ms_sett_changepass.Size = New System.Drawing.Size(189, 22)
        Me.ms_sett_changepass.Text = "Change Password"
        Me.ms_sett_changepass.Visible = False
        '
        'ms_sett_preferences
        '
        Me.ms_sett_preferences.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_sett_preferences.Name = "ms_sett_preferences"
        Me.ms_sett_preferences.Size = New System.Drawing.Size(189, 22)
        Me.ms_sett_preferences.Text = "Preferences"
        Me.ms_sett_preferences.Visible = False
        '
        'PeriodRestrictionsToolStripMenuItem
        '
        Me.PeriodRestrictionsToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.PeriodRestrictionsToolStripMenuItem.Name = "PeriodRestrictionsToolStripMenuItem"
        Me.PeriodRestrictionsToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.PeriodRestrictionsToolStripMenuItem.Text = "Period Restrictions"
        Me.PeriodRestrictionsToolStripMenuItem.Visible = False
        '
        'ImportingToolStripMenuItem
        '
        Me.ImportingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.JournalEntriesToolStripMenuItem, Me.ChartOfAccountToolStripMenuItem, Me.SupplierMasterToolStripMenuItem, Me.AccountRegisterToolStripMenuItem1})
        Me.ImportingToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ImportingToolStripMenuItem.Name = "ImportingToolStripMenuItem"
        Me.ImportingToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ImportingToolStripMenuItem.Text = "Data Migration Tools"
        Me.ImportingToolStripMenuItem.Visible = False
        '
        'JournalEntriesToolStripMenuItem
        '
        Me.JournalEntriesToolStripMenuItem.Name = "JournalEntriesToolStripMenuItem"
        Me.JournalEntriesToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.JournalEntriesToolStripMenuItem.Text = "Journal Entries"
        Me.JournalEntriesToolStripMenuItem.Visible = False
        '
        'ChartOfAccountToolStripMenuItem
        '
        Me.ChartOfAccountToolStripMenuItem.Name = "ChartOfAccountToolStripMenuItem"
        Me.ChartOfAccountToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ChartOfAccountToolStripMenuItem.Text = "Chart of Account"
        Me.ChartOfAccountToolStripMenuItem.Visible = False
        '
        'SupplierMasterToolStripMenuItem
        '
        Me.SupplierMasterToolStripMenuItem.Name = "SupplierMasterToolStripMenuItem"
        Me.SupplierMasterToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.SupplierMasterToolStripMenuItem.Text = "Supplier Master"
        Me.SupplierMasterToolStripMenuItem.Visible = False
        '
        'AccountRegisterToolStripMenuItem1
        '
        Me.AccountRegisterToolStripMenuItem1.Name = "AccountRegisterToolStripMenuItem1"
        Me.AccountRegisterToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.AccountRegisterToolStripMenuItem1.Text = "Account Register"
        '
        'CompanyProfileToolStripMenuItem
        '
        Me.CompanyProfileToolStripMenuItem.Name = "CompanyProfileToolStripMenuItem"
        Me.CompanyProfileToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.CompanyProfileToolStripMenuItem.Text = "Project Profile"
        Me.CompanyProfileToolStripMenuItem.Visible = False
        '
        'toolStripToolsOptions
        '
        Me.toolStripToolsOptions.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OptionsToolStripMenuItem})
        Me.toolStripToolsOptions.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.toolStripToolsOptions.Image = Global.CSAcctg.My.Resources.Resources.exec
        Me.toolStripToolsOptions.Name = "toolStripToolsOptions"
        Me.toolStripToolsOptions.Size = New System.Drawing.Size(65, 20)
        Me.toolStripToolsOptions.Text = "Tools"
        Me.toolStripToolsOptions.Visible = False
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.OptionsToolStripMenuItem.Text = "Options"
        Me.OptionsToolStripMenuItem.Visible = False
        '
        'ms_main_banking
        '
        Me.ms_main_banking.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_banking_writechecks, Me.ms_banking_makedeposit, Me.ms_banking_transferfunds, Me.ms_banking_reconcile})
        Me.ms_main_banking.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_banking.Image = CType(resources.GetObject("ms_main_banking.Image"), System.Drawing.Image)
        Me.ms_main_banking.Name = "ms_main_banking"
        Me.ms_main_banking.Size = New System.Drawing.Size(79, 20)
        Me.ms_main_banking.Text = "&Banking"
        Me.ms_main_banking.Visible = False
        '
        'ms_banking_writechecks
        '
        Me.ms_banking_writechecks.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_banking_writechecks.Name = "ms_banking_writechecks"
        Me.ms_banking_writechecks.Size = New System.Drawing.Size(155, 22)
        Me.ms_banking_writechecks.Text = "Write Checks"
        Me.ms_banking_writechecks.Visible = False
        '
        'ms_banking_makedeposit
        '
        Me.ms_banking_makedeposit.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_banking_makedeposit.Name = "ms_banking_makedeposit"
        Me.ms_banking_makedeposit.Size = New System.Drawing.Size(155, 22)
        Me.ms_banking_makedeposit.Text = "Make Deposits"
        '
        'ms_banking_transferfunds
        '
        Me.ms_banking_transferfunds.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_banking_transferfunds.Name = "ms_banking_transferfunds"
        Me.ms_banking_transferfunds.Size = New System.Drawing.Size(155, 22)
        Me.ms_banking_transferfunds.Text = "Transfer Funds"
        '
        'ms_banking_reconcile
        '
        Me.ms_banking_reconcile.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_banking_reconcile.Name = "ms_banking_reconcile"
        Me.ms_banking_reconcile.Size = New System.Drawing.Size(155, 22)
        Me.ms_banking_reconcile.Text = "Reconcile"
        Me.ms_banking_reconcile.Visible = False
        '
        'ms_main_customer
        '
        Me.ms_main_customer.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ms_cust_custmaster, Me.ToolStripSeparator6, Me.ms_cust_createSO, Me.ms_cust_createInvoice, Me.ms_cust_enterSR, Me.ToolStripSeparator7, Me.ms_cust_enterSC, Me.ms_cust_createS, Me.ms_cust_assesFC, Me.ToolStripSeparator8, Me.ms_cust_recvPayment, Me.ms_cust_createCM, Me.CreateDebitMemosToolStripMenuItem})
        Me.ms_main_customer.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_customer.Image = CType(resources.GetObject("ms_main_customer.Image"), System.Drawing.Image)
        Me.ms_main_customer.Name = "ms_main_customer"
        Me.ms_main_customer.Size = New System.Drawing.Size(87, 20)
        Me.ms_main_customer.Text = "&Customer"
        Me.ms_main_customer.Visible = False
        '
        'ms_cust_custmaster
        '
        Me.ms_cust_custmaster.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_custmaster.Name = "ms_cust_custmaster"
        Me.ms_cust_custmaster.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_custmaster.Text = "Customer Master"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(203, 6)
        '
        'ms_cust_createSO
        '
        Me.ms_cust_createSO.Enabled = False
        Me.ms_cust_createSO.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_createSO.Name = "ms_cust_createSO"
        Me.ms_cust_createSO.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_createSO.Text = "Create Sales Order"
        Me.ms_cust_createSO.Visible = False
        '
        'ms_cust_createInvoice
        '
        Me.ms_cust_createInvoice.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_createInvoice.Name = "ms_cust_createInvoice"
        Me.ms_cust_createInvoice.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_createInvoice.Text = "Create Invoices"
        '
        'ms_cust_enterSR
        '
        Me.ms_cust_enterSR.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_enterSR.Name = "ms_cust_enterSR"
        Me.ms_cust_enterSR.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_enterSR.Text = "Enter Sales Receipts"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(203, 6)
        Me.ToolStripSeparator7.Visible = False
        '
        'ms_cust_enterSC
        '
        Me.ms_cust_enterSC.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_enterSC.Name = "ms_cust_enterSC"
        Me.ms_cust_enterSC.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_enterSC.Text = "Enter Statement Charges"
        Me.ms_cust_enterSC.Visible = False
        '
        'ms_cust_createS
        '
        Me.ms_cust_createS.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_createS.Name = "ms_cust_createS"
        Me.ms_cust_createS.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_createS.Text = "Create Statements"
        Me.ms_cust_createS.Visible = False
        '
        'ms_cust_assesFC
        '
        Me.ms_cust_assesFC.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_assesFC.Name = "ms_cust_assesFC"
        Me.ms_cust_assesFC.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_assesFC.Text = "Assess Finance Charges"
        Me.ms_cust_assesFC.Visible = False
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(203, 6)
        '
        'ms_cust_recvPayment
        '
        Me.ms_cust_recvPayment.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_recvPayment.Name = "ms_cust_recvPayment"
        Me.ms_cust_recvPayment.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_recvPayment.Text = "Receive Payments"
        '
        'ms_cust_createCM
        '
        Me.ms_cust_createCM.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.ms_cust_createCM.Name = "ms_cust_createCM"
        Me.ms_cust_createCM.Size = New System.Drawing.Size(206, 22)
        Me.ms_cust_createCM.Text = "Create Credit Memos"
        '
        'CreateDebitMemosToolStripMenuItem
        '
        Me.CreateDebitMemosToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CreateDebitMemosToolStripMenuItem.Name = "CreateDebitMemosToolStripMenuItem"
        Me.CreateDebitMemosToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.CreateDebitMemosToolStripMenuItem.Text = "Create Debit Memos"
        '
        'ms_main_windows
        '
        Me.ms_main_windows.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_windows.Image = CType(resources.GetObject("ms_main_windows.Image"), System.Drawing.Image)
        Me.ms_main_windows.Name = "ms_main_windows"
        Me.ms_main_windows.Size = New System.Drawing.Size(87, 20)
        Me.ms_main_windows.Text = "&Windows"
        Me.ms_main_windows.Visible = False
        '
        'ms_main_help
        '
        Me.ms_main_help.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ms_main_help.Image = CType(resources.GetObject("ms_main_help.Image"), System.Drawing.Image)
        Me.ms_main_help.Name = "ms_main_help"
        Me.ms_main_help.Size = New System.Drawing.Size(60, 20)
        Me.ms_main_help.Text = "&Help"
        Me.ms_main_help.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.AliceBlue
        Me.Panel1.BackgroundImage = Global.CSAcctg.My.Resources.Resources.Project_Name_Panel
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.LblClassActivator)
        Me.Panel1.Controls.Add(Me.btnbar)
        Me.Panel1.Controls.Add(Me.lblCompanyName)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1018, 28)
        Me.Panel1.TabIndex = 2
        '
        'LblClassActivator
        '
        Me.LblClassActivator.AutoSize = True
        Me.LblClassActivator.Location = New System.Drawing.Point(679, 7)
        Me.LblClassActivator.Name = "LblClassActivator"
        Me.LblClassActivator.Size = New System.Drawing.Size(0, 13)
        Me.LblClassActivator.TabIndex = 2
        Me.LblClassActivator.Visible = False
        '
        'btnbar
        '
        Me.btnbar.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.btnbar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnbar.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnbar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnbar.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbar.ForeColor = System.Drawing.Color.White
        Me.btnbar.Location = New System.Drawing.Point(907, 2)
        Me.btnbar.Name = "btnbar"
        Me.btnbar.Size = New System.Drawing.Size(108, 23)
        Me.btnbar.TabIndex = 1
        Me.btnbar.Text = "Hide Side Bar"
        Me.btnbar.UseVisualStyleBackColor = False
        Me.btnbar.Visible = False
        '
        'lblCompanyName
        '
        Me.lblCompanyName.AutoSize = True
        Me.lblCompanyName.BackColor = System.Drawing.Color.Transparent
        Me.lblCompanyName.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyName.ForeColor = System.Drawing.Color.Black
        Me.lblCompanyName.Location = New System.Drawing.Point(6, 1)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.Size = New System.Drawing.Size(115, 23)
        Me.lblCompanyName.TabIndex = 0
        Me.lblCompanyName.Text = "Project name"
        '
        'axbar
        '
        Me.axbar.AnimateStateChanges = True
        Me.axbar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.axbar.BackColorEnd = System.Drawing.Color.Teal
        Me.axbar.BackColorStart = System.Drawing.Color.DodgerBlue
        Me.axbar.Dock = System.Windows.Forms.DockStyle.Left
        Me.axbar.DrawingStyle = vbAccelerator.Components.Controls.ExplorerBarDrawingStyle.Custom
        Me.axbar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.axbar.ForeColor = System.Drawing.Color.Black
        Me.axbar.ImageList = Nothing
        Me.axbar.Location = New System.Drawing.Point(0, 51)
        Me.axbar.Mode = vbAccelerator.Components.Controls.ExplorerBarMode.[Default]
        Me.axbar.Name = "axbar"
        Me.axbar.Redraw = True
        Me.axbar.ShowFocusRect = True
        Me.axbar.Size = New System.Drawing.Size(261, 437)
        Me.axbar.TabIndex = 23
        Me.axbar.TitleImageList = Nothing
        Me.axbar.ToolTip = Nothing
        '
        'sidebarImages
        '
        Me.sidebarImages.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.sidebarImages.ImageSize = New System.Drawing.Size(16, 16)
        Me.sidebarImages.TransparentColor = System.Drawing.Color.Transparent
        '
        'statusBar
        '
        Me.statusBar.BackColor = System.Drawing.Color.White
        Me.statusBar.BackgroundImage = Global.CSAcctg.My.Resources.Resources.Project_Name_Panel
        Me.statusBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.statusBar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.statusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblCurrentDate, Me.currentDate, Me.lblCurrentUser, Me.currentUser, databaseInUseIcon, Me.databaseInUse, Me.ToolStripStatusLabel1, Me.toolStripUsername})
        Me.statusBar.Location = New System.Drawing.Point(0, 534)
        Me.statusBar.Name = "statusBar"
        Me.statusBar.Size = New System.Drawing.Size(1018, 24)
        Me.statusBar.TabIndex = 33
        '
        'lblCurrentDate
        '
        Me.lblCurrentDate.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrentDate.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.lblCurrentDate.Name = "lblCurrentDate"
        Me.lblCurrentDate.Size = New System.Drawing.Size(76, 19)
        Me.lblCurrentDate.Text = "Current Date"
        '
        'currentDate
        '
        Me.currentDate.AutoSize = False
        Me.currentDate.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.currentDate.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.currentDate.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.currentDate.ForeColor = System.Drawing.Color.DarkBlue
        Me.currentDate.Name = "currentDate"
        Me.currentDate.Size = New System.Drawing.Size(150, 19)
        '
        'lblCurrentUser
        '
        Me.lblCurrentUser.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrentUser.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.lblCurrentUser.Name = "lblCurrentUser"
        Me.lblCurrentUser.Size = New System.Drawing.Size(108, 19)
        Me.lblCurrentUser.Text = "Current User Login"
        '
        'currentUser
        '
        Me.currentUser.AutoSize = False
        Me.currentUser.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.currentUser.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.currentUser.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.currentUser.ForeColor = System.Drawing.Color.Red
        Me.currentUser.Name = "currentUser"
        Me.currentUser.Size = New System.Drawing.Size(150, 19)
        '
        'databaseInUse
        '
        Me.databaseInUse.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.databaseInUse.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.databaseInUse.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.databaseInUse.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.databaseInUse.Name = "databaseInUse"
        Me.databaseInUse.Size = New System.Drawing.Size(110, 19)
        Me.databaseInUse.Text = "                                 "
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(70, 19)
        Me.ToolStripStatusLabel1.Text = "Username:"
        '
        'toolStripUsername
        '
        Me.toolStripUsername.BackColor = System.Drawing.Color.Transparent
        Me.toolStripUsername.Name = "toolStripUsername"
        Me.toolStripUsername.Size = New System.Drawing.Size(131, 19)
        Me.toolStripUsername.Text = "ToolStripStatusLabel2"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'cpics5
        '
        Me.cpics5.BackColor = System.Drawing.Color.White
        Me.cpics5.Image = CType(resources.GetObject("cpics5.Image"), System.Drawing.Image)
        Me.cpics5.InitialImage = Nothing
        Me.cpics5.Location = New System.Drawing.Point(359, 404)
        Me.cpics5.Name = "cpics5"
        Me.cpics5.Size = New System.Drawing.Size(80, 54)
        Me.cpics5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cpics5.TabIndex = 31
        Me.cpics5.TabStop = False
        Me.cpics5.Visible = False
        '
        'cpics4
        '
        Me.cpics4.BackColor = System.Drawing.Color.White
        Me.cpics4.Image = CType(resources.GetObject("cpics4.Image"), System.Drawing.Image)
        Me.cpics4.InitialImage = Nothing
        Me.cpics4.Location = New System.Drawing.Point(359, 348)
        Me.cpics4.Name = "cpics4"
        Me.cpics4.Size = New System.Drawing.Size(80, 54)
        Me.cpics4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cpics4.TabIndex = 30
        Me.cpics4.TabStop = False
        Me.cpics4.Visible = False
        '
        'cpics3
        '
        Me.cpics3.BackColor = System.Drawing.Color.White
        Me.cpics3.Image = CType(resources.GetObject("cpics3.Image"), System.Drawing.Image)
        Me.cpics3.InitialImage = Nothing
        Me.cpics3.Location = New System.Drawing.Point(276, 290)
        Me.cpics3.Name = "cpics3"
        Me.cpics3.Size = New System.Drawing.Size(80, 54)
        Me.cpics3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cpics3.TabIndex = 29
        Me.cpics3.TabStop = False
        Me.cpics3.Visible = False
        '
        'cpics2
        '
        Me.cpics2.BackColor = System.Drawing.Color.White
        Me.cpics2.Image = CType(resources.GetObject("cpics2.Image"), System.Drawing.Image)
        Me.cpics2.InitialImage = Nothing
        Me.cpics2.Location = New System.Drawing.Point(359, 290)
        Me.cpics2.Name = "cpics2"
        Me.cpics2.Size = New System.Drawing.Size(80, 54)
        Me.cpics2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cpics2.TabIndex = 28
        Me.cpics2.TabStop = False
        Me.cpics2.Visible = False
        '
        'cpics1
        '
        Me.cpics1.BackColor = System.Drawing.Color.White
        Me.cpics1.Image = CType(resources.GetObject("cpics1.Image"), System.Drawing.Image)
        Me.cpics1.InitialImage = Nothing
        Me.cpics1.Location = New System.Drawing.Point(276, 404)
        Me.cpics1.Name = "cpics1"
        Me.cpics1.Size = New System.Drawing.Size(80, 54)
        Me.cpics1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cpics1.TabIndex = 27
        Me.cpics1.TabStop = False
        Me.cpics1.Visible = False
        '
        'cpics
        '
        Me.cpics.BackColor = System.Drawing.Color.White
        Me.cpics.Image = CType(resources.GetObject("cpics.Image"), System.Drawing.Image)
        Me.cpics.InitialImage = Nothing
        Me.cpics.Location = New System.Drawing.Point(276, 348)
        Me.cpics.Name = "cpics"
        Me.cpics.Size = New System.Drawing.Size(80, 54)
        Me.cpics.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cpics.TabIndex = 25
        Me.cpics.TabStop = False
        Me.cpics.Visible = False
        '
        'DynamicColumnToolStripMenuItem
        '
        Me.DynamicColumnToolStripMenuItem.Name = "DynamicColumnToolStripMenuItem"
        Me.DynamicColumnToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.DynamicColumnToolStripMenuItem.Text = "Dynamic Column"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1018, 558)
        Me.Controls.Add(Me.statusBar)
        Me.Controls.Add(Me.cpics5)
        Me.Controls.Add(Me.cpics4)
        Me.Controls.Add(Me.cpics3)
        Me.Controls.Add(Me.cpics2)
        Me.Controls.Add(Me.cpics1)
        Me.Controls.Add(Me.cpics)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.msMainMenu)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.Text = "UCoreSoftware: Accounting System v.3.0"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.msMainMenu.ResumeLayout(False)
        Me.msMainMenu.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.statusBar.ResumeLayout(False)
        Me.statusBar.PerformLayout()
        CType(Me.cpics5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cpics4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cpics3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cpics2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cpics1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cpics, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents msMainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents ms_main_file As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_masterfile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_accountant As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_customer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_supplier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_banking As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_reports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_systemsettings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_windows As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_help As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblCompanyName As System.Windows.Forms.Label
    Friend WithEvents ms_file_newcompany As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_file_openCompany As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_file_closecompany As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_file_exit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_pricelevel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_billingrate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_tax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_salesrep As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_customertype As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_vendortype As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_terms As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_accountant_genjourentry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_accountant_wrktrialbal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_accountant_setclosdte As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_custmaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_createSO As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_createInvoice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_enterSR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_enterSC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_createS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_assesFC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_recvPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_cust_createCM As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents axbar As vbAccelerator.Components.Controls.acclExplorerBar
    Friend WithEvents ms_banking_writechecks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_banking_makedeposit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_banking_transferfunds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_banking_reconcile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_supmaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_enterbills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_paybills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_saleTax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_createPO As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_rcvItembill As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_rcveItm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_entRcvBill As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_tax_paysalestax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_tax_managetax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_tax_adjtaxdue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_tax_taxliability As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_tax_salestax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_tax_revenuesumm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sett_changepass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sett_preferences As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ms_file_LogOff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cpics As System.Windows.Forms.PictureBox
    Friend WithEvents ms_sett_userList1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents sidebarImages As System.Windows.Forms.ImageList
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ms_master_other As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_itemmaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_itemGroup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_salestax As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_salestaxcode As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_master_paymentMethod As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sup_billsapproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cpics1 As System.Windows.Forms.PictureBox
    Friend WithEvents cpics2 As System.Windows.Forms.PictureBox
    Friend WithEvents cpics3 As System.Windows.Forms.PictureBox
    Friend WithEvents cpics4 As System.Windows.Forms.PictureBox
    Friend WithEvents cpics5 As System.Windows.Forms.PictureBox
    Friend WithEvents statusBar As System.Windows.Forms.StatusStrip
    Friend WithEvents lblCurrentDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents currentDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblCurrentUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents currentUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnbar As System.Windows.Forms.Button
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreateDebitMemosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PeriodRestrictionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CustomerTransactionListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvoiceListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreditMemoListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitMemoListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BudgetMonitoringReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CostumerTransactionReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents databaseInUse As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ClassToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LblClassActivator As System.Windows.Forms.Label
    Friend WithEvents ms_sett_createuser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_sett_userList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FinancialStatementsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceSheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IncomeStatementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReceivablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents BillsPaymentAccountsReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountsPayableListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CollectionReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountsPayablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsSalesreport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JournalToolStripMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JournalVoucherSummary As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JournalVoucherPerAccountToolStripMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatementOfAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AccountantsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralLedgerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrialBalanceToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChartOfAccountsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IncomeStatementDrillDownToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IncomeStatementPrevYearComparisonToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IncomeStatementWithStatutoryAllocationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatutoryAllocationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents toolStripUsername As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents toolStripToolsOptions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreateClosingEntriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BankFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepositAccountingEntriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JournalEntriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentTypeSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChartOfAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SupplierMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CostCenterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CachedSupplierMasterReport1 As CSAcctg.CachedSupplierMasterReport
    Friend WithEvents CachedSupplierMasterReport2 As CSAcctg.CachedSupplierMasterReport
    Friend WithEvents CachedSupplierMasterReport3 As CSAcctg.CachedSupplierMasterReport
    Friend WithEvents DocumentTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckIssuanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CachedSupplierMasterReport4 As CSAcctg.CachedSupplierMasterReport
    Friend WithEvents ByDocTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColumnInterfaceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralJournalToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonthlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonthlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceSheetToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IncomeStatementToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CashReceiptsJournalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitCreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountRegisterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountRegisterToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JournalListingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountBalanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitAccountsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreditAccountsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotBalanceEntriesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseOderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseReturnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesReturnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserAccesibilityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByCollectorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByCashierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CombinedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CashPositionReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoansReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScheduleOfLRByCategoryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScheduleOfLRAllTypesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfLRByCategoryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountAnalysisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BankReconciliationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AuditTrailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountsReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountsPayableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientList1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientList2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_accountinquiry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountInquiryToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatementOfAccountToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CRJMonthlySummarizedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CRJMonthlySpreadsheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CDJMonthlySpreadsheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GJMonthlySpreadsheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralLedgerToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CashFlowStatementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrialBalanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WorkingTrialBalanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrialBalanceToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailyToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonthlyToolStripMenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfAccountReceivablesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfAdvancesToOEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfAccountsPayableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByCategoryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllTypesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_billing As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BillingReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CollectionsIntegratorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_computation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SavingsInterestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DividendToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PatronageRefundToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BereavementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_database As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackUpDatabaseToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResoreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataMigrationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountForwardBalanceToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResetForwardBalanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenerateTemplateToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientUploadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompanyProfileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountRegisterToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ms_main_accountschedules As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebitAccountsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfAccountsReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreditsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreditAcountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfAccountsPayableToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByCategoryToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AllTypesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgingOfLoansReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountAnalysisToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BillingListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnearnedInterestLapsingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PESOReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecurringEntrySetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatutoryFundToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents statutoryMaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DynamicColumnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
