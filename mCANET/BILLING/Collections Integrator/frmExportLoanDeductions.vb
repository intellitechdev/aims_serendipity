﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmExportLoanDeductions

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        'If isReportLoaded() Then
        '    crvRpt.ReportSource = rptsummary
        '    Me.Text = "Export Loan Deductions"
        'End If
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Function isReportLoaded(ByVal key As String) As Boolean
        Try
            Me.Text = "Loading..."
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\rptLoanDeductions.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@DocNumber", key)
            crvRpt.ReportSource = rptsummary
            Me.Text = "Billing Summary - By Group"
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function isPayroll(ByVal key As String) As Boolean
        Try
            Me.Text = "Loading..."
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\rptPayrollTemplate.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@DocNumber", key)
            crvRpt.ReportSource = rptsummary
            Me.Text = "Billing Summary - By Group"
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        crvRpt.ExportReport()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        crvRpt.PrintReport()
    End Sub

    Public Sub _Print_Billing_Diff()
        Try
            Me.Text = "Loading..."
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\rptBILLING_Diff.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            crvRpt.ReportSource = rptsummary
            Me.Text = "Billing (Over)/Under List Report"
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frmExportLoanDeductions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub _PrintPAYROLL_001(ByVal key As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\PayrollPrint001.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@DocNo", key)
            crvRpt.ReportSource = rptsummary
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub _Print_BILLINGMODULE(ByVal key As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\BillingModuleReport.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@docnumber", key)
            crvRpt.ReportSource = rptsummary
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub _Print_Billing_DETAILED(ByVal group As String, ByVal dt As String)
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\BillingPrintDetailed.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xDate", dt)
            rptsummary.SetParameterValue("@Group", group)
            crvRpt.ReportSource = rptsummary
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class