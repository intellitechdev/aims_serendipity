﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmSearch_Billing

    Private con As New Clsappconfiguration
    Dim ds As DataSet
    Public xmode As String = ""

    Private Sub frmSearch_Billing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded("") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Function isLoaded(ByVal key As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Billing_Search_DocNo",
                                           New SqlParameter("@search", key))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        xmode = ""
        dgvList.Columns.Clear()
        dgvList.DataSource = Nothing
        Me.Close()
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                xmode = ""
                Me.Close()
            End If
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isLoaded(txtSearch.Text) Then

        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            If xmode = "collect" Then
                'frmCollectionsIntegrator.txtDocNumber.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
                'frmCollectionsIntegrator.LoadDetails()
                frmCollectionsBilling.txtBilling.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
            Else
                If frmBilling_byGroup.isPrepareColumns() Then
                    frmBilling_byGroup.txtDocNumber.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
                    frmBilling_byGroup.isHeaderLoaded(dgvList.SelectedRows(0).Cells(0).Value.ToString)
                    frmBilling_byGroup.isDetailsLoaded(dgvList.SelectedRows(0).Cells(0).Value.ToString)
                End If
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        If isSelected() Then
            xmode = ""
            Me.Close()
        End If
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                xmode = ""
                Me.Close()
            End If
        End If
    End Sub

End Class