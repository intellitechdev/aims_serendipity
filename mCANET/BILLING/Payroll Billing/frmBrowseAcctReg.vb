﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBrowseAcctReg

    Private con As New Clsappconfiguration
    Dim ds As DataSet

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Function isLoaded(ByVal key As String) As Boolean
        Try
            Me.Text = "Loading..."
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Collections_LoadAcctRegister",
                                           New SqlParameter("@search", key))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub frmBrowseAcctReg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded("") Then
            Me.Text = "Account Register"
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isLoaded(txtSearch.Text) Then
                Me.Text = "Account Register"
            End If
        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            With dgvList.SelectedRows(0)
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(0).Value = .Cells(0).Value
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(1).Value = .Cells(1).Value
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(2).Value = " "
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(3).Value = .Cells(2).Value
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(4).Value = .Cells(3).Value
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(5).Value = .Cells(4).Value
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(8).Value = "null"
            End With
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        If isSelected() Then
            Me.Close()
        End If
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If isSelected() Then
            Me.Close()
        End If
    End Sub

End Class