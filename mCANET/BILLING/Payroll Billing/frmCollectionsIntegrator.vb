﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Data.OleDb
Imports Microsoft.VisualBasic.FileSystem
Public Class frmCollectionsIntegrator

    Dim filenym As String
    Dim fpath As String
    Private con As New Clsappconfiguration
    Public mIndex As Integer = 0
    Public mIndex2 As Integer = 0
    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        If MsgBox("Are you sure ?", vbYesNo + MsgBoxStyle.Question, "Exit") = vbYes Then
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub frmCollectionsIntegrator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub iscolPrepare()
        dgvList.DataSource = Nothing
        dgvList.Columns.Clear()
        dgvList.Rows.Clear()

        Dim idno As New DataGridViewTextBoxColumn
        Dim clientname As New DataGridViewTextBoxColumn
        Dim loanref As New DataGridViewTextBoxColumn
        Dim acctref As New DataGridViewTextBoxColumn
        Dim acctcode As New DataGridViewTextBoxColumn
        Dim accttitle As New DataGridViewTextBoxColumn
        Dim debit As New DataGridViewTextBoxColumn
        Dim credit As New DataGridViewTextBoxColumn
        Dim fxkey As New DataGridViewTextBoxColumn
        With idno
            .Name = "idno"
            .HeaderText = "ID No."
        End With
        With clientname
            .Name = "clientname"
            .HeaderText = "Name"
        End With
        With loanref
            .Name = "loanref"
            .HeaderText = "Loan Ref."
        End With
        With acctref
            .Name = "acctref"
            .HeaderText = "Account Ref."
        End With
        With acctcode
            .Name = "acctcode"
            .HeaderText = "Code"
        End With
        With accttitle
            .Name = "accttitle"
            .HeaderText = "Account Title"
        End With
        With debit
            .Name = "debit"
            .HeaderText = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With credit
            .Name = "credit"
            .HeaderText = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With fxkey
            .Name = "fxkey"
            .Visible = False
        End With
        With dgvList
            .Columns.Add(idno)
            .Columns.Add(clientname)
            .Columns.Add(loanref)
            .Columns.Add(acctref)
            .Columns.Add(acctcode)
            .Columns.Add(accttitle)
            .Columns.Add(debit)
            .Columns.Add(credit)
            .Columns.Add(fxkey)
        End With
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        iscolPrepare()
        dgvList.Rows.Add()
        clfields()
        isUnposted()
        ldDocnum()
        chkPosted.Enabled = False
        mIndex = 0
        mIndex2 = 0
    End Sub

    Private Sub clfields()
        chkCancelled.Checked = False
        chkPosted.Checked = False
        txtCollector.Text = ""
        txtCreatedby.Text = ""
        txtCredit.Text = "0.00"
        txtDebit.Text = "0.00"
        txtDifference.Text = "0.00"
        txtDocNumber.Text = ""
        txtORNo.Text = ""
        txtParticulars.Text = ""
        dtDate.Value = Date.Now
    End Sub

    Public Sub LoadDetails()
        iscolPrepare()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Collection2_GetBilling",
                                      New SqlParameter("@fcDocNumber", txtDocNumber.Text))
        While rd.Read
            With dgvList
                '.Rows.Add()
                '.Item("idno", mIndex).Value = rd(0)
                '.Item("clientname", mIndex).Value = rd(1)
                '.Item("loanref", mIndex).Value = rd(2)
                '.Item("acctref", mIndex).Value = " "
                '.Item("acctcode", mIndex).Value = rd(3)
                '.Item("accttitle", mIndex).Value = rd(4)
                '.Item("debit", mIndex).Value = "0.00"
                '.Item("credit", mIndex).Value = rd(5)
                '.Item("fxkey", mIndex).Value = "null"
                _PayLoans2(rd(2), rd(5))
                '_PayCharges(rd(2), rd(5))
                'mIndex += 1
            End With
        End While
        mIndex = 0
    End Sub

    Private Sub FormatNumValues()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                .Item("debit", i).Value = Format(CDec(.Item("debit", i).Value), "##,##0.00")
                .Item("credit", i).Value = Format(CDec(.Item("credit", i).Value), "##,##0.00")
            End With
        Next
    End Sub

    Private Sub dgvList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellValueChanged
        _FormatCells()
    End Sub

    Private Sub _FormatCells()
        Dim totdebit As Decimal = 0.0
        Dim totcredit As Decimal = 0.0
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                totdebit = totdebit + CDec(.Item("debit", i).Value)
                totcredit = totcredit + CDec(.Item("credit", i).Value)
            End With
        Next
        txtCredit.Text = Format(totcredit, "##,##0.00")
        txtDebit.Text = Format(totdebit, "##,##0.00")
        txtDifference.Text = Format((totdebit - totcredit), "##,##0.00")
        FormatNumValues()
    End Sub

    Private Sub btnGetBilling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetBilling.Click
        frmSearch_Billing.xmode = "collect"
        frmSearch_Billing.StartPosition = FormStartPosition.CenterScreen
        frmSearch_Billing.ShowDialog()
    End Sub

    Private Sub btnBrowseDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseDoc.Click
        frmBDoc.doctype = "OR"
        frmBDoc.StartPosition = FormStartPosition.CenterScreen
        frmBDoc.ShowDialog()
    End Sub

    Private Sub ldDocnum()
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_Select_Top1_OR")
            While rd.Read
                txtORNo.Text = rd(0)
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function isHeaderSaved() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_InsertUpdate_EntryHeader",
                                       New SqlParameter("@fiEntryNo", txtORNo.Text),
                                       New SqlParameter("@company", frmMain.lblCompanyName.Text),
                                       New SqlParameter("@fdTransDate", dtDate.Value),
                                       New SqlParameter("@fdTotAmount", CDec(txtDebit.Text)),
                                       New SqlParameter("@user", txtCreatedby.Text),
                                       New SqlParameter("@doctypeInitial", "OR"),
                                       New SqlParameter("@fbCancelled", chkCancelled.Checked),
                                       New SqlParameter("@fbPosted", chkPosted.Checked))
            Return True
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "isHeaderSaved")
            Return False
        End Try
    End Function

    Dim lastloan As String
    Dim totcredit As Decimal
    Private Sub SubsidiaryUpdating()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim acctcode As String = .Item("acctcode", i).Value.ToString
                    Dim acctName As String = .Item("accttitle", i).Value.ToString
                    Dim fxkey As String = IIf(.Item("fxkey", i).Value = "null", System.Guid.NewGuid.ToString, .Item("fxkey", i).Value.ToString)
                    Dim debit As Decimal = CDec(.Item("debit", i).Value.ToString)
                    Dim credit As Decimal = CDec(.Item("credit", i).Value.ToString)
                    Dim idno As String = .Item("idno", i).Value.ToString
                    Dim loanref As String = .Item("loanref", i).Value.ToString
                    Dim acctref As String = .Item("acctref", i).Value.ToString
                    Dim accnt_Id As String = .Item("fxkey", i).Tag.ToString
                    Dim keyid As New Guid(fxkey)

                    If chkPosted.Checked = True Then
                        If loanref <> "" Or loanref IsNot DBNull.Value Then
                            LoanAccountsDisbursement(loanref, accnt_Id, dtDate.Value, txtORNo.Text, txtParticulars.Text, credit, acctcode)
                            '_Update_LoanSubsidiary_2(loanref, txtORNo.Text, dtDate.Value, credit, txtParticulars.Text, acctype)
                        End If
                        'UpdateLoanSubsidiary_DebitEntry(loanref, dtDate.Value, txtORNo.Text, txtParticulars.Text, credit, 1, 1)
                        UpdateSubsidiary(acctref, txtORNo.Text, debit, credit, txtParticulars.Text, dtDate.Value, "Cash", 1)

                        '  _ApplyAmortization_ComputeBalances(loanref, dtDate.Value, txtORNo.Text, credit)

                    Else
                        If loanref <> "" Or loanref IsNot DBNull.Value Then
                            _Update_LoanSubsidiary_Unpost(loanref, txtORNo.Text, credit)
                        End If
                        'UpdateLoanSubsidiary_DebitEntry(loanref, dtDate.Value, txtORNo.Text, txtParticulars.Text, credit, 1, 0)
                        UpdateSubsidiary(acctref, txtORNo.Text, debit, credit, txtParticulars.Text, dtDate.Value, "Cash", 0)

                        _Unpost_Amortization_balancesEntry(txtORNo.Text)
                        'Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_SelectLoanHeader", _
                        '    New SqlParameter("@fcAccountCode", acctcode))
                        'If rd2.Read = True Then
                        '    Call UpdateLoanSubsidiary_UnpostEntry(loanref, dtDate.Value, txtORNo.Text, credit, 1)
                        'Else
                        '    Call UpdateLoanSubsidiary_UnpostEntry(loanref, dtDate.Value, txtORNo.Text, credit, 0)
                        'End If

                    End If
                End With
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "SubsidiaryUpdating")
        End Try
    End Sub

    Private Sub LoanAccountsDisbursement(ByVal loanNo As String, ByVal acnt_id As String, ByVal transdate As Date, ByVal docnum As String, ByVal memo As String, ByVal amount As Decimal, ByVal acntCode As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loan_Accounts", _
                                      New SqlParameter("@loanNo", loanNo),
                                      New SqlParameter("@fk_Account", acnt_id))
            If rd.Read = True Then
                Dim acntType As String = rd.Item("AcctType").ToString
                If acntType = "Interest" Or acntType = "Service Fee" Or acntType = "Unearned Interest" Or acntType = "Penalty" Or acntType = "Others" Or acntType = "Credit Account" Then
                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "_Collections_Loans_AccountsPaymentInsert", _
                                      New SqlParameter("@LoanNo", loanNo),
                                      New SqlParameter("@DocNo", docnum),
                                      New SqlParameter("@fdDate", transdate),
                                      New SqlParameter("@amount", amount),
                                      New SqlParameter("@particulars", memo),
                                      New SqlParameter("@AcntType", acntType))
                    _ApplyAmortization_ComputeBalances(loanNo, transdate, docnum, amount, acntType)
                End If
            Else
                Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_SelectLoanHeader", _
                                      New SqlParameter("@fcAccountCode", acntCode))
                If rd2.Read = True Then
                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "_Collections_Loans_AccountsPaymentInsert", _
                                      New SqlParameter("@LoanNo", loanNo),
                                      New SqlParameter("@DocNo", docnum),
                                      New SqlParameter("@fdDate", transdate),
                                      New SqlParameter("@amount", amount),
                                      New SqlParameter("@particulars", memo),
                                      New SqlParameter("@AcntType", "Balance"))
                    _ApplyAmortization_ComputeBalances(loanNo, transdate, docnum, amount, "Principal")
                End If
            End If

            'code here to apply amort payment
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "LoanAccountsDisbursement")
        End Try
    End Sub

    Private Sub _Update_LoanSubsidiary_2(ByVal loanno As String, ByVal docno As String, ByVal ddate As Date, ByVal amount As Decimal,
                                         ByVal particulars As String, ByVal acctype As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_Loans_AccountsPaymentInsert",
                                       New SqlParameter("@LoanNo", loanno),
                                       New SqlParameter("@DocNo", docno),
                                       New SqlParameter("@fdDate", ddate),
                                       New SqlParameter("@amount", amount),
                                       New SqlParameter("@particulars", particulars),
                                       New SqlParameter("@AcntType", acctype))
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Update_LoanSubsidiary_2")
        End Try
    End Sub
    Private Sub _Update_LoanSubsidiary_Unpost(ByVal loanno As String, ByVal docno As String, ByVal amount As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_Unpost_DeleteLoanSubsidiary",
                                       New SqlParameter("@LoanNo", loanno),
                                       New SqlParameter("@DocNo", docno),
                                       New SqlParameter("@amount", amount))
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Update_LoanSubsidiary_Unpost")
        End Try
    End Sub

    Private Sub _ApplyAmortization_ComputeBalances(ByVal loanno As String, ByVal dtdate As String,
                                                   ByVal docnum As String, ByVal amount As Decimal, ByVal acctType As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_InsertUpdate_AmortizationBalances",
                                       New SqlParameter("@DocNumber", docnum),
                                       New SqlParameter("@LoanRef", loanno),
                                       New SqlParameter("@Amount", amount),
                                       New SqlParameter("@Date", dtdate),
                                       New SqlParameter("@AcctType", acctType))
            'SqlHelper.ExecuteNonQuery(con.cnstring, "_Insert_Amortization_Payments",
            '                           New SqlParameter("@LoanNo", loanno),
            '                           New SqlParameter("@fcDate", dtdate),
            '                           New SqlParameter("@fcDocNumber", docnum),
            '                           New SqlParameter("@AmountPay", amount))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub _Unpost_Amortization_balancesEntry(ByVal docnum As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_Unpost_AmortizationPayments",
                                       New SqlParameter("@DocNumber", docnum))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function isDetailSaved() As Boolean
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                ProgressBar1.Value += 1
                With dgvList
                    Dim acctcode As String = .Item("acctcode", i).Value.ToString
                    Dim acctName As String = .Item("accttitle", i).Value.ToString
                    Dim fxkey As String = IIf(.Item("fxkey", i).Value = "null", System.Guid.NewGuid.ToString, .Item("fxkey", i).Value.ToString)
                    Dim debit As Decimal = CDec(.Item("debit", i).Value.ToString)
                    Dim credit As Decimal = CDec(.Item("credit", i).Value.ToString)
                    Dim idno As String = .Item("idno", i).Value.ToString
                    Dim loanref As String = .Item("loanref", i).Value.ToString
                    Dim acctref As String = .Item("acctref", i).Value.ToString
                    Dim keyid As New Guid(fxkey)

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_InsertUpdate_EntryDetails",
                           New SqlParameter("@fiEntryNo", txtORNo.Text),
                           New SqlParameter("@acctCode", acctcode),
                           New SqlParameter("@acctName", acctName),
                           New SqlParameter("@fxKey", keyid),
                           New SqlParameter("@fdDebit", debit),
                           New SqlParameter("@fdCredit", credit),
                           New SqlParameter("@fcMemo", txtParticulars.Text),
                           New SqlParameter("@IDNo", idno),
                           New SqlParameter("@transDate", dtDate.Value),
                           New SqlParameter("@fcLoanRef", loanref),
                           New SqlParameter("@fcAccRef", acctref))
                End With
            Next
            Return True
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "isDetailSaved")
            Return False
        End Try
    End Function

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        If txtORNo.Text <> "" Then
            If dgvList.Rows(0).Cells(5).Value = "" Then
                MsgBox("No Entry to save.", vbInformation, "Oops.")
                Exit Sub
            Else
                If isHeaderSaved() Then
                    _prepLoad()
                    ProgressBar1.Visible = True
                    If isDetailSaved() Then
                        updateddnumx()
                        MsgBox("Entry Successfully Saved!", vbInformation, "Done.")
                        RefreshingHAHA()
                        ProgressBar1.Visible = False
                        ProgressBar1.Value = 0
                        chkPosted.Enabled = True
                    End If
                End If
            End If
        Else
            MsgBox("No Document Number Available.", vbInformation, "Oops.")
            Exit Sub
        End If
    End Sub

    Private Sub UpdateLoanSubsidiary_DebitEntry(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal particulars As String, ByVal dDebit As Decimal, ByVal ischeck As Boolean, ByVal checkstatus As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "_Collections_Update_Subsidiary", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@Date", fdDate), _
                                      New SqlParameter("@DocNumber", docnum), _
                                      New SqlParameter("@Particulars", particulars), _
                                      New SqlParameter("@Debit", dDebit), _
                                      New SqlParameter("@isCheck", ischeck), _
                                      New SqlParameter("@CheckStatus", checkstatus))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateLoanSubsidiary_UnpostEntry(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal amountPay As Decimal, ByVal LoanType As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_UnpostedEntry", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@fcDate", fdDate), _
                                      New SqlParameter("@fcDocNumber", docnum), _
                                      New SqlParameter("@AmountPay", amountPay),
                                      New SqlParameter("@isLoan", LoanType))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub updateddnumx()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Update_DocNumberUsed",
                                       New SqlParameter("@docnumber", txtORNo.Text),
                                       New SqlParameter("@dateused", Date.Now))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SearchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchToolStripMenuItem.Click
        frmBSearch.StartPosition = FormStartPosition.CenterScreen
        frmBSearch.ShowDialog()
    End Sub

    Public Sub LoadHeader_OR()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_Select_EntryHeader",
                                      New SqlParameter("@fiEntryNo", txtORNo.Text))
        While rd.Read
            dtDate.Value = rd(0)
            txtParticulars.Text = rd(1)
            txtCreatedby.Text = rd(2)
            chkPosted.Checked = CBool(rd(3))
            chkCancelled.Checked = CBool(rd(4))
        End While
        If chkPosted.Checked = True Then
            isPosted()
        Else
            isUnposted()
        End If
    End Sub

    Private Sub RefreshingHAHA()
        iscolPrepare()
        LoadDetails_OR()
    End Sub

    Public Sub LoadDetails_OR()
        LoadHeader_OR()

        iscolPrepare()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_Select_EntryDetails",
                                      New SqlParameter("@fiEntryNo", txtORNo.Text))
        While rd.Read
            With dgvList
                .Rows.Add()
                .Item("idno", mIndex).Value = rd(0)
                .Item("clientname", mIndex).Value = rd(1)
                .Item("loanref", mIndex).Value = rd(2)
                .Item("acctref", mIndex).Value = rd(3)
                .Item("acctcode", mIndex).Value = rd(4)
                .Item("accttitle", mIndex).Value = rd(5)
                .Item("debit", mIndex).Value = rd(6)
                .Item("credit", mIndex).Value = rd(7)
                .Item("fxkey", mIndex).Value = rd(8).ToString
                .Item("fxkey", mIndex).Tag = rd(9).ToString
                mIndex += 1
            End With
        End While
        mIndex = 0
    End Sub

    Private Sub chkPosted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPosted.Click
        Select Case chkPosted.Checked
            Case False
                If MsgBox("Are you sure you want to unpost this document?", vbYesNo + MsgBoxStyle.Question, "Unpost") = vbYes Then
                    chkPosted.Checked = False
                    isUnposted()
                    'save
                    SubsidiaryUpdating()
                    If isHeaderSaved() Then
                        If isDetailSaved() Then
                            updateddnumx()
                        End If
                    End If
                    RefreshingHAHA()
                    Exit Sub
                Else
                    chkPosted.Checked = True
                    Exit Sub
                End If
            Case True
                If txtDebit.Text = "0.00" And txtCredit.Text = "0.00" And txtDifference.Text = "0.00" Then
                    MsgBox("Unable to proceed all amount is ZERO.", vbInformation, "Oops.")
                    chkPosted.Checked = False
                    Exit Sub
                End If
                If txtCredit.Text <> txtDebit.Text Then
                    MsgBox("Unable to post, ENTRY not BALANCE.", vbInformation, "Oops.")
                    chkPosted.Checked = False
                    Exit Sub
                Else
                    If MsgBox("Are you sure you want to post this document?", vbYesNo + MsgBoxStyle.Question, "Post") = vbYes Then
                        chkPosted.Checked = True
                        isPosted()
                        'save
                        SubsidiaryUpdating()
                        If isHeaderSaved() Then
                            If isDetailSaved() Then
                                updateddnumx()
                            End If
                        End If
                        RefreshingHAHA()
                        Exit Sub
                    Else
                        chkPosted.Checked = False
                        Exit Sub
                    End If
                End If
        End Select
    End Sub

    Private Sub UpdateSubsidiary(ByVal AccountRef As String, ByVal RefNo As String, ByVal debit As Decimal, ByVal credit As Decimal, ByVal Memo As String, ByVal fdDate As Date, ByVal PayMethod As String, ByVal cStatus As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_Update", _
                                      New SqlParameter("@AccountRef", AccountRef), _
                                      New SqlParameter("@RefNo", RefNo), _
                                      New SqlParameter("@Debit", debit), _
                                      New SqlParameter("@Credit", credit), _
                                      New SqlParameter("@Description", Memo), _
                                      New SqlParameter("@fdDate", fdDate), _
                                      New SqlParameter("@PaymentMethod", PayMethod), _
                                      New SqlParameter("@CheckStatus", cStatus))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkCancelled_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCancelled.Click
        Select Case chkCancelled.Checked
            Case True
                If MsgBox("Are you sure you want to cancelled this document?", vbYesNo + MsgBoxStyle.Question, "Cancel") = vbYes Then
                    chkCancelled.Checked = True
                    Exit Sub
                Else
                    chkCancelled.Checked = False
                    Exit Sub
                End If
        End Select
    End Sub

    Private Sub isPosted()
        dgvList.ReadOnly = True
        dtDate.Enabled = False
        txtParticulars.ReadOnly = True
        txtCollector.ReadOnly = True
        txtCreatedby.ReadOnly = True
    End Sub
    Private Sub isUnposted()
        dgvList.ReadOnly = False
        dtDate.Enabled = True
        txtParticulars.ReadOnly = False
        txtCollector.ReadOnly = False
        txtCreatedby.ReadOnly = False
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        If chkPosted.Checked = True Then
            MsgBox("Unpost document first to continue.", vbInformation, "Delete Document")
            Exit Sub
        Else
            If MsgBox("Are you sure you want to delete this document?", vbYesNo + MsgBoxStyle.Question, "Delete") = vbYes Then
                If isDeleted() Then
                    upddocunused()
                    iscolPrepare()
                    clfields()
                    isUnposted()
                    ldDocnum()
                End If
            End If
        End If
    End Sub

    Private Sub upddocunused()
        Dim sSQLCmd As String = "update mDocNumber Set fdDateUsed= Null where fcDocNumber='" & txtORNo.Text & "' "
        SqlHelper.ExecuteScalar(con.cnstring, CommandType.Text, sSQLCmd)
    End Sub

    Private Function isDeleted() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_Delete_Header_Details",
                                       New SqlParameter("@fiEntryNo", txtORNo.Text))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub ispCheckEntry()
        dgvCheck.DataSource = Nothing
        dgvCheck.Columns.Clear()
        dgvCheck.Rows.Clear()
        Dim bank As New DataGridViewTextBoxColumn
        Dim chknumber As New DataGridViewTextBoxColumn
        Dim chkdate As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        Dim maker As New DataGridViewTextBoxColumn
        Dim payee As New DataGridViewTextBoxColumn
        With bank
            .Name = "bank"
            .HeaderText = "Bank"
        End With
        With chknumber
            .Name = "chknumber"
            .HeaderText = "Check Number"
        End With
        With chkdate
            .Name = "chkdate"
            .HeaderText = "Check Date"
        End With
        With amount
            .Name = "amount"
            .HeaderText = "Amount"
        End With
        With maker
            .Name = "maker"
            .HeaderText = "Maker"
        End With
        With payee
            .Name = "payee"
            .HeaderText = "Payee"
        End With
        With dgvCheck
            .Columns.Add(bank)
            .Columns.Add(chknumber)
            .Columns.Add(chkdate)
            .Columns.Add(amount)
            .Columns.Add(maker)
            .Columns.Add(payee)
        End With
    End Sub

    Private Sub AddChecjToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddChecjToolStripMenuItem.Click
        ispCheckEntry() 'ongoing
        dgvCheck.Rows.Add()
    End Sub

    Private Sub dgvCheck_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvCheck.KeyDown
        Select Case dgvCheck.CurrentCellAddress.X
            Case 2
                If e.KeyCode = Keys.Enter Then
                    dtCheckDate.Left = dgvCheck.CurrentCellAddress.X + 430
                    dtCheckDate.Top = dgvCheck.CurrentCellAddress.Y + 300
                    dtCheckDate.Visible = True
                    dtCheckDate.Select()
                End If
            Case Else
                dtCheckDate.Visible = False
        End Select
    End Sub

    Private Sub dtCheckDate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtCheckDate.KeyDown
        If e.KeyCode = Keys.Enter Then
            dgvCheck.SelectedCells(0).Value = dtCheckDate.SelectionRange.Start.ToString("MM/dd/yyyy")
            dtCheckDate.Visible = False
        End If
        If e.KeyCode = Keys.Escape Then
            dtCheckDate.Visible = False
        End If
    End Sub

    Private Sub DeleteRow(ByVal key As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Delete_LineRow_Entry",
                           New SqlParameter("@key", key))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        If chkPosted.Checked = True Then
            Exit Sub
        End If
        If e.KeyCode = Keys.Delete Then
            Try
                DeleteRow(dgvList.CurrentRow.Cells(8).Value)
                dgvList.Rows.Remove(dgvList.CurrentRow)
            Catch ex As Exception
                dgvList.Rows.Add()
            End Try
        End If
        Select Case dgvList.CurrentCellAddress.X
            Case 0
                If e.KeyCode = Keys.Enter Then
                    frmBrowseClients.StartPosition = FormStartPosition.CenterScreen
                    frmBrowseClients.ShowDialog()
                End If
            Case 1
                If e.KeyCode = Keys.Enter Then
                    frmBrowseClients.StartPosition = FormStartPosition.CenterScreen
                    frmBrowseClients.ShowDialog()
                End If
            Case 3
                If e.KeyCode = Keys.Enter Then
                    frmBrowseAcctReg.StartPosition = FormStartPosition.CenterScreen
                    frmBrowseAcctReg.ShowDialog()
                End If
            Case 4
                If e.KeyCode = Keys.Enter Then
                    brwseAcct()
                End If
            Case 5
                If e.KeyCode = Keys.Enter Then
                    brwseAcct()
                End If
            Case 6
                If e.KeyCode = Keys.Enter Then
                    prepNewRow()
                End If
            Case 7
                If e.KeyCode = Keys.Enter Then
                    prepNewRow()
                End If
        End Select
    End Sub

    Private Sub brwseAcct()
        frmBAccounts.StartPosition = FormStartPosition.CenterScreen
        frmBAccounts.ShowDialog()
    End Sub

    Private Sub prepNewRow()
        mIndex = dgvList.RowCount
        With dgvList
            .Rows.Add()
            ' mIndex += 1
            .Item("idno", mIndex).Value = " "
            .Item("clientname", mIndex).Value = " "
            .Item("loanref", mIndex).Value = " "
            .Item("acctref", mIndex).Value = " "
            .Item("acctcode", mIndex).Value = " "
            .Item("accttitle", mIndex).Value = " "
            .Item("debit", mIndex).Value = "0.00"
            .Item("credit", mIndex).Value = "0.00"
            .Item("fxkey", mIndex).Value = "null"
        End With
        mIndex = 0
    End Sub

    Public Sub ldacctRegs()
        mIndex = dgvList.RowCount
        With dgvList
            '.Rows.Add()
            ' mIndex += 1
            '.Item("idno", mIndex).Value = idno
            '.Item("clientname", mIndex).Value = name
            .Item("loanref", mIndex).Value = " "
            '.Item("acctref", mIndex).Value = acctref
            '.Item("acctcode", mIndex).Value = acctcode
            '.Item("accttitle", mIndex).Value = accttitle
            .Item("debit", mIndex).Value = "0.00"
            .Item("credit", mIndex).Value = "0.00"
            .Item("fxkey", mIndex).Value = "null"
        End With
        mIndex = 0
    End Sub

    Private Sub btnImportCollections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportCollections.Click
        If ImportCollections() Then
            btnImportCollections.Text = "Import Collections - OR"
            btnImportCollections.ForeColor = Color.Black
        Else
            btnImportCollections.Text = "Import Collections - OR"
            btnImportCollections.ForeColor = Color.Black
        End If
    End Sub

    Private Function ImportCollections() As Boolean
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.FileName = "Select Excel File"
            Dim x As DialogResult = OpenFileDialog1.ShowDialog()
            If x = Windows.Forms.DialogResult.Cancel Then
                Exit Function
            End If
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath
            iscolPrepare()
            btnImportCollections.Text = "Importing..."
            btnImportCollections.ForeColor = Color.Blue
            Import()
            Return True
        Catch ex As Exception
            ' MsgBox("User Cancelled!", vbInformation, "...")
            MsgBox(ex.Message, vbInformation, "Import")
            Return False
        End Try
    End Function

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPAth.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader

        '=========================
        If chkPayLoans.Checked = True Then
            While rd.Read
                _PayLoans(rd(0), rd(6))
            End While
            mIndex = 0
        Else
            While rd.Read
                With dgvList
                    .Rows.Add()
                    If chkIncludeTIN.Checked = True Then
                        _IncludeTIN(rd(7), rd(4))
                    Else
                        .Item("idno", mIndex).Value = rd(0)
                        .Item("clientname", mIndex).Value = rd(1)
                        .Item("acctref", mIndex).Value = rd(3)
                    End If
                    If rd(2).ToString <> "" Then
                        If is_NotFound(rd(2)) Then
                            .Rows(mIndex).DefaultCellStyle.BackColor = Color.Red
                        End If
                    End If
                    .Item("loanref", mIndex).Value = rd(2)
                    '.Item("acctref", mIndex).Value = rd(3)
                    .Item("acctcode", mIndex).Value = rd(4)
                    .Item("accttitle", mIndex).Value = rd(5)
                    .Item("debit", mIndex).Value = "0.00"
                    .Item("credit", mIndex).Value = rd(6)
                    .Item("fxkey", mIndex).Value = "null"
                    mIndex += 1
                End With
            End While
            mIndex = 0
        End If
        MyConnection.Close()
    End Sub

    Private Sub _IncludeTIN(ByVal tin As String, ByVal acctCode As String)
        Try
            If tin = "" Or tin Is DBNull.Value Then
                Exit Sub
            End If
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_SearchMember_by_TIN",
                                          New SqlParameter("@TIN", tin))
            While rd.Read
                With dgvList
                    .Item("idno", mIndex).Value = rd(0)
                    .Item("clientname", mIndex).Value = rd(1)
                End With
            End While
            'Get Account Reference
            Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Collections_GetAcct_Reference",
                                                    New SqlParameter("@IDNo", dgvList.Item("idno", mIndex).Value),
                                                    New SqlParameter("@AcctCode", acctCode))
            If rd2.HasRows Then
                While rd2.Read
                    dgvList.Item("acctref", mIndex).Value = rd2(0)
                End While
            Else
                dgvList.Item("acctref", mIndex).Value = " "
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function _PayCharges(ByVal loanno As String, ByVal amount As Decimal) As Decimal
        Try
            If loanno = "" Or loanno Is DBNull.Value Then
                Exit Function
            Else
                Dim rd2 As SqlDataReader
                rd2 = SqlHelper.ExecuteReader(con.cnstring, "_Collections_Generate_Charges",
                                               New SqlParameter("@LoanNo", loanno))
                While rd2.Read
                    With dgvList
                        .Rows.Add()
                        .Item("idno", mIndex).Value = rd2(0)
                        .Item("clientname", mIndex).Value = rd2(1)
                        .Item("loanref", mIndex).Value = rd2(2)
                        .Item("acctref", mIndex).Value = rd2(3)
                        .Item("acctcode", mIndex).Value = rd2(4)
                        .Item("accttitle", mIndex).Value = rd2(5)
                        .Item("debit", mIndex).Value = "0.00"
                        If amount <> 0 Then
                            If rd2(6) = amount Then
                                .Item("credit", mIndex).Value = rd2(6)
                                amount = 0
                            Else
                                If rd2(6) > amount Then
                                    .Item("credit", mIndex).Value = amount
                                    amount = 0
                                Else
                                    .Item("credit", mIndex).Value = rd2(6)
                                    amount = amount - rd2(6)
                                End If
                            End If
                        Else
                            .Item("credit", mIndex).Value = "0.00"
                        End If

                        .Item("fxkey", mIndex).Value = "null"
                        .Item("fxkey", mIndex).Tag = "null"
                        mIndex += 1
                    End With
                End While
                Return amount
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_PayCharges")
        End Try
    End Function

    Private Sub _PayLoans(ByVal idno As String,
                          ByVal amount As Decimal)
        Try
            If idno = "" Or idno Is DBNull.Value Then
                Exit Sub
            End If

            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_Generate_Loan_Breakdown",
                                          New SqlParameter("@IDNo", idno))
            While rd.Read
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex).Value = rd(0)
                    .Item("clientname", mIndex).Value = rd(1)
                    .Item("loanref", mIndex).Value = rd(2)
                    .Item("acctref", mIndex).Value = rd(3)
                    .Item("acctcode", mIndex).Value = rd(4)
                    .Item("accttitle", mIndex).Value = rd(5)
                    .Item("debit", mIndex).Value = "0.00"
                    If amount <> 0 Then
                        If rd(6) = amount Then
                            .Item("credit", mIndex).Value = rd(6)
                            amount = 0
                        Else
                            If rd(6) > amount Then
                                .Item("credit", mIndex).Value = amount
                                amount = 0
                            Else
                                .Item("credit", mIndex).Value = rd(6)
                                amount = amount - rd(6)
                            End If
                        End If
                        .Item("fxkey", mIndex).Value = "null"
                        .Item("fxkey", mIndex).Tag = "null"
                        mIndex += 1
                        amount = _PayCharges(rd(2), amount)
                    Else
                        .Item("credit", mIndex).Value = "0.00"
                        .Item("fxkey", mIndex).Value = "null"
                        .Item("fxkey", mIndex).Tag = "null"
                        mIndex += 1
                    End If
                End With
            End While
            'check over payment
            If amount <> 0 Then
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex).Value = " "
                    .Item("clientname", mIndex).Value = " "
                    .Item("loanref", mIndex).Value = " "
                    .Item("acctref", mIndex).Value = " "
                    .Item("acctcode", mIndex).Value = " "
                    .Item("accttitle", mIndex).Value = " "
                    .Item("debit", mIndex).Value = "0.00"
                    .Item("credit", mIndex).Value = amount
                    .Item("fxkey", mIndex).Value = "null"
                    mIndex += 1
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_PayLoans")
        End Try
    End Sub

    Private Sub _PayLoans2(ByVal loanno As String,
                      ByVal amount As Decimal)
        Try
            If loanno = "" Or loanno Is DBNull.Value Then
                Exit Sub
            End If

            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_Generate_Loan_Breakdown_2",
                                          New SqlParameter("@LoanNo", loanno))
            While rd.Read
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex).Value = rd(0)
                    .Item("clientname", mIndex).Value = rd(1)
                    .Item("loanref", mIndex).Value = rd(2)
                    .Item("acctref", mIndex).Value = rd(3)
                    .Item("acctcode", mIndex).Value = rd(4)
                    .Item("accttitle", mIndex).Value = rd(5)
                    .Item("debit", mIndex).Value = "0.00"
                    If amount <> 0 Then
                        If rd(6) = amount Then
                            .Item("credit", mIndex).Value = rd(6)
                            amount = 0
                        Else
                            If rd(6) > amount Then
                                .Item("credit", mIndex).Value = amount
                                amount = 0
                            Else
                                .Item("credit", mIndex).Value = rd(6)
                                amount = amount - rd(6)
                            End If
                        End If
                        .Item("fxkey", mIndex).Value = "null"
                        .Item("fxkey", mIndex).Tag = "null"
                        mIndex += 1
                        amount = _PayCharges(rd(2), amount)
                    Else
                        .Item("credit", mIndex).Value = "0.00"
                        .Item("fxkey", mIndex).Value = "null"
                        .Item("fxkey", mIndex).Tag = "null"
                        mIndex += 1
                    End If
                End With
            End While
            'check over payment
            If amount <> 0 Then
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex).Value = " "
                    .Item("clientname", mIndex).Value = " "
                    .Item("loanref", mIndex).Value = " "
                    .Item("acctref", mIndex).Value = " "
                    .Item("acctcode", mIndex).Value = " "
                    .Item("accttitle", mIndex).Value = " "
                    .Item("debit", mIndex).Value = "0.00"
                    .Item("credit", mIndex).Value = amount
                    .Item("fxkey", mIndex).Value = "null"
                    mIndex += 1
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_PayLoans")
        End Try
    End Sub

    Private Sub btnCollectionsBilling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCollectionsBilling.Click
        frmCollectionsBilling.StartPosition = FormStartPosition.CenterScreen
        frmCollectionsBilling.ShowDialog()
    End Sub

    Public Sub _generate_Billing_Entry(ByVal idno As String, ByVal amount As Decimal, ByVal docnumber As String, ByVal nym As String, ByVal underpay As Decimal)
        Try
            If idno = "" Then
                Exit Sub
            End If
            Dim overpay As Decimal
            overpay = _GetBilling_Details(idno, docnumber, amount, nym, underpay)
            If overpay < 0 Then
                _Gen_OverpaymentEntry(idno, (overpay * -1))
            Else
                'MsgBox("positive")
            End If
            _Delete_ErrLine()
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_generate_Billing_Entry")
        End Try
    End Sub

    '### Generate Entry for over payment using selected account
    Public _AcctCode As String
    Public _AcctTitle As String
    Private Sub _Gen_OverpaymentEntry(ByVal idno As String, ByVal amt As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetOverpayment_Acct",
                                                               New SqlParameter("@IDNo", idno),
                                                               New SqlParameter("@AcctCode", _AcctCode))
            If rd.HasRows Then
                While rd.Read
                    With dgvList
                        .Rows.Add()
                        .Item("idno", mIndex2).Value = rd(0)
                        .Item("clientname", mIndex2).Value = rd(1)
                        .Item("loanref", mIndex2).Value = " "
                        .Item("acctref", mIndex2).Value = rd(2)
                        .Item("acctcode", mIndex2).Value = _AcctCode
                        .Item("accttitle", mIndex2).Value = _AcctTitle
                        .Item("debit", mIndex2).Value = "0.00"
                        .Item("credit", mIndex2).Value = amt
                        .Item("fxkey", mIndex2).Value = "null"
                    End With
                End While
            Else
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex2).Value = " "
                    .Item("clientname", mIndex2).Value = " "
                    .Item("loanref", mIndex2).Value = " "
                    .Item("acctref", mIndex2).Value = " "
                    .Item("acctcode", mIndex2).Value = _AcctCode
                    .Item("accttitle", mIndex2).Value = _AcctTitle
                    .Item("debit", mIndex2).Value = "0.00"
                    .Item("credit", mIndex2).Value = amt
                    .Item("fxkey", mIndex2).Value = "null"
                End With
                mIndex2 += 1
            End If

        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Gen_OverpaymentEntry")
        End Try
    End Sub
    '########

    Private Function _GetBilling_Details(ByVal idno As String, ByVal key As String, ByVal collected As Decimal, ByVal nym As String, ByVal underpay As Decimal) As Decimal
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetBilling_Details",
                                                               New SqlParameter("@key", key),
                                                               New SqlParameter("@idno", idno))
            While rd.Read
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex2).Value = idno
                    .Item("clientname", mIndex2).Value = nym
                    .Item("loanref", mIndex2).Value = rd(0)
                    .Item("acctref", mIndex2).Value = rd(1)
                    .Item("acctcode", mIndex2).Value = rd(2)
                    .Item("accttitle", mIndex2).Value = rd(3)
                    '.Item("currentdue", mIndex2).Value = rd(4)
                    '.Item("pastdue", mIndex2).Value = rd(5)
                    .Item("debit", mIndex2).Value = "0.00"
                    .Item("credit", mIndex2).Value = rd(6)
                    If collected = 0 Then
                        .Item("credit", mIndex2).Value = "0.00"
                    Else
                        If .Item("credit", mIndex2).Value < collected Then
                            .Item("credit", mIndex2).Value = .Item("credit", mIndex2).Value
                            collected = collected - .Item("credit", mIndex2).Value
                        Else
                            .Item("credit", mIndex2).Value = .Item("credit", mIndex2).Value  '- collected
                            collected = collected - .Item("credit", mIndex2).Value
                        End If
                        If .Item("credit", mIndex2).Value = collected Then
                            .Item("credit", mIndex2).Value = .Item("credit", mIndex2).Value
                            collected = 0
                        End If
                    End If
                    .Item("fxkey", mIndex2).Value = "null"
                    mIndex2 += 1
                    collected = _GetBilling_Detail_Charges(rd(0), key, collected, nym, idno, underpay)
                End With
            End While
            Return underpay  'get over payment
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_GetBilling_Details")
        End Try
    End Function
    Private Function _GetBilling_Detail_Charges(ByVal loanno As String, ByVal docnumber As String, ByVal collected As Decimal, ByVal nym As String, ByVal idno As String, ByVal underpay As Decimal) As Decimal
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_GetBilling_Details_Charges",
                                                               New SqlParameter("@LoanNo", loanno),
                                                               New SqlParameter("@DocNumber", docnumber))
            While rd.Read
                With dgvList
                    .Rows.Add()
                    .Item("idno", mIndex2).Value = idno
                    .Item("clientname", mIndex2).Value = nym
                    .Item("loanref", mIndex2).Value = rd(0)
                    .Item("acctref", mIndex2).Value = rd(1)
                    .Item("acctcode", mIndex2).Value = rd(2)
                    .Item("accttitle", mIndex2).Value = rd(3)
                    '.Item("currentdue", mIndex2).Value = rd(4)
                    '.Item("pastdue", mIndex2).Value = rd(5)
                    .Item("debit", mIndex2).Value = "0.00"
                    .Item("credit", mIndex2).Value = rd(6)
                    If collected = 0 Or .Item("credit", mIndex2).Value = 0 Then
                        .Item("credit", mIndex2).Value = "0.00"
                    Else
                        If .Item("credit", mIndex2).Value < collected Then
                            .Item("credit", mIndex2).Value = .Item("credit", mIndex2).Value
                            collected = collected - .Item("credit", mIndex2).Value
                        End If
                        If .Item("credit", mIndex2).Value > collected Then
                            .Item("credit", mIndex2).Value = .Item("credit", mIndex2).Value - underpay '- collected
                            collected = collected - .Item("credit", mIndex2).Value
                        End If
                        If .Item("credit", mIndex2).Value = collected Then
                            .Item("credit", mIndex2).Value = .Item("credit", mIndex2).Value
                            collected = 0
                        End If
                    End If
                    .Item("fxkey", mIndex2).Value = "null"
                    '.Item("collections", mIndex2).Value = "0.00" 'ongoing
                    mIndex2 += 1
                End With
            End While
            Return collected
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_GetBilling_Detail_Charges")
            Return 0
        End Try
    End Function

    Private Sub _Delete_ErrLine()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                If .Item("acctcode", i).Value = "" Or .Item("acctcode", i).Value Is Nothing Then
                    .Rows.Remove(.Rows(i))
                End If
            End With
        Next
    End Sub

    Private Sub btnCheckDataStat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckDataStat.Click
        _prepLoad()
        For i As Integer = 0 To dgvList.RowCount - 1
            ProgressBar1.Value += 1
            With dgvList
                If .Item("loanref", i).Value.ToString <> "" Then
                    If is_NotFound(.Item("loanref", i).Value) = True Then
                        .Rows(i).DefaultCellStyle.BackColor = Color.Red
                    End If
                End If
            End With
        Next
        ProgressBar1.Visible = False
        ProgressBar1.Value = 0
    End Sub
    Private Function is_NotFound(ByVal loanref As String) As Boolean
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Check_LoanRef_NotFound",
                                                    New SqlParameter("@LoanRef", loanref))
            If rd.HasRows Then
                While rd.Read
                    Return False
                End While
            Else
                Return True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub _prepLoad()
        ProgressBar1.Visible = True
        For i As Integer = 0 To dgvList.RowCount - 1
            ProgressBar1.Maximum += 1
        Next
    End Sub

End Class