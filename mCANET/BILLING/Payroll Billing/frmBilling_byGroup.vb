﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBilling_byGroup

    Private con As New Clsappconfiguration
    Dim i As Integer = 0

    Private Sub frmBilling_byGroup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isGroupLoad() Then
            Panel2.Enabled = False
            chkAllGroup.Checked = True
            cboGroup.Enabled = False
        End If
    End Sub

    Private Function isGroupLoad() As Boolean
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Billing_Select_Group")
            While rd.Read
                cboGroup.Items.Add(rd(0))
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isCreateBilling(ByVal xdate As Date, ByVal xgrp As String) As Boolean
        Try
            Dim rd As SqlDataReader
            btnCreate.Text = "Loading..."
            btnCreate.Enabled = False
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Billing_CreateBill_byDate",
                                           New SqlParameter("@xDate", xdate),
                                           New SqlParameter("@Group", xgrp))
            While rd.Read
                With dgvList
                    .Rows.Add()
                    .Item("loanref", i).Value = rd(0)
                    .Item("acctcode", i).Value = rd(1)
                    .Item("acctname", i).Value = rd(2)
                    .Item("idno", i).Value = rd(3)
                    .Item("clientname", i).Value = rd(4)
                    .Item("amount", i).Value = rd(5)
                    .Item("principal", i).Value = rd(6)
                    .Item("interest", i).Value = rd(7)
                    .Item("servicefee", i).Value = rd(8)
                    .Item("p_principal", i).Value = rd(9)
                    .Item("p_interest", i).Value = rd(10)
                    .Item("p_servicefee", i).Value = rd(11)
                    i += 1
                End With
            End While
            Return True
        Catch ex As Exception
            Throw
            Return False
        End Try
    End Function

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        If isPrepareColumns() Then
            If chkAllGroup.Checked = True Then
                If isCreateBilling(dtBillDate.Text, "All") Then
                    btnCreate.Text = "Create Billing"
                    btnCreate.Enabled = True
                    i = 0
                End If
            Else
                If isCreateBilling(dtBillDate.Text, cboGroup.Text) Then
                    btnCreate.Text = "Create Billing"
                    btnCreate.Enabled = True
                    i = 0
                End If
            End If
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        If MsgBox("Are you sure?", vbYesNo + MsgBoxStyle.Question, "Exit Billing List") = vbYes Then
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        If isPrepareColumns() Then
            Reconfgred()
            Loadfffdn()
        End If
    End Sub

    Private Sub Loadfffdn()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Billing_SelectDocNumber_Top")
        While rd.Read
            txtDocNumber.Text = rd(0)
        End While
    End Sub

    Private Sub Reconfgred()
        Panel2.Enabled = True
        txtCreatedBy.Text = ""
        txtDocNumber.Text = ""
        dtBillDate.Value = Date.Now
        chkPost.Checked = False
    End Sub

    Private Sub FormatNumValues()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                .Rows(i).Cells(5).Value = Format(CDec(.Rows(i).Cells(5).Value), "##,##0.00")
                .Rows(i).Cells(6).Value = Format(CDec(.Rows(i).Cells(6).Value), "##,##0.00")
                .Rows(i).Cells(7).Value = Format(CDec(.Rows(i).Cells(7).Value), "##,##0.00")
                .Rows(i).Cells(8).Value = Format(CDec(.Rows(i).Cells(8).Value), "##,##0.00")
            End With
        Next
    End Sub

    Private Sub dgvList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellValueChanged
        Dim totBillAmt As Decimal = 0.0
        Dim totPrincipal As Decimal = 0.0
        Dim totInterest As Decimal = 0.0
        Dim totServicefee As Decimal = 0.0
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                totBillAmt = totBillAmt + CDec(.Rows(i).Cells(5).Value)
                totPrincipal = totPrincipal + CDec(.Rows(i).Cells(6).Value)
                totInterest = totInterest + CDec(.Rows(i).Cells(7).Value)
                totServicefee = totServicefee + CDec(.Rows(i).Cells(8).Value)
            End With
        Next
        txtTotalBill.Text = Format(totBillAmt, "##,##0.00")
        txtTotPrincipal.Text = Format(totPrincipal, "##,##0.00")
        txtTotInterest.Text = Format(totInterest, "##,##0.00")
        txtTotServiceFee.Text = Format(totServicefee, "##,##0.00")
        FormatNumValues()
    End Sub

    Public Function isPrepareColumns() As Boolean
        Try
            dgvList.DataSource = Nothing
            dgvList.Columns.Clear()
            dgvList.Rows.Clear()

            Dim loanref As New DataGridViewTextBoxColumn
            Dim acctcode As New DataGridViewTextBoxColumn
            Dim acctname As New DataGridViewTextBoxColumn
            Dim idno As New DataGridViewTextBoxColumn
            Dim clientname As New DataGridViewTextBoxColumn
            Dim amount As New DataGridViewTextBoxColumn
            Dim principal As New DataGridViewTextBoxColumn
            Dim interest As New DataGridViewTextBoxColumn
            Dim servicefee As New DataGridViewTextBoxColumn
            Dim p_principal As New DataGridViewTextBoxColumn
            Dim p_interest As New DataGridViewTextBoxColumn
            Dim p_servicefee As New DataGridViewTextBoxColumn

            With loanref
                .Name = "loanref"
                .HeaderText = "Loan Ref."
            End With
            With acctcode
                .Name = "acctcode"
                .HeaderText = "Acct. Code"
            End With
            With acctname
                .Name = "acctname"
                .HeaderText = "Acct. Name"
            End With
            With idno
                .Name = "idno"
                .HeaderText = "ID No."
            End With
            With clientname
                .Name = "clientname"
                .HeaderText = "Client Name"
            End With
            With amount
                .Name = "amount"
                .HeaderText = "Amount"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            'Current Due
            With principal
                .Name = "principal"
                .HeaderText = "Principal"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With interest
                .Name = "interest"
                .HeaderText = "Interest"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With servicefee
                .Name = "servicefee"
                .HeaderText = "Service Fee"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            'Past Due
            With p_principal
                .Name = "p_principal"
                .HeaderText = "Principal"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With p_interest
                .Name = "p_interest"
                .HeaderText = "Interest"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With p_servicefee
                .Name = "p_servicefee"
                .HeaderText = "Service Fee"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With dgvList
                .Columns.Add(loanref)
                .Columns.Add(acctcode)
                .Columns.Add(acctname)
                .Columns.Add(idno)
                .Columns.Add(clientname)
                .Columns.Add(amount)
                .Columns.Add(principal)
                .Columns.Add(interest)
                .Columns.Add(servicefee)
                .Columns.Add(p_principal)
                .Columns.Add(p_interest)
                .Columns.Add(p_servicefee)
            End With
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub chkAllGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllGroup.Click
        If chkAllGroup.Checked = True Then
            cboGroup.Enabled = False
            Exit Sub
        End If
        If chkAllGroup.Checked = False Then
            cboGroup.Enabled = True
            Exit Sub
        End If
    End Sub

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        If txtDocNumber.Text <> "" Then
            If isHeaderSaved() Then
                If isDetailSaved() Then
                    updateddnumx()
                    MsgBox("Your data has been successfully saved!", vbInformation, "Success")
                End If
            End If
        Else
            MsgBox("Select Document No. to Continue.", vbInformation, "Oops.")
        End If
    End Sub

    Private Sub updateddnumx()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Update_DocNumberUsed",
                                       New SqlParameter("@docnumber", txtDocNumber.Text),
                                       New SqlParameter("@dateused", Date.Now))
        Catch ex As Exception

        End Try
    End Sub

    Private Function isHeaderSaved() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_InsertUpdate_Header",
                                       New SqlParameter("@fcDocNumber", txtDocNumber.Text),
                                       New SqlParameter("@fcCreatedBy", txtCreatedBy.Text),
                                       New SqlParameter("@dtDate", dtBillDate.Value),
                                       New SqlParameter("@fbPosted", chkPost.Checked))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isDetailSaved() As Boolean
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList

                    Dim loanref As String = .Item("loanref", i).Value.ToString
                    Dim acctcode As String = .Item("acctcode", i).Value.ToString
                    Dim acctname As String = .Item("acctname", i).Value.ToString
                    Dim idno As String = .Item("idno", i).Value.ToString
                    Dim clientname As String = .Item("clientname", i).Value.ToString
                    Dim amount As Decimal = CDec(.Item("amount", i).Value.ToString)
                    Dim principal As Decimal = CDec(.Item("principal", i).Value.ToString)
                    Dim interest As Decimal = CDec(.Item("interest", i).Value.ToString)
                    Dim servicefee As Decimal = CDec(.Item("servicefee", i).Value.ToString)
                    Dim p_principal As Decimal = CDec(.Item("p_principal", i).Value.ToString)
                    Dim p_interest As Decimal = CDec(.Item("p_interest", i).Value.ToString)
                    Dim p_servicefee As Decimal = CDec(.Item("p_servicefee", i).Value.ToString)

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_InsertUpdate_Details",
                                               New SqlParameter("@fcDocNumber", txtDocNumber.Text),
                                               New SqlParameter("@fcLoanRef", loanref),
                                               New SqlParameter("@fcAcctCode", acctcode),
                                               New SqlParameter("@fcAcctName", acctname),
                                               New SqlParameter("@fcIDNo", idno),
                                               New SqlParameter("@fcClientName", clientname),
                                               New SqlParameter("@fdAmount", amount),
                                               New SqlParameter("@fdPrincipal", principal),
                                               New SqlParameter("@fdInterest", interest),
                                               New SqlParameter("@fdServiceFee", servicefee),
                                               New SqlParameter("@fdPastPrincipal", p_principal),
                                               New SqlParameter("@fdPastInterest", p_interest),
                                               New SqlParameter("@fdPastServiceFee", p_servicefee))

                End With
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub SearchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchToolStripMenuItem.Click
        frmSearch_Billing.StartPosition = FormStartPosition.CenterScreen
        frmSearch_Billing.ShowDialog()
    End Sub

    Public Function isHeaderLoaded(ByVal docnumber As String) As Boolean
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Billing_Select_Header",
                                       New SqlParameter("@DocNumber", docnumber))
            While rd.Read
                ' txtDocNumber.Text = rd(0)
                txtCreatedBy.Text = rd(1).ToString
                dtBillDate.Value = CDate(rd(2))
                chkPost.Checked = CBool(rd(3))
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function isDetailsLoaded(ByVal docnumber As String) As Boolean
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Billing_Select_Details",
                                           New SqlParameter("@fcDocNumber", docnumber))
            While rd.Read
                With dgvList
                    .Rows.Add()
                    .Item("loanref", i).Value = rd(0)
                    .Item("acctcode", i).Value = rd(1)
                    .Item("acctname", i).Value = rd(2)
                    .Item("idno", i).Value = rd(3)
                    .Item("clientname", i).Value = rd(4)
                    .Item("amount", i).Value = rd(5)
                    .Item("principal", i).Value = rd(6)
                    .Item("interest", i).Value = rd(7)
                    .Item("servicefee", i).Value = rd(8)
                    .Item("p_principal", i).Value = rd(9)
                    .Item("p_interest", i).Value = rd(10)
                    .Item("p_servicefee", i).Value = rd(11)
                    i += 1
                End With
            End While
            i = 0
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        If txtDocNumber.Text <> "" Then
            If MsgBox("Are you sure you want to delete this billing?", vbYesNo + MsgBoxStyle.Question, "Confirmation") = vbYes Then
                Try
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_Delete_HeaderDetails",
                           New SqlParameter("@docnumber", txtDocNumber.Text))

                    MsgBox("Delete Success.", vbInformation, "Done.")
                    If isPrepareColumns() Then
                        i = 0
                        Reconfgred()
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                Exit Sub
            End If
        Else
            MsgBox("No data to be deleted!", vbInformation, "Oops.")
        End If
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        frmExportLoanDeductions.isReportLoaded(txtDocNumber.Text)
        frmExportLoanDeductions.StartPosition = FormStartPosition.CenterScreen
        frmExportLoanDeductions.ShowDialog()
    End Sub

    Private Sub btnBrowseDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseDoc.Click
        frmBrowse_BDoc.StartPosition = FormStartPosition.CenterScreen
        frmBrowse_BDoc.ShowDialog()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmExportLoanDeductions.isPayroll(txtDocNumber.Text)
        frmExportLoanDeductions.StartPosition = FormStartPosition.CenterScreen
        frmExportLoanDeductions.ShowDialog()
    End Sub

End Class