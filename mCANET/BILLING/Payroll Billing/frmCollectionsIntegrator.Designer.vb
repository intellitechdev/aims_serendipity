﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCollectionsIntegrator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.btnCheckDataStat = New System.Windows.Forms.Button()
        Me.btnCollectionsBilling = New System.Windows.Forms.Button()
        Me.chkIncludeTIN = New System.Windows.Forms.CheckBox()
        Me.chkPayLoans = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.btnImportCollections = New System.Windows.Forms.Button()
        Me.chkCancelled = New System.Windows.Forms.CheckBox()
        Me.btnGetBilling = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtORNo = New System.Windows.Forms.TextBox()
        Me.chkPosted = New System.Windows.Forms.CheckBox()
        Me.btnBrowseDoc = New System.Windows.Forms.Button()
        Me.txtDocNumber = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCollector = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCreatedby = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtParticulars = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDifference = New System.Windows.Forms.TextBox()
        Me.txtCredit = New System.Windows.Forms.TextBox()
        Me.txtDebit = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.AddChecjToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddBankToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.dgvCheck = New System.Windows.Forms.DataGridView()
        Me.dtCheckDate = New System.Windows.Forms.MonthCalendar()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.MenuStrip2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.SearchToolStripMenuItem, Me.SaveToolStripMenuItem, Me.NewToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 520)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1284, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CloseToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.button_cancel1
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(84, 20)
        Me.CloseToolStripMenuItem.Text = "Close [F5]"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.DeleteToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.deletebig
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(89, 20)
        Me.DeleteToolStripMenuItem.Text = "Delete [F4]"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.SearchToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Search
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(112, 20)
        Me.SearchToolStripMenuItem.Text = "Search OR  [F3]"
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.SaveToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(82, 20)
        Me.SaveToolStripMenuItem.Text = "Save [F2]"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.NewToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources._new
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(79, 20)
        Me.NewToolStripMenuItem.Text = "New [F1]"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.ProgressBar1)
        Me.Panel2.Controls.Add(Me.btnCheckDataStat)
        Me.Panel2.Controls.Add(Me.btnCollectionsBilling)
        Me.Panel2.Controls.Add(Me.chkIncludeTIN)
        Me.Panel2.Controls.Add(Me.chkPayLoans)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.txtPath)
        Me.Panel2.Controls.Add(Me.btnImportCollections)
        Me.Panel2.Controls.Add(Me.chkCancelled)
        Me.Panel2.Controls.Add(Me.btnGetBilling)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtORNo)
        Me.Panel2.Controls.Add(Me.chkPosted)
        Me.Panel2.Controls.Add(Me.btnBrowseDoc)
        Me.Panel2.Controls.Add(Me.txtDocNumber)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txtCollector)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.txtCreatedby)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.dtDate)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1284, 89)
        Me.Panel2.TabIndex = 2
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(467, 61)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(191, 23)
        Me.ProgressBar1.TabIndex = 29
        Me.ProgressBar1.Visible = False
        '
        'btnCheckDataStat
        '
        Me.btnCheckDataStat.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnCheckDataStat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCheckDataStat.Location = New System.Drawing.Point(664, 61)
        Me.btnCheckDataStat.Name = "btnCheckDataStat"
        Me.btnCheckDataStat.Size = New System.Drawing.Size(105, 23)
        Me.btnCheckDataStat.TabIndex = 28
        Me.btnCheckDataStat.Text = "Check Data"
        Me.btnCheckDataStat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCheckDataStat.UseVisualStyleBackColor = True
        Me.btnCheckDataStat.Visible = False
        '
        'btnCollectionsBilling
        '
        Me.btnCollectionsBilling.Location = New System.Drawing.Point(779, 60)
        Me.btnCollectionsBilling.Name = "btnCollectionsBilling"
        Me.btnCollectionsBilling.Size = New System.Drawing.Size(193, 23)
        Me.btnCollectionsBilling.TabIndex = 27
        Me.btnCollectionsBilling.Text = "Billing Payment"
        Me.btnCollectionsBilling.UseVisualStyleBackColor = True
        '
        'chkIncludeTIN
        '
        Me.chkIncludeTIN.AutoSize = True
        Me.chkIncludeTIN.Location = New System.Drawing.Point(779, 38)
        Me.chkIncludeTIN.Name = "chkIncludeTIN"
        Me.chkIncludeTIN.Size = New System.Drawing.Size(105, 17)
        Me.chkIncludeTIN.TabIndex = 26
        Me.chkIncludeTIN.Text = "Include TIN #"
        Me.chkIncludeTIN.UseVisualStyleBackColor = True
        '
        'chkPayLoans
        '
        Me.chkPayLoans.AutoSize = True
        Me.chkPayLoans.Location = New System.Drawing.Point(890, 38)
        Me.chkPayLoans.Name = "chkPayLoans"
        Me.chkPayLoans.Size = New System.Drawing.Size(84, 17)
        Me.chkPayLoans.TabIndex = 25
        Me.chkPayLoans.Text = "Pay Loans"
        Me.chkPayLoans.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(421, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "File Path :"
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(491, 11)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(285, 21)
        Me.txtPath.TabIndex = 23
        '
        'btnImportCollections
        '
        Me.btnImportCollections.Location = New System.Drawing.Point(782, 9)
        Me.btnImportCollections.Name = "btnImportCollections"
        Me.btnImportCollections.Size = New System.Drawing.Size(190, 23)
        Me.btnImportCollections.TabIndex = 22
        Me.btnImportCollections.Text = "Import Collections - OR"
        Me.btnImportCollections.UseVisualStyleBackColor = True
        '
        'chkCancelled
        '
        Me.chkCancelled.AutoSize = True
        Me.chkCancelled.Location = New System.Drawing.Point(1117, 67)
        Me.chkCancelled.Name = "chkCancelled"
        Me.chkCancelled.Size = New System.Drawing.Size(82, 17)
        Me.chkCancelled.TabIndex = 21
        Me.chkCancelled.Text = "Cancelled"
        Me.chkCancelled.UseVisualStyleBackColor = True
        '
        'btnGetBilling
        '
        Me.btnGetBilling.Location = New System.Drawing.Point(626, 35)
        Me.btnGetBilling.Name = "btnGetBilling"
        Me.btnGetBilling.Size = New System.Drawing.Size(143, 23)
        Me.btnGetBilling.TabIndex = 20
        Me.btnGetBilling.Text = "Get Billing"
        Me.btnGetBilling.UseVisualStyleBackColor = True
        Me.btnGetBilling.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(401, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Billing :"
        Me.Label5.Visible = False
        '
        'txtORNo
        '
        Me.txtORNo.BackColor = System.Drawing.Color.White
        Me.txtORNo.Location = New System.Drawing.Point(1027, 42)
        Me.txtORNo.Name = "txtORNo"
        Me.txtORNo.ReadOnly = True
        Me.txtORNo.Size = New System.Drawing.Size(163, 21)
        Me.txtORNo.TabIndex = 18
        '
        'chkPosted
        '
        Me.chkPosted.AutoSize = True
        Me.chkPosted.Location = New System.Drawing.Point(1027, 67)
        Me.chkPosted.Name = "chkPosted"
        Me.chkPosted.Size = New System.Drawing.Size(64, 17)
        Me.chkPosted.TabIndex = 17
        Me.chkPosted.Text = "Posted"
        Me.chkPosted.UseVisualStyleBackColor = True
        '
        'btnBrowseDoc
        '
        Me.btnBrowseDoc.Location = New System.Drawing.Point(1196, 38)
        Me.btnBrowseDoc.Name = "btnBrowseDoc"
        Me.btnBrowseDoc.Size = New System.Drawing.Size(39, 23)
        Me.btnBrowseDoc.TabIndex = 16
        Me.btnBrowseDoc.Text = "..."
        Me.btnBrowseDoc.UseVisualStyleBackColor = True
        '
        'txtDocNumber
        '
        Me.txtDocNumber.BackColor = System.Drawing.Color.White
        Me.txtDocNumber.Location = New System.Drawing.Point(457, 37)
        Me.txtDocNumber.Name = "txtDocNumber"
        Me.txtDocNumber.ReadOnly = True
        Me.txtDocNumber.Size = New System.Drawing.Size(163, 21)
        Me.txtDocNumber.TabIndex = 15
        Me.txtDocNumber.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(988, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "OR :"
        '
        'txtCollector
        '
        Me.txtCollector.BackColor = System.Drawing.Color.White
        Me.txtCollector.Location = New System.Drawing.Point(124, 12)
        Me.txtCollector.Name = "txtCollector"
        Me.txtCollector.Size = New System.Drawing.Size(220, 21)
        Me.txtCollector.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(37, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 13)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "Collector :"
        '
        'txtCreatedby
        '
        Me.txtCreatedby.BackColor = System.Drawing.Color.White
        Me.txtCreatedby.Location = New System.Drawing.Point(124, 38)
        Me.txtCreatedby.Name = "txtCreatedby"
        Me.txtCreatedby.Size = New System.Drawing.Size(220, 21)
        Me.txtCreatedby.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Created By :"
        '
        'dtDate
        '
        Me.dtDate.CalendarMonthBackground = System.Drawing.Color.White
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDate.Location = New System.Drawing.Point(1027, 11)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(163, 21)
        Me.dtDate.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(978, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Date :"
        '
        'txtParticulars
        '
        Me.txtParticulars.BackColor = System.Drawing.Color.White
        Me.txtParticulars.Location = New System.Drawing.Point(110, 8)
        Me.txtParticulars.Multiline = True
        Me.txtParticulars.Name = "txtParticulars"
        Me.txtParticulars.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtParticulars.Size = New System.Drawing.Size(802, 41)
        Me.txtParticulars.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Particulars :"
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(0, 89)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvList.Size = New System.Drawing.Size(1284, 262)
        Me.dgvList.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtDifference)
        Me.Panel1.Controls.Add(Me.txtCredit)
        Me.Panel1.Controls.Add(Me.txtDebit)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtParticulars)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.MenuStrip2)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 351)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1284, 169)
        Me.Panel1.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(992, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Difference :"
        '
        'txtDifference
        '
        Me.txtDifference.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.txtDifference.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDifference.ForeColor = System.Drawing.Color.Red
        Me.txtDifference.Location = New System.Drawing.Point(1075, 26)
        Me.txtDifference.Name = "txtDifference"
        Me.txtDifference.ReadOnly = True
        Me.txtDifference.Size = New System.Drawing.Size(133, 14)
        Me.txtDifference.TabIndex = 9
        Me.txtDifference.Text = "0.00"
        Me.txtDifference.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCredit
        '
        Me.txtCredit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.txtCredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCredit.Location = New System.Drawing.Point(1134, 6)
        Me.txtCredit.Name = "txtCredit"
        Me.txtCredit.ReadOnly = True
        Me.txtCredit.Size = New System.Drawing.Size(133, 14)
        Me.txtCredit.TabIndex = 8
        Me.txtCredit.Text = "0.00"
        Me.txtCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDebit
        '
        Me.txtDebit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.txtDebit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDebit.Location = New System.Drawing.Point(995, 6)
        Me.txtDebit.Name = "txtDebit"
        Me.txtDebit.ReadOnly = True
        Me.txtDebit.Size = New System.Drawing.Size(133, 14)
        Me.txtDebit.TabIndex = 7
        Me.txtDebit.Text = "0.00"
        Me.txtDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(937, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Totals :"
        '
        'MenuStrip2
        '
        Me.MenuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddChecjToolStripMenuItem, Me.AddBankToolStripMenuItem, Me.DeleteCheckToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(0, 52)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Size = New System.Drawing.Size(1284, 24)
        Me.MenuStrip2.TabIndex = 12
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'AddChecjToolStripMenuItem
        '
        Me.AddChecjToolStripMenuItem.Name = "AddChecjToolStripMenuItem"
        Me.AddChecjToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.AddChecjToolStripMenuItem.Text = "Add Check"
        '
        'AddBankToolStripMenuItem
        '
        Me.AddBankToolStripMenuItem.Name = "AddBankToolStripMenuItem"
        Me.AddBankToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.AddBankToolStripMenuItem.Text = "Add Bank"
        '
        'DeleteCheckToolStripMenuItem
        '
        Me.DeleteCheckToolStripMenuItem.Name = "DeleteCheckToolStripMenuItem"
        Me.DeleteCheckToolStripMenuItem.Size = New System.Drawing.Size(82, 20)
        Me.DeleteCheckToolStripMenuItem.Text = "Delete Check"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvCheck)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 76)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1284, 93)
        Me.Panel3.TabIndex = 11
        '
        'dgvCheck
        '
        Me.dgvCheck.AllowUserToAddRows = False
        Me.dgvCheck.AllowUserToDeleteRows = False
        Me.dgvCheck.AllowUserToResizeColumns = False
        Me.dgvCheck.AllowUserToResizeRows = False
        Me.dgvCheck.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCheck.BackgroundColor = System.Drawing.Color.White
        Me.dgvCheck.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCheck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCheck.Location = New System.Drawing.Point(0, 0)
        Me.dgvCheck.Name = "dgvCheck"
        Me.dgvCheck.RowHeadersVisible = False
        Me.dgvCheck.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCheck.Size = New System.Drawing.Size(1284, 93)
        Me.dgvCheck.TabIndex = 4
        '
        'dtCheckDate
        '
        Me.dtCheckDate.Location = New System.Drawing.Point(277, 436)
        Me.dtCheckDate.Name = "dtCheckDate"
        Me.dtCheckDate.TabIndex = 13
        Me.dtCheckDate.Visible = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'frmCollectionsIntegrator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1284, 544)
        Me.Controls.Add(Me.dgvList)
        Me.Controls.Add(Me.dtCheckDate)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip2
        Me.Name = "frmCollectionsIntegrator"
        Me.Text = "Collections Integrator"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        CType(Me.dgvCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtCollector As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtParticulars As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCreatedby As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseDoc As System.Windows.Forms.Button
    Friend WithEvents txtDocNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents chkPosted As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDifference As System.Windows.Forms.TextBox
    Friend WithEvents txtCredit As System.Windows.Forms.TextBox
    Friend WithEvents txtDebit As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents AddChecjToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents AddBankToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnGetBilling As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtORNo As System.Windows.Forms.TextBox
    Friend WithEvents chkCancelled As System.Windows.Forms.CheckBox
    Friend WithEvents dgvCheck As System.Windows.Forms.DataGridView
    Friend WithEvents dtCheckDate As System.Windows.Forms.MonthCalendar
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents btnImportCollections As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents chkPayLoans As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeTIN As System.Windows.Forms.CheckBox
    Friend WithEvents btnCollectionsBilling As System.Windows.Forms.Button
    Friend WithEvents btnCheckDataStat As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
End Class
