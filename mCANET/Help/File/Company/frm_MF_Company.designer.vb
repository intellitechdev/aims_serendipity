<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MF_Company
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_MF_Company))
        Me.lblCoInfo = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtcozipadd = New System.Windows.Forms.TextBox()
        Me.txtcocityadd = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtcolegal = New System.Windows.Forms.TextBox()
        Me.txtcotaxid = New System.Windows.Forms.TextBox()
        Me.txtcostadd = New System.Windows.Forms.TextBox()
        Me.txtcophone1 = New System.Windows.Forms.TextBox()
        Me.txtcofax1 = New System.Windows.Forms.TextBox()
        Me.txtcoemail = New System.Windows.Forms.TextBox()
        Me.txtcosite = New System.Windows.Forms.TextBox()
        Me.cbocoindustry = New System.Windows.Forms.ComboBox()
        Me.cbocororg = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cbofiscalYr = New System.Windows.Forms.ComboBox()
        Me.cbofiscalmonth = New System.Windows.Forms.ComboBox()
        Me.txtcorepass = New System.Windows.Forms.TextBox()
        Me.txtadminpassword = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnSegmnt = New System.Windows.Forms.Button()
        Me.cboconame = New System.Windows.Forms.ComboBox()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtcophone = New System.Windows.Forms.MaskedTextBox()
        Me.txtcofax = New System.Windows.Forms.MaskedTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ChckClassing = New System.Windows.Forms.CheckBox()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCoInfo
        '
        Me.lblCoInfo.AutoSize = True
        Me.lblCoInfo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoInfo.ForeColor = System.Drawing.Color.Blue
        Me.lblCoInfo.Location = New System.Drawing.Point(9, 20)
        Me.lblCoInfo.Name = "lblCoInfo"
        Me.lblCoInfo.Size = New System.Drawing.Size(233, 16)
        Me.lblCoInfo.TabIndex = 0
        Me.lblCoInfo.Text = "Please enter Project Information..."
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(35, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Project Name"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Legal Name"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(35, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Tax ID"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(35, 131)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Street Address"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(35, 156)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "City"
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(354, 156)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(25, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Zip"
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(35, 181)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Phone"
        '
        'txtcozipadd
        '
        Me.txtcozipadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcozipadd.BackColor = System.Drawing.Color.White
        Me.txtcozipadd.Location = New System.Drawing.Point(385, 153)
        Me.txtcozipadd.MaxLength = 10
        Me.txtcozipadd.Name = "txtcozipadd"
        Me.txtcozipadd.Size = New System.Drawing.Size(120, 21)
        Me.txtcozipadd.TabIndex = 5
        '
        'txtcocityadd
        '
        Me.txtcocityadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcocityadd.BackColor = System.Drawing.Color.White
        Me.txtcocityadd.Location = New System.Drawing.Point(140, 153)
        Me.txtcocityadd.Name = "txtcocityadd"
        Me.txtcocityadd.Size = New System.Drawing.Size(208, 21)
        Me.txtcocityadd.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.Blue
        Me.Label9.Location = New System.Drawing.Point(267, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(203, 13)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "How is your  Company organized?"
        Me.Label9.Visible = False
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Blue
        Me.Label10.Location = New System.Drawing.Point(267, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(185, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Select an Industry from the list"
        Me.Label10.Visible = False
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(35, 231)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Web Site"
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(35, 206)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 13)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Email Address"
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(354, 181)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(27, 13)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Fax"
        '
        'txtcolegal
        '
        Me.txtcolegal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcolegal.BackColor = System.Drawing.Color.White
        Me.txtcolegal.Location = New System.Drawing.Point(140, 78)
        Me.txtcolegal.Name = "txtcolegal"
        Me.txtcolegal.Size = New System.Drawing.Size(365, 21)
        Me.txtcolegal.TabIndex = 1
        '
        'txtcotaxid
        '
        Me.txtcotaxid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcotaxid.BackColor = System.Drawing.Color.White
        Me.txtcotaxid.Location = New System.Drawing.Point(140, 103)
        Me.txtcotaxid.Name = "txtcotaxid"
        Me.txtcotaxid.Size = New System.Drawing.Size(208, 21)
        Me.txtcotaxid.TabIndex = 2
        '
        'txtcostadd
        '
        Me.txtcostadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcostadd.BackColor = System.Drawing.Color.White
        Me.txtcostadd.Location = New System.Drawing.Point(140, 128)
        Me.txtcostadd.Name = "txtcostadd"
        Me.txtcostadd.Size = New System.Drawing.Size(365, 21)
        Me.txtcostadd.TabIndex = 3
        '
        'txtcophone1
        '
        Me.txtcophone1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcophone1.BackColor = System.Drawing.Color.White
        Me.txtcophone1.Location = New System.Drawing.Point(294, 261)
        Me.txtcophone1.Name = "txtcophone1"
        Me.txtcophone1.Size = New System.Drawing.Size(120, 21)
        Me.txtcophone1.TabIndex = 6
        Me.txtcophone1.Visible = False
        '
        'txtcofax1
        '
        Me.txtcofax1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcofax1.BackColor = System.Drawing.Color.White
        Me.txtcofax1.Location = New System.Drawing.Point(294, 288)
        Me.txtcofax1.Name = "txtcofax1"
        Me.txtcofax1.Size = New System.Drawing.Size(120, 21)
        Me.txtcofax1.TabIndex = 7
        Me.txtcofax1.Visible = False
        '
        'txtcoemail
        '
        Me.txtcoemail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcoemail.BackColor = System.Drawing.Color.White
        Me.txtcoemail.Location = New System.Drawing.Point(140, 203)
        Me.txtcoemail.Name = "txtcoemail"
        Me.txtcoemail.Size = New System.Drawing.Size(365, 21)
        Me.txtcoemail.TabIndex = 8
        '
        'txtcosite
        '
        Me.txtcosite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcosite.BackColor = System.Drawing.Color.White
        Me.txtcosite.Location = New System.Drawing.Point(140, 228)
        Me.txtcosite.Name = "txtcosite"
        Me.txtcosite.Size = New System.Drawing.Size(365, 21)
        Me.txtcosite.TabIndex = 9
        '
        'cbocoindustry
        '
        Me.cbocoindustry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbocoindustry.BackColor = System.Drawing.Color.White
        Me.cbocoindustry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbocoindustry.FormattingEnabled = True
        Me.cbocoindustry.Location = New System.Drawing.Point(477, 6)
        Me.cbocoindustry.Name = "cbocoindustry"
        Me.cbocoindustry.Size = New System.Drawing.Size(56, 21)
        Me.cbocoindustry.TabIndex = 10
        Me.cbocoindustry.Visible = False
        '
        'cbocororg
        '
        Me.cbocororg.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbocororg.BackColor = System.Drawing.Color.White
        Me.cbocororg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbocororg.FormattingEnabled = True
        Me.cbocororg.Location = New System.Drawing.Point(477, 31)
        Me.cbocororg.Name = "cbocororg"
        Me.cbocororg.Size = New System.Drawing.Size(56, 21)
        Me.cbocororg.TabIndex = 11
        Me.cbocororg.Visible = False
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(35, 269)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 13)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Fiscal Year"
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(35, 296)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(76, 13)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "Fiscal Month"
        Me.Label15.Visible = False
        '
        'cbofiscalYr
        '
        Me.cbofiscalYr.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbofiscalYr.BackColor = System.Drawing.Color.White
        Me.cbofiscalYr.FormattingEnabled = True
        Me.cbofiscalYr.Location = New System.Drawing.Point(140, 266)
        Me.cbofiscalYr.Name = "cbofiscalYr"
        Me.cbofiscalYr.Size = New System.Drawing.Size(120, 21)
        Me.cbofiscalYr.TabIndex = 10
        '
        'cbofiscalmonth
        '
        Me.cbofiscalmonth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbofiscalmonth.BackColor = System.Drawing.Color.White
        Me.cbofiscalmonth.FormattingEnabled = True
        Me.cbofiscalmonth.Items.AddRange(New Object() {"January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cbofiscalmonth.Location = New System.Drawing.Point(140, 292)
        Me.cbofiscalmonth.Name = "cbofiscalmonth"
        Me.cbofiscalmonth.Size = New System.Drawing.Size(120, 21)
        Me.cbofiscalmonth.TabIndex = 11
        Me.cbofiscalmonth.Visible = False
        '
        'txtcorepass
        '
        Me.txtcorepass.BackColor = System.Drawing.Color.White
        Me.txtcorepass.Location = New System.Drawing.Point(330, 395)
        Me.txtcorepass.Name = "txtcorepass"
        Me.txtcorepass.Size = New System.Drawing.Size(175, 21)
        Me.txtcorepass.TabIndex = 13
        Me.txtcorepass.UseSystemPasswordChar = True
        '
        'txtadminpassword
        '
        Me.txtadminpassword.BackColor = System.Drawing.Color.White
        Me.txtadminpassword.Location = New System.Drawing.Point(330, 370)
        Me.txtadminpassword.Name = "txtadminpassword"
        Me.txtadminpassword.Size = New System.Drawing.Size(175, 21)
        Me.txtadminpassword.TabIndex = 12
        Me.txtadminpassword.UseSystemPasswordChar = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.Red
        Me.Label16.Location = New System.Drawing.Point(198, 397)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(110, 13)
        Me.Label16.TabIndex = 29
        Me.Label16.Text = "Re-type Password"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.Red
        Me.Label17.Location = New System.Drawing.Point(198, 374)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(126, 13)
        Me.Label17.TabIndex = 28
        Me.Label17.Text = "Administor Password"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(420, 451)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(108, 28)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "Canc&el"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(192, 451)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(108, 28)
        Me.btnSave.TabIndex = 14
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(306, 451)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(108, 28)
        Me.btnClear.TabIndex = 15
        Me.btnClear.Text = "&Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Blue
        Me.Label18.Location = New System.Drawing.Point(198, 349)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(169, 13)
        Me.Label18.TabIndex = 34
        Me.Label18.Text = "Set Administrator Password "
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(198, 336)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(307, 2)
        Me.Panel1.TabIndex = 35
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(197, 432)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(308, 2)
        Me.Panel2.TabIndex = 36
        '
        'btnSegmnt
        '
        Me.btnSegmnt.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSegmnt.Location = New System.Drawing.Point(12, 451)
        Me.btnSegmnt.Name = "btnSegmnt"
        Me.btnSegmnt.Size = New System.Drawing.Size(122, 28)
        Me.btnSegmnt.TabIndex = 17
        Me.btnSegmnt.Text = "Create &Segment"
        Me.btnSegmnt.UseVisualStyleBackColor = True
        Me.btnSegmnt.Visible = False
        '
        'cboconame
        '
        Me.cboconame.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboconame.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboconame.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboconame.BackColor = System.Drawing.Color.White
        Me.cboconame.FormattingEnabled = True
        Me.cboconame.Location = New System.Drawing.Point(140, 51)
        Me.cboconame.Name = "cboconame"
        Me.cboconame.Size = New System.Drawing.Size(365, 21)
        Me.cboconame.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        Me.ErrorProvider1.Icon = CType(resources.GetObject("ErrorProvider1.Icon"), System.Drawing.Icon)
        '
        'txtcophone
        '
        Me.txtcophone.Location = New System.Drawing.Point(140, 178)
        Me.txtcophone.Mask = "(99) 000-0000"
        Me.txtcophone.Name = "txtcophone"
        Me.txtcophone.Size = New System.Drawing.Size(120, 21)
        Me.txtcophone.TabIndex = 6
        '
        'txtcofax
        '
        Me.txtcofax.Location = New System.Drawing.Point(385, 178)
        Me.txtcofax.Mask = "(99) 000-0000"
        Me.txtcofax.Name = "txtcofax"
        Me.txtcofax.Size = New System.Drawing.Size(120, 21)
        Me.txtcofax.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ChckClassing)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 336)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(180, 43)
        Me.GroupBox1.TabIndex = 37
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Other System Features"
        Me.GroupBox1.Visible = False
        '
        'ChckClassing
        '
        Me.ChckClassing.AutoSize = True
        Me.ChckClassing.Location = New System.Drawing.Point(26, 20)
        Me.ChckClassing.Name = "ChckClassing"
        Me.ChckClassing.Size = New System.Drawing.Size(143, 17)
        Me.ChckClassing.TabIndex = 38
        Me.ChckClassing.Text = "Classing Funtionality"
        Me.ChckClassing.UseVisualStyleBackColor = True
        '
        'frm_MF_Company
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(536, 493)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtcofax)
        Me.Controls.Add(Me.txtcophone)
        Me.Controls.Add(Me.cboconame)
        Me.Controls.Add(Me.btnSegmnt)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtcorepass)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.txtadminpassword)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.cbofiscalmonth)
        Me.Controls.Add(Me.cbofiscalYr)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.cbocororg)
        Me.Controls.Add(Me.cbocoindustry)
        Me.Controls.Add(Me.txtcosite)
        Me.Controls.Add(Me.txtcoemail)
        Me.Controls.Add(Me.txtcofax1)
        Me.Controls.Add(Me.txtcophone1)
        Me.Controls.Add(Me.txtcostadd)
        Me.Controls.Add(Me.txtcotaxid)
        Me.Controls.Add(Me.txtcolegal)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtcocityadd)
        Me.Controls.Add(Me.txtcozipadd)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCoInfo)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_MF_Company"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Project"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCoInfo As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtcozipadd As System.Windows.Forms.TextBox
    Friend WithEvents txtcocityadd As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtcolegal As System.Windows.Forms.TextBox
    Friend WithEvents txtcotaxid As System.Windows.Forms.TextBox
    Friend WithEvents txtcostadd As System.Windows.Forms.TextBox
    Friend WithEvents txtcophone1 As System.Windows.Forms.TextBox
    Friend WithEvents txtcofax1 As System.Windows.Forms.TextBox
    Friend WithEvents txtcoemail As System.Windows.Forms.TextBox
    Friend WithEvents txtcosite As System.Windows.Forms.TextBox
    Friend WithEvents cbocoindustry As System.Windows.Forms.ComboBox
    Friend WithEvents cbocororg As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cbofiscalYr As System.Windows.Forms.ComboBox
    Friend WithEvents cbofiscalmonth As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtcorepass As System.Windows.Forms.TextBox
    Friend WithEvents txtadminpassword As System.Windows.Forms.TextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnSegmnt As System.Windows.Forms.Button
    Friend WithEvents cboconame As System.Windows.Forms.ComboBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtcophone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtcofax As System.Windows.Forms.MaskedTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ChckClassing As System.Windows.Forms.CheckBox
End Class
