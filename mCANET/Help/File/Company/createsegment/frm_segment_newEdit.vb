Imports System.Windows.Forms

Public Class frm_segment_newEdit

    Private gMaster As New modMasterFile
    Private gTrans As New clsTransactionFunctions

    Private Sub frm_segment_newEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.gSegment.Rows.Clear()
        txtsegmentmask.Text = ""
        txtnumSegment.Text = ""
        gSegment.Refresh()
    End Sub

    Private Sub btn_mSegment_create_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_mSegment_create.Click
        gSegment.RowCount = 1
        Call mSegment_create()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.gSegment.Rows.Clear()
        txtsegmentmask.Text = ""
        Me.Close()
    End Sub

    Private Sub mSegment_create()

        Dim xRows As Integer = CInt(txtnumSegment.Text)
        Dim xCnt As Integer
        With gSegment
            For xCnt = 1 To xRows
                .RowCount = xCnt
            Next
        End With
    End Sub

    Private Sub gSegment_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gSegment.CellValidated
        Try
            Dim xVal As String = ""
            Dim xResult As String = ""
            Dim xCnt As Integer
            Dim xfilter As Integer
            Dim Count As Integer = 0
            With gSegment
                For xCnt = 0 To .Rows.Count - 1
                    If .Rows(xCnt).Cells(0).Value IsNot Nothing Then
                        xfilter += 1
                        If xfilter = 1 Then
                            If numeric_Validation(.Rows(xCnt).Cells(0).Value) = True Then
                                For Count = 1 To CInt(.Rows(xCnt).Cells(0).Value)
                                    xVal += "x"
                                Next
                                xResult += xVal
                                xVal = ""
                            End If
                        End If
                    End If
                    If .Rows(xCnt).Cells(1).Value IsNot Nothing Then
                        xfilter = 0
                        xResult += .Rows(xCnt).Cells(1).Value
                    End If
                Next
            End With
            txtsegmentmask.Text = xResult
        Catch ex As Exception
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim xCnt, xRow As Integer
        Dim xSep As String

        Dim ConfirmAction As DialogResult
        ConfirmAction = MessageBox.Show("You are about to save permanently a company information with a segment." + _
                    vbCr + "Are you sure of the information you've enter?" + vbCr + _
                    "Click YES to SAVE.", "Confirm Action", _
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        If ConfirmAction = Windows.Forms.DialogResult.Yes Then
            If frm_MF_Company.ChckClassing.Checked = True Then
                gClassingStatus = 1
            Else
                gClassingStatus = 0
            End If
            'gMaster.CreateCompany()
            Try
                With gSegment
                    If .Item(1, xCnt).Value <> "" Then
                        xSep = .Item(1, xCnt).Value
                    Else
                        xSep = ""
                    End If
                    xRow = .RowCount
                    For xCnt = 0 To xRow - 1
                        gMaster.saveSegment(gCompanyName, xCnt + 1, _
                                            .Item(0, xCnt).Value, _
                                            xSep, gUserName)

                    Next
                End With
                If MsgBox("Save successful...", MsgBoxStyle.Information) = MsgBoxResult.Ok Then
                    frm_MF_Company.Close()
                    Me.Close()
                End If
            Catch ex As Exception
                MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Save Segment")
            End Try
            If CheckClassingStatus(gCompanyID()) = 1 Then '=============check if class functionality is activated
                frmMain.LblClassActivator.Visible = True
            Else
                frmMain.LblClassActivator.Visible = False
            End If
            frmMain.lblCompanyName.Text = gCompanyName

        ElseIf ConfirmAction = Windows.Forms.DialogResult.Cancel Then

        End If

    End Sub

   
    Private Sub gSegment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gSegment.CellContentClick

    End Sub

    Private Sub txtnumSegment_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnumSegment.KeyPress
        If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
        If e.KeyChar = Chr(8) Then e.Handled = False
        If e.KeyChar = Chr(13) Then TextBox2.Focus()
    End Sub

    Private Sub txtnumSegment_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtnumSegment.TextChanged

        If "0" Or "" = txtnumSegment.Text Then
            btn_mSegment_create.Enabled = False
        Else
            btn_mSegment_create.Enabled = True
        End If
    End Sub

    Private Sub txtsegmentmask_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsegmentmask.TextChanged
        If txtsegmentmask.Text <> "" Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
    End Sub
End Class