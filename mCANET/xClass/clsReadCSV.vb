Option Explicit On

Imports System.IO
Imports System.Data

Public Class clsReadCSV
    Public Enum FileType
        Supplier = 0
        AccountType = 1
        CharOfAccounts = 2
    End Enum

    Private _File As String
    Private _DataTable As DataTable
    Private _dtSupplier As DataTable
    Private _FileType As FileType

    Public Sub New()
        _DataTable = New DataTable("CSV")
    End Sub

    Protected Overrides Sub Finalize()
        _DataTable = Nothing
        _File = ""
    End Sub

    ''' <summary>
    ''' Read the file that is specified in the SetFile property
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ReadFile()
        Dim _tmpFile As StreamReader
        Dim _delimeter(1) As Char
        Dim _ReadString As String
        Dim _tmpRecord() As String

        Try
            _delimeter(0) = ","
            _tmpFile = New StreamReader(_File)
            _ReadString = _tmpFile.ReadLine()

            _tmpRecord = _ReadString.Split(_delimeter)

            If _FileType = FileType.Supplier Then
                CreateSupplierColumns()
            ElseIf _FileType = FileType.AccountType Then
                CreateAccountTypeColumns()
            ElseIf _FileType = FileType.CharOfAccounts Then
                CreateAccountColumns()
            End If

            InputToDataTable(_tmpRecord)
            While _ReadString <> ""
                _ReadString = _tmpFile.ReadLine()
                If _ReadString = Nothing Then
                    Exit While
                End If
                _tmpRecord = _ReadString.Split(_delimeter)
                InputToDataTable(_tmpRecord)
            End While
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            _tmpFile.Close()
            _tmpFile.Dispose()
        End Try
    End Sub

#Region "Public Properties"
    ''' <summary>
    ''' Path of the File to be read
    ''' </summary>
    ''' <value>String</value>
    ''' <returns>String</returns>
    ''' <remarks>File Path</remarks>
    Public Property SetFile() As String
        Get
            SetFile = _File
        End Get
        Set(ByVal value As String)
            _File = value
        End Set
    End Property

    Public ReadOnly Property GetData() As DataTable
        Get
            GetData = _DataTable
        End Get
    End Property

    Public WriteOnly Property SetTable() As FileType
        Set(ByVal value As FileType)
            _FileType = value
        End Set
    End Property
#End Region


#Region "Private Methods"
    Private Sub CreateSupplierColumns()
        _DataTable.Columns.Add("1", Type.GetType("System.String"))
        _DataTable.Columns.Add("2", Type.GetType("System.DateTime"))
        _DataTable.Columns.Add("3", Type.GetType("System.String"))
        _DataTable.Columns.Add("4", Type.GetType("System.String"))
        _DataTable.Columns.Add("5", Type.GetType("System.String"))
        _DataTable.Columns.Add("6", Type.GetType("System.String"))
        _DataTable.Columns.Add("7", Type.GetType("System.String"))
        _DataTable.Columns.Add("8", Type.GetType("System.String"))
        _DataTable.Columns.Add("9", Type.GetType("System.String"))
        _DataTable.Columns.Add("10", Type.GetType("System.String"))
        _DataTable.Columns.Add("11", Type.GetType("System.String"))
        _DataTable.Columns.Add("12", Type.GetType("System.String"))
        _DataTable.Columns.Add("13", Type.GetType("System.String"))
        _DataTable.Columns.Add("14", Type.GetType("System.String"))
        _DataTable.Columns.Add("15", Type.GetType("System.String"))
        _DataTable.Columns.Add("16", Type.GetType("System.String"))
        _DataTable.Columns.Add("17", Type.GetType("System.Decimal"))
        _DataTable.Columns.Add("18", Type.GetType("System.String"))
        _DataTable.Columns.Add("19", Type.GetType("System.Decimal"))
        _DataTable.Columns.Add("20", Type.GetType("System.Boolean"))
        _DataTable.Columns.Add("21", Type.GetType("System.Guid"))
        _DataTable.Columns.Add("22", Type.GetType("System.Guid"))
        _DataTable.Columns.Add("23", Type.GetType("System.Guid"))
        _DataTable.Columns.Add("24", Type.GetType("System.Guid"))
        _DataTable.Columns.Add("25", Type.GetType("System.Decimal"))
        _DataTable.Columns.Add("26", Type.GetType("System.Guid"))
    End Sub

    Private Sub CreateAccountTypeColumns()
        _DataTable.Columns.Add("1", Type.GetType("System.Guid"))
        _DataTable.Columns.Add("2", Type.GetType("System.String"))
        _DataTable.Columns.Add("3", Type.GetType("System.String"))
        _DataTable.Columns.Add("4", Type.GetType("System.String"))
        _DataTable.Columns.Add("5", Type.GetType("System.String"))
        _DataTable.Columns.Add("6", Type.GetType("System.DateTime"))
        _DataTable.Columns.Add("7", Type.GetType("System.String"))
        _DataTable.Columns.Add("8", Type.GetType("System.DateTime"))
        With _DataTable
            .Columns(0).ColumnName = "Company ID"
            .Columns(1).ColumnName = "Account Type"
            .Columns(2).ColumnName = "Account Description"
            .Columns(3).ColumnName = "Segment Value"
            .Columns(4).ColumnName = "Created By"
            .Columns(5).ColumnName = "Date Created"
            .Columns(6).ColumnName = "Updated By"
            .Columns(7).ColumnName = "Date Updated"
        End With
    End Sub

    Private Sub CreateAccountColumns()
        _DataTable.Columns.Add("Company ID", Type.GetType("System.Guid")) 'co_id
        _DataTable.Columns.Add("Level", Type.GetType("System.Decimal")) 'Level
        _DataTable.Columns.Add("Account type", Type.GetType("System.String")) 'Account type
        _DataTable.Columns.Add("account name", Type.GetType("System.String")) 'account name
        _DataTable.Columns.Add("Active", Type.GetType("System.Boolean")) 'fbActive
        _DataTable.Columns.Add("Account sub", Type.GetType("System.Boolean")) 'Account sub
        _DataTable.Columns.Add("Account sub of", Type.GetType("System.String")) 'Account sub of
        _DataTable.Columns.Add("account description", Type.GetType("System.String")) 'account description
        _DataTable.Columns.Add("account note", Type.GetType("System.String")) 'account note
        _DataTable.Columns.Add("account code", Type.GetType("System.Decimal")) 'account code
        _DataTable.Columns.Add("account deleted", Type.GetType("System.Boolean")) 'account deleted
        _DataTable.Columns.Add("account balance", Type.GetType("System.Decimal")) 'account balance
        _DataTable.Columns.Add("account balance less", Type.GetType("System.Decimal")) 'account balance less
        _DataTable.Columns.Add("account balance date", Type.GetType("System.DateTime")) 'account balance date
        _DataTable.Columns.Add("Created by", Type.GetType("System.String")) 'Created by
        _DataTable.Columns.Add("date created", Type.GetType("System.DateTime")) 'date created
        _DataTable.Columns.Add("updated by", Type.GetType("System.String")) 'updated by
        _DataTable.Columns.Add("date update", Type.GetType("System.DateTime")) 'date update
        
    End Sub

    Private Sub InputToDataTable(ByVal Record() As String)
        Dim count As Integer
        Dim _rw As DataRow

        _rw = _DataTable.NewRow()
        While count <= Record.GetUpperBound(0)
            _rw(count) = Record(count).ToString()
            count += 1
        End While

        _DataTable.Rows.Add(_rw)
    End Sub
#End Region

End Class

