﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading, System.IO

Public Class frmNotBalanceEntry

    Private Sub LoadAccounts()
        DataGridView1.Rows.Clear()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "spu_NotBalanceEntries_List", _
                                   New SqlParameter("@coid", gCompanyID))
        Try

            While rd.Read
                Dim row As String() = New String() {rd.Item(0).ToString, rd.Item(1).ToString, Format(CDec(rd.Item(2).ToString), "##,##0.00"), Format(CDec(rd.Item(3).ToString), "##,##0.00")}
                DataGridView1.Rows.Add(row)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmNotBalanceEntry_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadAccounts()
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RefreshToolStripMenuItem.Click
        Call LoadAccounts()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class