﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransUploader
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.FcDoctype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcDocNum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcIdno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcLoanRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcAccountRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcAccountTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnDebit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnCredit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FcParticulars = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FdDate = New CSAcctg.GridDateControl()
        Me.fcBank = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcCheckNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fdCheckDate = New CSAcctg.GridDateControl()
        Me.fcMaker = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcPayee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.xSort = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GridDateControl1 = New CSAcctg.GridDateControl()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GridDateControl2 = New CSAcctg.GridDateControl()
        Me.GridDateControl3 = New CSAcctg.GridDateControl()
        Me.GridDateControl4 = New CSAcctg.GridDateControl()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GridDateControl5 = New CSAcctg.GridDateControl()
        Me.grdHeader = New System.Windows.Forms.DataGridView()
        Me.TransactionDetail = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CheckDetail = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.bgwDataCheck = New System.ComponentModel.BackgroundWorker()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.btnGenerateTemplate = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.bgwSave = New System.ComponentModel.BackgroundWorker()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSort = New System.Windows.Forms.Button()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FcDoctype, Me.FcDocNum, Me.FcIdno, Me.FcName, Me.FcLoanRef, Me.FcAccountRef, Me.FcCode, Me.FcAccountTitle, Me.FnDebit, Me.FnCredit, Me.FcParticulars, Me.FdDate, Me.fcBank, Me.fcCheckNumber, Me.fdCheckDate, Me.fcMaker, Me.fcPayee, Me.xSort})
        Me.dgvList.Location = New System.Drawing.Point(0, 28)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.Size = New System.Drawing.Size(1020, 352)
        Me.dgvList.TabIndex = 5
        '
        'FcDoctype
        '
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.PaleGreen
        Me.FcDoctype.DefaultCellStyle = DataGridViewCellStyle2
        Me.FcDoctype.HeaderText = "Doc. Type"
        Me.FcDoctype.Name = "FcDoctype"
        Me.FcDoctype.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcDoctype.Width = 75
        '
        'FcDocNum
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.PaleGreen
        Me.FcDocNum.DefaultCellStyle = DataGridViewCellStyle3
        Me.FcDocNum.HeaderText = "Doc. No."
        Me.FcDocNum.Name = "FcDocNum"
        Me.FcDocNum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcDocNum.Width = 120
        '
        'FcIdno
        '
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.PaleGreen
        Me.FcIdno.DefaultCellStyle = DataGridViewCellStyle4
        Me.FcIdno.HeaderText = "ID No."
        Me.FcIdno.Name = "FcIdno"
        Me.FcIdno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcIdno.Width = 120
        '
        'FcName
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.PaleGreen
        Me.FcName.DefaultCellStyle = DataGridViewCellStyle5
        Me.FcName.HeaderText = "Name"
        Me.FcName.Name = "FcName"
        Me.FcName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcName.Width = 300
        '
        'FcLoanRef
        '
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.PaleGreen
        Me.FcLoanRef.DefaultCellStyle = DataGridViewCellStyle6
        Me.FcLoanRef.HeaderText = "Loan Ref."
        Me.FcLoanRef.Name = "FcLoanRef"
        Me.FcLoanRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcLoanRef.Width = 150
        '
        'FcAccountRef
        '
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.PaleGreen
        Me.FcAccountRef.DefaultCellStyle = DataGridViewCellStyle7
        Me.FcAccountRef.HeaderText = "Account Ref."
        Me.FcAccountRef.Name = "FcAccountRef"
        Me.FcAccountRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcAccountRef.Width = 150
        '
        'FcCode
        '
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.PaleGreen
        Me.FcCode.DefaultCellStyle = DataGridViewCellStyle8
        Me.FcCode.HeaderText = "Code"
        Me.FcCode.Name = "FcCode"
        Me.FcCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcCode.Width = 80
        '
        'FcAccountTitle
        '
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.PaleGreen
        Me.FcAccountTitle.DefaultCellStyle = DataGridViewCellStyle9
        Me.FcAccountTitle.HeaderText = "Account Title"
        Me.FcAccountTitle.Name = "FcAccountTitle"
        Me.FcAccountTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcAccountTitle.Width = 250
        '
        'FnDebit
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.PaleGreen
        DataGridViewCellStyle10.Format = "#,###.##"
        Me.FnDebit.DefaultCellStyle = DataGridViewCellStyle10
        Me.FnDebit.HeaderText = "Debit"
        Me.FnDebit.Name = "FnDebit"
        Me.FnDebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FnDebit.Width = 150
        '
        'FnCredit
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.PaleGreen
        DataGridViewCellStyle11.Format = "#,###.##"
        DataGridViewCellStyle11.NullValue = "0.00"
        Me.FnCredit.DefaultCellStyle = DataGridViewCellStyle11
        Me.FnCredit.HeaderText = "Credit"
        Me.FnCredit.Name = "FnCredit"
        Me.FnCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FnCredit.Width = 150
        '
        'FcParticulars
        '
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.PaleGreen
        Me.FcParticulars.DefaultCellStyle = DataGridViewCellStyle12
        Me.FcParticulars.HeaderText = "Particulars"
        Me.FcParticulars.Name = "FcParticulars"
        Me.FcParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FcParticulars.Width = 300
        '
        'FdDate
        '
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.PaleGreen
        Me.FdDate.DefaultCellStyle = DataGridViewCellStyle13
        Me.FdDate.DividerWidth = 2
        Me.FdDate.HeaderText = "Date"
        Me.FdDate.Name = "FdDate"
        Me.FdDate.Width = 150
        '
        'fcBank
        '
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LightSteelBlue
        Me.fcBank.DefaultCellStyle = DataGridViewCellStyle14
        Me.fcBank.HeaderText = "Bank"
        Me.fcBank.Name = "fcBank"
        Me.fcBank.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcBank.Width = 250
        '
        'fcCheckNumber
        '
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.LightSteelBlue
        Me.fcCheckNumber.DefaultCellStyle = DataGridViewCellStyle15
        Me.fcCheckNumber.HeaderText = "Check No."
        Me.fcCheckNumber.Name = "fcCheckNumber"
        Me.fcCheckNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcCheckNumber.Width = 120
        '
        'fdCheckDate
        '
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.LightSteelBlue
        Me.fdCheckDate.DefaultCellStyle = DataGridViewCellStyle16
        Me.fdCheckDate.HeaderText = "Check Date"
        Me.fdCheckDate.Name = "fdCheckDate"
        Me.fdCheckDate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.fdCheckDate.Width = 150
        '
        'fcMaker
        '
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.LightSteelBlue
        Me.fcMaker.DefaultCellStyle = DataGridViewCellStyle17
        Me.fcMaker.HeaderText = "Maker"
        Me.fcMaker.Name = "fcMaker"
        Me.fcMaker.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcMaker.Width = 250
        '
        'fcPayee
        '
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.LightSteelBlue
        Me.fcPayee.DefaultCellStyle = DataGridViewCellStyle18
        Me.fcPayee.HeaderText = "Payee"
        Me.fcPayee.Name = "fcPayee"
        Me.fcPayee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcPayee.Width = 250
        '
        'xSort
        '
        Me.xSort.HeaderText = "xSort"
        Me.xSort.Name = "xSort"
        Me.xSort.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.xSort.Visible = False
        Me.xSort.Width = 50
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnImport.Location = New System.Drawing.Point(455, 401)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(89, 27)
        Me.btnImport.TabIndex = 14
        Me.btnImport.Text = "Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'TextBox5
        '
        Me.TextBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox5.Location = New System.Drawing.Point(72, 404)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(377, 21)
        Me.TextBox5.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(2, 407)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 15)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "File Path :"
        '
        'GridDateControl1
        '
        Me.GridDateControl1.HeaderText = "Date"
        Me.GridDateControl1.Name = "GridDateControl1"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GridDateControl2
        '
        Me.GridDateControl2.HeaderText = "Date"
        Me.GridDateControl2.Name = "GridDateControl2"
        '
        'GridDateControl3
        '
        Me.GridDateControl3.HeaderText = "Date"
        Me.GridDateControl3.Name = "GridDateControl3"
        '
        'GridDateControl4
        '
        Me.GridDateControl4.HeaderText = "Date"
        Me.GridDateControl4.Name = "GridDateControl4"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Loan Ref."
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Accounnt Ref."
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Account Title"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Account Title"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Debit"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Credit"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'GridDateControl5
        '
        Me.GridDateControl5.HeaderText = "Date"
        Me.GridDateControl5.Name = "GridDateControl5"
        '
        'grdHeader
        '
        Me.grdHeader.AllowUserToAddRows = False
        Me.grdHeader.AllowUserToDeleteRows = False
        Me.grdHeader.AllowUserToResizeColumns = False
        Me.grdHeader.AllowUserToResizeRows = False
        Me.grdHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdHeader.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdHeader.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grdHeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle19.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdHeader.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.grdHeader.ColumnHeadersHeight = 30
        Me.grdHeader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdHeader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TransactionDetail, Me.CheckDetail})
        Me.grdHeader.Location = New System.Drawing.Point(0, 0)
        Me.grdHeader.Name = "grdHeader"
        Me.grdHeader.RowHeadersVisible = False
        Me.grdHeader.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.grdHeader.Size = New System.Drawing.Size(1020, 30)
        Me.grdHeader.TabIndex = 6
        '
        'TransactionDetail
        '
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.White
        Me.TransactionDetail.DefaultCellStyle = DataGridViewCellStyle20
        Me.TransactionDetail.DividerWidth = 2
        Me.TransactionDetail.HeaderText = "Transaction Details"
        Me.TransactionDetail.Name = "TransactionDetail"
        Me.TransactionDetail.ReadOnly = True
        Me.TransactionDetail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TransactionDetail.Width = 1995
        '
        'CheckDetail
        '
        Me.CheckDetail.HeaderText = "Check Details"
        Me.CheckDetail.Name = "CheckDetail"
        Me.CheckDetail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CheckDetail.Width = 1020
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(-1, 380)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(1022, 10)
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.ProgressBar1.TabIndex = 52
        '
        'bgwDataCheck
        '
        Me.bgwDataCheck.WorkerReportsProgress = True
        Me.bgwDataCheck.WorkerSupportsCancellation = True
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.InitialDirectory = "C:\"
        Me.SaveFileDialog1.Title = "Save Template"
        '
        'btnGenerateTemplate
        '
        Me.btnGenerateTemplate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGenerateTemplate.BackColor = System.Drawing.SystemColors.Control
        Me.btnGenerateTemplate.Location = New System.Drawing.Point(550, 401)
        Me.btnGenerateTemplate.Name = "btnGenerateTemplate"
        Me.btnGenerateTemplate.Size = New System.Drawing.Size(131, 27)
        Me.btnGenerateTemplate.TabIndex = 85
        Me.btnGenerateTemplate.Text = "Generate Template"
        Me.btnGenerateTemplate.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnSave.Location = New System.Drawing.Point(687, 401)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(131, 27)
        Me.btnSave.TabIndex = 86
        Me.btnSave.Text = "Check and Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'bgwSave
        '
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClose.Location = New System.Drawing.Point(919, 401)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(89, 27)
        Me.btnClose.TabIndex = 87
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnSort
        '
        Me.btnSort.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSort.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnSort.Location = New System.Drawing.Point(824, 401)
        Me.btnSort.Name = "btnSort"
        Me.btnSort.Size = New System.Drawing.Size(89, 27)
        Me.btnSort.TabIndex = 88
        Me.btnSort.Text = "Sort"
        Me.btnSort.UseVisualStyleBackColor = False
        '
        'frmTransUploader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(1020, 435)
        Me.Controls.Add(Me.btnSort)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnGenerateTemplate)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dgvList)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.grdHeader)
        Me.Font = New System.Drawing.Font("MS Reference Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTransUploader"
        Me.Text = "Transaction Uploader"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GridDateControl1 As CSAcctg.GridDateControl
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GridDateControl2 As CSAcctg.GridDateControl
    Friend WithEvents GridDateControl3 As CSAcctg.GridDateControl
    Friend WithEvents GridDateControl4 As CSAcctg.GridDateControl
    Friend WithEvents GridDateControl5 As CSAcctg.GridDateControl
    Friend WithEvents grdHeader As System.Windows.Forms.DataGridView
    Friend WithEvents TransactionDetail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CheckDetail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents bgwDataCheck As System.ComponentModel.BackgroundWorker
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnGenerateTemplate As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents bgwSave As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSort As System.Windows.Forms.Button
    Friend WithEvents FcDoctype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcDocNum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcIdno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcLoanRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcAccountRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcAccountTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnDebit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnCredit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FcParticulars As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FdDate As CSAcctg.GridDateControl
    Friend WithEvents fcBank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcCheckNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fdCheckDate As CSAcctg.GridDateControl
    Friend WithEvents fcMaker As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcPayee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents xSort As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
