﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Linq
Imports System.Text.RegularExpressions

Public Class frmTransUploader

    Private con As New Clsappconfiguration
    Dim filenym As String
    Dim fpath As String
    Private UploaderType As String
    Private xFilePath As String = ""
    Dim xErrCount As Integer = 0

    Private Sub frmTransUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnImport_Click(sender As System.Object, e As System.EventArgs) Handles btnImport.Click
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.ShowDialog()
            If DialogResult.OK Then
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetFullPath(filenym)
                TextBox5.Text = fpath
                xFilePath = TextBox5.Text
                UploaderType = IO.Path.GetExtension(xFilePath)
                AuditTrail_Save("TRANSACTION UPLOADER", "Import Excel File")
                import()
            Else
                Exit Sub
            End If
        Catch ex As Exception
            ' MsgBox("User Cancelled!", vbInformation, "...")
            MsgBox(ex.Message, vbInformation, "Import")
        End Try
    End Sub

    Private Sub import()
        dgvList.Rows.Clear()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + TextBox5.Text + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader
        Try
            While rd.Read = True
                Dim row As String() = New String() {rd.Item(0).ToString.Trim(), rd.Item(1).ToString.Trim(), rd.Item(2).ToString.Trim(), rd.Item(3).ToString.Trim(), _
                                                    rd.Item(4).ToString.Trim(), rd.Item(5).ToString.Trim(), rd.Item(6).ToString.Trim(), rd.Item(7).ToString.Trim(), _
                                                    Format(CDec(rd.Item(8).ToString), "##,##0.00"), Format(CDec(rd.Item(9).ToString), "##,##0.00"), _
                                                    rd.Item(10).ToString.Trim(), rd.Item(11).ToString, rd.Item(12).ToString.Trim(), rd.Item(13).ToString.Trim(), _
                                                    rd.Item(14).ToString, rd.Item(15).ToString.Trim(), rd.Item(16).ToString.Trim(), 0}
                dgvList.Rows.Add(row)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataCheck()
        Try
            Dim xRowCount As Integer = dgvList.RowCount
            For xRow As Integer = 0 To dgvList.RowCount - 1
                Dim str As String
                Dim xCount As Integer = 0
                str = GetUploaderDataStatus(dgvList.Item("FcDocType", xRow).Value.ToString, dgvList.Item("FcIdno", xRow).Value.ToString, dgvList.Item("FcLoanRef", xRow).Value.ToString, dgvList.Item("FcAccountRef", xRow).Value.ToString, dgvList.Item("FcCode", xRow).Value.ToString, dgvList.Item("FcDocNum", xRow).Value.ToString, dgvList.Item("FcBank", xRow).Value.ToString, dgvList.Item("FcCheckNumber", xRow).Value.ToString)
                Dim strarr() As String = Str.Split(","c)

                If strarr(0) = "0" Then
                    dgvList.Rows(xRow).Cells("FcCode").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcCode").Style.BackColor = Color.PaleGreen
                    xCount = xCount
                End If

                If strarr(1) = "0" Then
                    dgvList.Rows(xRow).Cells("FcDocType").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcDocType").Style.BackColor = Color.PaleGreen
                    xCount = xCount
                End If

                If strarr(2) = "0" Then
                    dgvList.Rows(xRow).Cells("FcIdno").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcIdno").Style.BackColor = Color.PaleGreen
                    xCount = xCount
                End If

                If strarr(3) = "0" Then
                    dgvList.Rows(xRow).Cells("FcLoanRef").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcLoanRef").Style.BackColor = Color.PaleGreen
                    xCount = xCount
                End If

                If strarr(4) = "0" Then
                    dgvList.Rows(xRow).Cells("FcAccountRef").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcAccountRef").Style.BackColor = Color.PaleGreen
                    xCount = xCount
                End If

                'If strarr(5) = "1" Then
                '    dgvList.Rows(xRow).Cells("FcDocNum").Style.BackColor = Color.Red
                '    xErrCount = xErrCount + 1
                '    xCount = xCount + 1
                'Else
                '    dgvList.Rows(xRow).Cells("FcDocNum").Style.BackColor = Color.PaleGreen
                '    xCount = xCount
                'End If

                If strarr(6) = "0" Then
                    dgvList.Rows(xRow).Cells("FcBank").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcBank").Style.BackColor = Color.LightSteelBlue
                    xCount = xCount
                End If

                If strarr(7) = "1" Then
                    dgvList.Rows(xRow).Cells("FcCheckNumber").Style.BackColor = Color.Red
                    xErrCount = xErrCount + 1
                    xCount = xCount + 1
                Else
                    dgvList.Rows(xRow).Cells("FcCheckNumber").Style.BackColor = Color.LightSteelBlue
                    xCount = xCount
                End If

                If xCount <> 0 Then
                    dgvList.Rows(xRow).Cells("xSort").Value = 0
                Else
                    dgvList.Rows(xRow).Cells("xSort").Value = 1
                End If

                pRowCount = dgvList.RowCount
                pRow = xRow + 1

                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwDataCheck.ReportProgress(percent)

            Next
        Catch ex As Exception
            MsgBox(ex.Message)
            MsgBox(Err.ToString)
        End Try
    End Sub

    Private Sub dgvList_Scroll(sender As System.Object, e As System.Windows.Forms.ScrollEventArgs) Handles dgvList.Scroll
        grdHeader.HorizontalScrollingOffset = dgvList.HorizontalScrollingOffset
    End Sub

    Private Sub bgwDataCheck_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwDataCheck.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
    End Sub

    Private Sub bgwDataCheck_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwDataCheck.DoWork
        DataCheck()
    End Sub

    Private Sub bgwDataCheck_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwDataCheck.RunWorkerCompleted
        Me.ProgressBar1.Value = 0
        MsgBox("Data Checking Completed. " & xErrCount & " Error(s) Found.")
        AuditTrail_Save("TRANSACTION UPLOADER", "Data Checking Completed. " & xErrCount & " Error(s) Found.")
        If xErrCount = 0 Then
            If bgwSave.IsBusy = False Then
                ProgressBar1.Visible = True
                bgwSave.WorkerReportsProgress = True
                bgwSave.RunWorkerAsync()
            Else
                bgwSave.CancelAsync()
                ProgressBar1.Visible = True
                bgwSave.WorkerReportsProgress = True
                bgwSave.RunWorkerAsync()
            End If
        Else
            MsgBox("Please make sure that all cells are not filled with RED color. Uploading Failed.", MsgBoxStyle.Critical, "Uploader")
            xErrCount = 0
            Exit Sub
        End If
    End Sub

    Private Sub btnGenerateTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateTemplate.Click
        SaveFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        SaveFileDialog1.ShowDialog()
        If DialogResult = Windows.Forms.DialogResult.OK Then
            GenerateTemplate()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub GenerateTemplate()
        Dim xlApp As Microsoft.Office.Interop.Excel.Application
        Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
        Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim i As Integer
        Dim j As Integer

        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("sheet1")


        For i = 0 To dgvList.RowCount - 1
            For j = 0 To dgvList.ColumnCount - 1
                For k As Integer = 1 To dgvList.Columns.Count
                    xlWorkSheet.Cells(1, k) = dgvList.Columns(k - 1).HeaderText
                    xlWorkSheet.Cells(i + 2, j + 1) = dgvList(j, i).Value
                Next
            Next
        Next

        xlWorkSheet.SaveAs(Path.GetFullPath(SaveFileDialog1.FileName))
        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        Dim res As MsgBoxResult
        res = MsgBox("Process completed, Would you like to open file?", MsgBoxStyle.YesNo)
        If (res = MsgBoxResult.Yes) Then
            Process.Start(Path.GetFullPath(SaveFileDialog1.FileName))
        End If
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        AuditTrail_Save("TRANSACTION UPLOADER", "Check Excel Data")
        If bgwDataCheck.IsBusy = False Then
            ProgressBar1.Visible = True
            bgwDataCheck.WorkerReportsProgress = True
            bgwDataCheck.RunWorkerAsync()
        Else
            bgwDataCheck.CancelAsync()
            ProgressBar1.Visible = True
            bgwDataCheck.WorkerReportsProgress = True
            bgwDataCheck.RunWorkerAsync()
        End If
    End Sub

    Private Sub SaveTransaction()
        Try
            For xRow As Integer = 0 To dgvList.RowCount - 1
                'MsgBox(dgvList.Item("FcPayee", xRow).Value.ToString)
                Dim xRowCount As Integer = dgvList.RowCount
                Dim cGuid As New Guid(gCompanyID)
                SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "spu_TransactionUploader_InsertUpdate",
                                                       New SqlParameter("@fcDoctype", dgvList.Item("FcDoctype", xRow).Value.ToString),
                                                       New SqlParameter("@fcDocNum", dgvList.Item("FcDocNum", xRow).Value.ToString),
                                                       New SqlParameter("@fcIdNo", dgvList.Item("FcIdno", xRow).Value.ToString),
                                                       New SqlParameter("@fcLoanRef", dgvList.Item("FcLoanRef", xRow).Value.ToString),
                                                       New SqlParameter("@fcAccountRef", dgvList.Item("FcAccountRef", xRow).Value.ToString),
                                                       New SqlParameter("@fcCode", dgvList.Item("FcCode", xRow).Value.ToString),
                                                       New SqlParameter("@fnDebit", CDec(dgvList.Item("FnDebit", xRow).Value)),
                                                       New SqlParameter("@fnCredit", CDec(dgvList.Item("FnCredit", xRow).Value)),
                                                       New SqlParameter("@fcParticulars", dgvList.Item("FcParticulars", xRow).Value.ToString),
                                                       New SqlParameter("@fdDate", dgvList.Item("FdDate", xRow).Value),
                                                       New SqlParameter("@fcBank", dgvList.Item("FcBank", xRow).Value.ToString),
                                                       New SqlParameter("@fcCheckNo", dgvList.Item("fcCheckNumber", xRow).Value.ToString),
                                                       New SqlParameter("@fdCheckDate", dgvList.Item("FdCheckDate", xRow).Value),
                                                       New SqlParameter("@fcMaker", dgvList.Item("FcMaker", xRow).Value.ToString),
                                                       New SqlParameter("@fcPayee", dgvList.Item("FcPayee", xRow).Value.ToString),
                                                       New SqlParameter("@fkcoid", cGuid),
                                                       New SqlParameter("@fnLine", CInt(xRow)),
                                                       New SqlParameter("@fcName", dgvList.Item("FcName", xRow).Value.ToString),
                                                       New SqlParameter("@fcUser", frmMain.currentUser.Text))
                pRowCount = dgvList.RowCount
                pRow = xRow + 1

                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwSave.ReportProgress(percent)
            Next

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub bgwSave_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSave.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
    End Sub

    Private Sub bgwSave_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwSave.DoWork
        AuditTrail_Save("TRANSACTION UPLOADER", "Data uploading begins.")
        SaveTransaction()
    End Sub

    Private Sub bgwSave_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwSave.RunWorkerCompleted
        ProgressBar1.Value = 0
        MsgBox("Transaction Uploaded Successfully.", MsgBoxStyle.Information, "Uploader")
        AuditTrail_Save("TRANSACTION UPLOADER", "Data uploaded successfully.")
        xErrCount = 0
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSort_Click(sender As System.Object, e As System.EventArgs) Handles btnSort.Click
        AuditTrail_Save("TRANSACTION UPLOADER", "Sort Excel Data")
        dgvList.Sort(dgvList.Columns(17), System.ComponentModel.ListSortDirection.Ascending)
    End Sub
End Class