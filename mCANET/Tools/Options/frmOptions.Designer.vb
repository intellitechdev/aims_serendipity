<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptions))
        Me.tabOptions = New System.Windows.Forms.TabControl
        Me.pgeAccounts = New System.Windows.Forms.TabPage
        Me.grdAccountConfig = New System.Windows.Forms.DataGridView
        Me.panelAcctgBottom = New System.Windows.Forms.Panel
        Me.btnAcctgClose = New System.Windows.Forms.Button
        Me.btnAcctgApply = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.tabOptions.SuspendLayout()
        Me.pgeAccounts.SuspendLayout()
        CType(Me.grdAccountConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelAcctgBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabOptions
        '
        Me.tabOptions.Controls.Add(Me.pgeAccounts)
        Me.tabOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabOptions.Location = New System.Drawing.Point(0, 0)
        Me.tabOptions.Name = "tabOptions"
        Me.tabOptions.SelectedIndex = 0
        Me.tabOptions.Size = New System.Drawing.Size(654, 426)
        Me.tabOptions.TabIndex = 0
        '
        'pgeAccounts
        '
        Me.pgeAccounts.Controls.Add(Me.grdAccountConfig)
        Me.pgeAccounts.Controls.Add(Me.Panel1)
        Me.pgeAccounts.Controls.Add(Me.panelAcctgBottom)
        Me.pgeAccounts.Location = New System.Drawing.Point(4, 24)
        Me.pgeAccounts.Name = "pgeAccounts"
        Me.pgeAccounts.Padding = New System.Windows.Forms.Padding(3)
        Me.pgeAccounts.Size = New System.Drawing.Size(646, 398)
        Me.pgeAccounts.TabIndex = 0
        Me.pgeAccounts.Text = "Accounts"
        Me.pgeAccounts.ToolTipText = "Configure Accounts Here"
        Me.pgeAccounts.UseVisualStyleBackColor = True
        '
        'grdAccountConfig
        '
        Me.grdAccountConfig.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdAccountConfig.BackgroundColor = System.Drawing.SystemColors.Control
        Me.grdAccountConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdAccountConfig.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdAccountConfig.Location = New System.Drawing.Point(3, 49)
        Me.grdAccountConfig.Name = "grdAccountConfig"
        Me.grdAccountConfig.Size = New System.Drawing.Size(640, 300)
        Me.grdAccountConfig.TabIndex = 1
        '
        'panelAcctgBottom
        '
        Me.panelAcctgBottom.Controls.Add(Me.btnAcctgClose)
        Me.panelAcctgBottom.Controls.Add(Me.btnAcctgApply)
        Me.panelAcctgBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelAcctgBottom.Location = New System.Drawing.Point(3, 349)
        Me.panelAcctgBottom.Name = "panelAcctgBottom"
        Me.panelAcctgBottom.Size = New System.Drawing.Size(640, 46)
        Me.panelAcctgBottom.TabIndex = 0
        '
        'btnAcctgClose
        '
        Me.btnAcctgClose.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctgClose.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnAcctgClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAcctgClose.Location = New System.Drawing.Point(552, 6)
        Me.btnAcctgClose.Name = "btnAcctgClose"
        Me.btnAcctgClose.Size = New System.Drawing.Size(81, 33)
        Me.btnAcctgClose.TabIndex = 4
        Me.btnAcctgClose.Text = "Close"
        Me.btnAcctgClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAcctgClose.UseVisualStyleBackColor = True
        '
        'btnAcctgApply
        '
        Me.btnAcctgApply.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctgApply.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnAcctgApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAcctgApply.Location = New System.Drawing.Point(465, 6)
        Me.btnAcctgApply.Name = "btnAcctgApply"
        Me.btnAcctgApply.Size = New System.Drawing.Size(81, 33)
        Me.btnAcctgApply.TabIndex = 3
        Me.btnAcctgApply.Text = "Apply"
        Me.btnAcctgApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAcctgApply.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(640, 46)
        Me.Panel1.TabIndex = 2
        '
        'frmOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 426)
        Me.Controls.Add(Me.tabOptions)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(670, 543)
        Me.Name = "frmOptions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Accounting System Options"
        Me.tabOptions.ResumeLayout(False)
        Me.pgeAccounts.ResumeLayout(False)
        CType(Me.grdAccountConfig, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelAcctgBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabOptions As System.Windows.Forms.TabControl
    Friend WithEvents pgeAccounts As System.Windows.Forms.TabPage
    Friend WithEvents panelAcctgBottom As System.Windows.Forms.Panel
    Friend WithEvents btnAcctgClose As System.Windows.Forms.Button
    Friend WithEvents btnAcctgApply As System.Windows.Forms.Button
    Friend WithEvents grdAccountConfig As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
