﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmForwardBalance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdList = New System.Windows.Forms.DataGridView()
        Me.fcEmployeeNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AcctName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcAccountRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acnt_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acnt_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fnDebit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fnCredit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDocNumber = New System.Windows.Forms.TextBox()
        Me.btnSelectDocNumber = New System.Windows.Forms.Button()
        Me.dteTransDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkPost = New System.Windows.Forms.CheckBox()
        Me.txtParticulars = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTdebit = New System.Windows.Forms.TextBox()
        Me.txtTcredit = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnAddAcnt = New System.Windows.Forms.Button()
        Me.btnRemoveAccount = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdList
        '
        Me.grdList.AllowUserToAddRows = False
        Me.grdList.AllowUserToResizeRows = False
        Me.grdList.BackgroundColor = System.Drawing.Color.White
        Me.grdList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.fcEmployeeNo, Me.AcctName, Me.fcAccountRef, Me.acnt_code, Me.acnt_name, Me.fnDebit, Me.fnCredit})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdList.DefaultCellStyle = DataGridViewCellStyle4
        Me.grdList.Location = New System.Drawing.Point(10, 62)
        Me.grdList.Name = "grdList"
        Me.grdList.RowHeadersVisible = False
        Me.grdList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.grdList.Size = New System.Drawing.Size(809, 227)
        Me.grdList.TabIndex = 0
        '
        'fcEmployeeNo
        '
        Me.fcEmployeeNo.HeaderText = "ID"
        Me.fcEmployeeNo.Name = "fcEmployeeNo"
        Me.fcEmployeeNo.ReadOnly = True
        Me.fcEmployeeNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcEmployeeNo.Width = 80
        '
        'AcctName
        '
        Me.AcctName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.AcctName.HeaderText = "Name"
        Me.AcctName.Name = "AcctName"
        Me.AcctName.ReadOnly = True
        Me.AcctName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'fcAccountRef
        '
        Me.fcAccountRef.HeaderText = "Account Ref."
        Me.fcAccountRef.Name = "fcAccountRef"
        Me.fcAccountRef.ReadOnly = True
        Me.fcAccountRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcAccountRef.Width = 80
        '
        'acnt_code
        '
        Me.acnt_code.HeaderText = "Code"
        Me.acnt_code.Name = "acnt_code"
        Me.acnt_code.ReadOnly = True
        Me.acnt_code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.acnt_code.Width = 80
        '
        'acnt_name
        '
        Me.acnt_name.HeaderText = "Account Title"
        Me.acnt_name.Name = "acnt_name"
        Me.acnt_name.ReadOnly = True
        Me.acnt_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.acnt_name.Width = 200
        '
        'fnDebit
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle2.NullValue = Nothing
        Me.fnDebit.DefaultCellStyle = DataGridViewCellStyle2
        Me.fnDebit.HeaderText = "Debit"
        Me.fnDebit.Name = "fnDebit"
        Me.fnDebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fnDebit.Width = 80
        '
        'fnCredit
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle3.NullValue = Nothing
        Me.fnCredit.DefaultCellStyle = DataGridViewCellStyle3
        Me.fnCredit.HeaderText = "Credit"
        Me.fnCredit.Name = "fnCredit"
        Me.fnCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fnCredit.Width = 80
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(453, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Document Number:"
        '
        'txtDocNumber
        '
        Me.txtDocNumber.BackColor = System.Drawing.Color.White
        Me.txtDocNumber.Location = New System.Drawing.Point(558, 34)
        Me.txtDocNumber.Name = "txtDocNumber"
        Me.txtDocNumber.ReadOnly = True
        Me.txtDocNumber.Size = New System.Drawing.Size(153, 20)
        Me.txtDocNumber.TabIndex = 2
        '
        'btnSelectDocNumber
        '
        Me.btnSelectDocNumber.Location = New System.Drawing.Point(716, 32)
        Me.btnSelectDocNumber.Name = "btnSelectDocNumber"
        Me.btnSelectDocNumber.Size = New System.Drawing.Size(25, 23)
        Me.btnSelectDocNumber.TabIndex = 3
        Me.btnSelectDocNumber.Text = "..."
        Me.btnSelectDocNumber.UseVisualStyleBackColor = True
        '
        'dteTransDate
        '
        Me.dteTransDate.CustomFormat = "MMMM dd, yyyy"
        Me.dteTransDate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.dteTransDate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteTransDate.Location = New System.Drawing.Point(50, 34)
        Me.dteTransDate.Name = "dteTransDate"
        Me.dteTransDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dteTransDate.Size = New System.Drawing.Size(179, 23)
        Me.dteTransDate.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(11, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Date:"
        '
        'chkPost
        '
        Me.chkPost.AutoSize = True
        Me.chkPost.BackColor = System.Drawing.Color.Transparent
        Me.chkPost.Location = New System.Drawing.Point(760, 38)
        Me.chkPost.Name = "chkPost"
        Me.chkPost.Size = New System.Drawing.Size(59, 17)
        Me.chkPost.TabIndex = 47
        Me.chkPost.Text = "Posted"
        Me.chkPost.UseVisualStyleBackColor = False
        '
        'txtParticulars
        '
        Me.txtParticulars.Location = New System.Drawing.Point(72, 295)
        Me.txtParticulars.Multiline = True
        Me.txtParticulars.Name = "txtParticulars"
        Me.txtParticulars.Size = New System.Drawing.Size(480, 20)
        Me.txtParticulars.TabIndex = 54
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(9, 298)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Particulars:"
        '
        'txtTdebit
        '
        Me.txtTdebit.BackColor = System.Drawing.Color.White
        Me.txtTdebit.Location = New System.Drawing.Point(608, 295)
        Me.txtTdebit.Name = "txtTdebit"
        Me.txtTdebit.ReadOnly = True
        Me.txtTdebit.Size = New System.Drawing.Size(103, 20)
        Me.txtTdebit.TabIndex = 56
        Me.txtTdebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTcredit
        '
        Me.txtTcredit.BackColor = System.Drawing.Color.White
        Me.txtTcredit.Location = New System.Drawing.Point(716, 295)
        Me.txtTcredit.Name = "txtTcredit"
        Me.txtTcredit.ReadOnly = True
        Me.txtTcredit.Size = New System.Drawing.Size(103, 20)
        Me.txtTcredit.TabIndex = 57
        Me.txtTcredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(563, 298)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "Totals:"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(475, 356)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(64, 23)
        Me.btnSearch.TabIndex = 77
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(755, 356)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(64, 23)
        Me.btnClose.TabIndex = 76
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(685, 356)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(64, 23)
        Me.btnDelete.TabIndex = 75
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(615, 356)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(64, 23)
        Me.btnSave.TabIndex = 74
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(545, 356)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(64, 23)
        Me.btnNew.TabIndex = 81
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnAddAcnt
        '
        Me.btnAddAcnt.BackColor = System.Drawing.Color.Transparent
        Me.btnAddAcnt.Location = New System.Drawing.Point(10, 321)
        Me.btnAddAcnt.Name = "btnAddAcnt"
        Me.btnAddAcnt.Size = New System.Drawing.Size(115, 22)
        Me.btnAddAcnt.TabIndex = 82
        Me.btnAddAcnt.Text = "Add Account"
        Me.btnAddAcnt.UseVisualStyleBackColor = False
        '
        'btnRemoveAccount
        '
        Me.btnRemoveAccount.BackColor = System.Drawing.Color.Transparent
        Me.btnRemoveAccount.Location = New System.Drawing.Point(131, 321)
        Me.btnRemoveAccount.Name = "btnRemoveAccount"
        Me.btnRemoveAccount.Size = New System.Drawing.Size(115, 23)
        Me.btnRemoveAccount.TabIndex = 83
        Me.btnRemoveAccount.Text = "Remove Account"
        Me.btnRemoveAccount.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.InitialDirectory = "C:\"
        Me.OpenFileDialog1.Title = "Open File"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(283, 356)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(64, 22)
        Me.btnBrowse.TabIndex = 84
        Me.btnBrowse.Text = "Import"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(10, 358)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(267, 20)
        Me.txtPath.TabIndex = 85
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "fkEmployee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Account Ref."
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 80
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.Format = "##,###.00"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn6.HeaderText = "Account Title"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 200
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle6.Format = "##,###.00"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn7.HeaderText = "Debit"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Credit"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 80
        '
        'frmForwardBalance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(827, 393)
        Me.Controls.Add(Me.txtPath)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.btnRemoveAccount)
        Me.Controls.Add(Me.btnAddAcnt)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtTcredit)
        Me.Controls.Add(Me.txtTdebit)
        Me.Controls.Add(Me.txtParticulars)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.chkPost)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dteTransDate)
        Me.Controls.Add(Me.btnSelectDocNumber)
        Me.Controls.Add(Me.txtDocNumber)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.grdList)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmForwardBalance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdList As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDocNumber As System.Windows.Forms.TextBox
    Friend WithEvents btnSelectDocNumber As System.Windows.Forms.Button
    Friend WithEvents dteTransDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkPost As System.Windows.Forms.CheckBox
    Friend WithEvents txtParticulars As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTdebit As System.Windows.Forms.TextBox
    Friend WithEvents txtTcredit As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    'Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAddAcnt As System.Windows.Forms.Button
    Friend WithEvents btnRemoveAccount As System.Windows.Forms.Button
    Friend WithEvents fcEmployeeNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AcctName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcAccountRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acnt_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acnt_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fnDebit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fnCredit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
End Class
