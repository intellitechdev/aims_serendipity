﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class xSearch
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim i As Integer = grdList.CurrentRow.Index
        Select Case xModule
            Case "Search"
                EntryDetails()
                frmForwardBalance.keyID = grdList.Item("fcRefNo", i).Value.ToString

                Call frmForwardBalance.LoadItems()
            Case "DocNum"
                frmForwardBalance.txtDocNumber.Text = grdList.Item("fcDocNumber", i).Value.ToString
        End Select
        Me.Close()
    End Sub

    Private Sub ListDocNum()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        Select Case xModule
            Case "Search"
                ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_ForwardBalance_Search",
                                              New SqlParameter("@filter", txtSearch.Text))
                grdList.DataSource = ds.Tables(0)
                With grdList
                    .Columns("fcRefNo").HeaderText = "Document Number"
                    .Columns("fcRefNo").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("fcRefNo").Width = 260
                End With
                Try
                    frmForwardBalance.keyID = grdList.Item("fcRefNo", 0).Value.ToString

                Catch ex As Exception
                    frmForwardBalance.keyID = ""
                End Try

            Case "DocNum"
                ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_ForwardBalance_DocumentList")
                grdList.DataSource = ds.Tables(0)
                With grdList
                    .Columns("fcDocNumber").HeaderText = "Document Number"
                    .Columns("fcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("fcDocNumber").Width = 260
                End With
        End Select
    End Sub

    Private Sub xSearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ListDocNum()
    End Sub

    Private Sub EntryDetails()
        Select Case xModule
            Case "Search"
                Try
                    Dim i As Integer = grdList.CurrentRow.Index
                    frmForwardBalance.keyID = grdList.Item("fcRefNo", i).Value.ToString
                Catch ex As Exception

                End Try
        End Select
    End Sub

    Private Sub grdList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdList.Click
        Select Case xModule
            Case "Search"
                EntryDetails()
        End Select
    End Sub
    Private Sub grdList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdList.DoubleClick
        Dim i As Integer = grdList.CurrentRow.Index
        Select Case xModule
            Case "Search"
                EntryDetails()
                frmForwardBalance.keyID = grdList.Item("fcRefNo", i).Value.ToString
                Call frmForwardBalance.LoadItems()
            Case "DocNum"
                frmForwardBalance.txtDocNumber.Text = grdList.Item("fcDocNumber", i).Value.ToString
        End Select
        Me.Close()
    End Sub

    Private Sub grdList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdList.SelectionChanged
        Select Case xModule
            Case "Search"
                EntryDetails()
        End Select
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        frmForwardBalance.xModule = ""
        Me.Close()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call ListDocNum()
    End Sub
End Class