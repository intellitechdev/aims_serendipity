﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Data.OleDb, System.IO

Public Class frmForwardBalance
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public keyID As String
    Public xModule As String

    Dim filenym As String
    Dim fpath As String
    Private UploaderType As String
    Private xFilePath As String = ""

    Private Sub frmForwardBalance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnSelectDocNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectDocNumber.Click
        xSearch.xModule = "DocNum"
        xSearch.ShowDialog()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        xModule = "Search"
        xSearch.xModule = "Search"
        xSearch.ShowDialog()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Public Sub LoadItems()
        grdList.Rows.Clear()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "spu_ForwardBalance_List", _
                                   New SqlParameter("@DocNumber", keyID))
        Try

            While rd.Read
                Dim row As String() = New String() {rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, rd.Item(4).ToString, Format(CDec(rd.Item(5).ToString), "##,##0.00"), Format(CDec(rd.Item(6).ToString), "##,##0.00")}
                grdList.Rows.Add(row)
                dteTransDate.Value = rd.Item(7).ToString
                txtParticulars.Text = rd.Item(8).ToString
                txtDocNumber.Text = rd.Item(9).ToString
                chkPost.Checked = rd.Item(10).ToString
            End While

            rd.Close()
            ComputeDebitAndCreditSumDetails()
            AuditTrail_Save("DATABASE", "Data Migration Tools > Account Forward Balance > View " & txtDocNumber.Text)
        Catch ex As Exception

        End Try

    End Sub

    Public Function NormalizeValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        Try
            If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
                If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                    fieldName = grid.Item(columnName, rowIndex).Value.ToString()
                End If
            End If
        Catch ex As Exception

        End Try
        Return fieldName
    End Function

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtDocNumber.Text),
                    New SqlParameter("@fcDoctype", "FORWARD BALANCE"))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComputeDebitAndCreditSumDetails()
        Dim totalCredit As Decimal
        Dim totaldebit As Decimal
        Try
            For Each xRow As DataGridViewRow In grdList.Rows
                Dim credit As Object = NormalizeValuesInDataGridView(grdList, "fnCredit", xRow.Index)
                Dim debit As Object = NormalizeValuesInDataGridView(grdList, "fnDebit", xRow.Index)
                If credit IsNot Nothing Then
                    totalCredit += credit
                End If
                If debit IsNot Nothing Then
                    totaldebit += debit
                End If
            Next
        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        txtTcredit.Text = Format(CDec(totalCredit), "##,##0.00")
        txtTdebit.Text = Format(CDec(totaldebit), "##,##0.00")
    End Sub

    Private Sub grdList_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdList.CellEndEdit
        Select Case e.ColumnIndex
            Case 5
                grdList.Item("fnCredit", e.RowIndex).Value = 0
            Case 6
                grdList.Item("fnDebit", e.RowIndex).Value = 0
        End Select
    End Sub

    Private Sub grdList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdList.CellValueChanged
        If e.ColumnIndex <> -1 Then
            If grdList.Columns(e.ColumnIndex).Name = "fnDebit" Then
                If NormalizeValuesInDataGridView(grdList, "fnDebit", e.RowIndex) = "" Then
                    grdList.Item("fnDebit", e.RowIndex).Value = 0
                Else
                    grdList.Item("fnDebit", e.RowIndex).Value = Format(CDec(grdList.Item("fnDebit", e.RowIndex).Value.ToString()), "##,##0.00")
                    ComputeDebitAndCreditSumDetails()
                End If
            End If
            If grdList.Columns(e.ColumnIndex).Name = "fnCredit" Then
                If NormalizeValuesInDataGridView(grdList, "fnCredit", e.RowIndex) = "" Then
                    grdList.Item("fnCredit", e.RowIndex).Value = 0
                Else
                    grdList.Item("fnCredit", e.RowIndex).Value = Format(CDec(grdList.Item("fnCredit", e.RowIndex).Value.ToString()), "##,##0.00")
                    ComputeDebitAndCreditSumDetails()
                End If
            End If
        End If
    End Sub

    Private Sub grdList_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdList.EditingControlShowing
        If grdList.CurrentCell.ColumnIndex = 5 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress1
        End If

        If grdList.CurrentCell.ColumnIndex = 6 Then
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress1
        End If
    End Sub

    Private Sub TextBox_keyPress1(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim co_id As New Guid(gCompanyID())
        Try
            If txtDocNumber.Text = "" Then
                MsgBox("No document number.", MsgBoxStyle.Exclamation, "Forward Balance")
            Else
                If txtTcredit.Text = "" And txtTdebit.Text = "" Then
                    MsgBox("Nothing to save. If no totals, check your excel template then change cell format of Debit and Credit Column to Numeric; Delete columns", MsgBoxStyle.Exclamation, "Forward Balance")
                Else
                    UpdateEntry()

                    Dim sSQLCmd As String = "DELETE FROM tForwardBalance WHERE fcRefNo='" & txtDocNumber.Text & "' "
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                    Dim sSQLCmd2 As String = "UPDATE tForwardBalance SET fcRefNo='" & txtDocNumber.Text & "' WHERE fcRefNo='" & "Temp" & "'"
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd2)

                    Dim sSQLCmd3 As String = "UPDATE mDocNumber SET fdDateUsed='" & Now.Date & "'WHERE fcDocNumber='" & txtDocNumber.Text & "' And fkCoid='" & gCompanyID() & "'"
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd3)

                    MsgBox("Data updated", MsgBoxStyle.Information, "Forward Balance")
                    AuditTrail_Save("DATABASE", "Data Migration Tools > Account Forward Balance > Save " & txtDocNumber.Text)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateEntry()
        Try
            For xRow As Integer = 0 To grdList.RowCount - 1
                Dim DocNum As String = txtDocNumber.Text
                Dim memID As String = grdList.Item("fcEmployeeNo", xRow).Value
                Dim AcntReg As String = grdList.Item("fcAccountRef", xRow).Value
                Dim AcntCode As String = grdList.Item("acnt_code", xRow).Value
                Dim dDebit As Decimal = grdList.Item("fnDebit", xRow).Value
                Dim dCredit As Decimal = grdList.Item("fnCredit", xRow).Value
                Dim dtDate As Date = dteTransDate.Value.Date
                Dim cMemo As String = txtParticulars.Text
                Dim isPosted As Boolean = chkPost.Checked.ToString

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_ForwardBalance_Save", _
                                          New SqlParameter("@DocNum", "Temp"), _
                                          New SqlParameter("@Id", memID), _
                                          New SqlParameter("@fcAccountRef", AcntReg), _
                                          New SqlParameter("@AcntCode", AcntCode), _
                                          New SqlParameter("@fnDebit", dDebit), _
                                          New SqlParameter("@fnCredit", dCredit), _
                                          New SqlParameter("@fdDate", dtDate), _
                                          New SqlParameter("@fcDescription", cMemo), _
                                          New SqlParameter("@fbIsPosted", isPosted),
                                          New SqlParameter("@coid", gCompanyID()))
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PrepareSubsidiaryEntry(ByVal checkstatus As Boolean)
        For xRow As Integer = 0 To grdList.RowCount - 1
            Dim journalNo As String = txtDocNumber.Text
            Dim dDebit As Decimal = grdList.Item("fnDebit", xRow).Value
            Dim dCredit As Decimal = grdList.Item("fnCredit", xRow).Value
            Dim sMemo As String = txtParticulars.Text
            Dim dtTransact As Date = dteTransDate.Value.Date
            Dim AccountRef As String = grdList.Item("fcAccountRef", xRow).Value

            If checkstatus = True Then
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, "Cash", 1)
                End If
            Else
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, "Cash", 0)
                End If
            End If
            AccountRef = ""
        Next

    End Sub

    Private Sub UpdateSubsidiary(ByVal AccountRef As String, ByVal RefNo As String, ByVal debit As Decimal, ByVal credit As Decimal, ByVal Memo As String, ByVal fdDate As Date, ByVal PayMethod As String, ByVal cStatus As Boolean)
        Try
            Dim co_id As New Guid(gCompanyID)
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_Update", _
                                      New SqlParameter("@AccountRef", AccountRef), _
                                      New SqlParameter("@RefNo", RefNo), _
                                      New SqlParameter("@Debit", debit), _
                                      New SqlParameter("@Credit", credit), _
                                      New SqlParameter("@Description", Memo), _
                                      New SqlParameter("@fdDate", fdDate), _
                                      New SqlParameter("@PaymentMethod", PayMethod), _
                                      New SqlParameter("@CheckStatus", cStatus), _
                                      New SqlParameter("@co_id", co_id), _
                                      New SqlParameter("@Soaref", ""))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPost.Click
        If chkPost.Checked = True Then
            If txtTcredit.Text = "" And txtTdebit.Text = "" Then
                MsgBox("Nothing to post.", MsgBoxStyle.Information)
                chkPost.Checked = False
            Else
                If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                    Dim sSQLCmd As String = "update tForwardBalance Set fbIsPosted='1' where fcRefNo='" & txtDocNumber.Text & "' "
                    Try
                        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                        Call PrepareSubsidiaryEntry(1)
                        MsgBox("Posting Successful!", MsgBoxStyle.Information)
                        AuditTrail_Save("DATABASE", "Data Migration Tools > Account Forward Balance > Post " & txtDocNumber.Text)
                        grdList.ClearSelection()
                        grdList.CurrentCell = Nothing
                        Call SetAsReadOnly()
                    Catch ex As Exception
                        chkPost.Checked = False
                    End Try
                Else
                    chkPost.Checked = False
                End If
            End If
            
        Else
            If MsgBox("Do you want to UNPOST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                Dim sSQLCmd As String = "update tForwardBalance Set fbIsPosted='0' where fcRefNo='" & txtDocNumber.Text & "' "
                Try
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                    Call PrepareSubsidiaryEntry(0)
                    Call ForEditing()
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                chkPost.Checked = True
            End If
        End If
    End Sub

    Private Sub btnAddAcnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAcnt.Click
        frmSubsidiaryAccountFilter.xModule = "Forward"
        frmSubsidiaryAccountFilter.ShowDialog()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Call ForClear()
        Call ForEditing()
    End Sub

    Private Sub btnRemoveAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAccount.Click
        Try
            grdList.Rows.Remove(grdList.CurrentRow)
            Call ComputeDebitAndCreditSumDetails()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdList_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles grdList.RowsAdded
        grdList.ClearSelection()
        grdList.FirstDisplayedScrollingRowIndex = grdList.RowCount - 1
    End Sub

    Private Sub SetAsReadOnly()
        dteTransDate.Enabled = False
        txtDocNumber.ReadOnly = True
        btnSelectDocNumber.Enabled = False
        txtParticulars.ReadOnly = True
        btnAddAcnt.Enabled = False
        btnRemoveAccount.Enabled = False
    End Sub

    Private Sub ForEditing()
        dteTransDate.Enabled = True
        txtDocNumber.ReadOnly = False
        btnSelectDocNumber.Enabled = True
        txtParticulars.ReadOnly = False
        btnAddAcnt.Enabled = True
        btnRemoveAccount.Enabled = True
    End Sub

    Private Sub ForClear()
        grdList.Rows.Clear()
        txtDocNumber.Text = ""
        dteTransDate.Value = Now.Date
        txtParticulars.Text = ""
        txtTcredit.Text = ""
        txtTdebit.Text = ""
        chkPost.Checked = False
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If txtDocNumber.Text <> "" Then
            If MsgBox("Proceed to delete entry?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Delete Entry") = vbYes Then
                Dim sSQLCmd As String = "DELETE FROM tForwardBalance WHERE fcRefNo='" & txtDocNumber.Text & "' "
                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                Dim sSQLCmd1 As String = "UPDATE mDocNumber SET fdDateUsed=Null WHERE fcDocNumber='" & txtDocNumber.Text & "' "
                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd1)
                AuditTrail_Save("DATABASE", "Data Migration Tools > Account Forward Balance > Delete " & txtDocNumber.Text)
                Call ForClear()
            End If
        End If
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        OpenFileDialog1.ShowDialog()
        filenym = OpenFileDialog1.FileName
        fpath = Path.GetFullPath(filenym)
        txtpath.Text = fpath
        xFilePath = txtpath.Text
        UploaderType = IO.Path.GetExtension(xFilePath)
        If DialogResult.OK Then
            Import()
        End If
    End Sub

    Private Sub Import()
        grdList.Rows.Clear()
        'Dim queryString As String = "select * from [Sheet1$]"
        'Dim MyConnection As System.Data.OleDb.OleDbConnection
        'Dim DtSet As System.Data.DataSet
        'Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        'Dim str As String
        'If UploaderType = ".xlsx" Then
        '    MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & xFilePath & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1""")
        'Else
        '    str = ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & xFilePath & ";Extended Properties=""Excel 12.0 Xml;HDR=Yes;IMEX=1""")
        '    MsgBox(str)
        '    MyConnection = New System.Data.OleDb.OleDbConnection(str)
        '    'MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", xFilePath)

        '    '"provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & xFilePath & "';Extended Properties=Excel 8.0;"
        '    '"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", FilePath
        'End If
        'MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        'MyCommand.TableMappings.Add("Table", "Net-informations.com")
        ''DtSet = New System.Data.DataSet
        ''MyCommand.Fill(DtSet)
        ''grdList.DataSource = DtSet.Tables(0)
        'MyConnection.Open()

        'Dim rd As OleDbDataReader
        'Dim cmd As New OleDbCommand(queryString, MyConnection)
        'rd = cmd.ExecuteReader()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader
        Try
            While rd.Read = True
                Dim row As String() = New String() {rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, rd.Item(4).ToString, Format(CDec(rd.Item(5).ToString), "##,##0.00"), Format(CDec(rd.Item(6).ToString), "##,##0.00")}
                grdList.Rows.Add(row)
            End While
            rd.Close()
            Call ComputeDebitAndCreditSumDetails()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub chkPost_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPost.CheckedChanged

    End Sub
End Class