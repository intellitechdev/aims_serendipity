﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPatronageComputation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboPostingType = New System.Windows.Forms.ComboBox()
        Me.dtpPostingDate = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnPost = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJVNo = New System.Windows.Forms.TextBox()
        Me.btnSearchJVno = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCreditAccount = New System.Windows.Forms.TextBox()
        Me.btnSearchCredit = New System.Windows.Forms.Button()
        Me.txtDebitAccount = New System.Windows.Forms.TextBox()
        Me.btnSearchDebit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grdAccounts = New System.Windows.Forms.DataGridView()
        Me.acntKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.included = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.acntCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCompute = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtRate = New System.Windows.Forms.TextBox()
        Me.txtAllocatedIncome = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.grdPatronage = New System.Windows.Forms.DataGridView()
        Me.memKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Patronage = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.pbStatus = New System.Windows.Forms.ProgressBar()
        Me.bgwPrint = New System.ComponentModel.BackgroundWorker()
        Me.txtTotalPatronage = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.bgwPost = New System.ComponentModel.BackgroundWorker()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdPatronage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboPostingType)
        Me.GroupBox3.Controls.Add(Me.dtpPostingDate)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.btnPost)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtJVNo)
        Me.GroupBox3.Controls.Add(Me.btnSearchJVno)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtCreditAccount)
        Me.GroupBox3.Controls.Add(Me.btnSearchCredit)
        Me.GroupBox3.Controls.Add(Me.txtDebitAccount)
        Me.GroupBox3.Controls.Add(Me.btnSearchDebit)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 351)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(416, 157)
        Me.GroupBox3.TabIndex = 28
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Posting Parameters"
        '
        'cboPostingType
        '
        Me.cboPostingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostingType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPostingType.FormattingEnabled = True
        Me.cboPostingType.Items.AddRange(New Object() {"Per SL", "GL"})
        Me.cboPostingType.Location = New System.Drawing.Point(262, 94)
        Me.cboPostingType.Name = "cboPostingType"
        Me.cboPostingType.Size = New System.Drawing.Size(140, 21)
        Me.cboPostingType.TabIndex = 54
        '
        'dtpPostingDate
        '
        Me.dtpPostingDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPostingDate.Location = New System.Drawing.Point(100, 95)
        Me.dtpPostingDate.Name = "dtpPostingDate"
        Me.dtpPostingDate.Size = New System.Drawing.Size(145, 20)
        Me.dtpPostingDate.TabIndex = 53
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(17, 98)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 52
        Me.Label11.Text = "Posting Date"
        '
        'btnPost
        '
        Me.btnPost.Image = Global.CSAcctg.My.Resources.Resources.Save
        Me.btnPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPost.Location = New System.Drawing.Point(262, 119)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(140, 24)
        Me.btnPost.TabIndex = 51
        Me.btnPost.Text = "Save Patronage"
        Me.btnPost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(53, 123)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "JV No."
        '
        'txtJVNo
        '
        Me.txtJVNo.BackColor = System.Drawing.Color.White
        Me.txtJVNo.Location = New System.Drawing.Point(100, 120)
        Me.txtJVNo.Name = "txtJVNo"
        Me.txtJVNo.ReadOnly = True
        Me.txtJVNo.Size = New System.Drawing.Size(114, 20)
        Me.txtJVNo.TabIndex = 48
        '
        'btnSearchJVno
        '
        Me.btnSearchJVno.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchJVno.Location = New System.Drawing.Point(219, 118)
        Me.btnSearchJVno.Name = "btnSearchJVno"
        Me.btnSearchJVno.Size = New System.Drawing.Size(25, 25)
        Me.btnSearchJVno.TabIndex = 49
        Me.btnSearchJVno.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(5, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 13)
        Me.Label7.TabIndex = 47
        Me.Label7.Text = "Credit Account"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Debit Account"
        '
        'txtCreditAccount
        '
        Me.txtCreditAccount.BackColor = System.Drawing.Color.White
        Me.txtCreditAccount.Location = New System.Drawing.Point(100, 55)
        Me.txtCreditAccount.Name = "txtCreditAccount"
        Me.txtCreditAccount.ReadOnly = True
        Me.txtCreditAccount.Size = New System.Drawing.Size(275, 20)
        Me.txtCreditAccount.TabIndex = 44
        '
        'btnSearchCredit
        '
        Me.btnSearchCredit.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchCredit.Location = New System.Drawing.Point(380, 53)
        Me.btnSearchCredit.Name = "btnSearchCredit"
        Me.btnSearchCredit.Size = New System.Drawing.Size(25, 25)
        Me.btnSearchCredit.TabIndex = 45
        Me.btnSearchCredit.UseVisualStyleBackColor = True
        '
        'txtDebitAccount
        '
        Me.txtDebitAccount.BackColor = System.Drawing.Color.White
        Me.txtDebitAccount.Location = New System.Drawing.Point(100, 29)
        Me.txtDebitAccount.Name = "txtDebitAccount"
        Me.txtDebitAccount.ReadOnly = True
        Me.txtDebitAccount.Size = New System.Drawing.Size(275, 20)
        Me.txtDebitAccount.TabIndex = 42
        '
        'btnSearchDebit
        '
        Me.btnSearchDebit.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchDebit.Location = New System.Drawing.Point(380, 26)
        Me.btnSearchDebit.Name = "btnSearchDebit"
        Me.btnSearchDebit.Size = New System.Drawing.Size(25, 25)
        Me.btnSearchDebit.TabIndex = 43
        Me.btnSearchDebit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grdAccounts)
        Me.GroupBox1.Controls.Add(Me.btnPrint)
        Me.GroupBox1.Controls.Add(Me.dtpDate)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.btnCompute)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtRate)
        Me.GroupBox1.Controls.Add(Me.txtAllocatedIncome)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 333)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Computation Parameters"
        '
        'grdAccounts
        '
        Me.grdAccounts.AllowUserToAddRows = False
        Me.grdAccounts.AllowUserToDeleteRows = False
        Me.grdAccounts.AllowUserToResizeColumns = False
        Me.grdAccounts.AllowUserToResizeRows = False
        Me.grdAccounts.BackgroundColor = System.Drawing.Color.White
        Me.grdAccounts.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdAccounts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdAccounts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdAccounts.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.acntKey, Me.included, Me.acntCode, Me.acntTitle})
        Me.grdAccounts.Location = New System.Drawing.Point(9, 48)
        Me.grdAccounts.Name = "grdAccounts"
        Me.grdAccounts.RowHeadersVisible = False
        Me.grdAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdAccounts.Size = New System.Drawing.Size(399, 218)
        Me.grdAccounts.TabIndex = 43
        '
        'acntKey
        '
        Me.acntKey.HeaderText = "acntKey"
        Me.acntKey.Name = "acntKey"
        Me.acntKey.Visible = False
        '
        'included
        '
        Me.included.HeaderText = ""
        Me.included.Name = "included"
        Me.included.Width = 20
        '
        'acntCode
        '
        Me.acntCode.HeaderText = "Code"
        Me.acntCode.Name = "acntCode"
        Me.acntCode.ReadOnly = True
        Me.acntCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.acntCode.Width = 70
        '
        'acntTitle
        '
        Me.acntTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.acntTitle.HeaderText = "Account Title"
        Me.acntTitle.Name = "acntTitle"
        Me.acntTitle.ReadOnly = True
        Me.acntTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'btnPrint
        '
        Me.btnPrint.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(268, 300)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(140, 24)
        Me.btnPrint.TabIndex = 42
        Me.btnPrint.Text = "Print Patronage"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "yyyy"
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(316, 24)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(83, 20)
        Me.dtpDate.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(213, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Year to Compute"
        '
        'btnCompute
        '
        Me.btnCompute.Image = Global.CSAcctg.My.Resources.Resources.Calculator
        Me.btnCompute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompute.Location = New System.Drawing.Point(268, 274)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(140, 24)
        Me.btnCompute.TabIndex = 13
        Me.btnCompute.Text = "Compute Patronage"
        Me.btnCompute.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCompute.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(139, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Account(s) to Compute:"
        '
        'txtRate
        '
        Me.txtRate.BackColor = System.Drawing.Color.White
        Me.txtRate.Location = New System.Drawing.Point(116, 299)
        Me.txtRate.Name = "txtRate"
        Me.txtRate.ReadOnly = True
        Me.txtRate.Size = New System.Drawing.Size(145, 20)
        Me.txtRate.TabIndex = 7
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAllocatedIncome
        '
        Me.txtAllocatedIncome.Location = New System.Drawing.Point(116, 274)
        Me.txtAllocatedIncome.MaxLength = 18
        Me.txtAllocatedIncome.Name = "txtAllocatedIncome"
        Me.txtAllocatedIncome.Size = New System.Drawing.Size(145, 20)
        Me.txtAllocatedIncome.TabIndex = 6
        Me.txtAllocatedIncome.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 302)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(97, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Rate per Member"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 277)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Allocated Income"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.grdPatronage)
        Me.GroupBox2.Location = New System.Drawing.Point(434, 43)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(475, 423)
        Me.GroupBox2.TabIndex = 29
        Me.GroupBox2.TabStop = False
        '
        'grdPatronage
        '
        Me.grdPatronage.AllowUserToAddRows = False
        Me.grdPatronage.AllowUserToDeleteRows = False
        Me.grdPatronage.AllowUserToResizeColumns = False
        Me.grdPatronage.AllowUserToResizeRows = False
        Me.grdPatronage.BackgroundColor = System.Drawing.Color.White
        Me.grdPatronage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdPatronage.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdPatronage.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.grdPatronage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPatronage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.memKey, Me.memID, Me.memName, Me.Patronage})
        Me.grdPatronage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdPatronage.Location = New System.Drawing.Point(3, 16)
        Me.grdPatronage.Name = "grdPatronage"
        Me.grdPatronage.ReadOnly = True
        Me.grdPatronage.RowHeadersVisible = False
        Me.grdPatronage.Size = New System.Drawing.Size(469, 404)
        Me.grdPatronage.TabIndex = 0
        '
        'memKey
        '
        Me.memKey.HeaderText = "memKey"
        Me.memKey.Name = "memKey"
        Me.memKey.ReadOnly = True
        Me.memKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.memKey.Visible = False
        '
        'memID
        '
        Me.memID.HeaderText = "ID"
        Me.memID.Name = "memID"
        Me.memID.ReadOnly = True
        Me.memID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'memName
        '
        Me.memName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.memName.HeaderText = "Name"
        Me.memName.Name = "memName"
        Me.memName.ReadOnly = True
        Me.memName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Patronage
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Patronage.DefaultCellStyle = DataGridViewCellStyle3
        Me.Patronage.HeaderText = "Patronage"
        Me.Patronage.Name = "Patronage"
        Me.Patronage.ReadOnly = True
        Me.Patronage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(638, 26)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 31
        '
        'pbStatus
        '
        Me.pbStatus.Location = New System.Drawing.Point(437, 23)
        Me.pbStatus.Name = "pbStatus"
        Me.pbStatus.Size = New System.Drawing.Size(195, 20)
        Me.pbStatus.TabIndex = 30
        Me.pbStatus.Visible = False
        '
        'bgwPrint
        '
        Me.bgwPrint.WorkerReportsProgress = True
        Me.bgwPrint.WorkerSupportsCancellation = True
        '
        'txtTotalPatronage
        '
        Me.txtTotalPatronage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalPatronage.BackColor = System.Drawing.Color.White
        Me.txtTotalPatronage.Location = New System.Drawing.Point(761, 474)
        Me.txtTotalPatronage.Name = "txtTotalPatronage"
        Me.txtTotalPatronage.ReadOnly = True
        Me.txtTotalPatronage.Size = New System.Drawing.Size(145, 20)
        Me.txtTotalPatronage.TabIndex = 32
        Me.txtTotalPatronage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(665, 477)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 13)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "Total Patronage"
        '
        'bgwPost
        '
        Me.bgwPost.WorkerReportsProgress = True
        Me.bgwPost.WorkerSupportsCancellation = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "acntKey"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Account Title"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "memKey"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn7.HeaderText = "Patronage"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'frmPatronageComputation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(913, 520)
        Me.Controls.Add(Me.txtTotalPatronage)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.pbStatus)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmPatronageComputation"
        Me.Text = "Patronage Computation"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.grdPatronage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cboPostingType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpPostingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPost As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtJVNo As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchJVno As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCreditAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchCredit As System.Windows.Forms.Button
    Friend WithEvents txtDebitAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchDebit As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grdAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtRate As System.Windows.Forms.TextBox
    Friend WithEvents txtAllocatedIncome As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents acntKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents included As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents acntCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grdPatronage As System.Windows.Forms.DataGridView
    Friend WithEvents memKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents memID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents memName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Patronage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents pbStatus As System.Windows.Forms.ProgressBar
    Friend WithEvents bgwPrint As System.ComponentModel.BackgroundWorker
    Friend WithEvents txtTotalPatronage As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents bgwPost As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
