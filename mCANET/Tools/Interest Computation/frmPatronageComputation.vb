﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmPatronageComputation
    Private gCon As New Clsappconfiguration()
    Dim xAcntKey As String
    Dim xAcntCode As String
    Dim xAcntTitle As String
    Dim xCoid As String
    Dim keyIDdebit As String
    Dim keyIDcredit As String
    Dim entryID As String
    Dim xPatronage As Decimal

    Private Sub frmPatronageComputation_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim sSQLCmd As String = "DELETE FROM tbl_TempPatronage_InterestAccount"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        LoadCreditAccounts()

    End Sub

    Private Sub LoadCreditAccounts()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_PatronageComputation_LoadCreditAccounts",
                                                                            New SqlParameter("@coid", gCompanyID))
        While rd.Read = True
            AddRow(rd.Item("fxkey_AccountID").ToString, rd.Item("fcAccountCode").ToString, rd.Item("fcAccountName").ToString)
        End While
    End Sub

    Private Sub AddRow(ByVal acntKey As String,
                             ByVal acntCode As String,
                             ByVal acntTitle As String)

        Try
            Dim row As String() =
             {acntKey, False, acntCode, acntTitle}

            Dim nRowIndex As Integer
            With grdAccounts

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With
        Catch
            'MsgBox("Error on adding new row." & vbCr & vbCr & "Private Sub AddRow", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub LoadPatronage()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_PatronageComputation",
                                                                            New SqlParameter("@coid", gCompanyID),
                                                                            New SqlParameter("@currentYear", GetLastDayofYear()),
                                                                            New SqlParameter("@allocatedIncome", CDec(txtAllocatedIncome.Text)))
        While rd.Read = True
            txtRate.Text = Format(CDec(rd.Item("Rate")), "##,##0.0000000000")
            AddPatronageRow(rd.Item("memKey").ToString, rd.Item("memID").ToString, rd.Item("memName").ToString, rd.Item("Patronage"))
        End While
    End Sub

    Private Function GetLastDayofYear() As Date
        Dim year As New DateTime(dtpDate.Value.Year, 12, 31)
        Return year
    End Function

    Private Sub AddPatronageRow(ByVal memKey As String,
                             ByVal memID As String,
                             ByVal memName As String,
                             ByVal Patronage As Decimal)

        Try
            Dim row As String() =
             {memKey, memID, memName, Format(CDec(Patronage), "##,##0.00")}

            Dim nRowIndex As Integer
            With grdPatronage
                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
                'xPatronage = xPatronage + Patronage
            End With
        Catch
            'MsgBox("Error on adding new row." & vbCr & vbCr & "Private Sub AddRow", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub grdAccounts_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccounts.CellClick
        GetGridDetails(e.RowIndex)
        If grdAccounts.Item("included", e.RowIndex).Value = True Then
            grdAccounts.Item("included", e.RowIndex).Value = False
            InsertAccountToTemp(0)
        Else
            grdAccounts.Item("included", e.RowIndex).Value = True
            InsertAccountToTemp(1)
        End If
    End Sub

    Private Sub grdAccounts_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccounts.CellDoubleClick
        GetGridDetails(e.RowIndex)
        If grdAccounts.Item("included", e.RowIndex).Value = True Then
            grdAccounts.Item("included", e.RowIndex).Value = False
            InsertAccountToTemp(0)
        Else
            grdAccounts.Item("included", e.RowIndex).Value = True
            InsertAccountToTemp(1)
        End If
    End Sub

    Private Sub GetGridDetails(ByVal xRowIndex As Integer)
        xAcntKey = GetValuesInDataGridView(grdAccounts, "acntKey", xRowIndex)
        xAcntCode = GetValuesInDataGridView(grdAccounts, "acntCode", xRowIndex)
        xAcntTitle = GetValuesInDataGridView(grdAccounts, "acntTitle", xRowIndex)
        xCoid = gCompanyID()
    End Sub

    Private Sub InsertAccountToTemp(ByVal chkStatus As Boolean)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Patronage_TempAccounts_InsertUpdate",
                                                                            New SqlParameter("@acntKey", xAcntKey),
                                                                            New SqlParameter("@acntCode", xAcntCode),
                                                                            New SqlParameter("@acntTitle", xAcntTitle),
                                                                            New SqlParameter("@coid", gCompanyID),
                                                                            New SqlParameter("@CheckStatus", chkStatus))
    End Sub

    Private Sub btnCompute_Click(sender As System.Object, e As System.EventArgs) Handles btnCompute.Click
        If txtAllocatedIncome.Text = "" Then
            MsgBox("Please input Allocated Income.", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        Else
            grdPatronage.Rows.Clear()
            'xPatronage = 0.0
            LoadPatronage()
            'txtTotalPatronage.Text = Format(xPatronage, "N2")
            ComputeTotalPatronage()
        End If

    End Sub

    Private Sub txtAllocatedIncome_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtAllocatedIncome.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub txtAllocatedIncome_LostFocus(sender As Object, e As System.EventArgs) Handles txtAllocatedIncome.LostFocus
        Try
            Dim temp As Double = txtAllocatedIncome.Text
            txtAllocatedIncome.Text = Format(temp, "N2")
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        DeleteTemp()
        If bgwPrint.IsBusy = False Then
            pbStatus.Visible = True
            bgwPrint.WorkerReportsProgress = True
            bgwPrint.RunWorkerAsync()
        Else
            bgwPrint.CancelAsync()
            pbStatus.Visible = True
            bgwPrint.WorkerReportsProgress = True
            bgwPrint.RunWorkerAsync()
        End If
        'InsertToTemp()
        'MsgBox("Done")
    End Sub

    Private Sub DeleteTemp()
        Dim sSQLCmd As String = "DELETE FROM tbl_Temp_PatronageDistribution"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

        Dim sSQLCmd2 As String = "DBCC CHECKIDENT (tbl_Temp_PatronageDistribution, RESEED, 0)"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd2)
    End Sub

    Private Sub bgwPrint_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwPrint.DoWork
        InsertToTemp()
    End Sub

    Private Sub bgwPrint_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPrint.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Loading... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub bgwPrint_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPrint.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""
        pCallCompany(frmPatronageDistribution)
    End Sub

    Private Sub InsertToTemp()
        Try
            For xRow As Integer = 0 To grdPatronage.RowCount - 1
                pRowCount = grdPatronage.RowCount
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Patronage_InserttoTemp", _
                                  New SqlParameter("@memID", GetValuesInDataGridView(grdPatronage, "memID", xRow)),
                                  New SqlParameter("@memName", GetValuesInDataGridView(grdPatronage, "memName", xRow)),
                                  New SqlParameter("@patronage", CDec(GetValuesInDataGridView(grdPatronage, "Patronage", xRow))),
                                  New SqlParameter("@coName", gCompanyName))
                gCon.sqlconn.Close()
                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPrint.ReportProgress(percent)
            Next
        Catch ex As Exception
            MsgBox("Error on printing report." & vbCr & vbCr & "Private Sub InsertToTemp", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub btnSearchDebit_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchDebit.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtDebitAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            keyIDdebit = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnSearchCredit_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchCredit.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtCreditAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            keyIDcredit = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnSearchJVno_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchJVno.Click
        frmJVno.ShowDialog()
        If frmJVno.DialogResult = DialogResult.OK Then
            txtJVNo.Text = frmJVno.listDocNumber.SelectedItems.Item(0).Text
        End If
    End Sub

    Private Sub btnPost_Click(sender As System.Object, e As System.EventArgs) Handles btnPost.Click
        If txtDebitAccount.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        ElseIf txtCreditAccount.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        ElseIf txtJVNo.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        ElseIf cboPostingType.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        Else
            If cboPostingType.Text = "GL" Then
                entryID = System.Guid.NewGuid.ToString()
                saveEntryHeader(entryID)
            Else
                Dim sSQLCmd As String = "DELETE FROM tbl_Temp_Patronage_MembersWithoutPatronageAccount"
                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                If bgwPost.IsBusy = False Then
                    pbStatus.Visible = True
                    bgwPost.WorkerReportsProgress = True
                    bgwPost.RunWorkerAsync()
                Else
                    bgwPost.CancelAsync()
                    pbStatus.Visible = True
                    bgwPost.WorkerReportsProgress = True
                    bgwPost.RunWorkerAsync()
                End If
            End If
        End If
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function

    Private Sub saveEntryHeader(ByVal guid As String)
        Dim user As String = frmMain.currentUser.Text
        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dtpPostingDate.Value.Date, "JV")
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", gCompanyID()),
                    New SqlParameter("@fdTransDate", dtpPostingDate.Value),
                    New SqlParameter("@fdTotAmt", CDec(txtTotalPatronage.Text)),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", "JV"),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", txtJVNo.Text),
                    New SqlParameter("@fbIsCancelled", 0),
                    New SqlParameter("@fbIsPosted", 1),
                    New SqlParameter("@fdDatePrepared", dtpPostingDate.Value),
                    New SqlParameter("@SoaNo", ""))
            Call UpdateDocDate()
            Call saveCreditEntry(guid)
        Catch ex As Exception
            MsgBox("Error on saving Entry Header. (Private Sub saveEntryHeader)", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub saveCreditEntry(ByVal guid As String)
        Dim journalNo As String = txtJVNo.Text
        Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
        Dim sMemo As String = "Patronage Distribution for the year " & Year(dtpPostingDate.Value)
        Dim pkJournalID As String = guid

        Select Case cboPostingType.Text
            Case "GL"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                          New SqlParameter("@fiEntryNo", journalNo), _
                                          New SqlParameter("@fxKeyAccount", keyIDcredit), _
                                          New SqlParameter("@fdDebit", 0), _
                                          New SqlParameter("@fdCredit", CDec(txtTotalPatronage.Text)), _
                                          New SqlParameter("@fcMemo", sMemo), _
                                          New SqlParameter("@fxKey", System.Guid.NewGuid.ToString()), _
                                          New SqlParameter("@fk_JVHeader", guid), _
                                          New SqlParameter("@transDate", dtpPostingDate.Value), _
                                          New SqlParameter("@fcLoanRef", Nothing), _
                                          New SqlParameter("@fcAccRef", Nothing))
                    gCon.sqlconn.Close()
                    Call saveDebitEntry(guid)
                Catch ex As Exception
                    MsgBox("Error on saving Entry Header. (Private Sub saveEntryHeader)", MsgBoxStyle.Exclamation, "Patronage Computation")
                    Exit Sub
                End Try

            Case "Per SL"
                If BackgroundWorker1.IsBusy = False Then
                    pbStatus.Visible = True
                    BackgroundWorker1.WorkerReportsProgress = True
                    BackgroundWorker1.RunWorkerAsync()
                Else
                    BackgroundWorker1.CancelAsync()
                    pbStatus.Visible = True
                    BackgroundWorker1.WorkerReportsProgress = True
                    BackgroundWorker1.RunWorkerAsync()
                End If
        End Select

    End Sub

    Private Sub saveCreditEntryPerSL(ByVal guid As String)
        Dim journalNo As String = txtJVNo.Text
        Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
        Dim sMemo As String = "Patronage Distribution for the year " & Year(dtpPostingDate.Value)
        Dim pkJournalID As String = guid
        Try
            For xRow As Integer = 0 To grdPatronage.RowCount - 1
                pRowCount = grdPatronage.RowCount
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save_Patronage",
                                  New SqlParameter("@fiEntryNo", journalNo), _
                                  New SqlParameter("@fxKeyAccount", keyIDcredit), _
                                  New SqlParameter("@fdDebit", 0), _
                                  New SqlParameter("@fdCredit", CDec(GetValuesInDataGridView(grdPatronage, "Patronage", xRow))), _
                                  New SqlParameter("@fcMemo", sMemo), _
                                  New SqlParameter("@fxKeyNameID", GetValuesInDataGridView(grdPatronage, "memKey", xRow)), _
                                  New SqlParameter("@fxKey", System.Guid.NewGuid.ToString()), _
                                  New SqlParameter("@fk_JVHeader", guid), _
                                  New SqlParameter("@transDate", dtpPostingDate.Value))
                Dim percent As Integer = (xRow / pRowCount) * 100
                BackgroundWorker1.ReportProgress(percent)
            Next

            gCon.sqlconn.Close()
            Call saveDebitEntry(guid)

            Dim sSQLCmd As String = "INSERT INTO tbl_ComputationList (TransKey, JVNo, coid, TransType, Posted) VALUES ('" & guid & "', '" & txtJVNo.Text & "', '" & gCompanyID() & "', 'Patronage'," & 1 & ")"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

        Catch ex As Exception
            MsgBox("CheckMembersSL")
            Exit Sub
        End Try
    End Sub

    Private Sub saveDebitEntry(ByVal guid As String)
        Try
            Dim journalNo As String = txtJVNo.Text
            Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
            Dim sMemo As String = "Patronage Distribution for the year " & Year(dtpPostingDate.Value)
            Dim pkJournalID As String = guid
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                          New SqlParameter("@fiEntryNo", journalNo), _
                                          New SqlParameter("@fxKeyAccount", keyIDdebit), _
                                          New SqlParameter("@fdDebit", CDec(txtTotalPatronage.Text)), _
                                          New SqlParameter("@fdCredit", 0), _
                                          New SqlParameter("@fcMemo", sMemo), _
                                          New SqlParameter("@fxKey", jvDetailRecordID), _
                                          New SqlParameter("@fk_JVHeader", pkJournalID), _
                                          New SqlParameter("@transDate", dtpPostingDate.Value), _
                                          New SqlParameter("@fcLoanRef", Nothing), _
                                          New SqlParameter("@fcAccRef", Nothing))
            gCon.sqlconn.Close()
            MsgBox("Posting Successful.", MsgBoxStyle.Information, "Patronage Computation")
            Me.Close()
        Catch ex As Exception
            'MsgBox("Error on saving Debit Entry. (Private Sub saveDebitEntry)")
            Exit Sub
        End Try
    End Sub

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtJVNo.Text),
                    New SqlParameter("@fcDoctype", "JOURNAL VOUCHER"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception
            MsgBox("Error on updating document number. (Private Sub UpdateDocDate)", MsgBoxStyle.Exclamation, "Patronage Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub CheckMembersSL()
        Try
            For xRow As Integer = 0 To grdPatronage.RowCount - 1
                pRowCount = grdPatronage.RowCount
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Patronage_MembersWithoutPatronageAccount",
                                                                                            New SqlParameter("@memKey", GetValuesInDataGridView(grdPatronage, "memKey", xRow)),
                                                                                            New SqlParameter("@memID", GetValuesInDataGridView(grdPatronage, "memID", xRow)),
                                                                                            New SqlParameter("@memName", GetValuesInDataGridView(grdPatronage, "memName", xRow)),
                                                                                            New SqlParameter("@acntKey", keyIDcredit))
                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPost.ReportProgress(percent)
            Next
        Catch ex As Exception
            MsgBox("CheckMembersSL")
            Exit Sub
        End Try
    End Sub

    Private Sub bgwPost_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwPost.DoWork
        CheckMembersSL()
    End Sub

    Private Sub bgwPost_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPost.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Checking Members' Patronage SL... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub bgwPost_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPost.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, "SELECT * FROM tbl_Temp_Patronage_MembersWithoutPatronageAccount")
            If rd.Read = True Then
                MsgBox("Some clients doesn't have Subsidiary Ledger for Patronage Refund.")
                pCallCompany(frmPatronageSLChecking)
            Else
                'MsgBox("Proceed to SL Posting")
                entryID = System.Guid.NewGuid.ToString()
                saveEntryHeader(entryID)
            End If
        End Using
    End Sub

    Private Sub ComputeTotalPatronage()
        Dim totalPatronage As Decimal

        Try
            For Each xRow As DataGridViewRow In grdPatronage.Rows
                Dim Patronage As Object = GetValuesInDataGridView(grdPatronage, "Patronage", xRow.Index)

                If Patronage IsNot Nothing Then
                    totalPatronage += Patronage
                End If

            Next

        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        txtTotalPatronage.Text = Format(CDec(totalPatronage), "##,##0.00")
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        saveCreditEntryPerSL(entryID)
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Posting Patronage per SL... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""
    End Sub
End Class