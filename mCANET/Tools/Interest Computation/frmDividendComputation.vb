﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmDividendComputation
    Private gCon As New Clsappconfiguration()
    Dim keyID As String
    Dim keyIDdebit As String
    Dim keyIDcredit As String
    Dim xCount As String
    Dim xDividend As Decimal
    Dim entryID As String
    Private Sub btnProcess_Click(sender As System.Object, e As System.EventArgs) Handles btnCompute.Click
        If txtAccount.Text <> "" And txtAllocatedIncome.Text <> "" Then
            grdList.Rows.Clear()
            xCount = 0
            xDividend = 0
            ComputeDividend()
        ElseIf txtAccount.Text = "" And txtAllocatedIncome.Text <> "" Then
            MsgBox("Select account first.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        ElseIf txtAccount.Text <> "" And txtAllocatedIncome.Text = "" Then
            MsgBox("Set Allocated Income.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        ElseIf txtAccount.Text = "" And txtAllocatedIncome.Text = "" Then
            MsgBox("Select Account and Set Allocated Income.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        End If
    End Sub

    Private Sub ComputeDividend()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DividendComputation",
                                                                            New SqlParameter("@acntKey", keyID),
                                                                            New SqlParameter("@coid", gCompanyID()),
                                                                            New SqlParameter("@CurrentYear", dtpDate.Value),
                                                                            New SqlParameter("@AllocatedIncome", CDec(txtAllocatedIncome.Text)))
        While rd.Read = True
            txtDividendFactor.Text = rd.Item("DividendFactor")
            txtTotalAverageShare.Text = Format(rd.Item("TotalAverageShare"), "##,##0.00")
            txtTotalDividend.Text = Format(rd.Item("TotalAverageShare"), "##,##0.00")
            AddRow(rd.Item("memKey").ToString, rd.Item("memID").ToString, rd.Item("memName").ToString, rd.Item("acntRef").ToString, rd.Item("JanBal"), rd.Item("FebBal"), rd.Item("MarBal"), rd.Item("AprBal"), rd.Item("MayBal"), rd.Item("JunBal"), rd.Item("JulBal"), rd.Item("AugBal"), rd.Item("SepBal"), rd.Item("OctBal"), rd.Item("NovBal"), rd.Item("DecBal"), rd.Item("AverageShare"), rd.Item("colDividend"))
        End While
        txtTotalDividend.Text = Format(xDividend, "##,##0.00")
        txtTotalMember.Text = xCount
    End Sub

    Private Sub AddRow(ByVal memKey As String,
                             ByVal memID As String,
                             ByVal cName As String,
                             ByVal cAcntRef As String,
                             ByVal cJAN As Decimal,
                             ByVal cFEB As Decimal,
                             ByVal cMAR As Decimal,
                             ByVal cAPR As Decimal,
                             ByVal cMAY As Decimal,
                             ByVal cJUN As Decimal,
                             ByVal cJUL As Decimal,
                             ByVal cAUG As Decimal,
                             ByVal cSEP As Decimal,
                             ByVal cOCT As Decimal,
                             ByVal cNOV As Decimal,
                             ByVal cDECM As Decimal,
                             ByVal AverageShare As Decimal,
                             ByVal Dividend As Decimal)

        Try
            Dim row As String() =
             {memKey, memID, cName, cAcntRef, cJAN, Format(CDec(cFEB), "##,##0.00"), Format(CDec(cMAR), "##,##0.00"), Format(CDec(cAPR), "##,##0.00"), Format(CDec(cMAY), "##,##0.00"), Format(CDec(cJUN), "##,##0.00"), Format(CDec(cJUL), "##,##0.00"), Format(CDec(cAUG), "##,##0.00"), Format(CDec(cSEP), "##,##0.00"), Format(CDec(cOCT), "##,##0.00"), Format(CDec(cNOV), "##,##0.00"), Format(CDec(cDECM), "##,##0.00"), Format(CDec(AverageShare), "##,##0.00"), Format(Dividend, "##,##0.00")}

            Dim nRowIndex As Integer
            With grdList
                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
                xCount = xCount + 1
                xDividend = xDividend + Dividend
            End With
        Catch
            MsgBox("Error on adding new row." & vbCr & vbCr & "Private Sub AddRow", MsgBoxStyle.Exclamation, "Dividend Computation")
        End Try
    End Sub

    Private Sub SetControlEnabled(ByVal ctl As Control, ByVal enabled As Boolean)
        If ctl.InvokeRequired Then
            ctl.BeginInvoke(New Action(Of Control, Boolean)(AddressOf SetControlEnabled), ctl, enabled)
        Else
            ctl.Enabled = enabled
        End If
    End Sub

    Private Sub btnAccount_Click(sender As System.Object, e As System.EventArgs) Handles btnAccount.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            keyID = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub bgwCompute_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwPrint.DoWork
        InsertToTemp()
    End Sub

    Private Sub bgwCompute_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPrint.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""
        pCallCompany(frmDividendReport)
        'MsgBox("Done")
    End Sub

    Private Sub bgwCompute_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPrint.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Loading... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub txtAllocatedIncome_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtAllocatedIncome.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub txtAllocatedIncome_LostFocus(sender As Object, e As System.EventArgs) Handles txtAllocatedIncome.LostFocus
        Try
            Dim temp As Double = txtAllocatedIncome.Text
            txtAllocatedIncome.Text = Format(temp, "N2")
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnSearchDebit_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchDebit.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtDebitAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            keyIDdebit = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnPost_Click(sender As System.Object, e As System.EventArgs) Handles btnPost.Click
        If txtDebitAccount.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        ElseIf txtCreditAccount.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        ElseIf txtJVNo.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        ElseIf cboPostingType.Text = "" Then
            MsgBox("Please input all posting parameters.", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        Else
            If cboPostingType.Text = "GL" Then
                entryID = System.Guid.NewGuid.ToString()
                saveEntryHeader(entryID)
            Else
                Dim sSQLCmd As String = "DELETE FROM tbl_Temp_Patronage_MembersWithoutPatronageAccount"
                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                If bgwPost.IsBusy = False Then
                    pbStatus.Visible = True
                    bgwPost.WorkerReportsProgress = True
                    bgwPost.RunWorkerAsync()
                Else
                    bgwPost.CancelAsync()
                    pbStatus.Visible = True
                    bgwPost.WorkerReportsProgress = True
                    bgwPost.RunWorkerAsync()
                End If
            End If
        End If
    End Sub


    Private Sub btnSearchCredit_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchCredit.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtCreditAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_name").Value.ToString()
            keyIDcredit = frmSearchAccount.grdCoAList.SelectedRows(0).Cells("acnt_id").Value.ToString()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnSearchJVno_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchJVno.Click
        frmJVno.ShowDialog()
        If frmJVno.DialogResult = DialogResult.OK Then
            txtJVNo.Text = frmJVno.listDocNumber.SelectedItems.Item(0).Text
        End If
    End Sub

    Private Sub ComputeTotals()
        Dim totalDividend As Decimal

        Try
            For Each xRow As DataGridViewRow In grdList.Rows
                Dim dividend As Object = grdList.Item("colDividend", xRow.Index).Value

                If dividend IsNot Nothing Then
                    totalDividend += dividend
                End If
            Next
        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        txtDividendFactor.Text = Format(CDec(totalDividend), "##,##0.00")
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        DeleteTemp()
        If bgwPrint.IsBusy = False Then
            pbStatus.Visible = True
            bgwPrint.WorkerReportsProgress = True
            bgwPrint.RunWorkerAsync()
        Else
            bgwPrint.CancelAsync()
            pbStatus.Visible = True
            bgwPrint.WorkerReportsProgress = True
            bgwPrint.RunWorkerAsync()
        End If
        'InsertToTemp()
        'MsgBox("Done")
    End Sub

    Private Sub DeleteTemp()
        Dim sSQLCmd As String = "DELETE FROM tbl_TempDividend"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

        Dim sSQLCmd2 As String = "DBCC CHECKIDENT (tbl_TempDividend, RESEED, 0)"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd2)
    End Sub

    Private Sub InsertToTemp()
        Try
            For xRow As Integer = 0 To grdList.RowCount - 1
                pRowCount = grdList.RowCount
                Div_memID = GetValuesInDataGridView(grdList, "memID", xRow)
                Div_memName = GetValuesInDataGridView(grdList, "cName", xRow)
                Div_acntRef = GetValuesInDataGridView(grdList, "cAcntRef", xRow)
                Div_janBal = GetValuesInDataGridView(grdList, "cJan", xRow)
                Div_febBal = GetValuesInDataGridView(grdList, "cFeb", xRow)
                Div_marBal = GetValuesInDataGridView(grdList, "cMar", xRow)
                Div_aprBal = GetValuesInDataGridView(grdList, "cApr", xRow)
                Div_mayBal = GetValuesInDataGridView(grdList, "cMay", xRow)
                Div_junBal = GetValuesInDataGridView(grdList, "cJun", xRow)
                Div_julBal = GetValuesInDataGridView(grdList, "cJul", xRow)
                Div_augBal = GetValuesInDataGridView(grdList, "cAug", xRow)
                Div_sepBal = GetValuesInDataGridView(grdList, "cSep", xRow)
                Div_octBal = GetValuesInDataGridView(grdList, "cOct", xRow)
                Div_novBal = GetValuesInDataGridView(grdList, "cNov", xRow)
                Div_decBal = GetValuesInDataGridView(grdList, "colDec", xRow)
                Div_averageBal = GetValuesInDataGridView(grdList, "cAverage", xRow)
                Div_dividend = GetValuesInDataGridView(grdList, "cDividend", xRow)
                Div_AllocatedIncome = CDec(txtAllocatedIncome.Text)
                Div_DividendFactor = CDbl(txtDividendFactor.Text)
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Dividend_InsertToTemp", _
                                  New SqlParameter("@coName", gCompanyName),
                                  New SqlParameter("@memID", Div_memID),
                                  New SqlParameter("@memName", Div_memName),
                                  New SqlParameter("@acntRef", Div_acntRef),
                                  New SqlParameter("@janBal", Div_janBal),
                                  New SqlParameter("@febBal", Div_febBal),
                                  New SqlParameter("@marBal", Div_marBal),
                                  New SqlParameter("@aprBal", Div_aprBal),
                                  New SqlParameter("@novBal", Div_mayBal),
                                  New SqlParameter("@mayBal", Div_junBal),
                                  New SqlParameter("@junBal", Div_julBal),
                                  New SqlParameter("@julBal", Div_augBal),
                                  New SqlParameter("@augBal", Div_sepBal),
                                  New SqlParameter("@sepBal", Div_octBal),
                                  New SqlParameter("@octBal", Div_novBal),
                                  New SqlParameter("@decBal", Div_decBal),
                                  New SqlParameter("@averageBal", Div_averageBal),
                                  New SqlParameter("@dividend", Div_dividend),
                                  New SqlParameter("@allocatedincome", Div_AllocatedIncome),
                                  New SqlParameter("@dividendfactor", Div_DividendFactor))
                gCon.sqlconn.Close()
                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPrint.ReportProgress(percent)
            Next
        Catch ex As Exception
            MsgBox("Error on printing report." & vbCr & vbCr & "Private Sub InsertToTemp", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub saveEntryHeader(ByVal guid As String)
        Dim user As String = frmMain.currentUser.Text
        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dtpPostingDate.Value.Date, "JV")
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", gCompanyID()),
                    New SqlParameter("@fdTransDate", dtpPostingDate.Value),
                    New SqlParameter("@fdTotAmt", CDec(txtTotalDividend.Text)),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", "JV"),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", txtJVNo.Text),
                    New SqlParameter("@fbIsCancelled", 0),
                    New SqlParameter("@fbIsPosted", 1),
                    New SqlParameter("@fdDatePrepared", dtpPostingDate.Value),
                    New SqlParameter("@SoaNo", ""))
            Call UpdateDocDate()
            Call saveCreditEntry(guid)
        Catch ex As Exception
            MsgBox("Error on saving Entry Header. (Private Sub saveEntryHeader)", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub saveCreditEntry(ByVal guid As String)
        Dim journalNo As String = txtJVNo.Text
        Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
        Dim sMemo As String = "Patronage Distribution for the year " & Year(dtpPostingDate.Value)
        Dim pkJournalID As String = guid

        Select Case cboPostingType.Text
            Case "GL"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                          New SqlParameter("@fiEntryNo", journalNo), _
                                          New SqlParameter("@fxKeyAccount", keyIDcredit), _
                                          New SqlParameter("@fdDebit", 0), _
                                          New SqlParameter("@fdCredit", CDec(txtTotalDividend.Text)), _
                                          New SqlParameter("@fcMemo", sMemo), _
                                          New SqlParameter("@fxKey", System.Guid.NewGuid.ToString()), _
                                          New SqlParameter("@fk_JVHeader", guid), _
                                          New SqlParameter("@transDate", dtpPostingDate.Value), _
                                          New SqlParameter("@fcLoanRef", Nothing), _
                                          New SqlParameter("@fcAccRef", Nothing))
                    gCon.sqlconn.Close()
                    Call saveDebitEntry(guid)
                Catch ex As Exception
                    MsgBox("Error on saving Entry Header. (Private Sub saveEntryHeader)", MsgBoxStyle.Exclamation, "Patronage Computation")
                    Exit Sub
                End Try

            Case "Per SL"
                If BackgroundWorker1.IsBusy = False Then
                    pbStatus.Visible = True
                    BackgroundWorker1.WorkerReportsProgress = True
                    BackgroundWorker1.RunWorkerAsync()
                Else
                    BackgroundWorker1.CancelAsync()
                    pbStatus.Visible = True
                    BackgroundWorker1.WorkerReportsProgress = True
                    BackgroundWorker1.RunWorkerAsync()
                End If
        End Select

    End Sub

    Private Sub saveDebitEntry(ByVal guid As String)
        Try
            Dim journalNo As String = txtJVNo.Text
            Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
            Dim sMemo As String = "Dividend Distribution for the year " & Year(dtpPostingDate.Value)
            Dim pkJournalID As String = guid
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                          New SqlParameter("@fiEntryNo", journalNo), _
                                          New SqlParameter("@fxKeyAccount", keyIDdebit), _
                                          New SqlParameter("@fdDebit", CDec(txtTotalDividend.Text)), _
                                          New SqlParameter("@fdCredit", 0), _
                                          New SqlParameter("@fcMemo", sMemo), _
                                          New SqlParameter("@fxKey", jvDetailRecordID), _
                                          New SqlParameter("@fk_JVHeader", pkJournalID), _
                                          New SqlParameter("@transDate", dtpPostingDate.Value), _
                                          New SqlParameter("@fcLoanRef", Nothing), _
                                          New SqlParameter("@fcAccRef", Nothing))
            gCon.sqlconn.Close()
            MsgBox("Posting Successful.", MsgBoxStyle.Information, "Dividend Computation")
            Me.Close()
        Catch ex As Exception
            'MsgBox("Error on saving Debit Entry. (Private Sub saveDebitEntry)")
            Exit Sub
        End Try

    End Sub

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtJVNo.Text),
                    New SqlParameter("@fcDoctype", "JOURNAL VOUCHER"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception
            MsgBox("Error on updating document number. (Private Sub UpdateDocDate)", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        End Try
    End Sub

    Private Sub frmDividendComputation_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        
    End Sub

    Private Sub CheckMembersSL()
        Try
            For xRow As Integer = 0 To grdList.RowCount - 1
                pRowCount = grdList.RowCount
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Patronage_MembersWithoutPatronageAccount",
                                                                                            New SqlParameter("@memKey", GetValuesInDataGridView(grdList, "memKey", xRow)),
                                                                                            New SqlParameter("@memID", GetValuesInDataGridView(grdList, "memID", xRow)),
                                                                                            New SqlParameter("@memName", GetValuesInDataGridView(grdList, "cName", xRow)),
                                                                                            New SqlParameter("@acntKey", keyIDcredit))
                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPost.ReportProgress(percent)
            Next
        Catch ex As Exception
            MsgBox("CheckMembersSL")
            Exit Sub
        End Try
    End Sub

    Private Sub bgwPost_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwPost.DoWork
        CheckMembersSL()
    End Sub

    Private Sub bgwPost_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPost.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Checking Members' Dividend SL... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub bgwPost_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPost.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, "SELECT * FROM tbl_Temp_Patronage_MembersWithoutPatronageAccount")
            If rd.Read = True Then
                MsgBox("Some clients doesn't have Subsidiary Ledger for Dividend Account.")
                pCallCompany(frmPatronageSLChecking)
            Else
                'MsgBox("Proceed to SL Posting")
                entryID = System.Guid.NewGuid.ToString()
                saveEntryHeader(entryID)
            End If
        End Using
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        saveCreditEntryPerSL(entryID)
    End Sub

    Private Sub saveCreditEntryPerSL(ByVal guid As String)
        Dim journalNo As String = txtJVNo.Text
        Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
        Dim sMemo As String = "Dividend Distribution for the year " & Year(dtpPostingDate.Value)
        Dim pkJournalID As String = guid
        Try
            For xRow As Integer = 0 To grdList.RowCount - 1
                pRowCount = grdList.RowCount
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save_Patronage",
                                  New SqlParameter("@fiEntryNo", journalNo), _
                                  New SqlParameter("@fxKeyAccount", keyIDcredit), _
                                  New SqlParameter("@fdDebit", 0), _
                                  New SqlParameter("@fdCredit", CDec(GetValuesInDataGridView(grdList, "cDividend", xRow))), _
                                  New SqlParameter("@fcMemo", sMemo), _
                                  New SqlParameter("@fxKeyNameID", GetValuesInDataGridView(grdList, "memKey", xRow)), _
                                  New SqlParameter("@fxKey", System.Guid.NewGuid.ToString()), _
                                  New SqlParameter("@fk_JVHeader", guid), _
                                  New SqlParameter("@transDate", dtpPostingDate.Value))
                Dim percent As Integer = (xRow / pRowCount) * 100
                BackgroundWorker1.ReportProgress(percent)
            Next

            gCon.sqlconn.Close()
            Call saveDebitEntry(guid)

            Dim sSQLCmd As String = "INSERT INTO tbl_ComputationList (TransKey, JVNo, coid, TransType, Posted) VALUES ('" & guid & "', '" & txtJVNo.Text & "', '" & gCompanyID() & "', 'Dividend'," & 1 & ")"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

        Catch ex As Exception
            MsgBox("CheckMembersSL")
            Exit Sub
        End Try
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Posting Dividend per SL... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""
    End Sub
End Class