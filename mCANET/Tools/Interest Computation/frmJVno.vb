﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmJVno

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xDoctype As String

    Private Sub ViewDocNum(ByVal DocType As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_List_Unused", _
                                   New SqlParameter("@fcDocType", xDoctype),
                                   New SqlParameter("@coName", frmMain.lblCompanyName.Text))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Document Number", 300, HorizontalAlignment.Center)
            '.Columns.Add("Date Used", 150, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        '.SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "View Document Number")
            End Try
        End With
    End Sub

    Private Sub listDocNumber_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listDocNumber.DoubleClick
        'frmGeneralJournalEntries.txtGeneralJournalNo.Text = listDocNumber.SelectedItems.Item(0).Text
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call SearchDocNum(txtSearch.Text)
    End Sub

    Private Sub SearchDocNum(ByVal DocNum As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Search_Unused", _
                                   New SqlParameter("@fcDocNumber", DocNum),
                                   New SqlParameter("@fcDocType", xDoctype),
                                   New SqlParameter("@coName", frmMain.lblCompanyName.Text))
        With Me.listDocNumber
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Document Number", 300, HorizontalAlignment.Center)
            '.Columns.Add("Date Used", 150, HorizontalAlignment.Center)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                    End With
                End While
                'rd.Close()
            Catch ex As Exception
                'MessageBox.Show(ex.Message, "View Document Number")
            Finally
                rd.Close()
            End Try
        End With
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        'frm_vend_EnterBills.txtRefNo.Text = listDocNumber.SelectedItems.Item(0).Text
        'frmGeneralJournalEntries.txtGeneralJournalNo.Text = listDocNumber.SelectedItems.Item(0).Text
        'Me.Close()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub frmJVno_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call SearchDocNum(txtSearch.Text)
    End Sub
End Class