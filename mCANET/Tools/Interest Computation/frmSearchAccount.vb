﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmSearchAccount
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub frmSearchAccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call filterAccounts()
        txtSearch.Text = ""
        Me.ActiveControl = txtSearch
    End Sub

    Private Sub grdCoAList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdCoAList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call filterAccounts()
    End Sub

    Private Sub filterAccounts()
        '"CIMS_DebitCredit_SelectAll"
        Select Case xModule
            Case "Entry"
                Dim mycon As New Clsappconfiguration
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_mAccountsDS_filter",
                                               New SqlParameter("@coid", gCompanyID),
                                               New SqlParameter("@acnt_name", txtSearch.Text))
                grdCoAList.DataSource = ds.Tables(0)

                With grdCoAList
                    .Columns("fcDescription").Visible = False
                    .Columns("acnt_code").Width = 80
                    .Columns("acnt_code").HeaderText = "Code"
                    .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("acnt_id").Visible = False
                    .Columns("acnt_name").Width = 420
                    .Columns("acnt_name").HeaderText = "Account Title"
                    .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("code_description").Visible = False
                    .Columns("isdebit").Visible = False
                End With
            Case "Account"
                Dim mycon As New Clsappconfiguration
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "CIMS_DebitCredit_Filter",
                                               New SqlParameter("@filter", txtSearch.Text),
                                               New SqlParameter("@coid", gCompanyID))
                grdCoAList.DataSource = ds.Tables(0)
                With grdCoAList
                    .Columns("fcAccountCode").Width = 80
                    .Columns("fcAccountCode").HeaderText = "Code"
                    .Columns("fcAccountCode").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("fcAccountName").Width = 420
                    .Columns("fcAccountName").HeaderText = "Account Title"
                    .Columns("fcAccountName").SortMode = DataGridViewColumnSortMode.NotSortable
                End With
        End Select
        
    End Sub

End Class