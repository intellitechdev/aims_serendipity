﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports Microsoft.Reporting.WinForms
Public Class frmSavingsInterestComputation
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim startDate As Date
    Dim endDate As Date
    Dim entryID As String
    Dim KeyAccountDebit As String
    Dim KeyAccountCredit As String
    Dim Interest As Decimal

    Private Sub btnAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccount.Click
        frmSearchAccount.xModule = "Account"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
        End If
    End Sub

    Private Sub btnDebitAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDebitAccount.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtDebitAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
            KeyAccountDebit = frmSearchAccount.grdCoAList.SelectedRows(0).Cells(3).Value.ToString()
        End If
    End Sub

    Private Sub btnCreditAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreditAccount.Click
        frmSearchAccount.xModule = "Entry"
        frmSearchAccount.ShowDialog()
        If frmSearchAccount.DialogResult = DialogResult.OK Then
            txtCreditAccount.Text = frmSearchAccount.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
            KeyAccountCredit = frmSearchAccount.grdCoAList.SelectedRows(0).Cells(3).Value.ToString()
        End If
    End Sub

    Private Sub txtInterestPerAnnum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtInterestPerAnnum.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If
    End Sub

    Private Sub txtMinBal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMinBal.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub GetDates()
        Select Case cboPeriodToCompute.Text
            Case "Monthly"
                startDate = DateAdd("m", 0, DateSerial(Year(dtStartingMonth.Value), Month(dtStartingMonth.Value), 1))
                endDate = DateAdd("m", 1, DateSerial(Year(dtStartingMonth.Value), Month(dtStartingMonth.Value), 0))
            Case "Quarterly"
                startDate = DateAdd("m", 0, DateSerial(Year(dtStartingMonth.Value), Month(dtStartingMonth.Value), 1))
                endDate = DateAdd("m", 3, DateSerial(Year(dtStartingMonth.Value), Month(dtStartingMonth.Value), 0))
            Case "6 Months"
                startDate = DateAdd("m", 0, DateSerial(Year(dtStartingMonth.Value), Month(dtStartingMonth.Value), 1))
                endDate = DateAdd("m", 6, DateSerial(Year(dtStartingMonth.Value), Month(dtStartingMonth.Value), 0))
        End Select
        
    End Sub

    Private Sub btnProcessInterest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessInterest.Click
        If txtAccount.Text = "" Then
            MsgBox("Please fill up all computation parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        ElseIf txtInterestPerAnnum.Text = "" Then
            MsgBox("Please fill up all computation parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        ElseIf txtMinBal.Text = "" Then
            MsgBox("Please fill up all computation parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        ElseIf cboPeriodToCompute.Text = "" Then
            MsgBox("Please fill up all computation parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        Else
            If grdComputation.Rows.Count <> 0 Then
                grdComputation.Rows.Clear()
            End If
            Call GetDates()
            Int_PeriodToCompute = cboPeriodToCompute.Text
            Int_Rate = InterestRate()
            Int_AccountName = txtAccount.Text
            Int_minimum = Val(txtMinBal.Text)
            'Call ComputeInterest()
            'picLoading.Visible = True
            'If BackgroundWorker1.IsBusy = False Then
            '    BackgroundWorker1.RunWorkerAsync()
            'End If
            ComputeInterest()
            'Call ComputeTotals()
            AuditTrail_Save("Computations", "Savings Interest > Process Interest > Account: " & txtAccount.Text & ", Interest Per Annum: " & txtInterestPerAnnum.Text & "%, Minimum Balance to Compute: " & txtMinBal.Text & ", Starting Month to Compute: " & dtStartingMonth.Text & ", Period to Compute: " & cboPeriodToCompute.Text)
        End If
    End Sub

    Public Function InterestRate() As Decimal
        Dim txtRate As Decimal = Val(txtInterestPerAnnum.Text)
        Dim xRate As Decimal
        xRate = txtRate / 100
        Return xRate
    End Function

    Private Sub ComputeInterest()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_SavingsInterestComputation",
                                                                                New SqlParameter("@startDate", startDate),
                                                                                New SqlParameter("@endDate", endDate),
                                                                                New SqlParameter("@Rate", Int_Rate),
                                                                                New SqlParameter("@acntName", Int_AccountName),
                                                                                New SqlParameter("@minimumBal", Int_minimum))
            While rd.Read
                AddDetailItem(rd.Item("memKey").ToString, rd.Item("memID").ToString, rd.Item("memName").ToString, rd.Item("refNo").ToString, rd.Item("begbal"), rd.Item("endbal"), rd.Item("adb"), rd.Item("Interest"))
            End While
            rd.Close()
            ComputeTotals()
        Catch ex As Exception
            MsgBox(ex.ToString)
            Exit Sub
        End Try

    End Sub

    Private Sub AddDetailItem(ByVal empKey As String,
                             ByVal empID As String,
                             ByVal empName As String,
                             ByVal RefNo As String,
                             ByVal BegBal As Decimal,
                             ByVal EndBal As Decimal,
                             ByVal adb As Decimal,
                             ByVal Interest As Decimal)
        Dim cAccount As String = txtAccount.Text
        Try
            Dim row As String() =
             {empKey, empID, empName, RefNo, cAccount, "", Format(CDec(BegBal), "##,##0.00"), Format(CDec(EndBal), "##,##0.00"), Format(CDec(adb), "##,##0.00"), Format(CDec(Interest), "##,##0.00")}

            Dim nRowIndex As Integer
            With grdComputation

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With

        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ComputeTotals()
        Dim totalbegbal As Decimal
        Dim totalendbal As Decimal
        Dim totaladb As Decimal
        Dim totalinterest As Decimal

        Try
            For Each xRow As DataGridViewRow In grdComputation.Rows
                Dim adb As Object = grdComputation.Item("AVEDAILYBALANCE", xRow.Index).Value
                Dim interest As Object = grdComputation.Item("INTERESTADB", xRow.Index).Value
                Dim begbal As Object = grdComputation.Item("BEGBAL", xRow.Index).Value
                Dim endbal As Object = grdComputation.Item("ENDBAL", xRow.Index).Value

                If adb IsNot Nothing Then
                    totaladb += adb
                End If

                If interest IsNot Nothing Then
                    totalinterest += interest
                End If

                If begbal IsNot Nothing Then
                    totalbegbal += begbal
                End If

                If endbal IsNot Nothing Then
                    totalendbal += endbal
                End If
            Next
        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Interest = totalinterest
        txtTADB.Text = Format(CDec(totaladb), "##,##0.00")
        txtTInterest.Text = Format(CDec(totalinterest), "##,##0.00")
        txtTBegBal.Text = Format(CDec(totalbegbal), "##,##0.00")
        txtTEndBal.Text = Format(CDec(totalendbal), "##,##0.00")
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnJvNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJvNo.Click
        frmJVno.ShowDialog()
        If frmJVno.DialogResult = DialogResult.OK Then
            txtJVNo.Text = frmJVno.listDocNumber.SelectedItems.Item(0).Text
        End If
    End Sub

    Private Sub btnPostInterest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPostInterest.Click
        If txtDebitAccount.Text = "" Then
            MsgBox("Please fill up all posting parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        ElseIf txtCreditAccount.Text = "" Then
            MsgBox("Please fill up all posting parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        ElseIf txtJVNo.Text = "" Then
            MsgBox("Please fill up all posting parameters", MsgBoxStyle.Exclamation, "Savings Interest Computation")
            Exit Sub
        Else
            entryID = System.Guid.NewGuid.ToString()
            Call saveEntryHeader(entryID)
            If bgwPosting.IsBusy = False Then
                pbStatus.Visible = True
                bgwPosting.WorkerReportsProgress = True
                bgwPosting.RunWorkerAsync()
            Else
                bgwPosting.CancelAsync()
                pbStatus.Visible = True
                bgwPosting.WorkerReportsProgress = True
                bgwPosting.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function

    Private Sub saveEntryHeader(ByVal guid As String)
        Dim user As String = frmMain.currentUser.Text
        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dtPostingDate.Value.Date, "JV")
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", gCompanyID()),
                    New SqlParameter("@fdTransDate", dtPostingDate.Value),
                    New SqlParameter("@fdTotAmt", Interest),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", "JV"),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", txtJVNo.Text),
                    New SqlParameter("@fbIsCancelled", 0),
                    New SqlParameter("@fbIsPosted", 1),
                    New SqlParameter("@fdDatePrepared", dtPostingDate.Value),
                    New SqlParameter("@SoaNo", ""))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub saveDetails(ByVal guid As String)
        Try
            For xRow As Integer = 0 To grdComputation.RowCount - 1
                pRowCount = grdComputation.RowCount
                Dim journalNo As String = txtJVNo.Text
                Dim sAccount As String = KeyAccountCredit
                Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
                Dim dDebit As Decimal = 0
                Dim dCredit As Decimal = grdComputation.Item("INTERESTADB", xRow).Value
                Dim sMemo As String = ""
                Dim sNameID As String = grdComputation.Item("KEYMEMBER", xRow).Value
                Dim pkJournalID As String = guid
                Dim dtTransact As Date = dtPostingDate.Value.Date
                Dim LoanRef As String = Nothing
                Dim AccountRef As String = frmGeneralJournalEntries.NormalizeValuesInDataGridView(grdComputation, "REFERENCE", xRow)

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save", _
                                  New SqlParameter("@fiEntryNo", journalNo), _
                                  New SqlParameter("@fxKeyAccount", sAccount), _
                                  New SqlParameter("@fdDebit", dDebit), _
                                  New SqlParameter("@fdCredit", dCredit), _
                                  New SqlParameter("@fcMemo", sMemo), _
                                  New SqlParameter("@fxKeyNameID", sNameID), _
                                  New SqlParameter("@fxKey", jvDetailRecordID), _
                                  New SqlParameter("@fk_JVHeader", guid), _
                                  New SqlParameter("@transDate", dtTransact), _
                                  New SqlParameter("@fcLoanRef", LoanRef), _
                                  New SqlParameter("@fcAccRef", AccountRef),
                                  New SqlParameter("@SoaNo", ""))
                gCon.sqlconn.Close()
                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPosting.ReportProgress(percent)
            Next
            Call saveDebitEntry(guid)
            Call PrepareSubsidiaryEntry()
            Call UpdateDocDate()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub saveDebitEntry(ByVal guid As String)
        Dim journalNo As String = txtJVNo.Text
        Dim sAccount As String = KeyAccountDebit
        Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
        'Dim dDebit As Decimal = CDec(Val(txtTInterest.Text))
        Dim dCredit As Decimal = 0
        Dim sMemo As String = ""
        Dim pkJournalID As String = Guid
        Dim dtTransact As Date = dtPostingDate.Value.Date
        Dim LoanRef As String = Nothing
        Dim AccountRef As String = Nothing
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                      New SqlParameter("@fiEntryNo", journalNo), _
                                      New SqlParameter("@fxKeyAccount", sAccount), _
                                      New SqlParameter("@fdDebit", Interest), _
                                      New SqlParameter("@fdCredit", dCredit), _
                                      New SqlParameter("@fcMemo", sMemo), _
                                      New SqlParameter("@fxKey", jvDetailRecordID), _
                                      New SqlParameter("@fk_JVHeader", pkJournalID), _
                                      New SqlParameter("@transDate", dtTransact), _
                                      New SqlParameter("@fcLoanRef", LoanRef), _
                                      New SqlParameter("@fcAccRef", AccountRef))
        gCon.sqlconn.Close()
    End Sub

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtJVNo.Text),
                    New SqlParameter("@fcDoctype", "JOURNAL VOUCHER"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception

        End Try
    End Sub


    Private Sub PrepareSubsidiaryEntry()
        For xRow As Integer = 0 To grdComputation.RowCount - 1
            Dim journalNo As String = txtJVNo.Text
            Dim dCredit As Decimal = grdComputation.Item("INTERESTADB", xRow).Value
            Dim dtTransact As Date = dtPostingDate.Value.Date
            Dim AccountRef As String = frmGeneralJournalEntries.NormalizeValuesInDataGridView(grdComputation, "REFERENCE", xRow)

            Call UpdateSubsidiary(AccountRef, journalNo, 0, dCredit, "", dtTransact, "Cash", 1)
        Next
    End Sub

    Private Sub UpdateSubsidiary(ByVal AccountRef As String, ByVal RefNo As String, ByVal debit As Decimal, ByVal credit As Decimal, ByVal Memo As String, ByVal fdDate As Date, ByVal PayMethod As String, ByVal cStatus As Boolean)
        Dim myID As New Guid(gCompanyID)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_Update", _
                                      New SqlParameter("@AccountRef", AccountRef), _
                                      New SqlParameter("@RefNo", RefNo), _
                                      New SqlParameter("@Debit", debit), _
                                      New SqlParameter("@Credit", credit), _
                                      New SqlParameter("@Description", Memo), _
                                      New SqlParameter("@fdDate", fdDate), _
                                      New SqlParameter("@PaymentMethod", PayMethod), _
                                      New SqlParameter("@CheckStatus", cStatus),
                                      New SqlParameter("@coid", myID))
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Call ComputeInterest()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        picLoading.Visible = False
    End Sub

    Private Sub DeleteTemp()
        Dim sSQLCmd As String = "DELETE FROM tbl_TempInterest"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

        Dim sSQLCmd2 As String = "DBCC CHECKIDENT (tbl_TempInterest, RESEED, 0)"
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd2)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        DeleteTemp()
        If bgwPrint.IsBusy = False Then
            pbStatus.Visible = True
            bgwPrint.WorkerReportsProgress = True
            bgwPrint.RunWorkerAsync()
        Else
            bgwPrint.CancelAsync()
            pbStatus.Visible = True
            bgwPrint.WorkerReportsProgress = True
            bgwPrint.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwPrint_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwPrint.DoWork
        InsertToTemp()
    End Sub

    Private Sub bgwPrint_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPrint.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Loading... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub bgwPrint_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPrint.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""
        pCallCompany(frmInterestReport)
    End Sub

    Private Sub InsertToTemp()
        Try
            For xRow As Integer = 0 To grdComputation.RowCount - 1
                pRowCount = grdComputation.RowCount
                Int_memID = GetValuesInDataGridView(grdComputation, "CID", xRow)
                Int_memName = GetValuesInDataGridView(grdComputation, "cNAME", xRow)
                Int_refNo = GetValuesInDataGridView(grdComputation, "REFERENCE", xRow)
                Int_acntTitle = GetValuesInDataGridView(grdComputation, "TITLE", xRow)
                Int_interest = GetValuesInDataGridView(grdComputation, "INTERESTADB", xRow)

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_InterestComputation_InserttoReport", _
                                  New SqlParameter("@memID", Int_memID),
                                  New SqlParameter("@memName", Int_memName),
                                  New SqlParameter("@refNo", Int_refNo),
                                  New SqlParameter("@acntTitle", Int_acntTitle),
                                  New SqlParameter("@fnInterest", Int_interest),
                                  New SqlParameter("@startingMonth", dtStartingMonth.Value),
                                  New SqlParameter("@periodtocompute", Int_PeriodToCompute),
                                  New SqlParameter("@coName", gCompanyName))
                gCon.sqlconn.Close()
                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPrint.ReportProgress(percent)
            Next
        Catch ex As Exception
            MsgBox("Error on printing report." & vbCr & vbCr & "Private Sub InsertToTemp", MsgBoxStyle.Exclamation, "Dividend Computation")
            Exit Sub
        End Try
    End Sub
    
    Private Sub frmSavingsInterestComputation_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
    End Sub

    'Private Sub LoadReport()
    '    RptVwer.SetDisplayMode(DisplayMode.PrintLayout)
    '    RptVwer.ZoomMode = ZoomMode.Percent
    '    RptVwer.ZoomPercent = 100

    '    RptVwer.Reset()
    '    RptVwer.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
    '    RptVwer.LocalReport.ReportPath = Environment.CurrentDirectory + "\Accounting Reports\SSRS Reports\InterestComputationReport.rdl"

    '    'Dim paramList As New Generic.List(Of ReportParameter)

    '    'paramList.Add(New ReportParameter("startDate", startDate, False))
    '    'paramList.Add(New ReportParameter("ENDDate", endDate, False))
    '    'paramList.Add(New ReportParameter("Rate", Int_Rate, False))
    '    'paramList.Add(New ReportParameter("acntName", Int_AccountName, False))
    '    'paramList.Add(New ReportParameter("minimumBal", Int_minimum, False))
    '    'RptVwer.LocalReport.SetParameters(paramList)

    '    Dim dsSummary As DataSet = PrintDividend()
    '    Dim rDs As ReportDataSource = New ReportDataSource("DataSet1", dsSummary.Tables(0))
    '    RptVwer.LocalReport.DataSources.Clear()
    '    RptVwer.LocalReport.DataSources.Add(rDs)
    '    RptVwer.ServerReport.Refresh()
    '    RptVwer.RefreshReport()
    'End Sub

    Private Sub bgwPosting_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwPosting.DoWork
        Call saveDetails(entryID)
    End Sub

    Private Sub bgwPosting_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPosting.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Loading... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub bgwPosting_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPosting.RunWorkerCompleted
        MsgBox("Posting Successful", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Savings Interest Computation")
        AuditTrail_Save("Computations", "Savings Interest > Post Interest > Debit Account: " & txtDebitAccount.Text & ", Credit Account: " & txtCreditAccount.Text & ", Posting Date: " & dtPostingDate.Text & ", JV Number: " & txtJVNo.Text)
        pbStatus.Visible = False
        lblStatus.Text = ""
    End Sub
End Class