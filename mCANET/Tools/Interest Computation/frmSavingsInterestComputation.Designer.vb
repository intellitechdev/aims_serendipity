﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSavingsInterestComputation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSavingsInterestComputation))
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdComputation = New System.Windows.Forms.DataGridView()
        Me.txtTEndBal = New System.Windows.Forms.TextBox()
        Me.txtTADB = New System.Windows.Forms.TextBox()
        Me.txtTInterest = New System.Windows.Forms.TextBox()
        Me.lblTotals = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtInterestPerAnnum = New System.Windows.Forms.TextBox()
        Me.txtMinBal = New System.Windows.Forms.TextBox()
        Me.dtStartingMonth = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDebitAccount = New System.Windows.Forms.TextBox()
        Me.txtCreditAccount = New System.Windows.Forms.TextBox()
        Me.dtPostingDate = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJVNo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnPostInterest = New System.Windows.Forms.Button()
        Me.btnProcessInterest = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.txtAccount = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboPeriodToCompute = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAccount = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnJvNo = New System.Windows.Forms.Button()
        Me.btnDebitAccount = New System.Windows.Forms.Button()
        Me.btnCreditAccount = New System.Windows.Forms.Button()
        Me.txtTBegBal = New System.Windows.Forms.TextBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.pbStatus = New System.Windows.Forms.ProgressBar()
        Me.bgwPrint = New System.ComponentModel.BackgroundWorker()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KEYMEMBER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REFERENCE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TITLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DORMANT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BEGBAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ENDBAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AVEDAILYBALANCE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INTERESTADB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bgwPosting = New System.ComponentModel.BackgroundWorker()
        CType(Me.grdComputation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdComputation
        '
        Me.grdComputation.AllowUserToAddRows = False
        Me.grdComputation.AllowUserToDeleteRows = False
        Me.grdComputation.AllowUserToResizeColumns = False
        Me.grdComputation.AllowUserToResizeRows = False
        Me.grdComputation.BackgroundColor = System.Drawing.Color.White
        Me.grdComputation.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdComputation.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grdComputation.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdComputation.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.grdComputation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdComputation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.KEYMEMBER, Me.CID, Me.cNAME, Me.REFERENCE, Me.TITLE, Me.DORMANT, Me.BEGBAL, Me.ENDBAL, Me.AVEDAILYBALANCE, Me.INTERESTADB})
        Me.grdComputation.Location = New System.Drawing.Point(12, 12)
        Me.grdComputation.Name = "grdComputation"
        Me.grdComputation.ReadOnly = True
        Me.grdComputation.RowHeadersVisible = False
        Me.grdComputation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grdComputation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.grdComputation.Size = New System.Drawing.Size(964, 268)
        Me.grdComputation.TabIndex = 0
        '
        'txtTEndBal
        '
        Me.txtTEndBal.BackColor = System.Drawing.Color.White
        Me.txtTEndBal.Location = New System.Drawing.Point(731, 286)
        Me.txtTEndBal.Name = "txtTEndBal"
        Me.txtTEndBal.ReadOnly = True
        Me.txtTEndBal.Size = New System.Drawing.Size(82, 20)
        Me.txtTEndBal.TabIndex = 3
        Me.txtTEndBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTADB
        '
        Me.txtTADB.BackColor = System.Drawing.Color.White
        Me.txtTADB.Location = New System.Drawing.Point(811, 286)
        Me.txtTADB.Name = "txtTADB"
        Me.txtTADB.ReadOnly = True
        Me.txtTADB.Size = New System.Drawing.Size(82, 20)
        Me.txtTADB.TabIndex = 4
        Me.txtTADB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTInterest
        '
        Me.txtTInterest.BackColor = System.Drawing.Color.White
        Me.txtTInterest.Location = New System.Drawing.Point(891, 286)
        Me.txtTInterest.Name = "txtTInterest"
        Me.txtTInterest.ReadOnly = True
        Me.txtTInterest.Size = New System.Drawing.Size(82, 20)
        Me.txtTInterest.TabIndex = 5
        Me.txtTInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotals
        '
        Me.lblTotals.AutoSize = True
        Me.lblTotals.Location = New System.Drawing.Point(599, 291)
        Me.lblTotals.Name = "lblTotals"
        Me.lblTotals.Size = New System.Drawing.Size(49, 13)
        Me.lblTotals.TabIndex = 6
        Me.lblTotals.Text = "Totals:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(163, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Minimum Balance to Compute"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(349, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(157, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Starting Month to Compute"
        '
        'txtInterestPerAnnum
        '
        Me.txtInterestPerAnnum.Location = New System.Drawing.Point(141, 50)
        Me.txtInterestPerAnnum.MaxLength = 5
        Me.txtInterestPerAnnum.Name = "txtInterestPerAnnum"
        Me.txtInterestPerAnnum.Size = New System.Drawing.Size(44, 20)
        Me.txtInterestPerAnnum.TabIndex = 10
        Me.txtInterestPerAnnum.Text = "5"
        '
        'txtMinBal
        '
        Me.txtMinBal.Location = New System.Drawing.Point(189, 76)
        Me.txtMinBal.Name = "txtMinBal"
        Me.txtMinBal.Size = New System.Drawing.Size(70, 20)
        Me.txtMinBal.TabIndex = 11
        Me.txtMinBal.Text = "500"
        '
        'dtStartingMonth
        '
        Me.dtStartingMonth.CustomFormat = "MMMM yyyy"
        Me.dtStartingMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtStartingMonth.Location = New System.Drawing.Point(512, 23)
        Me.dtStartingMonth.Name = "dtStartingMonth"
        Me.dtStartingMonth.Size = New System.Drawing.Size(128, 20)
        Me.dtStartingMonth.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Interest Per Annum"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(188, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(13, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "%"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(391, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(115, 13)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Period to Compute:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(27, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 13)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Debit"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 63)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Credit"
        '
        'txtDebitAccount
        '
        Me.txtDebitAccount.BackColor = System.Drawing.Color.White
        Me.txtDebitAccount.Location = New System.Drawing.Point(70, 32)
        Me.txtDebitAccount.Name = "txtDebitAccount"
        Me.txtDebitAccount.ReadOnly = True
        Me.txtDebitAccount.Size = New System.Drawing.Size(201, 20)
        Me.txtDebitAccount.TabIndex = 24
        '
        'txtCreditAccount
        '
        Me.txtCreditAccount.BackColor = System.Drawing.Color.White
        Me.txtCreditAccount.Location = New System.Drawing.Point(70, 60)
        Me.txtCreditAccount.Name = "txtCreditAccount"
        Me.txtCreditAccount.ReadOnly = True
        Me.txtCreditAccount.Size = New System.Drawing.Size(201, 20)
        Me.txtCreditAccount.TabIndex = 25
        '
        'dtPostingDate
        '
        Me.dtPostingDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPostingDate.Location = New System.Drawing.Point(441, 29)
        Me.dtPostingDate.Name = "dtPostingDate"
        Me.dtPostingDate.Size = New System.Drawing.Size(147, 20)
        Me.dtPostingDate.TabIndex = 28
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(356, 33)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Posting Date"
        '
        'txtJVNo
        '
        Me.txtJVNo.BackColor = System.Drawing.Color.White
        Me.txtJVNo.Location = New System.Drawing.Point(441, 60)
        Me.txtJVNo.Name = "txtJVNo"
        Me.txtJVNo.ReadOnly = True
        Me.txtJVNo.Size = New System.Drawing.Size(147, 20)
        Me.txtJVNo.TabIndex = 31
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(374, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 13)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "JV Number"
        '
        'btnPostInterest
        '
        Me.btnPostInterest.Location = New System.Drawing.Point(799, 33)
        Me.btnPostInterest.Name = "btnPostInterest"
        Me.btnPostInterest.Size = New System.Drawing.Size(117, 23)
        Me.btnPostInterest.TabIndex = 33
        Me.btnPostInterest.Text = "Post Interest"
        Me.btnPostInterest.UseVisualStyleBackColor = True
        '
        'btnProcessInterest
        '
        Me.btnProcessInterest.Location = New System.Drawing.Point(799, 27)
        Me.btnProcessInterest.Name = "btnProcessInterest"
        Me.btnProcessInterest.Size = New System.Drawing.Size(117, 23)
        Me.btnProcessInterest.TabIndex = 34
        Me.btnProcessInterest.Text = "Process Interest"
        Me.btnProcessInterest.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(799, 61)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(117, 23)
        Me.btnClose.TabIndex = 35
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtAccount
        '
        Me.txtAccount.BackColor = System.Drawing.Color.White
        Me.txtAccount.Location = New System.Drawing.Point(71, 24)
        Me.txtAccount.Name = "txtAccount"
        Me.txtAccount.ReadOnly = True
        Me.txtAccount.Size = New System.Drawing.Size(201, 20)
        Me.txtAccount.TabIndex = 38
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(20, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(49, 13)
        Me.Label12.TabIndex = 37
        Me.Label12.Text = "Account"
        '
        'cboPeriodToCompute
        '
        Me.cboPeriodToCompute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodToCompute.FormattingEnabled = True
        Me.cboPeriodToCompute.Items.AddRange(New Object() {"Monthly", "Quarterly", "6 Months"})
        Me.cboPeriodToCompute.Location = New System.Drawing.Point(512, 49)
        Me.cboPeriodToCompute.Name = "cboPeriodToCompute"
        Me.cboPeriodToCompute.Size = New System.Drawing.Size(128, 21)
        Me.cboPeriodToCompute.TabIndex = 40
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(799, 56)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(117, 23)
        Me.Button2.TabIndex = 41
        Me.Button2.Text = "Print"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtAccount)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboPeriodToCompute)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnAccount)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtInterestPerAnnum)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtMinBal)
        Me.GroupBox1.Controls.Add(Me.dtStartingMonth)
        Me.GroupBox1.Controls.Add(Me.btnProcessInterest)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(27, 309)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(949, 107)
        Me.GroupBox1.TabIndex = 42
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Computation Parameters"
        '
        'btnAccount
        '
        Me.btnAccount.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnAccount.Location = New System.Drawing.Point(276, 21)
        Me.btnAccount.Name = "btnAccount"
        Me.btnAccount.Size = New System.Drawing.Size(26, 27)
        Me.btnAccount.TabIndex = 39
        Me.btnAccount.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtCreditAccount)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.btnClose)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.btnPostInterest)
        Me.GroupBox2.Controls.Add(Me.txtDebitAccount)
        Me.GroupBox2.Controls.Add(Me.btnJvNo)
        Me.GroupBox2.Controls.Add(Me.btnDebitAccount)
        Me.GroupBox2.Controls.Add(Me.txtJVNo)
        Me.GroupBox2.Controls.Add(Me.btnCreditAccount)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.dtPostingDate)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Location = New System.Drawing.Point(27, 441)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(947, 98)
        Me.GroupBox2.TabIndex = 43
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Posting Parameters"
        '
        'btnJvNo
        '
        Me.btnJvNo.Image = CType(resources.GetObject("btnJvNo.Image"), System.Drawing.Image)
        Me.btnJvNo.Location = New System.Drawing.Point(594, 56)
        Me.btnJvNo.Name = "btnJvNo"
        Me.btnJvNo.Size = New System.Drawing.Size(26, 27)
        Me.btnJvNo.TabIndex = 32
        Me.btnJvNo.UseVisualStyleBackColor = True
        '
        'btnDebitAccount
        '
        Me.btnDebitAccount.Image = CType(resources.GetObject("btnDebitAccount.Image"), System.Drawing.Image)
        Me.btnDebitAccount.Location = New System.Drawing.Point(276, 29)
        Me.btnDebitAccount.Name = "btnDebitAccount"
        Me.btnDebitAccount.Size = New System.Drawing.Size(26, 27)
        Me.btnDebitAccount.TabIndex = 26
        Me.btnDebitAccount.UseVisualStyleBackColor = True
        '
        'btnCreditAccount
        '
        Me.btnCreditAccount.Image = CType(resources.GetObject("btnCreditAccount.Image"), System.Drawing.Image)
        Me.btnCreditAccount.Location = New System.Drawing.Point(276, 57)
        Me.btnCreditAccount.Name = "btnCreditAccount"
        Me.btnCreditAccount.Size = New System.Drawing.Size(26, 27)
        Me.btnCreditAccount.TabIndex = 27
        Me.btnCreditAccount.UseVisualStyleBackColor = True
        '
        'txtTBegBal
        '
        Me.txtTBegBal.BackColor = System.Drawing.Color.White
        Me.txtTBegBal.Location = New System.Drawing.Point(650, 286)
        Me.txtTBegBal.Name = "txtTBegBal"
        Me.txtTBegBal.ReadOnly = True
        Me.txtTBegBal.Size = New System.Drawing.Size(82, 20)
        Me.txtTBegBal.TabIndex = 44
        Me.txtTBegBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.BackColor = System.Drawing.Color.White
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(431, 119)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(124, 95)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 42
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'pbStatus
        '
        Me.pbStatus.Location = New System.Drawing.Point(12, 286)
        Me.pbStatus.Name = "pbStatus"
        Me.pbStatus.Size = New System.Drawing.Size(195, 13)
        Me.pbStatus.TabIndex = 45
        Me.pbStatus.Visible = False
        '
        'bgwPrint
        '
        Me.bgwPrint.WorkerReportsProgress = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(213, 286)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 46
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "NAME"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "REFERENCE"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "TITLE"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 120
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "DORMANT"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle26
        Me.DataGridViewTextBoxColumn6.HeaderText = "END BAL"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 80
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridViewTextBoxColumn7.HeaderText = "AVERAGE DAILY BALANCE"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle28
        Me.DataGridViewTextBoxColumn8.HeaderText = "LOWEST BALANCE"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 80
        '
        'DataGridViewTextBoxColumn9
        '
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle29
        Me.DataGridViewTextBoxColumn9.HeaderText = "INTEREST ADB"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 80
        '
        'DataGridViewTextBoxColumn10
        '
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle30
        Me.DataGridViewTextBoxColumn10.HeaderText = "INTEREST LOWEST"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 80
        '
        'KEYMEMBER
        '
        Me.KEYMEMBER.HeaderText = "KEYMEMBER"
        Me.KEYMEMBER.Name = "KEYMEMBER"
        Me.KEYMEMBER.ReadOnly = True
        Me.KEYMEMBER.Visible = False
        '
        'CID
        '
        Me.CID.HeaderText = "ID"
        Me.CID.Name = "CID"
        Me.CID.ReadOnly = True
        Me.CID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.CID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CID.Width = 90
        '
        'cNAME
        '
        Me.cNAME.HeaderText = "NAME"
        Me.cNAME.Name = "cNAME"
        Me.cNAME.ReadOnly = True
        Me.cNAME.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.cNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cNAME.Width = 220
        '
        'REFERENCE
        '
        Me.REFERENCE.HeaderText = "REFERENCE"
        Me.REFERENCE.Name = "REFERENCE"
        Me.REFERENCE.ReadOnly = True
        Me.REFERENCE.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.REFERENCE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.REFERENCE.Width = 80
        '
        'TITLE
        '
        Me.TITLE.HeaderText = "TITLE"
        Me.TITLE.Name = "TITLE"
        Me.TITLE.ReadOnly = True
        Me.TITLE.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TITLE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TITLE.Width = 200
        '
        'DORMANT
        '
        Me.DORMANT.HeaderText = "DORMANT"
        Me.DORMANT.Name = "DORMANT"
        Me.DORMANT.ReadOnly = True
        Me.DORMANT.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DORMANT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DORMANT.Width = 50
        '
        'BEGBAL
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.BEGBAL.DefaultCellStyle = DataGridViewCellStyle22
        Me.BEGBAL.HeaderText = "BEGINNING BALANCE"
        Me.BEGBAL.Name = "BEGBAL"
        Me.BEGBAL.ReadOnly = True
        Me.BEGBAL.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.BEGBAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.BEGBAL.Width = 80
        '
        'ENDBAL
        '
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.ENDBAL.DefaultCellStyle = DataGridViewCellStyle23
        Me.ENDBAL.HeaderText = "END BALANCE"
        Me.ENDBAL.Name = "ENDBAL"
        Me.ENDBAL.ReadOnly = True
        Me.ENDBAL.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ENDBAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ENDBAL.Width = 80
        '
        'AVEDAILYBALANCE
        '
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.AVEDAILYBALANCE.DefaultCellStyle = DataGridViewCellStyle24
        Me.AVEDAILYBALANCE.HeaderText = "AVERAGE DAILY BALANCE"
        Me.AVEDAILYBALANCE.Name = "AVEDAILYBALANCE"
        Me.AVEDAILYBALANCE.ReadOnly = True
        Me.AVEDAILYBALANCE.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.AVEDAILYBALANCE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AVEDAILYBALANCE.Width = 80
        '
        'INTERESTADB
        '
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.INTERESTADB.DefaultCellStyle = DataGridViewCellStyle25
        Me.INTERESTADB.HeaderText = "INTEREST ADB"
        Me.INTERESTADB.Name = "INTERESTADB"
        Me.INTERESTADB.ReadOnly = True
        Me.INTERESTADB.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.INTERESTADB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.INTERESTADB.Width = 80
        '
        'bgwPosting
        '
        Me.bgwPosting.WorkerReportsProgress = True
        '
        'frmSavingsInterestComputation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(986, 559)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.pbStatus)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.txtTBegBal)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblTotals)
        Me.Controls.Add(Me.txtTInterest)
        Me.Controls.Add(Me.txtTADB)
        Me.Controls.Add(Me.txtTEndBal)
        Me.Controls.Add(Me.grdComputation)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmSavingsInterestComputation"
        Me.Text = "Savings Interest Computation"
        CType(Me.grdComputation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdComputation As System.Windows.Forms.DataGridView
    Friend WithEvents txtTEndBal As System.Windows.Forms.TextBox
    Friend WithEvents txtTADB As System.Windows.Forms.TextBox
    Friend WithEvents txtTInterest As System.Windows.Forms.TextBox
    Friend WithEvents lblTotals As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtInterestPerAnnum As System.Windows.Forms.TextBox
    Friend WithEvents txtMinBal As System.Windows.Forms.TextBox
    Friend WithEvents dtStartingMonth As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtDebitAccount As System.Windows.Forms.TextBox
    Friend WithEvents txtCreditAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnDebitAccount As System.Windows.Forms.Button
    Friend WithEvents btnCreditAccount As System.Windows.Forms.Button
    Friend WithEvents dtPostingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnJvNo As System.Windows.Forms.Button
    Friend WithEvents txtJVNo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPostInterest As System.Windows.Forms.Button
    Friend WithEvents btnProcessInterest As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnAccount As System.Windows.Forms.Button
    Friend WithEvents txtAccount As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboPeriodToCompute As System.Windows.Forms.ComboBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTBegBal As System.Windows.Forms.TextBox
    Friend WithEvents KEYMEMBER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cNAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REFERENCE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TITLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DORMANT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BEGBAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ENDBAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AVEDAILYBALANCE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INTERESTADB As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents pbStatus As System.Windows.Forms.ProgressBar
    Friend WithEvents bgwPrint As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents bgwPosting As System.ComponentModel.BackgroundWorker
End Class
