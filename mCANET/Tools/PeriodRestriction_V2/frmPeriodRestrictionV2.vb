﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmPeriodRestrictionV2
    Private gCon As New Clsappconfiguration()
    Dim pkPeriod As Integer
    Private Sub frmPeriodRestrictionV2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadDetails()
        DisableRestriction()
    End Sub

    Private Sub DisableRestriction()
        If chkEditRestriction.Checked = True Then
            nudDays.Enabled = False
        Else
            nudDays.Enabled = True
        End If
    End Sub

    Private Sub LoadDetails()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_PeriodRestriction_LoadDetails",
                                      New SqlParameter("@coid", gCompanyID()))
        While rd.Read
            pkPeriod = rd.Item("PK_PeriodRestriction")
            dtpFrom.Value = rd.Item("fdPfrom")
            dtpTo.Value = rd.Item("fdPto")
            dtpForward.Value = rd.Item("fdForwardDate")
            chkEditRestriction.Checked = rd.Item("fbIsRestricted")
            nudDays.Value = rd.Item("fnDays")
        End While
    End Sub

    Private Sub UpdatePeriodRestrictions()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_PeriodRestriction_UpdateDetails", _
                                      New SqlParameter("@pkPeriod", pkPeriod), _
                                      New SqlParameter("@fdPfrom", dtpFrom.Value), _
                                      New SqlParameter("@fdPto", dtpTo.Value), _
                                      New SqlParameter("@fdForwardDate", dtpForward.Value), _
                                      New SqlParameter("@fbIsRestricted", chkEditRestriction.CheckState), _
                                      New SqlParameter("@fnDays", nudDays.Value))
            MsgBox("Updated Successfully", MsgBoxStyle.Information, "Period Restriction")
        Catch ex As Exception
            MsgBox("Updating failed.", MsgBoxStyle.Critical, "Period Restriction")
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        UpdatePeriodRestrictions()
    End Sub

    Private Sub chkEditRestriction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEditRestriction.Click
        DisableRestriction()
    End Sub
End Class