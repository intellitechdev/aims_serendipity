<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmdataTransfer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGVdata = New System.Windows.Forms.DataGridView
        Me.BtnCopyData = New System.Windows.Forms.Button
        Me.BrowseDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ChartOfAccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.ToolStripCboTargetCompany = New System.Windows.Forms.ToolStripComboBox
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblSource = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblTarget = New System.Windows.Forms.ToolStripStatusLabel
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.DgAccountData = New System.Windows.Forms.DataGridView
        CType(Me.DGVdata, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.DgAccountData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVdata
        '
        Me.DGVdata.AllowUserToAddRows = False
        Me.DGVdata.AllowUserToDeleteRows = False
        Me.DGVdata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVdata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGVdata.Location = New System.Drawing.Point(0, 25)
        Me.DGVdata.Margin = New System.Windows.Forms.Padding(3, 3, 3, 50)
        Me.DGVdata.Name = "DGVdata"
        Me.DGVdata.Size = New System.Drawing.Size(715, 268)
        Me.DGVdata.TabIndex = 1
        '
        'BtnCopyData
        '
        Me.BtnCopyData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnCopyData.Location = New System.Drawing.Point(622, 1)
        Me.BtnCopyData.Name = "BtnCopyData"
        Me.BtnCopyData.Size = New System.Drawing.Size(89, 23)
        Me.BtnCopyData.TabIndex = 3
        Me.BtnCopyData.Text = "Copy Data"
        Me.BtnCopyData.UseVisualStyleBackColor = True
        '
        'BrowseDataToolStripMenuItem
        '
        Me.BrowseDataToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChartOfAccountsToolStripMenuItem})
        Me.BrowseDataToolStripMenuItem.Name = "BrowseDataToolStripMenuItem"
        Me.BrowseDataToolStripMenuItem.Size = New System.Drawing.Size(90, 21)
        Me.BrowseDataToolStripMenuItem.Text = "Browse Source"
        '
        'ChartOfAccountsToolStripMenuItem
        '
        Me.ChartOfAccountsToolStripMenuItem.Name = "ChartOfAccountsToolStripMenuItem"
        Me.ChartOfAccountsToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ChartOfAccountsToolStripMenuItem.Text = "Chart of Accounts"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BrowseDataToolStripMenuItem, Me.ToolStripCboTargetCompany})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(715, 25)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripCboTargetCompany
        '
        Me.ToolStripCboTargetCompany.Name = "ToolStripCboTargetCompany"
        Me.ToolStripCboTargetCompany.Size = New System.Drawing.Size(121, 21)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(44, 17)
        Me.ToolStripStatusLabel1.Text = "Source:"
        '
        'lblSource
        '
        Me.lblSource.BackColor = System.Drawing.Color.White
        Me.lblSource.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.lblSource.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.lblSource.Name = "lblSource"
        Me.lblSource.Size = New System.Drawing.Size(36, 17)
        Me.lblSource.Text = "None"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(23, 17)
        Me.ToolStripStatusLabel3.Text = "    |"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(46, 17)
        Me.ToolStripStatusLabel2.Text = "Target: "
        '
        'lblTarget
        '
        Me.lblTarget.BackColor = System.Drawing.Color.White
        Me.lblTarget.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.lblTarget.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.lblTarget.Name = "lblTarget"
        Me.lblTarget.Size = New System.Drawing.Size(36, 17)
        Me.lblTarget.Text = "None"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.lblSource, Me.ToolStripStatusLabel3, Me.ToolStripStatusLabel2, Me.lblTarget})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 293)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(715, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'DgAccountData
        '
        Me.DgAccountData.AllowUserToAddRows = False
        Me.DgAccountData.AllowUserToDeleteRows = False
        Me.DgAccountData.AllowUserToOrderColumns = True
        Me.DgAccountData.AllowUserToResizeColumns = False
        Me.DgAccountData.AllowUserToResizeRows = False
        Me.DgAccountData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgAccountData.Location = New System.Drawing.Point(12, 179)
        Me.DgAccountData.Name = "DgAccountData"
        Me.DgAccountData.Size = New System.Drawing.Size(693, 84)
        Me.DgAccountData.TabIndex = 6
        '
        'frmdataTransfer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 315)
        Me.Controls.Add(Me.DgAccountData)
        Me.Controls.Add(Me.DGVdata)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.BtnCopyData)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmdataTransfer"
        Me.Text = "frmdataTransfer"
        CType(Me.DGVdata, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.DgAccountData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVdata As System.Windows.Forms.DataGridView
    Friend WithEvents BtnCopyData As System.Windows.Forms.Button
    Friend WithEvents BrowseDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChartOfAccountsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripCboTargetCompany As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblSource As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTarget As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents DgAccountData As System.Windows.Forms.DataGridView
End Class
