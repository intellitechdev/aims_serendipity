Option Explicit On
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmdataTransfer
#Region "Variable Declaration"
    Public DefinedTable As String
    Public DefinedCompanyID As String
    Dim CopyMode As String

    Public xacnt_id As String
    Public xco_id As String
    Public xacnt_type As String
    Public xacnt_name As String
    Public xfbActive As String
    Public xacnt_sub As String
    Public xacnt_subof As String
    Public xacnt_desc As String
    Public xacnt_note As String
    Public xacnt_code As String
    Public xacnt_deleted As String
    Public xacnt_balance As String
    Public xacnt_balance_less As String
    Public xacnt_baldate As String
    Public xcreated_by As String
    Public xdate_created As String
    Public xupdated_by As String
    Public xdate_updated As String

    'Dim xactType_id As String
    'Dim co_id As String
    Dim ATacnt_type As String
    Dim ATdescription As String
    Dim ATseg_value As String
    Dim ATcreated_by As String
    Dim ATdate_created As String
    Dim ATupdated_by As String
    Dim ATdate_updated As String

#End Region
    Private gCon As New Clsappconfiguration
    Private Function GetCompanyID()
        Dim getCompanyIDCmd As String = "select co_id from dbo.mCompany"
        getCompanyIDCmd &= " where co_name = '" & frmMain.lblCompanyName.Text & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, getCompanyIDCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function getData(ByVal defTable As String) As DataSet
        Dim sSqlCmd As String = "select * from " & defTable
        sSqlCmd &= " where co_id='" & GetCompanyID() & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function getAccountType(ByVal Acnt_type_ID As String) As DataSet
        Dim sSqlCmd As String = "select * from dbo.mAccountType"
        sSqlCmd &= " where co_id='" & GetCompanyID() & "' AND actType_id ='" & Acnt_type_ID & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function getTargetCompanyID(ByVal CompanyName As String)
        Dim getCompanyIDCmd As String = "select co_id from dbo.mCompany"
        getCompanyIDCmd &= " where co_name = '" & CompanyName & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, getCompanyIDCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub GetCompanyNames()
        Dim sSqlCompanyName As String = "select co_name from dbo.mCompany"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCompanyName)
                While rd.Read
                    ToolStripCboTargetCompany.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Function getNewID() As String
        Dim sSqlCmd As String = "SELECT newid()"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub copyDataToCOA(ByVal Deftable As String)
        Dim sSqlTransfer As String = "insert into " & Deftable
        sSqlTransfer &= " (acnt_id, co_id, acnt_type, acnt_name, fbActive, acnt_sub, acnt_subof, acnt_desc, acnt_note, acnt_code, acnt_deleted, acnt_balance , acnt_balance_less, acnt_baldate, created_by, date_created, updated_by, date_updated)"
        sSqlTransfer &= " VALUES ('" & xacnt_id & "', '" & xco_id & "', '" & xacnt_type & "', '" _
        & xacnt_name & "', '" & xfbActive & "', '" & xacnt_sub & "', '" & xacnt_subof & "', '" & _
        xacnt_desc & "', '" & xacnt_note & "', '" & xacnt_code & "', '" & xacnt_deleted & "', '" & _
        xacnt_balance & "', '" & xacnt_balance_less & "', '" & xacnt_baldate & "', '" & xcreated_by _
        & "', '" & xdate_created & "', '" & xupdated_by & "', '" & xdate_updated & "')"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlTransfer)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CopyAcntType()
        Dim sSqlCopyAcntType As String = "INSERT INTO dbo.mAccountType"
        sSqlCopyAcntType &= " (actType_id, co_id, acnt_type, description, seg_value, created_by, date_created, updated_by, date_updated)"
        sSqlCopyAcntType &= " VALUES ('" & xacnt_type & ", '" & xco_id & ", '" & ATacnt_type & ", '" & _
        ATdescription & ", '" & ATseg_value & ", '" & ATcreated_by & ", '" & ATdate_created & ", '" & ATupdated_by & ", '" & xdate_updated & "')"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCopyAcntType)
        Catch ex As Exception

        End Try
    End Sub
   

    Private Sub GatheringDataForCOA()
        Dim xRow As Integer = 0
        DGVdata.SelectAll()
        For Each selectedRow As DataGridViewRow In DGVdata.SelectedRows
            'gathering data for chart of account
            xRow = selectedRow.Index
            Dim genAcnt_ID As String = getNewID()
            xacnt_id = genAcnt_ID

            xacnt_type = Convert.ToString(DGVdata.Item(2, xRow).Value)
            xacnt_name = Convert.ToString(DGVdata.Item(3, xRow).Value)
            If DGVdata.Item(4, xRow).Value = True Then
                xfbActive = 1
            Else
                xfbActive = 0
            End If
            xacnt_sub = DGVdata.Item(5, xRow).Value
            If DGVdata.Item(6, xRow).Value Is DBNull.Value Then
                xacnt_subof = "00000000-0000-0000-0000-000000000000"
            Else
                xacnt_subof = Convert.ToString(DGVdata.Item(6, xRow).Value)
            End If
            xacnt_desc = Convert.ToString(DGVdata.Item(7, xRow).Value)
            xacnt_note = Convert.ToString(DGVdata.Item(8, xRow).Value)
            xacnt_code = DGVdata.Item(9, xRow).Value
            If DGVdata.Item(10, xRow).Value = "True" Then
                xacnt_deleted = 1
            Else
                xacnt_deleted = 0
            End If
            xacnt_balance = Convert.ToString(DGVdata.Item(11, xRow).Value)
            xacnt_balance_less = Convert.ToString(DGVdata.Item(12, xRow).Value)
            xacnt_baldate = Convert.ToString(DGVdata.Item(13, xRow).Value)
            xcreated_by = Convert.ToString(DGVdata.Item(14, xRow).Value)
            xdate_created = Convert.ToString(DGVdata.Item(15, xRow).Value)
            xupdated_by = Convert.ToString(DGVdata.Item(16, xRow).Value)
            xdate_updated = Convert.ToString(DGVdata.Item(17, xRow).Value)

            copyDataToCOA("dbo.mAccounts")

            'gathering data for copying the account type
            Dim AccountTypeData As DataSet
            AccountTypeData = getAccountType(xacnt_type)
            With DgAccountData
                .DataSource = AccountTypeData.Tables(0).DefaultView
            End With
            If DgAccountData.RowCount <> 0 Then
                DgAccountData.SelectAll()
                Dim xRowofAcntTpye As Integer = 0
                For Each selectedAccountType As DataGridViewRow In DgAccountData.SelectedRows
                    xRowofAcntTpye = selectedAccountType.Index
                    ATacnt_type = DgAccountData.Item(2, xRowofAcntTpye).Value
                    ATdescription = DgAccountData.Item(3, xRowofAcntTpye).Value
                    ATseg_value = DgAccountData.Item(4, xRowofAcntTpye).Value
                    ATcreated_by = DgAccountData.Item(5, xRowofAcntTpye).Value
                    ATdate_created = DgAccountData.Item(6, xRowofAcntTpye).Value
                    If DgAccountData.Item(7, xRowofAcntTpye).Value Is DBNull.Value Then
                        ATupdated_by = Convert.ToString(DBNull.Value)
                    Else
                        ATupdated_by = DgAccountData.Item(7, xRowofAcntTpye).Value
                    End If
                    If DgAccountData.Item(8, xRowofAcntTpye).Value Is DBNull.Value Then
                        ATdate_updated = Convert.ToString(DBNull.Value)
                    Else
                        ATdate_updated = DgAccountData.Item(8, xRowofAcntTpye).Value
                    End If
                    CopyAcntType()
                Next
            End If
        Next
        DGVdata.Refresh()
    End Sub
    Private Sub ChartOfAccountsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChartOfAccountsToolStripMenuItem.Click
        Dim dataDB As DataSet
        DefinedTable = "dbo.mAccounts"
        lblSource.Text = frmMain.lblCompanyName.Text & " (Chart of Accounts)"
        dataDB = getData(DefinedTable)
        With DGVdata
            .DataSource = dataDB.Tables(0).DefaultView
        End With
    End Sub
    Private Sub frmdataTransfer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolStripCboTargetCompany.Items.Add("Please Select a target company.")
        GetCompanyNames()
        ToolStripCboTargetCompany.SelectedIndex = 0
    End Sub

    Private Sub BtnCopyData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCopyData.Click
        Me.Cursor = Cursors.WaitCursor
        If CopyMode = "Chart of Accounts" Then
            GatheringDataForCOA()
        End If

        CopyMode = "Standby"
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ToolStripCboTargetCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripCboTargetCompany.SelectedIndexChanged
        xco_id = getTargetCompanyID(ToolStripCboTargetCompany.SelectedItem)
        lblTarget.Text = ToolStripCboTargetCompany.SelectedItem & "(Chart of Accounts)"
        CopyMode = "Chart of Accounts"
    End Sub

    Private Sub ToolStripCboTargetCompany_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripCboTargetCompany.TextChanged
        xco_id = getTargetCompanyID(ToolStripCboTargetCompany.SelectedItem)
        lblTarget.Text = ToolStripCboTargetCompany.SelectedItem & "(Chart of Accounts)"
        CopyMode = "Chart of Accounts"
    End Sub
End Class