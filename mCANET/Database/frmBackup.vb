﻿Public Class frmBackup
    Private gFilePath As String = ""
    Private username As String = "Admin"

    Private Sub btnBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        If MsgBox("Are you sure?", vbYesNo, "Backup Database") = vbYes Then
            If txtPath.Text = "" Then
                MsgBox("Please Select Directory for backup.")
            Else
                processWait.Style = ProgressBarStyle.Marquee
                processWait.MarqueeAnimationSpeed = 5
                StartBackup()
            End If
        Else
            Exit Sub
        End If
    End Sub

    Private Function GetFilePath() As String
        Try
            Dim directory As String = ""
            Dim FolderBrowserDialog As New FolderBrowserDialog()
            With FolderBrowserDialog
                .RootFolder = Environment.SpecialFolder.Desktop
                .Description = "Select the folder where you will store your backup."

                If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                    directory = .SelectedPath() + "/"
                End If
            End With

            Return directory
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BackupDatabase()
        Try
            Dim database As New clsDatabaseFunctions()
            database.BackupDatabase("Coop_Accounting_Serendipity", txtPath.Text, Me.username)
            database.BackupDatabase("Coop_Membership_Serendipity", txtPath.Text, Me.username)
            database.BackupDatabase("Payroll_Serendipity", txtPath.Text, Me.username)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub StartBackup()
        Try
            BackupDatabase()
            MessageBox.Show("Backup of your Database is complete!", "Database Backup", MessageBoxButtons.OK, MessageBoxIcon.Information)
            AuditTrail_Save("DATABASE", "Backup Database > Path: " & txtPath.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Database Backup", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Close()
    End Sub

    Private Sub frmBackup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Refresh()
        processWait.Style = ProgressBarStyle.Marquee
        processWait.MarqueeAnimationSpeed = 0
        txtPath.Clear()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        txtPath.Text = GetFilePath()
    End Sub

End Class