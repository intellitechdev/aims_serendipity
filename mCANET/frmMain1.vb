Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmMain

    Private gMFiles As New modMasterFile
    Private gTrans As New clsTransactionFunctions
    Private gcon As New Clsappconfiguration
    Dim xSQLHelper As New NewSQLHelper

#Region "Main Menu"

#Region "File Menu"
    Private Sub Newcompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_file_newcompany.Click
        If gCompanyName <> "" Then
            gCompanyName = ""
            gMFiles.gCompanyEvents_1(frm_MF_Company, frm_MF_Company.cboconame, _
                                 frm_MF_Company.btnSave, frm_MF_Company.btnClear, _
                                 frm_MF_Company.lblCoInfo)
            gTrans.gEnableFormTxt(frm_MF_Company)
            Call NewCompanyFormLoad()
            frm_MF_Company.Size = New Size(542, 521)
            frm_MF_Company.ShowDialog()
        Else
            gCompanyName = ""
            gMFiles.gCompanyEvents_1(frm_MF_Company, frm_MF_Company.cboconame, _
                                 frm_MF_Company.btnSave, frm_MF_Company.btnClear, _
                                 frm_MF_Company.lblCoInfo)
            gTrans.gEnableFormTxt(frm_MF_Company)

            Call NewCompanyFormLoad()
            frm_MF_Company.Size = New Size(542, 521)
            frm_MF_Company.ShowDialog()
        End If
    End Sub
    Private Sub OpenCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_file_openCompany.Click
        gMFiles.gCompanyEvents(frm_MF_Company, frm_MF_Company.cboconame, _
                               frm_MF_Company.btnSave, frm_MF_Company.btnClear, _
                               frm_MF_Company.lblCoInfo)
        gTrans.gDisableFormTxt(frm_MF_Company)
        Call OpenCompanyFormLoad()
        frm_MF_Company.ShowDialog()
    End Sub
    Private Sub CloseCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_file_closecompany.Click
        If gCompanyName <> "" Then
            If MsgBox("You are about to close the current open project, do you still want to proceed?", MsgBoxStyle.YesNo, Me.Text) = MsgBoxResult.Yes Then
                AuditTrail_Save("PROJECTS", "Close project '" & gCompanyName & "'")
                gCompanyName = ""
                lblCompanyName.Text = "Project Name"
                DisableAllAccessibility()
                ClearAllBar()
                EnableOpenCompanyMenu()
            End If
        End If
    End Sub
    Private Sub EnableOpenCompanyMenu()
        ms_file_openCompany.Enabled = True
    End Sub
    Private Sub Logoff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_file_LogOff.Click
        gCompanyName = ""
        gOpenCompany = 0
        lblCompanyName.Text = "Project Name"
        frmSys_UserLogin.ShowDialog()
    End Sub
    Private Sub Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_file_exit.Click
        Me.Close()
    End Sub
#End Region

#Region "Master Files"
    Private Sub ItemMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_itemmaster.Click
        pCallCompany(frm_item_masterItem)
    End Sub
    Private Sub ItemGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_itemGroup.Click
        pCallCompany(frm_item_group)
    End Sub
    Private Sub PriceLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_pricelevel.Click
        pCallCompany(frm_acc_priceLevel)
    End Sub
    Private Sub BillingRate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_billingrate.Click
        pCallCompany(frm_acc_billRateLevel)
    End Sub
    Private Sub SalesTax_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_salestax.Click
        pCallCompany(frm_MF_tax)
    End Sub
    Private Sub SalesTaxCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_salestaxcode.Click
        pCallCompany(frm_vend_SalesTax)
    End Sub
    Private Sub SalesRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_salesrep.Click
        pCallCompany(frm_acc_salesRep)
    End Sub
    Private Sub Class_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClassToolStripMenuItem.Click
        pCallCompany(frmClasses)
    End Sub
    Private Sub CustomerType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_customertype.Click
        pCallCompany(frm_acc_customerType)
    End Sub
    Private Sub VendorType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_vendortype.Click
        pCallCompany(frm_acc_vendorType)
    End Sub
    Private Sub Terms_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_terms.Click
        pCallCompany(frm_acc_terms)
    End Sub
    Private Sub PaymentMethod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_paymentMethod.Click
        pCallCompany(frm_MF_paymentMethod)
    End Sub
    Private Sub Other_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_other.Click
        pCallCompany(frm_acc_otherNameMaster)
    End Sub
    Private Sub ChartOfAccountsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChartOfAccountsToolStripMenuItem.Click
        pCallCompany(frmChartOfAccounts)
    End Sub
#End Region

#Region "Customer"
    Private Sub ms_cust_custmaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_cust_custmaster.Click

    End Sub

    Private Sub ms_cust_createInvoice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_cust_createInvoice.Click

    End Sub

    Private Sub ms_cust_enterSR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_cust_enterSR.Click

    End Sub

    Private Sub ms_cust_recvPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_cust_recvPayment.Click

    End Sub

    Private Sub ms_cust_createCM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_cust_createCM.Click

    End Sub

    Private Sub CreateDebitMemosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateDebitMemosToolStripMenuItem.Click

    End Sub
#End Region

#Region "Accountant"

    Private Sub GeneralJournalEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_accountant_genjourentry.Click
        'pCallCompany(frm_acc_makeGeneralJournalEntry)
        pCallCompany(frmGeneralJournalEntries)
    End Sub
    Private Sub WorkingTrialBalance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_accountant_wrktrialbal.Click
        pCallCompany(frm_acc_workingtrialbalance)
    End Sub
    Private Sub SetClosingDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_accountant_setclosdte.Click
        pCallCompany(frm_acc_setClosingDate)
    End Sub
#End Region

#Region "Banking"
    Private Sub WriteChecks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_banking_writechecks.Click
        pCallCompany(frm_acc_writeChecks_v2)
    End Sub
    Private Sub MakeDeposit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_banking_makedeposit.Click
        pCallCompany(frm_acc_makedeposit)
    End Sub
    Private Sub TransferFunds_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_banking_transferfunds.Click
        pCallCompany(frm_acc_transferfunds)
    End Sub
    Private Sub Reconcile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_banking_reconcile.Click
        pCallCompany(frm_acc_reconcile)
    End Sub
#End Region

#Region "Supplier"
    Private Sub SupplierMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_supmaster.Click
        pCallCompany(frm_vend_masterVendor)
    End Sub
    Private Sub EnterBills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_enterbills.Click
        frm_vend_EnterBills.TabControl1.SelectedIndex = 0
        frm_vend_EnterBills.Text = "Enter Bills"
        frm_vend_EnterBills._SupplierBill = True
        frm_vend_EnterBills._Mode = 0
        pCallCompany(frm_vend_EnterBills)
    End Sub
    Private Sub BillsApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_billsapproval.Click
        pCallCompany(frmBillsListforApproval)
    End Sub
    Private Sub Paybills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_paybills.Click
        frm_vend_PayBills.GetUser() = Me.currentUser.ToString()
        frm_vend_PayBills.ShowDialog()
    End Sub
    Private Sub SaleTax_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_saleTax.Click
        pCallCompany(frm_MF_tax)
    End Sub
    Private Sub PurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_createPO.Click
        frm_vend_CreatePurchaseOrder.KeyPO = Guid.NewGuid.ToString
        frm_vend_CreatePurchaseOrder.IsNew = True
        frm_vend_CreatePurchaseOrder.Text = "Create Purchase Order"
        pCallCompany(frm_vend_CreatePurchaseOrder)
    End Sub
    Private Sub ReceiveItemBill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_rcvItembill.Click
        frm_vend_EnterBills.TabControl1.SelectedIndex = 1
        frm_vend_EnterBills.Text = "Enter Bills"
        frm_vend_EnterBills.chkBillRcv.Checked = True
        pCallCompany(frm_vend_EnterBills)
    End Sub
    Private Sub ReceiveItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_rcveItm.Click
        frm_vend_EnterBills.TabControl1.SelectedIndex = 1
        frm_vend_EnterBills.Text = "Create Item Receipts"
        frm_vend_EnterBills.chkBillRcv.Checked = False
        pCallCompany(frm_vend_EnterBills)
    End Sub
    Private Sub EnterBillsForItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sup_entRcvBill.Click
        pCallCompany(frm_vend_EnterBillsForItems)
    End Sub
#End Region

#Region "Reports"
    Private Sub GeneralLedgerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralLedgerToolStripMenuItem.Click
        pCallCompany(frmGeneralLedger)
    End Sub
    Private Sub GeneralLedgerPerAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pCallCompany(frmGeneralLedgerPerAccountvb)
    End Sub
    Private Sub TrialBalanceToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrialBalanceToolStripMenuItem1.Click
        pCallCompany(FrmTrialBalance)
    End Sub
    Private Sub BalanceSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BalanceSheetToolStripMenuItem.Click
        If VerifyCOA() = True Then
            pCallCompany(frmBalanceSheet)
        End If
    End Sub
    Private Sub IncomeStatement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IncomeStatementToolStripMenuItem.Click, IncomeStatementToolStripMenuItem.Click
        pCallCompany(frmIncomeStatement)
    End Sub
    Private Sub AccountsPayableSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountsPayablesToolStripMenuItem.Click
        pCallCompany(frmAccountsPayableSummary_v2)
    End Sub
    'Private Sub BillsPaymentAccounts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BillsPaymentAccountsReportToolStripMenuItem.Click
    '    pCallCompany(frmBillsPaymentAccounts)
    'End Sub
    Private Sub AccountsPayableList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountsPayableListToolStripMenuItem.Click
        pCallCompany(frmPVCVList)
    End Sub
    Private Sub AgingReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        pCallCompany(frmAgingReport)
    End Sub
    Private Sub CheckVoucherHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem16.Click
        pCallCompany(frmCheckReprint)
    End Sub
    Private Sub CollectionReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CollectionReportToolStripMenuItem.Click
        pCallCompany(frmCollection)
    End Sub
    Private Sub FinanceCharge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem4.Click
        pCallCompany(frmFCparameter)
    End Sub
    Private Sub JournalVoucherSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JournalVoucherSummary.Click
        pCallCompany(frmJVSummary)
    End Sub
    Private Sub JournalVoucherSummary_PerAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JournalVoucherPerAccountToolStripMenu.Click
        pCallCompany(frmJournalSummaryperAcnt)
    End Sub
    Private Sub CustomerList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem7.Click
        pCallCompany(frmCustomerList)
    End Sub
    Private Sub ItemList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem8.Click
        pCallCompany(frmItemList)
    End Sub
    Private Sub SupplierList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem9.Click
        pCallCompany(frmSupplierList)
    End Sub
    Private Sub ReimbursableExpenses_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem10.Click
        pCallCompany(frmReimbursableExpense)
    End Sub
    Private Sub SalesReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsSalesreport.Click
        Call pCallCompany(frmSalesRpt)
    End Sub
    Private Sub StatementOfAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatementOfAccountToolStripMenuItem.Click
        pCallCompany(frmStatment)
    End Sub
    Private Sub TaxReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem11.Click
        pCallCompany(frmTaxReport)
    End Sub
    Private Sub UndepositedFunds_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem3.Click
        pCallCompany(frmUndepositedParameter)
    End Sub
    Private Sub BudgetMonitoringReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BudgetMonitoringReportToolStripMenuItem.Click
        pCallCompany(frmBudgetMonitoring)
    End Sub
    Private Sub CustomerTransactionReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CostumerTransactionReportsToolStripMenuItem.Click
        pCallCompany(frmCustomerTransactionReport)
    End Sub
    Private Sub IncomeStatementDrillDownToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IncomeStatementDrillDownToolStripMenuItem.Click
        pCallCompany(frmIncomeStatement_Standard_DrillDown)
    End Sub
    Private Sub IncomeStatementPrevYearComparisonToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IncomeStatementPrevYearComparisonToolStripMenuItem.Click
        pCallCompany(frmIncomeStatement_PrevYear_DrillDown)
    End Sub
    Private Sub IncomeStatementWithStatutoryAllocationsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IncomeStatementWithStatutoryAllocationsToolStripMenuItem.Click
        pCallCompany(frmIncomeStatement_StatutoryAlloc_DrillDown)
    End Sub
#End Region

#Region "System Settings"
    Private Sub UserList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sett_userList.Click
        pCallCompany(frmUserList)
    End Sub
    Private Sub CreateUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sett_createuser.Click ' Handles ms_sett_createuser1.Click
        frmUserNew.mode = "ADD"
        pCallCompany(frmUserNew)
    End Sub
    Private Sub ChangePassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sett_changepass.Click
        pCallCompany(frmPasswordChange)
    End Sub
    Private Sub Preferences_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_sett_preferences.Click
        pCallCompany(frm_Preferences)
    End Sub
    Private Sub PeriodRestrictions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PeriodRestrictionsToolStripMenuItem.Click
        frmPeriodRestrictionV2.ShowDialog()
    End Sub
    Private Sub UploadData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pCallCompany(frmImportData)
    End Sub
    Private Sub UploadSalesReceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pCallCompany(frmSRImport)
    End Sub
    Private Sub UploadDeliveryReceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pCallCompany(frmDRImport)
    End Sub
    Private Sub UploadTemporary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pCallCompany(frmUploadItemMaster)
    End Sub
    Private Sub UploadChartOfAccountsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmItemMasterUploader.xMode = "Chart of Accounts"
        pCallCompany(frmItemMasterUploader)
    End Sub
#End Region

#End Region

#Region "Main Events"


    Private Function VerifyCOA() As Boolean
        Try
            Dim SQLStr As String = "SELECT COUNT(acnt_id) InvalidCounts FROM mAccounts WHERE fcLevel > 1 AND acnt_subof IS NULL AND co_id = '" & gCompanyID() & "'"

            Using rd As SqlDataReader = xSQLHelper.ExecuteReader(SQLStr)
                While rd.Read()
                    If rd("InvalidCounts") <> 0 Then
                        Dim xFrom As New frmCOAVerification
                        xFrom.ShowDialog()
                        Return False
                    Else
                        Return True
                    End If
                End While
            End Using
        Catch ex As Exception
            Return False
            ShowErrorMessage("VerifyCOA", Me.Name, ex.Message)
        End Try

    End Function
    Private Sub NewCompanyFormLoad()
        With frm_MF_Company
            .btnClear.Location = New Point(306, 451)
            .btnCancel.Location = New Point(420, 451)
            .cbofiscalYr.Visible = True
            .Label14.Visible = True
            .Size = New Point(542, 521)
            gTrans.gClearFormTxt(frm_MF_Company)
        End With
    End Sub
    Private Sub OpenCompanyFormLoad()
        With frm_MF_Company
            .btnClear.Location = New Point(282, 259)
            .btnCancel.Location = New Point(396, 259)
            .cbofiscalYr.Visible = False
            .Label14.Visible = False
            .cboconame.Enabled = True
            .Size = New Point(528, 318)
        End With
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mAbout As New ExplorerBar
        Me.axbar.Bars.Add(mAbout)
        With mAbout
            .BackColor = Color.AliceBlue
            .TitleBackColorStart = Color.SteelBlue
            .TitleBackColorEnd = Color.LightBlue
            .TitleForeColor = Color.Ivory
            .TitleForeColorHot = Color.LightCoral
            .Text = "About"
            .IsSpecial = True
            .ToolTipText = "Contains Information about the System."
            .State = ExplorerBarState.Expanded
        End With
        modMainSideBar.mSideAbout(Me.axbar, 0)

        currentDate.Text = Format(CDate(Now.Date), "dddd MM/dd/yyyy")
        currentUser.Text = strSysCurrentFullName
        toolStripUsername.Text = strSysCurrentUserName

        databaseInUse.Text = "Database: " & gcon.Database
        Sidebar_Status()
        toolStripToolsOptions.Visible = False
    End Sub
    Private Sub frmMain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim x As DialogResult
        x = MessageBox.Show("Are you sure you want to exit ?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        If x = Windows.Forms.DialogResult.OK Then
            'frmWait.MdiParent = Me
            AuditTrail_Save("LOG-OUT", "")
            frmWait.Show() 'splash screen            
        ElseIf x = Windows.Forms.DialogResult.Cancel Then
            e.Cancel = True
        End If
    End Sub

    Private Sub Sidebar_ItemClick(ByVal sender As Object, ByVal args As vbAccelerator.Components.Controls.ExplorerBarItemClickEventArgs) Handles axbar.ItemClick
        Select Case args.Item.Tag
            Case "mchart"
                Call pCallCompany(frm_acc_chartaccounts)
            Case "mitemt"
                Call pCallCompany(frm_item_masterItem)
            Case "mprice"
                Call pCallCompany(frm_acc_priceLevel)
            Case "mrate"
                Call pCallCompany(frm_acc_billRateLevel)
            Case "mTax"
                Call pCallCompany(frm_vend_SalesTax)
            Case "mrep"
                Call pCallCompany(frm_acc_salesRep)
            Case "mcusttype"
                Call pCallCompany(frm_acc_customerType)
            Case "msuptype"
                Call pCallCompany(frm_acc_vendorType)
            Case "mterm"
                Call pCallCompany(frm_acc_terms)
            Case "mCust"
                Call pCallCompany(frm_cust_masterCustomer)
            Case "mso"
                Call pCallCompany(frm_cust_CreateSalesOrder)
            Case "minv"
                Call pCallCompany(frm_cust_CreateInvoice)
            Case "mSalesrcp"
                Call pCallCompany(frm_cust_CreateSalesReceipt)
            Case "mscharg"
                Call pCallCompany(frm_cust_CreateSalesReceipt)
            Case "mstate"
                Call pCallCompany(frm_cust_CreateStatementCharges)
            Case "massess"
                Call pCallCompany(frm_cust_AssessFinanceCharges)
            Case "mrcvpay"
                Call pCallCompany(frm_cust_ReceivePayment)
            Case "mcmemo"
                Call pCallCompany(frm_cust_CreateCreditMemos)
            Case "mSupp"
                Call pCallCompany(frm_vend_masterVendor)
            Case "mEB"
                Call pCallCompany(frm_vend_EnterBills)
            Case "mPB"
                Call pCallCompany(frm_vend_PayBills)
            Case "mST"
                Call pCallCompany(frm_vend_SalesTax)
            Case "mCPO"
                Call pCallCompany(frm_vend_CreatePurchaseOrder)
            Case "mRIB"
                Call pCallCompany(frm_vend_EnterBills)
            Case "mRI"
                Call pCallCompany(frm_vend_EnterBills)
            Case "mERI"
                Call pCallCompany(frm_vend_EnterBillsForItems)
            Case "mWC"
                Call pCallCompany(frm_acc_writechecks)
            Case "mMD"
                Call pCallCompany(frm_acc_makedeposit)
            Case "mTF"
                Call pCallCompany(frm_acc_transferfunds)
            Case "mR"
                Call pCallCompany(frm_acc_reconcile)
            Case "DMemo"
                pCallCompany(frmDebitMemos)
            Case "mWrkngTrialblnc"
                pCallCompany(frm_acc_workingtrialbalance)
            Case "mSales"
                Call pCallCompany(frmSalesRpt)
            Case "mAPandCVList"
                Call pCallCompany(frmPVCVList)
            Case "mP"
                Call pCallCompany(frmCollection)
            Case "mReconcile"
                pCallCompany(frm_acc_reconcile)
            Case "mAPSummary"
                pCallCompany(frmAPSummary)
                'Case "mBPA"
                '    pCallCompany(frmBillsPaymentAccounts)
            Case "mAging"
                pCallCompany(frmAgingReport)
            Case "mBlnceSheet"
                pCallCompany(frmBalanceSheet)
            Case "mCVHistory"
                pCallCompany(frmCheckReprint)
            Case "mFC"
                pCallCompany(frmFCparameter)
            Case "mGLListing"
                pCallCompany(frmGLTransaction_listing)
            Case "mGLbyAccnt"
                pCallCompany(frmGLTransaction_listingByAct)
            Case "mIncmeStatement"
                pCallCompany(frmIncomeStatement)
            Case "mJVSummary"
                pCallCompany(frmJVSummary)
            Case "mJVSumPerAccnt"
                pCallCompany(frmJournalSummaryperAcnt)
            Case "mCustMasterList"
                pCallCompany(frmCustomerList)
            Case "mItemList"
                pCallCompany(frmItemList)
            Case "mSupList"
                pCallCompany(frmSupplierList)
            Case "mReimburse"
                pCallCompany(frmReimbursableExpense)
            Case "mSalesRpt"
                pCallCompany(frmSalesRpt)
            Case "mStatment"
                pCallCompany(frmStatment)
            Case "mTaxReport"
                pCallCompany(frmTaxReport)
            Case "mUndepositedParameter"
                pCallCompany(frmUndepositedParameter)
            Case "mBudgetMonitoring"
                pCallCompany(frmBudgetMonitoring)
            Case "mCustomerTransactionReport"
                pCallCompany(frmCustomerTransactionReport)
            Case "mUL"
                pCallCompany(frmUserList)
            Case "mCP"
                pCallCompany(frmPasswordChange)
            Case "mPref"
                Call pCallCompany(frm_Preferences)
            Case "mJournalEntry"
                pCallCompany(frm_acc_makeGeneralJournalEntry)
        End Select
    End Sub
    Private Sub SideBar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbar.Click
        If btnbar.Text = "Hide Side Bar" Then
            axbar.Visible = False
            btnbar.Text = "Show Side Bar"
        ElseIf btnbar.Text = "Show Side Bar" Then
            axbar.Visible = True
            btnbar.Text = "Hide Side Bar"
        End If
    End Sub
    Private Sub Sidebar_Status()
        If axbar.Visible = True Then
            btnbar.Text = "Hide Side Bar"
        ElseIf axbar.Visible = False Then
            btnbar.Text = "Show Side Bar"
        End If
    End Sub

    Private Sub pCalltemp(ByRef xform As Form)
        If gVerifyCompany() = True Then
            Try
                xform.Show()
            Catch ex As Exception
            End Try
        Else
            Try
                If MsgBox("Please Open project to proceed, do you want to continue?", _
                                     MsgBoxStyle.OkCancel, xform.Text) = MsgBoxResult.Ok Then
                    gMFiles.gCompanyEvents(frm_MF_Company, frm_MF_Company.cboconame, _
                                    frm_MF_Company.btnSave, frm_MF_Company.btnClear, _
                                    frm_MF_Company.lblCoInfo)
                    gTrans.gDisableFormTxt(frm_MF_Company)
                    Call OpenCompanyFormLoad()
                    frm_MF_Company.Show()
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub

#End Region

#Region "Disabled Events"
    'Private Sub pCallCompany(ByRef xform As Form)
    '    If gVerifyCompany() = True Then
    '        Try
    '            xform.MdiParent = Me
    '            xform.Show()
    '        Catch ex As Exception
    '        End Try
    '    Else
    '        Try
    '            If MsgBox("Please Open company to proceed, do you want to continue?", _
    '                                 MsgBoxStyle.OkCancel, xform.Text) = MsgBoxResult.Ok Then
    '                gMFiles.gCompanyEvents(frm_MF_Company, frm_MF_Company.cboconame, _
    '                                frm_MF_Company.btnSave, frm_MF_Company.btnClear, _
    '                                frm_MF_Company.lblCoInfo)
    '                gTrans.gDisableFormTxt(frm_MF_Company)
    '                Call OpenCompanyFormLoad()
    '                frm_MF_Company.MdiParent = Me
    '                frm_MF_Company.Show()
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    End If
    'End Sub

    'Private Sub ImportingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportingToolStripMenuItem.Click
    '    pCallCompany(frmImportData)
    'End Sub

    'Private Sub ms_tax_managetax_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_tax_managetax.Click
    '    pCallCompany(frm_vend_ManageSalesTax)
    'End Sub
    'Private Sub ms_tax_paysalestax_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_tax_paysalestax.Click
    '    pCallCompany(frm_vend_paySalesTax)
    'End Sub
    'Private Sub ms_tax_adjtaxdue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_tax_adjtaxdue.Click
    '    pCallCompany(frm_vend_AdjustSalesTax)
    'End Sub
    'Private Sub ms_master_tax_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_master_tax.Click
    '    pCallCompany(frm_MF_tax)
    'End Sub
    'Private Sub tsCVSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    pCallCompany(frmCVSummary)
    'End Sub
    'Private Sub UploadFromExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    pCallCompany(frmDRImport)
    'End Sub
#End Region

    Private Sub StatutoryAllocationsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatutoryAllocationsToolStripMenuItem.Click
        pCallCompany(frmStatutoryAllocations)
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OptionsToolStripMenuItem.Click
        pCallCompany(frmOptions)
    End Sub

    Private Sub CreateClosingEntriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateClosingEntriesToolStripMenuItem.Click
        pCallCompany(frmClosingEntries)
    End Sub

    Private Sub BankFileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BankFileToolStripMenuItem.Click
        pCallCompany(frmbankfileconvertion)
    End Sub

    Private Sub DepositAccountingEntriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepositAccountingEntriesToolStripMenuItem.Click
        pCallCompany(frmAccntEntries_Deposit)
    End Sub

    Private Sub JournalEntriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JournalEntriesToolStripMenuItem.Click
        frmMasterUploader.xMode = "Journal Entries"
        frmMasterUploader.GetUser() = currentUser.Text
        pCallCompany(frmMasterUploader)
    End Sub

    Private Sub DocumentTypeSettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DocumentTypeSettingsToolStripMenuItem.Click
        pCallCompany(frmAEDDoctype)
    End Sub

    Private Sub ChartOfAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChartOfAccountToolStripMenuItem.Click
        frmMasterUploader.xMode = "Chart of Accounts"
        frmMasterUploader.GetUser() = currentUser.Text
        pCallCompany(frmMasterUploader)
    End Sub

    Private Sub SupplierMasterToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SupplierMasterToolStripMenuItem.Click
        frmMasterUploader.xMode = "Supplier List"
        frmMasterUploader.GetUser() = currentUser.Text
        pCallCompany(frmMasterUploader)
    End Sub

    Private Sub CostCenterToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CostCenterToolStripMenuItem.Click
        pCallCompany(frmCostCenter_AddEdit)
    End Sub

    Private Sub DocumentTypeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DocumentTypeToolStripMenuItem.Click
        Document_Issuance.ShowDialog()
    End Sub

    Private Sub CheckIssuanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckIssuanceToolStripMenuItem.Click
        frmCheckIssuance.ShowDialog()
    End Sub

    Private Sub ColumnInterfaceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColumnInterfaceToolStripMenuItem.Click
        pCallCompany(ColumnInterface)
    End Sub

    Private Sub MonthlyToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyToolStripMenuItem1.Click
        pCallCompany(frmGeneralJournalReport)
    End Sub

    Private Sub DailyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailyToolStripMenuItem.Click
        pCallCompany(frmCDJDaily)
    End Sub

    Private Sub DailyToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailyToolStripMenuItem1.Click
        pCallCompany(frmGeneralJournalDailyReport)
    End Sub

    Private Sub MonthlyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyToolStripMenuItem.Click
        pCallCompany(frmCDJMonthly)
    End Sub

    Private Sub DebitCreditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DebitCreditToolStripMenuItem.Click
        pCallCompany(frmMasterFile_DebitCredit)
    End Sub

    Private Sub AccountRegisterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountRegisterToolStripMenuItem.Click
        pCallCompany(frmMasterfile_AccountRegister)
    End Sub

    Private Sub NotBalanceEntriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotBalanceEntriesToolStripMenuItem.Click
        frmNotBalanceEntry.ShowDialog()
    End Sub

    Private Sub DailyToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pCallCompany(frmPurchaseOrder)
    End Sub

    'Private Sub MonthlyToolStripMenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyToolStripMenuItem4.Click
    '    pCallCompany(frmSalesReturnMonthly)
    'End Sub

    'Private Sub MonthlyToolStripMenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyToolStripMenuItem3.Click
    '    pCallCompany(frmPurchaseReturnMonthly)
    'End Sub

    'Private Sub MonthlyToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyToolStripMenuItem2.Click
    '    pCallCompany(frmPurchaseOrderMonthly)
    'End Sub

    Private Sub UserAccesibilityToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UserAccesibilityToolStripMenuItem.Click
        pCallCompany(frmUserAccesibility)
    End Sub

    Private Sub BackUpDatabaseToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackUpDatabaseToolStripMenuItem1.Click
        pCallCompany(frmBackup)
    End Sub

    Private Sub AccountInquiryToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountInquiryToolStripMenuItem1.Click
        pCallCompany(frmVerification)
    End Sub

    Private Sub GenerateTemplateToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GenerateTemplateToolStripMenuItem1.Click
        pCallCompany(frmGenerateTemplate)
    End Sub

    Private Sub AccountForwardBalanceToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountForwardBalanceToolStripMenuItem1.Click
        pCallCompany(frmForwardBalance)
    End Sub

    Private Sub ResetForwardBalanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetForwardBalanceToolStripMenuItem.Click
        If MsgBox("Are you sure you want to RESET forward balance?" & vbNewLine & "Click yes to continue.", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Reset Forward Balance") = vbYes Then
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "spu_ForwardBalance_Reset")
            AuditTrail_Save("DATABASE", "Data Migration Tools > Reset Forward Balance")
        End If
    End Sub

    Private Sub DailyToolStripMenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailyToolStripMenuItem10.Click
        pCallCompany(frmTrialBalanceDailySingleForm)
    End Sub

    Private Sub AccountRegisterToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountRegisterToolStripMenuItem2.Click
        pCallCompany(frmAccountRegisterUploader)
    End Sub

    Private Sub SavingsInterestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SavingsInterestToolStripMenuItem.Click
        pCallCompany(frmSavingsInterestComputation)
    End Sub

    Private Sub CollectionsIntegratorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CollectionsIntegratorToolStripMenuItem.Click
        pCallCompany(frmCollectionsIntegrator)
    End Sub

    Private Sub ClientMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientMasterToolStripMenuItem.Click
        pCallCompany(frmMember_Master)
    End Sub

    Private Sub ByCollectorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByCollectorToolStripMenuItem.Click
        pCallCompany(frmDailyCashCollection)
    End Sub

    Private Sub JournalListingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JournalListingToolStripMenuItem.Click
        pCallCompany(frmJournalListingReport)
    End Sub

    Private Sub DebitAccountsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DebitAccountsToolStripMenuItem1.Click
        frmAccountBalance.xAccount = "Debit"
        pCallCompany(frmAccountBalance)
    End Sub

    Private Sub CreditAcountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreditAcountToolStripMenuItem.Click
        frmAccountBalance.xAccount = "Credit"
        pCallCompany(frmAccountBalance)
    End Sub

    Private Sub BillingListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BillingListToolStripMenuItem.Click
        pCallCompany(frmBilling_byGroup)
    End Sub

    Private Sub WorkingTrialBalanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkingTrialBalanceToolStripMenuItem.Click
        pCallCompany(frmTrialBalanceReport)
    End Sub

    Private Sub IncomeStatementToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IncomeStatementToolStripMenuItem1.Click
        pCallCompany(frmIncomeStatementv2)
    End Sub

    Private Sub BalanceSheetToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BalanceSheetToolStripMenuItem1.Click
        pCallCompany(frmBalanceSheetv2)
    End Sub

    Private Sub GeneralLedgerToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralLedgerToolStripMenuItem2.Click
        pCallCompany(frmGeneralLedgerDetailedV2)
    End Sub

    Private Sub AuditTrailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AuditTrailToolStripMenuItem.Click
        pCallCompany(frmAuditTrail)
    End Sub

    Private Sub ClientUploadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientUploadToolStripMenuItem.Click

    End Sub

    Private Sub CompanyProfileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompanyProfileToolStripMenuItem.Click
        CompanyProfile.ShowDialog()
    End Sub

    Private Sub AccountAnalysisToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountAnalysisToolStripMenuItem.Click
        pCallCompany(frmGeneralLedgerDetailedV2)
    End Sub

    Private Sub SalesInvoiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalesInvoiceToolStripMenuItem.Click
        pCallCompany(frmSalesInvoice)
    End Sub

    Private Sub PurchaseOderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PurchaseOderToolStripMenuItem.Click
        pCallCompany(frmPurchaseOrder)
    End Sub

    Private Sub PurchaseReturnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PurchaseReturnToolStripMenuItem.Click
        pCallCompany(frmPurchaseReturnDaily)
    End Sub

    Private Sub ByCategoryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByCategoryToolStripMenuItem.Click
        pCallCompany(frmAgingReports)
    End Sub

    Private Sub SalesReturnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalesReturnToolStripMenuItem.Click
        pCallCompany(frmSalesReturnDaily)
    End Sub

    Private Sub AccountAnalysisToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountAnalysisToolStripMenuItem1.Click
        pCallCompany(frmAccountAnalysis)
    End Sub

    Private Sub ClientList1ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientList1ToolStripMenuItem.Click
        pCallCompany(frmRpt_MemberList)
    End Sub

    Private Sub ClientList2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientList2ToolStripMenuItem.Click
        pCallCompany(frmReport_Client_v2)
    End Sub

    Private Sub CashPositionReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CashPositionReportToolStripMenuItem.Click
        pCallCompany(frmRptCashPosition)
    End Sub

    Private Sub AgingOfLoansReceivableToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgingOfLoansReceivableToolStripMenuItem.Click
        pCallCompany(frmAgingReports)
    End Sub

    Private Sub RecurringEntrySetupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecurringEntrySetupToolStripMenuItem.Click
        frmRecurringEntry.ShowDialog()
    End Sub

    'Private Sub SOARegisterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    FrmSoaRegister.ShowDialog()
    'End Sub

    Private Sub ms_main_computation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ms_main_computation.Click

    End Sub

    Private Sub CreditsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreditsToolStripMenuItem.Click

    End Sub

    Private Sub CRJMonthlySummarizedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CRJMonthlySummarizedToolStripMenuItem.Click

    End Sub

    Private Sub DividendToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DividendToolStripMenuItem.Click
        pCallCompany(frmDividendComputation)
    End Sub

    Private Sub PatronageRefundToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PatronageRefundToolStripMenuItem.Click
        pCallCompany(frmPatronageComputation)
    End Sub

    Private Sub StatutoryFundToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles StatutoryFundToolStripMenuItem.Click
        pCallCompany(frmStatutoryEntry)
    End Sub

    Private Sub statutoryMaster_Click(sender As System.Object, e As System.EventArgs) Handles statutoryMaster.Click
        pCallCompany(frmStatutoryMaster)
    End Sub

    Private Sub ByCashierToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ByCashierToolStripMenuItem.Click
        pCallCompany(frmReceiptsPerTeller)
    End Sub

    Private Sub CombinedToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CombinedToolStripMenuItem.Click
        pCallCompany(frmConsolidatedReceipts)
    End Sub

    Private Sub DynamicColumnToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DynamicColumnToolStripMenuItem.Click
        pCallCompany(frmCRJDynamicColumn)
    End Sub
End Class