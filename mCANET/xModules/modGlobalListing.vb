Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Module modSupplier

    Private gCon As New Clsappconfiguration

    Public Sub m_loadSupplierToMulticolumnCbobox(ByVal multiColumnCbo As MTGCComboBox)
        Dim DTSupplier As New DataTable("Supplier")
        Dim DR As DataRow
        Dim sSQLCmd As String = "SELECT fcSupplierName, fxKeySupplier "
        sSQLCmd &= " FROM mSupplier00Master WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcSupplierName"

        DTSupplier.Columns.Add("fcSupplierName", System.Type.GetType("System.String"))
        DTSupplier.Columns.Add("fxKeySupplier", System.Type.GetType("System.String"))

        DR = DTSupplier.NewRow
        DR("fcSupplierName") = "Create Name"
        DR("fxKeySupplier") = ""
        DTSupplier.Rows.Add(DR)

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    DR = DTSupplier.NewRow
                    DR("fcSupplierName") = rd.Item(0).ToString
                    DR("fxKeySupplier") = rd.Item(1).ToString
                    DTSupplier.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at m_loadSupplierToMulticolumnCbobox in modGlobalListing module" & vbCr & vbCr & ex.ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
        multiColumnCbo.Items.Clear()
        multiColumnCbo.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        multiColumnCbo.SourceDataString = New String(1) {"fcSupplierName", "fxKeySupplier"}
        multiColumnCbo.SourceDataTable = DTSupplier
    End Sub

    Public Sub m_loadShippingAddressToMtcbo(ByVal Mtcbo As MTGCComboBox, ByVal sKey As String)

        Dim DTShipAddress As New DataTable("Address")
        Dim DR As DataRow
        Dim sSQLCmd As String = "SELECT fxKeyAddress, fcAddressName, fcAddress FROM mAddress WHERE fxKeyID = '" & sKey & "' and fcAddressType = 'shipping' ORDER by fcAddressName"

        DTShipAddress.Columns.Add("fcAddressName", System.Type.GetType("System.String"))
        DTShipAddress.Columns.Add("fxKeyAddress", System.Type.GetType("System.String"))
        DTShipAddress.Columns.Add("fcAddress", System.Type.GetType("System.String"))

        DR = DTShipAddress.NewRow
        DR("fcAddressName") = "Create New"
        DR("fxKeyAddress") = ""
        DR("fcAddress") = ""
        DTShipAddress.Rows.Add(DR)

        If sKey <> "" Then
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                    While rd.Read
                        DR = DTShipAddress.NewRow
                        DR("fcAddressName") = rd.Item("fcAddressName").ToString
                        DR("fxKeyAddress") = rd.Item("fxKeyAddress").ToString
                        DR("fcAddress") = rd.Item("fcAddress").ToString
                        DTShipAddress.Rows.Add(DR)
                    End While
                End Using
            Catch ex As Exception
                MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Shipping Address")
            End Try
        End If

        Mtcbo.Items.Clear()
        Mtcbo.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        Mtcbo.SourceDataString = New String(2) {"fcAddressName", "fxKeyAddress", "fcAddress"}
        Mtcbo.SourceDataTable = DTShipAddress

    End Sub

    Public Sub M_ShipToCompanyNameToMtcbo(ByVal MtCbo As MTGCComboBox, ByVal bViewAll As Boolean)
        Dim Dr As DataRow
        Dim DTName As New DataTable("Names")
        Dim sSQLCmd As String

        DTName.Columns.Add("fcName", System.Type.GetType("System.String"))
        DTName.Columns.Add("fxKey", System.Type.GetType("System.String"))
        DTName.Columns.Add("fcType", System.Type.GetType("System.String"))


        sSQLCmd = "select co_name, co_id from dbo.mCompany where co_id = '" & gCompanyID() & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    Dr = DTName.NewRow
                    Dr("fcName") = rd.Item("co_name").ToString
                    Dr("fxKey") = rd.Item("co_id").ToString
                    Dr("fcType") = ""
                    DTName.Rows.Add(Dr)
                End While
            End Using
        Catch ex As Exception
            MsgBox("M_LoadAllNameToMtcbo", "modGlobalListing", ex.Message)
        End Try

        sSQLCmd = "SPU_SalesRepOption_List "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@bViewAll = " & IIf(bViewAll = True, 1, 0)

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    Dr = DTName.NewRow
                    Dr("fcName") = rd.Item("fcName").ToString
                    Dr("fxKey") = rd.Item("fxKey").ToString
                    Dr("fcType") = rd.Item("fcType").ToString
                    DTName.Rows.Add(Dr)
                End While
            End Using
        Catch ex As Exception
            MsgBox("M_LoadAllNameToMtcbo", "modGlobalListing", ex.Message)
        End Try

        MtCbo.Items.Clear()
        MtCbo.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MtCbo.SourceDataString = New String(2) {"fcName", "fxKey", "fcType"}
        MtCbo.SourceDataTable = DTName
    End Sub

    Public Sub LoadDefinedAcntsToMtcbo(ByVal MtCBO As MTGCComboBox, ByVal AccountShouldBeDisplay As String, Optional ByVal IncludeNoAcntOption As Boolean = False)
        Dim DTDefinedAcnts As New DataTable("Accounts")
        Dim DR As DataRow
        Dim sSqlCmd As String
        If AccountShouldBeDisplay = "ALL" Then
            sSqlCmd = "SELECT t1.acnt_id as fxKeyAccount, t1.acnt_desc from dbo.mAccounts t1 where t1.co_id = '" & gCompanyID() & "'order by t1.acnt_desc"
        Else
            sSqlCmd = "SELECT t1.acnt_id as fxKeyAccount, t1.acnt_desc " & _
                        "from dbo.mAccounts t1 inner join dbo.mAccountType t2 " & _
                        "on t1.acnt_type = t2.actType_id " & _
                        "where t1.co_id = '" & gCompanyID() & "' and t2.acnt_type = '" & AccountShouldBeDisplay & _
                        "' and t1.fbActive = 'True'" & " order by t1.acnt_desc"
        End If
        DTDefinedAcnts.Columns.Add("acnt_desc", System.Type.GetType("System.String"))
        DTDefinedAcnts.Columns.Add("fxKeyAccount", System.Type.GetType("System.String"))

        If IncludeNoAcntOption = True Then
            DR = DTDefinedAcnts.NewRow
            DR("acnt_desc") = "No Account"
            DR("fxKeyAccount") = ""
            DTDefinedAcnts.Rows.Add(DR)
        End If

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DR = DTDefinedAcnts.NewRow
                    DR("acnt_desc") = rd.Item(1).ToString
                    DR("fxKeyAccount") = rd.Item(0).ToString
                    DTDefinedAcnts.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            ShowErrorMessage("LoadDefinedAcntsToMtcbo", "modGlobalListing", ex.Message)
        End Try

        MtCBO.Items.Clear()
        MtCBO.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MtCBO.SourceDataString = New String(1) {"acnt_desc", "fxKeyAccount"}
        MtCBO.SourceDataTable = DTDefinedAcnts
    End Sub

    Public Sub GetSpecifiedAcnt(ByVal Mtcbo As MTGCComboBox, ByVal AccountTypeToBeGet As String)
        Dim DTAccounts As New DataTable("Accounts")
        Dim DR As DataRow

        DTAccounts.Columns.Add("AccountName")
        DTAccounts.Columns.Add("AccountID")

        Dim sSqlCmd As String = "Select t1.acnt_name, t1.acnt_id from dbo.mAccounts t1 " & _
                                "inner join dbo.mAccountType t2 " & _
                                "on t1.acnt_type = t2.actType_id " & _
                                "where t2.acnt_type = '" & AccountTypeToBeGet & "' and t1.co_id = '" & gCompanyID() & "' " & _
                                "and fbActive = 1 ORDER BY t1.Acnt_Code"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DR = DTAccounts.NewRow()
                    DR("AccountName") = rd.Item("acnt_name").ToString
                    DR("AccountID") = rd.Item("acnt_id").ToString
                    DTAccounts.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            ShowErrorMessage("GetSpecifiedAcnt", "modGlobalListing", ex.Message)
        End Try

        Mtcbo.Items.Clear()
        Mtcbo.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        Mtcbo.SourceDataString = New String(1) {"AccountName", "AccountID"}
        Mtcbo.SourceDataTable = DTAccounts
        If Mtcbo.Items.Count <> 0 Then
            Mtcbo.SelectedIndex = 0
        End If
    End Sub

    Public Sub ShowErrorMessage(ByVal SubOrFunctionName As String, ByVal ModuleName As String, ByVal ErrorMessage As String)
        MsgBox("Error encounter at " & SubOrFunctionName & " in " & ModuleName & "." & vbCr & vbCr & ErrorMessage, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error Occur!")
    End Sub

    Public Sub m_loadSupplier(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeySupplier, fcSupplierName "
        sSQLCmd &= " FROM mSupplier00Master WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcSupplierName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcSupplierName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeySupplier").ToString)
                End While
            End Using

            Dim xCnt As Integer

            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next

            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Supplier")
        End Try
    End Sub

    Public Sub m_GetSuppliers(ByRef cboTemp As ComboBox, ByRef sForm As Form)
        Try
            Dim cbo As New ComboBox
            Dim sSQLCmd As String = "usp_m_msupplier_getname "
            sSQLCmd &= " @coid='" & gCompanyID() & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cbo.Items.Add(" ")
                cbo.Items.Add("New")
                cbo.Items.Add("--------------")
                While rd.Read
                    cbo.Items.Add(rd.Item(1).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboTemp.Items.Clear()
            For xCnt = 0 To cbo.Items.Count - 1
                cboTemp.Items.Add(UCase(cbo.Items(xCnt).ToString))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub m_GetTermss(ByRef cboTemp As ComboBox, ByRef sForm As Form)
        Try
            Dim cbo As New ComboBox
            Dim sSQLCmd As String = "usp_m_mterms_getname "
            sSQLCmd &= " @coid='" & gCompanyID() & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cbo.Items.Add("")
                cbo.Items.Add("New")
                cbo.Items.Add("-------")
                While rd.Read
                    cbo.Items.Add(rd.Item("fcTermsName").ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboTemp.Items.Clear()
            For xCnt = 0 To cbo.Items.Count - 1
                cboTemp.Items.Add(cbo.Items(xCnt).ToString)
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub m_GetSalesDiscount(ByRef cboTemp As ComboBox, ByRef sForm As Form)
        Try
            Dim cbo As New ComboBox
            Dim sSQLCmd As String = "select fcSDCode from dbo.mSalesDiscount where fxKeyCompany='" & gCompanyID() & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cbo.Items.Add(" ")
                cbo.Items.Add("New")
                cbo.Items.Add("--------------")
                While rd.Read
                    cbo.Items.Add(rd.Item(0).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboTemp.Items.Clear()

            For xCnt = 0 To cbo.Items.Count - 1
                cboTemp.Items.Add(cbo.Items(xCnt).ToString)
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Function m_GetSalesDiscountPercentage(ByVal psCode As String, ByRef sForm As Form) As String
        Try
            Dim sVal As String = ""
            Dim sSQLCmd As String = "select fdSDDiscount from dbo.mSalesDiscount where fxKeyCompany='" & gCompanyID() & "' and fcSDCode='" & psCode & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sVal = rd.Item(0)
                End While
            End Using
            Return sVal
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
            Return Nothing
        End Try
    End Function
    Public Function ComputeSalesDiscountAmountForGL(ByVal RetailDiscountPercentage As Decimal, ByVal VAT As Decimal, ByVal SalesDiscountAmount As Decimal)
        Dim RemainingRetail As Decimal
        Dim VatPercentage As Decimal
        Dim Result As Decimal
        If SalesDiscountAmount = 0 Then
            Result = 0
        Else
            RemainingRetail = (100 - RetailDiscountPercentage) / 100
            VatPercentage = (VAT / 100) + 1
            Result = SalesDiscountAmount * RemainingRetail
            Result = Result / VatPercentage
        End If
        Return Result
    End Function
    Public Function m_GetRetailersDiscountPercentage(ByVal psCode As String, ByRef sForm As Form) As String
        Try
            Dim sVal As String = "0"
            Dim sSQLCmd As String = "select fdRDDiscount from dbo.mRetailersDiscount where fxKeyCompany='" & gCompanyID() & "' and fcRDCode='" & psCode & "' "
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sVal = rd.Item(0)
                End While
            End Using
            Return sVal
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
            Return Nothing
        End Try
    End Function

    Public Sub m_saveSalesDiscount(ByVal psSDCode As String, _
                                    ByVal psSDDescription As String, _
                                     ByVal pdSDDiscount As Decimal, _
                                      ByRef sForm As Form)
        Try
            Dim sSQLCmd As String = "usp_msalesdiscount_save "
            sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
            sSQLCmd &= ",@fcSDCode='" & psSDCode & "' "
            sSQLCmd &= ",@fcSDDescription='" & psSDDescription & "' "
            sSQLCmd &= ",@fdSDDiscount='" & pdSDDiscount & "' "
            sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "

            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

            MsgBox("save successful...", MsgBoxStyle.Information, sForm.Text)

        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_saveRetailersDiscount(ByVal psRDCode As String, _
                                       ByVal psRDDescription As String, _
                                       ByVal pdRDDiscount As Decimal, _
                                       ByRef sForm As Form)
        Try
            Dim sSQLCmd As String = "usp_mretailersdiscount_save "
            sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
            sSQLCmd &= ",@fcRDCode='" & psRDCode & "' "
            sSQLCmd &= ",@fcRDDescription='" & psRDDescription & "' "
            sSQLCmd &= ",@fdRDDiscount='" & pdRDDiscount & "' "
            sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "

            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

            MsgBox("save successful...", MsgBoxStyle.Information, sForm.Text)

        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_GetRetailerDiscount(ByRef cboTemp As ComboBox, ByRef sForm As Form)
        Try
            Dim cbo As New ComboBox
            Dim sSQLCmd As String = "select fcRDCode from dbo.mRetailersDiscount where fxKeyCompany='" & gCompanyID() & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cbo.Items.Add(" ")
                cbo.Items.Add("New")
                cbo.Items.Add("--------------")
                While rd.Read
                    cbo.Items.Add(rd.Item(0).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboTemp.Items.Clear()
            For xCnt = 0 To cbo.Items.Count - 1
                cboTemp.Items.Add(cbo.Items(xCnt).ToString)
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub m_loadSupplierType(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeySupplierType, fcTypeName "
        sSQLCmd &= " FROM mSupplier01Type WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= "  ORDER by fcTypeName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcTypeName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeySupplierType").ToString)
                End While
            End Using

            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Supplier Type")
        End Try
    End Sub

    Public Sub m_loadSendMethod(ByVal cboSendMethod As ComboBox)
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dv As DataView
        Dim sSQLCmd As String = "SELECT fxKeySendMethod, fcSendMethodName "
        sSQLCmd &= " FROM mCustomer02SendMethod "
        'sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "' OR fxKeyCompany = ''"
        sSQLCmd &= " ORDER by fcSendMethodName"
        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            dt = ds.Tables(0)
            dv = dt.DefaultView
            cboSendMethod.DataSource = dv
            cboSendMethod.DisplayMember = "fcSendMethodName"
            cboSendMethod.ValueMember = "fxKeySendMethod"
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Send Method")
        End Try
    End Sub

    Public Sub m_loadJobStatus(ByVal cboNew As ComboBox)
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dv As DataView
        Dim sSQLCmd As String = "SELECT fxKeyJobStatus, fcJobStatusName "
        sSQLCmd &= " FROM mCustomer05JobStatus "
        sSQLCmd &= " ORDER by fcJobStatusName"
        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            dt = ds.Tables(0)
            dv = dt.DefaultView
            cboNew.DataSource = dv
            cboNew.DisplayMember = "fcJobStatusName"
            cboNew.ValueMember = "fxKeyJobStatus"
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Job Status")
        End Try
    End Sub

    Public Sub m_loadJobType(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyJobType, fcJobTypeName FROM mCustomer06JobType "
        sSQLCmd &= " WHERE fbActive = 1 "
        sSQLCmd &= " ORDER by fcJobTypeName "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcJobTypeName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeyJobType").ToString)
                End While
            End Using

            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Job Type")
        End Try
    End Sub

    Public Sub m_LoadCustomers(ByRef cboName As ComboBox, ByRef sForm As Form)
        Dim cboTemp As New ComboBox
        Dim sSQLCmd As String = "Selec fcCustomerName from mCustomer00Master where fbActive = 1 and fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= " Order by fcCustomerName Asc "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("Create New")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MsgBox("Load Customer...", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_loadCustomer(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyCustomer, fcCustomerName "
        sSQLCmd &= " FROM mCustomer00Master WHERE  "
        sSQLCmd &= " fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcCustomerName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboName.Items.Add(rd.Item("fcCustomerName"))
                    cboID.Items.Add(rd.Item("fxKeyCustomer").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Custom")
        End Try
    End Sub

    Public Sub m_loadCustomerType(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox

        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")

        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyCustomerType, fcTypeName "
        sSQLCmd &= " FROM mCustomer01Type WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcTypeName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcTypeName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeyCustomerType").ToString)
                End While
            End Using

            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Customer Type")
        End Try
    End Sub

    Public Function m_displaySalesTax() As DataSet
        Dim sSQLCmd As String = " select fcTaxName from mSalesTax where fbActive = 1 and fxKeyCompany ='" & gCompanyID() & "' "
        sSQLCmd &= " Order By fcTaxName Asc "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_displaySalesPer(ByVal psTaxName As String) As Decimal
        Dim sTaxRate As Decimal = 0
        Dim sSQLCmd As String = " select fdTaxRate from mSalesTax where fbActive = 1 and fxKeyCompany ='" & gCompanyID() & "' and fcTaxName='" & psTaxName & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sTaxRate = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        Return sTaxRate
    End Function

    Public Sub m_LoadCustomerTaxCode(ByRef cboName As ComboBox, ByRef sForm As Form)
        Dim cboTemp As New ComboBox
        Dim sSQLCmd As String = " select fcSalesTaxCodeName from mSalesTaxCode where fbActive = 1 and fxKeyCompany ='" & gCompanyID() & "' "
        sSQLCmd &= " Order By fcSalesTaxCodeName Asc "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("Create New")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MsgBox("Load Customer Tax Code...", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_loadSalesTaxCode(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox

        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")

        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeySalesTaxCode, fcSalesTaxCodeName "
        sSQLCmd &= " FROM mSalesTaxCode WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcSalesTaxCodeName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcSalesTaxCodeName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeySalesTaxCode").ToString)
                End While
            End Using

            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Tax Code")
        End Try
    End Sub

    Public Sub m_loadTax(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox

        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")

        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Try
            Dim dtTax As DataSet = m_GetTax()

            Using rd As DataTableReader = dtTax.CreateDataReader
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcTaxName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeyTax").ToString)
                End While
            End Using

            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Tax Code")
        End Try
    End Sub

    Public Sub m_loadShippingAddress(ByVal cboName As ComboBox, ByVal cboID As ComboBox, ByVal sKey As String, Optional ByVal sType As String = "")
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")

        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyAddress, fcAddressName FROM mAddress WHERE fxKeyID = '"

        Select Case sType
            Case "Supplier"
                sSQLCmd &= sKey & "' AND fbSupplier = 1 "
            Case "Other"
                sSQLCmd &= sKey & "' AND fbOther = 1 "
            Case Else
                sSQLCmd &= sKey & "' AND fbCustomer = 1 AND fcAddressName <> '' ORDER by fcAddressName"
        End Select

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboName.Items.Add(rd.Item("fcAddressName"))
                    cboID.Items.Add(rd.Item("fxKeyAddress").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Shipping Address")
        End Try
    End Sub

    Public Sub gloadaddress(ByRef cboadd As ComboBox, ByRef cboid As ComboBox)
        Dim cbotmpadd, cbotmpid As New ComboBox

        Dim sSQLCmd As String = "SELECT fxKeyAddress, fcAddressName FROM mAddress "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cbotmpid.Items.Add(rd.Item("fcAddressName"))
                    cbotmpadd.Items.Add(rd.Item("fxKeyAddress").ToString)
                End While
            End Using

            Dim xcnt1 As Integer
            Dim xcnt2 As Integer

            cboadd.Items.Clear()
            cboid.Items.Clear()

            For xcnt1 = 0 To cbotmpid.Items.Count - 1
                cboid.Items.Add(cbotmpid.Items(xcnt1))
            Next

            For xcnt2 = 0 To cbotmpadd.Items.Count - 1
                cboadd.Items.Add(cbotmpadd.Items(xcnt2))
            Next

        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Address")
        End Try

    End Sub

    Public Function mgetAddressOfSupplier(ByVal fxKeySupplier As String) As String
        Dim sSQLCmd As String = "SELECT fcAddress FROM mAddress"
        sSQLCmd &= " WHERE fxKeyID = '" & fxKeySupplier & "'"

        Dim fcAddress As String = ""
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read() Then
                    fcAddress = rd.Item(0).ToString()
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return fcAddress
    End Function

    Public Sub m_rateInfo(ByVal grdField As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyRate, fcRateName, fdPercentage"
        sSQLCmd &= " FROM mSupplier02RateMaster "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER BY fcRateName"

        Try
            grdField.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Supplier Rate List")
        End Try
    End Sub

    Public Sub m_otherInfoList(ByVal grdField As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyOtherInfo, fcOtherInfoName, fbCustomer, fbSupplier"
        sSQLCmd &= " FROM mOtherInfo00Master "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER BY fcOtherInfoName"

        Try
            grdField.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Other Info. List")
        End Try
    End Sub

    Public Sub m_otherInfoValueGetVal(ByVal sKey As String, ByVal bSupplier As Boolean)
        Dim sSQLCmd As String = "usp_t_otherInfoValues_getTempValues "
        sSQLCmd &= "@fxKey = '" & sKey & "'"
        sSQLCmd &= ",@fbSupplier = " & IIf(bSupplier = True, 1, 0)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch
            'MessageBox.Show(Err.Description, "Other Info. List")
        End Try
    End Sub

    Public Sub m_otherInfoValList(ByVal grdField As DataGridView)
        Dim sSQLCmd As String = ""
        sSQLCmd &= "SELECT fxKeyID, fxKeyOtherInfo,"
        sSQLCmd &= " fxKeyInfoValue,fcOtherInfoName,fcNotes"

        sSQLCmd &= " FROM mOtherInfo02Temp "
        sSQLCmd &= "WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER BY fcOtherInfoName"

        Try
            grdField.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
            With grdField
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns.Item(3).HeaderText = "Other Info"
                .Columns.Item(4).HeaderText = "Data"

            End With
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Other Info. List")
        End Try
    End Sub

    Public Sub m_supplierTypeList(ByVal bShowInactive As Boolean, ByVal grdSupplierType As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeySupplierType, fbActive, fcTypeName FROM mSupplier01Type"
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLCmd &= " AND fbActive = 1"
        End If
        sSQLCmd &= " ORDER BY fcTypeName"

        Try
            grdSupplierType.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Supplier Type List")
        End Try
    End Sub

    Public Sub m_customerTypeList(ByVal bShowInactive As Boolean, ByVal grdCustomerType As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyCustomerType, fbActive, fcTypeName FROM mCustomer01Type"
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLCmd &= " AND fbActive = 1"
        End If
        sSQLCmd &= " ORDER BY fcTypeName"

        Try
            grdCustomerType.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Customer Type List")
        End Try
    End Sub

    Public Sub m_CustomerTerms(ByRef cboName As ComboBox, ByRef sForm As Form)
        Dim cboTemp As New ComboBox
        Dim sSQLCmd As String = " Select fcTermsName from mTerms where fbActive = 1 and fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= " Order By fcTermsName ASC"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("Create New")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MsgBox("Load Customer Terms...", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_loadTerms(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox

        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")

        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")


        Dim sSQLCmd As String = "SELECT fxKeyTerms, fcTermsName FROM mTerms "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " AND fbActive = 1 ORDER by fcTermsName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcTermsName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeyTerms").ToString)
                End While
            End Using
            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Terms")
        End Try
    End Sub

    Public Sub m_termsList(ByVal bShowInactive As Boolean, ByVal grdTerms As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyTerms, fbActive, fcTermsName FROM mTerms"
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLCmd &= " AND fbActive = 1"
        End If
        sSQLCmd &= " ORDER by fcTermsName"
        Try
            grdTerms.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Term List")
        End Try
    End Sub

    Public Sub m_paymentMethodList(ByVal bShowInactive As Boolean, ByVal grdPay As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyPaymentMethod, fbActive, fcPaymentMethodName "
        sSQLCmd &= "FROM mCustomer03PaymentMethod "
        sSQLCmd &= "WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLCmd &= " AND fbActive = 1"
        End If
        sSQLCmd &= " ORDER by fcPaymentMethodName"
        Try
            With grdPay
                .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)

                .Columns(0).Visible = False
                .Columns(1).Visible = False

                .Columns(1).HeaderText = "Active"
                .Columns(2).HeaderText = "Payment Method Name"
                .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                If bShowInactive Then
                    .Columns(1).Visible = True
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Payment Method List")
        End Try
    End Sub

    Public Sub m_taxList(ByVal bShowInactive As Boolean, ByVal grdTax As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyTax, fbActive, fcTaxName, "
        sSQLCmd &= " fdTaxRate FROM mSalesTax"
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLCmd &= " AND fbActive = 1"
        End If

        sSQLCmd &= " ORDER by fcTaxName"

        Try
            grdTax.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch ex As Exception
            MsgBox(Err.Description, "CLICKSOFTWARE:Sales Tax List")
        End Try
    End Sub

    Public Sub m_itemGroupList(ByVal bShowInactive As Boolean, ByVal grdGroup As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyItemGroup, fbActive, fcItemGroupName FROM mItem01Group "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLCmd &= " AND fbActive = 1 "
        End If
        sSQLCmd &= " ORDER by fcItemGroupName"
        Try
            grdGroup.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Item Group List")
        End Try
    End Sub

    Public Sub m_ItemList(ByVal bShowInactive As Boolean, ByVal grdItem As DataGridView, Optional ByVal xFilter As String = Nothing)
        Dim ActiveStatus As Integer
        Dim sSQLCmd As String
        If bShowInactive = False Then
            ActiveStatus = 1
        Else
            ActiveStatus = 0
        End If
        If xFilter Is Nothing Then
            sSQLCmd = "usp_t_DisplayItemMaster '" & gCompanyID() & "', '" & ActiveStatus & "'"
        Else
            sSQLCmd = "usp_t_DisplayItemMasterwithFilter '" & gCompanyID() & "', '" & ActiveStatus & "', '%" & xFilter & "%'"
        End If
        Try
            grdItem.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
            With grdItem
                grdItem.Columns(0).Visible = False
                grdItem.Columns(1).Visible = False
                grdItem.Columns.Item(1).HeaderText = "Active"
                grdItem.Columns.Item(2).HeaderText = "Item Name"
                grdItem.Columns.Item(3).HeaderText = "Group Name"
                grdItem.Columns.Item(4).HeaderText = "Brand"
                grdItem.Columns.Item(5).HeaderText = "Account"
                grdItem.Columns.Item(6).HeaderText = "On Hand"
                grdItem.Columns.Item(7).HeaderText = "Price"
                'grdItem.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                If bShowInactive = True Then
                    grdItem.Columns(1).Visible = True
                End If
                With grdItem
                    '.Columns(1).Width = 50
                    .Columns(2).Width = 400
                    .Columns(3).Width = 100
                    .Columns(4).Width = 80
                    .Columns(5).Width = 70
                    .Columns(6).Width = 60
                    .Columns(7).Width = 80
                    .Columns(8).Width = 80
                End With
            End With
        Catch ex As Exception
            '  MessageBox.Show(Err.Description, "CLICKSOFTWARE:Item List")
        End Try
    End Sub

    Public Sub m_supplierRate(ByVal grdRate As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyRate, fcRateName, fdPercentage "
        sSQLCmd &= "FROM mSupplier02RateMaster "
        sSQLCmd &= "WHERE fxKeyCompany = '" & gCompanyID() & "'"
        Try
            grdRate.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Supplier List")
        End Try
        With grdRate
            .Columns(0).Visible = False
            .Columns.Item(1).HeaderText = "Category"
            .Columns.Item(2).HeaderText = "Percentage %"
            ' .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        End With
    End Sub

    Public Sub m_supplierList(ByVal grdSupplier As DataGridView, ByVal sView As String, ByVal filter As String)
        Dim sSQLCmd As String = "SELECT t1.fxKeySupplier as fxKeySupplier, t1.fxKeyAddress as fxKeyAddress, t1.fbActive as fbActive, t1.fcSupplierName as fcSupplierName, t1.fdBalance  as fdBalance, t1.fcMainAddress "
        sSQLCmd &= "FROM mSupplier00Master as t1 left outer JOIN mAddress as t2 ON t2.fxKeyAddress = t1.fxKeyAddress "
        sSQLCmd &= "left outer JOIN mSupplier01Type as t3 ON t1.fxKeySupplierType = t3.fxKeySupplierType "
        sSQLCmd &= "WHERE t1.fxKeyCompany = '" & gCompanyID() & "'"
        If filter = "" Then
            sSQLCmd &= " and t1.fxKeySupplierType IS NULL OR t3.fcTypeName = 'Member' "
        Else
            sSQLCmd &= " and t1.fxKeySupplierType = '" & filter & "'"
        End If

        Select Case sView
            Case "Active Suppliers"
                sSQLCmd &= " AND t1.fbActive = 1 "
        End Select
        sSQLCmd &= " ORDER BY t1.fcSupplierName"
        Try
            grdSupplier.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE")
        End Try
        With grdSupplier
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns(2).Visible = False
            .Columns("fcMainAddress").Visible = False

            .Columns(4).Visible = False
            .Columns.Item(2).HeaderText = "Active"
            .Columns.Item(3).HeaderText = "Name"
            .Columns.Item(4).HeaderText = "Balance Total"
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader
            .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            If sView <> "Active Suppliers" Then
                .Columns(2).Visible = True
            End If
        End With
    End Sub

    Public Sub m_customerList(ByVal grdCustomer As DataGridView, ByVal sView As String)

        Dim sSQLCmd As String = "SELECT mCustomer00Master.fxKeyCustomer, mCustomer00Master.fxKeyAddress, mCustomer00Master.fbActive, mCustomer00Master.fcCustomerName, mCustomer00Master.fdBalance, mCustomer00Master.fbDeleted "
        sSQLCmd &= " FROM mCustomer00Master as mCustomer00Master left outer JOIN mAddress as mAddress ON mCustomer00Master.fxKeyAddress = mAddress.fxKeyAddress "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " AND fbDeleted = 0 "
        Select Case sView
            Case "Active Customers"
                sSQLCmd &= "AND mCustomer00Master.fbActive = 1 AND mCustomer00Master.fbDeleted = 0"
            Case "With Open Balances"
                sSQLCmd &= "AND mCustomer00Master.fdBalance <> 0 AND mCustomer00Master.fbDeleted = 0"
        End Select
        sSQLCmd &= "ORDER BY mCustomer00Master.fcCustomerName"
        Try
            grdCustomer.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
            With grdCustomer
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(5).Visible = False
                .Columns.Item(2).HeaderText = "Active"
                .Columns.Item(3).HeaderText = "Name"
                .Columns.Item(4).HeaderText = "Balance Total"
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader
                .Columns(2).Width = 20
                .Columns(3).Width = 150
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(4).DefaultCellStyle.Format = "##,##0.00"
                If sView <> "Active Customers" Then
                    .Columns(2).Visible = True
                End If
            End With
        Catch
            'MessageBox.Show(Err.Description, "CLICKSOFTWARE:Customer List")
        End Try

    End Sub


    Public Sub m_SalesTaxCodeList(ByVal bShowInactive As Boolean, ByVal grdSalesTxCode As DataGridView)
        Dim sSQLcmd As String = "SELECT fxKeySalesTaxCode, fbActive, fcSalesTaxCodeName, fcSalesTaxCodeDescription, fbTaxable FROM mSalesTaxCode"
        sSQLcmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        If bShowInactive = False Then
            sSQLcmd &= " AND fbActive = 1"
        End If
        sSQLcmd &= " ORDER BY fcSalesTaxCodeName"
        Try
            grdSalesTxCode.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLcmd).Tables(0)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Sales Tax Code List")
        End Try
    End Sub

    Public Sub m_SalesRepList(ByVal bShowInactive As Boolean, ByVal grdSalesRep As DataGridView)
        Dim sSQLCmd As String = "SPU_SalesRep_List @bShowInactive = " & IIf(bShowInactive = True, 1, 0)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        Try
            grdSalesRep.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Sales Rep List")
        End Try
    End Sub

    Public Sub m_chkSettings(ByVal chkInactive As CheckBox, ByVal sSQLCmd As String)
        chkInactive.Enabled = m_includeInactive(sSQLCmd)
    End Sub

    Public Sub m_OtherNameList(ByVal bShowInactive As Boolean, ByVal grdOtherName As DataGridView)
        Dim sSQLCmd As String = "SELECT fxKeyOtherName, fxKeyAddress,fbActive, fcOtherName, mOtherName00Master.fcNote "
        sSQLCmd &= " FROM mOtherName00Master INNER JOIN mAddress ON mOtherName00Master.fxKeyOtherName = mAddress.fxKeyID "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " AND fbOther = 1"
        If bShowInactive = False Then
            sSQLCmd &= " and mOtherName00Master.fbActive = 1"
        End If
        sSQLCmd &= " ORDER by fcOtherName"
        Try
            grdOtherName.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Other Name List")
        End Try
    End Sub

    'Public Sub m_loadBillingRate(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
    '    Dim cboNameTemp As New ComboBox
    '    Dim cboIDTemp As New ComboBox
    '    cboNameTemp.Items.Clear()
    '    cboNameTemp.Items.Add("")
    '    cboNameTemp.Items.Add("Create New")
    '    cboIDTemp.Items.Clear()
    '    cboIDTemp.Items.Add("")
    '    cboIDTemp.Items.Add("")
    '    Dim rd As SqlDataReader
    '    Dim sSQLCmd As String = "SELECT fxKeyBillRate, fcBillRateName "
    '    sSQLCmd &= " FROM mBillRateLevel WHERE fxKeyCompany = '" & gCompanyID() & "'"
    '    sSQLCmd &= " ORDER by fcBillRateName"
    '    Try
    '        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
    '        While rd.Read
    '            cboNameTemp.Items.Add(rd.Item("fcBillRateName"))
    '            cboIDTemp.Items.Add(rd.Item("fxKeyBillRate").ToString)
    '        End While
    '        rd.Close()
    '        Dim xCnt As Integer
    '        cboName.Items.Clear()
    '        For xCnt = 0 To cboNameTemp.Items.Count - 1
    '            cboName.Items.Add(cboNameTemp.Items(xCnt))
    '        Next
    '        cboID.Items.Clear()
    '        For xCnt = 0 To cboIDTemp.Items.Count - 1
    '            cboID.Items.Add(cboIDTemp.Items(xCnt))
    '        Next
    '    Catch ex As Exception
    '        MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Billing Rate")
    '    End Try
    'End Sub


    Public Sub m_loadBillingRate(ByVal grdOtherName As DataGridView)
        ' Dim rd As SqlDataReader
        Dim sSQLCmd As String = "SELECT  fxKeyBillRate, fcBillRateName,"
        sSQLCmd &= " CASE fbFixedRate WHEN 1 THEN 'Fixed Rate' WHEN 2 THEN 'Custom Rate' END AS RateType,"
        sSQLCmd &= " fdHourlyRate"
        sSQLCmd &= " FROM mBillRateLevel WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcBillRateName"
        Try
            grdOtherName.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Billing Rate")
        End Try
    End Sub

    Public Sub m_loadAllName(ByVal cboName As ComboBox, ByVal cboID As ComboBox, ByVal bViewAll As Boolean)
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SPU_SalesRepOption_List "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If bViewAll = True Then
            sSQLCmd &= ",@bViewAll = " & IIf(bViewAll = True, 1, 0)
        End If
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboName.Items.Add(rd.Item("fcType") & " - " & rd.Item("fcName"))
                    cboID.Items.Add(rd.Item("fxKey").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Sales Rep")
        End Try
    End Sub

    Public Sub m_loadSalesRep(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SPU_SalesRep_List @bShowInactive = 0"
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcSalesRepType") & " - " & rd.Item("fcRepName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeySalesRep").ToString)
                End While
            End Using
            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Sales Rep")
        End Try
    End Sub

    Public Sub m_loadPaymentMethod(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyPaymentMethod, fcPaymentMethodName "
        sSQLCmd &= " FROM mCustomer03PaymentMethod WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCOmpany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER BY fcPaymentMethodName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcPaymentMethodName").ToString)
                    cboIDTemp.Items.Add(rd.Item("fxKeyPaymentMethod").ToString)
                End While
            End Using
            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Payment Method")
        End Try
    End Sub

    Public Sub m_loadUnitType(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        cboName.Items.Clear()
        cboName.Items.Add("")
        Dim sSQLCmd As String = "SELECT fxKeyUnitType, fcUnitTypeName "
        sSQLCmd &= " FROM mItem03MeasurementType "
        sSQLCmd &= " ORDER by fcUnitTypeName"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboName.Items.Add(rd.Item("fcUnitTypeName"))
                    cboID.Items.Add(rd.Item("fxKeyUnitType").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Unit Type")
        End Try
    End Sub

    Public Sub m_loadUnit(ByVal cboName As ComboBox, ByVal cboID As ComboBox, ByVal sKey As String)
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "usp_m_measurement_listByType "
        sSQLCmd &= "@fxKeyUnitType = '" & sKey & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboName.Items.Add(rd.Item("fcUnitName"))
                    cboID.Items.Add(rd.Item("fxKeyUnit").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Unit of Measurement")
        End Try
    End Sub

    Public Sub m_loadUnitUsed(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")
        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyUnitUsed, fcUnitName "
        sSQLCmd &= " FROM mItme04MeasurementUsed "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcUnitName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboName.Items.Add(rd.Item("fcUnitName"))
                    cboID.Items.Add(rd.Item("fxKeyUnitUsed").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Unit Used")
        End Try
    End Sub

    Public Sub gLoadAccounts(ByVal grdView As DataGridView, _
                                  ByVal psInactive As Integer, _
                                  ByVal Mode As String, _
                                  ByVal sForm As Form)
        Dim sSql As String = "usp_m_getChartofAccounts "
        sSql &= "@fxKeyCompany = '" & gCompanyID() & "'"
        sSql &= ",@fbActive='" & psInactive & "' "
        sSql &= ",@Mode = '" & Mode & "'"

        Try
            With grdView
                .Columns.Clear()
                .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSql).Tables(0)
            End With
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub gLoadChecksandPayments(ByVal grdView As DataGridView, _
                                        ByVal psAcntID As String, _
                                        ByVal sForm As Form)
        Dim sSQLCmd As String = "usp_t_checksandpayments_load "
        sSQLCmd &= "@coid='" & gCompanyID() & "' "
        sSQLCmd &= ",@accountid='" & psAcntID & "' "
        Try
            With grdView
                .Columns.Clear()
                .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Width = 50
                .Columns(2).Width = 80
                .Columns(3).Width = 80
                .Columns(4).Width = 80
                .Columns(5).Width = 80
                .Columns(6).Width = 80

                .Columns(2).ReadOnly = True
                .Columns(3).ReadOnly = True
                .Columns(4).ReadOnly = True
                .Columns(5).ReadOnly = True
                .Columns(6).ReadOnly = True
            End With
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub gLoadDepositandCredits(ByVal grdView As DataGridView, _
                                       ByVal psAcntID As String, _
                                       ByVal sForm As Form)
        Dim sSQLCmd As String = "usp_t_depositsandcredit_load "
        sSQLCmd &= "@coid='" & gCompanyID() & "' "
        sSQLCmd &= ",@accountid='" & psAcntID & "' "
        Try
            With grdView
                .Columns.Clear()
                .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Width = 50
                .Columns(2).Width = 80
                .Columns(3).Width = 80
                .Columns(4).Width = 80
                .Columns(5).Width = 80
                .Columns(6).Width = 80

                .Columns(2).ReadOnly = True
                .Columns(3).ReadOnly = True
                .Columns(4).ReadOnly = True
                .Columns(5).ReadOnly = True
                .Columns(6).ReadOnly = True
            End With
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub m_loadShippingMethod(ByVal cboName As ComboBox, ByVal cboID As ComboBox)
        Dim cboNameTemp As New ComboBox
        Dim cboIDTemp As New ComboBox

        cboName.Items.Clear()
        cboName.Items.Add("")
        cboName.Items.Add("Create New")

        cboID.Items.Clear()
        cboID.Items.Add("")
        cboID.Items.Add("")

        Dim sSQLCmd As String = "SELECT fxKeyShippingMethod, fcShippingMethodName "
        sSQLCmd &= " FROM mShippingMethod WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcShippingMethodName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboNameTemp.Items.Add(rd.Item("fcShippingMethodName"))
                    cboIDTemp.Items.Add(rd.Item("fxKeyShippingMethod").ToString)
                End While
            End Using
            Dim xCnt As Integer
            For xCnt = 0 To cboNameTemp.Items.Count - 1
                cboName.Items.Add(cboNameTemp.Items(xCnt))
            Next
            For xCnt = 0 To cboIDTemp.Items.Count - 1
                cboID.Items.Add(cboIDTemp.Items(xCnt))
            Next

            cboName.Refresh()
            cboID.Refresh()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Shipping Method")
        End Try
    End Sub

    Public Sub m_loaddteRange(ByVal cboDateRange As ComboBox)
        With cboDateRange
            .Items.Clear()
            .Items.Add("All")
            .Items.Add("Today")
            .Items.Add("This Week")
            .Items.Add("This Week-to-Date")
            .Items.Add("This Month")
            .Items.Add("This Month-to-Date")
            .Items.Add("This Fiscal Quarter")
            .Items.Add("This Fiscal Quarter-to-Date")
            .Items.Add("This Fiscal Year")
            .Items.Add("This Fiscal Year-to-Date")
            .Items.Add("Yesterday")
            .Items.Add("Last Week")
            .Items.Add("Last Week-to-Date")
            .Items.Add("Last Month")
            .Items.Add("Last Month-to-Date")
            .Items.Add("Last Fiscal Quarter")
            .Items.Add("Last Fiscal Quarter-to-Date")
            .Items.Add("Last Fiscal Year")
            .Items.Add("Last Fiscal Year-to-Date")
            '.Items.Add("Next Week")
            '.Items.Add("Next 4 Weeks")
            '.Items.Add("Next Month")
            '.Items.Add("Next Fiscal Quarter")
            '.Items.Add("Next Fiscal Year")
            .SelectedIndex = 0
        End With

    End Sub

    Public Sub AuditTrail_Save(ByVal fcModule As String, ByVal fcDescription As String)
        Try
            Dim gCoid As New Guid(gCompanyID())
            SqlHelper.ExecuteNonQuery(gCon.cnstring, "spu_AuditTrail_Save",
                          New SqlParameter("@coid", gCoid),
                          New SqlParameter("@fdDate", Format(Now, "MM/dd/yyyy hh:mm:ss tt")),
                          New SqlParameter("@keyUser", intSysCurrentId),
                          New SqlParameter("@module", fcModule),
                          New SqlParameter("@fcDescription", fcDescription))
        Catch ex As Exception
            Dim gCoidLogin As New Guid("00000000-0000-0000-0000-000000000000")
            SqlHelper.ExecuteNonQuery(gCon.cnstring, "spu_AuditTrail_Save",
                          New SqlParameter("@coid", gCoidLogin),
                          New SqlParameter("@fdDate", Format(Now, "MM/dd/yyyy hh:mm:ss tt")),
                          New SqlParameter("@keyUser", intSysCurrentId),
                          New SqlParameter("@module", fcModule),
                          New SqlParameter("@fcDescription", fcDescription))
        End Try

    End Sub

End Module
