Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Module modMainSideBar
    Private gcon As New Clsappconfiguration
    Private Masterfile As Integer = 0
    Private Accounting As Integer = 1
    'Private CustomerMaster As Integer = 2
    Private SupplierMaster As Integer = 2
    Private Banking As Integer = 3
    Private Reports As Integer = 4
    Private SystemSettings As Integer = 5
    Private About As Integer

    Public Sub ClearAllBar()
        Dim barCount As Integer
        Dim CurrntBarIndex As Integer = 0
        Dim ctr As Integer = 0
        Dim mAbout As New ExplorerBar
        barCount = frmMain.axbar.Bars.Count '+ 1
        Do Until barCount = ctr
            frmMain.axbar.Bars.Remove(frmMain.axbar.Bars(CurrntBarIndex))
            ctr += 1
        Loop

        frmMain.axbar.Bars.Add(mAbout)
        With mAbout
            .BackColor = Color.AliceBlue
            .TitleBackColorStart = Color.SteelBlue
            .TitleBackColorEnd = Color.LightBlue
            .TitleForeColor = Color.Ivory
            .TitleForeColorHot = Color.LightCoral
            .Text = "About"
            .IsSpecial = True
            .ToolTipText = "Contains Information about the System."
            .State = ExplorerBarState.Expanded
            '.WatermarkMode = ExplorerBarWatermarkMode.Colourise
            '  .IconIndex = 4
        End With
        mSideAbout(frmMain.axbar, 0)
    End Sub
    Public Sub mainSideMenuBar(ByVal oAclBar As acclExplorerBar, ByRef oPic As PictureBox, ByRef oPic1 As PictureBox, _
                                ByRef oPic2 As PictureBox, ByRef oPic3 As PictureBox, ByRef oPic4 As PictureBox, _
                                ByRef oPic5 As PictureBox)
        Dim mMasterFile, mAccountant, mSupplier, mCustomer, mBanking, mReports, _
           mSystemSetting, mHelp, mAbout As New ExplorerBar
        Dim barCount As Integer
        Dim CurrntBarIndex As Integer = 0
        Dim ctr As Integer = 0
        barCount = frmMain.axbar.Bars.Count '+ 1
        Do Until barCount = ctr
            frmMain.axbar.Bars.Remove(frmMain.axbar.Bars(CurrntBarIndex))
            ctr += 1
        Loop

        Dim sSqlCmd As String = "SELECT * FROM dbo.tUserAccesibilitySetting WHERE fxKeyUser ='" & _
                                intSysCurrentId & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        With oAclBar
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                    While rd.Read
                        '#################### Master Files ############################
                        'If rd.Item("fcChartofAccnt") = False And rd.Item("fcItemMaster") = False And rd.Item("fcSalesTax") = False And _
                        '    rd.Item("fcSalesRep") = False And rd.Item("fcCustType") = False And rd.Item("fcSupType") = False And _
                        '    rd.Item("fcTerm") = False Then
                        'Else
                        '    .Bars.Add(mMasterFile)
                        '    With mMasterFile
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "Master Files"
                        '        .IsSpecial = True
                        '        .ToolTipText = "Contains All Master File Screens"
                        '        .State = ExplorerBarState.Collapsed
                        '        oPic.Visible = False
                        '        .Watermark = oPic.Image
                        '    End With

                        '    mSideMasterFile(oAclBar, rd.Item("fcChartofAccnt"), _
                        '                     rd.Item("fcTerm"))
                        '    'rd.Item("fcItemMaster"),rd.Item("fcSalesTax"),rd.Item("fcSalesRep"),rd.Item("fcSupType"), rd.Item("fcCustType"),
                        'End If
                        '#####################################################################
                        '######################### Accountant ################################
                        'If rd.Item("fcGenJournalEntries") = False And rd.Item("fcAccntRecon") = False And rd.Item("fcWTrialBal") = False Then
                        'Else
                        '    .Bars.Add(mAccountant)
                        '    With mAccountant
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "Accountant"
                        '        .IsSpecial = True
                        '        .State = ExplorerBarState.Expanded
                        '    End With
                        '    mSideAccounting(oAclBar, rd.Item("fcGenJournalEntries"), rd.Item("fcAccntRecon"), rd.Item("fcWTrialBal"))
                        'End If
                        '##############################################################################
                        '######################### Customer ################################
                        'If rd.Item("fcCustMaster") = False And rd.Item("fcSO") = False And rd.Item("fcInv") = False _
                        '    And rd.Item("fcSR") = False And rd.Item("fcRecvePayment") = False And rd.Item("fcCreditMmo") = False _
                        '    And rd.Item("fcDebitMmo") = False Then
                        'Else
                        '    .Bars.Add(mCustomer)
                        '    With mCustomer
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "Customer"
                        '        .IsSpecial = True
                        '        .ToolTipText = "Contains Customer Details"
                        '        .State = ExplorerBarState.Expanded
                        '        oPic1.Visible = False
                        '        .Watermark = oPic1.Image
                        '    End With
                        '    mSideCustomer(oAclBar, rd.Item("fcCustMaster"), rd.Item("fcSO"), rd.Item("fcInv"), rd.Item("fcSR"), rd.Item("fcRecvePayment"), rd.Item("fcCreditMmo"), rd.Item("fcDebitMmo"))
                        'End If
                        '##############################################################################
                        '######################### Supplier ################################
                        'If rd.Item("fcSupMaster") = False And rd.Item("fcBills") = False And rd.Item("fcBills4Approval") = False _
                        '    And rd.Item("fcPayBills") = False And rd.Item("fcPO") = False And rd.Item("fcReciveItem") = False _
                        '    And rd.Item("fcEnterBills4RcvItem") = False Then
                        'Else
                        '    .Bars.Add(mSupplier)
                        '    With mSupplier
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "Supplier"
                        '        .IsSpecial = True
                        '        .ToolTipText = "Contains Supplier Details"
                        '        .State = ExplorerBarState.Collapsed
                        '        oPic2.Visible = False
                        '        .Watermark = oPic2.Image
                        '    End With
                        '    mSideSupplier(oAclBar, rd.Item("fcSupMaster"), rd.Item("fcBills"), _
                        '                rd.Item("fcBills4Approval"), rd.Item("fcPayBills"), _
                        '                rd.Item("fcPO"), rd.Item("fcReciveItem"), _
                        '                rd.Item("fcEnterBills4RcvItem"))
                        'End If
                        '##############################################################################
                        '################################# Banking ####################################
                        'If rd.Item("fcWriteChecks") = False And rd.Item("fcBankRecon") = False And rd.Item("fcTransFunds") = False And rd.Item("fcMkeDepot") = False Then
                        'Else
                        '    .Bars.Add(mBanking)
                        '    With mBanking
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "Banking"
                        '        .IsSpecial = True
                        '        .ToolTipText = "Contains Banking Details"
                        '        .State = ExplorerBarState.Collapsed
                        '        '.WatermarkMode = ExplorerBarWatermarkMode.Colourise
                        '        '  .IconIndex = 4
                        '        oPic3.Visible = False
                        '        .Watermark = oPic3.Image
                        '    End With
                        '    mSideBanking(oAclBar, rd.Item("fcWriteChecks"), rd.Item("fcBankRecon"), rd.Item("fcTransFunds"), rd.Item("fcMkeDepot"))
                        'End If
                        '##############################################################################
                        '################################# Report ####################################
                        'If rd.Item("fcAPSummary") = False And rd.Item("fcBllsPaymentAccntRpt") = False And _
                        '    rd.Item("fcAPCVList") = False And rd.Item("fcAging") = False And rd.Item("fcBalSheet") = False And _
                        '    rd.Item("fcCVHistory") = False And rd.Item("fcCollectRpt") = False And _
                        '    rd.Item("fcFinanceCharge") = False And rd.Item("fcGLListing") = False And rd.Item("fcGLByAccnt") = False And _
                        '    rd.Item("fcIncmeSttmnt") = False And rd.Item("fcJVSummary") = False And rd.Item("fcJVSummaryByAccnt") = False And _
                        '    rd.Item("fcCustListRpt") = False And rd.Item("fcItemMasterInvntry") = False And rd.Item("fcSupMasterRpt") = False And _
                        '    rd.Item("fcReimburse") = False And rd.Item("fcSalesRpt") = False And rd.Item("fcSttmentofAccnt") = False And rd.Item("fcTaxRpt") = False And _
                        '    rd.Item("fcUndepotstdFunds") = False And rd.Item("fcBudgetMonitoringRpt") = False And rd.Item("fcCustTransactRpt") = False Then
                        'Else
                        '    .Bars.Add(mReports)
                        '    With mReports
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "Reports"
                        '        .IsSpecial = True
                        '        .ToolTipText = "Contains Accounting Reports"
                        '        .State = ExplorerBarState.Collapsed
                        '        .WatermarkMode = ExplorerBarWatermarkMode.Colourise
                        '        '  .IconIndex = 4
                        '        oPic4.Visible = False
                        '        .Watermark = oPic4.Image
                        '    End With
                        '    mSideReports(oAclBar, rd.Item("fcAPSummary"), _
                        '    rd.Item("fcBllsPaymentAccntRpt"), _
                        '    rd.Item("fcAPCVList"), _
                        '    rd.Item("fcAging"), _
                        '    rd.Item("fcBalSheet"), _
                        '    rd.Item("fcCVHistory"), _
                        '    rd.Item("fcCollectRpt"), _
                        '    rd.Item("fcFinanceCharge"), _
                        '    rd.Item("fcGLListing"), _
                        '    rd.Item("fcGLByAccnt"), _
                        '    rd.Item("fcIncmeSttmnt"), _
                        '    rd.Item("fcJVSummary"), _
                        '    rd.Item("fcJVSummaryByAccnt"), _
                        '    rd.Item("fcCustListRpt"), _
                        '    rd.Item("fcItemMasterInvntry"), _
                        '    rd.Item("fcSupMasterRpt"), _
                        '    rd.Item("fcReimburse"), _
                        '    rd.Item("fcSalesRpt"), _
                        '    rd.Item("fcSttmentofAccnt"), _
                        '    rd.Item("fcTaxRpt"), _
                        '    rd.Item("fcUndepotstdFunds"), _
                        '    rd.Item("fcBudgetMonitoringRpt"), _
                        '    rd.Item("fcCustTransactRpt"))
                        'End If
                        ''##############################################################################
                        ''################################# System Setting #############################
                        'If rd.Item("fcUserOptn") = False And rd.Item("fcChaPass") = False And _
                        'rd.Item("fcPrefference") = False And rd.Item("fcPerRestrct") = False Then
                        'Else
                        '    .Bars.Add(mSystemSetting)
                        '    With mSystemSetting
                        '        .BackColor = Color.AliceBlue
                        '        .TitleBackColorStart = Color.SteelBlue
                        '        .TitleBackColorEnd = Color.LightBlue
                        '        .TitleForeColor = Color.Ivory
                        '        .TitleForeColorHot = Color.LightCoral
                        '        .Text = "System Settings"
                        '        .IsSpecial = True
                        '        .ToolTipText = "Contains, creating user/ changing passwords."
                        '        .State = ExplorerBarState.Collapsed
                        '        '.WatermarkMode = ExplorerBarWatermarkMode.Colourise
                        '        '.IconIndex = 4
                        '        oPic5.Visible = False
                        '        .Watermark = oPic5.Image
                        '    End With
                        '    mSideSystem(oAclBar, rd.Item("fcUserOptn"), rd.Item("fcChaPass"), rd.Item("fcPrefference"), rd.Item("fcPerRestrct"))
                        'End If
                        '##############################################################################
                    End While
                End Using
            Catch ex As Exception
            End Try

            .Bars.Add(mAbout)
            With mAbout
                .BackColor = Color.AliceBlue
                .TitleBackColorStart = Color.SteelBlue
                .TitleBackColorEnd = Color.LightBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.LightCoral
                .Text = "About"
                .IsSpecial = True
                .ToolTipText = "Contains Information about the System."
                .State = ExplorerBarState.Expanded
                '.WatermarkMode = ExplorerBarWatermarkMode.Colourise
                '  .IconIndex = 4
                About = 0
                About = frmMain.axbar.Bars.Count - 1
            End With
            mSideAbout(oAclBar, About)

            'With mHelp
            '    .BackColor = Color.AliceBlue
            '    .TitleBackColorStart = Color.SteelBlue
            '    .TitleBackColorEnd = Color.LightBlue
            '    .TitleForeColor = Color.Ivory
            '    .TitleForeColorHot = Color.LightCoral
            '    .Text = "Help"
            '    .IsSpecial = True
            '    .ToolTipText = "Contains System Tutorials"
            '    .State = ExplorerBarState.Collapsed
            '    '.WatermarkMode = ExplorerBarWatermarkMode.Colourise
            '    '  .IconIndex = 4
            'End With
        End With
    End Sub
    Public Sub mSideAbout(ByVal oAclBar As acclExplorerBar, ByVal barIndex As Integer)
        Dim softwareName, copyright, warning, DateUpdate As New ExplorerBarTextItem
        With softwareName
            .Bold = False
            .Text = "UCORESoftware:" & vbNewLine & "Accounting System v3.0 "
        End With
        With copyright
            .Text = "�2012 UCore Technologies Inc." & vbNewLine & "All rights reserved."
        End With
        With warning
            .Text = "WARNING:This program is protected "
            .Text &= " by copyright and international treaties."
            .Text &= " Unauthorized distribution of this program or any"
            .Text &= " portion of it may result of civil and criminal penalties,"
            .Text &= " and will be prosecuted to the maximum extent possible under the law."

        End With
        With DateUpdate
            .Text &= " Date Modified: " & FileDateTime(Application.ExecutablePath).Date
        End With
        With frmMain.axbar 'oAclBar
            .Bars(barIndex).Items.Add(softwareName)
            .Bars(barIndex).Items.Add(copyright)
            .Bars(barIndex).Items.Add(warning)
            .Bars(barIndex).Items.Add(DateUpdate)
        End With
    End Sub
    Private Sub mSideMasterFile(ByVal oAclBar As acclExplorerBar, ByVal COAValue As Double, ByVal TermValue As Double)
        'ByVal ItemMasterValue As Double,ByVal SalesTaxValue As Double,ByVal SalesRepValue As Double, _ByVal CustTypeValue As Double, ByVal SupplierTypeValue As Double, _

        Dim mChartofAcnt, mItemMaster, mPriceLvl, mBillrate, _
            mSalesTax, mSalesRep, mCustType, mSuppType, _
            mTerms As New ExplorerBarLinkItem
        With mChartofAcnt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mchart"
            .Text = "  Chart of Accounts"
        End With
        With mItemMaster
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mitemt"
            .Text = "  Item Master"
        End With
        With mPriceLvl
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mprice"
            .Text = "  Price Level"
        End With
        With mBillrate
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mrate"
            .Text = "  Bill Rate"
        End With
        With mSalesTax
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mTax"
            .Text = "  Sales Tax"
        End With
        With mSalesRep
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mrep"
            .Text = "  Sales Rep"
        End With
        With mCustType
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mcusttype"
            .Text = "  Customer Type"
        End With
        With mSuppType
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "msuptype"
            .Text = "  Supplier Type"
        End With
        With mTerms
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mterm"
            .Text = "  Terms"
        End With

        With oAclBar
            If COAValue = True Then
                .Bars(Masterfile).Items.Add(mChartofAcnt)
            End If
            'If ItemMasterValue = True Then
            '    .Bars(Masterfile).Items.Add(mItemMaster)
            'End If
            ''.Bars(0).Items.Add(mPriceLvl)
            ''.Bars(0).Items.Add(mBillrate)
            'If SalesTaxValue = True Then
            '    .Bars(Masterfile).Items.Add(mSalesTax)
            'End If
            'If SalesRepValue = True Then
            '    .Bars(Masterfile).Items.Add(mSalesRep)
            'End If
            'If CustTypeValue = True Then
            '    .Bars(Masterfile).Items.Add(mCustType)
            'End If
            'If SupplierTypeValue = True Then
            '    .Bars(Masterfile).Items.Add(mSuppType)
            'End If
            If TermValue = True Then
                .Bars(Masterfile).Items.Add(mTerms)
            End If
        End With
    End Sub
    Private Sub mSideAccounting(ByVal oAclBar As acclExplorerBar, ByVal GenJEntryvalue As Double, _
                                ByVal ReconcileValue As Double, ByVal WTrialBalanceValue As Double)
        Dim mJournalEntry As New ExplorerBarLinkItem
        Dim mWrkngTrialblnc As New ExplorerBarLinkItem
        Dim mReconcile As New ExplorerBarLinkItem

        With mJournalEntry
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mJournalEntry"
            .Text = "General Journal Entry"
        End With

        With mWrkngTrialblnc
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mWrkngTrialblnc"
            .Text = "Working Trial Balance"
        End With

        With mReconcile
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mReconcile"
            .Text = "Bank Reconciliation"
        End With

        With oAclBar
            If GenJEntryvalue = True Then
                .Bars(Accounting).Items.Add(mJournalEntry)
            End If
            If ReconcileValue = True Then
                .Bars(Accounting).Items.Add(mReconcile)
            End If
            If WTrialBalanceValue = True Then
                .Bars(Accounting).Items.Add(mWrkngTrialblnc)
            End If
        End With

    End Sub
    Private Sub mSideCustomer(ByVal oAclBar As acclExplorerBar, ByVal CustMasterValue As Double, ByVal SOValue As Double, _
                            ByVal InvValue As Double, ByVal SRValue As Double, ByVal RecvePaymentValue As Double, _
                            ByVal CMValue As Double, ByVal DMValue As Double)
        Dim mMaster, mSalesOrder, mInvoice, mSalesRecpt, _
            mStateChgs, mStatement, mAssesFinChg, mRcvPayment, _
            mCrdtMemos, mDbtMemos As New ExplorerBarLinkItem

        With mMaster
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mCust"
            .Text = "  Customer Master"
        End With
        With mSalesOrder
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mso"
            .Text = "  Create Sales Order"
        End With
        With mInvoice
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "minv"
            .Text = "  Create Invoice"
        End With
        With mSalesRecpt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mSalesrcp"
            .Text = "  Enter Sales Receipts"
        End With
        With mStateChgs
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mscharg"
            .Text = "  Enter Statement of Charges"
        End With
        With mStatement
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mstate"
            .Text = "  Create Statements"
        End With
        With mAssesFinChg
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "massess"
            .Text = "  Assess Finance Charges"
        End With
        With mRcvPayment
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mrcvpay"
            .Text = "  Receive Payments"
        End With
        With mCrdtMemos
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mcmemo"
            .Text = "  Create Credit Memos"
        End With
        With mDbtMemos
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "DMemo"
            .Text = "  Create Debit Memos"
        End With

        'With oAclBar
        '    If CustMasterValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mMaster)
        '    End If
        '    If SOValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mSalesOrder)
        '    End If
        '    If InvValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mInvoice)
        '    End If
        '    If SRValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mSalesRecpt)
        '    End If
        '    '.Bars(1).Items.Add(mStateChgs)
        '    '.Bars(1).Items.Add(mStatement)
        '    '.Bars(1).Items.Add(mAssesFinChg)
        '    If RecvePaymentValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mRcvPayment)
        '    End If
        '    If CMValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mCrdtMemos)
        '    End If
        '    If DMValue = True Then
        '        .Bars(CustomerMaster).Items.Add(mDbtMemos)
        '    End If
        'End With
    End Sub
    Private Sub mSideSupplier(ByVal oAclBar As acclExplorerBar, ByVal SupMasterValue As Double, ByVal BillsValue As Double, _
                                ByVal Bills4ApprovalValue As Double, ByVal PayBillsValue As Double, ByVal POValue As Double, _
                                ByVal ReciveItemValue As Double, ByVal EnterBills4RcvItemValue As Double)
        Dim mMaster, mEnterBills, mPayBills, mSalesTax, _
           mPO, mRecItemBill, mRecItem, mforRecItem As New ExplorerBarLinkItem

        With mMaster
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mSupp"
            .Text = "  Supplier Master"
        End With
        With mEnterBills
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mEB"
            .Text = "  Enter Bills"
        End With
        With mPayBills
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mPB"
            .Text = "  Check Disbursement"
        End With
        With mSalesTax
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mST"
            .Text = "  Sales Tax"
        End With
        'With mPO
        '    .ForeColorHot = Color.Black
        '    .Bold = False
        '    .Tag = "mCPO"
        '    .Text = "  Create Purchase Orders"
        'End With
        With mRecItemBill
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mRIB"
            .Text = "  Received Item and Bill"
        End With
        With mRecItem
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mRI"
            .Text = "  Receive Item"
        End With
        With mforRecItem
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mERI"
            .Text = "  Enter for Received Item"
        End With

        With oAclBar
            If SupMasterValue = True Then
                .Bars(SupplierMaster).Items.Add(mMaster)
            End If
            If BillsValue = True Then
                .Bars(SupplierMaster).Items.Add(mEnterBills)
            End If
            If PayBillsValue = True Then
                .Bars(SupplierMaster).Items.Add(mPayBills)
            End If
            '.Bars(SupplierMaster).Items.Add(mSalesTax)
            If POValue = True Then
                .Bars(SupplierMaster).Items.Add(mPO)
            End If
            '.Bars(SupplierMaster).Items.Add(mRecItemBill)\
            If ReciveItemValue = True Then
                .Bars(SupplierMaster).Items.Add(mRecItem)
            End If
            If EnterBills4RcvItemValue = True Then
                .Bars(SupplierMaster).Items.Add(mforRecItem)
            End If
        End With
    End Sub
    Private Sub mSideBanking(ByVal oAclBar As acclExplorerBar, ByVal WriteChecksvalue As Double, _
                            ByVal BankReconvalue As Double, ByVal TransFundsvalue As Double, _
                            ByVal MkeDepotvalue As Double)
        Dim mWriteCheck, mMakeDeposit, mTransferFunds, _
            mReconcile As New ExplorerBarLinkItem

        With mWriteCheck
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mWC"
            .Text = "  Write Check"
        End With
        With mMakeDeposit
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mMD"
            .Text = "  Make Deposits"
        End With
        With mTransferFunds
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mTF"
            .Text = "  Transfer Funds"
        End With
        With mReconcile
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mR"
            .Text = "  Reconcile"
        End With

        With oAclBar
            If WriteChecksvalue = True Then
                .Bars(Banking).Items.Add(mWriteCheck)
            End If
            If MkeDepotvalue = True Then
                .Bars(Banking).Items.Add(mMakeDeposit)
            End If
            If TransFundsvalue = True Then
                .Bars(Banking).Items.Add(mTransferFunds)
            End If
            If BankReconvalue = True Then
                .Bars(Banking).Items.Add(mReconcile)
            End If
        End With
    End Sub
    Private Sub mSideReports(ByVal oAclBar As acclExplorerBar, ByVal APSummaryValue As Double, _
        ByVal BllsPaymentAccntRptValue As Double, ByVal APCVListValue As Double, _
        ByVal AgingValue As Double, ByVal BalSheetValue As Double, _
        ByVal CVHistoryValue As Double, ByVal CollectRptValue As Double, _
        ByVal FinanceChargeValue As Double, ByVal GLListingValue As Double, _
        ByVal GLByAccntValue As Double, ByVal IncmeSttmntValue As Double, _
        ByVal JVSummaryValue As Double, ByVal JVSummaryByAccntValue As Double, _
        ByVal CustListRptValue As Double, ByVal ItemMasterInvntryValue As Double, _
        ByVal SupMasterRptValue As Double, ByVal ReimburseValue As Double, _
        ByVal SalesRptValue As Double, ByVal SttmentofAccnt As Double, ByVal TaxRptValue As Double, _
        ByVal UndepotstdFundsValue As Double, ByVal BudgetMonitoringRptValue As Double, _
        ByVal CustTransactRptValue As Double)

        Dim mAPSummary, mBPaymentAccnt, mSalesRpt, mAPandCVList, _
            mAging, mCVHistory, mCollectionRpt, mFinanceCharge, _
            mGLListing, mGLbyAccnt, mIncmeStatement, mJVSummary, _
            mJVSumPerAccnt, mCustList, mItemList, mSupList, mReimburse, _
            msSalesRpt, mStatment, mTaxReport, mUndepositedParameter, _
            mBudgetMonitoring, mCustomerTransactionReport As New ExplorerBarLinkItem

        With mAPSummary
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mAPSummary"
            .Text = "  Accounts Payable Summary"
        End With
        With mBPaymentAccnt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mBPA"
            .Text = "  Bills Payment Accounts"
        End With
        With mSalesRpt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mSales"
            .Text = "  Sales Report"
        End With
        With mAPandCVList
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "AP and CV List"
            .Text = "  AP and CV List"
        End With
        With mAging
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mAging"
            .Text = "  Aging Report"
        End With
        With mCVHistory
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mCVHistory"
            .Text = "  Check Voucher History"
        End With
        With mCollectionRpt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mP"
            .Text = "  Collection Report"
        End With
        With mFinanceCharge
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mFC"
            .Text = "  Finance Charged"
        End With
        With mGLListing
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mGLListing"
            .Text = "  GL Transaction Listing"
        End With
        With mGLbyAccnt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mGLbyAccnt"
            .Text = "  GL Transaction Listing by Account"
        End With
        With mIncmeStatement
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mIncmeStatement"
            .Text = "  Income Statement"
        End With
        With mJVSummary
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mJVSummary"
            .Text = "  Journal Voucher Summary"
        End With
        With mJVSumPerAccnt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "JVSumPerAccnt"
            .Text = "  Journal Voucher Summary Per Account"
        End With
        With mCustList
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mCustMasterList"
            .Text = "  Customer List"
        End With
        With mItemList
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mItemList"
            .Text = "  Item Master/Inventory List"
        End With
        With mSupList
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mSupList"
            .Text = "  Supplier Master"
        End With
        With mReimburse
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mReimburse"
            .Text = "  Reimbursable Expenses"
        End With
        With msSalesRpt
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mSalesRpt"
            .Text = "  Sales Report"
        End With
        With mStatment
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mStatment"
            .Text = "  Statement of Account"
        End With
        With mTaxReport
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mTaxReport"
            .Text = "  Tax Report"
        End With
        With mUndepositedParameter
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mUndepositedParameter"
            .Text = "  Undeposited Funds"
        End With
        With mBudgetMonitoring
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mBudgetMonitoring"
            .Text = "  Budget Monitoring"
        End With
        With mCustomerTransactionReport
            .ForeColorHot = Color.Black
            .Bold = False
            .Tag = "mCustomerTransactionReport"
            .Text = "  Customer Transaction"
        End With

        With oAclBar
            If APSummaryValue = True Then
                .Bars(Reports).Items.Add(mAPSummary)
            End If
            If BllsPaymentAccntRptValue = True Then
                .Bars(Reports).Items.Add(mBPaymentAccnt)
            End If
            If APCVListValue = True Then
                .Bars(Reports).Items.Add(mAPandCVList)
            End If
            If AgingValue = True Then
                .Bars(Reports).Items.Add(mAging)
            End If
            If CVHistoryValue = True Then
                .Bars(Reports).Items.Add(mCVHistory)
            End If
            If CollectRptValue = True Then
                .Bars(Reports).Items.Add(mCollectionRpt)
            End If
            If FinanceChargeValue = True Then
                .Bars(Reports).Items.Add(mFinanceCharge)
            End If
            If GLListingValue = True Then
                .Bars(Reports).Items.Add(mGLListing)
            End If
            If GLByAccntValue = True Then
                .Bars(Reports).Items.Add(mGLbyAccnt)
            End If
            If IncmeSttmntValue = True Then
                .Bars(Reports).Items.Add(mIncmeStatement)
            End If
            If JVSummaryValue = True Then
                .Bars(Reports).Items.Add(mJVSummary)
            End If
            If JVSummaryByAccntValue = True Then
                .Bars(Reports).Items.Add(mJVSumPerAccnt)
            End If
            If CustListRptValue = True Then
                .Bars(Reports).Items.Add(mCustList)
            End If
            If ItemMasterInvntryValue = True Then
                .Bars(Reports).Items.Add(mSupList)
            End If
            If ReimburseValue = True Then
                .Bars(Reports).Items.Add(mReimburse)
            End If
            If SalesRptValue = True Then
                .Bars(Reports).Items.Add(mSalesRpt)
            End If
            If SttmentofAccnt = True Then
                .Bars(Reports).Items.Add(mStatment)
            End If
            If TaxRptValue = True Then
                .Bars(Reports).Items.Add(mTaxReport)
            End If
            If UndepotstdFundsValue = True Then
                .Bars(Reports).Items.Add(mUndepositedParameter)
            End If
            If BudgetMonitoringRptValue = True Then
                .Bars(Reports).Items.Add(mBudgetMonitoring)
            End If
            If CustTransactRptValue = True Then
                .Bars(Reports).Items.Add(mCustomerTransactionReport)
            End If
        End With
    End Sub
    Private Sub mSideSystem(ByVal oAclBar As acclExplorerBar, ByVal UserOptn As Double, ByVal ChaPass As Double, ByVal Prefference As Double, _
                            ByVal PerRestrct As Double)
        Dim mCreateUser, mUserList, mChangePassword, _
            mPreferences As New ExplorerBarLinkItem


        With mUserList
            .ForeColorHot = Color.Red
            .Bold = False
            .Tag = "mUL"
            .Text = "  User List"
        End With
        With mChangePassword
            .ForeColorHot = Color.Red
            .Bold = False
            .Tag = "mCP"
            .Text = "  Change Password"
        End With
        With mPreferences
            .ForeColorHot = Color.Red
            .Bold = False
            .Tag = "mPref"
            .Text = "  Preferences"
        End With

        With oAclBar
            If UserOptn = True Then
                .Bars(SystemSettings).Items.Add(mUserList)
            End If
            If ChaPass = True Then
                .Bars(SystemSettings).Items.Add(mChangePassword)
            End If
            If Prefference = True Then
                .Bars(SystemSettings).Items.Add(mPreferences)
            End If
        End With
    End Sub

End Module
