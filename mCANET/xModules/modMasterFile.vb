Option Explicit On
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WPM_EDCRYPT


Public Class modMasterFile

#Region "Declarations"

    Private gcon As New Clsappconfiguration
    Private gTrans As New clsTransactionFunctions
    Private gEncrypt As New WPMED
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
#End Region

#Region "MASTERFILES FUNCTIONS"

    Public Sub saveCompany(ByVal psId As String, _
                                    ByVal psName As String, _
                                    ByVal psLegal As String, _
                                    ByVal psTaxid As String, _
                                    ByVal psStreet As String, _
                                    ByVal psCity As String, _
                                    ByVal psZip As String, _
                                    ByVal psPhone As String, _
                                    ByVal psFax As String, _
                                    ByVal psEmail As String, _
                                    ByVal psSite As String, _
                                    ByVal psIndustry As String, _
                                    ByVal psOrg As String, _
                                    ByVal psFiscalYr As String, _
                                    ByVal psFiscalMth As String, _
                                    ByVal psPassword As String, _
                                    ByVal psCreatedby As String, _
                                    ByVal psClassing As String)
        SqlHelper.ExecuteNonQuery(cs, CommandType.StoredProcedure, "usp_m_company_create ",
                                        New SqlParameter("@coid", psId),
                                        New SqlParameter("@coname", psName),
                                        New SqlParameter("@colegal", psLegal),
                                        New SqlParameter("@cotaxid", psTaxid),
                                        New SqlParameter("@costreet", psStreet),
                                        New SqlParameter("@cocity", psCity),
                                        New SqlParameter("@cozip", psZip),
                                        New SqlParameter("@cophone", psPhone),
                                        New SqlParameter("@cofax", psFax),
                                        New SqlParameter("@coemail", psEmail),
                                        New SqlParameter("@cosite", psSite),
                                        New SqlParameter("@coindustry", psIndustry),
                                        New SqlParameter("@coorganize", psOrg),
                                        New SqlParameter("@cofiscalyr", psFiscalYr),
                                        New SqlParameter("@cofiscalmth", psFiscalMth),
                                        New SqlParameter("@copassword", psPassword),
                                        New SqlParameter("@createdby", psCreatedby),
                                        New SqlParameter("@ActivateClassing", psClassing))
    End Sub

    Public Sub saveCompany_AccountType(ByVal coid As String)
        SqlHelper.ExecuteNonQuery(cs, CommandType.StoredProcedure, "spu_NewProject_InsertAccountType",
                                        New SqlParameter("@coid", coid))
    End Sub

    'Public Sub CreateCompany()

    '    saveCompany(frm_MF_Company.cboconame.Text, frm_MF_Company.txtcolegal.Text, frm_MF_Company.txtcotaxid.Text, _
    '                frm_MF_Company.txtcostadd.Text, frm_MF_Company.txtcocityadd.Text, frm_MF_Company.txtcozipadd.Text, _
    '                frm_MF_Company.txtcophone.Text, frm_MF_Company.txtcofax.Text, frm_MF_Company.txtcoemail.Text, _
    '                frm_MF_Company.txtcosite.Text, frm_MF_Company.cbocoindustry.SelectedItem, frm_MF_Company.cbocororg.SelectedItem, _
    '                frm_MF_Company.cbofiscalYr.SelectedItem, frm_MF_Company.cbofiscalmonth.SelectedItem, _
    '                gEncrypt.gEncryptString(frm_MF_Company.txtadminpassword.Text), gUserName, gClassingStatus)

    'End Sub

    Public Sub saveSegment(ByVal psconame As String, _
                                ByVal pssegSeq As String, _
                                ByVal pssegSize As String, _
                                ByVal pssegSep As String, _
                                ByVal psCreatedby As String)
        Dim sSQL As String = "usp_m_segment_create "
        sSQL &= "@coid='" & frm_segment_newEdit.txtsegmentmask.Text & "' "
        sSQL &= "@coname='" & psconame & "' "
        sSQL &= ",@segSeq=" & IIf(pssegSeq = "", "NULL", "'" & pssegSeq & "'") & " "
        sSQL &= ",@segSize=" & IIf(pssegSize = "", "NULL", "'" & pssegSize & "'") & " "
        sSQL &= ",@segSep=" & IIf(pssegSep = "", "NULL", "'" & pssegSep & "'") & " "
        sSQL &= ",@createdby=" & IIf(psCreatedby = "", "NULL", "'" & psCreatedby & "'") & " "
        SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)
    End Sub

    Public Sub gSaveAcntTypefromGrd(ByVal psAcntType As String, _
                             ByVal psDescrip As String, _
                             ByVal psSegVal As String, _
                             ByRef sForm As Form)
        Try
            Dim sSQL As String = "usp_m_accountType_create "
            sSQL &= "@coid='" & gCompanyID() & "' "
            sSQL &= ",@acttype='" & psAcntType & "' "
            sSQL &= ",@descrip='" & psDescrip & "' "
            sSQL &= ",@segval='" & psSegVal & "' "
            sSQL &= ",@createdby='" & gUserName & "' "

            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub gSaveAcntType(ByVal psAcntType As String, _
                             ByVal psDescrip As String, _
                             ByVal psSegVal As String, _
                             ByRef sForm As Form)
        Try
            Dim sSQL As String = "usp_m_accountType_create "
            sSQL &= "@coid='" & gCompanyID() & "' "
            sSQL &= ",@acttype='" & psAcntType & "' "
            sSQL &= ",@descrip='" & psDescrip & "' "
            sSQL &= ",@segval='" & psSegVal & "' "
            sSQL &= ",@createdby='" & gUserName & "' "

            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)

            If MsgBox("Save successful..Do you still want to add an account type?", MsgBoxStyle.YesNo, sForm.Text) = MsgBoxResult.No Then
                sForm.Close()
            Else
                gTrans.gClearFormTxt(frm_acc_newacnttype)
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub gSaveAccountfromGrid(ByVal psAcntTypeID As String, _
                            ByVal psAcntName As String, _
                            ByVal psAcntDescription As String, _
                            ByVal psAcntCode As String, _
                            ByRef oForm As Form)
        Try
            Dim sSQL As String = "usp_m_saveaccounts_imports "
            sSQL &= " @fxKeyCompany='" & gCompanyID() & "' "
            sSQL &= ",@fxKeyAcntType='" & psAcntTypeID & "' "
            sSQL &= ",@fcAcntName='" & psAcntName & "' "
            sSQL &= ",@fcAcntDesc='" & psAcntDescription & "' "
            sSQL &= ",@fcAcntCode='" & psAcntCode & "' "
            sSQL &= ",@fuCreatedby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)
        Catch ex As Exception
            ' MessageBox.Show(Err.Description, "CLICKSOFTWARE: " + oForm.Text)
        End Try
    End Sub

    Public Sub gSaveAccount(ByVal psAcntName As String, _
                            ByVal psInactive As Integer, _
                            ByVal psAcntSub As Integer, _
                            ByVal psAcntDescription As String, _
                            ByVal psAcntNote As String, _
                            ByVal psAcntCode As String, _
                            ByRef oForm As Form)
        Try
            Dim sSQL As String = "usp_m_mAccounts_create "
            sSQL &= "@coid='" & gCompanyID() & "' "
            sSQL &= ",@acnttype='" & gAcntTypeID() & "' "
            sSQL &= ",@acntsubof=" & IIf(gAccountsID() = "", "NULL", "'" & gAccountsID() & "'") & " "
            sSQL &= ",@acntname='" & psAcntName & "' "
            sSQL &= ",@inactive='" & psInactive & "' "
            sSQL &= ",@acntsub='" & psAcntSub & "' "
            sSQL &= ",@acntdesc='" & psAcntDescription & "' "
            sSQL &= ",@acntnote='" & psAcntNote & "' "
            sSQL &= ",@acntcode='" & psAcntCode & "' "
            sSQL &= ",@acntbalance='" & gEndingBal & "' "
            sSQL &= ",@acntballess='" & gEndingBal & "' "
            sSQL &= ",@acntbaldate='" & gEndingdate & "' "
            sSQL &= ",@createdby='" & gUserName & "' "

            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)
            saveAccount_BegBudget(psAcntDescription)

            If MsgBox("Save successful..", MsgBoxStyle.Information, oForm.Text) = MsgBoxResult.Ok Then
                gLoadAccounts(frm_acc_chartaccounts.grdAccounts, 1, "", frm_acc_chartaccounts)
                gTrans.gClearFormTxt(frm_item_masterItemAddEdit)
                frm_acc_chartaccounts.grdAccounts.Columns(0).Visible = False
                gTrans.gClearGrpTxt(frm_acc_editaccount.grpEditAccount)
                gTrans.gClearGrpTxt(frm_acc_editaccount.grpOptional)
                frm_acc_editaccount.chkedit_accountinactive.Checked = False
                frm_acc_editaccount.chkedit_subaccount.Checked = False
                m_DisplayAccountsAll(frm_item_masterItemAddEdit.cboCOGs)
                gEnterEndingBalance = 0
                oForm.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + oForm.Text)
        End Try
    End Sub
    Public Sub saveAccount_BegBudget(ByVal psAcntDescription As String)
        Dim saveBudgetCommand As String = "saveAcntBudget"
        saveBudgetCommand &= " @psAcntDescription='" & psAcntDescription & "'"
        saveBudgetCommand &= ",@fcBudgetAmount='" & gBeginning_Balance & "'"
        saveBudgetCommand &= ",@fdBeginningDate='" & gBeginningDate & "'"

        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, saveBudgetCommand)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try
    End Sub


    Public Sub gClearTransaction(ByVal bCleared As Integer, _
                                    ByVal psReconcileID As String, _
                                    ByVal pstransactionID As String, _
                                    ByRef sForm As Form)
        Try
            Dim sSQL As String = "usp_t_transactions_cleared "
            sSQL &= "@coid='" & gCompanyID() & "' "
            sSQL &= ",@reconcileID='" & psReconcileID & "' "
            sSQL &= ",@transactionID='" & pstransactionID & "' "
            sSQL &= ",@cleared='" & bCleared & "' "
            sSQL &= ",@updatedby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub gDeleteAccounts(ByRef sForm As Form)
        Try
            Dim sSQL As String = "UPDATE mAccounts SET acnt_deleted ='" & 1 & "' where acnt_id ='" & gAccountID & "' "
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQL)
            MessageBox.Show("Delete successful..", "CLICKSOFTWARE:" + sForm.Text)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:" + sForm.Text)
        End Try
    End Sub

    Public Sub gLoadCboRecords(ByVal cboBox As ComboBox)

        Dim cboDatatmp As New ComboBox
        Dim sSQL As String = "select co_name from dbo.mCompany ORDER BY co_name"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    cboDatatmp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cboBox.Items.Clear()
        For xCnt = 0 To cboDatatmp.Items.Count - 1
            cboBox.Items.Add(cboDatatmp.Items(xCnt))
        Next
    End Sub

    Public Sub gLoadCboAcntType(ByRef cbobox As ComboBox)
        Dim cboDatatmp As New ComboBox
        Dim sSQL As String = "select acnt_type from dbo.mAccountType where co_id = '" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    cboDatatmp.Items.Add(UCase(rd.Item(0).ToString))
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cbobox.Items.Clear()
        For xCnt = 0 To cboDatatmp.Items.Count - 1
            cbobox.Items.Add(cboDatatmp.Items(xCnt))
        Next
    End Sub

    Public Sub gLoadCboAccounts(ByRef cbobox As ComboBox)

        Dim cboDatatmp As New ComboBox
        Dim sSQL As String = "select t1.acnt_desc from dbo.mAccounts t1 left outer join dbo.mAccountType t2"
        sSQL &= " on t2.actType_id = t1.acnt_type where t2.co_id ='" & gCompanyID() & "' and t2.acnt_type='" & gAcntType & "' "
        sSQL &= " order by t1.acnt_desc asc "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    cboDatatmp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cbobox.Items.Clear()
        For xCnt = 0 To cboDatatmp.Items.Count - 1
            cbobox.Items.Add(cboDatatmp.Items(xCnt))
        Next
    End Sub

    Public Function gLoadLblAcntDescription(ByRef psAcntType As String) As String

        Dim sVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "select [description] from dbo.mAccountType where acnt_type ='" & psAcntType & "' and co_id = '" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    sVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sDescription = sVal
        Return sDescription
    End Function

    Public Sub gCompanyEvents(ByVal myForm As Form, ByVal cboBox As ComboBox, _
                              ByVal btn1 As Button, ByVal btn2 As Button, _
                              ByVal lbltxt As Label)
        With myForm
            .Text = "Open Previous Project"
            .Size = New Size(542, 349)
        End With
        cboBox.Text = ""
        gLoadCboRecords(cboBox)
        btn1.Enabled = False
        btn1.Text = ""
        btn2.Text = "OK"
        lbltxt.Text = "Please choose a project..."
    End Sub

    Public Sub gCompanyEvents_1(ByVal myForm As Form, ByVal cboBox As ComboBox, _
                                ByVal btn1 As Button, ByVal btn2 As Button, _
                                ByVal lbltxt As Label)
        With myForm
            .Text = "New Company"
            .Size = New Size(447, 512)
        End With
        cboBox.Items.Clear()
        cboBox.Text = ""
        btn1.Enabled = True
        btn1.Text = "Save"
        btn2.Text = "Clear"
        lbltxt.Text = "Please enter company information..."
    End Sub

#End Region

End Class
