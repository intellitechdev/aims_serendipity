
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Module modDefaultValue

    Private gCon As New Clsappconfiguration
    Public Function GetUploaderDataStatus(ByVal FcDocType As String, ByVal FcEmployeeNo As String, ByVal FcLoanRef As String, ByVal FcAcntRef As String, ByVal FcAcntCode As String, ByVal FcDocnum As String, ByVal FcBank As String, ByVal FcCheck As String)
        Dim str As String
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_TransactionUploaderDataChecker",
                                          New SqlParameter("@fcDocType", FcDocType),
                                          New SqlParameter("@fcEmployeeNo", FcEmployeeNo),
                                          New SqlParameter("@fcLoanRef", FcLoanRef),
                                          New SqlParameter("@fcAccountRef", FcAcntRef),
                                          New SqlParameter("@fcAccountCode", FcAcntCode),
                                          New SqlParameter("@coid", gCompanyID()),
                                          New SqlParameter("@fcDocnum", FcDocnum),
                                          New SqlParameter("@fcBank", FcBank),
                                          New SqlParameter("@fcCheck", FcCheck))
        If rd.Read = True Then
            str = rd.Item(0).ToString
        Else
            str = ""
        End If
        Return str
    End Function
    Public Function GetMtcboShipToValue(ByVal sKeyName As String, ByVal fxKeyCompany As String) As String
        If sKeyName <> "" And fxKeyCompany <> "" Then
            Dim sSqlCmd As String
            sSqlCmd = "select [Name] from dbo.loadnames where fxKeyID = '" & sKeyName & "' and fxKeyCompany = '" & gCompanyID() & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = True Then
                        While rd.Read
                            Return rd.Item(0).ToString
                        End While
                    Else
                        If sKeyName = gCompanyID() Then
                            Return gCompanyName(0).ToString
                        End If
                    End If
                End Using
            Catch ex As Exception
                ShowErrorMessage("GetMtcboShipToValue", "modDefaultValue", ex.Message)
            End Try
        End If
    End Function

    Public Function GetValueForMTcboShippingAddress(ByVal skeyAddress As String, ByVal sKeyName As String) As String
        If skeyAddress <> "" And sKeyName <> "" Then
            If skeyAddress <> "" And sKeyName <> "" Then
                Dim sSqlCmd As String = "select fcAddressName from dbo.mAddress where fxKeyID = '" & sKeyName & "' and fxKeyAddress = '" & skeyAddress & "'"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        While rd.Read
                            Return rd.Item(0).ToString
                        End While
                    End Using
                Catch ex As Exception
                    ShowErrorMessage("GetValueForMTcboShippingAddress", "modDefaultValue", ex.Message)
                End Try
            End If
        End If
    End Function

    Public Function m_selectedValuesForMtCboNames(ByVal Mtcbo As MTGCComboBox, ByVal fxKey As String) As String
        If fxKey <> "" Then

            Dim xName As String = ""
            Dim sSqlCmd As String = "select [Name] from dbo.loadnames where fxKeyID = '" & fxKey & "' and fxKeyCompany = '" & gCompanyID() & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    While rd.Read
                        xName = rd.Item(0).ToString
                    End While
                End Using
            Catch ex As Exception
                ShowErrorMessage("m_selectedValuesForMtCboNames", "ModDefaultValue", ex.Message)
                Return Nothing
            End Try

            If xName = "" Then
                sSqlCmd = "select co_name, co_id from dbo.mCompany where co_id = '" & gCompanyID() & "'"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        While rd.Read
                            xName = rd.Item(0).ToString
                        End While
                    End Using
                Catch ex As Exception
                    ShowErrorMessage("m_selectedValuesForMtCboNames", "ModDefaultValue", ex.Message)
                    Return Nothing
                End Try
            End If

            Return xName
        End If
    End Function

    Public Function GetAccountValueForMtcbo(ByVal KeyAccount As String) As String
        Dim sSqlCmd As String = "select acnt_name from dbo.mAccounts where acnt_id = '" & KeyAccount & "' and co_id = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            ShowErrorMessage("GetAccountValueForMtcbo", "modDefaultValue", ex.Message)
        End Try
    End Function

    Public Function CheckClassingStatus(ByVal _fxKeyCompany As String)
        Dim ReturnValue As String
        Dim sSqlCmd As String = "SELECT co_Classing FROM dbo.mCompany WHERE co_id = '" & _fxKeyCompany & "'"
        Try
            ReturnValue = SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception

        End Try
        Return ReturnValue
    End Function

    Public Sub SetAccesibility()
        'If intSysCurrentId = 17 Or intSysCurrentId = 19 Then
        '################################# Client Manager ###############################
        frmMain.ms_main_supplier.Visible = UserModule(28)
        frmMain.ClientListToolStripMenuItem.Visible = UserModule(27)
        'frmMain.ClientList1ToolStripMenuItem.Visible = True
        'frmMain.ClientList2ToolStripMenuItem.Visible = True
        frmMain.AccountSetupToolStripMenuItem.Visible = UserModule(8)
        frmMain.DebitCreditToolStripMenuItem.Visible = UserModule(39)
        frmMain.AccountRegisterToolStripMenuItem.Visible = UserModule(5)
        '################################# Transaction Entry ############################
        frmMain.ms_main_accountant.Visible = UserModule(63)
        frmMain.ms_accountant_genjourentry.Visible = UserModule(62)
        frmMain.NotBalanceEntriesToolStripMenuItem.Visible = UserModule(50)
        '################################# Account Inquiry ##############################
        frmMain.ms_main_accountinquiry.Visible = UserModule(4)
        frmMain.AccountInquiryToolStripMenuItem1.Visible = UserModule(3)
        'frmMain.StatementOfAccountToolStripMenuItem1.Visible = UserModule(59)
        '################################# Books of Account ##############################
        frmMain.BooksOfAccountToolStripMenuItem.Visible = True
        ''################################# Reports #################################
        frmMain.ms_main_reports.Visible = UserModule(54)
        'frmMain.JournalToolStripMenu.Visible = UserModule(47)                         '-----journal
        frmMain.CashReceiptsJournalToolStripMenuItem.Visible = UserModule(20)
        'frmMain.CRJMonthlySummarizedToolStripMenuItem.Visible = UserModule(36)
        'frmMain.CRJMonthlySpreadsheetToolStripMenuItem.Visible = UserModule(35)
        frmMain.ByCollectorToolStripMenuItem.Visible = UserModule(23)
        frmMain.ByCashierToolStripMenuItem.Visible = UserModule(22)
        frmMain.CombinedToolStripMenuItem.Visible = UserModule(21)
        'frmMain.ByDocTypeToolStripMenuItem.Visible = UserModule(17)                   '-----disbursement
        'frmMain.DailyToolStripMenuItem.Visible = True                       '-----disbursement daily
        'frmMain.MonthlyToolStripMenuItem.Visible = True                     '-----disbursement monthly
        'frmMain.GeneralJournalToolStripMenuItem1.Visible = UserModule(44)
        'frmMain.DailyToolStripMenuItem1.Visible = True
        'frmMain.MonthlyToolStripMenuItem1.Visible = True
        'frmMain.JournalListingToolStripMenuItem.Visible = UserModule(48)
        'frmMain.PurchaseOderToolStripMenuItem.Visible = True
        'frmMain.PurchaseReturnToolStripMenuItem.Visible = True
        'frmMain.SalesReturnToolStripMenuItem.Visible = True
        frmMain.FinancialStatementsToolStripMenuItem.Visible = UserModule(43)         '-----financial statement
        frmMain.BalanceSheetToolStripMenuItem1.Visible = UserModule(11)
        frmMain.IncomeStatementToolStripMenuItem1.Visible = UserModule(60)           '-----account balance
        'frmMain.DebitAccountsToolStripMenuItem.Visible = True
        'frmMain.CreditAccountsToolStripMenuItem.Visible = True
        'frmMain.GeneralLedgerToolStripMenuItem2.Visible = UserModule(45)
        frmMain.TrialBalanceToolStripMenuItem.Visible = UserModule(64)
        frmMain.WorkingTrialBalanceToolStripMenuItem.Visible = UserModule(69)
        frmMain.TrialBalanceToolStripMenuItem2.Visible = UserModule(65)
        frmMain.CashPositionReportToolStripMenuItem.Visible = UserModule(19)
        'frmMain.CashFlowStatementToolStripMenuItem.Visible = UserModule(18)
        'frmMain.BankReconciliationToolStripMenuItem.Visible = UserModule(12)
        frmMain.AuditTrailToolStripMenuItem.Visible = UserModule(9)
        'frmMain.PESOReportsToolStripMenuItem.Visible = UserModule(53)
        '################################# Account Schedule ##############################
        frmMain.ms_main_accountschedules.Visible = UserModule(7)
        frmMain.DebitsToolStripMenuItem.Visible = UserModule(40)
        frmMain.CreditsToolStripMenuItem.Visible = UserModule(34)
        'frmMain.LoanReceivableToolStripMenuItem.Visible = UserModule(49)
        frmMain.AccountAnalysisToolStripMenuItem1.Visible = UserModule(1)
        'frmMain.UnearnedInterestLapsingToolStripMenuItem.Visible = UserModule(66)
        '################################# Billing #######################################
        frmMain.ms_main_billing.Visible = UserModule(14)
        'frmMain.BillingReportsToolStripMenuItem.Visible = UserModule(15)
        frmMain.BillingListToolStripMenuItem.Visible = UserModule(16)
        frmMain.CollectionsIntegratorToolStripMenuItem.Visible = UserModule(30)
        '################################# Set Ups #######################################
        frmMain.ms_main_masterfile.Visible = UserModule(58)
        frmMain.ChartOfAccountsToolStripMenuItem.Visible = UserModule(25)
        'frmMain.DocumentTypeSettingsToolStripMenuItem.Visible = False
        frmMain.DocumentTypeToolStripMenuItem.Visible = UserModule(42)
        frmMain.CheckIssuanceToolStripMenuItem.Visible = UserModule(26)
        '################################# Computations ##################################
        frmMain.ms_main_computation.Visible = UserModule(32)
        frmMain.SavingsInterestToolStripMenuItem.Visible = UserModule(57)
        frmMain.DividendToolStripMenuItem.Visible = UserModule(41)
        frmMain.PatronageRefundToolStripMenuItem.Visible = UserModule(51)
        frmMain.BereavementToolStripMenuItem.Visible = UserModule(13)
        '################################# Database ######################################
        'frmMain.ms_main_database.Visible = UserModule(37)
        'frmMain.BackUpDatabaseToolStripMenuItem1.Visible = UserModule(10)
        'frmMain.ResoreToolStripMenuItem.Visible = UserModule(56)
        'frmMain.DataMigrationToolStripMenuItem.Visible = UserModule(38)
        'frmMain.AccountRegisterToolStripMenuItem2.Visible = UserModule(6)
        'frmMain.AccountForwardBalanceToolStripMenuItem1.Visible = UserModule(2)
        'frmMain.GenerateTemplateToolStripMenuItem1.Visible = UserModule(46)
        'frmMain.ResetForwardBalanceToolStripMenuItem.Visible = UserModule(55)
        'frmMain.ClientUploadToolStripMenuItem.Visible = UserModule(29)
        '################################# System Settings ###############################
        frmMain.ms_main_systemsettings.Visible = UserModule(61)
        frmMain.ms_sett_userList1.Visible = UserModule(68)
        frmMain.ms_sett_createuser.Visible = UserModule(33)
        frmMain.UserAccesibilityToolStripMenuItem.Visible = UserModule(67)
        frmMain.ms_sett_changepass.Visible = UserModule(24)
        frmMain.PeriodRestrictionsToolStripMenuItem.Visible = UserModule(52)
        frmMain.CompanyProfileToolStripMenuItem.Visible = UserModule(31)
        'frmMain.ms_main_accountschedules.Visible = True

        'End If


        'Try
        '    Dim sSqlCmd As String = "SELECT * FROM dbo.tUserModules WHERE fk_UserKey ='" & _
        '                        intSysCurrentId & "' AND fk_CompanyKey ='" & gCompanyID() & "'"
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
        '        While rd.Read
        '            If rd.Item(2) = 2 And rd.Item(3) = True Then
        '                frmMain.ms_main_masterfile.Visible = True
        '            Else
        '                frmMain.ms_main_masterfile.Visible = False
        '            End If

        '        End While
        '    End Using
        'Catch ex As Exception

        'End Try
    End Sub

    Public Function UserModule(ByVal moduleID As Integer)
        Dim isAccessible As Boolean
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "spu_UserAccess_SelectModuleAccess", _
                                       New SqlParameter("@fk_ModuleKey", moduleID),
                                       New SqlParameter("@fk_UserKey", intSysCurrentId),
                                       New SqlParameter("@fk_CompanyKey", gCompanyID))
            While rd.Read = True
                isAccessible = rd("fbIsAllowed")
            End While
            rd.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            mycon.sqlconn.Close()
        End Try
        
        Return isAccessible
    End Function

    Public Function PaymentMethod()
        Dim payMethod As String
        Dim totalamount As Decimal

        Try
            For Each xRow As DataGridViewRow In frmGeneralJournalEntries.grdListofEntries.Rows
                Dim cAmount As Object = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdListofEntries, "checkAmount", xRow.Index)
                If cAmount IsNot Nothing Then
                    totalamount += cAmount
                End If
            Next
        Catch ex As Exception
            payMethod = "CASH"
            Return payMethod
        End Try

        Select Case frmGeneralJournalEntries.cboDoctype.Text
            Case "OFFICIAL RECEIPT"
                If totalamount = 0 Then
                    payMethod = "CASH"
                    Return payMethod
                Else
                    payMethod = "CHECK"
                    Return payMethod
                End If
            Case "DEPOSIT SLIP"
                If totalamount = 0 Then
                    payMethod = "CASH"
                    Return payMethod
                Else
                    payMethod = "CHECK"
                    Return payMethod
                End If
            Case Else
                payMethod = "CASH"
                Return payMethod
        End Select
    End Function

    Public Sub DisableAllAccessibility()
        frmMain.ms_main_masterfile.Visible = False
        'frmMain.ms_master_chartofaccounts.Visible = False
        'frmMain.ms_main_database.Visible = False
        frmMain.ms_main_computation.Visible = False
        frmMain.ms_main_billing.Visible = False
        frmMain.ms_main_accountinquiry.Visible = False
        frmMain.ms_master.Visible = False
        frmMain.ms_master_tax.Visible = False
        frmMain.ms_master_salesrep.Visible = False
        frmMain.ms_master_customertype.Visible = False
        frmMain.ms_master_vendortype.Visible = False
        frmMain.ms_master_terms.Visible = False
        frmMain.ClassToolStripMenuItem.Visible = False
        frmMain.ms_main_accountant.Visible = False
        frmMain.ms_accountant_genjourentry.Visible = False
        frmMain.ms_accountant_wrktrialbal.Visible = False
        'frmMain.ms_main_banking.Visible = False
        'frmMain.ms_banking_writechecks.Visible = False
        'frmMain.ms_banking_reconcile.Visible = False
        'frmMain.ms_banking_transferfunds.Visible = False
        'frmMain.ms_banking_makedeposit.Visible = False
        'frmMain.ms_main_customer.Visible = False
        'frmMain.ms_cust_custmaster.Visible = False
        'frmMain.ms_cust_createSO.Visible = False
        'frmMain.ms_cust_createInvoice.Visible = False
        'frmMain.ms_cust_enterSR.Visible = False
        'frmMain.ms_cust_recvPayment.Visible = False
        'frmMain.ms_cust_createCM.Visible = False
        'frmMain.CreateDebitMemosToolStripMenuItem.Visible = False
        frmMain.ms_main_supplier.Visible = False
        frmMain.ms_sup_supmaster.Visible = False
        frmMain.ms_sup_enterbills.Visible = False
        frmMain.ms_sup_billsapproval.Visible = False
        frmMain.ms_sup_paybills.Visible = False
        frmMain.ms_sup_createPO.Visible = False
        frmMain.ms_sup_rcveItm.Visible = False
        frmMain.ms_sup_entRcvBill.Visible = False
        frmMain.ms_main_systemsettings.Visible = False
        frmMain.ms_sett_userList1.Visible = False
        frmMain.ms_sett_changepass.Visible = False
        frmMain.ms_sett_preferences.Visible = False
        frmMain.PeriodRestrictionsToolStripMenuItem.Visible = False
        frmMain.ImportingToolStripMenuItem.Visible = False
        frmMain.ms_main_reports.Visible = False
        frmMain.AccountsPayablesToolStripMenuItem.Visible = False
        frmMain.BillsPaymentAccountsReportToolStripMenuItem.Visible = False
        frmMain.AccountsPayableListToolStripMenuItem.Visible = False
        frmMain.ToolStripMenuItem2.Visible = False
        frmMain.BalanceSheetToolStripMenuItem.Visible = False
        frmMain.ToolStripMenuItem16.Visible = False
        frmMain.CollectionReportToolStripMenuItem.Visible = False
        frmMain.ToolStripMenuItem4.Visible = False
        frmMain.GeneralLedgerToolStripMenuItem.Visible = False
        frmMain.IncomeStatementToolStripMenuItem.Visible = False
        frmMain.JournalVoucherSummary.Visible = False
        frmMain.JournalVoucherPerAccountToolStripMenu.Visible = False
        frmMain.ToolStripMenuItem6.Visible = False
        frmMain.ToolStripMenuItem7.Visible = False
        frmMain.ToolStripMenuItem8.Visible = False
        frmMain.ToolStripMenuItem9.Visible = False
        frmMain.ToolStripMenuItem10.Visible = False
        frmMain.tsSalesreport.Visible = False
        frmMain.ToolStripMenuItem11.Visible = False
        frmMain.ToolStripMenuItem3.Visible = False
        frmMain.BudgetMonitoringReportToolStripMenuItem.Visible = False
        frmMain.CostumerTransactionReportsToolStripMenuItem.Visible = False
        'frmMain.toolStripToolsOptions.Visible = False
        frmMain.ms_main_accountschedules.Visible = False
        frmMain.BooksOfAccountToolStripMenuItem.Visible = False
    End Sub
    Public Function validateAccountability(ByVal What_to_Validate As String)
        Dim sSQLCmd As String
        Dim ValidatingMsg As String = ""
        Dim RecordCount As String = "Many Record"
        Select Case What_to_Validate
            Case Is = "Invoice"
                Try
                    If gInvoiceKey <> "" Then
                        sSQLCmd = "usp_t_InvoiceAccountabilityLoad "
                        sSQLCmd &= " @fxKeyInvoice='" & gInvoiceKey & "' "
                        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
                        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                            If rd.Read Then
                                If rd.Item(0).ToString = "" Or rd.Item(0).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Debit Account Recievable."
                                End If
                                If rd.Item(1).ToString = "" Or rd.Item(1).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Tax Credit Account."
                                End If
                                If rd.Item(2).ToString = "" Or rd.Item(2).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Credit Gross Sales."
                                End If
                                If rd.Item(3).ToString = "" Or rd.Item(3).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Credit Output Tax."
                                End If
                                If rd.Item(6).ToString = "" Or rd.Item(6).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Debit COS."
                                End If
                                If rd.Item(7).ToString = "" Or rd.Item(7).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Credit Merchandise."
                                End If
                                If rd.Item(8).ToString = "" Or rd.Item(8).ToString = " " Then
                                    ValidatingMsg &= vbCr & "- Debit Sales Discount."
                                End If
                            Else
                                RecordCount = "No Record Found"
                            End If
                        End Using
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try




            Case Is = "SR"
                If gKeySR <> "" Then
                    sSQLCmd = "usp_t_SRAccountabilityLoad "
                    sSQLCmd &= " @fxKeySR='" & gKeySR & "' "
                    sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "

                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                        If rd.Read Then
                            If rd.Item(0).ToString = "" Or rd.Item(0).ToString = " " Or rd.Item(0).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Receivable Debit Account."
                            End If
                            If rd.Item(1).ToString = "" Or rd.Item(1).ToString = " " Or rd.Item(1).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Tax Credit Account."
                            End If
                            If rd.Item(2).ToString = "" Or rd.Item(2).ToString = " " Or rd.Item(2).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Credit Gross Sales."
                            End If
                            If rd.Item(3).ToString = "" Or rd.Item(3).ToString = " " Or rd.Item(3).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Credit Output Tax."
                            End If
                            If rd.Item(6).ToString = "" Or rd.Item(6).ToString = " " Or rd.Item(6).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Credit COS."
                            End If
                            If rd.Item(7).ToString = "" Or rd.Item(7).ToString = " " Or rd.Item(7).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Debit Merchandise."
                            End If
                            If rd.Item(8).ToString = "" Or rd.Item(8).ToString = " " Or rd.Item(8).ToString Is Nothing Then
                                ValidatingMsg &= vbCr & "- Debit Sales Discount."
                            End If
                        Else
                            RecordCount = "No Record Found"
                        End If
                    End Using
                End If

            Case Is = "CM"
                If gKeyCredit <> "" Then
                    sSQLCmd &= "usp_t_CreditAccountabilityLoad "
                    sSQLCmd &= " @fxKeyCredit='" & gKeyCredit & "' "
                    sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
                    Try
                        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                            If rd.Read = True Then
                                If rd.Item(0).ToString = "" Or rd.Item(0).ToString = " " Or rd.Item(0).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Receivable Credit Account."
                                End If
                                If rd.Item(1).ToString = "" Or rd.Item(1).ToString = " " Or rd.Item(1).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Tax Credit Account."
                                End If
                                If rd.Item(2).ToString = "" Or rd.Item(2).ToString = " " Or rd.Item(2).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Debit Sales Return."
                                End If
                                If rd.Item(3).ToString = "" Or rd.Item(3).ToString = " " Or rd.Item(3).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Debit Output Tax."
                                End If
                                If rd.Item(6).ToString = "" Or rd.Item(6).ToString = " " Or rd.Item(6).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Credit COS."
                                End If
                                If rd.Item(7).ToString = "" Or rd.Item(7).ToString = " " Or rd.Item(7).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Debit Merchandise."
                                End If
                                If rd.Item(8).ToString = "" Or rd.Item(8).ToString = " " Or rd.Item(8).ToString Is Nothing Then
                                    ValidatingMsg &= vbCr & "- Credit Sales Discount."
                                End If
                            Else
                                RecordCount = "No Record Found"
                            End If
                        End Using
                    Catch ex As Exception
                    End Try
                End If
        End Select

        If ValidatingMsg <> "" Then
            If RecordCount = "No Record Found" Then
                MsgBox("No accountability found!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Warning")
            Else
                MsgBox("The following accountability/ies was not detected:" & ValidatingMsg, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Warning")
            End If
            Return "Kulang"
        Else
            If RecordCount = "No Record Found" Then
                MsgBox("No accountability found!", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Warning")
                Return "Kulang"
            Else
                Return "Sapat"
            End If
        End If
    End Function
    Public Function GetAccountability(ByVal acnt_id As String)
        Dim Accountability As String = ""
        Dim sSqlCmd As String = "SELECT acnt_desc FROM dbo.mAccounts WHERE acnt_id = CAST('" & _
                                acnt_id & "' AS UNIQUEIDENTIFIER) AND co_id = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Accountability = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
        'MsgBox(Accountability)
        Return Accountability
    End Function
    Public Function GetClassName(ByVal ClassReferenceNumber As String)
        Dim DbClassName As String
        Dim Classname(0) As String
        Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE ClassRefnumber='" & ClassReferenceNumber & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DbClassName = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
        Classname = Split(DbClassName, "���")
        Return Classname(0)
    End Function
    Public Sub m_getnames(ByRef cboName As ComboBox)
        Dim cboTmp As New ComboBox
        cboTmp.Items.Clear()
        Dim sSQLCmd As String = "select [Name] from dbo.loadnames where fxKeyCompany='" & gCompanyID() & "' order by [Name] "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTmp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cboName.Items.Clear()

        For xCnt = 0 To cboTmp.Items.Count - 1
            cboName.Items.Add(cboTmp.Items(xCnt).ToString)
        Next
    End Sub

    Public Sub m_selectedCboValue(ByVal cboName As ComboBox, ByVal cboID As ComboBox, ByVal sKey As String)
        If cboName.Items.Count > 0 Then
            If Not sKey = Nothing And Not sKey = "" Then
                cboID.SelectedText = sKey
                cboID.SelectedItem = sKey
                cboName.SelectedIndex = cboID.SelectedIndex
            Else
                cboName.SelectedIndex = 0
                cboID.SelectedIndex = 0
            End If
        End If
    End Sub

    Public Function m_getAddressByID(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mAddress "
        sSQLCmd &= "WHERE fxKeyAddress = '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Address")
            Return Nothing
        End Try
    End Function

    Public Function m_getitemdetails(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "usp_m_item_load "
        sSQLCmd &= " @fxKeyItem= '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Items")
            Return Nothing
        End Try
    End Function

    Public Function m_getAddressbyPayeeID(ByVal sKey As String) As String
        Dim psadd As String = ""
        Dim sSQLCmd As String = " select fcAddress+' '+fcStreet+' '+fcCity+' '+cast(fnPostal as varchar(10)) as [Address] "
        sSQLCmd &= " from dbo.mAddress where fxKeyID='" & sKey & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    psadd = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return psadd
    End Function

    Public Function m_getAddress(ByVal sKey As String, ByVal sType As String) As DataSet
        'Dim rd As SqlDataReader
        Dim sSQLCmd As String = "USP_m_Address_InformationList "
        sSQLCmd &= "@fxKeyID = '" & sKey & "'"
        sSQLCmd &= ",@fcType = '" & sType & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Address")
            Return Nothing
        End Try
    End Function

    Public Function m_getSpecificAddress(ByVal sKey As String) As String
        Try
            Dim sValue As String = ""
            Dim sSQLCmd As String = "select isnull(fcMainAddress, '') as [Address] "
            sSQLCmd &= " from dbo.mSupplier00Master where fxKeySupplier ='" & sKey & "' "


            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue = rd.Item(0).ToString
                End While
            End Using

            Return sValue
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Address")
            Return Nothing
        End Try
    End Function

    Public Function m_billexpensevalidation(ByVal skey As String, ByRef sform As Form) As Integer
        Try
            Dim sValue As Integer = 0
            Dim sSQLCmd As String = "select count(fuCreatedby) from dbo.tBillsExpenses where fxKeyBill='" & skey & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue = rd.Item(0)
                End While
            End Using

            Return sValue
        Catch ex As Exception
            MsgBox("Bill expense validation...", MsgBoxStyle.Critical, sform.Text)
        End Try
    End Function

    Public Function m_itemreceiptvalidation(ByVal skey As String, ByRef sform As Form) As Integer
        Try
            Dim sValue As Integer = 0
            Dim sSQLCmd As String = "select count(fcSRDescription) from dbo.tSalesReceipt_item where fxKeySR='" & skey & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue = rd.Item(0)
                End While
            End Using

            Return sValue
        Catch ex As Exception
            MsgBox("Sales Receipt validation...", MsgBoxStyle.Critical, sform.Text)
        End Try
    End Function

    Public Function m_billitemvalidation(ByVal skey As String, ByRef sform As Form) As Integer
        Try
            Dim sValue As Integer = 0
            Dim sSQLCmd As String = "select count(fcDescription) from dbo.tBillsItems where fxKeyBill='" & skey & "' "
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue = rd.Item(0)
                End While
            End Using
            Return sValue
        Catch ex As Exception
            MsgBox("Bill item validation...", MsgBoxStyle.Critical, sform.Text)
        End Try
    End Function

    Public Function m_getTermsss(ByVal sKey As String) As String
        Try
            Dim sValue As String = ""
            Dim sSQLCmd As String = "select t2.fcTermsName from dbo.mSupplier00Master as t1 left outer join dbo.mTerms as t2 "
            sSQLCmd &= " on t2.fxKeyTerms = t1.fxKeyTerms where t1.fxKeySupplier ='" & sKey & "' "
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue = rd.Item(0).ToString
                End While
            End Using
            Return sValue
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Terms")
            Return Nothing
        End Try
    End Function

    Public Function m_GetCustomer(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String
        If sKey <> "" Then
            sSQLCmd = "usp_m_customer_informationList "
            sSQLCmd &= "@fxKeyCustomer = '" & sKey & "'"
        Else
            sSQLCmd = "SELECT * FROM mCustomer00Master "
            sSQLCmd &= "WHERE fxKeyCompany ='" & gCompanyID() & "'"
            sSQLCmd &= "ORDER BY fcCustomerName "
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Customer")
            Return Nothing
        End Try
    End Function

    Public Function m_GetCustomerByName(Optional ByVal sName As String = "") As String
        Dim sCustomer As String = ""

        Dim sSQLCmd As String = "SELECT fxKeyCustomer FROM mCustomer00Master "
        If sName <> "" Then
            sSQLCmd &= "WHERE fcCustomerName = '" & sName & "'"
        End If
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sCustomer = rd.Item("fxKeyCustomer").ToString
                End If
            End Using
            Return sCustomer
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Customer by Name")
            Return ""
        End Try
    End Function

    Public Function m_GetListofBills(ByVal pdDueDate As Date, ByVal pbViewall As Boolean) As DataSet
        Dim sSQLCmd As String = "usp_tBills_loadList "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fdDueDate='" & pdDueDate & "' "
        sSQLCmd &= ",@fcViewAll='" & pbViewall & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Bills")
            Return Nothing
        End Try
    End Function

    Public Function m_GetSalesDiscountRP(ByVal invoicekey As String) As Decimal
        Dim sSDAmt As Decimal = 0.0
        Dim sSQLCmd As String = "select fdSDAmt from dbo.tInvoice where fxKeyInvoice='" & invoicekey & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sSDAmt = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sSDAmt
    End Function

    Public Function m_GetRetailersDiscountRP(ByVal invoicekey As String) As Decimal
        Dim sRDAmt As Decimal = 0.0
        Dim sSQLCmd As String = "select fdRDAmt from dbo.tInvoice where fxKeyInvoice='" & invoicekey & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sRDAmt = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return sRDAmt
    End Function

    Public Function m_referencenumbervalidation(ByVal refno As String) As String
        Dim result As String = ""
        Dim sSQLCmd As String = "select fcReferenceNo from dbo.tBills where fcReferenceNo ='" & refno & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    result = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return result
    End Function

    Public Function m_GetBillsforApproval(ByVal pdFrmDate As Date, _
                                            ByVal pdToDate As Date, _
                                             Optional ByVal pbViewall As Boolean = True) As DataSet
        Dim sSQLCmd As String = "usp_tBillsList_load "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fdFrmDate='" & pdFrmDate & "' "
        sSQLCmd &= ",@fdToDate='" & pdToDate & "' "
        sSQLCmd &= ",@fcViewAll='" & pbViewall & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Bills for approval.")
            Return Nothing
        End Try
    End Function

    Public Function m_GetViewAllBills(ByVal pdFrmDate As Date, _
                                          ByVal pdToDate As Date, _
                                           Optional ByVal pbViewall As Boolean = True) As DataSet
        Dim sSQLCmd As String = "usp_tBillsList_viewall "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fdFrmDate='" & pdFrmDate & "' "
        sSQLCmd &= ",@fdToDate='" & pdToDate & "' "
        sSQLCmd &= ",@fcViewAll='" & pbViewall & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: View Bills.")
            Return Nothing
        End Try
    End Function

    Public Function m_GetInvoiceTransactions(ByVal psKeyCustomer As String) As DataSet
        Dim sSQLCmd As String = "usp_t_tgetInvoice_list "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyCustomer='" & psKeyCustomer & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: List of Invoice.")
            Return Nothing
        End Try
    End Function


    Public Sub m_deletebills(ByVal psKeyBill As String)
        Dim sSQLCmd As String = "UPDATE tBills SET fbDeleted = 1 where fxKeyBill='" & psKeyBill & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Delete successful...", "CLICKSOFTWARE: Delete Bill")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Delete Bill")
        End Try
    End Sub

    Public Function m_GetBillsforApprovalReviewed() As DataSet
        Dim sSQLCmd As String = "usp_tBillsList_loadreviewed "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Bills for approval.")
            Return Nothing
        End Try
    End Function

    Public Sub g_postbills(ByVal psBillKey As String, ByVal User As String)
        Dim sSQLCmd As String = "usp_t_tbills_posted "
        sSQLCmd &= " @fxKeyCompany ='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyBill ='" & psBillKey & "' "
        sSQLCmd &= ",@fbPosted='1' "
        sSQLCmd &= ",@User = '" & User & "'"
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            ShowErrorMessage("g_postbills", "ModDefaultVaue", ex.Message)
        End Try
    End Sub


    Public Sub g_reviewedbills(ByVal psBillKey As String, ByVal User As String)
        Dim sSQLCmd As String = "usp_t_tbills_review "
        sSQLCmd &= " @fxKeyCompany ='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyBill ='" & psBillKey & "' "
        sSQLCmd &= ",@fbPosted='1' "
        sSQLCmd &= ",@fdUser ='" & User & "'"
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            ShowErrorMessage("g_reviewedbills", "ModDefaultVaue", ex.Message)
        End Try
    End Sub


    Public Function m_GetItem(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String
        If sKey <> "" Then
            sSQLCmd = "usp_m_item_informationList "
            sSQLCmd &= "@fxKeyItem = '" & sKey & "'"
            sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "' "
        Else
            sSQLCmd = "select distinct t1.fxKeyItem, t1.fbActive, t1.fcDescription+' '+isnull(t3.fcColorDescription, '')+' '+isnull(t1.fcSizeCode, '') as [fcItemName], "
            sSQLCmd &= " isnull(t4.fcItemGroupName,'') as fcItemGroupName, ' ', 0, t1.fdUnitPrice as [fdPrice], t1.fcDescription, t1.fcUnitMeasurement as fcUnitAbbreviation, '0' as [fdOnHand], '0' as [fdQuantityOnSO] "
            sSQLCmd &= " from dbo.mItemMaster as t1 left outer join dbo.mItemCode as t2 "
            sSQLCmd &= " on t2.fcItemCode = t1.fcItemCode "
            sSQLCmd &= " left outer join dbo.mItemColor as t3 on t3.fcColorCode = t1.fcColorCode "
            sSQLCmd &= " left outer join dbo.mItem01Group as t4 on t4.fxKeyItemGroup = t1.fxKeyGroupName "
            sSQLCmd &= " WHERE t1.fxKeyCompany = '" & gCompanyID() & "'"
            sSQLCmd &= " ORDER by fcItemName"
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Item")
            Return Nothing
        End Try
    End Function

    Public Function m_GetItemMossimo() As DataSet
        Dim sSQLCmd As String
        sSQLCmd = "select fcItemCode as [fcItemName]"
        sSQLCmd &= " from dbo.mItemMaster "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Item")
            Return Nothing
        End Try
    End Function

    Public Function m_GetItemByName(ByVal sItemName As String) As String
        Dim sKeyItem As String = ""
        Dim sSQLCmd As String = "usp_t_mitem_getkey "
        sSQLCmd &= "@fcItemName = '" & sItemName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sKeyItem = rd.Item(0).ToString
                End If
            End Using
            Return sKeyItem
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Item By Name")
            Return ""
        End Try
    End Function

    Public Function m_GetPO_byPONumber(ByVal sPOnum As String) As String
        Dim sKeyItem As String = ""

        Dim sSQLCmd As String = "select fxKeyPO from dbo.tPurchaseOrder "
        sSQLCmd &= "where fcPoNo ='" & sPOnum & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sKeyItem = rd.Item("fxKeyItem").ToString
                End If
            End Using

            Return sKeyItem
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Purchase Order by P.O.#")
            Return ""
        End Try
    End Function

    Public Function m_GetPOItem(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "usp_t_purchaseOrderItem_list "
        sSQLCmd &= "@fxKeyPO = '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get P.O. Item")
            Return Nothing
        End Try
    End Function

    Public Function m_GetPO(Optional ByVal sKey As String = "", Optional ByVal sType As String = "", Optional ByVal sDateFrom As String = "", Optional ByVal sDateTo As String = "") As DataSet
        Dim sSQLCmd As String = "usp_t_purchaseOrder_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKey <> "" Then
            sSQLCmd &= ",@fxKeyPO = '" & sKey & "'"
        End If
        sSQLCmd &= ",@fdDateFrom = '" & sDateFrom & "'"
        sSQLCmd &= ",@fdDateTo = '" & sDateTo & "'"
        sSQLCmd &= ",@fxType='" & sType & "'"

        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get P.O.")
            Return Nothing
        End Try
    End Function


    Public Function m_GetBillsInformationList(Optional ByVal sDateFrom As String = "", _
                    Optional ByVal sDateTo As String = "") As DataSet

        Dim sSQLCmd As String = "usp_t_Bills_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@fdDateFrom = '" & sDateFrom & "'"
        sSQLCmd &= ",@fdDateTo = '" & sDateTo & "'"
        'sSQLCmd &= ",@TransactionType='" & sType & "'"

        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Bills Information")
            Return Nothing
        End Try
    End Function

    Public Function m_GetInventory(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "usp_t_inventory_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKey <> "" Then
            sSQLCmd &= ",@fxKeyInventory = '" & sKey & "'"
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Inventory")
            Return Nothing
        End Try
    End Function

    Public Function m_getMeasurementUsed(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "SELECT mItme04MeasurementUsed.* , mItem02Measurement.fxKeyUnitType "
        sSQLCmd &= "FROM mItme04MeasurementUsed INNEr JOIN mItem02Measurement ON "
        sSQLCmd &= "mItem02Measurement.fxKeyUnit = mItme04MeasurementUsed.fxKeyUnit "

        If sKey <> "" Then
            sSQLCmd &= "WHERE fxKeyUnitUsed = '" & sKey & "'"
        Else
            sSQLCmd &= "WHERE mItme04MeasurementUsed.fxKeyCompany = '" & gCompanyID() & "'"
        End If

        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get U/M Used")
            Return Nothing
        End Try
    End Function

    Public Function m_getMeasurementType(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mItem03MeasurementType "
        If sKey <> "" Then
            sSQLCmd &= "WHERE fxKeyUnitType = '" & sKey & "'"
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get U/M Type")
            Return Nothing
        End Try
    End Function

    Public Function m_getMeasurement(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mItem02Measurement "
        If sKey <> "" Then
            sSQLCmd &= "WHERE fxKeyUnit = '" & sKey & "'"
        End If
        sSQLCmd &= " ORDER BY fcUnitName "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get U/M")
            Return Nothing
        End Try
    End Function

    Public Function m_GetPayment(Optional ByVal sKeyPay As String = "", Optional ByVal sDateFrom As String = Nothing, Optional ByVal sDateTo As String = Nothing) As DataSet
        Dim sSQLCmd As String = "usp_t_payment_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKeyPay <> "" Then
            sSQLCmd &= ",@fxKeyPayment = '" & sKeyPay & "'"
        End If
        sSQLCmd &= ",@fdDateFrom=' " & sDateFrom & "' "
        sSQLCmd &= ",@fdDateTo=' " & sDateTo & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Payment")
            Return Nothing
        End Try
    End Function

    Public Function m_GetCreditMemos(Optional ByVal sKeyPay As String = "", Optional ByVal sDateFrom As String = Nothing, Optional ByVal sDateTo As String = Nothing) As DataSet
        Dim sSQLCmd As String = "dbo.usp_t_creditmemo_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKeyPay <> "" Then
            sSQLCmd &= ",@fxKeyPayment = '" & sKeyPay & "'"
        End If
        sSQLCmd &= ",@fdDateFrom=' " & sDateFrom & "' "
        sSQLCmd &= ",@fdDateTo=' " & sDateTo & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Credit Memo")
            Return Nothing
        End Try
    End Function

    Public Function m_GetDebitMemos(Optional ByVal skeypay As String = "", Optional ByVal sDateFrom As String = Nothing, Optional ByVal sDateTo As String = Nothing) As DataSet
        Dim sSQLCmd As String = "usp_t_debitmemo_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If skeypay <> "" Then
            sSQLCmd &= ",@fxKeyPayment = '" & skeypay & "'"
        End If
        sSQLCmd &= ",@fdDateFrom=' " & sDateFrom & "' "
        sSQLCmd &= ",@fdDateTo=' " & sDateTo & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Debit Memo")
            Return Nothing
        End Try
    End Function

    Public Function m_GetPaymentInvoice(ByVal sKey As String, Optional ByVal _
            sKeyInPayment As String = "") As DataSet
        Dim sSQLCmd As String = "usp_t_paymentInvoice_list "
        sSQLCmd &= "@fxKeyPayment = '" & sKey & "'"

        If sKeyInPayment <> "" Then
            sSQLCmd &= ",@fxKeyPaymentItem = '" & sKeyInPayment & "'"
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Payment Invoice")
            Return Nothing
        End Try
    End Function


    Public Function m_GetInvoice(Optional ByVal sKey As String = "", Optional ByVal sDateFrom As String = Nothing, Optional ByVal sDateTo As String = Nothing) As DataSet
        Dim sSQLCmd As String = "usp_t_invoice_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKey <> "" Then
            sSQLCmd &= ",@fxKeyInvoice = '" & sKey & "'"
        End If
        sSQLCmd &= ",@fdDateFrom=' " & sDateFrom & "' "
        sSQLCmd &= ",@fdDateTo=' " & sDateTo & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function m_GetInvoiceItem(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "usp_t_invoiceItem_list "
        sSQLCmd &= "@fxKeyInvoice = '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Invoice Item")
            Return Nothing
        End Try
    End Function


    Public Function m_GetCreditMemo(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "usp_t_creditMemos_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKey <> "" Then
            sSQLCmd &= ",@fxKeyCredit = '" & sKey & "'"
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Credit Memo")
            Return Nothing
        End Try
    End Function

    Public Function m_GetSO(Optional ByVal sKey As String = "", Optional ByVal sDateFrom As String = Nothing, Optional ByVal sDateTo As String = Nothing) As DataSet
        Dim sSQLCmd As String = "usp_t_salesOrder_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKey <> "" Then
            sSQLCmd &= ",@fxKeySO = '" & sKey & "'"
        End If
        sSQLCmd &= ",@fdDateFrom = '" & sDateFrom & "' "
        sSQLCmd &= ",@fdDateTo = '" & sDateTo & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get S.O.")
            Return Nothing
        End Try
    End Function

    Public Function m_GetSOItem(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "usp_t_salesOrderItem_list "
        sSQLCmd &= "@fxKeySO = '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get S.O. Item")
            Return Nothing
        End Try
    End Function

    Public Function m_GetSR(Optional ByVal sKey As String = "", Optional ByVal sDateFrom As String = Nothing, Optional ByVal sDateTo As String = Nothing) As DataSet
        Dim sSQLCmd As String = "usp_t_salesReceipt_informationList "
        sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
        If sKey <> "" Then
            sSQLCmd &= ",@fxKeySR = '" & sKey & "'"
        End If
        sSQLCmd &= ",@fdDateFrom = '" & sDateFrom & "' "
        sSQLCmd &= ",@fdDateTo = '" & sDateTo & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Sales Receipt")
            Return Nothing
        End Try
    End Function

    Public Function m_GetSRItem(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "usp_t_salesReceiptItem_list "
        sSQLCmd &= "@fxKeySR = '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Sales Receipt Item")
            Return Nothing
        End Try
    End Function

    Public Function m_GetTax(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mSalesTax "
        If sKey <> "" Then
            sSQLCmd &= "WHERE fxKeyTax = '" & sKey & "'"
        Else
            sSQLCmd &= "WHERE fxKeyCompany = '" & gCompanyID() & "' "
            sSQLCmd &= "ORDER BY fcTaxName "
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Sales Tax")
            Return Nothing
        End Try
    End Function

    Public Function m_displayitemcategory() As DataSet
        Dim sSQLCmd As String = "select fcCategoryCode as [Category Code], fcCategoryDescription as [Description] from dbo.mItemCategory where fxKeyCompany='" & gCompanyID() & "' "
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Category")
            Return Nothing
        End Try
    End Function

    Public Function m_GetSalesTaxCode(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mSalesTaxCode where fxKeyCompany='" & gCompanyID() & "' "
        If sKey <> "" Then
            sSQLCmd &= "and  fxKeySalesTaxCode = '" & sKey & "' "
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Sales Tax Code")
            Return Nothing
        End Try
    End Function

    Public Function m_getSalesTaxCodeByName(ByVal sName As String) As String
        Dim sKey As String = ""

        Dim sSQLCmd As String = "SELECT fxKeySalesTaxCode FROM mSalesTaxCode "
        sSQLCmd &= "WHERE fcSalesTaxCodeName = '" & sName & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sKey = rd.Item("fxKeySalesTaxCode").ToString
                End If
            End Using
            Return sKey
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Sales Tax Code")
            Return ""
        End Try
    End Function

    Public Function m_getTerms(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mTerms "
        sSQLCmd &= "WHERE fxKeyTerms = '" & sKey & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Terms")
            Return Nothing
        End Try
    End Function

    Public Function m_getinvoiceterms(ByVal sTermName As String, ByRef sForm As Form) As String
        Dim sSQLCmd As String = " select fxKeyTerms from dbo.mTerms where fcTermsName='" & sTermName & "' and fxKeyCompany ='" & gCompanyID() & "' "
        Dim sKeyTerms As String = ""
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sKeyTerms = rd.Item(0).ToString
                End While
            End Using
            Return sKeyTerms
        Catch ex As Exception
            MsgBox("Get Invoice Terms...", MsgBoxStyle.Information, sForm.Text)
            Return Nothing
        End Try
    End Function

    Public Function m_GetSupplier(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "select * from dbo.mSupplier00Master "
        If sKey <> "" Then
            sSQLCmd &= "WHERE fxKeySupplier ='" & sKey & "'"
        End If
        sSQLCmd &= "order by fcSupplierName"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Supplier")
            Return Nothing
        End Try
    End Function

    Public Function m_GetOtherName(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "select * from dbo.mOtherInfo00Master "
        If sKey <> "" Then
            sSQLCmd &= "WHERE fxKeyOtherInfo ='" & sKey & "'"
        End If
        sSQLCmd &= "order by fcOtherInfoName"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Others")
            Return Nothing
        End Try
    End Function

    Public Function m_GetAccount(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String = "SELECT * FROM mAccounts "
        If sKey <> "" Then
            sSQLCmd &= "Where acnt_id ='" & sKey & "' "
        End If
        sSQLCmd &= "acnt_deleted='" & 0 & "' "
        sSQLCmd &= "Order by acnt_name"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Account")
            Return Nothing
        End Try
    End Function

    Public Sub m_DisplayBankAccounts(ByRef cboAccounts As ComboBox)
        Dim cboTemp As New ComboBox
        Dim sSQLCmd As String = "Select t1.acnt_desc, t2.acnt_type from dbo.mAccounts t1 "
        sSQLCmd &= " left outer join dbo.mAccountType t2 on t2.actType_id = t1.acnt_type "
        sSQLCmd &= " where t1.co_id='" & gCompanyID() & "' and t1.acnt_deleted='" & 0 & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cboTemp.Items.Add(" ")
                While rd.Read
                    If InStr(rd.Item(0).ToString, "BANK", CompareMethod.Text) > 0 _
                       Or InStr(rd.Item(0).ToString, "bank", CompareMethod.Text) > 0 Then
                        ''FOR TRC UPDATE ONLY
                        cboTemp.Items.Add(rd.Item(0).ToString)
                        ''FOR TERRY UPDATE ONLY
                        ''   cboTemp.Items.Add("BANK" + "," + rd.Item(0).ToString)
                    End If
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cboAccounts.Items.Clear()

        For xCnt = 0 To cboTemp.Items.Count - 1
            cboAccounts.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Function get_bankaccount() As String
        Dim sSQLCmd As String = "Select t2.acnt_desc from tPaybills t1 "
        sSQLCmd &= " left outer join mAccounts t2 on t1.fxKeyPayAccnt = t2.acnt_id "
        sSQLCmd &= " left outer join tBills t3 on t3.fxKeyPayBills = t1.fxKeyPayBills "
        sSQLCmd &= " where t3.fxKeySupplier='" & gPayeeID & "' "
        Dim sVal As String = ""
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sVal = "BANK" + "," + rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sVal
    End Function

    Public Sub m_DisplayAssetAccounts(ByRef cboAccounts As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "Select t1.acnt_name, t2.acnt_type from dbo.mAccounts t1 "
        sSQLCmd &= " left outer join dbo.mAccountType t2 on t2.actType_id = t1.acnt_type "
        sSQLCmd &= " where t1.co_id='" & gCompanyID() & "' and t1.acnt_deleted='" & 0 & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cboTemp.Items.Add(" ")
                While rd.Read
                    If InStr(rd.Item(1).ToString, "BANK", CompareMethod.Text) > 0 _
                       Or InStr(rd.Item(1).ToString, "EQUITY", CompareMethod.Text) > 0 _
                       Or InStr(rd.Item(1).ToString, "ASSET", CompareMethod.Text) > 0 Then
                        cboTemp.Items.Add(rd.Item(0).ToString + " - " + rd.Item(1).ToString)
                    End If
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cboAccounts.Items.Clear()

        For xCnt = 0 To cboTemp.Items.Count - 1
            cboAccounts.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Sub m_DisplayExpenseAccounts(ByRef cboAccounts As DataGridViewComboBoxColumn)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_m_mAccountExpenseType_load "
        sSQL &= "@coid='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                'cboTemp.Items.Add("Add New")
                'cboTemp.Items.Add("-------------------------------")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString + " , " + rd.Item(1).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboAccounts.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Sub m_DisplayAccounts(ByRef cboAccounts As DataGridViewComboBoxColumn)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_m_mAccounts_load "
        sSQL &= "@coid='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                'cboTemp.Items.Add("Add New")
                'cboTemp.Items.Add("-------------------------------")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString + ", " + rd.Item(1).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboAccounts.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub


    Public Function m_tagbillsasprinted(ByVal fxKeyBill As String)
        Dim sSQL As String = "UPDATE  tBills SET fbPrinted = 1 "
        sSQL &= " where fxKeyBill='" & fxKeyBill & "' "
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQL)
        Return True
    End Function

    Public Function m_displayAPCVlisting(ByVal frmdate As Date, _
                                         ByVal enddate As Date, _
                                         ByVal choice As Integer)
        Dim sSQL As String = " usp_t_accountspayablevoucher_list "
        sSQL &= " @startDate='" & frmdate & "' "
        sSQL &= ",@endDate='" & enddate & "' "
        sSQL &= ",@fxKeyCompany='" & gCompanyID() & "' "
        sSQL &= ",@choice='" & choice & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)
    End Function

    Public Function m_displaybillsforchecks() As DataSet
        Dim sSQL As String = " usp_t_tbillspaid_load "
        sSQL &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQL &= ",@fxKeySupplier='" & gPayeeID & "' "
        ' sSQL &= ",@fcReferenceNo='" & gPayeeRefno & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)
    End Function


    Public Sub m_DisplayAccountsExpenses(ByRef cboAccounts As DataGridViewComboBoxColumn)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_m_mAccountExpenseType_load "
        sSQL &= "@coid='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                'cboTemp.Items.Add("Add New")
                'cboTemp.Items.Add("-------------------------------")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboAccounts.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Sub m_DisplayAccountsPayable(ByRef cboAccounts As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_m_mAccountPayableType_load "
        sSQL &= "@coid='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                'cboTemp.Items.Add("Add New")
                'cboTemp.Items.Add("-------------------------------")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboAccounts.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Sub m_DisplayAccountsAlltoolstrip(ByRef cboAccounts As ToolStripComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_m_mAccounts_load "
        sSQL &= "@coid='" & gCompanyID() & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboAccounts.Items.Clear()
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboAccounts.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Display accounts")
        End Try
    End Sub

    Public Sub m_DisplayAccountsAll(ByRef cboAccounts As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_m_mAccounts_load "
        sSQL &= "@coid='" & gCompanyID() & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using

            Dim xCnt As Integer
            cboAccounts.Items.Clear()

            For xCnt = 0 To cboTemp.Items.Count - 1
                cboAccounts.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Display accounts")
        End Try
    End Sub

    Public Sub m_DisplayCustomer(ByRef cboNames As DataGridViewComboBoxColumn)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "SELECT fxKeyCustomer, fcCustomerName "
        sSQLCmd &= " FROM mCustomer00Master WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by fcCustomerName"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cboTemp.Items.Add(" ")
                'cboTemp.Items.Add("-------------------------------")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(1).ToString + " - " + "Customer")
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboNames.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Function m_displaycustomerDS() As DataSet
        'Dim sSQLCmd As String = "SELECT fxKeyCustomer, fcCustomerName "
        'sSQLCmd &= " FROM mCustomer00Master WHERE fbActive = 1 "
        'sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        'sSQLCmd &= " ORDER by fcCustomerName"
        'Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

        Dim sSQLCmd As String = "SELECT fxKeyID, Name "
        sSQLCmd &= " FROM loadnames2 "
        sSQLCmd &= " WHERE fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by Name"
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Sub m_DisplayItem(ByRef cboNames As DataGridViewComboBoxColumn)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select t1.fxKeyItem, t2.fcItemDescription+' '+t1.fcCategory+' '+isnull(t3.fcColorDescription, ' ')+' '+isnull(t1.fcSizeCode, ' ') as [fcItemName] "
        sSQLCmd &= " from dbo.mItemMaster as t1 left outer join dbo.mItemCode as t2 "
        sSQLCmd &= " on t2.fcItemCode = t1.fcItemCode "
        sSQLCmd &= " left outer join dbo.mItemColor as t3 on t3.fcColorCode = t1.fcColorCode "
        sSQLCmd &= " WHERE t1.fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER by t1.fcItemCode"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                cboTemp.Items.Add(" ")
                'cboTemp.Items.Add("-------------------------------")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(1).ToString + " - " + "Item")
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboNames.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Sub m_cDisplayNames(ByRef cboNames As ComboBox, _
                             Optional ByVal bViewAll As Boolean = True)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_t_NamesDisplay "
        sSQL &= "@fxKeyCompany='" & gCompanyID() & "' "

        If bViewAll = True Then
            sSQL &= ",@bViewAll = " & IIf(bViewAll = True, 1, 0)
        End If

        Try
            cboNames.Items.Clear()
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(1).ToString + " - " + rd.Item(2).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboNames.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub

    Public Sub m_DisplayNames(ByRef cboNames As DataGridViewComboBoxColumn, _
                              Optional ByVal bViewAll As Boolean = True)
        Dim cboTemp As New ComboBox

        Dim sSQL As String = "usp_t_NamesDisplay "
        sSQL &= "@fxKeyCompany='" & gCompanyID() & "' "
        If bViewAll = True Then
            sSQL &= ",@bViewAll = " & IIf(bViewAll = True, 1, 0)
        End If

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                cboTemp.Items.Add(" ")
                While rd.Read
                    cboTemp.Items.Add(rd.Item(1).ToString + " - " + rd.Item(2).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        For xCnt = 0 To cboTemp.Items.Count - 1
            cboNames.Items.Add(cboTemp.Items(xCnt))
        Next
    End Sub
    Public Function m_DisplayAccountRef() As DataSet
        'Dim sSQL As String = "AccountREF_SearchList "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "AccountREF_SearchList")
    End Function

    Public Function m_displayAccountsDS() As DataSet
        Dim sSQL As String = "usp_m_mAccountsDS_load "
        sSQL &= "@coid='" & gCompanyID() & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)
    End Function

    Public Function m_DisplayNamesJV() As DataSet
        Dim sSQL As String = "usp_t_NamesDisplay_bytypes "
        sSQL &= "@fxKeyCompany='" & gCompanyID() & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)
    End Function

    Public Function m_DisplayNamesDS(Optional ByVal bViewAll As Boolean = True) As DataSet
        Dim sSQL As String = "usp_t_NamesDisplay "
        sSQL &= "@fxKeyCompany='" & gCompanyID() & "' "
        If bViewAll = True Then
            sSQL &= ",@bViewAll = " & IIf(bViewAll = True, 1, 0)
        End If
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)
    End Function

    Public Sub m_GetLoadCheckAndPayments(ByRef grdtmp As DataGridView, _
                                            ByVal storeproc As String)
        Dim sSQLCmd As String = storeproc
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        With grdtmp
            .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
            .Columns(0).Visible = False
        End With
    End Sub

    Public Sub m_GetCocentersList(ByRef cboTemp As ComboBox)
        Dim cbo As New ComboBox
        Dim sSQLCmd As String = " select fcCenterCode from mCoCenters where fxKeyCompany='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cbo.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cboTemp.Items.Clear()
        cboTemp.Items.Add("")
        cboTemp.Items.Add("create")

        For xCnt = 0 To cbo.Items.Count - 1
            cboTemp.Items.Add(cbo.Items(xCnt))
        Next
    End Sub

    Public Function m_getLoadCoCenters() As DataSet
        Dim sSQLCmd As String = " select fxKeyCoCenter, fcCenterCode as [Center Code], fcCenterDescription as [Description] from mCoCenters where fxKeyCompany='" & gCompanyID() & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_savecocenter(ByVal psCenterCode As String, _
                              ByVal psDescription As String, ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = " usp_m_cocenters_save "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fcCenterCode='" & psCenterCode & "' "
        sSQLCmd &= ",@fcCenterDescription='" & psDescription & "' "
        sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "save successful.."
            Return msg
        Catch ex As Exception
            MsgBox("saving co-center encounters an error...", MsgBoxStyle.Critical, sForm.Text)
            Return Nothing
        End Try
    End Function

    Public Function m_GetAccountID(ByVal sActName As String) As String
        Dim sAccount As String = ""

        Dim sSQLCmd As String = "select acnt_id from dbo.mAccounts where acnt_desc ='" & sActName & "'  and co_id='" & gCompanyID() & "' and acnt_deleted='" & 0 & "' "
        Try

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sAccount = rd.Item(0).ToString
                End If
            End Using

            Return sAccount
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Account ID")
            Return ""
        End Try
    End Function


    Public Function m_GetAccountID_byAccountCode(ByVal sActCode As String) As String
        Dim sAccount As String = ""

        Dim sSQLCmd As String = "select acnt_id from dbo.mAccounts where acnt_code ='" & sActCode & "'  and co_id='" & gCompanyID() & "' and acnt_deleted='" & 0 & "' "
        Try

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sAccount = rd.Item(0).ToString
                End If
            End Using

            Return sAccount
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Account ID")
            Return ""
        End Try
    End Function

    Public Function m_GetAccountName(ByVal sActID As String) As String
        Dim sAccount As String = ""

        Dim sSQLCmd As String = "select acnt_desc from dbo.mAccounts where acnt_id ='" & sActID & "'  and co_id='" & gCompanyID() & "' and acnt_deleted='" & 0 & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sAccount = rd.Item(0).ToString
                End If
            End Using
            Return sAccount
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Account ID")
            Return ""
        End Try
    End Function


    Public Function m_GetTermsID(ByVal psTerms As String) As String
        Dim sTermID As String = ""
        Dim sSQLCmd As String = "Select fxKeyTerms from dbo.mTerms "
        sSQLCmd &= "where fxKeyCompany='" & gCompanyID() & "' and fcTermsName='" & psTerms & "' and fbActive='" & 1 & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sTermID = rd.Item(0).ToString
                End While
            End Using
            Return sTermID
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Terms")
            Return ""
        End Try
    End Function

    Public Function m_GetAccountNameAndType(ByVal sActID As String) As String
        Dim sAccount As String = ""

        Dim sSQLCmd As String = "select mAccounts.acnt_name,mAccountType.acnt_type "
        sSQLCmd &= "from dbo.mAccounts left join mAccountType on "
        sSQLCmd &= "mAccounts.acnt_type = mAccountType.actType_id "
        sSQLCmd &= "where acnt_id ='" & sActID & "' and mAccounts.co_id='" & gCompanyID() & "' and mAccounts.acnt_deleted='" & 0 & "' "
        Try

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sAccount = rd.Item(0).ToString & " - " & rd.Item(1).ToString
                End If
            End Using

            Return sAccount
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Account Name")
            Return ""
        End Try
    End Function

    Public Function m_getname(ByVal sname As String, ByVal sType As String) As String
        Dim sNames As String = ""

        Dim sSQLCmd As String = "usp_displaynames_selected "
        sSQLCmd &= " @name='" & sname & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@type='" & sType & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sNames = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sNames
    End Function

    Public Function m_GetNameID(ByVal sName As String, ByVal NameType As String) As String
        Dim sNames As String = ""

        Dim sSQLCmd As String = "usp_t_NamesDisplay_getID "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fcName='" & sName & "' "
        sSQLCmd &= ",@fcType='" & NameType & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sNames = rd.Item(0).ToString
                End If
            End Using
            Return sNames
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Name ID by Type")
            Return ""
        End Try
    End Function

    Public Function m_GetCheckName(ByVal sName As String, ByVal NameType As String) As String
        Dim sNames As String = ""

        Dim sSQLCmd As String = " usp_t_NamesDisplay_getcheckname "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fcName='" & sName & "' "
        sSQLCmd &= ",@fcType='" & NameType & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sNames = rd.Item(0).ToString
                End If
            End Using
            Return sNames
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Name ID by Type")
            Return ""
        End Try
    End Function

    Public Function m_GetGLJournalEntries(ByVal sEntryNo As String) As DataSet
        Dim sSQLCmd As String = "usp_t_tGenJournalLedger_getEntry "
        sSQLCmd &= " @coid='" & gCompanyID() & "' "
        sSQLCmd &= ",@fiEntryNo='" & sEntryNo & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_GetAccountDetails() As DataSet
        Dim sSQLCmd As String = "usp_tAccountsTransaction_load "
        sSQLCmd &= "@fxKeyAccount='" & gKeyAccount & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function
    ''FOR TERRY UPDATE USE ONLY
    'Public Function m_GetTransactionsforWorkingBalance(ByVal pdEndDate As Date) As DataSet
    '    Dim sSQLCmd As String = "usp_workingtrialbalance "
    '    sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
    '    sSQLCmd &= ",@endDate='" & pdEndDate & "' "
    '    Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    'End Function

    ''FOR TRC UPDATE USE ONLY 
    Public Function m_GetTransactionsforWorkingBalance(ByVal pdStartDate As Date, ByVal pdEndDate As Date) As DataSet
        Dim sSQLCmd As String = "usp_workingtrialbalance_revised "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@frmDate='" & pdStartDate & "' "
        sSQLCmd &= ",@endDate='" & pdEndDate & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_getNetIncome(ByVal dFrom As Date, ByVal dEnd As Date) As Decimal
        Dim dVal As Decimal = 0.0
        Dim sSQLCmd As String = "usp_t_netincome "
        sSQLCmd &= " @frmdate='" & dFrom & "' "
        sSQLCmd &= ",@enddate='" & dEnd & "' "
        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    dVal = IIf(IsDBNull(rd.Item(0)) = True, 0, rd.Item(0))
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return Format(CDec(dVal), "##,##0.00")
    End Function

    Public Function m_GetGLJournalEntriesPerDate(ByVal dFrom As DateTime, _
                                                 ByVal dTo As DateTime) As DataSet
        Dim xAccount As String = getUsersGroup(gUserName)

        If xAccount <> 3 Then
            xAccount = gUserName
        End If

        Dim sSQLCmd As String = "usp_t_tGenJournalLedger_getperdate "
        sSQLCmd &= "@coid='" & gCompanyID() & "' "
        sSQLCmd &= ",@dtFrom='" & dFrom & "' "
        sSQLCmd &= ",@dtTo='" & dTo & "' "
        sSQLCmd &= ",@username='" & xAccount & "' "

        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_GetGLJournalEntriesforPosting(ByVal dFrom As DateTime, _
                                                    ByVal dTo As DateTime, _
                                                    ByVal bAll As Boolean, _
                                                    ByVal bPosted As Boolean, _
                                                    ByVal bCancelled As Boolean, _
                                                    ByVal docType As String) As DataSet
        Dim sSQLCmd As String = "usp_t_tGenJournalLedger_getperdate_all "
        sSQLCmd &= "@coid='" & gCompanyID() & "' "
        sSQLCmd &= ",@dtFrom='" & dFrom & "' "
        sSQLCmd &= ",@dtTo='" & dTo & "' "
        sSQLCmd &= ",@bAll = " & IIf(bAll = True, 1, 0)
        sSQLCmd &= ",@bPosted = " & IIf(bPosted = True, 1, 0)
        sSQLCmd &= ",@bCancelled = " & IIf(bCancelled = True, 1, 0)
        sSQLCmd &= ",@docType ='" & docType & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_GetCheckVoucherHistory() As DataSet
        Dim sSQLCmd As String = "usp_t_checkvoucher_history "
        sSQLCmd &= " @fxKeyCompany='" & gCompanyID() & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function m_GetIndividualJournalEntries(ByVal sEntryID As String) As DataSet
        Dim sSQLCmd As String = "usp_t_tGenJournalLedger_get "
        sSQLCmd &= "@gl_entryno='" & sEntryID & "' "
        Dim dtable As New DataTable
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Public Function defaultCheckVoucherNo() As String
        Dim sDefault As String = "1"
        Dim sInvc As String = ""
        Dim sInvcNum As String = ""
        Dim sSCLCmd As String = "select fdCVNo from dbo.tPaybills where fxKeyCompany='" & gCompanyID() & "' Order by fdCVNo"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    sInvc = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        If sInvc = "" Or sInvc = "0" Then
            sInvcNum = sDefault
        ElseIf sInvc <> "" Or sInvc <> "0" Then
            sInvcNum = CInt(sInvc) + 1
        End If

        Return sInvcNum
    End Function

    Public Function defaultPayableVoucherNo() As String
        Dim sDefault As String = "1"
        Dim sInvc As String = ""
        Dim sInvcNum As String = ""
        Dim sSCLCmd As String = "select fcAPVno from dbo.tbills where fxKeyCompany='" & gCompanyID() & "' Order by fcAPVno"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    sInvc = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        If sInvc = "" Or sInvc = "0" Then
            sInvcNum = sDefault
        ElseIf sInvc <> "" Or sInvc <> "0" Then
            sInvcNum = CInt(sInvc) + 1
        End If
        Return sInvcNum
    End Function


    Public Function defaultReferenceNo() As String
        Dim sDefault As String = "1"
        Dim sInvc As String = ""
        Dim sInvcNum As String = ""
        Dim sSCLCmd As String = "select fcReferenceNo from dbo.tbills where fxKeyCompany='" & gCompanyID() & "' Order by fcReferenceNo"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    sInvc = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        If sInvc = "" Or sInvc = "0" Then
            sInvcNum = sDefault
        ElseIf sInvc <> "" Or sInvc <> "0" Then
            sInvcNum = Int64.Parse(sInvc) + 1
        End If
        Return sInvcNum
    End Function

    Public Function m_GetActNameByNameAndType(ByVal sNameSelected As String) As String
        Dim sName As String = ""
        Dim sNameArray() As String = {""}
        Dim sNameTemp As String = ""
        Dim i As Integer = 0
        Dim iCtr As Integer = 0
        Try
            sNameArray = Split(sNameSelected, " , ")
            iCtr = UBound(sNameArray)

            If iCtr > 0 Then
                While i < iCtr
                    If sName = "" Then
                        sName = sName + sNameArray(i)
                    Else
                        sName = sName + " , " & sNameArray(i)
                    End If
                    i += 1
                End While
            End If

            Return sName
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Account Name")
            Return ""
        End Try
    End Function

    'Public Sub m_savepaybills(ByVal psKeyPayBills As String, _
    '                            ByVal psKeyPayAccount As String, _
    '                            ByVal psPayMethod As String, _
    '                            ByVal pdPayDate As Date, _
    '                            ByVal pbPrinted As Integer, _
    '                            ByVal pbACNo As Integer, _
    '                            ByVal pdDiscount As Decimal, _
    '                            ByVal pdCredit As Decimal, _
    '                            ByVal pdAmt2Pay As Decimal, _
    '                            ByVal psChkno As String, _
    '                            ByVal psCVno As Integer, _
    '                            ByVal fk_tBills As String, _
    '                            ByRef sForm As Form)
    '    Dim sSQLCmd As String = " usp_t_paybills_save "
    '    sSQLCmd &= " @fxKeyPayBills='" & psKeyPayBills & "' "
    '    sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
    '    sSQLCmd &= ",@fxKeyPayAccnt='" & psKeyPayAccount & "' "
    '    sSQLCmd &= ",@fcPayMethod='" & psPayMethod & "' "
    '    sSQLCmd &= ",@fdPaymentDate='" & pdPayDate & "' "
    '    sSQLCmd &= ",@fbPrinted ='" & pbPrinted & "' "
    '    sSQLCmd &= ",@fbAssignCheckNo ='" & pbACNo & "' "
    '    sSQLCmd &= ",@fdDiscount ='" & pdDiscount & "' "
    '    sSQLCmd &= ",@fdCredit ='" & pdCredit & "' "
    '    sSQLCmd &= ",@fdAmountToPay  ='" & pdAmt2Pay & "' "
    '    sSQLCmd &= ",@fuCreatedBy  ='" & gUserName & "' "
    '    sSQLCmd &= ",@fcChkno='" & psChkno & "' "
    '    sSQLCmd &= ",@fdCVno='" & psCVno & "' "
    '    sSQLCmd &= ",@fk_tBills='" & fk_tBills & "'"
    '    Try
    '        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
    '    Catch ex As Exception
    '        MsgBox(Err.Description, MsgBoxStyle.Critical, sForm.Text)
    '    End Try
    'End Sub

    Public Sub m_savepaybillsitems(ByVal psKeyPayBills As String, _
                                    ByVal pbPaid As Integer, _
                                    ByVal psKeyBills As String, _
                                    ByVal pdDiscAmt As Decimal, _
                                    ByVal pdCreditAmt As Decimal, _
                                    ByVal pdAmt2Pay As Decimal, _
                                    ByVal psCVNo As String, _
                                    ByRef sForm As Form)
        Dim sSQLCmd As String = " usp_t_itemspaybills_save "
        sSQLCmd &= " @fxKeyPayBills='" & psKeyPayBills & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fbPaid='" & pbPaid & "' "
        sSQLCmd &= ",@fuUpdateBy='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyBills='" & psKeyBills & "' "
        sSQLCmd &= ",@fdDiscountAmt='" & pdDiscAmt & "' "
        sSQLCmd &= ",@fdCreditAmt='" & pdCreditAmt & "' "
        sSQLCmd &= ",@fdAmttoPay='" & pdAmt2Pay & "' "
        sSQLCmd &= ",@fcCVNo=' " & psCVNo & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox("Pay Bills Items...", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_savechecksprinted(ByVal pspayeeid As String, _
                                    ByVal pschknumber As String, _
                                    ByVal pschkdate As Date, _
                                    ByVal pspayorder As String, _
                                    ByVal psChkAmt As Decimal, _
                                    ByVal psChkword As String, _
                                    ByVal pspayeeadd As String, _
                                    ByVal pspayeememo As String, _
                                    ByVal pschkmode As String, _
                                    ByVal pscashaccount As String, _
                                    ByVal pscashbalance As String)
        Dim sSQLCmd As String = " usp_t_twprinchecks_save "
        sSQLCmd &= " @payeeid='" & pspayeeid & "' "
        sSQLCmd &= ",@checknumber ='" & pschknumber & "' "
        sSQLCmd &= ",@checkdate='" & pschkdate & "' "
        sSQLCmd &= ",@payorder='" & pspayorder & "' "
        sSQLCmd &= ",@checkamt='" & psChkAmt & "' "
        sSQLCmd &= ",@checkwords='" & psChkword & "' "
        sSQLCmd &= ",@payeeadd='" & pspayeeadd & "' "
        sSQLCmd &= ",@payeememo='" & pspayeememo & "' "
        sSQLCmd &= ",@checkmode='" & pschkmode & "' "
        sSQLCmd &= ",@cashinbank='" & pscashaccount & "' "
        sSQLCmd &= ",@bankBalance ='" & pscashbalance & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fcCreatedBy='" & gUserName & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            Dim s As String = Err.Description
        End Try
    End Sub

    Public Function m_saveitemcode(ByVal psItemCode As String, _
                                ByVal psDescription As String, _
                             ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = "usp_m_itemcode_save "
        sSQLCmd &= " @fcitemcode='" & psItemCode & "' "
        sSQLCmd &= ",@fcItemDescription='" & psDescription & "' "
        sSQLCmd &= ",@fcCreatedby='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            Return msg
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function m_saveitemsize(ByVal psItemCode As String, _
                                   ByVal psDescription As String, _
                                   ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = "usp_m_itemsizecode_save "
        sSQLCmd &= " @fcitemcode='" & psItemCode & "' "
        sSQLCmd &= ",@fcItemDescription='" & psDescription & "' "
        sSQLCmd &= ",@fcCreatedby='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "save successful..."
            Return msg
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function m_saveitemcolor(ByVal psItemCode As String, _
                                ByVal psDescription As String, _
                                ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = "usp_m_itemcolorcode_save "
        sSQLCmd &= " @fcitemcode='" & psItemCode & "' "
        sSQLCmd &= ",@fcItemDescription='" & psDescription & "' "
        sSQLCmd &= ",@fcCreatedby='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "Save Successful!"
            Return msg
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function m_saveitembrand(ByVal psItemCode As String, _
                                ByVal psDescription As String, _
                                ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = "usp_m_itembrand_save "
        sSQLCmd &= " @fcitemcode='" & psItemCode & "' "
        sSQLCmd &= ",@fcItemDescription='" & psDescription & "' "
        sSQLCmd &= ",@fcCreatedby='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "save successful..."
            Return msg
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Function m_saveitemunit(ByVal psItemCode As String, _
                                ByVal psDescription As String, _
                                ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = "usp_m_itemunitcode_save "
        sSQLCmd &= " @fcitemcode='" & psItemCode & "' "
        sSQLCmd &= ",@fcItemDescription='" & psDescription & "' "
        sSQLCmd &= ",@fcCreatedby='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "save successful..."
            Return msg
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function m_saveitemcategory(ByVal psItemCode As String, _
                              ByVal psDescription As String, _
                              ByRef sForm As Form) As String
        Dim msg As String = ""
        Dim sSQLCmd As String = "usp_m_itemcategory_save "
        sSQLCmd &= " @fcitemcode='" & psItemCode & "' "
        sSQLCmd &= ",@fcItemDescription='" & psDescription & "' "
        sSQLCmd &= ",@fcCreatedby='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "save successful..."
            Return msg
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub m_saveItemMaster(ByVal ItemCode As String, _
                                ByVal Description As String, _
                                ByVal Department As String, _
                                ByVal Category As String, _
                                ByVal SubCategory As String, _
                                ByVal Style As String, _
                                ByVal SizeCode As String, _
                                ByVal ColorCode As String, _
                                ByVal UnitPrice As Decimal)

        Dim sSQLCmd As String = "usp_m_ItemMasterSave "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyGroupName='F8AE8E36-09E3-48F5-983E-DD1030506AA0' " 'has to replace this
        sSQLCmd &= ",@fcYear='" & DatePart(DateInterval.Year, Now()) & "' "
        sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
        sSQLCmd &= ",@fcItemCode='" & ItemCode & "' "
        sSQLCmd &= ",@fcDescription='" & Description & "' "
        sSQLCmd &= ",@fcBrandDepartment='" & Department & "' "
        sSQLCmd &= ",@fcCategory='" & Category & "' "
        sSQLCmd &= ",@fcSubCategory='" & SubCategory & "' "
        sSQLCmd &= ",@fcModelStyle='" & Style & "' "
        sSQLCmd &= ",@fcSizeCode='" & SizeCode & "' "
        sSQLCmd &= ",@fcColorCode='" & ColorCode & "' "
        sSQLCmd &= ",@fdUnitPrice='" & UnitPrice & "' "

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Item Master")
        End Try
    End Sub

    Public Function m_GetBalanceAmount(ByVal psCode As String) As String
        Dim sVal As String = "0.00"

        Dim sSQLCmd As String = "select acnt_balance_less from dbo.mAccounts "
        sSQLCmd &= "where acnt_desc ='" & psCode & "' and co_id='" & gCompanyID() & "' and acnt_deleted='" & 0 & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sVal
    End Function

    Public Function m_GetAccountBalanceAmount(ByVal psAccountID As String) As String
        Dim sVal As String = "0.00"

        Dim sSQLCmd As String = "select acnt_balance_less from dbo.mAccounts "
        sSQLCmd &= "where acnt_id ='" & psAccountID & "' "

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
            While rd.Read
                sVal = rd.Item(0).ToString
            End While
        End Using

        Return sVal
    End Function

    Public Function m_GetCheck(Optional ByVal pkWriteCheck As String = Nothing) As DataSet
        Dim sSQLCmd As String = "usp_t_writecheck_Get "
        sSQLCmd &= "@compID='" & gCompanyID() & "' "

        If pkWriteCheck IsNot Nothing Then
            sSQLCmd &= ",@pk_WriteCheck='" & pkWriteCheck & "' "
        End If

        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Checks")
            Return Nothing
        End Try
    End Function

    Public Sub m_deleteInvoiceItem(ByVal psKeyInvoiceItem As String, ByRef sForm As Form)
        Dim sSQLCmd As String = " usp_t_tinvoiceitem_delete "
        sSQLCmd &= " @fxKeyInvoiceItem='" & psKeyInvoiceItem & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox("Error in deleting Invoice Item: Please report this to your admin.....", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_deleteCreditItem(ByVal psKeyCreditItem As String, ByRef sForm As Form)
        Dim sSQLCmd As String = " usp_t_creditmemoitem_delete "
        sSQLCmd &= " @fxKeyCreditItem='" & psKeyCreditItem & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox("Error in deleting Invoice Item: Please report this to your admin.....", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub m_deleteReceiptItem(ByVal psKeyReceiptItem As String, ByRef sForm As Form)
        Dim sSQLCmd As String = " usp_t_treceiptitem_delete "
        sSQLCmd &= "@fxKeySRItem='" & psKeyReceiptItem & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox("Error in deleting Receipt Item: Please report this to your admin.....", MsgBoxStyle.Critical, sForm.Text)
        End Try
    End Sub

    Public Sub getDateRange(ByVal lbl As Label, ByVal dt As ComboBox)
        Dim CurrentMonth As Integer = Now().Month
        Dim CurrentDay As Integer = Now().Day
        Dim CurrentYear As Integer = Now.Year
        Dim CurrentDate As Date = FormatDateTime(DateAdd(DateInterval.Day, 1, Now), DateFormat.ShortDate)
        CurrentDate = DateAdd(DateInterval.Second, -1, CurrentDate)
        Dim LastYear As Integer = CurrentYear - 1
        Dim LastMonth As Integer = DateAdd(DateInterval.Month, -1, Now).Month
        Dim NextYear As Integer = CurrentYear + 1
        Dim FirstDayNextMonth As Date = FormatDateTime(DateSerial(CurrentYear, CurrentMonth + 1, 1), DateFormat.ShortDate)
        Dim FirstDayNextNextMonth As Date = FormatDateTime(DateSerial(CurrentYear, CurrentMonth + 2, 1), DateFormat.ShortDate)
        Dim LastDayNextMonth As Date = FormatDateTime(DateAdd("d", -1, FirstDayNextNextMonth), DateFormat.ShortDate)
        Dim LastDayCurrentMonth As Date = DateAdd(DateInterval.Second, -1, FirstDayNextMonth)
        Dim FirstDateCurrentMonth As Date = FormatDateTime(DateSerial(CurrentYear, CurrentMonth, 1), DateFormat.ShortDate)
        'Dim FirstDateLastMonth As Date = FormatDateTime(DateSerial(CurrentYear, LastMonth, 1), DateFormat.ShortDate)
        Dim FirstDateLastMonth As Date = DateAdd(DateInterval.Month, -1, FirstDateCurrentMonth)
        Dim LastDateLastMonth As Date = DateAdd(DateInterval.Second, -1, FirstDateCurrentMonth)
        Dim FirstDateLastYear As Date = FormatDateTime(DateSerial(CurrentYear - 1, 1, 1), DateFormat.ShortDate)
        Dim LastDateLastYear As Date = DateAdd(DateInterval.Second, -1, DateSerial(CurrentYear, 1, 1))
        Dim FirstDateCurrentYear As Date = FormatDateTime(DateSerial(CurrentYear, 1, 1), DateFormat.ShortDate)
        Dim LastDateCurrentYear As Date = DateAdd(DateInterval.Second, -1, DateSerial(CurrentYear + 1, 1, 1))
        Dim FirstDateNextYear As Date = FormatDateTime(DateSerial(CurrentYear + 1, 1, 1), DateFormat.ShortDate)
        Dim LastDateNextYear As Date = FormatDateTime(DateSerial(CurrentYear + 1, 12, 31), DateFormat.ShortDate)

        Dim FirstDateCurrentWeek As Date

        If CurrentDate.DayOfWeek = DayOfWeek.Sunday Then
            FirstDateCurrentWeek = CurrentDate
        Else
            Select Case CurrentDate.DayOfWeek
                Case DayOfWeek.Monday
                    FirstDateCurrentWeek = FormatDateTime(DateAdd(DateInterval.Day, -1, CurrentDate), DateFormat.ShortDate)
                Case DayOfWeek.Tuesday
                    FirstDateCurrentWeek = FormatDateTime(DateAdd(DateInterval.Day, -2, CurrentDate), DateFormat.ShortDate)
                Case DayOfWeek.Wednesday
                    FirstDateCurrentWeek = FormatDateTime(DateAdd(DateInterval.Day, -3, CurrentDate), DateFormat.ShortDate)
                Case DayOfWeek.Thursday
                    FirstDateCurrentWeek = FormatDateTime(DateAdd(DateInterval.Day, -4, CurrentDate), DateFormat.ShortDate)
                Case DayOfWeek.Friday
                    FirstDateCurrentWeek = FormatDateTime(DateAdd(DateInterval.Day, -5, CurrentDate), DateFormat.ShortDate)
                Case DayOfWeek.Saturday
                    FirstDateCurrentWeek = FormatDateTime(DateAdd(DateInterval.Day, -6, CurrentDate), DateFormat.ShortDate)
            End Select
        End If

        Dim LastDateCurrentWeek As Date = FormatDateTime(DateAdd(DateInterval.Day, 6, FirstDateCurrentWeek), DateFormat.ShortDate)
        Dim FirstDateLastWeek = FormatDateTime(DateAdd(DateInterval.Day, -7, FirstDateCurrentWeek), DateFormat.ShortDate)
        Dim LastDateLastweek = FormatDateTime(DateAdd(DateInterval.Day, -7, LastDateCurrentWeek), DateFormat.ShortDate)
        Dim FirstDateNextWeek = FormatDateTime(DateAdd(DateInterval.Day, 7, FirstDateCurrentWeek), DateFormat.ShortDate)
        Dim LastDateNextWeek = FormatDateTime(DateAdd(DateInterval.Day, 7, LastDateCurrentWeek), DateFormat.ShortDate)
        Dim Next4Weeks = FormatDateTime(DateAdd(DateInterval.Day, 28, FirstDateCurrentWeek), DateFormat.ShortDate)
        Dim FirstDateCurrentQuarter As Date
        Dim LastDateCurrentQuarter As Date
        Dim FirstDateLastQuarter As Date
        Dim LastDateLastQuarter As Date
        Dim FirstDateNextQuarter As Date
        Dim LastDateNextQuarter As Date

        Select Case CurrentMonth
            Case 1, 2, 3
                FirstDateCurrentQuarter = DateSerial(CurrentYear, 1, 1)
                LastDateCurrentQuarter = DateSerial(CurrentYear, 3, 31)
                FirstDateLastQuarter = DateSerial(LastYear, 10, 1)
                LastDateLastQuarter = DateSerial(LastYear, 12, 31)
                FirstDateNextQuarter = DateSerial(CurrentYear, 4, 1)
                LastDateNextQuarter = DateSerial(CurrentYear, 6, 30)
            Case 4, 5, 6
                FirstDateCurrentQuarter = DateSerial(CurrentYear, 4, 1)
                LastDateCurrentQuarter = DateSerial(CurrentYear, 6, 30)
                FirstDateLastQuarter = DateSerial(CurrentYear, 1, 1)
                LastDateLastQuarter = DateSerial(CurrentYear, 3, 31)
                FirstDateNextQuarter = DateSerial(CurrentYear, 7, 1)
                LastDateNextQuarter = DateSerial(CurrentYear, 9, 30)
            Case 7, 8, 9
                FirstDateCurrentQuarter = DateSerial(CurrentYear, 7, 1)
                LastDateCurrentQuarter = DateSerial(CurrentYear, 9, 30)
                FirstDateLastQuarter = DateSerial(CurrentYear, 4, 1)
                LastDateLastQuarter = DateSerial(CurrentYear, 6, 30)
                FirstDateNextQuarter = DateSerial(CurrentYear, 10, 1)
                LastDateNextQuarter = DateSerial(CurrentYear, 12, 31)
            Case 10, 11, 12
                FirstDateCurrentQuarter = DateSerial(CurrentYear, 10, 1)
                LastDateCurrentQuarter = DateSerial(CurrentYear, 12, 31)
                FirstDateLastQuarter = DateSerial(CurrentYear, 7, 1)
                LastDateLastQuarter = DateSerial(CurrentYear, 9, 30)
                FirstDateNextQuarter = DateSerial(NextYear, 1, 1)
                LastDateNextQuarter = DateSerial(NextYear, 3, 31)
            Case Else
        End Select

        FirstDateCurrentQuarter = FormatDateTime(FirstDateCurrentQuarter, DateFormat.ShortDate)
        LastDateCurrentQuarter = DateAdd(DateInterval.Day, 1, LastDateCurrentQuarter)
        LastDateCurrentQuarter = DateAdd(DateInterval.Second, -1, LastDateCurrentQuarter)
        FirstDateLastQuarter = FormatDateTime(FirstDateLastQuarter, DateFormat.ShortDate)
        LastDateLastQuarter = DateAdd(DateInterval.Day, 1, LastDateLastQuarter)
        LastDateLastQuarter = DateAdd(DateInterval.Second, -1, LastDateLastQuarter)
        FirstDateNextQuarter = FormatDateTime(FirstDateNextQuarter, DateFormat.ShortDate)
        LastDateNextQuarter = FormatDateTime(LastDateNextQuarter, DateFormat.ShortDate)

        Select Case dt.SelectedItem

            Case "All"
                lbl.Text = ""
            Case "Today"
                lbl.Text = FormatDateTime(Now, DateFormat.ShortDate)
            Case "This Week"
                lbl.Text = FirstDateCurrentWeek & " - " & LastDateCurrentWeek
            Case "This Week-to-Date"
                lbl.Text = FirstDateCurrentWeek & " - " & CurrentDate
            Case "This Month"
                lbl.Text = FirstDateCurrentMonth & " - " & LastDayCurrentMonth
            Case "This Month-to-Date"
                lbl.Text = FirstDateCurrentMonth & " - " & CurrentDate
            Case "This Fiscal Quarter"
                lbl.Text = FirstDateCurrentQuarter & " - " & LastDateCurrentQuarter
            Case "This Fiscal Quarter-to-Date"
                lbl.Text = FirstDateCurrentQuarter & " - " & CurrentDate
            Case "This Fiscal Year"
                lbl.Text = FirstDateCurrentYear & " - " & LastDateCurrentYear
            Case "This Fiscal Year-to-Date"
                lbl.Text = FirstDateCurrentYear & " - " & CurrentDate
            Case "Yesterday"
                lbl.Text = FormatDateTime(DateAdd(DateInterval.Day, -1, Now), DateFormat.ShortDate)
            Case "Last Week"
                lbl.Text = FirstDateLastWeek & " - " & LastDateLastweek
            Case "Last Week-to-Date"
                lbl.Text = FirstDateLastWeek & " - " & CurrentDate
            Case "Last Month"
                lbl.Text = FirstDateLastMonth & " - " & LastDateLastMonth
            Case "Last Month-to-Date"
                lbl.Text = FirstDateLastMonth & " - " & CurrentDate
            Case "Last Fiscal Quarter"
                lbl.Text = FirstDateLastQuarter & " - " & LastDateLastQuarter
            Case "Last Fiscal Quarter-to-Date"
                lbl.Text = FirstDateLastQuarter & " - " & CurrentDate
            Case "Last Fiscal Year"
                lbl.Text = FirstDateLastYear & " - " & LastDateLastYear
            Case "Last Fiscal Year-to-Date"
                lbl.Text = FirstDateLastYear & " - " & CurrentDate
            Case "Next Week"
                lbl.Text = FirstDateNextWeek & " - " & LastDateNextWeek
            Case "Next 4 Weeks"
                lbl.Text = FirstDateCurrentWeek & " - " & Next4Weeks
            Case "Next Month"
                lbl.Text = FirstDayNextMonth & " - " & LastDayNextMonth
            Case "Next Fiscal Quarter"
                lbl.Text = FirstDateNextQuarter & " - " & LastDateNextQuarter
            Case "Next Fiscal Year"
                lbl.Text = FirstDateNextYear & " - " & LastDateNextYear
        End Select
    End Sub

    Public Function pCallCompany(ByRef xform As Form) As Boolean
        Dim gMFiles As New modMasterFile
        Dim gTrans As New clsTransactionFunctions

        If gVerifyCompany() = True Then
            Try
                xform.MdiParent = frmMain
                xform.Show()
            Catch ex As Exception
            End Try
        Else
            Try
                If MsgBox("Please Open company to proceed, do you want to continue?", _
                                     MsgBoxStyle.OkCancel, xform.Text) = MsgBoxResult.Ok Then
                    gMFiles.gCompanyEvents(frm_MF_Company, frm_MF_Company.cboconame, _
                                    frm_MF_Company.btnSave, frm_MF_Company.btnClear, _
                                    frm_MF_Company.lblCoInfo)
                    gTrans.gDisableFormTxt(frm_MF_Company)
                    Call OpenCompanyFormLoad()
                    frm_MF_Company.MdiParent = frmMain
                    frm_MF_Company.Show()
                    Return True
                End If
            Catch ex As Exception
                Return False
            End Try
        End If
    End Function

    Private Sub OpenCompanyFormLoad()
        With frm_MF_Company
            .btnClear.Location = New Point(304, 289)
            .btnCancel.Location = New Point(418, 289)
            .cbofiscalYr.Visible = False
            .Label14.Visible = False
            .cboconame.Enabled = True
            .Size = New Point(542, 367)
        End With
    End Sub

    Public Function m_displaybillsforchecks_refNo_only() As DataSet

        Dim sSQL As String = " usp_t_tbillspaid_load_refno_only "
        sSQL &= "@fxKeyCompany='" & gCompanyID() & "' "
        '' sSQL &= ",@fxKeySupplier='" & gPayeeID & "' "
        sSQL &= ",@fcReferenceNo='" & gPayeeRefno & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)

    End Function

    Public Function Get_Name_by_Reference(ByVal sKeyRefno As String) As String
        Dim xName As String
        Dim sSQLCmd As String = "usp_GetName_And_Type "
        sSQLCmd &= "@fxReferenceNo = '" & sKeyRefno & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                rd.Read()
                xName = rd.Item(0).ToString
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return xName
    End Function
    Public Function Get_Type_by_Reference(ByVal sKeyRefno As String) As String
        Dim xType As String
        Dim sSQLCmd As String = "usp_GetName_And_Type "
        sSQLCmd &= "@fxReferenceNo = '" & sKeyRefno & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                rd.Read()
                xType = rd.Item(1).ToString
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return xType
    End Function
    Public Function CheckDateRange(ByVal fdDate As Date) As Boolean
        Dim rd As SqlDataReader
        Dim deyt As Date
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_PeriodRestriction_LoadDetails",
                                      New SqlParameter("@coid", gCompanyID()))
        While rd.Read
            deyt = rd.Item("fdForwardDate")
        End While

        If fdDate < deyt Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function GetDateLock() As Integer
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_PeriodRestriction_LoadDetails",
                                      New SqlParameter("@coid", gCompanyID()))
        If rd.Read = True Then
            Return rd.Item("fnDays")
        End If
    End Function

    Public Function CheckEditingRestriction() As Boolean
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_PeriodRestriction_LoadDetails",
                                      New SqlParameter("@coid", gCompanyID()))
        If rd.Read = True Then
            Return rd.Item("fbIsRestricted")
        End If
    End Function

    Public Function GetchkPostedValue() As Boolean
        If frmGeneralJournalEntries.chkPosted.Checked Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetchkPostingValue() As Boolean
        If frmGeneralJournalEntries.chkPosted.Checked Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function PrintDividend() As DataSet
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Dividend_PrintDividend")
            Return ds
        Catch ex As Exception
            MsgBox(ex.ToString())
            Return Nothing
        End Try
    End Function
End Module


