Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Data

Module modGlobalDeclarations

    Private gCon As New Clsappconfiguration
    Dim cs = gCon.cnstring
    Dim Account As String

    Public strCurrentDir As String
    Public strCurrentFileName As String

    Public cnstring As String
    Public blnCloseMain As Boolean
    Public strSysCurrentPwd As String
    Public intSysCurrentId As Integer
    Public strSysCurrentUserName As String
    Public strSysCurrentFullName As String
    Public strSysCurrentDesc As String
    Public strSysCurrentGroup As String
    Public intSysCurrentGroupID As Integer

    Public gUserName As String '= gVerifyUserLoginName()
    Public gCompanyName As String
    Public gAcntType As String
    Public gAccountDesc As String
    Public gAccountID As String
    Public gColIndex As Integer
    Public gRowIndex As Integer
    Public gEnterEndingBalance As Integer
    Public gEndingBal As Decimal = 0
    Public gEndingdate As Date
    Public gCheckID As String
    Public gOpenCompany As Integer
    Public gAccountName As String
    Public gEditEvent As Integer = 0
    Public gAccountCode As String
    Public gLoader As Integer = 0
    Public gClassingStatus As Integer = 0
    Public gkeyUser As String
    Public gRefName As String

    '' creating new company
    Public gxTmp As String

    ''Company Info
    Public gLegalName As String
    Public gTaxId As String
    Public gStAdd As String
    Public gCityAdd As String
    Public gZipAdd As String
    Public gPhone As String
    Public gFax As String
    Public gEmail As String
    Public gWebSite As String

    Public gServiceCharge As Long = 0
    Public gServiceChargeDate As Date
    Public gServiceChargeAccount As String

    Public gInterestEarned As Long = 0
    Public gInterestEarnedDate As Date
    Public gInterestEarnedAccount As String

    Public gEndingBalance As Decimal = 0
    Public gClearedBalance As Decimal = 0
    Public gDifference As Decimal = 0
    Public gReconDate As Date
    Public gVerifyReconcile As Integer = 0

    Public gReconcileDiscripancy As String
    Public gSupplierBill As Integer = 0
    Public gInvoiceKey As String

    Public gDeliveryNumber As String
    Public gProject As String

    Public gAppliedVAT As String

    Public gKeyBills As String
    Public gCheckno As String
    Public gBanko As String
    Public gRvwd As String
    Public gPreparedBy As String
    Public gCVno As String
    Public gApprvd As String

    ''used for finding bills transaction per customer/supplier/others and reference no
    Public gPayeeID As String
    Public gPayeeRefno As String
    Public gPayeeName As String
    Public xVarPayee As String

    ''used for printing checks
    Public gCheckNumber As String
    Public gCheckDate As Date
    Public gPayorder As String
    Public gCheckAmt As Decimal
    Public gCheckAmtwords As String
    Public gPayeesAdd As String
    Public gPayeesMemo As String
    Public gCheckPrintMode As String
    Public gBankAccount As String
    Public gBankEndBalance As String

    ''used for item module
    Public gItemsKeys As String
    Public gKeyBillExpenses As String

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''General Journal Ledger count number of entries per list
    Public gNumberOfEntries As Integer

    ''Used for Recurring Configuration Settings
    Public gRecurringPeriod As String
    Public gRecurringStartDate As Date
    Public gRecurringEndDate As Date

    ''Used for Recurring events
    Public gDiscDate As Date
    Public gDate As Date
    Public gDateBillDue As Date

    ''Used for Sales Report
    Public gMonth As String
    Public gYear As String
    Public gBrand As String

    ''Used for Sales Receipt Report
    Public gKeySR As String

    ''Used for Listing
    Public gStartDate As Date
    Public gEndDate As Date

    ''Customer ID 
    Public gKeyCustomer As String

    ''Supplier ID
    Public gKeySupplier As String

    ''used for invoice accountabilities
    Public gKeyPayAccount As String
    Public gKeyTaxAccount As String
    Public gKeySalesAccount As String
    Public gTaxPercent As Decimal = 0.0
    Public gKeyOutputTax As String
    Public gOutputTaxPercent As Decimal = 0.0
    Public gKeyCOSAccount As String
    Public gKeyMerchAcnt As String
    Public gKeyDiscount As String

    ''used for statement of account
    Public gKeySANo As Decimal = 0
    Public gKeyStatmentNumber As String = ""

    ''used for account code
    Public gAcntCode As String = ""
    Public gKeyAccount As String

    ''used for credit memo
    Public gKeyCredit As String = ""

    ''used for journal voucher
    Public gfiEntryNo As String
    Public gfiJVKey As String

    ''used for debit memo
    Public gKeyDebit As String = ""

    ''used for item master uploading
    Public gItemMasterFilePath As String = Nothing

    ''used for Customer Master Report
    Public gTransactionType As String = ""
    Public gDateFrom As String = ""
    Public gDateTo As String = ""
    Public gtransactionsCoveredDate As String = ""

    ''used for Saving Account Budgets
    Public gBeginning_Balance As Decimal = 0.0
    Public gBeginningDate As Date = DateSerial(DatePart(DateInterval.Year, Now()), 1, 1)

    ''use for getting name and type using reference no 
    Public gName As String
    Public gType As String
    Public xVar As String

    ''use for cancelling multiple or single bills
    Public Response As String

    ''use for getting fxkeycompany 
    Public gfxKeySupplier As String

    ''use for changing the mode of Frm_Item_masterAddEDit
    Public gItemMasterMode As String
    Public TxtVarrefresher As String

    ''Posting loading details
    Public journalNo As String
    Public dDebit As Decimal
    Public dCredit As Decimal
    Public sMemo As String
    Public dtTransact As Date
    Public acntID As String
    Public acntCode As String
    Public acntTitle As String
    Public LoanRef As String
    Public AccountRef As String
    Public pID As String
    Public pName As String
    Public pRowCount As Integer
    Public pRow As Integer
    Public pMemKey As String

    ''Loan Release
    Public bLoanForRelease As Boolean

    Public qweqw As Boolean = False

    Public grdcolID As String
    Public grdcolName As String
    Public grdcolLoanRef As String
    Public grdcolAcntRef As String
    Public grdcolCode As String
    Public grdcolTitle As String
    Public grdcolSoa As String

    Public Sub grdColumns(ByVal xRow As Integer)
        grdcolID = ""
        grdcolName = ""
        grdcolLoanRef = ""
        grdcolAcntRef = ""
        grdcolSoa = ""
        grdcolCode = ""
        grdcolTitle = ""

        grdcolID = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cID", xRow)
        grdcolName = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cName", xRow)
        grdcolLoanRef = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cLoanRef", xRow)
        grdcolAcntRef = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cAccountRef", xRow)
        grdcolSoa = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cSOA", xRow)
        grdcolCode = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cAccountCode", xRow)
        grdcolTitle = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cAccountTitle", xRow)
    End Sub
    Public Sub ReccuringGrdColumns(ByVal xRow As Integer)
        grdcolID = ""
        grdcolName = ""
        grdcolLoanRef = ""
        grdcolAcntRef = ""
        grdcolCode = ""
        grdcolTitle = ""

        grdcolID = GetValuesInDataGridView(frmRecurringEntry.grdList, "memID", xRow)
        grdcolName = GetValuesInDataGridView(frmRecurringEntry.grdList, "memName", xRow)
        grdcolLoanRef = GetValuesInDataGridView(frmRecurringEntry.grdList, "LoanRef", xRow)
        grdcolAcntRef = GetValuesInDataGridView(frmRecurringEntry.grdList, "AcntRef", xRow)
        grdcolCode = GetValuesInDataGridView(frmRecurringEntry.grdList, "acntCode", xRow)
        grdcolTitle = GetValuesInDataGridView(frmRecurringEntry.grdList, "acntTitle", xRow)
    End Sub

    Public Function GetValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        Try
            If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
                If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                    fieldName = grid.Item(columnName, rowIndex).Value.ToString()

                End If
            End If
        Catch ex As Exception

        End Try
        Return fieldName

    End Function
    Public Function gVerifyCompany() As Boolean
        If gCompanyName = "" Then
            gVerifyCompany = False
        Else
            gVerifyCompany = True
        End If
    End Function
    Public Function gAccountRef() As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select fxKey_Account from Coop_Membership_Test.dbo.CIMS_Masterfile_AccountRegister where fcDocNumber = '" & frmGeneralJournalEntries.txtGeneralJournalNo.Text & "' "


        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sDescription = stVal
        Return sDescription
    End Function
    Public Function LoadAccounts() As String
        Dim LAccount As String = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cAccounts", 0)
        Dim stVal As String = ""
        Dim sDescription As String
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "AccountREF_ListMember",
                                     New SqlParameter("@fcDocNumber", LAccount))

        While rd.Read
            stVal = rd.Item(0).ToString
        End While

        sDescription = stVal
        Return sDescription
    End Function
    Public Function gAccounts() As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select fcAccountName from Coop_Membership_Test.dbo.CIMS_Masterfile_DebitCredit where fcAccountCode = '" & frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cAccountRef", 0) & "' "


        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sDescription = stVal
        Return sDescription
    End Function

    Public Function gCompanyID() As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select co_id from dbo.mCompany where co_name = '" & gCompanyName & "' "

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sDescription = stVal
        Return sDescription
    End Function

    Public Function gAccID() As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select PK_BankID from dbo.mBankList acnt_name='" & gAccountName & "' "

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sDescription = stVal
        Return sDescription
    End Function

    Public Function gAccNym() As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select acnt_name from dbo.mAccounts acnt_name='" & gAccountName & "' "


        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sDescription = stVal
        Return sDescription
    End Function


    Public Function getCompanyAddress() As String

        Dim commandString As String = "usp_rpt_CompanyAddress " & "'" & gCompanyID() & "'"
        Dim companyAddress As String = ""

        Try
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, commandString)
                If reader.Read Then
                    companyAddress = reader.Item(0).ToString
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return companyAddress
    End Function

    Public Function gAcntTypeID() As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select actType_id from dbo.mAccountType where co_id='" & gCompanyID() & "' and acnt_type='" & gAcntType & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sDescription = stVal
        Return sDescription
    End Function

    Public Function getAcntTypeID(ByVal accntname As String) As String
        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select actType_id from dbo.mAccountType where co_id='" & gCompanyID() & "' and acnt_type='" & accntname & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sDescription = stVal
        Return sDescription
    End Function

    Public Function gAcntTypeName(ByVal AcntTypeID As String) As String
        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "usp_m_mAccounts_getAccountType "
        sSQL &= "@coid='" & gCompanyID() & "' "
        sSQL &= ",@accountid='" & AcntTypeID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sDescription = stVal
        Return sDescription
    End Function

    Public Function gAccountsID() As String
        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select acnt_id from dbo.mAccounts where co_id='" & gCompanyID() & "' and acnt_type='" & gAcntTypeID() & "' and acnt_name='" & gAccountDesc & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sDescription = stVal
        Return sDescription
    End Function

    Public Function getAccountID(ByVal psAcntName As String) As String
        Dim stVal As String = ""
        Dim sAcntID As String
        Dim sSQL As String = "Select acnt_id from dbo.mAccounts where co_id='" & gCompanyID() & "' and acnt_desc='" & psAcntName & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sAcntID = stVal
        Return sAcntID
    End Function

    Public Function getGroupID(ByVal psGrpName As String) As String
        Dim stVal As String = ""
        Dim sAcntID As String
        Dim sSQL As String = "select fxKeyItemGroup from dbo.mItem01Group where fxKeyCompany='" & gCompanyID() & "' and fcItemGroupName ='" & psGrpName & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sAcntID = stVal
        Return sAcntID
    End Function

    Public Function getBrandID(ByVal sBrand As String) As String
        Dim sSQLCmd As String = "SELECT * FROM mItemBrand WHERE "
        sSQLCmd &= "fcBrandCode = '" & sBrand & "'"
        Dim sReturn As String = ""

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sReturn = rd.Item("fxKeyBrand").ToString
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sReturn
    End Function
    '##### changes#######################
    '######################################
    Public Function getSupplierID(ByVal psSupName As String) As String
        Dim stVal As String = ""
        Dim sSupplierID As String = ""
        Dim sSQL As String = "select fxKeySupplier from dbo.mSupplier00Master where fxKeyCompany='" & gCompanyID() & "' and fcSupplierName ='" & psSupName & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sSupplierID = stVal
        Return sSupplierID
    End Function
    '##########################################
    '############################################

    Public Function getCustomerID(ByVal psCusName As String) As String
        Dim stVal As String = ""
        Dim sSupplierID As String = ""
        Dim sSQL As String = "select fxKeyCustomer from dbo.mCustomer00Master where fxKeyCompany='" & gCompanyID() & "' and fcCustomerName ='" & psCusName & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sSupplierID = stVal
        Return sSupplierID
    End Function

    Public Function getCustomerName(ByVal psCusID As String) As String
        Dim stVal As String = ""
        Dim sCustomerID As String = ""
        Dim sSQL As String = "select fcCustomerName from dbo.mCustomer00Master where fxKeyCustomer='" & psCusID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sCustomerID = stVal
        Return sCustomerID
    End Function
    Public Function getSupplierName(ByVal psSupID As String) As String
        Dim SupplierName As String = ""
        Dim sSQL As String = "select fcSupplierName from dbo.mSupplier00Master where fxKeySupplier='" & psSupID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    SupplierName = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return SupplierName
    End Function

    Public Function getAddressName(ByVal psAddID As String) As String
        Dim stVal As String = ""
        Dim sSupplierID As String = ""
        Dim sSQL As String = "select fcAddressName from dbo.mAddress where fxKeyAddress='" & psAddID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sSupplierID = stVal
        Return sSupplierID
    End Function

    Public Function getUsersGroup(ByVal psUserID As String) As Integer
        Dim sGrp As Integer = 0
        Dim sSQLCmd As String = "select fxKeyGroup from dbo.tUsers where fcLogName ='" & psUserID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sGrp = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        Return sGrp
    End Function

    Public Function getUserDescription(ByVal psUserID As String) As String
        Dim sDescription As String = ""
        Dim sSQLCmd As String = "select fcDescription from dbo.tUsers where fcLogName ='" & psUserID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sDescription = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sDescription
    End Function

    Public Function getRepName(ByVal psRepID As String) As String
        Dim stVal As String = ""
        Dim sSupplierID As String = ""
        Dim sSQL As String = "select fcSalesRepInitials from dbo.mSalesRep where fxKeySalesRep='" & psRepID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sSupplierID = stVal
        Return sSupplierID
    End Function

    Public Function getTermID(ByVal psTermName As String) As String
        Dim stVal As String = ""
        Dim sTermID As String = ""
        Dim sSQL As String = "select fxKeyTerms from dbo.mTerms where fxKeyCompany='" & gCompanyID() & "' and fcTermsName='" & psTermName & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sTermID = stVal
        Return sTermID
    End Function

    Public Function getTermName(ByVal psTermID As String) As String
        Dim stVal As String = ""
        Dim sTermID As String = ""
        Dim sSQL As String = "select fcTermsName from dbo.mTerms where fxKeyTerms='" & psTermID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sTermID = stVal
        Return sTermID
    End Function

    Public Function getTaxName(ByVal psTaxID As String) As String
        Dim stVal As String = ""
        Dim sTermID As String = ""
        Dim sSQL As String = "select fcSalesTaxCodeName from dbo.mSalesTaxCode where fxKeySalesTaxCode = '" & psTaxID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sTermID = stVal
        Return sTermID
    End Function

    Public Function m_getDiscount(ByVal sKey As String) As Integer
        Try
            Dim sValue As String = ""
            Dim sSQLCmd As String = "select ISNULL(t2.fdDiscountPercentage,0) from dbo.mSupplier00Master as t1 left outer join dbo.mTerms as t2 "
            sSQLCmd &= " on t2.fxKeyTerms = t1.fxKeyTerms where t1.fxKeySupplier ='" & sKey & "' "

            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                    While rd.Read
                        sValue = rd.Item(0).ToString
                    End While
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            If sValue Is Nothing Or sValue = "" Then
                sValue = 0
            End If
            Return sValue
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Discount")
            Return Nothing
        End Try
    End Function

    Public Function getAccountName(ByVal psAcntID As String) As String

        Dim stVal As String = ""
        Dim sDescription As String
        Dim sSQL As String = "Select acnt_name from dbo.mAccounts where co_id='" & gCompanyID() & "' and acnt_id='" & psAcntID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sDescription = stVal
        Return sDescription
    End Function

    Public Function gCompanyInfo() As DataSet
        Try
            Dim sKeyComp As String = gCompanyID()
            If sKeyComp <> "" Then
                Dim sSQLCmd As String = "SELECT * FROM mCompany "
                sSQLCmd &= " WHERE co_id ='" & sKeyComp & "'"
                Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting: Company Information")
            Return Nothing
        End Try
    End Function

    Public Function gCountSegment() As Integer

        Dim stVal As Integer
        Dim sSQL As String = "Select count(seg_charsize) from dbo.mSegment where co_id ='" & gCompanyID() & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    stVal = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return stVal
    End Function

    Public Function gSegmentValidation() As Boolean
        Dim bVal As Boolean
        Dim xVal As Integer
        xVal = gCountSegment()
        If xVal <> 0 Then
            bVal = True
        ElseIf xVal = 0 Then
            bVal = False
        End If
        Return bVal
    End Function

    Public m_ProgressbarCaption As String = "Loading... Please Wait...."

    <DebuggerStepThrough()> Public Sub ShowProgressBar()
        Dim Caption As String = "Loading"

        If m_ProgressbarCaption.Length = 0 Then
            Caption = Caption
        End If
        Dim fStatus As New frmSplashForm(m_ProgressbarCaption)
        fStatus.ProgressBar1.Value = 0
        fStatus.Show()
        fStatus.Refresh()
        While fStatus.ProgressBar1.Value <> 100
            If fStatus.ProgressBar1.Value < 90 Then
                fStatus.ProgressBar1.Value += 10
            Else
                fStatus.ProgressBar1.Value = 0
            End If
            'Thread.CurrentThread.Sleep(10)
            Thread.Sleep(50)
        End While
        fStatus.Hide()
        fStatus.Dispose()
        m_ProgressbarCaption = ""
    End Sub

    Public Function verificationofexecution(ByVal psPassword As String) As Boolean
        Dim xResult As Boolean = False
        If psPassword = "password" Then
            xResult = True
        ElseIf psPassword <> "password" Then
            xResult = False
        End If
        Return xResult
    End Function

    Public Function get_preparedby(Optional ByVal sName = "") As String
        Dim sSQLCmd As String
        Dim sReturn As String = Nothing
        If sName = "" Then
            sSQLCmd = "select fcFullName from dbo.tUsers where fcLogName='" & gUserName & "' "
        Else
            sSQLCmd = "select fcFullName from dbo.tUsers where fcLogName='" & sName & "' "
        End If

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sReturn = rd.Item(0).ToString
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sReturn
    End Function

End Module
