﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmStatutoryEntry

    Private gCon As New Clsappconfiguration()
    Dim TotalAmount As Integer


    Private Sub frmStatutoryEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub SaveToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        Call saveEntryHeader(System.Guid.NewGuid.ToString())

    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        grdList.Rows.Clear()
        If rbMonthly.Checked = True Then
            GetMonthlyEntry()
        Else
            GetYearlyEntry()
        End If

        ComputeTotals()
    End Sub

    Private Sub GetMonthlyEntry()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_StatutoryAllocations_GetEntry",
                                                                            New SqlParameter("@coid", gCompanyID()),
                                                                            New SqlParameter("@FdDate", dtpDate.Text))

        While rd.Read = True
            AddDetailItem(rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(4))
        End While
    End Sub

    Private Sub GetYearlyEntry()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_StatutoryAllocations_GetEntryYearly",
                                                                            New SqlParameter("@coid", gCompanyID()),
                                                                            New SqlParameter("@FdDate", dtpDate.Text))

        While rd.Read = True
            AddDetailItem(rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(4))
        End While
    End Sub

    Private Sub AddDetailItem(ByVal acntKey As String,
                             ByVal acntCode As String,
                             ByVal acntTitle As String,
                             ByVal Credit As Decimal)

        Try
            Dim row As String() =
             {acntKey, acntCode, acntTitle, Format(CDec(Credit), "##,##0.00")}

            Dim nRowIndex As Integer
            With grdList

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        grdList.Rows.Clear()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub btnSearchDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchDoc.Click
        frmJVno.ShowDialog()
        If frmJVno.DialogResult = DialogResult.OK Then
            txtJVNo.Text = frmJVno.listDocNumber.SelectedItems.Item(0).Text
        End If
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function

    Private Sub saveEntryHeader(ByVal guid As String)
        Dim user As String = frmMain.currentUser.Text
        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dtpDate.Value.Date, "JV")
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", gCompanyID()),
                    New SqlParameter("@fdTransDate", dtpDate.Value),
                    New SqlParameter("@fdTotAmt", TotalAmount),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", "JV"),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", txtJVNo.Text),
                    New SqlParameter("@fbIsCancelled", 0),
                    New SqlParameter("@fbIsPosted", 0),
                    New SqlParameter("@fdDatePrepared", dtpDate.Value),
                    New SqlParameter("@SoaNo", ""))
            Call UpdateDocDate()
            Call saveDetails(guid)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub saveDetails(ByVal guid As String)
        Try
            For xRow As Integer = 0 To grdList.RowCount - 1
                Dim journalNo As String = txtJVNo.Text
                Dim sAccount As String = grdList.Item("acntKey", xRow).Value.ToString
                Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
                Dim dDebit As Decimal = 0
                Dim dCredit As Decimal = grdList.Item("Amount", xRow).Value
                Dim sMemo As String = ""
                Dim pkJournalID As String = guid
                Dim dtTransact As Date = dtpDate.Value.Date
                

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                  New SqlParameter("@fiEntryNo", journalNo), _
                                  New SqlParameter("@fxKeyAccount", sAccount), _
                                  New SqlParameter("@fdDebit", dDebit), _
                                  New SqlParameter("@fdCredit", dCredit), _
                                  New SqlParameter("@fcMemo", ""), _
                                  New SqlParameter("@fxKey", jvDetailRecordID), _
                                  New SqlParameter("@fk_JVHeader", guid), _
                                  New SqlParameter("@transDate", dtTransact), _
                                  New SqlParameter("@fcLoanRef", ""), _
                                  New SqlParameter("@fcAccRef", ""))
                gCon.sqlconn.Close()
            Next
            MsgBox("Statutory Allocation Entry Saved!", MsgBoxStyle.Information, "Statutory Allocation")
            grdList.Rows.Clear()
            txtJVNo.Clear()
            TotalAmount = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtJVNo.Text),
                    New SqlParameter("@fcDoctype", "JOURNAL VOUCHER"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComputeTotals()
        Dim totalallocation As Decimal

        Try
            For Each xRow As DataGridViewRow In grdList.Rows
                Dim credit As Object = grdList.Item("Amount", xRow.Index).Value

                If credit IsNot Nothing Then
                    totalallocation += credit
                End If
            Next
        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        TotalAmount = totalallocation
    End Sub
End Class