Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmbankfileconvertion
    Private Sub frmbankfileconvertion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadNetPay(Me.DateTimePicker1.Value.Date)
        recordcount(Me.DateTimePicker1.Value.Date)
        LoadCompanyCode()
        LoadBranchCode()
        LoadCompanyAccount()
        If txtbatch.Text = "" Or txtceilingamnt.Text = "" Or Me.grdBills.RowCount = 0 Then
            Me.btnGenerate.Enabled = False
        Else
            Me.btnGenerate.Enabled = True
        End If
        clearme()
    End Sub
    Private Sub clearme()

        Me.txtceilingamnt.Clear()
        Me.txtbatch.Clear()
        Me.txtRcount.Clear()
        Me.txtTNetPay.Clear()

    End Sub
    Private Sub loadNetPay(ByVal Dtfrom As Date)
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "TotalNetPay", _
                                    New SqlParameter("@daterel", Dtfrom))
        Me.txtTNetPay.Clear()
        Try
            While dr.Read
                Me.txtTNetPay.Text = dr.Item(0)
            End While
            dr.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub recordcount(ByVal dtfrom As Date)
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "RecordCount", _
                                    New SqlParameter("@daterel", dtfrom))
        Me.txtRcount.Clear()
        Try
            While dr.Read
                Me.txtRcount.Text = dr.Item(0)
            End While
            dr.Close()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub LoadCompanyCode()
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "companycode")
        Me.txtCompCode.Clear()
        Try
            While dr.Read
                Me.txtCompCode.Text = dr.Item(0)
            End While
            dr.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub LoadCeilingamnt(ByVal dateto As Date)
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "ceilingamnt", _
                                New SqlParameter("@daterel", dateto))
        Me.txtceilingamnt.Clear()
        Try
            While dr.Read
                Me.txtceilingamnt.Text = dr.Item(0)
            End While
            dr.Close()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadCompanyAccount()
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CompanyAccount")
        Me.txtCompAcct.Clear()
        Try
            While dr.Read
                Me.txtCompAcct.Text = dr.Item(0)
            End While
            dr.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub LoadBranchCode()
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "branchcode")
        Me.txtPres.Clear()
        Try
            While dr.Read
                Me.txtPres.Text = dr.Item(0)
            End While
            dr.Close()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadbankfiletoConvert(ByVal dtfrom As Date)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("LoadBankFileToConvert", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@dtfrom", SqlDbType.Date).Value = dtfrom
        End With
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Bank_File")

            Me.grdBills.DataSource = ds
            Me.grdBills.DataMember = "Bank_File"
            Me.grdBills.Columns(0).Width = 200
            grdBills.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdBills.ReadOnly = True
            grdBills.Columns(1).Width = 300

            Me.grdBills.Columns(1).DefaultCellStyle.Format = "#,##0.00"
            Me.grdBills.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            grdBills.Columns(2).Visible = False
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Bills")
        Finally
            gcon.sqlconn.Close()
        End Try

    End Sub
    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        clearme()
        Call LoadbankfiletoConvert(Me.DateTimePicker1.Value.Date)
        Call loadNetPay(Me.DateTimePicker1.Value.Date)
        Call recordcount(Me.DateTimePicker1.Value.Date)
        Call LoadCeilingamnt(DateTimePicker1.Value.Date)
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub
    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click

        frmBankFileConvert.getdatestart() = Me.DateTimePicker1.Value.Date
        frmBankFileConvert.getbatch() = Me.txtbatch.Text
        frmBankFileConvert.getceilingamt() = Me.txtceilingamnt.Text
        frmBankFileConvert.getrowcount() = Me.txtRcount.Text
        frmBankFileConvert.getAmount() = Me.txtTNetPay.Text
        frmBankFileConvert.getAcctNo() = Me.txtCompAcct.Text

        frmBankFileConvert.Show()
    End Sub
    Private Sub txtbatch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbatch.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtbatch.Focus()
        End If
    End Sub
    Private Sub txtceilingamnt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtceilingamnt.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtceilingamnt.Focus()
        End If
    End Sub
    Private Sub txtbatch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbatch.TextChanged
        If txtbatch.Text = "" Or txtceilingamnt.Text = "" Then
            Me.btnGenerate.Enabled = False
        Else
            Me.btnGenerate.Enabled = True
        End If
    End Sub
    Private Sub txtceilingamnt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtceilingamnt.TextChanged
        If txtbatch.Text = "" Or txtceilingamnt.Text = "" Then
            Me.btnGenerate.Enabled = False
        Else
            Me.btnGenerate.Enabled = True
        End If
    End Sub
End Class
