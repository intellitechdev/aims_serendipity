Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports CrystalDecisions.Shared
Public Class Bank_Advice2
    Private gcon As New Clsappconfiguration
    Private rptSummary As New ReportDocument
    Private datestart As Date
    Public Property getdatestart() As Date
        Get
            Return datestart
        End Get
        Set(ByVal value As Date)
            datestart = value
        End Set
    End Property
    Private batch As Integer
    Public Property getbatch() As Integer
        Get
            Return batch
        End Get
        Set(ByVal value As Integer)
            batch = value
        End Set
    End Property
    Private Sub Bank_Advice2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptSummary.Close()
    End Sub
    Private Sub Bank_Advice2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadBankAdviceReport2()
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadBankAdviceReport2()
        Dim reportpath As String = Application.StartupPath & "\Accounting Reports\Bank_advice3.rpt"
        rptSummary.Load(reportpath)
        Logon(rptSummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))
        rptSummary.Refresh()
        With rptSummary
            .SetParameterValue("@daterel", getdatestart())
            .SetParameterValue("@batchNo", getbatch())
        End With
        Me.crvReport.Visible = True
        Me.crvReport.ReportSource = rptSummary

    End Sub
End Class