Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_acc_writeChecks_v2_Void

    Private gCon As New Clsappconfiguration

    Private paybillsID As String
    Public Property GetPaybillsID() As String
        Get
            Return paybillsID
        End Get
        Set(ByVal value As String)
            paybillsID = value
        End Set
    End Property

    Private user As String
    Public Property GetUsername() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property

    Private writeChecksID As String
    Public Property GetWriteChecksID() As String
        Get
            Return writeChecksID
        End Get
        Set(ByVal value As String)
            writeChecksID = value
        End Set
    End Property


#Region "Validations"
    Private Function IsReasonValid() As Boolean
        Dim isReasonIncluded As Boolean = True

        If txtReason.Text.Length < 20 Then
            isReasonIncluded = False
        End If

        Return isReasonIncluded
    End Function

    Private Function IsCheckNoValid()
        Dim isCheckNoIncluded As Boolean = True

        If txtCheckNo.Text.Length = 0 Then
            isCheckNoIncluded = False
        End If

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_CheckIfCheckNoExist", _
                New SqlParameter("@checkNo", txtCheckNo.Text.Trim))
            If rd.HasRows = True Then
                isCheckNoIncluded = False
            End If
        End Using

        Return isCheckNoIncluded
    End Function
#End Region

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
        Dispose()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            If IsReasonValid() Then
                If IsCheckNoValid() Then
                    Call ApplyVoidType()
                    Call CloseActiveWindows()
                Else
                    MessageBox.Show("Check No. is invalid.", "Invalid Check", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("You need atleast 20 characters to make a valid reason.", "Invalid Reason", _
                    MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CloseActiveWindows()
        Me.Close()
        frm_acc_writeChecks_v2.Close()
    End Sub

    Private Sub ApplyVoidType()
        Try
            If optReprint.Checked = True Then
                Call ReprintCheck()
                Call LoadCheckVoucherReport()
            Else
                MessageBox.Show("Not yet available")
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub ReprintCheck()
        Dim storedProcedure As String = "CAS_t_RePrintChecks"
        Dim newWriteCheckID As String = System.Guid.NewGuid.ToString()

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                    New SqlParameter("@newWriteCheckID", newWriteCheckID), _
                    New SqlParameter("@writeCheckID", Me.GetWriteChecksID()), _
                    New SqlParameter("@newCheckNo", txtCheckNo.Text), _
                    New SqlParameter("@reason", txtReason.Text), _
                    New SqlParameter("@user", GetUsername()))

            'Apply the new Write Check ID
            GetWriteChecksID() = newWriteCheckID
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadCheckVoucherReport()
        With frmCheckVoucherReport
            .GetWriteCheckID() = Me.GetWriteChecksID()
            .GetPayBillsID = Me.GetPaybillsID()
            .ShowDialog()
        End With
    End Sub

    Private Sub optReprint_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optReprint.CheckedChanged
        If optReprint.Checked = True Then
            txtCheckNo.Enabled = True
        Else
            txtCheckNo.Enabled = False
        End If
    End Sub

    Private Sub optCancelPayment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCancelPayment.CheckedChanged
        If optCancelPayment.Checked = True Then
            txtCheckNo.Enabled = False
        Else
            txtCheckNo.Enabled = True
        End If
    End Sub

    Private Sub txtReason_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReason.TextChanged
        If txtReason.Text.Length <> 0 Then
            lblReason.Text = "Reason:" & " (" & txtReason.Text.Length & " characters)"
        Else
            lblReason.Text = "Reason:"
        End If

    End Sub
End Class