Public Class frm_acc_transferfunds

    Private gTrans As New clsTransactionFunctions
    Dim checkDate As New clsRestrictedDate

    Private Sub frm_acc_transferfunds_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IsDateRestricted()
        Call m_DisplayAssetAccounts(cboTransferFrom)
        Call m_DisplayAssetAccounts(cboTransferTo)

    End Sub

    Private Sub btn_trans_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If frmClosingEntries.IsPeriodClosed(dteTransferred.Value.Date, btnSaveClose) = False Then
            If cboTransferFrom.SelectedItem <> cboTransferTo.SelectedItem Then
                If cboTransferFrom.SelectedItem <> " " Or cboTransferTo.SelectedItem <> " " Or _
                cboTransferFrom.Text <> "" Or cboTransferTo.Text <> "" Or txtTransferAmt.Text <> "0.00" Then
                    Call saveTransferFund()
                    Call cleartext()
                    Me.Close()
                Else
                    MessageBox.Show("some of the fields requires an entry.", "CLICKSOFTWARE:" + Me.Text)
                End If
            Else
                MessageBox.Show("You had to choose other/another account to continue this transactions.", "CLICKSOFTWARE:" + Me.Text)
            End If
        End If
    End Sub

    Private Sub txtTransferAmt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtTransferAmt.Validating
        txtTransferAmt.Text = Format(CDec(txtTransferAmt.Text), "##,##0.00")
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        If frmClosingEntries.IsPeriodClosed(dteTransferred.Value.Date, btnSaveNew) = False Then
            If cboTransferFrom.SelectedItem <> cboTransferTo.SelectedItem Then
                If cboTransferFrom.SelectedItem <> " " Or cboTransferTo.SelectedItem <> " " Or _
                   cboTransferFrom.Text <> "" Or cboTransferTo.Text <> "" Or txtTransferAmt.Text <> "0.00" Then
                    Call saveTransferFund()
                    Call cleartext()
                Else
                    MessageBox.Show("some of the fields requires an entry.", "CLICKSOFTWARE:" + Me.Text)
                End If
            Else
                MessageBox.Show("You had to choose other/another account to continue this transactions.", "CLICKSOFTWARE:" + Me.Text)
            End If
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Call cleartext()
    End Sub

    Private Sub cboTransferFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTransferFrom.SelectedIndexChanged
        Dim sFrmAccnt() As String
        sFrmAccnt = Split(cboTransferFrom.SelectedItem, "-")
        txtAcntBalFrom.Text = Format(CDec(m_GetBalanceAmount(sFrmAccnt(0))), "#,##0.00")
    End Sub

    Private Sub cboTransferTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTransferTo.SelectedIndexChanged
        Dim sToAccnt() As String
        sToAccnt = Split(cboTransferTo.SelectedItem, "-")
        txtAcntBalTo.Text = Format(CDec(m_GetBalanceAmount(sToAccnt(0))), "#,##0.00")
    End Sub

    Private Sub ts_trans_journal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_trans_journal.Click
        frm_acc_makeGeneralJournalEntry.Show()
        Me.Close()
    End Sub

    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dteTransferred.Value) Then
            btnSaveNew.Enabled = False 'true if date is NOT within range of period restricted
            btnSaveClose.Enabled = False
        Else
            btnSaveNew.Enabled = True 'false if date is within range of period restricted
            btnSaveClose.Enabled = True
        End If
    End Sub

    Private Sub dteTransferred_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteTransferred.ValueChanged
        IsDateRestricted()
    End Sub

#Region "Functions/Procedures"

    Private Sub saveTransferFund()
        Dim sFrmAccnt() As String
        Dim sToAccnt() As String
        Dim sFrmAccntID, sToAccntID As String
        sFrmAccnt = Split(cboTransferFrom.SelectedItem, "-")
        sFrmAccntID = m_GetAccountID(sFrmAccnt(0))
        sToAccnt = Split(cboTransferTo.SelectedItem, "-")
        sToAccntID = m_GetAccountID(sToAccnt(0))
        Call gTrans.gSaveTransferFunds(sFrmAccntID, sToAccntID, txtTransferAmt.Text, _
                                        txtMemo.Text, Microsoft.VisualBasic.FormatDateTime(dteTransferred.Value, DateFormat.ShortDate), Me)

        Call gTrans.gUpdateAccountBalanceAmount(sFrmAccntID, (CDec(txtAcntBalFrom.Text) - CDec(txtTransferAmt.Text)), Me)
        Call gTrans.gUpdateAccountBalanceAmount(sToAccntID, (CDec(txtAcntBalTo.Text) + CDec(txtTransferAmt.Text)), Me)
    End Sub

    Private Sub cleartext()
        If Me.btnClear.Text = "Clear" Then
            Me.btnClear.Text = "Close"
            dteTransferred.Value = Now.Date
            cboTransferFrom.Text = ""
            cboTransferTo.Text = ""
            txtTransferAmt.Text = "0.00"
            txtAcntBalFrom.Text = "0.00"
            txtAcntBalTo.Text = "0.00"
            txtMemo.Text = ""
        ElseIf Me.btnClear.Text = "Close" Then
            Me.btnClear.Text = "Clear"
            Me.Close()
        End If
    End Sub

#End Region


End Class