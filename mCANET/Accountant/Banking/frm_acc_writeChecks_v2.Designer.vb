<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_writeChecks_v2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim PanelCheck As System.Windows.Forms.Panel
        Dim Label13 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim lblPayto As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_writeChecks_v2))
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.lblCheckNo = New System.Windows.Forms.Label
        Me.lblAccountName = New System.Windows.Forms.Label
        Me.lblBankAccount = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblAmountInWords = New System.Windows.Forms.Label
        Me.lblPayToOrder = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.dtePaydate = New System.Windows.Forms.DateTimePicker
        Me.panelTop = New System.Windows.Forms.Panel
        Me.cboBankList = New MTGCComboBox
        Me.cboSupplierList = New MTGCComboBox
        Me.lblBank = New System.Windows.Forms.Label
        Me.lblMember = New System.Windows.Forms.Label
        Me.lblPaydate = New System.Windows.Forms.Label
        Me.panelBottom = New System.Windows.Forms.Panel
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnVoid = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.LblTransStatus = New CSAcctg.modTransparentLabel
        PanelCheck = New System.Windows.Forms.Panel
        Label13 = New System.Windows.Forms.Label
        Label8 = New System.Windows.Forms.Label
        Label7 = New System.Windows.Forms.Label
        Label6 = New System.Windows.Forms.Label
        Label5 = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        Label3 = New System.Windows.Forms.Label
        Label2 = New System.Windows.Forms.Label
        Label1 = New System.Windows.Forms.Label
        lblPayto = New System.Windows.Forms.Label
        PanelCheck.SuspendLayout()
        Me.panelTop.SuspendLayout()
        Me.panelBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelCheck
        '
        PanelCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        PanelCheck.BackgroundImage = Global.CSAcctg.My.Resources.Resources.ChequeTexture1
        PanelCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        PanelCheck.Controls.Add(Me.LblTransStatus)
        PanelCheck.Controls.Add(Me.lblAmount)
        PanelCheck.Controls.Add(Me.txtAmount)
        PanelCheck.Controls.Add(Label13)
        PanelCheck.Controls.Add(Me.lblCheckNo)
        PanelCheck.Controls.Add(Me.lblAccountName)
        PanelCheck.Controls.Add(Me.lblBankAccount)
        PanelCheck.Controls.Add(Me.lblDate)
        PanelCheck.Controls.Add(Me.lblAmountInWords)
        PanelCheck.Controls.Add(Me.lblPayToOrder)
        PanelCheck.Controls.Add(Me.Label12)
        PanelCheck.Controls.Add(Me.Label11)
        PanelCheck.Controls.Add(Me.Label10)
        PanelCheck.Controls.Add(Me.Label9)
        PanelCheck.Controls.Add(Label8)
        PanelCheck.Controls.Add(Label7)
        PanelCheck.Controls.Add(Label6)
        PanelCheck.Controls.Add(Label5)
        PanelCheck.Controls.Add(Label4)
        PanelCheck.Controls.Add(Label3)
        PanelCheck.Controls.Add(Label2)
        PanelCheck.Controls.Add(Label1)
        PanelCheck.Controls.Add(lblPayto)
        PanelCheck.Location = New System.Drawing.Point(0, 39)
        PanelCheck.Name = "PanelCheck"
        PanelCheck.Size = New System.Drawing.Size(776, 270)
        PanelCheck.TabIndex = 0
        '
        'lblAmount
        '
        Me.lblAmount.BackColor = System.Drawing.Color.Transparent
        Me.lblAmount.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(601, 93)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(153, 23)
        Me.lblAmount.TabIndex = 20
        Me.lblAmount.Text = "**********1,000.00"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(604, 95)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(150, 20)
        Me.txtAmount.TabIndex = 25
        '
        'Label13
        '
        Label13.BackColor = System.Drawing.Color.DimGray
        Label13.Location = New System.Drawing.Point(604, 220)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(150, 1)
        Label13.TabIndex = 24
        '
        'lblCheckNo
        '
        Me.lblCheckNo.BackColor = System.Drawing.Color.Transparent
        Me.lblCheckNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCheckNo.Location = New System.Drawing.Point(498, 28)
        Me.lblCheckNo.Name = "lblCheckNo"
        Me.lblCheckNo.Size = New System.Drawing.Size(153, 23)
        Me.lblCheckNo.TabIndex = 23
        Me.lblCheckNo.Text = "<Check No.>"
        '
        'lblAccountName
        '
        Me.lblAccountName.BackColor = System.Drawing.Color.Transparent
        Me.lblAccountName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountName.Location = New System.Drawing.Point(197, 28)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(302, 23)
        Me.lblAccountName.TabIndex = 22
        Me.lblAccountName.Text = "<Account Name>"
        '
        'lblBankAccount
        '
        Me.lblBankAccount.BackColor = System.Drawing.Color.Transparent
        Me.lblBankAccount.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankAccount.Location = New System.Drawing.Point(46, 28)
        Me.lblBankAccount.Name = "lblBankAccount"
        Me.lblBankAccount.Size = New System.Drawing.Size(153, 23)
        Me.lblBankAccount.TabIndex = 21
        Me.lblBankAccount.Text = "<Account No>"
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(591, 54)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(160, 23)
        Me.lblDate.TabIndex = 19
        Me.lblDate.Text = "10/19/2010"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAmountInWords
        '
        Me.lblAmountInWords.BackColor = System.Drawing.Color.Transparent
        Me.lblAmountInWords.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountInWords.Location = New System.Drawing.Point(98, 139)
        Me.lblAmountInWords.Name = "lblAmountInWords"
        Me.lblAmountInWords.Size = New System.Drawing.Size(653, 14)
        Me.lblAmountInWords.TabIndex = 18
        Me.lblAmountInWords.Text = "Amount In Words *****************************************************************" & _
            "*************************"
        '
        'lblPayToOrder
        '
        Me.lblPayToOrder.BackColor = System.Drawing.Color.Transparent
        Me.lblPayToOrder.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayToOrder.ForeColor = System.Drawing.Color.Black
        Me.lblPayToOrder.Location = New System.Drawing.Point(131, 94)
        Me.lblPayToOrder.Name = "lblPayToOrder"
        Me.lblPayToOrder.Size = New System.Drawing.Size(451, 20)
        Me.lblPayToOrder.TabIndex = 16
        Me.lblPayToOrder.Text = "Member Name"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.DimGray
        Me.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label12.Location = New System.Drawing.Point(667, 15)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 13)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "R/T No."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.DimGray
        Me.Label11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label11.Location = New System.Drawing.Point(498, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Check No."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DimGray
        Me.Label10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label10.Location = New System.Drawing.Point(197, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(75, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Account Name"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DimGray
        Me.Label9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label9.Location = New System.Drawing.Point(44, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Account No."
        '
        'Label8
        '
        Label8.BackColor = System.Drawing.Color.DimGray
        Label8.Location = New System.Drawing.Point(601, 116)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(150, 1)
        Label8.TabIndex = 9
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.BackColor = System.Drawing.Color.Transparent
        Label7.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.SaddleBrown
        Label7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Label7.Location = New System.Drawing.Point(582, 96)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(23, 26)
        Label7.TabIndex = 8
        Label7.Text = "P"
        '
        'Label6
        '
        Label6.BackColor = System.Drawing.Color.DimGray
        Label6.Location = New System.Drawing.Point(591, 77)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(160, 1)
        Label6.TabIndex = 7
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.BackColor = System.Drawing.Color.Transparent
        Label5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.SaddleBrown
        Label5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Label5.Location = New System.Drawing.Point(544, 63)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(44, 19)
        Label5.TabIndex = 6
        Label5.Text = "DATE"
        '
        'Label4
        '
        Label4.BackColor = System.Drawing.Color.DimGray
        Label4.Location = New System.Drawing.Point(98, 155)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(653, 1)
        Label4.TabIndex = 5
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.BackColor = System.Drawing.Color.Transparent
        Label3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.SaddleBrown
        Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Label3.Location = New System.Drawing.Point(43, 139)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(53, 19)
        Label3.TabIndex = 4
        Label3.Text = "PESOS"
        '
        'Label2
        '
        Label2.BackColor = System.Drawing.Color.DimGray
        Label2.Location = New System.Drawing.Point(132, 116)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(450, 1)
        Label2.TabIndex = 3
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.Transparent
        Label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.SaddleBrown
        Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Label1.Location = New System.Drawing.Point(43, 102)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(78, 19)
        Label1.TabIndex = 2
        Label1.Text = "ORDER OF"
        '
        'lblPayto
        '
        lblPayto.AutoSize = True
        lblPayto.BackColor = System.Drawing.Color.Transparent
        lblPayto.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblPayto.ForeColor = System.Drawing.Color.SaddleBrown
        lblPayto.Location = New System.Drawing.Point(43, 83)
        lblPayto.Name = "lblPayto"
        lblPayto.Size = New System.Drawing.Size(87, 19)
        lblPayto.TabIndex = 1
        lblPayto.Text = "PAY TO THE"
        '
        'dtePaydate
        '
        Me.dtePaydate.CalendarTitleBackColor = System.Drawing.Color.DodgerBlue
        Me.dtePaydate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtePaydate.Location = New System.Drawing.Point(640, 10)
        Me.dtePaydate.Name = "dtePaydate"
        Me.dtePaydate.Size = New System.Drawing.Size(124, 20)
        Me.dtePaydate.TabIndex = 14
        '
        'panelTop
        '
        Me.panelTop.Controls.Add(Me.cboBankList)
        Me.panelTop.Controls.Add(Me.cboSupplierList)
        Me.panelTop.Controls.Add(Me.lblBank)
        Me.panelTop.Controls.Add(Me.lblMember)
        Me.panelTop.Controls.Add(Me.lblPaydate)
        Me.panelTop.Controls.Add(Me.dtePaydate)
        Me.panelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTop.Location = New System.Drawing.Point(0, 0)
        Me.panelTop.Name = "panelTop"
        Me.panelTop.Size = New System.Drawing.Size(776, 39)
        Me.panelTop.TabIndex = 0
        '
        'cboBankList
        '
        Me.cboBankList.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboBankList.ArrowColor = System.Drawing.Color.Black
        Me.cboBankList.BindedControl = CType(resources.GetObject("cboBankList.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboBankList.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboBankList.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboBankList.ColumnNum = 2
        Me.cboBankList.ColumnWidth = "121;0"
        Me.cboBankList.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboBankList.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboBankList.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboBankList.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboBankList.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboBankList.DisplayMember = "Text"
        Me.cboBankList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboBankList.DropDownBackColor = System.Drawing.Color.LightSkyBlue
        Me.cboBankList.DropDownForeColor = System.Drawing.Color.Black
        Me.cboBankList.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cboBankList.DropDownWidth = 141
        Me.cboBankList.GridLineColor = System.Drawing.Color.LightGray
        Me.cboBankList.GridLineHorizontal = False
        Me.cboBankList.GridLineVertical = False
        Me.cboBankList.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboBankList.Location = New System.Drawing.Point(309, 10)
        Me.cboBankList.ManagingFastMouseMoving = True
        Me.cboBankList.ManagingFastMouseMovingInterval = 30
        Me.cboBankList.Name = "cboBankList"
        Me.cboBankList.SelectedItem = Nothing
        Me.cboBankList.SelectedValue = Nothing
        Me.cboBankList.Size = New System.Drawing.Size(194, 21)
        Me.cboBankList.TabIndex = 27
        '
        'cboSupplierList
        '
        Me.cboSupplierList.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboSupplierList.ArrowColor = System.Drawing.Color.Black
        Me.cboSupplierList.BindedControl = CType(resources.GetObject("cboSupplierList.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboSupplierList.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboSupplierList.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboSupplierList.ColumnNum = 2
        Me.cboSupplierList.ColumnWidth = "121;0"
        Me.cboSupplierList.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboSupplierList.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboSupplierList.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboSupplierList.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboSupplierList.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboSupplierList.DisplayMember = "Text"
        Me.cboSupplierList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboSupplierList.DropDownBackColor = System.Drawing.Color.AliceBlue
        Me.cboSupplierList.DropDownForeColor = System.Drawing.Color.Black
        Me.cboSupplierList.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cboSupplierList.DropDownWidth = 141
        Me.cboSupplierList.GridLineColor = System.Drawing.Color.LightGray
        Me.cboSupplierList.GridLineHorizontal = False
        Me.cboSupplierList.GridLineVertical = False
        Me.cboSupplierList.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboSupplierList.Location = New System.Drawing.Point(70, 10)
        Me.cboSupplierList.ManagingFastMouseMoving = True
        Me.cboSupplierList.ManagingFastMouseMovingInterval = 30
        Me.cboSupplierList.Name = "cboSupplierList"
        Me.cboSupplierList.SelectedItem = Nothing
        Me.cboSupplierList.SelectedValue = Nothing
        Me.cboSupplierList.Size = New System.Drawing.Size(194, 21)
        Me.cboSupplierList.TabIndex = 26
        '
        'lblBank
        '
        Me.lblBank.AutoSize = True
        Me.lblBank.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBank.Location = New System.Drawing.Point(268, 14)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(37, 15)
        Me.lblBank.TabIndex = 19
        Me.lblBank.Text = "Bank:"
        '
        'lblMember
        '
        Me.lblMember.AutoSize = True
        Me.lblMember.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMember.Location = New System.Drawing.Point(7, 14)
        Me.lblMember.Name = "lblMember"
        Me.lblMember.Size = New System.Drawing.Size(59, 15)
        Me.lblMember.TabIndex = 17
        Me.lblMember.Text = "Member:"
        '
        'lblPaydate
        '
        Me.lblPaydate.AutoSize = True
        Me.lblPaydate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaydate.Location = New System.Drawing.Point(545, 14)
        Me.lblPaydate.Name = "lblPaydate"
        Me.lblPaydate.Size = New System.Drawing.Size(89, 15)
        Me.lblPaydate.TabIndex = 15
        Me.lblPaydate.Text = "Payment Date:"
        '
        'panelBottom
        '
        Me.panelBottom.Controls.Add(Me.btnPrint)
        Me.panelBottom.Controls.Add(Me.btnVoid)
        Me.panelBottom.Controls.Add(Me.btnOk)
        Me.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelBottom.Location = New System.Drawing.Point(0, 313)
        Me.panelBottom.Name = "panelBottom"
        Me.panelBottom.Size = New System.Drawing.Size(776, 38)
        Me.panelBottom.TabIndex = 0
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(567, 6)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(99, 29)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print Check"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnVoid
        '
        Me.btnVoid.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnVoid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVoid.Location = New System.Drawing.Point(9, 6)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.Size = New System.Drawing.Size(99, 29)
        Me.btnVoid.TabIndex = 2
        Me.btnVoid.Text = "Void Check"
        Me.btnVoid.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOk.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOk.Location = New System.Drawing.Point(668, 6)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(99, 29)
        Me.btnOk.TabIndex = 1
        Me.btnOk.Text = "Accept"
        Me.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'LblTransStatus
        '
        Me.LblTransStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblTransStatus.Font = New System.Drawing.Font("Calibri", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTransStatus.ForeColor = System.Drawing.Color.Sienna
        Me.LblTransStatus.Location = New System.Drawing.Point(12, 7)
        Me.LblTransStatus.Name = "LblTransStatus"
        Me.LblTransStatus.quadrantMode = False
        Me.LblTransStatus.rotationAngle = -25
        Me.LblTransStatus.Size = New System.Drawing.Size(752, 247)
        Me.LblTransStatus.TabIndex = 37
        Me.LblTransStatus.Text = "Status"
        Me.LblTransStatus.Visible = False
        '
        'frm_acc_writeChecks_v2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnOk
        Me.ClientSize = New System.Drawing.Size(776, 351)
        Me.Controls.Add(PanelCheck)
        Me.Controls.Add(Me.panelBottom)
        Me.Controls.Add(Me.panelTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(782, 379)
        Me.Name = "frm_acc_writeChecks_v2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cheque Payment"
        PanelCheck.ResumeLayout(False)
        PanelCheck.PerformLayout()
        Me.panelTop.ResumeLayout(False)
        Me.panelTop.PerformLayout()
        Me.panelBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelTop As System.Windows.Forms.Panel
    Friend WithEvents panelBottom As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblPayToOrder As System.Windows.Forms.Label
    Friend WithEvents dtePaydate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblAmountInWords As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblCheckNo As System.Windows.Forms.Label
    Friend WithEvents lblAccountName As System.Windows.Forms.Label
    Friend WithEvents lblBankAccount As System.Windows.Forms.Label
    Friend WithEvents lblMember As System.Windows.Forms.Label
    Friend WithEvents lblPaydate As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents btnVoid As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents lblBank As System.Windows.Forms.Label
    Friend WithEvents cboBankList As MTGCComboBox
    Friend WithEvents cboSupplierList As MTGCComboBox
    Friend WithEvents LblTransStatus As CSAcctg.modTransparentLabel
End Class
