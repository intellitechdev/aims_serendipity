Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmCheckNo

    Private gCon As New Clsappconfiguration
    Private Function validateCheckNumber(ByVal UsersCheckNumber As String)
        'this function will check if the checknumber are already existed
        Dim a As String
        Dim sqlCVNoChecker As String = "SELECT * FROM dbo.tWPrintedChecks WHERE fcChkNumber='" & UsersCheckNumber & "'"
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sqlCVNoChecker)
            While rd.Read
                a = rd.Item(0).ToString
            End While
        Catch ex As Exception
            Return Nothing
        End Try
        If a Is Nothing Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim CVValidator As Boolean = validateCheckNumber(txtChkNo.Text)
        If CVValidator = True Then
            gCheckNumber = txtChkNo.Text
            gCheckPrintMode = ComboBox1.SelectedItem
            gRvwd = txtCheckBy.Text
            gApprvd = txtReviewedBy.Text
            txtChkNo.Text = "0"
            ComboBox1.Text = ""
            Call savecheckno()
            frmCheckVoucher_Individual.ShowDialog()
            Me.Close()
        Else
            MsgBox("Check number already exist!. Please check another", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Invalid Check Number")
            txtChkNo.SelectAll()
            txtChkNo.Select()
        End If
    End Sub

    Private Function savecheckno()
        Dim sSQLCmd As String = "usp_t_checkno_save "
        sSQLCmd &= " @fxKeyPayBills='" & gItemsKeys & "' "
        sSQLCmd &= ",@fcChkNo='" & gCheckNumber & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            Return True
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
            Return False
        End Try
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtChkNo.Text = "0"
        ComboBox1.Text = ""
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedItem = "Check" Then
            txtChkNo.Enabled = True
        Else
            txtChkNo.Enabled = False
        End If
    End Sub

    Private Sub frmCheckNo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ComboBox1.SelectedIndex = 0
        ''TRC UPDATE ONLY
        Try
            txtChkNo.Text = CInt(getchecknumber()) + 1
        Catch ex As Exception
            txtChkNo.Text = 0
        End Try
    End Sub

    ''TRC UPDATE ONLY
    Private Function getchecknumber() As String

        Dim sResult As String = "0"
        Try
            Dim sSQLCmd As String = "select top 1 isnull(fcChkNo,'') from dbo.tPaybills order by fcChkNo desc  "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sResult = rd.Item(0).ToString
                End While
            End Using

            Return sResult

        Catch ex As Exception
            Return sResult
        End Try

    End Function


End Class