<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_undefund_goto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cbo_goto_whichfield = New System.Windows.Forms.ComboBox
        Me.cbo_goto_searchfor = New System.Windows.Forms.ComboBox
        Me.btn_goto_cancel = New System.Windows.Forms.Button
        Me.btn_goto_Ok = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Account: Undeposited Funds"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Which Field "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Search For"
        '
        'cbo_goto_whichfield
        '
        Me.cbo_goto_whichfield.FormattingEnabled = True
        Me.cbo_goto_whichfield.Location = New System.Drawing.Point(87, 36)
        Me.cbo_goto_whichfield.Name = "cbo_goto_whichfield"
        Me.cbo_goto_whichfield.Size = New System.Drawing.Size(183, 21)
        Me.cbo_goto_whichfield.TabIndex = 3
        '
        'cbo_goto_searchfor
        '
        Me.cbo_goto_searchfor.FormattingEnabled = True
        Me.cbo_goto_searchfor.Location = New System.Drawing.Point(87, 67)
        Me.cbo_goto_searchfor.Name = "cbo_goto_searchfor"
        Me.cbo_goto_searchfor.Size = New System.Drawing.Size(183, 21)
        Me.cbo_goto_searchfor.TabIndex = 4
        '
        'btn_goto_cancel
        '
        Me.btn_goto_cancel.Location = New System.Drawing.Point(194, 104)
        Me.btn_goto_cancel.Name = "btn_goto_cancel"
        Me.btn_goto_cancel.Size = New System.Drawing.Size(76, 23)
        Me.btn_goto_cancel.TabIndex = 27
        Me.btn_goto_cancel.Text = "Cancel"
        Me.btn_goto_cancel.UseVisualStyleBackColor = True
        '
        'btn_goto_Ok
        '
        Me.btn_goto_Ok.Location = New System.Drawing.Point(112, 104)
        Me.btn_goto_Ok.Name = "btn_goto_Ok"
        Me.btn_goto_Ok.Size = New System.Drawing.Size(76, 23)
        Me.btn_goto_Ok.TabIndex = 28
        Me.btn_goto_Ok.Text = "OK"
        Me.btn_goto_Ok.UseVisualStyleBackColor = True
        '
        'frm_undefund_goto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 139)
        Me.Controls.Add(Me.btn_goto_Ok)
        Me.Controls.Add(Me.btn_goto_cancel)
        Me.Controls.Add(Me.cbo_goto_searchfor)
        Me.Controls.Add(Me.cbo_goto_whichfield)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_undefund_goto"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Go to"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbo_goto_whichfield As System.Windows.Forms.ComboBox
    Friend WithEvents cbo_goto_searchfor As System.Windows.Forms.ComboBox
    Friend WithEvents btn_goto_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_goto_Ok As System.Windows.Forms.Button
End Class
