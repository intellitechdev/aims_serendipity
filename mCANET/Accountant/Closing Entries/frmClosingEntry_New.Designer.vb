﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClosingEntry_New
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdList = New System.Windows.Forms.DataGridView()
        Me.acntKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnDebit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FnCredit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdList
        '
        Me.grdList.BackgroundColor = System.Drawing.Color.White
        Me.grdList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.grdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.acntKey, Me.acntCode, Me.acntTitle, Me.FnDebit, Me.FnCredit})
        Me.grdList.Location = New System.Drawing.Point(12, 100)
        Me.grdList.Name = "grdList"
        Me.grdList.ReadOnly = True
        Me.grdList.RowHeadersVisible = False
        Me.grdList.Size = New System.Drawing.Size(737, 258)
        Me.grdList.TabIndex = 0
        '
        'acntKey
        '
        Me.acntKey.HeaderText = "acntKey"
        Me.acntKey.Name = "acntKey"
        Me.acntKey.ReadOnly = True
        Me.acntKey.Visible = False
        '
        'acntCode
        '
        Me.acntCode.HeaderText = "Code"
        Me.acntCode.Name = "acntCode"
        Me.acntCode.ReadOnly = True
        Me.acntCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'acntTitle
        '
        Me.acntTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.acntTitle.HeaderText = "Account Title"
        Me.acntTitle.Name = "acntTitle"
        Me.acntTitle.ReadOnly = True
        Me.acntTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'FnDebit
        '
        Me.FnDebit.HeaderText = "Debit"
        Me.FnDebit.Name = "FnDebit"
        Me.FnDebit.ReadOnly = True
        Me.FnDebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FnDebit.Width = 120
        '
        'FnCredit
        '
        Me.FnCredit.HeaderText = "Credit"
        Me.FnCredit.Name = "FnCredit"
        Me.FnCredit.ReadOnly = True
        Me.FnCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FnCredit.Width = 120
        '
        'dtpFrom
        '
        Me.dtpFrom.CustomFormat = "MMMM dd, yyyy"
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFrom.Location = New System.Drawing.Point(65, 20)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(200, 23)
        Me.dtpFrom.TabIndex = 1
        '
        'dtpTo
        '
        Me.dtpTo.CustomFormat = "MMMM dd, yyyy"
        Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTo.Location = New System.Drawing.Point(65, 49)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(200, 23)
        Me.dtpTo.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 15)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "From"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "To"
        '
        'frmClosingEntry_New
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(761, 413)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpTo)
        Me.Controls.Add(Me.dtpFrom)
        Me.Controls.Add(Me.grdList)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmClosingEntry_New"
        Me.Text = "Closing Entry"
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdList As System.Windows.Forms.DataGridView
    Friend WithEvents acntKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnDebit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FnCredit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
