<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClosingEntries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClosingEntries))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.grdClosingDetails = New System.Windows.Forms.DataGridView
        Me.panelTopDetails = New System.Windows.Forms.Panel
        Me.lblClosingDesc = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.panelBottomDetails = New System.Windows.Forms.Panel
        Me.txtEntryNoFilter = New System.Windows.Forms.TextBox
        Me.lblReference = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblPeriodFrom = New System.Windows.Forms.Label
        Me.btnFilter = New System.Windows.Forms.Button
        Me.dtPeriodTo = New System.Windows.Forms.DateTimePicker
        Me.dtPeriodFrom = New System.Windows.Forms.DateTimePicker
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.grdClosingList = New System.Windows.Forms.DataGridView
        Me.panelBottomList = New System.Windows.Forms.Panel
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnPost = New System.Windows.Forms.Button
        Me.btnCreate = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.bgwCreateEntries = New System.ComponentModel.BackgroundWorker
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.grdClosingDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelTopDetails.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelBottomDetails.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdClosingList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelBottomList.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.grdClosingDetails)
        Me.SplitContainer1.Panel1.Controls.Add(Me.panelTopDetails)
        Me.SplitContainer1.Panel1.Controls.Add(Me.panelBottomDetails)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.picLoading)
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdClosingList)
        Me.SplitContainer1.Panel2.Controls.Add(Me.panelBottomList)
        Me.SplitContainer1.Size = New System.Drawing.Size(759, 447)
        Me.SplitContainer1.SplitterDistance = 228
        Me.SplitContainer1.TabIndex = 0
        '
        'grdClosingDetails
        '
        Me.grdClosingDetails.AllowUserToAddRows = False
        Me.grdClosingDetails.AllowUserToDeleteRows = False
        Me.grdClosingDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdClosingDetails.BackgroundColor = System.Drawing.Color.White
        Me.grdClosingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdClosingDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdClosingDetails.Location = New System.Drawing.Point(0, 49)
        Me.grdClosingDetails.Name = "grdClosingDetails"
        Me.grdClosingDetails.Size = New System.Drawing.Size(759, 137)
        Me.grdClosingDetails.TabIndex = 1
        '
        'panelTopDetails
        '
        Me.panelTopDetails.Controls.Add(Me.lblClosingDesc)
        Me.panelTopDetails.Controls.Add(Me.PictureBox1)
        Me.panelTopDetails.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTopDetails.Location = New System.Drawing.Point(0, 0)
        Me.panelTopDetails.Name = "panelTopDetails"
        Me.panelTopDetails.Size = New System.Drawing.Size(759, 49)
        Me.panelTopDetails.TabIndex = 0
        '
        'lblClosingDesc
        '
        Me.lblClosingDesc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblClosingDesc.BackColor = System.Drawing.SystemColors.Control
        Me.lblClosingDesc.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClosingDesc.Location = New System.Drawing.Point(49, 16)
        Me.lblClosingDesc.Name = "lblClosingDesc"
        Me.lblClosingDesc.Size = New System.Drawing.Size(698, 22)
        Me.lblClosingDesc.TabIndex = 1
        Me.lblClosingDesc.Text = "Automatically creates closing entries on a specified period of time."
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.CSAcctg.My.Resources.Resources.invoice
        Me.PictureBox1.Location = New System.Drawing.Point(12, 9)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(31, 34)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'panelBottomDetails
        '
        Me.panelBottomDetails.Controls.Add(Me.txtEntryNoFilter)
        Me.panelBottomDetails.Controls.Add(Me.lblReference)
        Me.panelBottomDetails.Controls.Add(Me.Label1)
        Me.panelBottomDetails.Controls.Add(Me.lblPeriodFrom)
        Me.panelBottomDetails.Controls.Add(Me.btnFilter)
        Me.panelBottomDetails.Controls.Add(Me.dtPeriodTo)
        Me.panelBottomDetails.Controls.Add(Me.dtPeriodFrom)
        Me.panelBottomDetails.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelBottomDetails.Location = New System.Drawing.Point(0, 186)
        Me.panelBottomDetails.Name = "panelBottomDetails"
        Me.panelBottomDetails.Size = New System.Drawing.Size(759, 42)
        Me.panelBottomDetails.TabIndex = 0
        '
        'txtEntryNoFilter
        '
        Me.txtEntryNoFilter.Location = New System.Drawing.Point(417, 9)
        Me.txtEntryNoFilter.Name = "txtEntryNoFilter"
        Me.txtEntryNoFilter.Size = New System.Drawing.Size(137, 23)
        Me.txtEntryNoFilter.TabIndex = 7
        '
        'lblReference
        '
        Me.lblReference.AutoSize = True
        Me.lblReference.Location = New System.Drawing.Point(348, 13)
        Me.lblReference.Name = "lblReference"
        Me.lblReference.Size = New System.Drawing.Size(63, 15)
        Me.lblReference.TabIndex = 6
        Me.lblReference.Text = "Reference:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(207, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 15)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "to"
        '
        'lblPeriodFrom
        '
        Me.lblPeriodFrom.AutoSize = True
        Me.lblPeriodFrom.Location = New System.Drawing.Point(10, 13)
        Me.lblPeriodFrom.Name = "lblPeriodFrom"
        Me.lblPeriodFrom.Size = New System.Drawing.Size(75, 15)
        Me.lblPeriodFrom.TabIndex = 4
        Me.lblPeriodFrom.Text = "Date Closed:"
        '
        'btnFilter
        '
        Me.btnFilter.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.btnFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFilter.Location = New System.Drawing.Point(560, 4)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(88, 33)
        Me.btnFilter.TabIndex = 3
        Me.btnFilter.Text = "Filter List"
        Me.btnFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'dtPeriodTo
        '
        Me.dtPeriodTo.CustomFormat = "M/d/yyyy"
        Me.dtPeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPeriodTo.Location = New System.Drawing.Point(231, 9)
        Me.dtPeriodTo.Name = "dtPeriodTo"
        Me.dtPeriodTo.Size = New System.Drawing.Size(111, 23)
        Me.dtPeriodTo.TabIndex = 1
        '
        'dtPeriodFrom
        '
        Me.dtPeriodFrom.CustomFormat = "M/d/yyyy"
        Me.dtPeriodFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPeriodFrom.Location = New System.Drawing.Point(90, 9)
        Me.dtPeriodFrom.Name = "dtPeriodFrom"
        Me.dtPeriodFrom.Size = New System.Drawing.Size(111, 23)
        Me.dtPeriodFrom.TabIndex = 0
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.ajax_loader
        Me.picLoading.Location = New System.Drawing.Point(210, 61)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(316, 30)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 2
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'grdClosingList
        '
        Me.grdClosingList.AllowUserToAddRows = False
        Me.grdClosingList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdClosingList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdClosingList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdClosingList.Location = New System.Drawing.Point(0, 0)
        Me.grdClosingList.Name = "grdClosingList"
        Me.grdClosingList.Size = New System.Drawing.Size(759, 162)
        Me.grdClosingList.TabIndex = 1
        '
        'panelBottomList
        '
        Me.panelBottomList.Controls.Add(Me.btnPreview)
        Me.panelBottomList.Controls.Add(Me.btnPost)
        Me.panelBottomList.Controls.Add(Me.btnCreate)
        Me.panelBottomList.Controls.Add(Me.btnClose)
        Me.panelBottomList.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelBottomList.Location = New System.Drawing.Point(0, 162)
        Me.panelBottomList.Name = "panelBottomList"
        Me.panelBottomList.Size = New System.Drawing.Size(759, 53)
        Me.panelBottomList.TabIndex = 0
        '
        'btnPreview
        '
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPreview.Location = New System.Drawing.Point(12, 8)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(113, 33)
        Me.btnPreview.TabIndex = 3
        Me.btnPreview.Text = "Preview Entry"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnPost
        '
        Me.btnPost.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPost.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPost.Location = New System.Drawing.Point(573, 8)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(94, 33)
        Me.btnPost.TabIndex = 2
        Me.btnPost.Text = "Post Entry"
        Me.btnPost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'btnCreate
        '
        Me.btnCreate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCreate.Image = Global.CSAcctg.My.Resources.Resources.report
        Me.btnCreate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCreate.Location = New System.Drawing.Point(403, 8)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(164, 33)
        Me.btnCreate.TabIndex = 1
        Me.btnCreate.Text = "Create Closing Entries"
        Me.btnCreate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(673, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(74, 33)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'bgwCreateEntries
        '
        '
        'frmClosingEntries
        '
        Me.AcceptButton = Me.btnFilter
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(759, 447)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(775, 485)
        Me.Name = "frmClosingEntries"
        Me.Text = "Closing Entries"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.grdClosingDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelTopDetails.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelBottomDetails.ResumeLayout(False)
        Me.panelBottomDetails.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdClosingList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelBottomList.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents panelTopDetails As System.Windows.Forms.Panel
    Friend WithEvents panelBottomDetails As System.Windows.Forms.Panel
    Friend WithEvents panelBottomList As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents btnPost As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents grdClosingDetails As System.Windows.Forms.DataGridView
    Friend WithEvents grdClosingList As System.Windows.Forms.DataGridView
    Friend WithEvents dtPeriodFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodFrom As System.Windows.Forms.Label
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents dtPeriodTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEntryNoFilter As System.Windows.Forms.TextBox
    Friend WithEvents lblReference As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblClosingDesc As System.Windows.Forms.Label
    Friend WithEvents bgwCreateEntries As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
End Class
