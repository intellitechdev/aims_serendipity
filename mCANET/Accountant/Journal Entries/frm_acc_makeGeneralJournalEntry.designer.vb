<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_makeGeneralJournalEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_makeGeneralJournalEntry))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ts_acc_makedeposit = New System.Windows.Forms.ToolStrip
        Me.ts_genjour_previous = New System.Windows.Forms.ToolStripButton
        Me.ts_genjour_next = New System.Windows.Forms.ToolStripButton
        Me.ts_genjour_print = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_preview = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_dep_depositSummary = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_spliter1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_spliter2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_genjour_find = New System.Windows.Forms.ToolStripButton
        Me.ts_genjour_history = New System.Windows.Forms.ToolStripButton
        Me.ts_genjour_reverse = New System.Windows.Forms.ToolStripButton
        Me.ts_genjour_reports = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_genjour_adjustJE = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_genjour_lastmonth = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_genjour_lastfiscalyear = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_genjour_thisfiscalyear = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_genjour_allentries = New System.Windows.Forms.ToolStripMenuItem
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.txtClassRefno = New System.Windows.Forms.TextBox
        Me.LblClassName = New System.Windows.Forms.Label
        Me.BtnBrowseClass = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txt_genjour_totalcredit = New System.Windows.Forms.TextBox
        Me.txt_genjour_totaldebit = New System.Windows.Forms.TextBox
        Me.ListDeleted = New System.Windows.Forms.ListBox
        Me.btnDeleteRow = New System.Windows.Forms.Button
        Me.grdGenJournalDetails = New System.Windows.Forms.DataGridView
        Me.chk_genjour_adjEntry = New System.Windows.Forms.CheckBox
        Me.txtJVNumber = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.dte_genjour_date = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnDeleteJV = New System.Windows.Forms.Button
        Me.btnDisplay = New System.Windows.Forms.Button
        Me.dtTo = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dtFrom = New System.Windows.Forms.DateTimePicker
        Me.grdListofEntries = New System.Windows.Forms.DataGridView
        Me.btn_genjour_saveClose = New System.Windows.Forms.Button
        Me.btn_genjour_saveNew = New System.Windows.Forms.Button
        Me.btn_genjour_New = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.ts_acc_makedeposit.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdGenJournalDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdListofEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ts_acc_makedeposit
        '
        Me.ts_acc_makedeposit.BackColor = System.Drawing.Color.White
        Me.ts_acc_makedeposit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ts_acc_makedeposit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_genjour_previous, Me.ts_genjour_next, Me.ts_genjour_print, Me.ts_spliter1, Me.ts_spliter2, Me.ts_genjour_find, Me.ts_genjour_history, Me.ts_genjour_reverse, Me.ts_genjour_reports})
        Me.ts_acc_makedeposit.Location = New System.Drawing.Point(0, 0)
        Me.ts_acc_makedeposit.Name = "ts_acc_makedeposit"
        Me.ts_acc_makedeposit.Size = New System.Drawing.Size(931, 25)
        Me.ts_acc_makedeposit.TabIndex = 2
        Me.ts_acc_makedeposit.Text = "ToolStrip1"
        '
        'ts_genjour_previous
        '
        Me.ts_genjour_previous.Image = CType(resources.GetObject("ts_genjour_previous.Image"), System.Drawing.Image)
        Me.ts_genjour_previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_previous.Name = "ts_genjour_previous"
        Me.ts_genjour_previous.Size = New System.Drawing.Size(84, 22)
        Me.ts_genjour_previous.Text = "Previous"
        Me.ts_genjour_previous.Visible = False
        '
        'ts_genjour_next
        '
        Me.ts_genjour_next.Image = CType(resources.GetObject("ts_genjour_next.Image"), System.Drawing.Image)
        Me.ts_genjour_next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_next.Name = "ts_genjour_next"
        Me.ts_genjour_next.Size = New System.Drawing.Size(57, 22)
        Me.ts_genjour_next.Text = "Next"
        Me.ts_genjour_next.Visible = False
        '
        'ts_genjour_print
        '
        Me.ts_genjour_print.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_preview, Me.ts_dep_depositSummary})
        Me.ts_genjour_print.Image = CType(resources.GetObject("ts_genjour_print.Image"), System.Drawing.Image)
        Me.ts_genjour_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_print.Name = "ts_genjour_print"
        Me.ts_genjour_print.Size = New System.Drawing.Size(67, 22)
        Me.ts_genjour_print.Text = "Print"
        '
        'ts_preview
        '
        Me.ts_preview.Name = "ts_preview"
        Me.ts_preview.Size = New System.Drawing.Size(189, 22)
        Me.ts_preview.Text = "Preview"
        '
        'ts_dep_depositSummary
        '
        Me.ts_dep_depositSummary.Name = "ts_dep_depositSummary"
        Me.ts_dep_depositSummary.Size = New System.Drawing.Size(189, 22)
        Me.ts_dep_depositSummary.Text = "Deposit Summary"
        Me.ts_dep_depositSummary.Visible = False
        '
        'ts_spliter1
        '
        Me.ts_spliter1.Name = "ts_spliter1"
        Me.ts_spliter1.Size = New System.Drawing.Size(6, 25)
        Me.ts_spliter1.Visible = False
        '
        'ts_spliter2
        '
        Me.ts_spliter2.Name = "ts_spliter2"
        Me.ts_spliter2.Size = New System.Drawing.Size(6, 25)
        Me.ts_spliter2.Visible = False
        '
        'ts_genjour_find
        '
        Me.ts_genjour_find.Image = CType(resources.GetObject("ts_genjour_find.Image"), System.Drawing.Image)
        Me.ts_genjour_find.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_find.Name = "ts_genjour_find"
        Me.ts_genjour_find.Size = New System.Drawing.Size(55, 22)
        Me.ts_genjour_find.Text = "Find"
        Me.ts_genjour_find.Visible = False
        '
        'ts_genjour_history
        '
        Me.ts_genjour_history.Image = CType(resources.GetObject("ts_genjour_history.Image"), System.Drawing.Image)
        Me.ts_genjour_history.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_history.Name = "ts_genjour_history"
        Me.ts_genjour_history.Size = New System.Drawing.Size(184, 22)
        Me.ts_genjour_history.Text = "&Journal Voucher Posting"
        '
        'ts_genjour_reverse
        '
        Me.ts_genjour_reverse.Image = CType(resources.GetObject("ts_genjour_reverse.Image"), System.Drawing.Image)
        Me.ts_genjour_reverse.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_reverse.Name = "ts_genjour_reverse"
        Me.ts_genjour_reverse.Size = New System.Drawing.Size(80, 22)
        Me.ts_genjour_reverse.Text = "Reverse"
        Me.ts_genjour_reverse.Visible = False
        '
        'ts_genjour_reports
        '
        Me.ts_genjour_reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_genjour_adjustJE, Me.ts_genjour_lastmonth, Me.ts_genjour_lastfiscalyear, Me.ts_genjour_thisfiscalyear, Me.ts_genjour_allentries})
        Me.ts_genjour_reports.Image = CType(resources.GetObject("ts_genjour_reports.Image"), System.Drawing.Image)
        Me.ts_genjour_reports.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_genjour_reports.Name = "ts_genjour_reports"
        Me.ts_genjour_reports.Size = New System.Drawing.Size(86, 22)
        Me.ts_genjour_reports.Text = "Reports"
        Me.ts_genjour_reports.Visible = False
        '
        'ts_genjour_adjustJE
        '
        Me.ts_genjour_adjustJE.Name = "ts_genjour_adjustJE"
        Me.ts_genjour_adjustJE.Size = New System.Drawing.Size(228, 22)
        Me.ts_genjour_adjustJE.Text = "Adjust Journal Entries"
        '
        'ts_genjour_lastmonth
        '
        Me.ts_genjour_lastmonth.Name = "ts_genjour_lastmonth"
        Me.ts_genjour_lastmonth.Size = New System.Drawing.Size(228, 22)
        Me.ts_genjour_lastmonth.Text = "Last Month"
        '
        'ts_genjour_lastfiscalyear
        '
        Me.ts_genjour_lastfiscalyear.Name = "ts_genjour_lastfiscalyear"
        Me.ts_genjour_lastfiscalyear.Size = New System.Drawing.Size(228, 22)
        Me.ts_genjour_lastfiscalyear.Text = "Last Fiscal Quarter"
        '
        'ts_genjour_thisfiscalyear
        '
        Me.ts_genjour_thisfiscalyear.Name = "ts_genjour_thisfiscalyear"
        Me.ts_genjour_thisfiscalyear.Size = New System.Drawing.Size(228, 22)
        Me.ts_genjour_thisfiscalyear.Text = "This Fiscal Year to Date"
        '
        'ts_genjour_allentries
        '
        Me.ts_genjour_allentries.Name = "ts_genjour_allentries"
        Me.ts_genjour_allentries.Size = New System.Drawing.Size(228, 22)
        Me.ts_genjour_allentries.Text = "All Entries"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 25)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.grdGenJournalDetails)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtClassRefno)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblClassName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BtnBrowseClass)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ListDeleted)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnDeleteRow)
        Me.SplitContainer1.Panel1.Controls.Add(Me.chk_genjour_adjEntry)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtJVNumber)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dte_genjour_date)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnDeleteJV)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnDisplay)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dtTo)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dtFrom)
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdListofEntries)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_genjour_saveClose)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_genjour_saveNew)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_genjour_New)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Size = New System.Drawing.Size(931, 484)
        Me.SplitContainer1.SplitterDistance = 214
        Me.SplitContainer1.TabIndex = 40
        '
        'txtClassRefno
        '
        Me.txtClassRefno.Enabled = False
        Me.txtClassRefno.Location = New System.Drawing.Point(627, 47)
        Me.txtClassRefno.Name = "txtClassRefno"
        Me.txtClassRefno.Size = New System.Drawing.Size(185, 21)
        Me.txtClassRefno.TabIndex = 54
        Me.txtClassRefno.Visible = False
        '
        'LblClassName
        '
        Me.LblClassName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblClassName.BackColor = System.Drawing.Color.White
        Me.LblClassName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LblClassName.Location = New System.Drawing.Point(660, 6)
        Me.LblClassName.Name = "LblClassName"
        Me.LblClassName.Size = New System.Drawing.Size(185, 21)
        Me.LblClassName.TabIndex = 53
        '
        'BtnBrowseClass
        '
        Me.BtnBrowseClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnBrowseClass.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowseClass.Location = New System.Drawing.Point(851, 4)
        Me.BtnBrowseClass.Name = "BtnBrowseClass"
        Me.BtnBrowseClass.Size = New System.Drawing.Size(66, 27)
        Me.BtnBrowseClass.TabIndex = 52
        Me.BtnBrowseClass.Text = "Browse"
        Me.BtnBrowseClass.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(610, 12)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 51
        Me.Label8.Text = "Class:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txt_genjour_totalcredit)
        Me.GroupBox1.Controls.Add(Me.txt_genjour_totaldebit)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(20, 166)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(341, 36)
        Me.GroupBox1.TabIndex = 50
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Total"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(183, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Credit"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(40, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Debit"
        '
        'txt_genjour_totalcredit
        '
        Me.txt_genjour_totalcredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_genjour_totalcredit.BackColor = System.Drawing.Color.White
        Me.txt_genjour_totalcredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_genjour_totalcredit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_genjour_totalcredit.Location = New System.Drawing.Point(239, 19)
        Me.txt_genjour_totalcredit.Name = "txt_genjour_totalcredit"
        Me.txt_genjour_totalcredit.Size = New System.Drawing.Size(91, 14)
        Me.txt_genjour_totalcredit.TabIndex = 51
        Me.txt_genjour_totalcredit.Text = "0.00"
        Me.txt_genjour_totalcredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_genjour_totaldebit
        '
        Me.txt_genjour_totaldebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_genjour_totaldebit.BackColor = System.Drawing.Color.White
        Me.txt_genjour_totaldebit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_genjour_totaldebit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_genjour_totaldebit.Location = New System.Drawing.Point(83, 19)
        Me.txt_genjour_totaldebit.Name = "txt_genjour_totaldebit"
        Me.txt_genjour_totaldebit.Size = New System.Drawing.Size(94, 14)
        Me.txt_genjour_totaldebit.TabIndex = 50
        Me.txt_genjour_totaldebit.Text = "0.00"
        Me.txt_genjour_totaldebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ListDeleted
        '
        Me.ListDeleted.FormattingEnabled = True
        Me.ListDeleted.Location = New System.Drawing.Point(429, 47)
        Me.ListDeleted.Name = "ListDeleted"
        Me.ListDeleted.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.ListDeleted.Size = New System.Drawing.Size(169, 69)
        Me.ListDeleted.TabIndex = 47
        Me.ListDeleted.Visible = False
        '
        'btnDeleteRow
        '
        Me.btnDeleteRow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteRow.Enabled = False
        Me.btnDeleteRow.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteRow.Image = Global.CSAcctg.My.Resources.Resources.deletebig
        Me.btnDeleteRow.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteRow.Location = New System.Drawing.Point(797, 166)
        Me.btnDeleteRow.Name = "btnDeleteRow"
        Me.btnDeleteRow.Size = New System.Drawing.Size(123, 36)
        Me.btnDeleteRow.TabIndex = 46
        Me.btnDeleteRow.Text = "Delete Current Row"
        Me.btnDeleteRow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDeleteRow.UseVisualStyleBackColor = True
        '
        'grdGenJournalDetails
        '
        Me.grdGenJournalDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdGenJournalDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdGenJournalDetails.BackgroundColor = System.Drawing.Color.LightSteelBlue
        Me.grdGenJournalDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdGenJournalDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ActiveBorder
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdGenJournalDetails.DefaultCellStyle = DataGridViewCellStyle1
        Me.grdGenJournalDetails.Enabled = False
        Me.grdGenJournalDetails.Location = New System.Drawing.Point(6, 34)
        Me.grdGenJournalDetails.Name = "grdGenJournalDetails"
        Me.grdGenJournalDetails.Size = New System.Drawing.Size(914, 126)
        Me.grdGenJournalDetails.TabIndex = 45
        '
        'chk_genjour_adjEntry
        '
        Me.chk_genjour_adjEntry.AutoSize = True
        Me.chk_genjour_adjEntry.Location = New System.Drawing.Point(417, 10)
        Me.chk_genjour_adjEntry.Name = "chk_genjour_adjEntry"
        Me.chk_genjour_adjEntry.Size = New System.Drawing.Size(96, 17)
        Me.chk_genjour_adjEntry.TabIndex = 43
        Me.chk_genjour_adjEntry.Text = "Adjust Entry"
        Me.chk_genjour_adjEntry.UseVisualStyleBackColor = True
        '
        'txtJVNumber
        '
        Me.txtJVNumber.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtJVNumber.Location = New System.Drawing.Point(243, 8)
        Me.txtJVNumber.Name = "txtJVNumber"
        Me.txtJVNumber.ReadOnly = True
        Me.txtJVNumber.Size = New System.Drawing.Size(151, 21)
        Me.txtJVNumber.TabIndex = 42
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(179, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Entry No."
        '
        'dte_genjour_date
        '
        Me.dte_genjour_date.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dte_genjour_date.Location = New System.Drawing.Point(44, 7)
        Me.dte_genjour_date.Name = "dte_genjour_date"
        Me.dte_genjour_date.Size = New System.Drawing.Size(116, 21)
        Me.dte_genjour_date.TabIndex = 40
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Date"
        '
        'btnDeleteJV
        '
        Me.btnDeleteJV.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteJV.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteJV.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.btnDeleteJV.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteJV.Location = New System.Drawing.Point(389, 228)
        Me.btnDeleteJV.Name = "btnDeleteJV"
        Me.btnDeleteJV.Size = New System.Drawing.Size(146, 31)
        Me.btnDeleteJV.TabIndex = 50
        Me.btnDeleteJV.Text = "Delete JV Entry"
        Me.btnDeleteJV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDeleteJV.UseVisualStyleBackColor = True
        '
        'btnDisplay
        '
        Me.btnDisplay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisplay.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisplay.Location = New System.Drawing.Point(797, 6)
        Me.btnDisplay.Name = "btnDisplay"
        Me.btnDisplay.Size = New System.Drawing.Size(123, 24)
        Me.btnDisplay.TabIndex = 49
        Me.btnDisplay.Text = "Display List"
        Me.btnDisplay.UseVisualStyleBackColor = True
        '
        'dtTo
        '
        Me.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtTo.Location = New System.Drawing.Point(604, 6)
        Me.dtTo.Name = "dtTo"
        Me.dtTo.Size = New System.Drawing.Size(155, 21)
        Me.dtTo.TabIndex = 48
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(577, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 13)
        Me.Label6.TabIndex = 47
        Me.Label6.Text = "To"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(374, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 13)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "From"
        '
        'dtFrom
        '
        Me.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFrom.Location = New System.Drawing.Point(416, 6)
        Me.dtFrom.Name = "dtFrom"
        Me.dtFrom.Size = New System.Drawing.Size(155, 21)
        Me.dtFrom.TabIndex = 45
        Me.dtFrom.Value = New Date(2008, 1, 1, 0, 0, 0, 0)
        '
        'grdListofEntries
        '
        Me.grdListofEntries.AllowUserToDeleteRows = False
        Me.grdListofEntries.AllowUserToOrderColumns = True
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.grdListofEntries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.grdListofEntries.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdListofEntries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdListofEntries.BackgroundColor = System.Drawing.Color.LightSteelBlue
        Me.grdListofEntries.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdListofEntries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdListofEntries.Location = New System.Drawing.Point(6, 44)
        Me.grdListofEntries.Name = "grdListofEntries"
        Me.grdListofEntries.ReadOnly = True
        Me.grdListofEntries.Size = New System.Drawing.Size(914, 178)
        Me.grdListofEntries.TabIndex = 44
        '
        'btn_genjour_saveClose
        '
        Me.btn_genjour_saveClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_genjour_saveClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_genjour_saveClose.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btn_genjour_saveClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_genjour_saveClose.Location = New System.Drawing.Point(541, 228)
        Me.btn_genjour_saveClose.Name = "btn_genjour_saveClose"
        Me.btn_genjour_saveClose.Size = New System.Drawing.Size(122, 31)
        Me.btn_genjour_saveClose.TabIndex = 43
        Me.btn_genjour_saveClose.Text = "Save && &Close"
        Me.btn_genjour_saveClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_genjour_saveClose.UseVisualStyleBackColor = True
        '
        'btn_genjour_saveNew
        '
        Me.btn_genjour_saveNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_genjour_saveNew.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_genjour_saveNew.Image = Global.CSAcctg.My.Resources.Resources.filenew
        Me.btn_genjour_saveNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_genjour_saveNew.Location = New System.Drawing.Point(669, 228)
        Me.btn_genjour_saveNew.Name = "btn_genjour_saveNew"
        Me.btn_genjour_saveNew.Size = New System.Drawing.Size(122, 31)
        Me.btn_genjour_saveNew.TabIndex = 42
        Me.btn_genjour_saveNew.Text = "&Save && New"
        Me.btn_genjour_saveNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_genjour_saveNew.UseVisualStyleBackColor = True
        '
        'btn_genjour_New
        '
        Me.btn_genjour_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_genjour_New.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_genjour_New.Image = Global.CSAcctg.My.Resources.Resources._new
        Me.btn_genjour_New.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_genjour_New.Location = New System.Drawing.Point(797, 228)
        Me.btn_genjour_New.Name = "btn_genjour_New"
        Me.btn_genjour_New.Size = New System.Drawing.Size(123, 31)
        Me.btn_genjour_New.TabIndex = 41
        Me.btn_genjour_New.Text = "&New"
        Me.btn_genjour_New.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_genjour_New.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(364, 13)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "List of selected General Journal entries: ( Set by Date Range )"
        '
        'frm_acc_makeGeneralJournalEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(931, 509)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.ts_acc_makedeposit)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(906, 541)
        Me.Name = "frm_acc_makeGeneralJournalEntry"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Make General Journal Entries"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ts_acc_makedeposit.ResumeLayout(False)
        Me.ts_acc_makedeposit.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdGenJournalDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdListofEntries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ts_acc_makedeposit As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_genjour_previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_genjour_next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_genjour_print As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_preview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_dep_depositSummary As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_spliter1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_spliter2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_genjour_find As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_genjour_reverse As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_genjour_reports As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_genjour_adjustJE As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_genjour_lastmonth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_genjour_lastfiscalyear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_genjour_thisfiscalyear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_genjour_allentries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_genjour_history As System.Windows.Forms.ToolStripButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents ListDeleted As System.Windows.Forms.ListBox
    Friend WithEvents btnDeleteRow As System.Windows.Forms.Button
    Friend WithEvents grdGenJournalDetails As System.Windows.Forms.DataGridView
    Friend WithEvents chk_genjour_adjEntry As System.Windows.Forms.CheckBox
    Friend WithEvents txtJVNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dte_genjour_date As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnDeleteJV As System.Windows.Forms.Button
    Friend WithEvents btnDisplay As System.Windows.Forms.Button
    Friend WithEvents dtTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents grdListofEntries As System.Windows.Forms.DataGridView
    Friend WithEvents btn_genjour_saveClose As System.Windows.Forms.Button
    Friend WithEvents btn_genjour_saveNew As System.Windows.Forms.Button
    Friend WithEvents btn_genjour_New As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_genjour_totalcredit As System.Windows.Forms.TextBox
    Friend WithEvents txt_genjour_totaldebit As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents LblClassName As System.Windows.Forms.Label
    Friend WithEvents BtnBrowseClass As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtClassRefno As System.Windows.Forms.TextBox
End Class
