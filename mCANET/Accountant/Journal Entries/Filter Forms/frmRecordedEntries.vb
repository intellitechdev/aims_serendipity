﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmRecordedEntries
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim coid As String

    Private Sub frmRecordedEntries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ListDocNum()
        Try
            TextBox1.Text = grdList.Item("fxKeyJVNo", 0).Value.ToString
        Catch ex As Exception
            TextBox1.Text = ""
        End Try
        bLoanForRelease = False
    End Sub

    Private Sub ListDocNum()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_t_tJVEntry_List",
                                      New SqlParameter("@docType", frmGeneralJournalEntries.cboDoctype.Text),
                                      New SqlParameter("@filter", txtSearch.Text),
                                      New SqlParameter("@coid", gCompanyID))

        grdList.DataSource = ds.Tables(0)

        With grdList
            .Columns("fxKeyJVNo").Visible = False
            .Columns("fiEntryNo").HeaderText = "Document Number"
            .Columns("fiEntryNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fiEntryNo").Width = 260
        End With
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call ListDocNum()
    End Sub

    Private Sub EntryDetails()
        Try
            Dim i As Integer = grdList.CurrentRow.Index
            TextBox1.Text = grdList.Item("fxKeyJVNo", i).Value.ToString
            frmGeneralJournalEntries.keyID = TextBox1.Text
            frmGeneralJournalEntries.EntryNo = grdList.Item("fiEntryNo", i).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdList.Click
        EntryDetails()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        frmGeneralJournalEntries.keyID = TextBox1.Text
        Me.DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub CheckIfForRelease()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_CIMS_Loans_CheckIfForRelease",
                                      New SqlParameter("@filter", TextBox1.Text))
        'MsgBox(ds.Tables(0).Rows.Count)
        If ds.Tables(0).Rows.Count <> 0 Then
            bLoanForRelease = True
        Else
            bLoanForRelease = False
        End If
    End Sub

    Private Sub grdList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdList.DoubleClick
        frmGeneralJournalEntries.keyID = TextBox1.Text
        Me.DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub grdList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                frmGeneralJournalEntries.keyID = TextBox1.Text
                Me.DialogResult = Windows.Forms.DialogResult.Yes
        End Select
    End Sub

    Private Sub grdList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdList.SelectionChanged
        Call EntryDetails()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class