﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports CrystalDecisions.Shared
Public Class FrmSoaRegister
    Private AccountKey As String
    Private EmployeeId As String
    Private fxAccountKeyRegister As String
    Private mycon As New Clsappconfiguration
    Dim Isclosed As Boolean
#Region "Property"
    Public Property GetAccountID() As String
        Get
            Return AccountKey
        End Get
        Set(ByVal value As String)
            AccountKey = value
        End Set
    End Property

    Public Property GetfxAccountKeyRegister() As String
        Get
            Return fxAccountKeyRegister
        End Get
        Set(ByVal value As String)
            fxAccountKeyRegister = value
        End Set
    End Property
    Public Property GetEmployeeID() As String
        Get
            Return EmployeeId
        End Get
        Set(ByVal value As String)
            EmployeeId = value
        End Set
    End Property
#End Region

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub EditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditToolStripMenuItem.Click
        If EditToolStripMenuItem.Text = "Update" Then
            Update_SoaRegister(fxAccountKeyRegister, txtAccountNo.Text, AccountKey, EmployeeId, Convert.ToDecimal(txtSchedule.Text), CBclose.Checked, txtName.Text)
            Load_SoaRegister(gCompanyID)
            AuditTrail_Save("ACCOUNT REGISTER", "Update account account register " & txtAccountNo.Text & " | " & txtAccountTitle.Text & " | " & txtName.Text)
            SaveToolStripMenuItem.Enabled = True
            DeleteToolStripMenuItem.Enabled = True
            CloseToolStripMenuItem.Text = "Close"
            EditToolStripMenuItem.Text = "Edit"
            btnBrowseNo.Enabled = False
            btnBrowseTitle.Enabled = False
            btnName.Enabled = False

            txtSchedule.Enabled = False
            txtSchedule.Clear()
            txtAccountCode.Clear()
            txtAccountTitle.Clear()
            txtAccountNo.Clear()
            txtName.Clear()
            CBclose.Checked = False
            CBclose.Enabled = False
            Exit Sub
        End If
        Panel1.Enabled = True
        If EditToolStripMenuItem.Text = "Edit" Then
            EditToolStripMenuItem.Text = "Update"
            CloseToolStripMenuItem.Text = "Cancel"
            SaveToolStripMenuItem.Enabled = False
            DeleteToolStripMenuItem.Enabled = False
            txtSchedule.Enabled = True
            btnBrowseNo.Enabled = True
            btnBrowseTitle.Enabled = True
            btnName.Enabled = True

            CBclose.Enabled = True
            Exit Sub
        End If
    End Sub

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click

        Dim AccountKey As String

        If GetAccountID <> "" Then
            AccountKey = GetAccountID
        Else
            AccountKey = Guid.NewGuid.ToString
        End If

        Panel1.Enabled = False
        Add_SOARegister(AccountKey, txtAccountNo.Text, EmployeeId, Convert.ToDecimal(txtSchedule.Text), CBclose.Checked, gCompanyID)
        UpdateAccountInAccounting(txtAccountNo.Text, Date.Now)
        AuditTrail_Save("ACCOUNT REGISTER", "Create new account account register " & txtAccountNo.Text & " | " & txtAccountTitle.Text & " | " & txtName.Text)
        dgvSoa.Columns.Clear()
        Load_SoaRegister(gCompanyID)
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        Panel1.Enabled = True
        btnBrowseNo.Enabled = True
        btnBrowseTitle.Enabled = True
        btnName.Enabled = True
        'EditToolStripMenuItem.Enabled = False
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        If CloseToolStripMenuItem.Text = "Cancel" Then
            CloseToolStripMenuItem.Text = "Close"
            EditToolStripMenuItem.Text = "Edit"
            Panel1.Enabled = False
            btnBrowseNo.Enabled = False
            btnBrowseTitle.Enabled = False
            btnName.Enabled = False
            txtAccountNo.Clear()
            txtAccountCode.Clear()
            txtAccountTitle.Clear()
            txtName.Clear()
            txtSchedule.Text = "0.00"
            CBClosedList.Checked = False
            CBclose.Checked = False
        Else
            Me.Close()
        End If
    End Sub
    Private Sub Add_SOARegister(ByVal fxKey_Account As String, ByVal fcDocNumber As String,
                                       ByVal fk_Employee As String, ByVal fnSchedule As Double, ByVal fbClosed As Boolean, ByVal fxkey_coid As String)
        'Dim MyGuids As New Guid(fk_Employee)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim MyGuids As New Guid(fxkey_coid)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Member_SoaRegister_Insert",
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fxKey_coid", MyGuids),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fcEmployeeNo", fk_Employee),
                                      New SqlParameter("@fcFullname", txtName.Text),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed))
            MessageBox.Show("Record Succesfully Added!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Add Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub UpdateAccountInAccounting(ByVal fcDocNumber As String, ByVal fdDateUsed As Date)
        Try
            Dim MyGuids As New Guid(gCompanyID)
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_UpdateAccount",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fdDateUsed", fdDateUsed),
                                      New SqlParameter("@co_id", MyGuids))
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Document Number....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Delete_SoaRegister(ByVal fxKey_Account As String)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim coid As New Guid(gCompanyID)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Member_SoaRegister_Delete",
                                      New SqlParameter("@fxKeySoaRegister", MyGuid),
                                      New SqlParameter("@coid", coid))
            MessageBox.Show("Record Succesfully Deleted!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Delete Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnBrowseNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseNo.Click
        frmMasterfile_SoaNumber.GetID = 3
        frmMasterfile_SoaNumber.ShowDialog()
        If frmMasterfile_SoaNumber.GrdAccounts.SelectedCells.Count <> 0 Then
            If frmMasterfile_SoaNumber.DialogResult = DialogResult.OK Then
                For Each row As DataGridViewRow In frmMasterfile_SoaNumber.GrdAccounts.SelectedRows
                    txtAccountNo.Text = row.Cells(0).Value.ToString
                Next
            End If
        End If
    End Sub

    Private Sub btnBrowseTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseTitle.Click
        frmMasterFile_Accounts.GetActive = 2
        frmMasterFile_Accounts.ShowDialog()
        With frmMasterFile_Accounts
            'If .Active = 1 Then
            'If .GrdAccounts.SelectedCells.Count <> 0 Then
            'If frmMasterFile_Accounts.DialogResult = DialogResult.OK Then
            '    For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
            '        frmMasterFile_DebitCredit.fxid = row.Cells(0).Value.ToString
            '        frmMasterFile_DebitCredit.txtaccountcode.Text = row.Cells(1).Value.ToString
            '        frmMasterFile_DebitCredit.txtaccountname.Text = row.Cells(2).Value.ToString
            '    Next
            '    Me.Close()
            'End If
            'ElseIf 
            If .Active = 2 Then
                If .GrdAccounts.SelectedCells.Count <> 0 Then
                    If frmMasterFile_Accounts.DialogResult = DialogResult.OK Then
                        For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
                            GetAccountID = row.Cells(0).Value.ToString
                            txtAccountCode.Text = row.Cells(1).Value.ToString
                            txtAccountTitle.Text = row.Cells(2).Value.ToString
                            TextBox1.Text = row.Cells(0).Value.ToString()
                        Next
                    End If
                End If
            End If
        End With
    End Sub

    Private Sub btnName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnName.Click
        frmMember_List.ShowDialog()
        With frmMember_List
            If .GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In .GrdAccounts.SelectedRows
                    GetEmployeeID = row.Cells(1).Value.ToString
                    txtName.Text = row.Cells(2).Value.ToString
                Next
            End If
        End With
    End Sub

    Private Sub FrmSoaRegister_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'dgvSoa.Columns.Clear()
        Load_SoaRegister(gCompanyID)
    End Sub
    Private Sub Load_SoaRegister(ByVal fxkey_coid As String)
        Try
            Dim MyGuids As New Guid(fxkey_coid)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "_Member_SoaRegister_ListClosed",
                                                         New SqlParameter("@coid", MyGuids))

            With dgvSoa
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False

                .Columns(2).HeaderText = "Soa Number"
                .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
                '.Columns(2).Width = 100
                .Columns(2).ReadOnly = True
                .Columns(3).HeaderText = "ID"
                .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
                '.Columns(3).Width = 100
                .Columns(3).ReadOnly = True
                .Columns(4).HeaderText = "Name"
                .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(4).ReadOnly = True
                '.Columns(4).Width = 200
                .Columns(5).HeaderText = "Account Title"
                .Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(5).ReadOnly = True
                '.Columns(5).Width = 200
                .Columns(6).HeaderText = "Schedule"
                .Columns(6).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(6).ReadOnly = True
                '.Columns(6).Width = 80
                .Columns(7).HeaderText = "Closed"
                .Columns(7).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
                '.Columns(7).Width = 80
            End With
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Load SOA Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Update_SoaRegister(ByVal fxKeyAccountRegister As String, ByVal fcDocNumber As String, ByVal fxKey_Account As String,
                                        ByVal fk_Employee As String, ByVal fnSchedule As Decimal, ByVal fbClosed As Boolean, ByVal Name As String)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim MyGuids As New Guid(fxAccountKeyRegister)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Member_SoaRegister_Update",
                                      New SqlParameter("@fxKeySoaRegister", MyGuids),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fcEmployeeNo", fk_Employee),
                                      New SqlParameter("@fcFullname", Name),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed))
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update SOA Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvSoa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSoa.Click
        For Each Row As DataGridViewRow In dgvSoa.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNo.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtAccountTitle.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
        Next
    End Sub

    Private Sub dgvSoa_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSoa.KeyDown
        For Each Row As DataGridViewRow In dgvSoa.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNo.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtAccountTitle.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
        Next
    End Sub

    Private Sub dgvSoa_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSoa.KeyUp
        For Each Row As DataGridViewRow In dgvSoa.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNo.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtAccountTitle.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
        Next
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
            Delete_SoaRegister(fxAccountKeyRegister)
            Dim sSQLCmd As String = "update mDocNumber Set fdDateUsed= Null where fcDocNumber='" & txtAccountNo.Text & "' "
            SqlHelper.ExecuteScalar(mycon.cnstring, CommandType.Text, sSQLCmd)
            AuditTrail_Save("ACCOUNT REGISTER", "Delete " & txtAccountNo.Text & " | " & txtAccountTitle.Text & " | " & txtName.Text)
            Load_SoaRegister(gCompanyID)
            btnBrowseTitle.Enabled = True
            btnName.Enabled = True

            txtSchedule.Enabled = True
            txtAccountCode.Clear()
            txtAccountTitle.Clear()
            txtAccountNo.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            CBclose.Checked = False
        Else
            Exit Sub
        End If
    End Sub

    Private Sub txtSchedule_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSchedule.GotFocus
        txtSchedule.Clear()
    End Sub

    Private Sub txtSchedule_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSchedule.Leave
        txtSchedule.Text = "0.00"
    End Sub
    Public Sub SearchList(ByVal Search As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Member_SoaRegister_Search",
                                      New SqlParameter("@Search", Search))
        With dgvSoa
            .DataSource = ds.Tables(0)
            .Columns(0).Visible = False
            .Columns(1).Visible = False

            .Columns(2).HeaderText = "Soa Number"
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(2).Width = 100
            .Columns(2).ReadOnly = True
            .Columns(3).HeaderText = "ID"
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(3).Width = 100
            .Columns(3).ReadOnly = True
            .Columns(4).HeaderText = "Name"
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(4).ReadOnly = True
            .Columns(4).Width = 200
            .Columns(5).HeaderText = "Account Title"
            .Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(5).ReadOnly = True
            .Columns(5).Width = 200
            .Columns(6).HeaderText = "Schedule"
            .Columns(6).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(6).ReadOnly = True
            .Columns(6).Width = 80
            .Columns(7).HeaderText = "Closed"
            .Columns(7).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(7).Width = 80
        End With
    End Sub

    Private Sub txtsearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsearch.TextChanged
        SearchList(txtsearch.Text)
    End Sub

End Class