﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmPayee
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim coid As String

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        SelectedColumnVAR()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub MemberSearchByNum()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "Member_Search",
                                    New SqlParameter("@EmpNo", txtSearch.Text))
        'New SqlParameter("@fULLNAME", txtSearch.Text))
        With grdCoAList
            .DataSource = ds.Tables(0)
            .Columns("pk_Employee").Visible = False
            .Columns("fcEmployeeNo").HeaderText = "ID Number"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcEmployeeNo").Width = 165
            .Columns("Fullname").HeaderText = "Name"
            .Columns("Fullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Fullname").Width = 400
        End With
    End Sub

    Private Sub frmPayee_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call ListDocNum()
        txtSearch.Text = ""
        Me.ActiveControl = txtSearch
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Then
            ListDocNum()
        Else
            MemberSearchByNum()
        End If
    End Sub

    Private Sub ListDocNum()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "Member_List")

        grdCoAList.DataSource = ds.Tables(0)

        With grdCoAList
            .Columns("pk_Employee").Visible = False
            .Columns("fcEmployeeNo").HeaderText = "ID Number"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcEmployeeNo").Width = 165
            .Columns("Fullname").HeaderText = "Name"
            .Columns("Fullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Fullname").Width = 400
        End With
    End Sub

    Private Sub SelectedColumnVAR()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub grdCoAList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdCoAList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub
End Class