﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmLoanFilter
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim coid As String

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmLoanFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.ActiveControl = txtSearch
        txtSearch.Text = ""
        Call SearchApprovedLoan()
    End Sub
    Private Sub SearchApprovedLoan()
        'If frmGeneralJournalEntries.cboDoctype.Text = "OFFICIAL RECEIPT" Then
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet
            Dim co_id As New Guid(gCompanyID)
            'frmGeneralJournalEntries.grdListofEntries.Rows.Clear()
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "spu_CIMS_Loans_ApprovedLoans_List", _
                                          New SqlParameter("@filter", txtSearch.Text),
                                          New SqlParameter("@coid", gCompanyID))
            grdLoanList.DataSource = ds.Tables(0)
            With grdLoanList
                .Columns(0).Width = 80
                .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(0).ReadOnly = True
                .Columns(0).HeaderText = "ID No."
                .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(1).ReadOnly = True
                .Columns(1).HeaderText = "Name"
                .Columns(2).Width = 100
                .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(2).ReadOnly = True
                .Columns(2).HeaderText = "Loan Type"
                .Columns(3).Width = 80
                .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(3).ReadOnly = True
                .Columns(3).HeaderText = "Loan Ref."
                .Columns(4).Width = 80
                .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(4).ReadOnly = True
                .Columns(4).HeaderText = "Amount"
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(4).DefaultCellStyle.Format = "###,##0.00"
                .Columns(5).Visible = False
                .Columns(6).Visible = False
                .Columns(7).Visible = False
                .Columns(8).Visible = False
                .Columns(9).Visible = False
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'End If

    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call SearchApprovedLoan()
    End Sub

    Private Sub AddItem(ByVal IDMember As String,
                              ByVal NameMember As String,
                              ByVal LoanRef As String,
                              ByVal KeyAcnt As String,
                              ByVal CodeAcnt As String,
                              ByVal TitleAcnt As String,
                              ByVal dDebit As Decimal,
                              ByVal dCredit As Decimal,
                              ByVal KeyMember As String)

        Dim newItemDetailID As String = System.Guid.NewGuid.ToString()

        Dim row As String() =
         {IDMember, NameMember, LoanRef, "", KeyAcnt, CodeAcnt, TitleAcnt, dDebit, dCredit, KeyMember}

        Dim nRowIndex As Integer
        With frmGeneralJournalEntries.grdGenJournalDetails
                .Rows.Add(row)
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
                .Columns("Debit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns("Credit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Debit").DefaultCellStyle.Format = "###,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "###,##0.00"
                frmGeneralJournalEntries.grdGenJournalDetails.Item("Credit", nRowIndex).Value = Format(CDec(frmGeneralJournalEntries.grdGenJournalDetails.Item("Credit", nRowIndex).Value.ToString()), "##,##0.00")
            frmGeneralJournalEntries.grdGenJournalDetails.Item("Debit", nRowIndex).Value = Format(CDec(frmGeneralJournalEntries.grdGenJournalDetails.Item("Debit", nRowIndex).Value.ToString()), "##,##0.00")

        End With
    End Sub

    Private Sub AddItemDataset(ByVal IDMember As String,
                              ByVal NameMember As String,
                              ByVal LoanRef As String,
                              ByVal KeyAcnt As String,
                              ByVal CodeAcnt As String,
                              ByVal TitleAcnt As String,
                              ByVal dDebit As Decimal,
                              ByVal dCredit As Decimal,
                              ByVal KeyMember As String)

        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey1",
                                                                        New SqlParameter("@JVKeyID", frmGeneralJournalEntries.keyID))

        With ds
            For i As Integer = 0 To .Tables(0).Rows.Count - 1
                frmGeneralJournalEntries.grdGenJournalDetails.Rows.Add(IDMember, NameMember, LoanRef, "", KeyAcnt, CodeAcnt, TitleAcnt, dDebit, dCredit, KeyMember, "", "", "", "", "", "", "")
            Next
        End With
    End Sub

    Private Sub grdLoanList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdLoanList.DoubleClick
        Me.DialogResult = DialogResult.OK
    End Sub

    'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Call SearchApprovedLoan()
    'End Sub

    Private Sub grdLoanList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdLoanList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub
End Class