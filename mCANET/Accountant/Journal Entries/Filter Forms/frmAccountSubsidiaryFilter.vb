﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports System.Threading, System.IO
Imports System.Data.OleDb

Public Class frmAccountSubsidiaryFilter
    Private gCon As New Clsappconfiguration()
    Dim dv As New DataView
    Dim ds As New DataSet

    Private Sub frmAccountSubsidiaryFilter_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        loadAccountSubsidiaryListUnpaid()
        LoadGridDetails()
    End Sub

    Private Sub loadAccountSubsidiaryListUnpaid()
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountSubsidiary_ListUnpaid")
        dv.Table = ds.Tables(0)
    End Sub

    Private Sub LoadGridDetails()
        dv = New DataView(ds.Tables(0))
        dv.RowFilter = "Fullname LIKE '%" & txtSearch.Text & "%' OR fcEmployeeNo LIKE '%" & txtSearch.Text & "%' OR FcDocNumber LIKE '%" & txtSearch.Text & "%'"
        grdList.DataSource = dv

        With grdList
            .Columns("PkSubsidiaryHeaderID").Visible = False
            .Columns("FkAmortization").Visible = False
            .Columns("FkMember").Visible = False
            .Columns("fcEmployeeNo").Width = 100
            .Columns("fcEmployeeNo").ReadOnly = True
            .Columns("fcEmployeeNo").HeaderText = "ID"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Fullname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("Fullname").ReadOnly = True
            .Columns("Fullname").HeaderText = "Name"
            .Columns("Fullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FcDocNumber").HeaderText = "Ref. Number"
            .Columns("FcDocNumber").Width = 100
            .Columns("FcDocNumber").ReadOnly = True
            .Columns("FcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").Width = 70
            .Columns("acnt_code").ReadOnly = True
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").Width = 200
            .Columns("acnt_name").ReadOnly = True
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FnAmount").HeaderText = "Amount"
            .Columns("FnAmount").Width = 120
            .Columns("FnAmount").ReadOnly = True
            .Columns("FnAmount").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FnAmount").DefaultCellStyle.Format = "##,##0.00"
        End With
        dv.Sort = "Fullname, acnt_name"
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        LoadGridDetails()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class