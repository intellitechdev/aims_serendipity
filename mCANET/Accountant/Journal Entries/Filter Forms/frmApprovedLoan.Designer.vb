﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprovedLoan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnselect = New System.Windows.Forms.Button()
        Me.btncancel = New System.Windows.Forms.Button()
        Me.dgvApprovedLoan = New System.Windows.Forms.DataGridView()
        Me.txtname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        CType(Me.dgvApprovedLoan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnselect
        '
        Me.btnselect.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnselect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnselect.Location = New System.Drawing.Point(463, 229)
        Me.btnselect.Name = "btnselect"
        Me.btnselect.Size = New System.Drawing.Size(87, 29)
        Me.btnselect.TabIndex = 0
        Me.btnselect.Text = "Select"
        Me.btnselect.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnselect.UseVisualStyleBackColor = True
        '
        'btncancel
        '
        Me.btncancel.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.btncancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btncancel.Location = New System.Drawing.Point(556, 229)
        Me.btncancel.Name = "btncancel"
        Me.btncancel.Size = New System.Drawing.Size(87, 29)
        Me.btncancel.TabIndex = 1
        Me.btncancel.Text = "Cancel"
        Me.btncancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btncancel.UseVisualStyleBackColor = True
        '
        'dgvApprovedLoan
        '
        Me.dgvApprovedLoan.AllowUserToAddRows = False
        Me.dgvApprovedLoan.AllowUserToDeleteRows = False
        Me.dgvApprovedLoan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvApprovedLoan.BackgroundColor = System.Drawing.Color.White
        Me.dgvApprovedLoan.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvApprovedLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvApprovedLoan.Location = New System.Drawing.Point(12, 12)
        Me.dgvApprovedLoan.Name = "dgvApprovedLoan"
        Me.dgvApprovedLoan.ReadOnly = True
        Me.dgvApprovedLoan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvApprovedLoan.Size = New System.Drawing.Size(635, 211)
        Me.dgvApprovedLoan.TabIndex = 2
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(56, 236)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(401, 20)
        Me.txtname.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 239)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Search"
        '
        'btnSearch
        '
        Me.btnSearch.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(12, 232)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(87, 30)
        Me.btnSearch.TabIndex = 21
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        Me.btnSearch.Visible = False
        '
        'frmApprovedLoan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(659, 267)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtname)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvApprovedLoan)
        Me.Controls.Add(Me.btncancel)
        Me.Controls.Add(Me.btnselect)
        Me.Controls.Add(Me.btnSearch)
        Me.Name = "frmApprovedLoan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loans for Release"
        CType(Me.dgvApprovedLoan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnselect As System.Windows.Forms.Button
    Friend WithEvents btncancel As System.Windows.Forms.Button
    Friend WithEvents dgvApprovedLoan As System.Windows.Forms.DataGridView
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
End Class
