﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmApprovedPayroll
    Private gCon As New Clsappconfiguration()
    Public fxKeyAccount As String
    Dim Loanref As String
    Public RecordedInAccounting As Boolean
    Private Sub frmApprovedPayroll_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SearchApprovedPayroll()
        dgvApprovedLoan.Cursor.ToString()
    End Sub
    Private Sub SearchApprovedPayroll()
        Try
            Dim mycon As New Clsappconfiguration
            Dim co_id As New Guid(gCompanyID)
            Dim ds As DataSet
            frmGeneralJournalEntries.grdListofEntries.Rows.Clear()
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "AprovedPayrollSearchList",
                                          New SqlParameter("@co_id", co_id), _
                                          New SqlParameter("@Name", txtname.Text))
            'New SqlParameter("@DocNo", frmGeneralJournalEntries.cboDoctype.Text), _
            dgvApprovedLoan.DataSource = ds.Tables(0)
            With dgvApprovedLoan
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Width = 80
                .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(2).ReadOnly = True
                .Columns(2).Visible = False
                .Columns(2).HeaderText = "Card No."
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(3).ReadOnly = True
                .Columns(3).Visible = False
                .Columns(3).HeaderText = "Card Name"
                .Columns(4).Width = 100
                .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(4).ReadOnly = True
                .Columns(4).Visible = False
                .Columns(4).HeaderText = "Account Ref."
                .Columns(5).Width = 110
                .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(5).ReadOnly = True
                .Columns(5).HeaderText = "SOA No."
                .Columns(6).Width = 110
                .Columns(6).ReadOnly = True
                .Columns(6).HeaderText = "Amount"
                .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(6).DefaultCellStyle.Format = "##,##0.00"
                .Columns(6).Visible = False
                .Columns(7).Width = 150
                .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(7).ReadOnly = True
                .Columns(7).HeaderText = "Cut-Off"
                

            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnselect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnselect.Click
        Me.DialogResult = DialogResult.OK
    End Sub
    Public Sub AvoidDuplicate_Soa()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        Dim co_id As New Guid(gCompanyID)
        Dim fcFilter As String = dgvApprovedLoan.SelectedRows(0).Cells(5).Value.ToString()
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "spu_t_tJVEntry_List_bySOA",
                                      New SqlParameter("@docType", frmGeneralJournalEntries.cboDoctype.Text),
                                      New SqlParameter("@filter", fcFilter),
                                      New SqlParameter("@coid", co_id))
        'MsgBox(frmGeneralJournalEntries.cboDoctype.Text)
        If ds.Tables(0).Rows.Count <> 0 Then
            RecordedInAccounting = True
        Else
            RecordedInAccounting = False
        End If

    End Sub
    Public Sub EntryDetails()
        Try
            fxKeyAccount = dgvApprovedLoan.SelectedRows(0).Cells(0).Value.ToString()
            frmGeneralJournalEntries.keyID = fxKeyAccount
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgvApprovedLoan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvApprovedLoan.Click
        Call EntryDetails()
    End Sub

    Private Sub txtname_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtname.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub txtname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtname.TextChanged
        SearchApprovedPayroll()
    End Sub
    Private Sub dgvApprovedLoan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvApprovedLoan.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Me.Close()
    End Sub
End Class