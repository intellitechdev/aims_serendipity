﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmSoaFilter
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String
    Private Sub CancelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub frmSoaFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadSoaAccounts()
        txtsearch.Text = ""
        Me.ActiveControl = txtsearch
    End Sub
    Private Sub loadSoaAccounts()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "_Member_SoaRegister_List",
                                      New SqlParameter("@coid", gCompanyID))
        dgvsoafilter.DataSource = ds.Tables(0)

        With dgvsoafilter
            .Columns("fxKeySoaRegister").Visible = False
            .Columns("fcDocNumber").Width = 100
            .Columns("fcDocNumber").ReadOnly = True
            .Columns("fcDocNumber").HeaderText = "Soa Ref."
            .Columns("fcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcEmployeeNo").Width = 100
            .Columns("fcEmployeeNo").ReadOnly = True
            .Columns("fcEmployeeNo").HeaderText = "ID"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcFullname").Width = 255
            .Columns("fcFullname").ReadOnly = True
            .Columns("fcFullname").HeaderText = "Name"
            .Columns("fcFullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_name").Width = 290
            .Columns("acnt_name").ReadOnly = True
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fxKey_Account").Visible = False
            .Columns("pk_Employee").Visible = False
            .Columns("acnt_code").ReadOnly = True
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").Width = 70
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcAcct_No").Visible = False
        End With
    End Sub
    Private Sub SelectedColumnVAR()

        Select Case xModule
            Case "Entry"
                Try
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                    'txtSearch.Text = ""
                Catch ex As Exception

                End Try
            Case "Forward"
                Try
                    Dim row As String() = New String() {dgvsoafilter.SelectedRows(0).Cells(2).Value.ToString(), dgvsoafilter.SelectedRows(0).Cells(3).Value.ToString(), dgvsoafilter.SelectedRows(0).Cells(1).Value.ToString(), dgvsoafilter.SelectedRows(0).Cells(4).Value.ToString(), dgvsoafilter.SelectedRows(0).Cells(5).Value.ToString(), Format(CDec(0), "##,##0.00"), Format(CDec(0), "##,##0.00")}
                    frmForwardBalance.grdList.Rows.Add(row)
                    Me.Close()
                    'txtSearch.Text = ""
                Catch ex As Exception

                End Try
        End Select
    End Sub

    Private Sub OkToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OkToolStripMenuItem.Click
        SelectedColumnVAR()
    End Sub

    Private Sub dgvsoafilter_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvsoafilter.CellContentClick

    End Sub

    Private Sub dgvsoafilter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvsoafilter.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Call SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
                txtsearch.Text = ""
        End Select
    End Sub

    Private Sub txtsearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Call SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
                txtsearch.Text = ""
        End Select
    End Sub

    Private Sub txtsearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtsearch.TextChanged
        'Call loadSubsidiaryAccountsFilter()
        ''Call SelectedColumnVAR()
        'If txtsearch.Text = "" Then
        '    Call loadSubsidiaryAccounts()
        'End If
    End Sub
End Class