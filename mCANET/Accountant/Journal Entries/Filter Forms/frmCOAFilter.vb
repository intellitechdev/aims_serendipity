﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmCOAFilter
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String
    Dim isdebit As Boolean

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub DebitCredit_Focus()
        isdebit = grdCoAList.SelectedRows(0).Cells(5).Value.ToString()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Call SelectedColumnVAR()
    End Sub

    Private Sub frmCOAFilter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case xModule
            Case "Deductions"
                Call FilterDeductions()
            Case Else
                Call filterAccounts()
        End Select
        txtSearch.Text = ""
        Me.ActiveControl = txtSearch
    End Sub

    'Private Sub loadAccounts()
    '    Dim mycon As New Clsappconfiguration
    '    Dim co_id As New Guid(gCompanyID)
    '    Dim ds As DataSet

    '    ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_mAccountsDS_load",
    '                                   New SqlParameter("@coid", co_id))
    '    grdCoAList.DataSource = ds.Tables(0)

    '    With grdCoAList
    '        .Columns("fcDescription").Visible = False
    '        .Columns("acnt_code").Width = 80
    '        .Columns("acnt_code").HeaderText = "Code"
    '        .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
    '        .Columns("acnt_id").Visible = False
    '        .Columns("acnt_name").Width = 420
    '        .Columns("acnt_name").HeaderText = "Account Title"
    '        .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
    '        .Columns("code_description").Visible = False
    '        .Columns("isdebit").Visible = False
    '        .ClearSelection()
    '    End With
    'End Sub

    Private Sub SelectedColumnVAR()
        Select Case xModule
            Case "Entry"
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Case "GL Report"
                frmGeneralLedgerDetailedV2.txtAccountTitle.Text = grdCoAList.SelectedRows(0).Cells(1).Value.ToString() + " | " + grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                frmGeneralLedgerDetailedV2.acnt_id = grdCoAList.SelectedRows(0).Cells(3).Value.ToString
                frmGeneralLedgerDetailedV2.acnt_code = grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                frmGeneralLedgerDetailedV2.acnt_name = grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                Me.Close()
            Case "Account Analysis"
                frmAccountAnalysis.txtAccountTitle.Text = grdCoAList.SelectedRows(0).Cells(1).Value.ToString() + " | " + grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                frmAccountAnalysis.acnt_id = grdCoAList.SelectedRows(0).Cells(3).Value.ToString
                frmAccountAnalysis.acnt_code = grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                frmAccountAnalysis.acnt_name = grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                Me.Close()
            Case "Cash Balance"
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Case "Recurring Entry"
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Case "Deductions"
                Me.DialogResult = Windows.Forms.DialogResult.OK
        End Select
    End Sub

    Private Sub grdCoAList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdCoAList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
        End Select
        'If e.KeyCode = Keys.Enter Then
        '    SelectedColumnVAR()
        'End If

        'If e.KeyCode = Keys.Escape Then
        '    Me.Close()
        'End If
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Call SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
        End Select

        'If e.KeyCode = Keys.Enter Then
        '    Call SelectedColumnVAR()
        'End If

        'If e.KeyCode = Keys.Escape Then
        '    Me.Close()
        'End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Select Case xModule
            Case "Deductions"
                Call FilterDeductions()
            Case Else
                Call filterAccounts()
        End Select
    End Sub

    Private Sub filterAccounts()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_mAccountsDS_filter",
                                       New SqlParameter("@coid", gCompanyID),
                                       New SqlParameter("@acnt_name", txtSearch.Text))
        grdCoAList.DataSource = ds.Tables(0)

        With grdCoAList
            .Columns("fcDescription").Visible = False
            .Columns("acnt_code").Width = 80
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_id").Visible = False
            .Columns("acnt_name").Width = 420
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("code_description").Visible = False
            .Columns("isdebit").Visible = False
        End With
    End Sub

    Private Sub FilterDeductions()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_AccountAmortization_Filter",
                                       New SqlParameter("@coid", gCompanyID),
                                       New SqlParameter("@filter", txtSearch.Text))
        grdCoAList.DataSource = ds.Tables(0)

        With grdCoAList
            .Columns("PkAccount").Visible = False
            .Columns("FkAccount").Visible = False
            .Columns("acnt_code").Visible = False
            .Columns("acnt_name").Visible = False
            .Columns("FkAccount").Visible = False
            .Columns("acntwithcode").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acntwithcode").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        End With
    End Sub

    Private Sub filterDeductionAccounts()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_AccountAmortization_View",
                                       New SqlParameter("@coid", gCompanyID),
                                       New SqlParameter("@acnt_name", txtSearch.Text))
        grdCoAList.DataSource = ds.Tables(0)

        With grdCoAList
            .Columns("fcDescription").Visible = False
            .Columns("acnt_code").Width = 80
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_id").Visible = False
            .Columns("acnt_name").Width = 420
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("code_description").Visible = False
            .Columns("isdebit").Visible = False
        End With
    End Sub

    'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    If txtSearch.Text = "" Then
    '        Call loadAccounts()
    '    Else
    '        Call filterAccounts()
    '    End If
    'End Sub

    Private Sub grdCoAList_CellMouseEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCoAList.CellMouseEnter
        'Dim dgvCell As DataGridViewCell
        'Dim dgvCell2 As DataGridViewCell
        'Dim guid As Guid

        'For Each drow As DataGridViewRow In grdCoAList.Rows
        '    dgvCell = drow.Cells("acnt_name")
        '    dgvCell2 = drow.Cells("acnt_id")

        '    If dgvCell2.Value <> Nothing Or dgvCell2.Value.ToString() <> "" Then
        '        guid = New Guid(dgvCell2.Value.ToString())

        '        Dim rdH As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "[spu_SelectCashBalance]",
        '                                                                            New SqlParameter("@acnt_code", guid))
        '        Try
        '            While rdH.Read
        '                dgvCell.ToolTipText = FormatNumber(rdH.Item("total").ToString, 2)

        '                'MessageBox.Show(FormatNumber(rdH.Item("total").ToString, 2).ToString())
        '            End While
        '            rdH.Close()
        '        Catch
        '            'MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '        End Try
        '    End If
        'Next
    End Sub
End Class