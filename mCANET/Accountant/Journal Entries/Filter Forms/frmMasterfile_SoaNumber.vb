﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_SoaNumber
    Public ID As Integer = 0
    Public Property GetID() As Integer
        Get
            Return ID
        End Get
        Set(ByVal value As Integer)
            ID = value
        End Set
    End Property
    Private Sub frmMasterfile_SoaNumber_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case frmGeneralJournalEntries.cboDoctype.Text
            Case "RECEIPTS"
                Load_AccountNumber()
            Case Else
                Load_AccountNumber2()
        End Select

    End Sub
    Private Sub Load_AccountNumber()
        Dim mycon As New Clsappconfiguration
        Dim coid As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "_Masterfile_SoaNumberList1",
                                                     New SqlParameter("@coid", coid))
        Try
            With GrdAccounts
                .DataSource = Nothing
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False

                .Columns(4).Visible = False
                .Columns(5).Visible = False
                .Columns(6).Visible = False
                .Columns(7).Visible = False
                .Columns(8).Visible = False
                .Columns(9).Visible = False
                .Columns(10).Visible = False
                .Columns(11).Visible = False
                .Columns(12).Visible = False
                .Columns(13).Visible = False

            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Load_AccountNumber2()
        Dim mycon As New Clsappconfiguration
        Dim coid As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "_Masterfile_SoaNumberList2",
                                                     New SqlParameter("@coid", coid))
        Try
            With GrdAccounts
                .DataSource = Nothing
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtLastName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLastName.TextChanged
        Select Case frmGeneralJournalEntries.cboDoctype.Text
            Case "OFFICIAL RECEIPT"
                Load_SoabyID_Used(txtLastName.Text)
            Case Else
                Load_SoabyID(txtLastName.Text)
        End Select
    End Sub
    Private Sub Load_SoabyID(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim coid As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "_Masterfile_SoaNumberListByID",
                                                     New SqlParameter("@fcDocNumber", ID),
                                                     New SqlParameter("@coid", coid))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Load_SoabyID_Used(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim coid As New Guid(gCompanyID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "_Masterfile_SoaNumberListByID_Used",
                                                     New SqlParameter("@fcDocNumber", ID),
                                                     New SqlParameter("@coid", coid))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.DialogResult = DialogResult.OK
        Me.Hide()
    End Sub

    Private Sub GrdAccounts_DoubleClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdAccounts.DoubleClick
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub
End Class