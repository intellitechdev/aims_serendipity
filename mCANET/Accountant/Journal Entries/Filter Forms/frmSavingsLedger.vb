﻿Imports System
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmSavingsLedger
    Private mycon As New Clsappconfiguration
    Private Sub frmSavingsLedger_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadCreditAccounts()
        AccountsDetail(txtIdno.Text, "Credit")
        txtIdno.Text = frmGeneralJournalEntries.grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()
        txtname.Text = frmGeneralJournalEntries.grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()
        txttemporary.Text = frmGeneralJournalEntries.grdGenJournalDetails.Rows(0).Cells(3).Value.ToString()
    End Sub
    Private Sub AccountsDetail(ByVal refno As String, ByVal mode As String)
        Try
            Dim ds As DataSet
            'ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_AccountVerification_Select_DebitCredit",
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CreditDetails",
                                           New SqlParameter("@EmployeeNo", txtIdno.Text),
                                           New SqlParameter("@RefNo", refno))
            Select Case mode
                Case "Credit"
                    dgvsavingsledger.DataSource = ds.Tables(0)
                    Format(CDec(Val(frmGeneralJournalEntries.txtCurrent.Text)), "##,##0.00")
                    Format(CDec(Val(frmGeneralJournalEntries.txtAvailable.Text)), "##,##0.00")
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub LoadCreditAccounts()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_AccountVerification_Select_Debit_Credit",
                                          New SqlParameter("@employeeno", frmGeneralJournalEntries.grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()),
                                          New SqlParameter("@Mode", "Credit"))

            dgvcredit.DataSource = ds.Tables(0)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub dgvcredit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvcredit.Click
        Try
            Dim refno As String
            refno = dgvcredit.SelectedRows(0).Cells(0).Value.ToString
            AccountsDetail(refno, "Credit")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class