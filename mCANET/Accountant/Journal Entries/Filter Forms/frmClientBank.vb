﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports WPM_EDCRYPT
Imports System.Text.RegularExpressions

Public Class frmClientBank

    Private con As New Clsappconfiguration
    Dim BankID As String
    Dim bankName As String

    Public Property GetBankID() As Integer
        Get
            Return BankID
        End Get
        Set(ByVal value As Integer)
            BankID = value
        End Set
    End Property

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'btnSave.Visible = True
        btnSave.Text = "&Save"
        btnSelect.Enabled = False
        btnClose.Text = "&Cancel"
        btnAdd.Enabled = False
        dgvClientBank.Columns("fcBankName").ReadOnly = False
        dgvClientBank.AllowUserToAddRows = True
        Call Bank_CellFocus()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim y As New DialogResult
        Dim BankID As String

        If GetBankID <> 0 Then
            BankID = GetBankID
        Else
            BankID = Guid.NewGuid.ToString
        End If
        If btnSave.Text = "&Save" Then
            Call AddClientBank(BankID, gCompanyID, bankName)
            MessageBox.Show("Successfully Saved", "Client Bank", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ElseIf btnSave.Text = "&Delete" Then
            y = MessageBox.Show("Are you sure you want to delete Bank?", "Delete Bank", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If y = Windows.Forms.DialogResult.Yes Then
                ClientBankList_DeletePerRow()
                dgvClientBank.Rows.Remove(dgvClientBank.CurrentRow)
                MessageBox.Show("Successfully Deleted", "Client Bank", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                btnSelect.Enabled = True
                'btnSave.Visible = False
                btnSave.Text = "&Delete"
                btnClose.Text = "&Close"
                btnAdd.Enabled = True
                Exit Sub
            End If
        End If
        btnSelect.Enabled = True
        'btnSave.Visible = False
        btnSave.Text = "&Delete"
        btnClose.Text = "&Close"
        btnAdd.Enabled = True
        dgvClientBank.Columns("fcBankName").ReadOnly = True
        dgvClientBank.AllowUserToAddRows = False
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If btnClose.Text = "&Cancel" Then
            'btnSave.Visible = False
            btnSave.Text = "&Delete"
            btnSelect.Enabled = True
            btnClose.Text = "&Close"
            btnAdd.Enabled = True
            dgvClientBank.Columns("fcBankName").ReadOnly = True
            dgvClientBank.AllowUserToAddRows = False
        ElseIf btnClose.Text = "&Close" Then
            Me.Close()
        End If
    End Sub

    Public Function NormalizeValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        Try
            If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
                If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                    fieldName = grid.Item(columnName, rowIndex).Value.ToString()

                End If
            End If
        Catch ex As Exception

        End Try
        Return fieldName
    End Function

    Private Sub AddClientBank(ByVal BankId As String, ByVal Company As String, ByVal BankName As String)
        Try
            For i As Integer = 0 To dgvClientBank.Rows.Count - 2
                BankName = NormalizeValuesInDataGridView(dgvClientBank, "fcBankName", i)
            Next
            SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "ClientBankList_Insert",
                                      New SqlParameter("@fk_BankId", BankId),
                                      New SqlParameter("@fk_coId", Company),
                                      New SqlParameter("@fcBankName", BankName))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ClientBankList_List()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, CommandType.StoredProcedure, "ClientBankList_Search",
                                      New SqlParameter("@fk_coId", gCompanyID))
        dgvClientBank.DataSource = ds.Tables(0)
        With dgvClientBank
            .Columns("fk_BankId").Visible = False
            .Columns("fcBankName").HeaderText = "Description"
            .Columns("fcBankName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("fcBankName").ReadOnly = True
        End With
    End Sub

    Private Sub ClientBankList_Filter()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, CommandType.StoredProcedure, "ClientBankList_Filter",
                                      New SqlParameter("@fk_coId", gCompanyID),
                                      New SqlParameter("@fcBankName", txtSearch.Text))
        dgvClientBank.DataSource = ds.Tables(0)
        With dgvClientBank
            .Columns("fk_BankId").Visible = False
            .Columns("fcBankName").HeaderText = "Description"
            .Columns("fcBankName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("fcBankName").ReadOnly = True
        End With
    End Sub

    Private Sub Bank_CellFocus()
        Dim strRowNumber As String

        For Each row As DataGridViewRow In dgvClientBank.Rows
            strRowNumber = dgvClientBank.Rows.Count - 1
            dgvClientBank.Rows(strRowNumber).Selected = True
            dgvClientBank.CurrentCell = dgvClientBank.Rows(strRowNumber).Cells(1)
            dgvClientBank.BeginEdit(True)
        Next
    End Sub

    Private Sub frmClientBank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ClientBankList_List()
    End Sub

    Private Sub ClientBankList_DeletePerRow()
        'Try
        SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "ClientBankList_Delete",
                                  New SqlParameter("@fk_bankId", TextBox1.Text),
                                  New SqlParameter("@fk_coId", gCompanyID))
        'Catch ex As Exception

        'End Try
    End Sub

    'Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
    '    Dim i As Integer = dgvClientBank.CurrentRow.Index
    '    frmGeneralJournalEntries.grdListofEntries.Item("Bank", i).Value = dgvClientBank.SelectedRows(0).Cells("fcBankName").Value.ToString()
    '    Me.Close()
    'End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        ClientBankList_Filter()
    End Sub

    Private Sub dgvClientBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClientBank.Click
        Dim i As Integer = dgvClientBank.CurrentRow.Index
        TextBox1.Text = dgvClientBank.Item("fk_bankId", i).Value.ToString
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub dgvClientBank_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvClientBank.KeyDown
        'If e.KeyCode = Keys.Enter Then
        '    Dim i As Integer = dgvClientBank.CurrentRow.Index
        '    frmGeneralJournalEntries.grdListofEntries.Item("Bank", i).Value = dgvClientBank.SelectedRows(0).Cells(1).Value.ToString()
        '    Me.Close()
        'End If
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub
End Class