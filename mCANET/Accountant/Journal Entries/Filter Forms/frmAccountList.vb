﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmAccountList
    Dim gcon As New Clsappconfiguration
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim acntID As String

    Public Sub AccountList()
        Try
            Dim coid As New Guid(gCompanyID())
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "Entries_DefaultAccountList",
                                                            New SqlParameter("@coid", coid))

            dgvAccountList.DataSource = ds.Tables(0)
            With dgvAccountList
                .Columns("acnt_id").Visible = False
                .Columns("acnt_code").Width = 75
                .Columns("acnt_code").HeaderText = "Code"
                .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("acnt_name").HeaderText = "Account Title"
                .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("acnt_name").Width = 273
            End With


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub frmAccountList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSearch.Focus()
        AccountList()
        Call DefaultAccount_Search()
    End Sub

    Private Sub SearchAccount(ByVal Account As String)
        Dim co_id As New Guid(gCompanyID())
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "Entries_DefaultFilter", _
                                       New SqlParameter("@acnt_name", Account),
                                       New SqlParameter("@coid", co_id))
            dgvAccountList.DataSource = ds.Tables(0)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DefaultAccount_Insert()
        Try
            SqlHelper.ExecuteNonQuery(gcon.sqlconn, CommandType.StoredProcedure, "DefaultAccount_Insert", _
                                      New SqlParameter("@acnt_id", acntID),
                                      New SqlParameter("@acnt_code", txtCode.Text),
                                      New SqlParameter("@acnt_name", txtAccount.Text),
                                      New SqlParameter("@co_id", gCompanyID))
            MsgBox("Successfully Set", vbInformation, "Default Account Set!")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Call SelectDefaultAccount()
    End Sub

    Private Sub dgvAccountList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAccountList.DoubleClick
        Call SelectDefaultAccount()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub DefaultAccount_Search()
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "DefaultAccount_Search",
                                      New SqlParameter("@co_id", gCompanyID))
            While rd.Read
                txtCode.Text = rd.Item("acnt_code")
                txtAccount.Text = rd.Item("acnt_name")
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SelectDefaultAccount()

        acntID = dgvAccountList.SelectedRows(0).Cells(0).Value.ToString()
        txtCode.Text = dgvAccountList.SelectedRows(0).Cells(0).Value.ToString()
        txtAccount.Text = dgvAccountList.SelectedRows(0).Cells(2).Value.ToString()
        frmGeneralJournalEntries.NoneToolStripMenuItem.Text = dgvAccountList.SelectedRows(0).Cells(2).Value.ToString()
        Call DefaultAccount_Insert()
        Call frmGeneralJournalEntries.DefaultAccount_Search()
    End Sub

    Private Sub dgvAccountList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvAccountList.KeyDown
        If e.KeyCode = Keys.Enter Then
            SelectDefaultAccount()
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            SelectDefaultAccount()
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call SearchAccount(txtSearch.Text)
    End Sub
End Class