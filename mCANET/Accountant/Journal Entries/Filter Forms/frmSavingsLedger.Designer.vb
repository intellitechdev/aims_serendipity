﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSavingsLedger
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtIdno = New System.Windows.Forms.TextBox()
        Me.dgvsavingsledger = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtname = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txttemporary = New System.Windows.Forms.TextBox()
        Me.dgvcredit = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.dgvsavingsledger, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvcredit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnclose
        '
        Me.btnclose.Location = New System.Drawing.Point(609, 425)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(75, 32)
        Me.btnclose.TabIndex = 0
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "ID No."
        '
        'txtIdno
        '
        Me.txtIdno.BackColor = System.Drawing.SystemColors.Control
        Me.txtIdno.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtIdno.Enabled = False
        Me.txtIdno.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdno.ForeColor = System.Drawing.Color.DodgerBlue
        Me.txtIdno.Location = New System.Drawing.Point(73, 15)
        Me.txtIdno.Multiline = True
        Me.txtIdno.Name = "txtIdno"
        Me.txtIdno.Size = New System.Drawing.Size(181, 23)
        Me.txtIdno.TabIndex = 5
        '
        'dgvsavingsledger
        '
        Me.dgvsavingsledger.AllowUserToAddRows = False
        Me.dgvsavingsledger.AllowUserToDeleteRows = False
        Me.dgvsavingsledger.AllowUserToResizeColumns = False
        Me.dgvsavingsledger.AllowUserToResizeRows = False
        Me.dgvsavingsledger.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvsavingsledger.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvsavingsledger.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvsavingsledger.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvsavingsledger.Location = New System.Drawing.Point(6, 241)
        Me.dgvsavingsledger.Name = "dgvsavingsledger"
        Me.dgvsavingsledger.ReadOnly = True
        Me.dgvsavingsledger.Size = New System.Drawing.Size(678, 178)
        Me.dgvsavingsledger.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(273, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Client Name"
        '
        'txtname
        '
        Me.txtname.BackColor = System.Drawing.SystemColors.Control
        Me.txtname.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtname.Enabled = False
        Me.txtname.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtname.ForeColor = System.Drawing.Color.DodgerBlue
        Me.txtname.Location = New System.Drawing.Point(352, 15)
        Me.txtname.Multiline = True
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(260, 23)
        Me.txtname.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtIdno)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtname)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(689, 47)
        Me.Panel1.TabIndex = 9
        '
        'txttemporary
        '
        Me.txttemporary.BackColor = System.Drawing.SystemColors.Control
        Me.txttemporary.Location = New System.Drawing.Point(174, 437)
        Me.txttemporary.Name = "txttemporary"
        Me.txttemporary.Size = New System.Drawing.Size(100, 20)
        Me.txttemporary.TabIndex = 10
        '
        'dgvcredit
        '
        Me.dgvcredit.AllowUserToAddRows = False
        Me.dgvcredit.AllowUserToDeleteRows = False
        Me.dgvcredit.AllowUserToResizeColumns = False
        Me.dgvcredit.AllowUserToResizeRows = False
        Me.dgvcredit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvcredit.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvcredit.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvcredit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcredit.Location = New System.Drawing.Point(6, 75)
        Me.dgvcredit.Name = "dgvcredit"
        Me.dgvcredit.ReadOnly = True
        Me.dgvcredit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvcredit.Size = New System.Drawing.Size(678, 132)
        Me.dgvcredit.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "List of Credit Accounts"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 225)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Details"
        '
        'frmSavingsLedger
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(689, 465)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvcredit)
        Me.Controls.Add(Me.txttemporary)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.dgvsavingsledger)
        Me.Controls.Add(Me.btnclose)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSavingsLedger"
        CType(Me.dgvsavingsledger, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvcredit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtIdno As System.Windows.Forms.TextBox
    Friend WithEvents dgvsavingsledger As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txttemporary As System.Windows.Forms.TextBox
    Friend WithEvents dgvcredit As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
