﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmApprovedLoan
    Private gCon As New Clsappconfiguration()
    Public fxKeyAccount As String
    Dim Loanref As String
    Public RecordedInAccounting As Boolean

    Private Sub frmApprovedLoan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SearchApprovedLoan()
        'EntryDetails()
        dgvApprovedLoan.Cursor.ToString()
    End Sub
    Private Sub SearchApprovedLoan()
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet
            frmGeneralJournalEntries.grdListofEntries.Rows.Clear()
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "AprovedLoanSearchList", _
                                          New SqlParameter("@Name", txtname.Text),
                                          New SqlParameter("@doctype", frmGeneralJournalEntries.cboDoctype.Text),
                                          New SqlParameter("@coid", gCompanyID))
            dgvApprovedLoan.DataSource = ds.Tables(0)
            With dgvApprovedLoan
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Width = 80
                .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(2).ReadOnly = True
                .Columns(2).HeaderText = "ID No."
                .Columns(4).Width = 100
                .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(4).ReadOnly = True
                .Columns(4).HeaderText = "Loan Type"
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(3).ReadOnly = True
                .Columns(3).HeaderText = "Name"
                .Columns(5).Width = 80
                .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(5).ReadOnly = True
                .Columns(5).HeaderText = "Loan Ref."
                .Columns(6).Width = 80
                .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(6).ReadOnly = True
                .Columns(6).HeaderText = "Amount"
                .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(6).DefaultCellStyle.Format = "##,##0.00"
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub SearchLoanRef()
        Try
            Dim mycon As New Clsappconfiguration
            Dim rd As SqlDataReader
            frmGeneralJournalEntries.grdListofEntries.Rows.Clear()
            rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "AprovedLoanSearchByLoanRef", _
                                          New SqlParameter("@fxKeyJVDetails", fxKeyAccount))
            While rd.Read
                Loanref = rd("fcLoanReference")
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Me.Close()
    End Sub

    Private Sub btnselect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnselect.Click

        Me.DialogResult = DialogResult.OK

    End Sub

    Public Sub AvoidDuplicate()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        Dim fcFilter As String = dgvApprovedLoan.SelectedRows(0).Cells(1).Value.ToString()
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "spu_t_tJVEntry_List",
                                      New SqlParameter("@docType", frmGeneralJournalEntries.cboDoctype.Text),
                                      New SqlParameter("@filter", fcFilter),
                                      New SqlParameter("@coid", gCompanyID()))
        'MsgBox(ds.Tables(0).Rows.Count)
        If ds.Tables(0).Rows.Count <> 0 Then
            RecordedInAccounting = True
        Else
            RecordedInAccounting = False
        End If

    End Sub

    Public Sub EntryDetails()
        Try
            fxKeyAccount = dgvApprovedLoan.SelectedRows(0).Cells(0).Value.ToString()
            frmGeneralJournalEntries.keyID = fxKeyAccount
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgvApprovedLoan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvApprovedLoan.Click
        Call EntryDetails()
    End Sub

    Private Sub txtname_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtname.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub txtname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtname.TextChanged
        Call SearchApprovedLoan()
    End Sub

    'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Call SearchApprovedLoan()
    'End Sub

    Private Sub dgvApprovedLoan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvApprovedLoan.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Me.DialogResult = DialogResult.OK
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub
End Class