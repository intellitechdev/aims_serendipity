﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSoaFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OkToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtsearch = New System.Windows.Forms.TextBox()
        Me.dgvsoafilter = New System.Windows.Forms.DataGridView()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgvsoafilter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SearchToolStripMenuItem, Me.CancelToolStripMenuItem, Me.OkToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 184)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(843, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Search
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.SearchToolStripMenuItem.Text = "Search"
        '
        'CancelToolStripMenuItem
        '
        Me.CancelToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CancelToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.button_cancel2
        Me.CancelToolStripMenuItem.Name = "CancelToolStripMenuItem"
        Me.CancelToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.CancelToolStripMenuItem.Text = "Close"
        '
        'OkToolStripMenuItem
        '
        Me.OkToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.OkToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.OkToolStripMenuItem.Name = "OkToolStripMenuItem"
        Me.OkToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.OkToolStripMenuItem.Text = "Ok"
        '
        'txtsearch
        '
        Me.txtsearch.Location = New System.Drawing.Point(76, 186)
        Me.txtsearch.Name = "txtsearch"
        Me.txtsearch.Size = New System.Drawing.Size(405, 20)
        Me.txtsearch.TabIndex = 1
        '
        'dgvsoafilter
        '
        Me.dgvsoafilter.AllowUserToAddRows = False
        Me.dgvsoafilter.AllowUserToDeleteRows = False
        Me.dgvsoafilter.AllowUserToResizeColumns = False
        Me.dgvsoafilter.AllowUserToResizeRows = False
        Me.dgvsoafilter.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.dgvsoafilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvsoafilter.Location = New System.Drawing.Point(12, 12)
        Me.dgvsoafilter.Name = "dgvsoafilter"
        Me.dgvsoafilter.RowHeadersVisible = False
        Me.dgvsoafilter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvsoafilter.Size = New System.Drawing.Size(819, 155)
        Me.dgvsoafilter.TabIndex = 2
        '
        'frmSoaFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.CSAcctg.My.Resources.Resources.Project_Name_Panel
        Me.ClientSize = New System.Drawing.Size(843, 208)
        Me.Controls.Add(Me.dgvsoafilter)
        Me.Controls.Add(Me.txtsearch)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmSoaFilter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Soa Account Filter"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgvsoafilter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OkToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtsearch As System.Windows.Forms.TextBox
    Friend WithEvents dgvsoafilter As System.Windows.Forms.DataGridView
End Class
