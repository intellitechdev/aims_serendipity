<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_nametype
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpNameTypes = New System.Windows.Forms.GroupBox
        Me.rdOther = New System.Windows.Forms.RadioButton
        Me.rdCustomer = New System.Windows.Forms.RadioButton
        Me.rdSupplier = New System.Windows.Forms.RadioButton
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.grpNameTypes.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpNameTypes
        '
        Me.grpNameTypes.Controls.Add(Me.rdOther)
        Me.grpNameTypes.Controls.Add(Me.rdCustomer)
        Me.grpNameTypes.Controls.Add(Me.rdSupplier)
        Me.grpNameTypes.Location = New System.Drawing.Point(6, 11)
        Me.grpNameTypes.Name = "grpNameTypes"
        Me.grpNameTypes.Size = New System.Drawing.Size(286, 68)
        Me.grpNameTypes.TabIndex = 1
        Me.grpNameTypes.TabStop = False
        Me.grpNameTypes.Text = "Select a name type and click OK to continue"
        '
        'rdOther
        '
        Me.rdOther.AutoSize = True
        Me.rdOther.Location = New System.Drawing.Point(196, 31)
        Me.rdOther.Name = "rdOther"
        Me.rdOther.Size = New System.Drawing.Size(53, 17)
        Me.rdOther.TabIndex = 3
        Me.rdOther.TabStop = True
        Me.rdOther.Text = "Other"
        Me.rdOther.UseVisualStyleBackColor = True
        '
        'rdCustomer
        '
        Me.rdCustomer.AutoSize = True
        Me.rdCustomer.Location = New System.Drawing.Point(106, 31)
        Me.rdCustomer.Name = "rdCustomer"
        Me.rdCustomer.Size = New System.Drawing.Size(71, 17)
        Me.rdCustomer.TabIndex = 1
        Me.rdCustomer.TabStop = True
        Me.rdCustomer.Text = "Customer"
        Me.rdCustomer.UseVisualStyleBackColor = True
        '
        'rdSupplier
        '
        Me.rdSupplier.AutoSize = True
        Me.rdSupplier.Location = New System.Drawing.Point(24, 31)
        Me.rdSupplier.Name = "rdSupplier"
        Me.rdSupplier.Size = New System.Drawing.Size(63, 17)
        Me.rdSupplier.TabIndex = 0
        Me.rdSupplier.TabStop = True
        Me.rdSupplier.Text = "Supplier"
        Me.rdSupplier.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(199, 85)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(93, 23)
        Me.btnCancel.TabIndex = 28
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(103, 85)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(93, 23)
        Me.btnOK.TabIndex = 29
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'frm_acc_nametype
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(302, 116)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.grpNameTypes)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_acc_nametype"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Name Types"
        Me.grpNameTypes.ResumeLayout(False)
        Me.grpNameTypes.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpNameTypes As System.Windows.Forms.GroupBox
    Friend WithEvents rdCustomer As System.Windows.Forms.RadioButton
    Friend WithEvents rdSupplier As System.Windows.Forms.RadioButton
    Friend WithEvents rdOther As System.Windows.Forms.RadioButton
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
End Class
