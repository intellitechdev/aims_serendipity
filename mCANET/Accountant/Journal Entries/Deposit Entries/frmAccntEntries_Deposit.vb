Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmAccntEntries_Deposit
    Private gCon As New Clsappconfiguration()
    Private dsJournalDetails As New DataSet
    Private prevRowIndex As Integer = 0
    Private isFirstLoad As Boolean = True
    Private cb As ComboBox

    Private jvKeyID As String
    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

#Region "Events "
    Private Sub btnPreviewJV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreviewJV.Click
        PreviewJournalVoucher()
    End Sub

    Private Sub frmGeneralJournalEntries_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            dtFrom.Value = SetAsFirstDayOfTheMonth()
            grdListofEntries.DataSource = LoadGeneralJournalEntries(dtFrom.Value.Date, dtTo.Value.Date).Tables(0)
            Call FormatGeneralJournalEntriesGrid()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub grdListofEntries_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListofEntries.CellClick
        'Use for Control on Rows Added Event 
        isFirstLoad = True

        Call DisplayGeneralJournalEntries_Details()
    End Sub

    Private Sub btnDisplay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Try
            grdListofEntries.DataSource = LoadGeneralJournalEntries(dtFrom.Value.Date, dtTo.Value.Date).Tables(0)
            Call FormatGeneralJournalEntriesGrid()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bgwJournalEntries_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwJournalEntries.DoWork
        'Periodically checks if process has been cancelled. Exit if Cancelled
        If bgwJournalEntries.CancellationPending Then
            Thread.Sleep(1000)

            e.Cancel = True
            Exit Sub
        End If

        Try
            Dim jvKeyId As String = GetJVKeyID()
            dsJournalDetails = LoadGeneralJournalEntries_Details(jvKeyId)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bgwJournalEntries_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwJournalEntries.RunWorkerCompleted
        picLoading.Visible = False
        grdGenJournalDetails.DataSource() = dsJournalDetails.Tables(0)
        Call FormatGeneralJournalDetailsGrid()
        Call ComputeDebitAndCreditSumDetails()

        Call GenerateKeyIDForDetailEntry(grdGenJournalDetails.Rows.GetLastRow(DataGridViewElementStates.None))

        'Control for New rows added
        isFirstLoad = False
    End Sub

    Private Sub toolStripPostJournal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStripPostJournal.Click
        frmJVPosting.MdiParent = frmMain
        frmJVPosting.Show()
    End Sub

    Private Sub grdListofEntries_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdListofEntries.KeyDown
        Call DisplayGeneralJournalEntries_Details()
    End Sub

    Private Sub grdListofEntries_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdListofEntries.KeyUp
        Call DisplayGeneralJournalEntries_Details()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Call PrepareForANewGeneralJournal()
    End Sub

    Private Sub grdListofEntries_UserDeletingRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdListofEntries.UserDeletingRow
        Try
            Dim isPosted As Boolean = Boolean.Parse(grdListofEntries.Item("Posted", e.Row.Index).Value.ToString)
            If Not isPosted Then
                If MessageBox.Show("Are you sure you want to delete this entry?", "Delete Journal Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim jvEntryID As String = grdListofEntries.Item("fxKeyJVNo", e.Row.Index).Value.ToString()
                    Call DeleteJVEntry(jvEntryID)
                Else
                    e.Cancel = True
                End If
            Else
                MessageBox.Show("You cannot delete posted Entries.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Cancel = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub grdGenJournalDetails_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellLeave
        If Not cb Is Nothing Then
            cb.SelectedIndex = cb.FindStringExact(cb.Text)
        End If
    End Sub

    Private Sub grdGenJournalDetails_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles grdGenJournalDetails.CellValidating
        Dim cellValue As Decimal
        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cCredit" Then
            If Not Decimal.TryParse(e.FormattedValue.ToString(), cellValue) Then
                e.Cancel = True
                grdGenJournalDetails.Rows(e.RowIndex).ErrorText = "The value must be in numerical."
            Else
                grdGenJournalDetails.Rows(e.RowIndex).ErrorText = ""
            End If
        End If


        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cDebit" Then
            If Not Decimal.TryParse(e.FormattedValue.ToString(), cellValue) Then
                e.Cancel = True
                grdGenJournalDetails.Rows(e.RowIndex).ErrorText = "The value must be in numerical."
            Else
                grdGenJournalDetails.Rows(e.RowIndex).ErrorText = ""
            End If
        End If

    End Sub

    Private Sub grdGenJournalDetails_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellClick
        If e.ColumnIndex <> -1 Then
            If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cCredit" Then
                If NormalizeValuesInDataGridView(grdGenJournalDetails, "cCredit", e.RowIndex) = "" Then
                    grdGenJournalDetails.Item("cCredit", e.RowIndex).Value = 0
                End If
            End If

            If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cDebit" Then
                If NormalizeValuesInDataGridView(grdGenJournalDetails, "cDebit", e.RowIndex) = "" Then
                    grdGenJournalDetails.Item("cDebit", e.RowIndex).Value = 0
                End If
            End If
        End If
    End Sub

    Private Sub grdGenJournalDetails_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellValueChanged
        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cAccounts" Then
            Try
                Dim accountID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccounts", e.RowIndex)
                Call IsAccountValid(accountID)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                grdGenJournalDetails.Item("cAccounts", e.RowIndex).Value = System.DBNull.Value
            End Try
        End If

        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cCredit" Then
            Try
                Call ComputeDebitAndCreditSumDetails()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cDebit" Then
            Try
                Call ComputeDebitAndCreditSumDetails()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        If frmClosingEntries.IsPeriodClosed(dteGeneralJournal.Value.Date, btnSaveNew) = False Then
            Call RecordGeneralEntry()
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If frmClosingEntries.IsPeriodClosed(dteGeneralJournal.Value.Date, btnSaveClose) = False Then
            Call RecordGeneralEntry()
            Close()
        End If
    End Sub

    Private Sub grdGenJournalDetails_UserAddedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdGenJournalDetails.UserAddedRow
        Call GenerateKeyIDForDetailEntry(e.Row.Index)
    End Sub

    Private Sub grdGenJournalDetails_UserDeletedRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdGenJournalDetails.UserDeletedRow
        Try
            Call ComputeDebitAndCreditSumDetails()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

#End Region

#Region "Subroutines and other Functions "
    Private Sub RecordGeneralEntry()
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If

            Dim companyId As String = gCompanyID()
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim transDate As String = dteGeneralJournal.Value.Date
            Dim isAdjustment As Boolean = False
            Dim memo As String = grdGenJournalDetails.Item("cMemo", 0).Value
            Dim totalAmount As String = txtTotalCredit.Text
            Dim user As String = frmMain.currentUser.Text
            Dim subsidiaryID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cName", 0)

            If IsDetailedTableNotEmpty() Then
                If IsDebitAndCreditBalanced() Then

                    'SAVE ENTRY HEADER/DETAILS
                    Call SaveCurrentJournalEntry_Header(GetJVKeyID(), companyId, journalNo, transDate, isAdjustment, subsidiaryID, memo, totalAmount, user)
                    Call SaveCurrentJournalEntry_Details()

                    'RE-LOAD UPDATED LIST OF ENTRIES
                    grdListofEntries.DataSource = LoadGeneralJournalEntries(dtFrom.Value.Date, dtTo.Value.Date).Tables(0)
                    Call FormatGeneralJournalEntriesGrid()

                    'SUCCESSFUL ENTRY
                    MessageBox.Show("You have successfully entered an entry.", "General Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("Total Debit and Credit is not balance. You cannot proceed.", _
                         "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                End If
            Else
                MessageBox.Show("Entries are incomplete. Please enter a valid entry", "User Error", _
                        MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As ArgumentOutOfRangeException
            MessageBox.Show("User Error! There is nothing to save.", "General Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PrepareForANewGeneralJournal()
        Call ResetGridToNew()
        Call FormatGeneralJournalDetailsGrid()
        Call GenerateKeyIDForDetailEntry(0)
    End Sub

    Private Sub SaveCurrentJournalEntry_Header(ByVal jvKeyID As String, ByVal companyID As String, ByVal journalNo As String, ByVal transdate As Date, _
    ByVal isAdjust As String, ByVal subsidiaryID As String, ByVal memo As String, ByVal total As Decimal, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", jvKeyID), _
                    New SqlParameter("@companyID", companyID), _
                    New SqlParameter("@fiEntryNo", journalNo), _
                    New SqlParameter("@fdTransDate", transdate), _
                    New SqlParameter("@fbAdjust", isAdjust), _
                    New SqlParameter("@fxKeyNameID", subsidiaryID), _
                    New SqlParameter("@fcMemo", memo), _
                    New SqlParameter("@fdTotAmt", total), _
                    New SqlParameter("@user", user))

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Private Function NormalizeValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
            If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                fieldName = grid.Item(columnName, rowIndex).Value.ToString()
            End If
        End If

        Return fieldName
    End Function

    Private Sub SaveCurrentJournalEntry_Details()
        Try
            Call DeleteAllDetailedEntriesPerJV(GetJVKeyID())

            For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
                Dim journalNo As String = txtGeneralJournalNo.Text
                Dim sAccount As String = IIf(IsDBNull(grdGenJournalDetails.Item("cAccounts", xRow).Value), Nothing, grdGenJournalDetails.Item("cAccounts", xRow).Value.ToString())
                Dim jvDetailRecordID As String = grdGenJournalDetails.Item("fxKey", xRow).Value.ToString()
                Dim dDebit As Decimal = grdGenJournalDetails.Item("cDebit", xRow).Value
                Dim dCredit As Decimal = grdGenJournalDetails.Item("cCredit", xRow).Value
                Dim sMemo As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cMemo", xRow)
                Dim sNote As String = ""
                Dim sNameID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cName", xRow)
                Dim pkJournalID As String = GetJVKeyID()
                Dim dtTransact As Date = dteGeneralJournal.Value.Date

                Dim sSQLCmd As String = "usp_t_tJVEntry_details_save "
                sSQLCmd &= " @fiEntryNo='" & journalNo & "' "
                sSQLCmd &= ",@fxKeyAccount='" & sAccount & "' "
                sSQLCmd &= ",@fdDebit='" & dDebit & "' "
                sSQLCmd &= ",@fdCredit='" & dCredit & "' "
                sSQLCmd &= ",@fcMemo='" & sMemo & "' "
                sSQLCmd &= ",@fcNote='" & sNote & "' "
                sSQLCmd &= ",@fxKeyNameID=" & IIf(sNameID = "", "NULL", "'" & sNameID & "'") & " "
                sSQLCmd &= ",@fxKey=" & IIf(jvDetailRecordID = "", "NULL", "'" & jvDetailRecordID & "'") & " "
                sSQLCmd &= ",@fk_JVHeader='" & GetJVKeyID() & "'"
                sSQLCmd &= ",@transDate='" & dtTransact & "'"

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)
            Next

        Catch ex As Exception
            'Activate if needed
            'Rollback If error Occurs
            'Call DeleteAllDetailedEntriesPerJV(GetJVKeyID())
            Throw ex
        End Try
    End Sub

    Private Sub DeleteAllDetailedEntriesPerJV(ByVal jvKeyID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVENtry_detailedAllDelete_PerJV", _
                    New SqlParameter("@pk_JVEntryID", jvKeyID))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ResetGridToNew()
        'Set A new key for the New Entry
        GetJVKeyID() = System.Guid.NewGuid.ToString()

        txtGeneralJournalNo.Text = ""
        dteGeneralJournal.Value = Date.Now.Date
        txtTotalDebit.Text = "0.00"
        txtTotalCredit.Text = "0.00"

        'Clears Databounded Grid
        grdGenJournalDetails.DataSource = Nothing

        'Generate Random number
        Call GenerateJournalNo_RandomMethod()

        'or Generate Serial Number
    End Sub

    Private Function SetAsFirstDayOfTheMonth() As Date
        'Monthly
        Dim thisDate As Date

        Dim dateNow As Date = Date.Now.Date
        thisDate = dateNow.AddDays(-dateNow.Day + 1)

        Return thisDate
    End Function

    Private Sub DisplayNotification(ByVal notification As String, ByVal displayNotification As Boolean)
        If displayNotification Then
            lblPostingNotification.Visible = True
            lblPostingNotification.Text = notification
        Else
            lblPostingNotification.Visible = False
        End If

    End Sub

    Private Function LoadGeneralJournalEntries(ByVal dtFrom As Date, ByVal dtTo As Date) As DataSet
        Dim ds As New DataSet
        Dim companyID As String = gCompanyID()
        Dim username As String = frmMain.toolStripUsername.Text

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getperdate_perusername_deposit", _
                    New SqlParameter("@coid", companyID), _
                    New SqlParameter("@dtFrom", dtFrom), _
                    New SqlParameter("@dtTo", dtTo), _
                    New SqlParameter("@username", username))
        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Private Sub FormatGeneralJournalEntriesGrid()
        Dim jvKeyID As New DataGridViewTextBoxColumn
        Dim jvDate As New DataGridViewTextBoxColumn
        Dim jvEntryNo As New DataGridViewTextBoxColumn
        Dim subsidiary As New DataGridViewTextBoxColumn
        Dim particulars As New DataGridViewTextBoxColumn
        Dim total As New DataGridViewTextBoxColumn
        Dim posted As New DataGridViewTextBoxColumn
        Dim createdBy As New DataGridViewTextBoxColumn
        Dim updatedBy As New DataGridViewTextBoxColumn
        Dim isAdjustment As New DataGridViewTextBoxColumn


        With jvKeyID
            .HeaderText = "jvKeyID"
            .Name = "fxKeyJVNo"
            .DataPropertyName = "fxKeyJVNo"
        End With

        With jvDate
            .HeaderText = "Date"
            .Name = "Date"
            .DataPropertyName = "Date"
        End With

        With jvEntryNo
            .HeaderText = "JV No."
            .Name = "EntryNo"
            .DataPropertyName = "EntryNo"
        End With

        With subsidiary
            .HeaderText = "Subsidiary"
            .Name = "Name"
            .DataPropertyName = "Name"
        End With

        With particulars
            .HeaderText = "Particulars"
            .Name = "Memo"
            .DataPropertyName = "Memo"
        End With

        With total
            .HeaderText = "Amount"
            .Name = "Amount"
            .DataPropertyName = "Amount"
        End With

        With posted
            .HeaderText = "Posted"
            .Name = "Posted"
            .DataPropertyName = "Posted"
        End With

        With createdBy
            .HeaderText = "Created By"
            .Name = "fuCreatedBy"
            .DataPropertyName = "fuCreatedBy"
        End With

        With updatedBy
            .HeaderText = "Updated By"
            .Name = "fuUpdatedBy"
            .DataPropertyName = "fuUpdatedBy"
        End With

        With isAdjustment
            .HeaderText = "Adjustment"
            .Name = "Adjust"
            .DataPropertyName = "Adjust"
        End With


        With grdListofEntries
            .Columns.Clear()

            .Columns.Add(jvKeyID)
            .Columns.Add(jvDate)
            .Columns.Add(jvEntryNo)
            .Columns.Add(subsidiary)
            .Columns.Add(particulars)
            .Columns.Add(total)
            .Columns.Add(posted)
            .Columns.Add(createdBy)
            .Columns.Add(updatedBy)
            .Columns.Add(isAdjustment)

            .Columns("fxKeyJVNo").Visible = False

            .Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Amount").DefaultCellStyle.Format = "n2"
            .Columns("Posted").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("fuCreatedBy").Visible = False
            .Columns("fuUpdatedBy").Visible = False
            .Columns("Adjust").Visible = False

            .Columns("Date").Width = 40
            .Columns("EntryNo").Width = 80
            .Columns("Name").Width = 80
            .Columns("Memo").Width = 180
            .Columns("Amount").Width = 80
            .Columns("Posted").Width = 50
        End With

    End Sub

    Private Sub FormatGeneralJournalDetailsGrid()
        Dim fxKey As New DataGridViewTextBoxColumn
        Dim colAccounts As New DataGridViewComboBoxColumn
        Dim txtDebit As New DataGridViewTextBoxColumn
        Dim txtCredit As New DataGridViewTextBoxColumn
        Dim txtMemo As New DataGridViewTextBoxColumn
        Dim txtNote As New DataGridViewTextBoxColumn
        Dim chkBillable As New DataGridViewCheckBoxColumn
        Dim cboNames As New DataGridViewComboBoxColumn

        Dim dtAccounts As DataTable = m_displayAccountsDS().Tables(0)
        Dim dtNames As DataTable = m_DisplayNamesJV().Tables(0)

        With colAccounts
            .Items.Clear()
            .HeaderText = "Accounts"
            .DataPropertyName = "fxKeyAccount"
            .Name = "cAccounts"
            .DataSource = dtAccounts.DefaultView
            .DisplayMember = "code_description"
            .ValueMember = "acnt_id"
            .DropDownWidth = 500
            .AutoComplete = True
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            .MaxDropDownItems = 10
            .Resizable = DataGridViewTriState.True
        End With

        With cboNames
            .Items.Clear()
            .HeaderText = "Name"
            .DataPropertyName = "fxKeyNameID"
            .Name = "cName"
            .DataSource = dtNames.DefaultView
            .DisplayMember = "fcName"
            .ValueMember = "fxKey"
            .DropDownWidth = 200
            .AutoComplete = True
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            .MaxDropDownItems = 10
            .Resizable = DataGridViewTriState.True
        End With

        With fxKey
            .Name = "fxKey"
            .DataPropertyName = "fxKey"
        End With

        With txtDebit
            .HeaderText = "Debit"
            .Name = "cDebit"
            .DataPropertyName = "fdDebit"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtCredit
            .HeaderText = "Credit"
            .Name = "cCredit"
            .DataPropertyName = "fdCredit"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtMemo
            .HeaderText = "Memo"
            .Name = "cMemo"
            .DataPropertyName = "fcMemo"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtNote
            .HeaderText = "Note"
            .Name = "cNote"
            .DataPropertyName = "fcNote"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With chkBillable
            .FlatStyle = FlatStyle.Standard
            .HeaderText = "Billable"
            .Name = "cBillable"
            .DataPropertyName = "fbBillable"
            .Width = 50
            .ValueType() = GetType(Boolean)
        End With

        With Me.grdGenJournalDetails
            .Columns.Clear()
            .Columns.Add(colAccounts)
            .Columns.Add(txtMemo)
            .Columns.Add(txtDebit)
            .Columns.Add(txtCredit)

            .Columns.Add(txtNote)
            .Columns.Add(cboNames)
            .Columns.Add(chkBillable)
            .Columns.Add(fxKey)

            .Columns("cAccounts").Width = 220
            .Columns("cDebit").Width = 80
            .Columns("cCredit").Width = 80
            .Columns("cMemo").Width = 350
            .Columns("cNote").Width = 80
            .Columns("cName").Width = 200
            .Columns("cBillable").Width = 50
            .Columns("fxKey").Visible = False

            .Columns("cDebit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("cCredit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

            .Columns("cDebit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("cCredit").DefaultCellStyle.Format = "##,##0.00"

            .Columns("cNote").Visible = False
        End With

    End Sub

    Private Function LoadGeneralJournalEntries_Details(ByVal jvKeyID As String) As DataSet

        Dim ds As New DataSet

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey_Deposit", _
                New SqlParameter("@JVKeyID", jvKeyID))
        Catch ex As Exception
            Throw ex
        End Try

        Return ds

    End Function

    Private Sub DisplayGeneralJournalEntries_Details()
        'load selected detailed entries per JVKeyID in the background
        GetJVKeyID() = grdListofEntries.CurrentRow.Cells("fxKeyJVNo").Value.ToString()

        If GetJVKeyID() <> "" Then
            'If already posted the entries can no longer be edited
            If grdListofEntries.CurrentRow.Cells("Posted").Value.ToString = "True" Then
                grdGenJournalDetails.DataSource = Nothing
                grdGenJournalDetails.Enabled = False

                If bgwJournalEntries.IsBusy Then
                    MessageBox.Show("The data is still loading. Please wait for all the data to load first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    grdListofEntries.Item(grdListofEntries.CurrentCell.ColumnIndex, prevRowIndex).Selected = True
                Else
                    Call DisplayNotification("This entry has already been posted.", True)
                End If
            Else
                'Display Grid Details
                grdGenJournalDetails.Enabled = True

                'Clears the grid
                grdGenJournalDetails.DataSource = Nothing

                'Cancels if Busy
                If bgwJournalEntries.IsBusy Then
                    MessageBox.Show("The data is still loading. Please wait for all the data to load first.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    grdListofEntries.Item(grdListofEntries.CurrentCell.ColumnIndex, prevRowIndex).Selected = True
                Else
                    'Run Backgound loading of Journal Details
                    bgwJournalEntries.RunWorkerAsync()
                    picLoading.Visible = True
                End If

                'Do not Display Notification
                Call DisplayNotification("This entry has already been posted.", False)
            End If

            'Gets the previous row index
            prevRowIndex = grdListofEntries.CurrentRow.Index

            'Displays EntryNo.
            txtGeneralJournalNo.Text = grdListofEntries.CurrentRow.Cells("EntryNo").Value.ToString()

            'Displays Journal Date
            dteGeneralJournal.Value = grdListofEntries.CurrentRow.Cells("Date").Value
        End If
    End Sub

    Private Sub ComputeDebitAndCreditSumDetails()
        Dim totalCredit As Decimal
        Dim totaldebit As Decimal

        Try
            For Each xRow As DataGridViewRow In grdGenJournalDetails.Rows
                'Dim credit As Object = grdGenJournalDetails.Item("cCredit", xRow.Index).Value
                Dim credit As Object = NormalizeValuesInDataGridView(grdGenJournalDetails, "cCredit", xRow.Index)

                'Dim debit As Object = grdGenJournalDetails.Item("cDebit", xRow.Index).Value
                Dim debit As Object = NormalizeValuesInDataGridView(grdGenJournalDetails, "cDebit", xRow.Index)


                If credit IsNot Nothing Then
                    totalCredit += credit
                End If

                If debit IsNot Nothing Then
                    totaldebit += debit
                End If

            Next

            btnSaveClose.Enabled = True
            btnSaveNew.Enabled = True

        Catch ex As InvalidCastException
            btnSaveClose.Enabled = False
            btnSaveNew.Enabled = False

            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        txtTotalCredit.Text = Format(CDec(totalCredit), "##,##0.00")
        txtTotalDebit.Text = Format(CDec(totaldebit), "##,##0.00")
    End Sub

    Private Function IsDebitAndCreditBalanced() As Boolean
        Dim isValid As Boolean = False

        If txtTotalCredit.Text = txtTotalDebit.Text Then
            isValid = True
        Else
            isValid = False
        End If

        Return isValid
    End Function

    Private Function IsJournalNoValid(ByVal journalNo As String) As Boolean
        Dim result As String = "True"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_ChecksIfJournalNoExists", _
            New SqlParameter("@journalNo", journalNo))
            If rd.Read() Then
                result = rd.Item("result").ToString()
            End If
        End Using

        'If journal no. exists then no is not valid
        If result = "True" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function IsDetailedTableNotEmpty() As Boolean
        Dim isNotEmpty As Boolean

        If grdGenJournalDetails.RowCount > 2 Then
            isNotEmpty = True
        Else
            isNotEmpty = False
        End If

        Return isNotEmpty
    End Function

    Private Sub PreviewJournalVoucher()
        Try
            Dim pkJournalID As String = grdListofEntries.CurrentRow.Cells("fxKeyJVNo").Value.ToString()
            If pkJournalID <> "" Then
                With frmDeposit_Voucher
                    .GetJournalID() = pkJournalID
                    .MdiParent = frmMain
                    .Show()
                End With
            Else
                MsgBox("There is no selected entry to display the voucher. Please select first.", _
                            MsgBoxStyle.Information, Me.Text)
            End If
        Catch ex As NullReferenceException
            MessageBox.Show("There is no selected entry to display the voucher. Please select first.", _
                            "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GenerateJournalNo_RandomMethod()
        'Generate Random 
        Dim stemp As String = ""
        Dim xCnt As Integer = 0
        Dim sSQLCmd As String = "usp_xxxrandomid "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    stemp = rd.Item(0).ToString
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        txtGeneralJournalNo.Text = "JV" + CStr(Format(CDec(dteGeneralJournal.Value.Year), "0000")) + "-" + CStr(Format(CDec(dteGeneralJournal.Value.Month), "00")) + "-" + CStr(Format(CDec(dteGeneralJournal.Value.Day), "00")) + "-" + CStr(stemp)
    End Sub

    Private Sub DeleteJVEntry(ByVal JVEntryID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_Delete", _
                    New SqlParameter("@pkJVEntry", JVEntryID))
            MessageBox.Show("The JV Entry has been successfully deleted.", "General Journal Delete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GenerateKeyIDForDetailEntry(ByVal rowIndex As Integer)
        Dim newItemDetailID As String = System.Guid.NewGuid.ToString()
        grdGenJournalDetails.Item("fxKey", rowIndex).Value = newItemDetailID
    End Sub

    Private Sub IsAccountValid(ByVal accountID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_IsAccountActive", _
                New SqlParameter("@acnt_id", accountID))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub grdGenJournalDetails_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdGenJournalDetails.EditingControlShowing
        If TypeOf e.Control Is ComboBox Then
            cb = e.Control
            cb.DropDownStyle = ComboBoxStyle.DropDown
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.AutoCompleteSource = AutoCompleteSource.ListItems
        End If
    End Sub

End Class