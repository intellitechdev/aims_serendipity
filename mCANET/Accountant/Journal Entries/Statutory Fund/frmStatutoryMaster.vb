﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes

Public Class frmStatutoryMaster
    Private mycon As New Clsappconfiguration
    Private key As Long

    Private Sub btnclose_Click(sender As System.Object, e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Function StatutoryInsertUpdate() As Boolean
        Dim _return As Boolean
        Try
            Dim MyGuid As New Guid(modGlobalDeclarations.gCompanyID)
            Dim accoungGuid As New Guid(Me.txtAccount.Tag.ToString())
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "spu_Statutory_InsertUpdate",
                                      New SqlParameter("@fxKeyStatutoryID", key),
                                      New SqlParameter("@fxKeyCompany", MyGuid),
                                      New SqlParameter("@fxKeyAccountID", accoungGuid),
                                      New SqlParameter("@fnPercent", Me.txtPercent.Text),
                                      New SqlParameter("@isYear", Me.rbYear.Checked),
                                      New SqlParameter("@isMonth", Me.rbMonth.Checked))
            MessageBox.Show("Record Succesfully Updated!")
            _return = True
        Catch ex As Exception
            _return = False
            MessageBox.Show("Error! " + ex.ToString, "Update Statutory....", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Function
        End Try
        Return _return
    End Function

    Private Sub StatutoryDelete()
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "spu_Statutory_Delete",
                                      New SqlParameter("@fxKeyStatutoryID", key))
            MessageBox.Show("Record Succesfully Deleted!")
            StatutoryList()
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Delete Statutory....", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        Me.txtAccount.Enabled = True
        Me.txtPercent.Enabled = True
        Me.rbMonth.Enabled = True
        Me.rbYear.Enabled = True
        Me.txtAccount.Clear()
        Me.txtPercent.Text = "0.00"

        Me.btnSave.Visible = True
        Me.btnCancel.Visible = True

        Me.btnNew.Visible = False
        Me.btnEdit.Visible = False
        Me.btndelete.Visible = False

        Me.btnBrowseAccount.Enabled = True
        key = 0
    End Sub

    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click
        If Me.dgvListAccountRegister.SelectedRows.Count <> 0 Then
            Me.txtAccount.Enabled = True
            Me.txtPercent.Enabled = True
            Me.rbMonth.Enabled = True
            Me.rbYear.Enabled = True

            Me.btnSave.Visible = True
            Me.btnCancel.Visible = True

            Me.btnNew.Visible = False
            Me.btnEdit.Visible = False
            Me.btndelete.Visible = False

            Me.btnBrowseAccount.Enabled = False
            key = Me.dgvListAccountRegister.CurrentRow.Cells(0).Value
        End If
    End Sub

    Private Sub btndelete_Click(sender As System.Object, e As System.EventArgs) Handles btndelete.Click
        If Me.dgvListAccountRegister.SelectedRows.Count <> 0 Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                StatutoryDelete()
                AuditTrail_Save("Statutory Master", "Delete Data")
                Me.txtAccount.Clear()
                Me.txtPercent.Text = "0.00"
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If StatutoryInsertUpdate() Then
            If key = 0 Then
                AuditTrail_Save("Statutory Master", "Insert Data")
            Else
                AuditTrail_Save("Statutory Master", "Update Data")
            End If
            StatutoryList()
            Me.txtAccount.Enabled = False
            Me.txtPercent.Enabled = False
            Me.rbMonth.Enabled = False
            Me.rbYear.Enabled = False
            Me.txtAccount.Clear()
            Me.txtPercent.Text = "0.00"

            Me.btnSave.Visible = False
            Me.btnCancel.Visible = False

            Me.btnNew.Visible = True
            Me.btnEdit.Visible = True
            Me.btndelete.Visible = True

            Me.btnBrowseAccount.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.txtAccount.Enabled = False
        Me.txtPercent.Enabled = False
        Me.rbMonth.Enabled = False
        Me.rbYear.Enabled = False
        Me.txtAccount.Clear()
        Me.txtPercent.Text = "0.00"

        Me.btnSave.Visible = False
        Me.btnCancel.Visible = False

        Me.btnNew.Visible = True
        Me.btnEdit.Visible = True
        Me.btndelete.Visible = True

        Me.btnBrowseAccount.Enabled = False
    End Sub

    Private Sub btnBrowseAccount_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowseAccount.Click
        Dim x As DialogResult
        Dim frm As New frmStatutoryAccountList
        x = frm.ShowDialog()
        If x = Windows.Forms.DialogResult.OK Then
            Me.txtAccount.Tag = frm.dgvAccountList.CurrentRow.Cells("acnt_id").Value.ToString()
            Me.txtAccount.Text = frm.dgvAccountList.CurrentRow.Cells("acnt_name").Value.ToString()
        End If
    End Sub

    Private Sub StatutoryList()
        Try
            Dim mycon As New Clsappconfiguration
            Dim MyGuid As New Guid(modGlobalDeclarations.gCompanyID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "spu_Statutory_List", _
                                       New SqlParameter("@fxKeyCompany", MyGuid))
            Me.dgvListAccountRegister.DataSource = ds.Tables(0)

            With dgvListAccountRegister
                .Columns("fxKeyStatutoryID").Visible = False
                .Columns("fxKeyAccountID").Visible = False

                .Columns("acnt_name").HeaderText = "Account Title"
                .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("acnt_name").Width = 250

                .Columns("fnPercent").HeaderText = "Percent"
                .Columns("fnPercent").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("fnPercent").Width = 100

                .Columns("isYear").HeaderText = "Yearly"
                .Columns("isYear").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("isYear").Width = 100

                .Columns("isMonth").HeaderText = "Monthly"
                .Columns("isMonth").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("isMonth").Width = 100
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub frmStatutoryMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        StatutoryList()
    End Sub

    Private Sub dgvListAccountRegister_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListAccountRegister.CellClick
        If Me.dgvListAccountRegister.SelectedRows.Count <> 0 Then
            With Me.dgvListAccountRegister
                Me.txtAccount.Tag = .CurrentRow.Cells("fxKeyAccountID").Value
                Me.txtAccount.Text = .CurrentRow.Cells("acnt_name").Value

                Me.txtPercent.Text = Format(.CurrentRow.Cells("fnPercent").Value, "##,###,###.00")
                Me.rbMonth.Checked = .CurrentRow.Cells("isMonth").Value
                Me.rbYear.Checked = .CurrentRow.Cells("isYear").Value
            End With
        End If
    End Sub

    Private Sub txtPercent_Leave(sender As System.Object, e As System.EventArgs) Handles txtPercent.Leave
        Me.txtPercent.Text = FormatNumber(Me.txtPercent.Text, 2)
    End Sub
End Class