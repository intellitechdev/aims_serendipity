﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmStatutoryAccountList

    Private Sub frmStatutoryAccountList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtSearch.Focus()
        AccountList()
        'Call DefaultAccount_Search()
        'LoadDoctype()
    End Sub

    Dim gcon As New Clsappconfiguration
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim acntID As String
    Dim key As Integer

    Public Sub AccountList()
        'Try
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "Entries_DefaultAccountList",
                                                        New SqlParameter("coid", gCompanyID))

        dgvAccountList.DataSource = ds.Tables(0)
        With dgvAccountList
            .Columns("acnt_code").Width = 75
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_id").Visible = False
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_name").Width = 273
        End With


        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
    End Sub

    'Private Sub LoadDoctype()
    '    Dim ds As New DataSet
    '    Dim ad As New SqlDataAdapter
    '    Dim cmd As New SqlCommand("Masterfile_LoadDoctype2", gCon.sqlconn)
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Try
    '        ad.SelectCommand = cmd
    '        ad.Fill(ds, "Document")
    '        With cboDocType
    '            .ValueMember = "pk_Initial"
    '            .DisplayMember = "fcDocName"
    '            .DataSource = ds.Tables(0)
    '            '.SelectedIndex = -1
    '            .Text = "Select"
    '        End With
    '        gCon.sqlconn.Close()
    '    Catch ex As Exception

    '    End Try

    'End Sub

    Private Sub SearchAccount(ByVal Account As String)
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "Entries_DefaultFilter", _
                                       New SqlParameter("@acnt_name", Account),
                                       New SqlParameter("coid", gCompanyID))
            dgvAccountList.DataSource = ds.Tables(0)

            With dgvAccountList
                .Columns("acnt_code").Width = 75
                .Columns("acnt_code").HeaderText = "Code"
                .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("acnt_id").Visible = False
                .Columns("acnt_name").HeaderText = "Account Title"
                .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("acnt_name").Width = 273
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Private Sub DefaultAccount_Insert()
    '    Try
    '        SqlHelper.ExecuteNonQuery(gcon.sqlconn, CommandType.StoredProcedure, "DefaultAccount_Insert", _
    '                                  New SqlParameter("@pk_id", key),
    '                                  New SqlParameter("@acnt_id", acntID),
    '                                  New SqlParameter("@acnt_code", txtCode.Text),
    '                                  New SqlParameter("@acnt_name", txtAccount.Text),
    '                                  New SqlParameter("@co_id", gCompanyID),
    '                                  New SqlParameter("@transType", cboDocType.Text),
    '                                  New SqlParameter("@userID", intSysCurrentId))
    '        MsgBox("Successfully Set", vbInformation, "Default Account Set!")
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    'Private Sub DefaultAccount_Search()
    '    Try
    '        Dim rd As SqlDataReader
    '        'MsgBox(intSysCurrentId)
    '        rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "DefaultAccount_Search",
    '                                  New SqlParameter("@co_id", gCompanyID),
    '                                  New SqlParameter("@userID", intSysCurrentId),
    '                                  New SqlParameter("@transType", cboDocType.Text))
    '        If rd.Read Then
    '            txtCode.Text = rd.Item("acnt_code")
    '            txtAccount.Text = rd.Item("acnt_name")
    '            key = rd.Item("pk_id")
    '            frmGeneralJournalEntries.NoneToolStripMenuItem.Text = rd.Item("acnt_name")
    '        Else
    '            txtCode.Clear()
    '            txtAccount.Clear()
    '            key = 0
    '            frmGeneralJournalEntries.NoneToolStripMenuItem.Text = Nothing
    '        End If
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Private Sub SelectDefaultAccount()
        'If cboDocType.Text = "Select" Then
        '    MsgBox("Please select Transaction Type.")
        '    Exit Sub
        'Else
        '    acntID = dgvAccountList.SelectedRows(0).Cells(0).Value.ToString
        '    txtCode.Text = dgvAccountList.SelectedRows(0).Cells(1).Value.ToString
        '    txtAccount.Text = dgvAccountList.SelectedRows(0).Cells(2).Value.ToString
        '    'frmGeneralJournalEntries.NoneToolStripMenuItem.Text = dgvAccountList.SelectedRows(0).Cells(2).Value.ToString
        '    Call DefaultAccount_Insert()
        '    Call frmGeneralJournalEntries.DefaultAccount_Search()
        'End If

    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        If Me.dgvAccountList.SelectedRows.Count <> 0 Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub dgvAccountList_DoubleClick(sender As System.Object, e As System.EventArgs) Handles dgvAccountList.DoubleClick
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        Call SearchAccount(txtSearch.Text)
    End Sub
End Class