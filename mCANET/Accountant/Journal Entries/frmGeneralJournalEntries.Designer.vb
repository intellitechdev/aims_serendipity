<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralJournalEntries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGeneralJournalEntries))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitGeneralJournal = New System.Windows.Forms.SplitContainer()
        Me.lblNotif = New System.Windows.Forms.Label()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.lblPostingNotification = New System.Windows.Forms.Label()
        Me.grdGenJournalDetails = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblStatus = New System.Windows.Forms.TextBox()
        Me.dtpDatePrepared = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.lblclientname = New System.Windows.Forms.Label()
        Me.txtclientname = New System.Windows.Forms.TextBox()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.PBsign = New System.Windows.Forms.PictureBox()
        Me.PBimage = New System.Windows.Forms.PictureBox()
        Me.chkCancelled = New System.Windows.Forms.CheckBox()
        Me.chkPosted = New System.Windows.Forms.CheckBox()
        Me.txtCollector = New System.Windows.Forms.TextBox()
        Me.txtCreatedBy = New System.Windows.Forms.TextBox()
        Me.lblCollector = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnDisplay = New System.Windows.Forms.Button()
        Me.btnSelectDocNum = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtref = New System.Windows.Forms.TextBox()
        Me.cboDoctype = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtGeneralJournalNo = New System.Windows.Forms.TextBox()
        Me.lblEntryNo = New System.Windows.Forms.Label()
        Me.dteGeneralJournal = New System.Windows.Forms.DateTimePicker()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.DeleteDocToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseF6ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintVoucherToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DefaultAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NoneToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecurringEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.grdListofEntries = New System.Windows.Forms.DataGridView()
        Me.txtcheck = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblsoa = New System.Windows.Forms.Label()
        Me.txtsoa = New System.Windows.Forms.TextBox()
        Me.txtCashBalance = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtAvailable = New System.Windows.Forms.TextBox()
        Me.lblCurrent = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.txtCurrent = New System.Windows.Forms.TextBox()
        Me.lblAvailable = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.txtTotalCredit = New System.Windows.Forms.TextBox()
        Me.txtTotalDebit = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txttotal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMemo = New System.Windows.Forms.TextBox()
        Me.dtTo = New System.Windows.Forms.DateTimePicker()
        Me.lblDateTo = New System.Windows.Forms.Label()
        Me.lblDateFrom = New System.Windows.Forms.Label()
        Me.dtFrom = New System.Windows.Forms.DateTimePicker()
        Me.lblDateRange = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CheckDepositToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewVoucherF3ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowSiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddBankToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtBank = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_genjour_totalcredit = New System.Windows.Forms.TextBox()
        Me.txt_genjour_totaldebit = New System.Windows.Forms.TextBox()
        Me.bgwJournalEntries = New System.ComponentModel.BackgroundWorker()
        Me.bgwPosting = New System.ComponentModel.BackgroundWorker()
        Me.bgwUnposting = New System.ComponentModel.BackgroundWorker()
        Me.bgwload = New System.ComponentModel.BackgroundWorker()
        CType(Me.SplitGeneralJournal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitGeneralJournal.Panel1.SuspendLayout()
        Me.SplitGeneralJournal.Panel2.SuspendLayout()
        Me.SplitGeneralJournal.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdGenJournalDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PBsign, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBimage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip2.SuspendLayout()
        CType(Me.grdListofEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitGeneralJournal
        '
        Me.SplitGeneralJournal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitGeneralJournal.Location = New System.Drawing.Point(0, 0)
        Me.SplitGeneralJournal.Name = "SplitGeneralJournal"
        Me.SplitGeneralJournal.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitGeneralJournal.Panel1
        '
        Me.SplitGeneralJournal.Panel1.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.lblNotif)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.picLoading)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.lblPostingNotification)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.grdGenJournalDetails)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.Panel1)
        '
        'SplitGeneralJournal.Panel2
        '
        Me.SplitGeneralJournal.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.dtpDate)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.MenuStrip2)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.grdListofEntries)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.txtcheck)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.Panel2)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.txtBank)
        Me.SplitGeneralJournal.Size = New System.Drawing.Size(874, 559)
        Me.SplitGeneralJournal.SplitterDistance = 335
        Me.SplitGeneralJournal.SplitterWidth = 3
        Me.SplitGeneralJournal.TabIndex = 1
        '
        'lblNotif
        '
        Me.lblNotif.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblNotif.BackColor = System.Drawing.Color.White
        Me.lblNotif.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotif.Location = New System.Drawing.Point(242, 203)
        Me.lblNotif.Name = "lblNotif"
        Me.lblNotif.Size = New System.Drawing.Size(391, 31)
        Me.lblNotif.TabIndex = 48
        Me.lblNotif.Text = "NO DOCUMENT NUMBER AVAILABLE"
        Me.lblNotif.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblNotif.Visible = False
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = CType(resources.GetObject("picLoading.Image"), System.Drawing.Image)
        Me.picLoading.Location = New System.Drawing.Point(-7, 138)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(11, 29)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLoading.TabIndex = 47
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'lblPostingNotification
        '
        Me.lblPostingNotification.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblPostingNotification.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostingNotification.Location = New System.Drawing.Point(-7, 126)
        Me.lblPostingNotification.Name = "lblPostingNotification"
        Me.lblPostingNotification.Size = New System.Drawing.Size(11, 23)
        Me.lblPostingNotification.TabIndex = 46
        Me.lblPostingNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPostingNotification.Visible = False
        '
        'grdGenJournalDetails
        '
        Me.grdGenJournalDetails.AllowUserToResizeRows = False
        Me.grdGenJournalDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdGenJournalDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.grdGenJournalDetails.BackgroundColor = System.Drawing.Color.White
        Me.grdGenJournalDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdGenJournalDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdGenJournalDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.grdGenJournalDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdGenJournalDetails.DefaultCellStyle = DataGridViewCellStyle7
        Me.grdGenJournalDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdGenJournalDetails.GridColor = System.Drawing.Color.DarkGray
        Me.grdGenJournalDetails.Location = New System.Drawing.Point(0, 115)
        Me.grdGenJournalDetails.MultiSelect = False
        Me.grdGenJournalDetails.Name = "grdGenJournalDetails"
        Me.grdGenJournalDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.grdGenJournalDetails.Size = New System.Drawing.Size(874, 220)
        Me.grdGenJournalDetails.TabIndex = 46
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel1.Controls.Add(Me.lblStatus)
        Me.Panel1.Controls.Add(Me.dtpDatePrepared)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.ProgressBar1)
        Me.Panel1.Controls.Add(Me.lblclientname)
        Me.Panel1.Controls.Add(Me.txtclientname)
        Me.Panel1.Controls.Add(Me.lblCode)
        Me.Panel1.Controls.Add(Me.txtCode)
        Me.Panel1.Controls.Add(Me.PBsign)
        Me.Panel1.Controls.Add(Me.PBimage)
        Me.Panel1.Controls.Add(Me.chkCancelled)
        Me.Panel1.Controls.Add(Me.chkPosted)
        Me.Panel1.Controls.Add(Me.txtCollector)
        Me.Panel1.Controls.Add(Me.txtCreatedBy)
        Me.Panel1.Controls.Add(Me.lblCollector)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btnDisplay)
        Me.Panel1.Controls.Add(Me.btnSelectDocNum)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtref)
        Me.Panel1.Controls.Add(Me.cboDoctype)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtGeneralJournalNo)
        Me.Panel1.Controls.Add(Me.lblEntryNo)
        Me.Panel1.Controls.Add(Me.dteGeneralJournal)
        Me.Panel1.Controls.Add(Me.lblDate)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(874, 115)
        Me.Panel1.TabIndex = 0
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStatus.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(99, 91)
        Me.lblStatus.Multiline = True
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.ReadOnly = True
        Me.lblStatus.Size = New System.Drawing.Size(516, 15)
        Me.lblStatus.TabIndex = 74
        '
        'dtpDatePrepared
        '
        Me.dtpDatePrepared.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpDatePrepared.CustomFormat = "MMMM dd, yyyy"
        Me.dtpDatePrepared.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDatePrepared.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDatePrepared.Location = New System.Drawing.Point(728, 7)
        Me.dtpDatePrepared.Name = "dtpDatePrepared"
        Me.dtpDatePrepared.Size = New System.Drawing.Size(139, 20)
        Me.dtpDatePrepared.TabIndex = 72
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(650, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 13)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "Date Prepared"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(4, 91)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(90, 16)
        Me.ProgressBar1.TabIndex = 70
        Me.ProgressBar1.Visible = False
        '
        'lblclientname
        '
        Me.lblclientname.AutoSize = True
        Me.lblclientname.BackColor = System.Drawing.Color.Transparent
        Me.lblclientname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblclientname.Location = New System.Drawing.Point(269, 45)
        Me.lblclientname.Name = "lblclientname"
        Me.lblclientname.Size = New System.Drawing.Size(35, 13)
        Me.lblclientname.TabIndex = 69
        Me.lblclientname.Text = "Name"
        '
        'txtclientname
        '
        Me.txtclientname.BackColor = System.Drawing.Color.White
        Me.txtclientname.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtclientname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtclientname.ForeColor = System.Drawing.Color.Black
        Me.txtclientname.Location = New System.Drawing.Point(270, 60)
        Me.txtclientname.Multiline = True
        Me.txtclientname.Name = "txtclientname"
        Me.txtclientname.ReadOnly = True
        Me.txtclientname.Size = New System.Drawing.Size(182, 20)
        Me.txtclientname.TabIndex = 68
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.BackColor = System.Drawing.Color.Transparent
        Me.lblCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(267, 7)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(38, 13)
        Me.lblCode.TabIndex = 67
        Me.lblCode.Text = "ID No."
        '
        'txtCode
        '
        Me.txtCode.BackColor = System.Drawing.Color.White
        Me.txtCode.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.ForeColor = System.Drawing.Color.Black
        Me.txtCode.Location = New System.Drawing.Point(268, 23)
        Me.txtCode.Multiline = True
        Me.txtCode.Name = "txtCode"
        Me.txtCode.ReadOnly = True
        Me.txtCode.Size = New System.Drawing.Size(182, 20)
        Me.txtCode.TabIndex = 66
        '
        'PBsign
        '
        Me.PBsign.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.PBsign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PBsign.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PBsign.Image = CType(resources.GetObject("PBsign.Image"), System.Drawing.Image)
        Me.PBsign.Location = New System.Drawing.Point(535, 3)
        Me.PBsign.Name = "PBsign"
        Me.PBsign.Size = New System.Drawing.Size(114, 74)
        Me.PBsign.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBsign.TabIndex = 65
        Me.PBsign.TabStop = False
        '
        'PBimage
        '
        Me.PBimage.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.PBimage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PBimage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PBimage.Image = CType(resources.GetObject("PBimage.Image"), System.Drawing.Image)
        Me.PBimage.Location = New System.Drawing.Point(459, 3)
        Me.PBimage.Name = "PBimage"
        Me.PBimage.Size = New System.Drawing.Size(71, 74)
        Me.PBimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBimage.TabIndex = 64
        Me.PBimage.TabStop = False
        '
        'chkCancelled
        '
        Me.chkCancelled.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkCancelled.AutoSize = True
        Me.chkCancelled.BackColor = System.Drawing.Color.Transparent
        Me.chkCancelled.Location = New System.Drawing.Point(746, 91)
        Me.chkCancelled.Name = "chkCancelled"
        Me.chkCancelled.Size = New System.Drawing.Size(121, 17)
        Me.chkCancelled.TabIndex = 63
        Me.chkCancelled.Text = "&Cancelled (ALT + C)"
        Me.chkCancelled.UseVisualStyleBackColor = False
        '
        'chkPosted
        '
        Me.chkPosted.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkPosted.AutoSize = True
        Me.chkPosted.BackColor = System.Drawing.Color.Transparent
        Me.chkPosted.Location = New System.Drawing.Point(633, 90)
        Me.chkPosted.Name = "chkPosted"
        Me.chkPosted.Size = New System.Drawing.Size(107, 17)
        Me.chkPosted.TabIndex = 62
        Me.chkPosted.Text = "&Posted (ALT + P)"
        Me.chkPosted.UseVisualStyleBackColor = False
        '
        'txtCollector
        '
        Me.txtCollector.BackColor = System.Drawing.Color.White
        Me.txtCollector.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCollector.Location = New System.Drawing.Point(72, 9)
        Me.txtCollector.Multiline = True
        Me.txtCollector.Name = "txtCollector"
        Me.txtCollector.ReadOnly = True
        Me.txtCollector.Size = New System.Drawing.Size(192, 21)
        Me.txtCollector.TabIndex = 61
        '
        'txtCreatedBy
        '
        Me.txtCreatedBy.BackColor = System.Drawing.Color.White
        Me.txtCreatedBy.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCreatedBy.Location = New System.Drawing.Point(73, 34)
        Me.txtCreatedBy.Multiline = True
        Me.txtCreatedBy.Name = "txtCreatedBy"
        Me.txtCreatedBy.ReadOnly = True
        Me.txtCreatedBy.Size = New System.Drawing.Size(192, 21)
        Me.txtCreatedBy.TabIndex = 60
        '
        'lblCollector
        '
        Me.lblCollector.AutoSize = True
        Me.lblCollector.BackColor = System.Drawing.Color.Transparent
        Me.lblCollector.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCollector.Location = New System.Drawing.Point(7, 11)
        Me.lblCollector.Name = "lblCollector"
        Me.lblCollector.Size = New System.Drawing.Size(48, 13)
        Me.lblCollector.TabIndex = 59
        Me.lblCollector.Text = "Collector"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 58
        Me.Label5.Text = "Created by"
        '
        'btnDisplay
        '
        Me.btnDisplay.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisplay.Image = CType(resources.GetObject("btnDisplay.Image"), System.Drawing.Image)
        Me.btnDisplay.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDisplay.Location = New System.Drawing.Point(871, 226)
        Me.btnDisplay.Name = "btnDisplay"
        Me.btnDisplay.Size = New System.Drawing.Size(11, 24)
        Me.btnDisplay.TabIndex = 55
        Me.btnDisplay.Text = "Display List"
        Me.btnDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDisplay.UseVisualStyleBackColor = True
        Me.btnDisplay.Visible = False
        '
        'btnSelectDocNum
        '
        Me.btnSelectDocNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSelectDocNum.Enabled = False
        Me.btnSelectDocNum.Location = New System.Drawing.Point(651, 54)
        Me.btnSelectDocNum.Name = "btnSelectDocNum"
        Me.btnSelectDocNum.Size = New System.Drawing.Size(25, 23)
        Me.btnSelectDocNum.TabIndex = 57
        Me.btnSelectDocNum.Text = "..."
        Me.btnSelectDocNum.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(858, 231)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 15)
        Me.Label2.TabIndex = 56
        Me.Label2.Text = "Ref"
        Me.Label2.Visible = False
        '
        'txtref
        '
        Me.txtref.Location = New System.Drawing.Point(869, 229)
        Me.txtref.Name = "txtref"
        Me.txtref.Size = New System.Drawing.Size(10, 20)
        Me.txtref.TabIndex = 49
        Me.txtref.Visible = False
        '
        'cboDoctype
        '
        Me.cboDoctype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDoctype.FormattingEnabled = True
        Me.cboDoctype.Location = New System.Drawing.Point(72, 59)
        Me.cboDoctype.Name = "cboDoctype"
        Me.cboDoctype.Size = New System.Drawing.Size(192, 21)
        Me.cboDoctype.TabIndex = 48
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 65)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Doc.Type"
        '
        'txtGeneralJournalNo
        '
        Me.txtGeneralJournalNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGeneralJournalNo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtGeneralJournalNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneralJournalNo.Location = New System.Drawing.Point(728, 56)
        Me.txtGeneralJournalNo.Name = "txtGeneralJournalNo"
        Me.txtGeneralJournalNo.ReadOnly = True
        Me.txtGeneralJournalNo.Size = New System.Drawing.Size(139, 20)
        Me.txtGeneralJournalNo.TabIndex = 46
        '
        'lblEntryNo
        '
        Me.lblEntryNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEntryNo.AutoSize = True
        Me.lblEntryNo.BackColor = System.Drawing.Color.Transparent
        Me.lblEntryNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntryNo.Location = New System.Drawing.Point(678, 59)
        Me.lblEntryNo.Name = "lblEntryNo"
        Me.lblEntryNo.Size = New System.Drawing.Size(47, 13)
        Me.lblEntryNo.TabIndex = 45
        Me.lblEntryNo.Text = "Doc No."
        '
        'dteGeneralJournal
        '
        Me.dteGeneralJournal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteGeneralJournal.CustomFormat = "MMMM dd, yyyy"
        Me.dteGeneralJournal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteGeneralJournal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteGeneralJournal.Location = New System.Drawing.Point(728, 31)
        Me.dteGeneralJournal.Name = "dteGeneralJournal"
        Me.dteGeneralJournal.Size = New System.Drawing.Size(139, 20)
        Me.dteGeneralJournal.TabIndex = 44
        '
        'lblDate
        '
        Me.lblDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDate.AutoSize = True
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(650, 34)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(66, 13)
        Me.lblDate.TabIndex = 43
        Me.lblDate.Text = "Date Posted"
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpDate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(326, 131)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(127, 20)
        Me.dtpDate.TabIndex = 76
        Me.dtpDate.Visible = False
        '
        'MenuStrip2
        '
        Me.MenuStrip2.AutoSize = False
        Me.MenuStrip2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.MenuStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MenuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteDocToolStripMenuItem, Me.CloseF6ToolStripMenuItem, Me.SaveToolStripMenuItem, Me.NewFToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.SearchToolStripMenuItem, Me.PrintCheckToolStripMenuItem, Me.PrintVoucherToolStripMenuItem, Me.DefaultAccountToolStripMenuItem, Me.RecurringEntryToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(0, 192)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Padding = New System.Windows.Forms.Padding(5, 2, 0, 2)
        Me.MenuStrip2.Size = New System.Drawing.Size(874, 29)
        Me.MenuStrip2.TabIndex = 54
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'DeleteDocToolStripMenuItem
        '
        Me.DeleteDocToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteDocToolStripMenuItem.Image = CType(resources.GetObject("DeleteDocToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DeleteDocToolStripMenuItem.Name = "DeleteDocToolStripMenuItem"
        Me.DeleteDocToolStripMenuItem.ShortcutKeyDisplayString = "F5"
        Me.DeleteDocToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.DeleteDocToolStripMenuItem.Size = New System.Drawing.Size(118, 25)
        Me.DeleteDocToolStripMenuItem.Text = "Delete Doc. (F5)"
        '
        'CloseF6ToolStripMenuItem
        '
        Me.CloseF6ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CloseF6ToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseF6ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CloseF6ToolStripMenuItem.Name = "CloseF6ToolStripMenuItem"
        Me.CloseF6ToolStripMenuItem.ShortcutKeyDisplayString = "Alt+X"
        Me.CloseF6ToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CloseF6ToolStripMenuItem.Size = New System.Drawing.Size(89, 25)
        Me.CloseF6ToolStripMenuItem.Text = "Close (Alt+X)"
        Me.CloseF6ToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.SaveToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S"
        Me.SaveToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(87, 25)
        Me.SaveToolStripMenuItem.Text = "Save (Ctrl+S)"
        '
        'NewFToolStripMenuItem
        '
        Me.NewFToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.NewFToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NewFToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.NewFToolStripMenuItem.Name = "NewFToolStripMenuItem"
        Me.NewFToolStripMenuItem.ShortcutKeyDisplayString = "F8"
        Me.NewFToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8
        Me.NewFToolStripMenuItem.Size = New System.Drawing.Size(66, 25)
        Me.NewFToolStripMenuItem.Text = "New (F8)"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.RefreshToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.ShortcutKeyDisplayString = "F9"
        Me.RefreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(81, 25)
        Me.RefreshToolStripMenuItem.Text = "Refresh (F9)"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.SearchToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchToolStripMenuItem.Image = CType(resources.GetObject("SearchToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SearchToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.ShortcutKeyDisplayString = "F10"
        Me.SearchToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(99, 25)
        Me.SearchToolStripMenuItem.Text = "Search (F10)"
        '
        'PrintCheckToolStripMenuItem
        '
        Me.PrintCheckToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintCheckToolStripMenuItem.Name = "PrintCheckToolStripMenuItem"
        Me.PrintCheckToolStripMenuItem.ShortcutKeyDisplayString = "F11"
        Me.PrintCheckToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11
        Me.PrintCheckToolStripMenuItem.Size = New System.Drawing.Size(109, 25)
        Me.PrintCheckToolStripMenuItem.Text = "Print Check (F11)"
        '
        'PrintVoucherToolStripMenuItem
        '
        Me.PrintVoucherToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintVoucherToolStripMenuItem.Name = "PrintVoucherToolStripMenuItem"
        Me.PrintVoucherToolStripMenuItem.ShortcutKeyDisplayString = "F11"
        Me.PrintVoucherToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12
        Me.PrintVoucherToolStripMenuItem.Size = New System.Drawing.Size(120, 25)
        Me.PrintVoucherToolStripMenuItem.Text = "P&rint Voucher (F12)"
        '
        'DefaultAccountToolStripMenuItem
        '
        Me.DefaultAccountToolStripMenuItem.DoubleClickEnabled = True
        Me.DefaultAccountToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NoneToolStripMenuItem})
        Me.DefaultAccountToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DefaultAccountToolStripMenuItem.Name = "DefaultAccountToolStripMenuItem"
        Me.DefaultAccountToolStripMenuItem.Size = New System.Drawing.Size(105, 25)
        Me.DefaultAccountToolStripMenuItem.Text = "Default Account"
        '
        'NoneToolStripMenuItem
        '
        Me.NoneToolStripMenuItem.Name = "NoneToolStripMenuItem"
        Me.NoneToolStripMenuItem.Size = New System.Drawing.Size(103, 22)
        Me.NoneToolStripMenuItem.Text = "None"
        Me.NoneToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'RecurringEntryToolStripMenuItem
        '
        Me.RecurringEntryToolStripMenuItem.Enabled = False
        Me.RecurringEntryToolStripMenuItem.Name = "RecurringEntryToolStripMenuItem"
        Me.RecurringEntryToolStripMenuItem.Size = New System.Drawing.Size(100, 25)
        Me.RecurringEntryToolStripMenuItem.Text = "Recurring Entry"
        '
        'grdListofEntries
        '
        Me.grdListofEntries.AllowUserToOrderColumns = True
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        Me.grdListofEntries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.grdListofEntries.BackgroundColor = System.Drawing.Color.White
        Me.grdListofEntries.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdListofEntries.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdListofEntries.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.grdListofEntries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdListofEntries.DefaultCellStyle = DataGridViewCellStyle10
        Me.grdListofEntries.Dock = System.Windows.Forms.DockStyle.Top
        Me.grdListofEntries.Location = New System.Drawing.Point(0, 112)
        Me.grdListofEntries.MultiSelect = False
        Me.grdListofEntries.Name = "grdListofEntries"
        Me.grdListofEntries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.grdListofEntries.Size = New System.Drawing.Size(874, 80)
        Me.grdListofEntries.TabIndex = 45
        '
        'txtcheck
        '
        Me.txtcheck.Location = New System.Drawing.Point(520, 192)
        Me.txtcheck.Name = "txtcheck"
        Me.txtcheck.Size = New System.Drawing.Size(86, 20)
        Me.txtcheck.TabIndex = 75
        Me.txtcheck.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel2.Controls.Add(Me.lblsoa)
        Me.Panel2.Controls.Add(Me.txtsoa)
        Me.Panel2.Controls.Add(Me.txtCashBalance)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.txtAvailable)
        Me.Panel2.Controls.Add(Me.lblCurrent)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Controls.Add(Me.txtCurrent)
        Me.Panel2.Controls.Add(Me.lblAvailable)
        Me.Panel2.Controls.Add(Me.lblTotal)
        Me.Panel2.Controls.Add(Me.txtTotalCredit)
        Me.Panel2.Controls.Add(Me.txtTotalDebit)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txttotal)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.txtMemo)
        Me.Panel2.Controls.Add(Me.dtTo)
        Me.Panel2.Controls.Add(Me.lblDateTo)
        Me.Panel2.Controls.Add(Me.lblDateFrom)
        Me.Panel2.Controls.Add(Me.dtFrom)
        Me.Panel2.Controls.Add(Me.lblDateRange)
        Me.Panel2.Controls.Add(Me.MenuStrip1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(874, 112)
        Me.Panel2.TabIndex = 1
        '
        'lblsoa
        '
        Me.lblsoa.AutoSize = True
        Me.lblsoa.BackColor = System.Drawing.Color.Transparent
        Me.lblsoa.Location = New System.Drawing.Point(375, 36)
        Me.lblsoa.Name = "lblsoa"
        Me.lblsoa.Size = New System.Drawing.Size(49, 13)
        Me.lblsoa.TabIndex = 81
        Me.lblsoa.Text = "SOA No."
        Me.lblsoa.Visible = False
        '
        'txtsoa
        '
        Me.txtsoa.BackColor = System.Drawing.Color.White
        Me.txtsoa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtsoa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsoa.Location = New System.Drawing.Point(426, 37)
        Me.txtsoa.Name = "txtsoa"
        Me.txtsoa.ReadOnly = True
        Me.txtsoa.Size = New System.Drawing.Size(91, 13)
        Me.txtsoa.TabIndex = 82
        Me.txtsoa.Visible = False
        '
        'txtCashBalance
        '
        Me.txtCashBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCashBalance.BackColor = System.Drawing.Color.White
        Me.txtCashBalance.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCashBalance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCashBalance.Location = New System.Drawing.Point(600, 36)
        Me.txtCashBalance.Name = "txtCashBalance"
        Me.txtCashBalance.ReadOnly = True
        Me.txtCashBalance.Size = New System.Drawing.Size(91, 13)
        Me.txtCashBalance.TabIndex = 80
        Me.txtCashBalance.Text = "0.00"
        Me.txtCashBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCashBalance.Visible = False
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Location = New System.Drawing.Point(522, 36)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 13)
        Me.Label8.TabIndex = 79
        Me.Label8.Text = "Cash Balance"
        Me.Label8.Visible = False
        '
        'txtAvailable
        '
        Me.txtAvailable.BackColor = System.Drawing.Color.White
        Me.txtAvailable.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAvailable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAvailable.Location = New System.Drawing.Point(405, 9)
        Me.txtAvailable.Name = "txtAvailable"
        Me.txtAvailable.ReadOnly = True
        Me.txtAvailable.Size = New System.Drawing.Size(91, 13)
        Me.txtAvailable.TabIndex = 72
        Me.txtAvailable.Text = "0.00"
        Me.txtAvailable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrent
        '
        Me.lblCurrent.AutoSize = True
        Me.lblCurrent.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrent.Location = New System.Drawing.Point(307, 26)
        Me.lblCurrent.Name = "lblCurrent"
        Me.lblCurrent.Size = New System.Drawing.Size(83, 13)
        Me.lblCurrent.TabIndex = 71
        Me.lblCurrent.Text = "Current Balance"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.LinkLabel2)
        Me.Panel5.Controls.Add(Me.LinkLabel4)
        Me.Panel5.Controls.Add(Me.LinkLabel3)
        Me.Panel5.Controls.Add(Me.LinkLabel1)
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(432, 24)
        Me.Panel5.TabIndex = 78
        '
        'LinkLabel2
        '
        Me.LinkLabel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.LinkLabel2.Image = CType(resources.GetObject("LinkLabel2.Image"), System.Drawing.Image)
        Me.LinkLabel2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LinkLabel2.LinkColor = System.Drawing.Color.Black
        Me.LinkLabel2.Location = New System.Drawing.Point(224, 0)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(112, 24)
        Me.LinkLabel2.TabIndex = 76
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Delete Line Entry"
        Me.LinkLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LinkLabel4
        '
        Me.LinkLabel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.LinkLabel4.Image = CType(resources.GetObject("LinkLabel4.Image"), System.Drawing.Image)
        Me.LinkLabel4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LinkLabel4.LinkColor = System.Drawing.Color.Black
        Me.LinkLabel4.Location = New System.Drawing.Point(123, 0)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(101, 24)
        Me.LinkLabel4.TabIndex = 75
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Payroll Entry"
        Me.LinkLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LinkLabel3.LinkColor = System.Drawing.Color.Black
        Me.LinkLabel3.Location = New System.Drawing.Point(213, 6)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(81, 13)
        Me.LinkLabel3.TabIndex = 73
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Savings Ledger"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.LinkLabel1.Image = CType(resources.GetObject("LinkLabel1.Image"), System.Drawing.Image)
        Me.LinkLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LinkLabel1.LinkColor = System.Drawing.Color.Black
        Me.LinkLabel1.Location = New System.Drawing.Point(0, 0)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(123, 24)
        Me.LinkLabel1.TabIndex = 71
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Loan for Release"
        Me.LinkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCurrent
        '
        Me.txtCurrent.BackColor = System.Drawing.Color.White
        Me.txtCurrent.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCurrent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrent.Location = New System.Drawing.Point(405, 27)
        Me.txtCurrent.Name = "txtCurrent"
        Me.txtCurrent.ReadOnly = True
        Me.txtCurrent.Size = New System.Drawing.Size(91, 13)
        Me.txtCurrent.TabIndex = 73
        Me.txtCurrent.Text = "0.00"
        Me.txtCurrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAvailable
        '
        Me.lblAvailable.AutoSize = True
        Me.lblAvailable.BackColor = System.Drawing.Color.Transparent
        Me.lblAvailable.Location = New System.Drawing.Point(307, 9)
        Me.lblAvailable.Name = "lblAvailable"
        Me.lblAvailable.Size = New System.Drawing.Size(92, 13)
        Me.lblAvailable.TabIndex = 70
        Me.lblAvailable.Text = "Available Balance"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(615, 9)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(39, 13)
        Me.lblTotal.TabIndex = 70
        Me.lblTotal.Text = "Totals:"
        '
        'txtTotalCredit
        '
        Me.txtTotalCredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalCredit.BackColor = System.Drawing.Color.White
        Me.txtTotalCredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalCredit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCredit.Location = New System.Drawing.Point(776, 10)
        Me.txtTotalCredit.Name = "txtTotalCredit"
        Me.txtTotalCredit.ReadOnly = True
        Me.txtTotalCredit.Size = New System.Drawing.Size(91, 13)
        Me.txtTotalCredit.TabIndex = 67
        Me.txtTotalCredit.Text = "0.00"
        Me.txtTotalCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalDebit
        '
        Me.txtTotalDebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalDebit.BackColor = System.Drawing.Color.White
        Me.txtTotalDebit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalDebit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalDebit.Location = New System.Drawing.Point(668, 10)
        Me.txtTotalDebit.Name = "txtTotalDebit"
        Me.txtTotalDebit.ReadOnly = True
        Me.txtTotalDebit.Size = New System.Drawing.Size(95, 13)
        Me.txtTotalDebit.TabIndex = 66
        Me.txtTotalDebit.Text = "0.00"
        Me.txtTotalDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(709, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 65
        Me.Label4.Text = "Difference"
        '
        'txttotal
        '
        Me.txttotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttotal.BackColor = System.Drawing.Color.White
        Me.txttotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txttotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttotal.ForeColor = System.Drawing.Color.Red
        Me.txttotal.Location = New System.Drawing.Point(776, 35)
        Me.txttotal.Name = "txttotal"
        Me.txttotal.ReadOnly = True
        Me.txttotal.Size = New System.Drawing.Size(91, 13)
        Me.txttotal.TabIndex = 64
        Me.txttotal.Text = "0.00"
        Me.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "Particulars:"
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.BackColor = System.Drawing.Color.White
        Me.txtMemo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMemo.Location = New System.Drawing.Point(69, 57)
        Me.txtMemo.Multiline = True
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.ReadOnly = True
        Me.txtMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemo.Size = New System.Drawing.Size(797, 30)
        Me.txtMemo.TabIndex = 57
        '
        'dtTo
        '
        Me.dtTo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtTo.Location = New System.Drawing.Point(861, 192)
        Me.dtTo.Name = "dtTo"
        Me.dtTo.Size = New System.Drawing.Size(10, 23)
        Me.dtTo.TabIndex = 54
        Me.dtTo.Visible = False
        '
        'lblDateTo
        '
        Me.lblDateTo.AutoSize = True
        Me.lblDateTo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(869, 132)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(0, 15)
        Me.lblDateTo.TabIndex = 53
        Me.lblDateTo.Visible = False
        '
        'lblDateFrom
        '
        Me.lblDateFrom.AutoSize = True
        Me.lblDateFrom.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(859, 194)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(0, 15)
        Me.lblDateFrom.TabIndex = 52
        Me.lblDateFrom.Visible = False
        '
        'dtFrom
        '
        Me.dtFrom.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFrom.Location = New System.Drawing.Point(871, 192)
        Me.dtFrom.Name = "dtFrom"
        Me.dtFrom.Size = New System.Drawing.Size(10, 23)
        Me.dtFrom.TabIndex = 51
        Me.dtFrom.Value = New Date(2011, 1, 1, 0, 0, 0, 0)
        Me.dtFrom.Visible = False
        '
        'lblDateRange
        '
        Me.lblDateRange.AutoSize = True
        Me.lblDateRange.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateRange.Location = New System.Drawing.Point(847, 194)
        Me.lblDateRange.Name = "lblDateRange"
        Me.lblDateRange.Size = New System.Drawing.Size(0, 15)
        Me.lblDateRange.TabIndex = 50
        Me.lblDateRange.Visible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckDepositToolStripMenuItem, Me.AddCheckToolStripMenuItem, Me.ViewCheckToolStripMenuItem, Me.ViewVoucherF3ToolStripMenuItem, Me.ShowSiToolStripMenuItem, Me.ViewImageToolStripMenuItem, Me.AddBankToolStripMenuItem, Me.ToolStripMenuItem1, Me.DeleteCheckToolStripMenuItem, Me.ToolStripMenuItem2, Me.ToolStripMenuItem3})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 88)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(5, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(874, 24)
        Me.MenuStrip1.TabIndex = 76
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CheckDepositToolStripMenuItem
        '
        Me.CheckDepositToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.CheckDepositToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckDepositToolStripMenuItem.Name = "CheckDepositToolStripMenuItem"
        Me.CheckDepositToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.CheckDepositToolStripMenuItem.Text = "Check Deposit >>"
        Me.CheckDepositToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AddCheckToolStripMenuItem
        '
        Me.AddCheckToolStripMenuItem.Image = CType(resources.GetObject("AddCheckToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AddCheckToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddCheckToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.AddCheckToolStripMenuItem.Name = "AddCheckToolStripMenuItem"
        Me.AddCheckToolStripMenuItem.ShortcutKeyDisplayString = "(F1)"
        Me.AddCheckToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.AddCheckToolStripMenuItem.Size = New System.Drawing.Size(116, 20)
        Me.AddCheckToolStripMenuItem.Text = "Add Check (F1)"
        Me.AddCheckToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.AddCheckToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal
        '
        'ViewCheckToolStripMenuItem
        '
        Me.ViewCheckToolStripMenuItem.Enabled = False
        Me.ViewCheckToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ViewCheckToolStripMenuItem.Name = "ViewCheckToolStripMenuItem"
        Me.ViewCheckToolStripMenuItem.ShortcutKeyDisplayString = ""
        Me.ViewCheckToolStripMenuItem.Size = New System.Drawing.Size(103, 20)
        Me.ViewCheckToolStripMenuItem.Text = "View Check (F2)"
        Me.ViewCheckToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ViewVoucherF3ToolStripMenuItem
        '
        Me.ViewVoucherF3ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ViewVoucherF3ToolStripMenuItem.Name = "ViewVoucherF3ToolStripMenuItem"
        Me.ViewVoucherF3ToolStripMenuItem.ShortcutKeyDisplayString = "F3"
        Me.ViewVoucherF3ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3
        Me.ViewVoucherF3ToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.ViewVoucherF3ToolStripMenuItem.Text = "View Voucher (F3)"
        Me.ViewVoucherF3ToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ShowSiToolStripMenuItem
        '
        Me.ShowSiToolStripMenuItem.Name = "ShowSiToolStripMenuItem"
        Me.ShowSiToolStripMenuItem.Size = New System.Drawing.Size(101, 20)
        Me.ShowSiToolStripMenuItem.Text = "Show Signature"
        '
        'ViewImageToolStripMenuItem
        '
        Me.ViewImageToolStripMenuItem.Name = "ViewImageToolStripMenuItem"
        Me.ViewImageToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.ViewImageToolStripMenuItem.Text = "View Image"
        '
        'AddBankToolStripMenuItem
        '
        Me.AddBankToolStripMenuItem.Image = CType(resources.GetObject("AddBankToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AddBankToolStripMenuItem.Name = "AddBankToolStripMenuItem"
        Me.AddBankToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.AddBankToolStripMenuItem.Text = "Add Bank"
        Me.AddBankToolStripMenuItem.Visible = False
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(12, 20)
        '
        'DeleteCheckToolStripMenuItem
        '
        Me.DeleteCheckToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteCheckToolStripMenuItem.Image = CType(resources.GetObject("DeleteCheckToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DeleteCheckToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DeleteCheckToolStripMenuItem.Name = "DeleteCheckToolStripMenuItem"
        Me.DeleteCheckToolStripMenuItem.ShortcutKeyDisplayString = "F4"
        Me.DeleteCheckToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.DeleteCheckToolStripMenuItem.Size = New System.Drawing.Size(127, 20)
        Me.DeleteCheckToolStripMenuItem.Text = "Delete Check (F4)"
        Me.DeleteCheckToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(12, 20)
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(12, 20)
        '
        'txtBank
        '
        Me.txtBank.Location = New System.Drawing.Point(426, 192)
        Me.txtBank.Name = "txtBank"
        Me.txtBank.Size = New System.Drawing.Size(86, 20)
        Me.txtBank.TabIndex = 74
        Me.txtBank.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(183, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Credit"
        '
        'txt_genjour_totalcredit
        '
        Me.txt_genjour_totalcredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_genjour_totalcredit.BackColor = System.Drawing.Color.White
        Me.txt_genjour_totalcredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_genjour_totalcredit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_genjour_totalcredit.Location = New System.Drawing.Point(239, 19)
        Me.txt_genjour_totalcredit.Name = "txt_genjour_totalcredit"
        Me.txt_genjour_totalcredit.Size = New System.Drawing.Size(91, 14)
        Me.txt_genjour_totalcredit.TabIndex = 51
        Me.txt_genjour_totalcredit.Text = "0.00"
        Me.txt_genjour_totalcredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_genjour_totaldebit
        '
        Me.txt_genjour_totaldebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_genjour_totaldebit.BackColor = System.Drawing.Color.White
        Me.txt_genjour_totaldebit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_genjour_totaldebit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_genjour_totaldebit.Location = New System.Drawing.Point(83, 19)
        Me.txt_genjour_totaldebit.Name = "txt_genjour_totaldebit"
        Me.txt_genjour_totaldebit.Size = New System.Drawing.Size(94, 14)
        Me.txt_genjour_totaldebit.TabIndex = 50
        Me.txt_genjour_totaldebit.Text = "0.00"
        Me.txt_genjour_totaldebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'bgwJournalEntries
        '
        Me.bgwJournalEntries.WorkerSupportsCancellation = True
        '
        'bgwPosting
        '
        '
        'bgwUnposting
        '
        '
        'bgwload
        '
        '
        'frmGeneralJournalEntries
        '
        Me.AcceptButton = Me.btnDisplay
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(874, 559)
        Me.Controls.Add(Me.SplitGeneralJournal)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(767, 440)
        Me.Name = "frmGeneralJournalEntries"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Transaction Entry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitGeneralJournal.Panel1.ResumeLayout(False)
        Me.SplitGeneralJournal.Panel2.ResumeLayout(False)
        Me.SplitGeneralJournal.Panel2.PerformLayout()
        CType(Me.SplitGeneralJournal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitGeneralJournal.ResumeLayout(False)
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdGenJournalDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PBsign, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBimage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        CType(Me.grdListofEntries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitGeneralJournal As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtGeneralJournalNo As System.Windows.Forms.TextBox
    Friend WithEvents lblEntryNo As System.Windows.Forms.Label
    Friend WithEvents dteGeneralJournal As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_genjour_totalcredit As System.Windows.Forms.TextBox
    Friend WithEvents txt_genjour_totaldebit As System.Windows.Forms.TextBox
    Friend WithEvents grdGenJournalDetails As System.Windows.Forms.DataGridView
    Friend WithEvents btnDisplay As System.Windows.Forms.Button
    Friend WithEvents dtTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateRange As System.Windows.Forms.Label
    Friend WithEvents lblPostingNotification As System.Windows.Forms.Label
    Friend WithEvents bgwJournalEntries As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents grdListofEntries As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtref As System.Windows.Forms.TextBox
    Friend WithEvents cboDoctype As System.Windows.Forms.ComboBox
    Friend WithEvents btnSelectDocNum As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents txtTotalCredit As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalDebit As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txttotal As System.Windows.Forms.TextBox
    Friend WithEvents txtCollector As System.Windows.Forms.TextBox
    Friend WithEvents txtCreatedBy As System.Windows.Forms.TextBox
    Friend WithEvents lblCollector As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblNotif As System.Windows.Forms.Label
    Friend WithEvents chkCancelled As System.Windows.Forms.CheckBox
    Friend WithEvents chkPosted As System.Windows.Forms.CheckBox
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents txtcheck As System.Windows.Forms.TextBox
    Friend WithEvents txtBank As System.Windows.Forms.TextBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents AddCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewVoucherF3ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents DeleteDocToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseF6ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintVoucherToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PBsign As System.Windows.Forms.PictureBox
    Friend WithEvents PBimage As System.Windows.Forms.PictureBox
    Friend WithEvents ShowSiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
    Friend WithEvents lblCurrent As System.Windows.Forms.Label
    Friend WithEvents lblAvailable As System.Windows.Forms.Label
    Friend WithEvents txtCurrent As System.Windows.Forms.TextBox
    Friend WithEvents txtAvailable As System.Windows.Forms.TextBox
    Friend WithEvents DefaultAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckDepositToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NoneToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddBankToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblclientname As System.Windows.Forms.Label
    Friend WithEvents txtclientname As System.Windows.Forms.TextBox
    Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents dtpDatePrepared As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCashBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel4 As System.Windows.Forms.LinkLabel
    Friend WithEvents lblsoa As System.Windows.Forms.Label
    Friend WithEvents txtsoa As System.Windows.Forms.TextBox
    Friend WithEvents RecurringEntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblStatus As System.Windows.Forms.TextBox
    Friend WithEvents bgwPosting As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwUnposting As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwload As System.ComponentModel.BackgroundWorker
End Class
