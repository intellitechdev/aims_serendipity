Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Math
Imports System.IO
'Notes to remember:   
'DbNull cannot be casted to String
'Nothing can be casted to String

Public Class frmGeneralJournalEntries

    Private gCon As New Clsappconfiguration()
    Private dsJournalDetails As New DataSet
    Private prevRowIndex As Integer = 0
    Private isFirstLoad As Boolean = True
    Private cb As ComboBox
    Private previtem As Integer
    Private txtAccount As New DataGridViewTextBoxEditingControl
    'variable for saving listenties value
    Dim djournalNo As String
    Dim dAmount As Decimal
    Dim dtTransact As Date
    Dim dBankName As String
    Dim dKeyID As String
    Dim dBankCode As String
    Dim dPayee As String
    Dim dMaker As String
    Dim dCheckNumber As String
    Private jvKeyID As String
    Private BankName As String
    Dim Total As Double
    Dim Test As Double
    Dim dtpcheckdate As New DateTimePicker
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim Account As String
    Dim SaveInButton As Boolean
    Dim IDKey As String
    Dim BankID As String
    'For Withdrawal
    Dim img As Image
    Dim PicData2 As Byte()
    Dim SigData2 As Byte()
    Dim Picdata As Byte()
    Dim SigData As Byte()
    Dim AccountReference As String
    'for RECEIPTS
    Dim ORcode As String
    Dim ORAccount As String
    Dim ORacntId As Guid
    Dim xLoad As Boolean

    Public xX As String
    Public xY As String
    Public xCtr As Integer = 0

    Public keyID As String
    Public EntryNo As String
    Public isUpdate As Boolean = False
    Dim CheckID As String

    Private flag_cell_edited As Boolean = False
    Private currentRow As Integer
    Private currentColumn As Integer
    Public keyDetails As String
    Public txtNameID As String
    Public selRow As New DataGridViewRow
    Private KeyListEntries As String

    Dim ORtotalAmount As Double
    Dim totalDebit As Double

    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

#Region "Events "

    Private Sub frmGeneralJournalEntries_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F1
                frmSelectCheck.Show()
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub frmGeneralJournalEntries_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Select Case e.Handled
            Case Keys.F1
                frmSelectCheck.Show()
        End Select
    End Sub

    Private Sub frmGeneralJournalEntries_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        chkPosted.Visible = UserModule(70)
        chkCancelled.Visible = UserModule(70)
        LinkLabel1.LinkBehavior = LinkBehavior.NeverUnderline
        LinkLabel2.LinkBehavior = LinkBehavior.NeverUnderline
        LinkLabel4.LinkBehavior = LinkBehavior.NeverUnderline
        DeleteDocToolStripMenuItem.Visible = True
        Try
            dtFrom.Value = SetAsFirstDayOfTheMonth()
            'grdListofEntries.DataSource = LoadGeneralJournalEntries(dtFrom.Value.Date, dtTo.Value.Date).Tables(0)
            'Call FormatGeneralJournalEntriesGrid()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Call LoadDoctype()
        Call TransactionForm()
        txtGeneralJournalNo.Text = ""
        Call LoadDocNum()
        isUpdate = False
        SaveInButton = False
        Call DefaultAccount_Search()
        NoneToolStripMenuItem.Text = ORAccount

    End Sub

    Private Sub VisibleGridList()
        Dim docType As String = cboDoctype.Text

        Select Case docType
            Case "RECEIPTS"
                grdListofEntries.Visible = True
                Call LoadDocNum()
            Case "DEPOSIT SLIP"
                grdListofEntries.Visible = True
                Call LoadDocNum()
            Case "CHECK VOUCHER"
                grdListofEntries.Visible = True
                Call LoadDocNum()
            Case "CASH VOUCHER"
                grdListofEntries.Visible = False
                Call LoadDocNum()
            Case "JOURNAL VOUCHER"
                grdListofEntries.Visible = False
                Call LoadDocNum()
            Case "WITHDRAWAL SLIP"
                grdListofEntries.Visible = True
                Call LoadDocNum()
            Case Else
                grdListofEntries.Visible = False
                Call LoadDocNum()
        End Select
    End Sub
    Public Sub CLearvalue()
        txtCode.Clear()
        txtclientname.Clear()
        txtAvailable.Text = "0.00"
        txtCurrent.Text = "0.00"
        PBsign.Image = My.Resources.signature
        PBimage.Image = My.Resources.photo
        grdGenJournalDetails.Columns.Clear()
        grdListofEntries.Columns.Clear()
    End Sub
    Public Sub ClearWithdrawalInOtherForm()
        txtCode.Clear()
        txtclientname.Clear()
        txtAvailable.Text = "0.00"
        txtCurrent.Text = "0.00"
        PBsign.Image = My.Resources.signature
        PBimage.Image = My.Resources.photo
    End Sub

    Private Sub TransactionForm()
        Dim docType As String = cboDoctype.Text

        Select Case docType
            Case "RECEIPTS"
                lblCollector.Visible = True
                txtCollector.Visible = True
                'LinkLabel1.Visible = True
                'grdListofEntries.Visible = True
                AddCheckToolStripMenuItem.Visible = True
                AddBankToolStripMenuItem.Visible = True
                CheckDepositToolStripMenuItem.Visible = True
                DeleteCheckToolStripMenuItem.Visible = True
                ViewCheckToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                LinkLabel1.Visible = False
                LinkLabel3.Visible = False
                LinkLabel4.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                'grdListofEntries.Visible = False
                PrintVoucherToolStripMenuItem.Text = "Print OR (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                'ListofEntriesReadOnly()
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "DEPOSIT SLIP"
                lblCollector.Visible = True
                txtCollector.Visible = True
                'grdListofEntries.Visible = True
                AddBankToolStripMenuItem.Visible = True
                AddCheckToolStripMenuItem.Visible = True
                CheckDepositToolStripMenuItem.Visible = True
                DeleteCheckToolStripMenuItem.Visible = True
                ViewCheckToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                LinkLabel1.Visible = False
                LinkLabel3.Visible = False
                LinkLabel4.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                'grdListofEntries.Visible = False
                PrintVoucherToolStripMenuItem.Text = "Print Deposit (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "CHECK VOUCHER"
                lblCollector.Visible = False
                txtCollector.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                LinkLabel4.Visible = True
                AddCheckToolStripMenuItem.Visible = True
                CheckDepositToolStripMenuItem.Visible = True
                DeleteCheckToolStripMenuItem.Visible = True
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                PrintCheckToolStripMenuItem.Visible = True
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                grdListofEntries.Enabled = True
                'grdListofEntries.Columns.Clear()
                'grdGenJournalDetails.Columns.Clear()
                PrintVoucherToolStripMenuItem.Text = "Print CDV (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "CASH VOUCHER"
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                LinkLabel3.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                LinkLabel4.Visible = True
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                'grdListofEntries.Visible = False
                PrintVoucherToolStripMenuItem.Text = "Print CV (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "JOURNAL VOUCHER"
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                LinkLabel4.Visible = True
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                PrintVoucherToolStripMenuItem.Text = "Print JV (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "WITHDRAWAL SLIP"
                ShowSiToolStripMenuItem.Visible = True
                ViewImageToolStripMenuItem.Visible = True
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = True
                AddCheckToolStripMenuItem.Visible = True
                CheckDepositToolStripMenuItem.Visible = True
                DeleteCheckToolStripMenuItem.Visible = True
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                'grdListofEntries.Visible = False
                LinkLabel1.Visible = False
                LinkLabel3.Visible = True
                LinkLabel4.Visible = False
                PBimage.Visible = True
                PBsign.Visible = True
                lblCode.Visible = True
                txtCode.Visible = True
                lblclientname.Visible = True
                txtclientname.Visible = True
                lblAvailable.Visible = True
                txtAvailable.Visible = True
                lblCurrent.Visible = True
                txtCurrent.Visible = True
                PrintVoucherToolStripMenuItem.Text = "Print Withdrawal (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "SALES INVOICE"
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                LinkLabel4.Visible = True
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                PrintVoucherToolStripMenuItem.Text = "Print Invoice (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "ACCOUNTS PAYABLE VOUCHER"
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = True
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                LinkLabel4.Visible = True
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                PrintVoucherToolStripMenuItem.Text = "Print APV (F12)"
                PrintVoucherToolStripMenuItem.Visible = True
                Label4.Text = "Difference"
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = True
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "CREDIT MEMO"
                lblCollector.Visible = False
                txtCollector.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                PrintVoucherToolStripMenuItem.Visible = False
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = True
                NoneToolStripMenuItem.Visible = False
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                LinkLabel4.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = False
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case "DEBIT MEMO"
                lblCollector.Visible = False
                txtCollector.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                PrintVoucherToolStripMenuItem.Visible = False
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = False
                NoneToolStripMenuItem.Visible = False
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                LinkLabel4.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                LinkLabel1.Visible = True
                LinkLabel3.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = False
                lblsoa.Visible = False
                txtsoa.Visible = False
            Case Else
                lblCollector.Visible = False
                txtCollector.Visible = False
                ShowSiToolStripMenuItem.Visible = False
                ViewImageToolStripMenuItem.Visible = False
                ViewCheckToolStripMenuItem.Visible = False
                ViewVoucherF3ToolStripMenuItem.Visible = False
                PrintCheckToolStripMenuItem.Visible = False
                PrintVoucherToolStripMenuItem.Visible = False
                AddCheckToolStripMenuItem.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                DeleteCheckToolStripMenuItem.Visible = False
                DefaultAccountToolStripMenuItem.Visible = False
                NoneToolStripMenuItem.Visible = False
                PBimage.Visible = False
                PBsign.Visible = False
                lblCode.Visible = False
                txtCode.Visible = False
                lblclientname.Visible = False
                txtclientname.Visible = False
                lblAvailable.Visible = False
                LinkLabel4.Visible = False
                txtAvailable.Visible = False
                lblCurrent.Visible = False
                txtCurrent.Visible = False
                LinkLabel1.Visible = False
                LinkLabel3.Visible = False
                CheckDepositToolStripMenuItem.Visible = False
                chkPosted.Checked = False
                DeleteDocToolStripMenuItem.Enabled = False
                lblsoa.Visible = False
                txtsoa.Visible = False
        End Select
        bLoanForRelease = False
    End Sub
    Private Sub btnDisplay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Try
            Call FormatGeneralJournalEntriesGrid()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bgwJournalEntries_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwJournalEntries.DoWork
        'Periodically checks if process has been cancelled. Exit if Cancelled
        If bgwJournalEntries.CancellationPending Then
            Thread.Sleep(1000)

            e.Cancel = True
            Exit Sub
        End If

        Try
            'Dim jvKeyId As String = keyID
            dsJournalDetails = LoadGeneralJournalEntries_Details(keyID)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bgwJournalEntries_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwJournalEntries.RunWorkerCompleted
        Try
            picLoading.Visible = False
            grdGenJournalDetails.DataSource() = dsJournalDetails.Tables(0)
            Call FormatGeneralJournalDetailsGrid()
            Call ComputeDebitAndCreditSumDetails()

            Call GenerateKeyIDForDetailEntry(grdGenJournalDetails.Rows.GetLastRow(DataGridViewElementStates.None))

            'Control for New rows added
            isFirstLoad = False

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmRecordedEntries.ShowDialog()
    End Sub

    Public Sub LoadEntryDetails()
        Try
            grdGenJournalDetails.DataSource = Nothing
            grdGenJournalDetails.Rows.Clear()
            Dim mycon As New Clsappconfiguration
            Dim co_id As New Guid(gCompanyID())
            'Dim ds As DataSet
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey1",
                                                                            New SqlParameter("@JVKeyID", keyID), _
                                                                            New SqlParameter("@co_id", co_id))
            Try
                Dim row As Integer
                If rd.HasRows = True Then
                    While rd.Read
                        row = grdGenJournalDetails.RowCount - 1
                        txtGeneralJournalNo.Text = rd.Item("EntryNo").ToString
                        dteGeneralJournal.Value = rd.Item("fdTransDate").ToString
                        chkPosted.Checked = rd.Item("fdPosted").ToString
                        chkCancelled.Checked = rd.Item("fbCancelled").ToString
                        txtCreatedBy.Text = rd.Item("fuCreatedBy").ToString
                        txtMemo.Text = rd.Item("cMemo").ToString
                        cboDoctype.Text = rd.Item("fcDocName").ToString

                        txtsoa.Text = rd.Item("fcSoaNo").ToString

                        '-------for withdrawal-------'
                        'txtCode.Text = rd.Item("cID").ToString
                        'txtclientname.Text = rd.Item("cName").ToString
                        GetJVKeyID() = rd.Item("fxKeyJVNo").ToString
                        dtpDatePrepared.Value = rd.Item("fdDateCreated").ToString

                        AddDetailItem(rd.Item("cID").ToString, rd.Item("cName").ToString, rd.Item("cLoanRef").ToString, rd.Item("cAccountRef").ToString,
                               rd.Item("fcSoaNo").ToString, rd.Item("cAccounts").ToString, rd.Item("cAccountCode").ToString, rd.Item("cAccountTitle").ToString,
                                rd.Item("Debit"), rd.Item("Credit"), rd.Item("cMemID").ToString, rd.Item("fxKey").ToString)


                    End While
                    rd.Close()
                    isUpdate = True
                    txtCode.Text = grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()
                    txtclientname.Text = grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()
                Else
                    Dim rdH As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey1_HeaderOnly",
                                                                                    New SqlParameter("@JVKeyID", keyID))
                    Try
                        Dim rowH As Integer
                        While rdH.Read
                            rowH = grdGenJournalDetails.RowCount - 1
                            txtGeneralJournalNo.Text = rdH.Item("EntryNo").ToString
                            dteGeneralJournal.Value = rdH.Item("fdTransDate").ToString
                            chkPosted.Checked = rdH.Item("fdPosted").ToString
                            chkCancelled.Checked = rdH.Item("fbCancelled").ToString
                            txtCreatedBy.Text = rdH.Item("fuCreatedBy").ToString
                            txtMemo.Text = rdH.Item("cMemo").ToString
                            cboDoctype.Text = rdH.Item("fcDocName").ToString
                            GetJVKeyID() = rdH.Item("fxKeyJVNo").ToString
                        End While
                        txtCode.Clear()
                        txtclientname.Clear()
                        txtAvailable.Text = "0.00"
                        txtCurrent.Text = "0.00"
                        PBsign.Image = My.Resources.signature
                        PBimage.Image = My.Resources.photo
                        rdH.Close()
                    Catch
                        MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End Try
                    '---CHECK STATUS---'
                    'lblNotif.Visible = False
                    'Call CheckStatus()
                    'grdGenJournalDetails.Enabled = True
                    'grdListofEntries.Enabled = True
                    'txtMemo.ReadOnly = False

                    dteGeneralJournal.Enabled = True
                    btnSelectDocNum.Enabled = True
                    DeleteDocToolStripMenuItem.Enabled = True
                    mycon.sqlconn.Close()
                    isUpdate = True
                    Call GenerateKeyIDForDetailEntry(0)
                End If
                Call ComputeDebitAndCreditSumDetails()
            Catch
                'MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
            grdGenJournalDetails.Rows(0).Cells(0).Selected = False
            lblNotif.Visible = False
            Call CheckStatus()
            mycon.sqlconn.Close()
            isUpdate = True

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub LoadEntryDetailsForLoanRelease()
        Try
            grdGenJournalDetails.DataSource = Nothing
            grdGenJournalDetails.Rows.Clear()

            Dim mycon As New Clsappconfiguration
            'Dim ds As DataSet

            Dim rd = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "AproveLoneTemplate",
                                                                New SqlParameter("@JVKeyID", keyID),
                                                                New SqlParameter("@coid", gCompanyID))

            Try
                Dim row As Integer
                If rd.HasRows = True Then
                    While rd.Read

                        row = grdGenJournalDetails.RowCount - 1
                        txtGeneralJournalNo.Text = rd.Item("EntryNo").ToString
                        dteGeneralJournal.Value = rd.Item("fdTransDate").ToString
                        chkPosted.Checked = rd.Item("fdPosted").ToString
                        chkCancelled.Checked = rd.Item("fbCancelled").ToString
                        txtCreatedBy.Text = rd.Item("fuCreatedBy").ToString
                        txtMemo.Text = rd.Item("cMemo").ToString
                        cboDoctype.Text = rd.Item("fcDocName").ToString
                        '-------for withdrawal-------'
                        txtCode.Text = rd.Item("cID").ToString
                        txtclientname.Text = rd.Item("cName").ToString
                        GetJVKeyID() = rd.Item("fxKeyJVNo").ToString

                        AddDetailItem(rd.Item("cID").ToString, rd.Item("cName").ToString, rd.Item("cLoanRef").ToString, rd.Item("cAccountRef").ToString,
                               txtsoa.Text, rd.Item("cAccounts").ToString, rd.Item("cAccountCode").ToString, rd.Item("cAccountTitle").ToString,
                                rd.Item("Debit"), rd.Item("Credit"), rd.Item("cMemID").ToString, rd.Item("fxKey").ToString)

                    End While
                    rd.Close()
                    isUpdate = True
                Else
                    Dim rdH As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "AproveLoneTemplate_HeaderOnly",
                                                                                    New SqlParameter("@JVKeyID", keyID))
                    Try
                        Dim rowH As Integer
                        While rdH.Read
                            rowH = grdGenJournalDetails.RowCount - 1
                            txtGeneralJournalNo.Text = rdH.Item("EntryNo").ToString
                            dteGeneralJournal.Value = rdH.Item("fdTransDate").ToString
                            chkPosted.Checked = rdH.Item("fdPosted").ToString
                            chkCancelled.Checked = rdH.Item("fbCancelled").ToString
                            txtCreatedBy.Text = rdH.Item("fuCreatedBy").ToString
                            txtMemo.Text = rdH.Item("cMemo").ToString
                            cboDoctype.Text = rdH.Item("fcDocName").ToString
                            GetJVKeyID() = rdH.Item("fxKeyJVNo").ToString
                        End While
                        rdH.Close()
                    Catch
                        MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End Try
                    '---CHECK STATUS---'
                    lblNotif.Visible = False
                    Call CheckStatus()
                    'grdGenJournalDetails.Enabled = True
                    'grdListofEntries.Enabled = True
                    'txtMemo.ReadOnly = False
                    dteGeneralJournal.Enabled = True
                    btnSelectDocNum.Enabled = True
                    DeleteDocToolStripMenuItem.Enabled = True

                    mycon.sqlconn.Close()
                    isUpdate = True
                    Call GenerateKeyIDForDetailEntry(0)
                End If
                Call ComputeDebitAndCreditSumDetails()
            Catch
                'MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
            grdGenJournalDetails.Rows(0).Cells(0).Selected = False
            lblNotif.Visible = False
            Call CheckStatus()
            mycon.sqlconn.Close()
            isUpdate = True

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CheckStatus()
        If chkPosted.Checked = True And chkCancelled.Checked = False Then
            grdGenJournalDetails.Columns("Debit").ReadOnly = True
            grdGenJournalDetails.Columns("Credit").ReadOnly = True
            grdListofEntries.Enabled = False
            txtMemo.ReadOnly = True
            dteGeneralJournal.Enabled = False
            btnSelectDocNum.Enabled = False
            SaveToolStripMenuItem.Enabled = False
            dtpDatePrepared.Enabled = False
            chkCancelled.Enabled = False
        ElseIf chkPosted.Checked = False And chkCancelled.Checked = True Then
            grdGenJournalDetails.Columns("Debit").ReadOnly = True
            grdGenJournalDetails.Columns("Credit").ReadOnly = True
            grdListofEntries.Enabled = False
            txtMemo.ReadOnly = True
            dteGeneralJournal.Enabled = False
            btnSelectDocNum.Enabled = False
            SaveToolStripMenuItem.Enabled = False
            dtpDatePrepared.Enabled = False
            chkPosted.Checked = True
            chkPosted.Enabled = False
        Else
            Call MakeItEnabled()
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.cboDoctype.Text = "" Then
            MessageBox.Show("Please Select Document Type", "NOTIFICATION", MessageBoxButtons.OK)
        Else
            lblPostingNotification.Visible = False
            grdGenJournalDetails.Enabled = True
            txtTotalDebit.Clear()
            txtTotalCredit.Clear()
            txttotal.Clear()
            Call ResetGridToNew()
            Call FormatGeneralJournalEntriesGrid()
            Call FormatGeneralJournalDetailsGrid()
            Call LoadDocNum()
            btnSelectDocNum.Enabled = True
            txtMemo.ReadOnly = False
            txtMemo.Text = ""
        End If

    End Sub

    Private Sub grdGenJournalDetails_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellEndEdit
        flag_cell_edited = True
        currentColumn = e.ColumnIndex
        currentRow = e.RowIndex
    End Sub

    Private Sub grdGenJournalDetails_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellLeave
        If Not cb Is Nothing Then
            cb.SelectedIndex = cb.FindStringExact(cb.Text)
        End If
    End Sub

    Private Sub grdGenJournalDetails_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellClick
        Dim i As Decimal

        Try
            If e.ColumnIndex <> -1 Then
                If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "Credit" Then
                    If NormalizeValuesInDataGridView(grdGenJournalDetails, "Credit", e.RowIndex) = "" Then
                        grdGenJournalDetails.Item("Credit", e.RowIndex).Value = i
                        grdGenJournalDetails.Item("Credit", e.RowIndex).Style.SelectionBackColor = Color.DodgerBlue
                        grdGenJournalDetails.Item("Credit", e.RowIndex).Style.SelectionForeColor = Color.Black
                    End If
                End If

                If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "Debit" Then
                    If NormalizeValuesInDataGridView(grdGenJournalDetails, "Debit", e.RowIndex) = "" Then
                        grdGenJournalDetails.Item("Debit", e.RowIndex).Value = i
                        grdGenJournalDetails.Item("Debit", e.RowIndex).Style.SelectionBackColor = Color.DodgerBlue
                        grdGenJournalDetails.Item("Debit", e.RowIndex).Style.SelectionForeColor = Color.Black
                    End If
                End If
            End If
        Catch ex As Exception

        End Try

        Try
            IDKey = grdGenJournalDetails.Item("cAccounts", e.RowIndex).Value.ToString
        Catch ex As Exception

        End Try

        Try
            keyDetails = grdGenJournalDetails.Item("fxKey", e.RowIndex).Value.ToString
        Catch ex As Exception

        End Try


    End Sub

#Region "Ito ang bagong update,,April 15 2014 5:22"

    Private Sub grdGenJournalDetails_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellValueChanged
        If e.ColumnIndex <> -1 Then

            If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "Debit" Then
                If NormalizeValuesInDataGridView(grdGenJournalDetails, "Debit", e.RowIndex) = "" Then
                    grdGenJournalDetails.Item("Debit", e.RowIndex).Value = 0
                Else
                    'If isUpdate = True Then
                    '    'grdGenJournalDetails.Columns("Debit").DefaultCellStyle.Format = "N2"
                    '    grdGenJournalDetails.Item("Debit", e.RowIndex).Value = Format(CDec(grdGenJournalDetails.Item("Debit", e.RowIndex).Value.ToString()), "##,##0.00")
                    'Else

                    grdGenJournalDetails.Item("Debit", e.RowIndex).Value = Format(Abs(CDec(grdGenJournalDetails.Item("Debit", e.RowIndex).Value.ToString())), "##,##0.00")
                    'End If
                End If
            End If

            If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "Credit" Then
                If NormalizeValuesInDataGridView(grdGenJournalDetails, "Credit", e.RowIndex) = "" Then
                    grdGenJournalDetails.Item("Credit", e.RowIndex).Value = 0
                Else
                    'If isUpdate = True Then
                    '    grdGenJournalDetails.Columns("Credit").DefaultCellStyle.Format = "N2"
                    'Else
                    grdGenJournalDetails.Item("Credit", e.RowIndex).Value = Format(Abs(CDec(grdGenJournalDetails.Item("Credit", e.RowIndex).Value.ToString())), "##,##0.00")
                    'End If
                End If
            End If

        End If

        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "cAccounts" Then
            Try
                Dim accountID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccounts", e.RowIndex)
                Call IsAccountValid(accountID)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                grdGenJournalDetails.Item("cAccounts", e.RowIndex).Value = System.DBNull.Value
            End Try
        End If

        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "Credit" Then
            Try
                Call ComputeDebitAndCreditSumDetails()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

        If grdGenJournalDetails.Columns(e.ColumnIndex).Name = "Debit" Then
            Try
                Call ComputeDebitAndCreditSumDetails()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

#End Region

    Private Sub grdGenJournalDetails_UserAddedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdGenJournalDetails.UserAddedRow
        Call GenerateKeyIDForDetailEntry(e.Row.Index)
    End Sub

#End Region

#Region "Subroutines and other Functions "
    Public Sub RecordGeneralEntry()
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If
            Dim companyId As String = gCompanyID()
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim transDate As String = dteGeneralJournal.Value.Date
            Dim memo As String = txtMemo.Text
            Dim totalAmount As String = txtTotalCredit.Text
            Dim user As String = frmMain.currentUser.Text
            Dim subsidiaryID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cMemID", 0)
            Dim doctype As String = Me.cboDoctype.SelectedValue.ToString
            Dim isCancelled As Boolean = False
            'Dim SoaNo As String = txtsoa.Text
            Dim SoaNo As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", 0)

            'SAVE ENTRY HEADER/DETAILS
            Call SaveCurrentJournalEntry_Header(GetJVKeyID(), companyId, journalNo, transDate, totalAmount, user, doctype, isCancelled, SoaNo)
            Call SaveCurrentJournalEntry_Details()

        Catch ex As ArgumentOutOfRangeException
            MessageBox.Show("User Error! There is nothing to save.", "Transaction Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PrepareForANewGeneralJournal()
        Call ResetGridToNew()
        Call FormatGeneralJournalEntriesGrid()
        'Call FormatGeneralJournalDetailsGrid()
        Call FormatGeneralJournalDetailsGrid_OR()
        Call GenerateKeyIDForDetailEntry(0)
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function

    Private Sub SaveCurrentJournalEntry_Header(ByVal jvKeyID As String, ByVal companyID As String, ByVal journalNo As String, ByVal transdate As Date, _
    ByVal total As Decimal, ByVal user As String, ByVal doctype As String, ByVal isCancelled As String, ByVal SOA As String)

        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dteGeneralJournal.Value.Date, doctype)
        Dim guid As New Guid(jvKeyID)
        Dim co_id As New Guid(companyID)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", co_id),
                    New SqlParameter("@fdTransDate", transdate),
                    New SqlParameter("@fdTotAmt", total),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", doctype),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", journalNo),
                    New SqlParameter("@fbIsCancelled", isCancelled),
                    New SqlParameter("@fbIsPosted", 0),
                    New SqlParameter("@fdDatePrepared", dtpDatePrepared.Text),
                    New SqlParameter("@SoaNo", SOA))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtGeneralJournalNo.Text),
                    New SqlParameter("@fcDoctype", cboDoctype.Text),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateDocDate_SOA(ByVal Soa As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update_for_SOA", _
                    New SqlParameter("@fcDocNumber", Soa),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpdateDocDate_ToNull_SOA(ByVal Soa As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update_ToNull_for_SOA", _
                    New SqlParameter("@fcDocNumber", Soa),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub UpdateAprovedLoan()
    '    Try
    '        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "AprovedLoan_Delete", _
    '                New SqlParameter("@fk_tJVEntry", GetJVKeyID()),
    '                New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text))
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Public Function NormalizeValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        Try
            If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
                If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                    fieldName = grid.Item(columnName, rowIndex).Value.ToString()

                End If
            End If
        Catch ex As Exception

        End Try
        Return fieldName

    End Function

    Private Sub SaveCurrentJournalEntry_Details()
        Try
            For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
                Dim journalNo As String = txtGeneralJournalNo.Text
                'Dim sAccount As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccounts", 0)
                Dim sAccount As String = IIf(IsDBNull(grdGenJournalDetails.Item("cAccounts", xRow).Value), Nothing, grdGenJournalDetails.Item("cAccounts", xRow).Value.ToString())
                Dim jvDetailRecordID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "fxKey", xRow)
                Dim dDebit As Decimal = grdGenJournalDetails.Item("Debit", xRow).Value
                Dim dCredit As Decimal = grdGenJournalDetails.Item("Credit", xRow).Value
                'Dim sMemo As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cMemo", xRow)
                Dim sMemo As String = txtMemo.Text
                'Dim sNote As String = ""
                'Dim sNameID As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cMemID", xRow)
                Dim sNameID As String = grdGenJournalDetails.Item("cMemID", xRow).Value
                Dim pkJournalID As String = GetJVKeyID()
                Dim dtTransact As Date = dteGeneralJournal.Value.Date
                Dim LoanRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cLoanRef", xRow)
                Dim AccountRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow)
                Dim SoaNo As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", xRow)

                Account = sAccount
                'If isUpdate = True Then
                '    'jvDetailRecordID = grdGenJournalDetails.Item("fxKey", xRow).Value.ToString()
                '    jvDetailRecordID = IIf(IsDBNull(grdGenJournalDetails.Item("fxKey", xRow).Value), Nothing, grdGenJournalDetails.Item("fxKey", xRow).Value.ToString())
                '    If jvDetailRecordID <> "" Then
                '        jvDetailRecordID = grdGenJournalDetails.Item("fxKey", xRow).Value.ToString()
                '    Else
                '        jvDetailRecordID = System.Guid.NewGuid.ToString()
                '    End If
                'Else
                '    jvDetailRecordID = NormalizeValuesInDataGridView(grdGenJournalDetails, "fxKey", 0)
                If jvDetailRecordID = "" Then
                    '    jvDetailRecordID = grdGenJournalDetails.Item("fxKey", xRow).Value.ToString()
                    'Else
                    jvDetailRecordID = System.Guid.NewGuid.ToString()
                End If
                'End If

                If sNameID <> "" Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save", _
                                      New SqlParameter("@fiEntryNo", journalNo), _
                                      New SqlParameter("@fxKeyAccount", sAccount), _
                                      New SqlParameter("@fdDebit", dDebit), _
                                      New SqlParameter("@fdCredit", dCredit), _
                                      New SqlParameter("@fcMemo", sMemo), _
                                      New SqlParameter("@fxKeyNameID", sNameID), _
                                      New SqlParameter("@fxKey", jvDetailRecordID), _
                                      New SqlParameter("@fk_JVHeader", GetJVKeyID()), _
                                      New SqlParameter("@transDate", dtTransact), _
                                      New SqlParameter("@fcLoanRef", LoanRef), _
                                      New SqlParameter("@fcAccRef", AccountRef), _
                                      New SqlParameter("@SoaNo", SoaNo))
                    gCon.sqlconn.Close()
                Else
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                      New SqlParameter("@fiEntryNo", journalNo), _
                                      New SqlParameter("@fxKeyAccount", sAccount), _
                                      New SqlParameter("@fdDebit", dDebit), _
                                      New SqlParameter("@fdCredit", dCredit), _
                                      New SqlParameter("@fcMemo", sMemo), _
                                      New SqlParameter("@fxKey", jvDetailRecordID), _
                                      New SqlParameter("@fk_JVHeader", GetJVKeyID()), _
                                      New SqlParameter("@transDate", dtTransact), _
                                      New SqlParameter("@fcLoanRef", LoanRef), _
                                      New SqlParameter("@fcAccRef", AccountRef))
                    gCon.sqlconn.Close()
                End If
                Call UpdateDocDate_SOA(SoaNo)
            Next

            Call UpdateDocDate()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Public Property GetBankName() As String
        Get
            Return BankName
        End Get
        Set(ByVal value As String)
            BankName = value
        End Set
    End Property

    Private Sub UpdateCheckMaster()
        Try
            Dim row As Integer = grdListofEntries.RowCount - 2
            For xRow As Integer = 0 To grdListofEntries.Rows.Count - 1
                If row = xRow Then
                    Dim Key As String = grdListofEntries.Item("KeyID", row).Value
                    If Key IsNot Nothing Then
                        Dim Check As String
                        Dim checkBank As String
                        Dim rd As SqlDataReader

                        Dim CheckNum As String = NormalizeValuesInDataGridView(grdListofEntries, "CheckNo", xRow)
                        Dim BankID As String = NormalizeValuesInDataGridView(grdListofEntries, "Bank", xRow)
                        Dim IsUsed As Boolean = True
                        Dim Payee As String = NormalizeValuesInDataGridView(grdListofEntries, "Payee", xRow)
                        Dim Amount As String = NormalizeValuesInDataGridView(grdListofEntries, "checkAmount", xRow)
                        Dim isCancelled As Boolean = CancelledState()
                        Dim BankCode As String = NormalizeValuesInDataGridView(grdListofEntries, "BankCode", xRow)
                        Dim CheckKeyID As String = Key
                        Dim CheckDate As Date = NormalizeValuesInDataGridView(grdListofEntries, "CheckDate", xRow)
                        Dim Maker As String = NormalizeValuesInDataGridView(grdListofEntries, "Maker", xRow)

                        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "Check_SearchCheck",
                                                       New SqlParameter("@FX_CheckNumber", CheckNum), _
                                                       New SqlParameter("@fcBankName", BankID))
                        With rd.Read
                            Check = rd.Item("FX_CheckNumber")
                            checkBank = rd.Item("PK_BankID").ToString
                        End With

                        BankID = checkBank

                        If CheckNum = Check And checkBank = BankID Then
                            CheckNumberUpdate(CheckNum, BankID, IsUsed, Payee, Amount, isCancelled, CheckDate, Maker)
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Function CancelledState() As Boolean
        If chkCancelled.Checked = True Then
            Return True
        Else
            Return False
        End If
    End Function

#Region "Old UpdateCheck Number"
    'Dim Check As String
    'Dim checkBank As String
    'Dim rd As SqlDataReader
    'Dim BankName As String

    'If GetBankName <> 0 Then
    '    BankName = GetBankName
    'Else
    '    BankName = Guid.NewGuid.ToString
    'End If

    'Dim CheckNum As String = NormalizeValuesInDataGridView(grdListofEntries, "CheckNo", 0)
    'Dim BankID As String = NormalizeValuesInDataGridView(grdListofEntries, "Bank", 0)
    'Dim IsUsed As Boolean = False
    'Dim Payee As String = NormalizeValuesInDataGridView(grdListofEntries, "Payee", 0)
    'Dim Amount As String = NormalizeValuesInDataGridView(grdListofEntries, "Amounts", 0)
    'Dim isCancelled As Boolean = False

    'Dim CheckDAte As String = NormalizeValuesInDataGridView(grdListofEntries, "CheckDate", 0)
    'Dim Maker As String = NormalizeValuesInDataGridView(grdListofEntries, "Maker", 0)

    ''Dim bID As New Guid(BankID)

    'rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "Check_SearchCheck",
    '                               New SqlParameter("@FX_CheckNumber", CheckNum))
    'With rd.Read
    '    Check = rd("FX_CheckNumber")
    'End With

    'If CheckNum = Check Then
    '    CheckSearch(CheckNum, BankID, txtGeneralJournalNo.Text, IsUsed, Payee, Amount, isCancelled, CheckDAte, Maker)

    '    MsgBox("Data Updated", vbInformation, "Updated Success")

    'Else
    '    MsgBox("No Data to Updated", vbInformation, "Updated Success")
    'End If
#End Region

#Region "Old CheckNumberList"
    'Private Sub CheckNumberInsert()
    '    Dim lastrow As Integer = grdGenJournalDetails.Rows.Count - 1
    '    For xRow As Integer = 0 To grdListofEntries.RowCount - 1
    '        Dim journalNo As String = txtGeneralJournalNo.Text
    '        Dim sAccount As String = IIf(IsDBNull(grdGenJournalDetails.Item("cAccounts", lastrow).Value), Nothing, grdGenJournalDetails.Item("cAccounts", lastrow).Value.ToString())
    '        Dim dCredit As Decimal = grdGenJournalDetails.Item("Credit", lastrow).Value
    '        Dim pkJournalID As String = GetJVKeyID()
    '        Dim dtTransact As Date = dteGeneralJournal.Value.Date
    '        Dim CheckNumber As String = grdListofEntries.Item("CheckNo", xRow).Value

    '        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Save", _
    '                                          New SqlParameter("@fxKeyJVNo", GetJVKeyID), _
    '                                          New SqlParameter("@fiEntryNo", journalNo), _
    '                                          New SqlParameter("@fxKeyAccount", sAccount), _
    '                                          New SqlParameter("@fdCredit", dCredit), _
    '                                          New SqlParameter("@transDate", dtTransact), _
    '                                          New SqlParameter("@fcCheckNo", CheckNumber))

    '    Next
    'End Sub
#End Region
    Private Sub CheckNumberUpdate(ByVal CheckNum As String, ByVal BankId As String,
                                ByVal isUsed As Boolean, ByVal Payee As String, ByVal Amount As Double,
                                ByVal isCancel As Boolean, ByVal checkDate As Date, ByVal Maker As String)
        Try
            Dim myguid As New Guid(BankId)
            Dim guids As New Guid(GetJVKeyID)
            SqlHelper.ExecuteNonQuery(gCon.cnstring, "Check_Update", _
                          New SqlParameter("@FX_CheckNumber", CheckNum), _
                          New SqlParameter("@FK_BankId", myguid), _
                          New SqlParameter("@FK_TransId", guids), _
                          New SqlParameter("@fbIsUsed", isUsed), _
                          New SqlParameter("@fcPayee", Payee), _
                          New SqlParameter("@fnAmount", Amount), _
                          New SqlParameter("@fbIsCancelled", isCancel), _
                          New SqlParameter("@fdCheckDate", checkDate), _
                          New SqlParameter("@fcMaker", Maker))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub CheckSearch(ByVal CheckNum As String, ByVal BankID As String, ByVal Trans As String, ByVal IsUsed As Boolean, ByVal Payee As String,
                           ByVal Amount As Double, ByVal isCancelled As Boolean, ByVal CheckDAte As String, ByVal Maker As String)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Update", _
                  New SqlParameter("@FX_CheckNumber", CheckNum),
                  New SqlParameter("@FK_BankId", BankID),
                  New SqlParameter("@FK_TransId", Trans),
                  New SqlParameter("@fbIsUsed", IsUsed),
                  New SqlParameter("@fcPayee", Payee),
                  New SqlParameter("@fnAmount", Amount),
                  New SqlParameter("@fbIsCancelled", isCancelled),
                  New SqlParameter("@fdCheckDate", CheckDAte),
                  New SqlParameter("@fcMaker", Maker))
    End Sub

    Private Sub DeleteAllDetailedEntriesPerJV(ByVal jvKeyID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVENtry_detailedAllDelete_PerJV", _
                    New SqlParameter("@pk_JVEntryID", jvKeyID))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ResetGridToNew()
        'Set A new key for the New Entry

        GetJVKeyID() = System.Guid.NewGuid.ToString()


        txtGeneralJournalNo.Text = ""
        dteGeneralJournal.Value = Date.Now.Date
        dteGeneralJournal.Enabled = True
        dtpDatePrepared.Value = Date.Now.Date
        dtpDatePrepared.Enabled = True
        txtTotalDebit.Text = "0.00"
        txtTotalCredit.Text = "0.00"

        txtCreatedBy.Text = ""
        chkCancelled.Checked = False
        chkPosted.Checked = False
        chkPosted.Enabled = True
        chkCancelled.Enabled = True
        txttotal.Text = "0.00"
        txtMemo.Text = ""
        isUpdate = False
        btnSelectDocNum.Enabled = True
        RecurringEntryToolStripMenuItem.Enabled = True

        'Clears Databounded Grid
        grdListofEntries.DataSource = Nothing
        grdGenJournalDetails.DataSource = Nothing

        'Generate Random number
        'Call GenerateJournalNo_RandomMethod()

        'or Generate Serial Number
    End Sub

    Private Function SetAsFirstDayOfTheMonth() As Date
        'Monthly
        Dim thisDate As Date

        Dim dateNow As Date = Date.Now.Date
        thisDate = dateNow.AddDays(-dateNow.Day + 1)

        Return thisDate
    End Function

    Private Sub DisplayNotification(ByVal notification As String, ByVal displayNotification As Boolean)
        If displayNotification Then
            lblPostingNotification.Visible = True
            lblPostingNotification.Text = notification
        Else
            lblPostingNotification.Visible = False
        End If

    End Sub

    Private Function LoadGeneralJournalEntries(ByVal dtFrom As Date, ByVal dtTo As Date) As DataSet
        Dim ds As New DataSet
        Dim companyID As String = gCompanyID()
        Dim username As String = frmMain.toolStripUsername.Text
        Dim doctype As String = cboDoctype.Text

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getperdate_perusername", _
                    New SqlParameter("@coid", companyID), _
                    New SqlParameter("@dtFrom", dtFrom), _
                    New SqlParameter("@dtTo", dtTo), _
                    New SqlParameter("@username", username), _
                    New SqlParameter("@fcDocType", doctype))
        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Public Sub FormatGeneralJournalEntriesGrid()
        Dim KeyID As New DataGridViewTextBoxColumn
        Dim Bank As New DataGridViewTextBoxColumn
        Dim CheckNo As New DataGridViewTextBoxColumn
        Dim CheckDate As New griddatecontrol
        Dim checkAmount As New DataGridViewTextBoxColumn
        Dim Maker As New DataGridViewTextBoxColumn
        Dim Payee As New DataGridViewTextBoxColumn
        Dim BankCode As New DataGridViewTextBoxColumn
        Dim MemID As New DataGridViewTextBoxColumn
        Dim MemKey As New DataGridViewTextBoxColumn

        'Dim dtNames As DataTable = m_DisplayNamesJV().Tables(0)

        With KeyID
            .Name = "KeyID"
            .DataPropertyName = "KeyID"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Visible = False
        End With

        With Bank
            .HeaderText = "Bank"
            .Name = "Bank"
            .DataPropertyName = "Bank"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With CheckNo
            .HeaderText = "Check Number"
            .Name = "CheckNo"
            .DataPropertyName = "CheckNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With CheckDate
            .HeaderText = "Check Date"
            .Name = "CheckDate"
            .DataPropertyName = "CheckDate"
            .DefaultCellStyle.Format = "MMMM dd, yyyy"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With checkAmount
            .HeaderText = "Amount"
            .Name = "checkAmount"
            .DataPropertyName = "checkAmount"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .DefaultCellStyle.Format = "##,##0.00"
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With Maker
            .HeaderText = "Maker"
            .Name = "Maker"
            .DataPropertyName = "Maker"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With Payee
            .HeaderText = "Payee"
            .Name = "Payee"
            .DataPropertyName = "fxKeyNameID"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With BankCode
            .HeaderText = "BankCode"
            .Name = "BankCode"
            .Visible = False
        End With

        With MemID
            .HeaderText = "ID"
            .Name = "MemID"
            .Visible = False
        End With

        With MemKey
            .HeaderText = "Member Key"
            .Name = "MemKey"
            .Visible = False
        End With

        With grdListofEntries
            .Columns.Clear()
            .Columns.Add(KeyID)
            .Columns.Add(Bank)
            .Columns.Add(CheckNo)
            .Columns.Add(CheckDate)
            .Columns.Add(checkAmount)
            .Columns.Add(Maker)
            .Columns.Add(Payee)
            .Columns.Add(BankCode)
            .Columns.Add(MemID)
            .Columns.Add(MemKey)

            .Columns("Bank").Width = 150
            .Columns("CheckNo").Width = 200
            .Columns("CheckDate").Width = 150
            .Columns("checkAmount").Width = 100
            .Columns("checkAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Maker").Width = 200
            .Columns("Payee").Width = 200
        End With

    End Sub

    Public Sub LoadListofEntriesPosted()
        grdListofEntries.DataSource = Nothing
        grdListofEntries.Rows.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "ListPerCheckEntriesPerJV",
                                          New SqlParameter("@FX_CheckNumber", GetJVKeyID))
        Dim row As Integer
        While rd.Read
            row = grdListofEntries.RowCount - 1
            LoadItem(rd.Item("FK_AcntId").ToString, rd.Item("fcBankName").ToString, rd.Item("FX_CheckNumber").ToString,
                    rd.Item("fdCheckDate"), rd.Item("fnAmount"), rd.Item("fcMaker"), rd.Item("fcPayee"), rd.Item("acnt_code"))
        End While
        rd.Close()
        gCon.sqlconn.Close()
    End Sub

    Public Sub LoadListofEntries()
        grdListofEntries.DataSource = Nothing
        grdListofEntries.Rows.Clear()
        Dim rd As SqlDataReader
        Dim row As Integer
        'If keyID = "" Then
        '    MsgBox("Try Again!", vbInformation, "No Data has Found!")
        'Else

        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Search",
                                      New SqlParameter("@FX_CheckNumber", keyID))

        While rd.Read
            row = grdListofEntries.RowCount - 1
            For xRow As Integer = 0 To grdListofEntries.RowCount - 1
                If row = xRow Then
                    grdListofEntries.Rows.Add()
                    grdListofEntries.Item("Bank", row).Value = rd.Item("fcBankName")
                    grdListofEntries.Item("CheckNo", row).Value = rd.Item("FX_CheckNumber")
                    grdListofEntries.Item("CheckDate", row).Value = rd.Item("fdCheckDate")
                    grdListofEntries.Item("checkAmount", row).Value = rd.Item("fnAmount")
                    grdListofEntries.Item("Maker", row).Value = rd.Item("fcMaker")
                    grdListofEntries.Item("Payee", row).Value = rd.Item("fcPayee")
                End If
            Next
        End While
        rd.Close()
    End Sub

    Private Sub FormatGeneralJournalDetailsGrid()
        Dim fxKey As New DataGridViewTextBoxColumn
        Dim txtLoanRef As New DataGridViewTextBoxColumn
        Dim txtAccountRef As New DataGridViewTextBoxColumn
        Dim colAccountsID As New DataGridViewTextBoxColumn
        Dim colAccountCode As New DataGridViewTextBoxColumn
        Dim colAccounts As New DataGridViewTextBoxColumn
        Dim txtDebit As New DataGridViewTextBoxColumn
        Dim txtCredit As New DataGridViewTextBoxColumn
        Dim txtID As New DataGridViewTextBoxColumn
        Dim cboNames As New DataGridViewTextBoxColumn
        Dim txtMemberID As New DataGridViewTextBoxColumn

        With txtLoanRef
            .HeaderText = "Loan Ref."
            .Name = "cLoanRef"
            .DataPropertyName = "fcLoanReference"
            .SortMode = DataGridViewColumnSortMode.NotSortable
            .ReadOnly = True
        End With
        With txtAccountRef
            .HeaderText = "Account Ref."
            .Name = "cAccountRef"
            .DataPropertyName = "fxKeyAccountID"
            .SortMode = DataGridViewColumnSortMode.NotSortable
            .ReadOnly = True
        End With

        With colAccountsID
            .HeaderText = "Account ID"
            .DataPropertyName = "fxKeyAccount"
            .Name = "cAccounts"
            .Visible = False
        End With

        With colAccountCode
            .HeaderText = "Code"
            .Name = "cAccountCode"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtID
            .HeaderText = "ID No."
            .Name = "cID"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With colAccounts
            .HeaderText = "Account Title"
            .DataPropertyName = "fcAccountName"
            .Name = "cAccountTitle"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With cboNames
            .HeaderText = "Name"
            .DataPropertyName = "fxKeyNameID"
            .Name = "cName"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With fxKey
            .Name = "fxKey"
            .DataPropertyName = "fxKey"
            .Visible = False
        End With

        With txtDebit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "fdDebit"
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtCredit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "fdCredit"
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtMemberID
            .HeaderText = "MemberID"
            .DataPropertyName = "fxKeySupplier"
            .Name = "cMemID"
            .Visible = False
        End With

        With Me.grdGenJournalDetails
            .Columns.Clear()
            .Columns.Add(txtID)
            .Columns.Add(cboNames)
            .Columns.Add(txtLoanRef)
            .Columns.Add(txtAccountRef)
            .Columns.Add(colAccountsID)
            .Columns.Add(colAccountCode)
            .Columns.Add(colAccounts)
            .Columns.Add(txtDebit)
            .Columns.Add(txtCredit)
            .Columns.Add(txtMemberID)
            .Columns.Add(fxKey)

            .Columns("cID").Width = 60
            .Columns("cLoanRef").Width = 80
            .Columns("cAccountRef").Width = 80
            .Columns("cAccounts").Visible = False
            .Columns("cAccountCode").Width = 60
            .Columns("cAccountTitle").Width = 180
            .Columns("Debit").Width = 90
            .Columns("Credit").Width = 120
            .Columns("cName").Width = 190
            .Columns("fxKey").Visible = False
            .Columns("Debit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Credit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
        End With
    End Sub
    Private Sub FormatGeneralJournalDetailsGrid_OR()
        Dim fxKey As New DataGridViewTextBoxColumn
        Dim txtLoanRef As New DataGridViewTextBoxColumn
        Dim txtAccountRef As New DataGridViewTextBoxColumn
        Dim txtSoa_ID As New DataGridViewTextBoxColumn
        Dim colAccountsID As New DataGridViewTextBoxColumn
        Dim colAccountCode As New DataGridViewTextBoxColumn
        Dim colAccounts As New DataGridViewTextBoxColumn
        Dim txtDebit As New DataGridViewTextBoxColumn
        Dim txtCredit As New DataGridViewTextBoxColumn
        Dim txtID As New DataGridViewTextBoxColumn
        Dim cboNames As New DataGridViewTextBoxColumn
        Dim txtMemberID As New DataGridViewTextBoxColumn

        With txtLoanRef
            .HeaderText = "Loan Ref."
            .Name = "cLoanRef"
            .DataPropertyName = "fcLoanReference"
            .SortMode = DataGridViewColumnSortMode.NotSortable
            .ReadOnly = True
        End With
        With txtAccountRef
            .HeaderText = "Account Ref."
            .Name = "cAccountRef"
            .DataPropertyName = "fxKeyAccountID"
            .SortMode = DataGridViewColumnSortMode.NotSortable
            .ReadOnly = True
        End With

        With txtSoa_ID
            .HeaderText = "Invoice No."
            .Name = "cSOA"
            .DataPropertyName = "fcSoaNo"
            .SortMode = DataGridViewColumnSortMode.NotSortable
            .ReadOnly = True
        End With

        With colAccountsID
            .HeaderText = "Account ID"
            .DataPropertyName = "fxKeyAccount"
            .Name = "cAccounts"
            .Visible = False
        End With

        With colAccountCode
            .HeaderText = "Code"
            .Name = "cAccountCode"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtID
            .HeaderText = "ID No."
            .Name = "cID"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With colAccounts
            .HeaderText = "Account Title"
            .DataPropertyName = "fcAccountName"
            .Name = "cAccountTitle"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With cboNames
            .HeaderText = "Name"
            .DataPropertyName = "fxKeyNameID"
            .Name = "cName"
            .ReadOnly = True
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With fxKey
            .Name = "fxKey"
            .DataPropertyName = "fxKey"
            .Visible = False
        End With

        With txtDebit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "fdDebit"
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtCredit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "fdCredit"
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtMemberID
            .HeaderText = "MemberID"
            .DataPropertyName = "fxKeySupplier"
            .Name = "cMemID"
            .Visible = False
        End With

        With Me.grdGenJournalDetails
            .Columns.Clear()
            .Columns.Add(txtID)
            .Columns.Add(cboNames)
            .Columns.Add(txtLoanRef)
            .Columns.Add(txtAccountRef)
            .Columns.Add(txtSoa_ID)
            .Columns.Add(colAccountsID)
            .Columns.Add(colAccountCode)
            .Columns.Add(colAccounts)
            .Columns.Add(txtDebit)
            .Columns.Add(txtCredit)
            .Columns.Add(txtMemberID)
            .Columns.Add(fxKey)

            .Columns("cID").Width = 60
            .Columns("cLoanRef").Width = 80
            .Columns("cAccountRef").Width = 80
            .Columns("cSOA").Width = 80
            .Columns("cAccounts").Visible = False
            .Columns("cAccountCode").Width = 60
            .Columns("cAccountTitle").Width = 180
            .Columns("Debit").Width = 90
            .Columns("Credit").Width = 120
            .Columns("cName").Width = 190
            .Columns("fxKey").Visible = False
            .Columns("Debit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Credit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
        End With
    End Sub
    Public Function m_DisplayAccounts() As DataSet
        Dim sSQL As String = "AccountREF_ListMember "
        sSQL &= "@fcDocNumber='" & NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", 0) & "' "
        Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQL)
    End Function

    Private Function LoadGeneralJournalEntries_Details(ByVal jvKeyID As String) As DataSet

        Dim ds As New DataSet

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey1", _
                New SqlParameter("@JVKeyID", jvKeyID))
        Catch ex As Exception
            Throw ex
        End Try

        Return ds

    End Function

    Private Sub ComputeDebitAndCreditSumDetails()
        Dim totalCredit As Decimal
        Dim totaldebit As Decimal

        Try
            For Each xRow As DataGridViewRow In grdGenJournalDetails.Rows
                'Dim credit As Object = grdGenJournalDetails.Item("Credit", xRow.Index).Value
                Dim credit As Object = NormalizeValuesInDataGridView(grdGenJournalDetails, "Credit", xRow.Index)

                'Dim debit As Object = grdGenJournalDetails.Item("Debit", xRow.Index).Value
                Dim debit As Object = NormalizeValuesInDataGridView(grdGenJournalDetails, "Debit", xRow.Index)

                If credit IsNot Nothing Then
                    totalCredit += credit
                End If

                If debit IsNot Nothing Then
                    totaldebit += debit
                End If

            Next

            'btnSaveClose.Enabled = True
            SaveToolStripMenuItem.Enabled = True

        Catch ex As InvalidCastException
            'btnSaveClose.Enabled = False
            SaveToolStripMenuItem.Enabled = False

            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        txtTotalCredit.Text = Format(CDec(totalCredit), "##,##0.00")
        txtTotalDebit.Text = Format(CDec(totaldebit), "##,##0.00")

        txttotal.Text = Val(totaldebit) - Val(totalCredit)
        txttotal.Text = Format(CDec(txttotal.Text), "##,##0.00")
        Total = txttotal.Text
        'Me.grdListofEntries.Rows(0).Cells(4).Value = Format(CDec(Math.Abs(Total)), "##,##0.00")
    End Sub

    Private Function IsDebitAndCreditBalanced() As Boolean
        Dim isValid As Boolean = False

        If txtTotalCredit.Text = txtTotalDebit.Text Then
            isValid = True
        Else
            isValid = False
        End If

        Return isValid
    End Function

    Private Function IsJournalNoValid(ByVal journalNo As String) As Boolean
        Dim result As String = "True"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_ChecksIfJournalNoExists", _
            New SqlParameter("@journalNo", journalNo))
            If rd.Read() Then
                result = rd.Item("result").ToString()
            End If
        End Using

        'If journal no. exists then no is not valid
        If result = "True" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function IsDetailedTableNotEmpty() As Boolean
        Dim isNotEmpty As Boolean

        If grdGenJournalDetails.RowCount > 2 Then
            isNotEmpty = True
        Else
            isNotEmpty = False
        End If

        Return isNotEmpty
    End Function

    Private Sub PreviewJournalVoucher()
        Try
            Dim pkJournalID As String = grdListofEntries.CurrentRow.Cells("fxKeyJVNo").Value.ToString()
            If pkJournalID <> "" Then
                With frmJournalRpt
                    .GetJournalID() = pkJournalID
                    .MdiParent = frmMain
                    .Show()
                End With
            Else
                MsgBox("There is no selected entry to display the voucher. Please select first.", _
                            MsgBoxStyle.Information, Me.Text)
            End If
        Catch ex As NullReferenceException
            MessageBox.Show("There is no selected entry to display the voucher. Please select first.", _
                            "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GenerateJournalNo_RandomMethod()
        'Generate Random 
        Dim stemp As String = ""
        Dim xCnt As Integer = 0
        Dim sSQLCmd As String = "usp_xxxrandomid "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    stemp = rd.Item(0).ToString
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'txtGeneralJournalNo.Text = Me.cboDoctype.SelectedValue.ToString + CStr(Format(CDec(dteGeneralJournal.Value.Year), "0000")) + "-" + CStr(Format(CDec(dteGeneralJournal.Value.Month), "00")) + "-" + CStr(Format(CDec(dteGeneralJournal.Value.Day), "00")) + "-" + CStr(stemp)
    End Sub

    Private Sub DeleteJVEntry(ByVal JVEntryID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_Delete", _
                    New SqlParameter("@pkJVEntry", JVEntryID))
            MessageBox.Show("The JV Entry has been successfully deleted.", "General Journal Delete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GenerateKeyIDForDetailEntry(ByVal rowIndex As Integer)
        Dim newItemDetailID As String = System.Guid.NewGuid.ToString()
        grdGenJournalDetails.Item("fxKey", rowIndex).Value = newItemDetailID
    End Sub

    Private Sub IsAccountValid(ByVal accountID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_m_IsAccountActive", _
                New SqlParameter("@acnt_id", accountID))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub grdGenJournalDetails_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdGenJournalDetails.EditingControlShowing
        If TypeOf e.Control Is ComboBox Then
            cb = e.Control
            cb.DropDownStyle = ComboBoxStyle.DropDown
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.AutoCompleteSource = AutoCompleteSource.ListItems
        End If

        If grdGenJournalDetails.CurrentCell.ColumnIndex = 8 Then

            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress1

        End If

        If grdGenJournalDetails.CurrentCell.ColumnIndex = 9 Then

            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress1

        End If

    End Sub

    Private Sub TextBox_keyPress1(ByVal sender As Object, ByVal e As KeyPressEventArgs)

        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True
        End If

    End Sub

    Public Sub LoadDocNum()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_DocumentNumber_Selectop1",
                                      New SqlParameter("@docType", cboDoctype.Text),
                                      New SqlParameter("@coid", gCompanyID()))

        If ds.Tables(0).Rows.Count = 0 Then
            txtGeneralJournalNo.Text = ""
            lblNotif.Visible = True
            'grdGenJournalDetails.Enabled = False
            NewFToolStripMenuItem.Enabled = False
            SaveToolStripMenuItem.Enabled = False
            DeleteDocToolStripMenuItem.Enabled = False
            grdGenJournalDetails.Columns.Clear()
            grdListofEntries.Columns.Clear()
            '------------SOA-----------------
            'txtsoa.Text = ""

        Else
            txtGeneralJournalNo.Text = ds.Tables(0).Rows(0).Item("fcDocNumber").ToString
            lblNotif.Visible = False
            'grdGenJournalDetails.Enabled = True
            NewFToolStripMenuItem.Enabled = True
            'SaveToolStripMenuItem.Enabled = True
            DeleteDocToolStripMenuItem.Enabled = True
            'grdListofEntries.Visible = True
        End If

        mycon.sqlconn.Close()
    End Sub

    Private Sub LoadDoctype()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("Masterfile_LoadDoctype2", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Document")
            With cboDoctype
                .ValueMember = "pk_Initial"
                .DisplayMember = "fcDocName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub cboDoctype_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cboDoctype.MouseWheel
        Dim disable As HandledMouseEventArgs = e
        disable.Handled = True
    End Sub
    Private Sub cboDoctype_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDoctype.SelectedValueChanged
        If grdGenJournalDetails.RowCount <> 0 Then
            If previtem <> Me.cboDoctype.SelectedIndex Then
                If Me.cboDoctype.Text <> "" Then
                    If Me.cboDoctype.SelectedValue <> "" Then
                        Dim x As New DialogResult
                        x = MessageBox.Show("You have changed DocType, This will create NEW entry and Delete unsaved entry.. PROCEED?", "Notification", MessageBoxButtons.YesNo)
                        If x = Windows.Forms.DialogResult.Yes Then
                            Call PrepareForANewGeneralJournal()
                            grdGenJournalDetails.Columns.Clear()
                            grdListofEntries.Columns.Clear()
                            Call LoadDocNum()
                            Call CLearvalue()
                            Call DefaultAccount_Search()
                        Else
                            MessageBox.Show("Cancelled", "Notification", MessageBoxButtons.OK)
                            Me.cboDoctype.SelectedIndex = previtem
                        End If
                    End If
                End If
            End If
        End If
        previtem = Me.cboDoctype.SelectedIndex
        'LoadGeneralJournalEntries()
        'FormatGeneralJournalEntriesGrid()
        Try
            dtFrom.Value = SetAsFirstDayOfTheMonth()
            'grdListofEntries.DataSource = LoadGeneralJournalEntries(dtFrom.Value.Date, dtTo.Value.Date).Tables(0)
            'Call FormatGeneralJournalEntriesGrid()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Call TransactionForm()
        Call VisibleGridList()

    End Sub

    Private Sub btnSelectDocNum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectDocNum.Click
        frmSelectDocNum.ShowDialog()
    End Sub

    Private Sub txtTotalCredit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txttotal.Text = Format(CDec(Val(txtTotalDebit.Text)), "##,##0.00") - Format(CDec(Val(txtTotalCredit.Text)), "##,##0.00")
    End Sub

    Private Sub grdListofEntries_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListofEntries.CellValueChanged
        If e.ColumnIndex <> -1 Then
            If grdListofEntries.Columns(e.ColumnIndex).Name = "checkAmount" Then
                If NormalizeValuesInDataGridView(grdListofEntries, "checkAmount", e.RowIndex) = "" Then
                    grdListofEntries.Item("checkAmount", e.RowIndex).Value = 0
                Else
                    grdListofEntries.Item("checkAmount", e.RowIndex).Value = Format(CDec(grdListofEntries.Item("checkAmount", e.RowIndex).Value.ToString()), "##,##0.00")
                End If
            End If
        End If
    End Sub

    Private Sub grdListofEntries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdListofEntries.Click
        'EntryDetails()
    End Sub

    Private Sub txttotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FormatGeneralJournalEntriesGrid()
    End Sub

    Private Sub grdGenJournalDetails_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdGenJournalDetails.KeyDown
        Dim docuType As String = cboDoctype.Text
        Dim xRowIndex As Integer = grdGenJournalDetails.CurrentRow.Index
        Call grdColumns(xRowIndex)
        xX = grdGenJournalDetails.CurrentCellAddress.X
        xY = grdGenJournalDetails.CurrentCellAddress.Y
        If chkPosted.Checked = False Then

            Select Case xX
                Case 0
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmMembersFilter.ShowDialog()
                            If frmMembersFilter.DialogResult = DialogResult.OK Then
                                Dim cName As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                                Dim cID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cMemID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddDetailItem(cID, cName, "", "", "", "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), cMemID, "")
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                    End Try
                                Else
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(0).Cells("cAccountCode").Selected = True
                                    End Try
                                    grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                                    grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                                    grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                                End If
                            End If
                        Else
                            If xRowIndex = 0 Then
                                grdGenJournalDetails.Rows(0).Cells("cID").Selected = True
                            Else
                                grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cID").Selected = True
                            End If

                            Exit Sub
                        End If
                    End If
                Case 1
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmMembersFilter.ShowDialog()
                            If frmMembersFilter.DialogResult = DialogResult.OK Then
                                Dim cName As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                                Dim cID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cMemID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddDetailItem(cID, cName, "", "", "", "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), cMemID, "")
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                    End Try
                                Else
                                    grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                                    grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                                    grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                    End Try
                                End If
                            End If
                        Else
                            If xRowIndex = 0 Then
                                grdGenJournalDetails.Rows(0).Cells("cName").Selected = True
                            Else
                                grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cName").Selected = True
                            End If
                            Exit Sub
                        End If
                    End If
                Case 2
                    If e.KeyCode = Keys.Enter Then
                        frmLoanFilter.ShowDialog()
                        If frmLoanFilter.DialogResult = DialogResult.OK Then
                            Dim cID As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(0).Value
                            Dim cName As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(1).Value
                            Dim cAccountTitle As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(2).Value
                            Dim cLoanRef As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(3).Value
                            Dim cMemID As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(6).Value.ToString
                            Dim cAccountCode As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(7).Value
                            Dim cAccounts As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(8).Value.ToString
                            Dim dCredit As Decimal = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(5).Value
                            Dim LoanType As String = frmLoanFilter.grdLoanList.SelectedRows(0).Cells(9).Value.ToString
                            'AddDetailItem(cID, cName, cLoanRef, "", cAccounts, cAccountCode, cAccountTitle, 0, dCredit, cMemID, "")

                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddDetailItem(cID, cName, cLoanRef, "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(0)), "##,##0.00"), Format(CDec(Val(dCredit)), "##,##0.00"), cMemID, "")
                                Try
                                    Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_LoadAccountsForPayment",
                                      New SqlParameter("@pkLoanType", LoanType),
                                      New SqlParameter("@IDno", cID))

                                    Dim row As Integer
                                    While rd.Read
                                        row = grdGenJournalDetails.RowCount - 1
                                        Dim AcntID = rd.Item("fk_Account").ToString
                                        Dim AcntCode = rd.Item("acnt_code").ToString
                                        Dim AcntTitle = rd.Item("Description").ToString
                                        Dim Amount = rd.Item("Amount").ToString
                                        Dim AcntRef = rd.Item("fcDocNumber").ToString
                                        AddDetailItem(cID, cName, cLoanRef, AcntRef, "", AcntID, AcntCode, AcntTitle, Format(CDec(Val(0)), "##,##0.00"), Format(CDec(Val(Amount)), "##,##0.00"), cMemID, "")
                                    End While
                                    rd.Close()
                                Catch ex As Exception
                                    MsgBox(ex.Message, , "Load Loan Accounts")
                                End Try
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(0).Cells("Debit").Selected = True
                                End Try
                            ElseIf grdGenJournalDetails.Item("cAccounts", xRowIndex).Value <> "" And cMemID = grdGenJournalDetails.Item("cMemID", xRowIndex).Value.ToString Then
                                grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                                grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                                grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                                grdGenJournalDetails.Item("cLoanRef", xRowIndex).Value = cLoanRef
                                'grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccounts
                                '[grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                'grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            End If
                            Call ComputeDebitAndCreditSumDetails()
                        End If
                    End If
                Case 3
                    If e.KeyCode = Keys.Enter Then
                        frmSubsidiaryAccountFilter.xModule = "Entry"
                        frmSubsidiaryAccountFilter.ShowDialog()
                        If frmSubsidiaryAccountFilter.DialogResult = DialogResult.OK Then
                            Dim cAccountRef As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(1).Value.ToString()
                            Dim cID As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(2).Value.ToString()
                            Dim cName As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(3).Value.ToString()
                            Dim cMemID As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(7).Value.ToString()
                            Dim cAccountTitle As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(5).Value.ToString()
                            Dim cAccount As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(6).Value.ToString()
                            Dim cAccountCode As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(4).Value.ToString()
                            Dim Debit As Decimal = 0
                            Dim Credit As Decimal = 0
                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolSoa = "" And grdcolCode = "" And grdcolTitle = "" And grdcolSoa = "" Then
                                AddDetailItem(cID, cName, "", cAccountRef, "", cAccount, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), cMemID, "")
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            Else
                                grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                                grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                                grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                                grdGenJournalDetails.Item("cAccountRef", xRowIndex).Value = cAccountRef
                                grdGenJournalDetails.Item("cSoa", xRowIndex).Value = grdcolSoa
                                grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccount
                                grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            End If
                            txtCode.Text = grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()
                            txtclientname.Text = grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()
                            Call ListCurrentBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString())
                            Call ListAvailableBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString(), dteGeneralJournal.Text)
                        End If
                    End If
                Case 4
                    If e.KeyCode = Keys.Enter Then
                        frmMasterfile_SoaNumber.ShowDialog()
                        If frmMasterfile_SoaNumber.DialogResult = DialogResult.OK Then
                            Select Case cboDoctype.Text
                                Case "RECEIPTS"
                                    Dim cSoaRef As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(0).Value.ToString()
                                    Dim AcntName As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(4).Value.ToString()
                                    Dim Code As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(5).Value.ToString()
                                    Dim ID As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(6).Value.ToString()
                                    Dim Name As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(7).Value.ToString()
                                    Dim Debit As Decimal = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(8).Value.ToString()
                                    Dim Credit As Decimal = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(9).Value.ToString()
                                    Dim KeyAccount As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(10).Value.ToString()
                                    Dim AcntRef As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(11).Value.ToString()
                                    Dim AcntLoan As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(12).Value.ToString()
                                    Dim EmpID As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(13).Value.ToString()
                                    If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" And grdcolSoa = "" Then
                                        AddDetailItem(ID, Name, AcntLoan, AcntRef, cSoaRef, KeyAccount, Code, AcntName, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), EmpID, "")
                                        Call ComputeDebitAndCreditSumDetails()
                                        Try
                                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                        Catch ex As Exception
                                            grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                        End Try
                                    Else
                                        grdGenJournalDetails.Item("cID", xRowIndex).Value = ID
                                        grdGenJournalDetails.Item("cName", xRowIndex).Value = Name
                                        grdGenJournalDetails.Item("cAccountRef", xRowIndex).Value = AcntRef
                                        grdGenJournalDetails.Item("cSoa", xRowIndex).Value = cSoaRef
                                        grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = AcntName
                                        grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = Code
                                        grdGenJournalDetails.Item("Debit", xRowIndex).Value = Debit
                                        grdGenJournalDetails.Item("Credit", xRowIndex).Value = Credit
                                        grdGenJournalDetails.Item("cMemID", xRowIndex).Value = EmpID
                                        Call ComputeDebitAndCreditSumDetails()
                                        Try
                                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                        Catch ex As Exception
                                            grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                        End Try
                                    End If
                                Case Else
                                    Dim cSoaRef As String = frmMasterfile_SoaNumber.GrdAccounts.SelectedRows(0).Cells(0).Value.ToString()
                                    Dim Debit As Decimal = 0
                                    Dim Credit As Decimal = 0
                                    If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" And grdcolSoa = "" Then
                                        AddDetailItem("", "", "", "", cSoaRef, "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), "", "")
                                        Call ComputeDebitAndCreditSumDetails()
                                        Try
                                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                        Catch ex As Exception
                                            grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                        End Try
                                    Else
                                        grdGenJournalDetails.Item("cSoa", xRowIndex).Value = cSoaRef
                                        Try
                                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                        Catch ex As Exception
                                            grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                        End Try
                                    End If

                            End Select
                        End If
                    End If
                Case 6
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmCOAFilter.xModule = "Entry"
                            frmCOAFilter.ShowDialog()
                            If frmCOAFilter.DialogResult = DialogResult.OK Then
                                Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                                Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(3).Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddDetailItem("", "", "", "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), "", "")
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                    End Try
                                Else
                                    grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccounts
                                    grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                    grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                    End Try
                                End If
                            End If
                        Else
                            Try
                                grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                            Catch ex As Exception
                                grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                            End Try
                            Exit Sub
                        End If
                    End If
                Case 7
                    If e.KeyCode = Keys.Enter Then
                        If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                            frmCOAFilter.xModule = "Entry"
                            frmCOAFilter.ShowDialog()
                            If frmCOAFilter.DialogResult = DialogResult.OK Then
                                Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                                Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(3).Value.ToString()
                                Dim Debit As Decimal = 0
                                Dim Credit As Decimal = 0
                                If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                    AddDetailItem("", "", "", "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), "", "")
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                    End Try
                                Else
                                    grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccounts
                                    grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                    grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                    Try
                                        grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                    Catch ex As Exception
                                        grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                    End Try
                                End If
                            End If
                        Else
                            Try
                                grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountTitle").Selected = True
                            Catch ex As Exception
                                grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountTitle").Selected = True
                            End Try
                            Exit Sub
                        End If
                    End If
            End Select

            If e.KeyCode = Keys.Delete Then
                If MsgBox("Are you sure you want to DELETE this LINE?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                    Call DeleteRow()
                Else
                    Exit Sub
                End If
            End If
        End If

    End Sub

    Private Sub AddDetailItem(ByVal cID As String,
                             ByVal cName As String,
                             ByVal cLoanRef As String,
                             ByVal cAccountRef As String,
                             ByVal Soa As String,
                             ByVal cAccounts As String,
                             ByVal cAccountCode As String,
                             ByVal cAccountTitle As String,
                             ByVal Debit As Decimal,
                             ByVal Credit As Decimal,
                             ByVal cMemID As String,
                             ByVal fxKey As String)

        Try
            Dim row As String() = {cID, cName, cLoanRef, cAccountRef, Soa, cAccounts, cAccountCode, cAccountTitle, Format(CDec(Debit), "##,##0.00"), Format(CDec(Credit), "##,##0.00"), cMemID, fxKey}

            Dim nRowIndex As Integer
            With grdGenJournalDetails
                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub DeleteSelectedRow()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_Details_DeleteSeletedRow", _
                New SqlParameter("@fxKeyJVDetails", keyDetails))
            grdGenJournalDetails.Rows.Remove(grdGenJournalDetails.CurrentRow)
        Catch ex As Exception
            'grdGenJournalDetails.Rows.Remove(grdGenJournalDetails.CurrentRow)
            grdGenJournalDetails.Rows.Remove(grdGenJournalDetails.CurrentRow)
        End Try
    End Sub

    Private Sub grdListofEntries_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdListofEntries.KeyDown
        Dim docuType As String = cboDoctype.Text
        xX = grdListofEntries.CurrentCellAddress.X
        xY = grdListofEntries.CurrentCellAddress.Y
        Dim i As Integer = grdListofEntries.CurrentRow.Index
        Select Case docuType
            Case "RECEIPTS"
                Select Case xX
                    Case 1
                        If e.KeyCode = Keys.Enter Then
                            ListofEntriesReadOnly()
                            frmClientBank.ShowDialog()
                            frmClientBank.StartPosition = FormStartPosition.CenterScreen
                            If frmClientBank.DialogResult = DialogResult.OK Then
                                Dim cBankName As String = frmClientBank.dgvClientBank.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cBankID As String = frmClientBank.dgvClientBank.SelectedRows(0).Cells(0).Value.ToString()
                                Dim cMaker As String = frmMain.lblCompanyName.Text
                                Dim cPayee As String = IIf(IsDBNull(grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()), "", grdGenJournalDetails.Rows(0).Cells(1).Value.ToString())
                                Dim cAmount As Decimal = (CDec(txttotal.Text)) * (-1)
                                If grdListofEntries.Item("Bank", i).Value = "" And grdListofEntries.Item("CheckNo", i).Value = "" Then
                                    AddItem(cBankID, cBankName, "", Format(Date.Now, "MMMM dd, yyyy"), cAmount, cMaker, cPayee, "")
                                    Try
                                        grdListofEntries.Rows(i - 1).Cells("CheckNo").Selected = True
                                    Catch ex As Exception
                                        grdListofEntries.Rows(i).Cells("CheckNo").Selected = True
                                    End Try
                                Else
                                    Call DeleteORCheck(cBankID, grdListofEntries.Item("CheckNo", i).Value.ToString, GetJVKeyID())
                                    AddItem(cBankID, cBankName, "", Format(Date.Now, "MMMM dd, yyyy"), cAmount, cMaker, cPayee, "")
                                    Try
                                        grdListofEntries.Rows(i - 1).Cells("CheckNo").Selected = True
                                    Catch ex As Exception
                                        grdListofEntries.Rows(i).Cells("CheckNo").Selected = True
                                    End Try
                                    Exit Sub
                                End If
                            End If
                        End If
                End Select

            Case "DEPOSIT SLIP"
                Select Case xX
                    Case 1
                        If e.KeyCode = Keys.Enter Then
                            ListofEntriesReadOnly()
                            frmClientBank.ShowDialog()
                            frmClientBank.StartPosition = FormStartPosition.CenterScreen
                            If frmClientBank.DialogResult = DialogResult.OK Then
                                Dim cBankName As String = frmClientBank.dgvClientBank.SelectedRows(0).Cells(1).Value.ToString()
                                Dim cBankID As String = frmClientBank.dgvClientBank.SelectedRows(0).Cells(0).Value.ToString()
                                Dim cMaker As String = frmMain.lblCompanyName.Text
                                Dim cPayee As String = IIf(IsDBNull(grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()), "", grdGenJournalDetails.Rows(0).Cells(1).Value.ToString())
                                Dim cAmount As Decimal = (CDec(txttotal.Text)) * (-1)
                                If grdListofEntries.Item("Bank", i).Value = "" And grdListofEntries.Item("CheckNo", i).Value = "" Then
                                    AddItem(cBankID, cBankName, "", Format(Date.Now, "MMMM dd, yyyy"), cAmount, cMaker, cPayee, "")
                                    Try
                                        grdListofEntries.Rows(i - 1).Cells("CheckNo").Selected = True
                                    Catch ex As Exception
                                        grdListofEntries.Rows(i).Cells("CheckNo").Selected = True
                                    End Try
                                Else
                                    Call DeleteORCheck(cBankID, grdListofEntries.Item("CheckNo", i).Value.ToString, GetJVKeyID())
                                    AddItem(cBankID, cBankName, "", Format(Date.Now, "MMMM dd, yyyy"), cAmount, cMaker, cPayee, "")
                                    Try
                                        grdListofEntries.Rows(i - 1).Cells("CheckNo").Selected = True
                                    Catch ex As Exception
                                        grdListofEntries.Rows(i).Cells("CheckNo").Selected = True
                                    End Try
                                    Exit Sub
                                End If
                            End If
                        End If
                End Select

            Case Else
                Select Case xX
                    Case 1
                        If e.KeyCode = Keys.Enter Then
                            If IsNumeric(txttotal.Text) = True And Convert.ToDecimal(txttotal.Text) > 0.0 Then
                                frmBankList.ShowDialog()
                                frmBankList.StartPosition = FormStartPosition.CenterScreen
                                If frmBankList.DialogResult = DialogResult.OK Then
                                    Dim bank As String = frmBankList.dgvBankList.SelectedRows(0).Cells(0).Value.ToString()
                                    Dim Bankid As String = frmBankList.dgvBankList.SelectedRows(0).Cells(1).Value.ToString()
                                    Dim bankcode As String = frmBankList.dgvBankList.SelectedRows(0).Cells(2).Value.ToString()
                                    Dim cMaker As String = frmMain.lblCompanyName.Text
                                    Dim cPayee As String = IIf(IsDBNull(grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()), "", grdGenJournalDetails.Rows(0).Cells(1).Value.ToString())
                                    AddItem(Bankid, bank, "", Format(Date.Now, "MMMM dd, yyyy"), CDec(txttotal.Text), cMaker, cPayee, bankcode)
                                End If
                            Else
                                MsgBox("Difference must be greater than zero", vbInformation)
                            End If
                        End If
                    Case 2
                        If e.KeyCode = Keys.Enter Then
                            frmSelectCheck.ShowDialog()
                            frmSelectCheck.StartPosition = FormStartPosition.CenterScreen
                            If frmSelectCheck.DialogResult = DialogResult.OK Then
                                grdListofEntries.Item("CheckNo", i).Value = frmSelectCheck.dgvSelectCheck.SelectedRows(0).Cells(0).Value.ToString()
                                frmCheckReport.txtcheckNo.Text = frmSelectCheck.dgvSelectCheck.SelectedRows(0).Cells(0).Value.ToString()
                            End If
                        End If
                    Case 6
                        If e.KeyCode = Keys.Enter Then
                            frmPayee.ShowDialog()
                            frmPayee.StartPosition = FormStartPosition.CenterScreen
                            If frmPayee.DialogResult = DialogResult.OK Then
                                Dim dPayee As String = frmPayee.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                                Dim dMemID As String = frmPayee.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                                Dim dMemKey As String = frmPayee.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()

                                grdListofEntries.Item("MemID", i).Value = dMemID
                                grdListofEntries.Item("Payee", i).Value = dPayee
                                grdListofEntries.Item("MemKey", i).Value = dMemKey
                            End If
                        End If
                End Select
        End Select
    End Sub

    Private Sub AddItem(ByVal keyID As String,
                              ByVal Bank As String,
                              ByVal CheckNo As String,
                              ByVal CheckDate As String,
                              ByVal checkAmount As Decimal,
                              ByVal Maker As String,
                              ByVal Payee As String,
                              ByVal BankCode As String)
        Dim cMemID As String = IIf(IsDBNull(grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()), "", grdGenJournalDetails.Rows(0).Cells(0).Value.ToString())
        Dim cMemKey As String = IIf(IsDBNull(grdGenJournalDetails.Rows(0).Cells(9).Value.ToString()), "", grdGenJournalDetails.Rows(0).Cells(9).Value.ToString())
        Try
            Dim row As String() =
             {keyID, Bank, CheckNo, CheckDate, Format(CDec(checkAmount), "##,##0.00"), Maker, Payee, BankCode, cMemID, cMemKey}

            Dim nRowIndex As Integer
            With grdListofEntries

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub LoadItem(ByVal keyID As String,
                              ByVal Bank As String,
                              ByVal CheckNo As String,
                              ByVal CheckDate As String,
                              ByVal checkAmount As Decimal,
                              ByVal Maker As String,
                              ByVal Payee As String,
                              ByVal BankCode As String)
        Dim cMemID As String
        Dim cMemKey As String

        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "spu_CIMS_getClientKey",
                                                                            New SqlParameter("@filter", Payee))
        If rd.Read = True Then
            cMemID = rd.Item(1).ToString
            cMemKey = rd.Item(0).ToString
        Else
            cMemID = ""
            cMemKey = ""
            Payee = ""
        End If

        Try
            Dim row As String() =
             {keyID, Bank, CheckNo, CheckDate, Format(CDec(checkAmount), "##,##0.00"), Maker, Payee, BankCode, cMemID, cMemKey}

            Dim nRowIndex As Integer
            With grdListofEntries

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 2
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


    Private Sub grdGenJournalDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdGenJournalDetails.DoubleClick
        xX = grdGenJournalDetails.CurrentCellAddress.X
        xY = grdGenJournalDetails.CurrentCellAddress.Y
        Dim xRowIndex As Integer = grdGenJournalDetails.CurrentRow.Index
        Call grdColumns(xRowIndex)
        If chkPosted.Checked = False Then
            Select Case xX
                Case 0
                    If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                        frmMembersFilter.ShowDialog()
                        If frmMembersFilter.DialogResult = DialogResult.OK Then
                            Dim cName As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                            Dim cID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                            Dim cMemID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
                            Dim Debit As Decimal = 0
                            Dim Credit As Decimal = 0
                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddDetailItem(cID, cName, "", "", "", "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), cMemID, System.Guid.NewGuid.ToString)
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                End Try
                            Else
                                grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                                grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                                grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                End Try
                            End If
                        End If
                    Else
                        Try
                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cID").Selected = True
                        Catch ex As Exception
                            grdGenJournalDetails.Rows(xRowIndex).Cells("cID").Selected = True
                        End Try
                        Exit Sub
                    End If
                Case 1
                    If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                        frmMembersFilter.ShowDialog()
                        If frmMembersFilter.DialogResult = DialogResult.OK Then
                            Dim cName As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(2).Value.ToString()
                            Dim cID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                            Dim cMemID As String = frmMembersFilter.grdCoAList.SelectedRows(0).Cells(0).Value.ToString()
                            Dim Debit As Decimal = 0
                            Dim Credit As Decimal = 0
                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddDetailItem(cID, cName, "", "", "", "", "", "", Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), cMemID, "")
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                End Try
                            Else
                                grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                                grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                                grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                                End Try
                            End If
                        End If
                    Else
                        Try
                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cName").Selected = True
                        Catch ex As Exception
                            grdGenJournalDetails.Rows(xRowIndex).Cells("cName").Selected = True
                        End Try
                        Exit Sub
                    End If
                Case 2
                    frmLoanFilter.ShowDialog()
                Case 3
                    frmSubsidiaryAccountFilter.xModule = "Entry"
                    frmSubsidiaryAccountFilter.ShowDialog()
                    If frmSubsidiaryAccountFilter.DialogResult = DialogResult.OK Then
                        Dim cAccountRef As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(1).Value.ToString()
                        Dim cID As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(2).Value.ToString()
                        Dim cName As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(3).Value.ToString()
                        Dim cMemID As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(7).Value.ToString()
                        Dim cAccountTitle As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(5).Value.ToString()
                        Dim cAccount As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(6).Value.ToString()
                        Dim cAccountCode As String = frmSubsidiaryAccountFilter.grdSAList.SelectedRows(0).Cells(4).Value.ToString()
                        Dim Debit As Decimal = 0
                        Dim Credit As Decimal = 0
                        If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                            AddDetailItem(cID, cName, "", cAccountRef, "", cAccount, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), cMemID, "")
                            Try
                                grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                            Catch ex As Exception
                                grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                            End Try
                        Else
                            grdGenJournalDetails.Item("cID", xRowIndex).Value = cID
                            grdGenJournalDetails.Item("cName", xRowIndex).Value = cName
                            grdGenJournalDetails.Item("cMemID", xRowIndex).Value = cMemID
                            grdGenJournalDetails.Item("cAccountRef", xRowIndex).Value = cAccountRef
                            grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccount
                            grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                            grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                            Try
                                grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                            Catch ex As Exception
                                grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                            End Try
                        End If
                        txtCode.Text = grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()
                        txtclientname.Text = grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()
                        Call ListCurrentBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString())
                        Call ListAvailableBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString(), dteGeneralJournal.Text)
                        'End If
                    End If
                Case 5
                    If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                        frmCOAFilter.xModule = "Entry"
                        frmCOAFilter.ShowDialog()
                        If frmCOAFilter.DialogResult = DialogResult.OK Then
                            Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                            Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                            Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(3).Value.ToString()
                            Dim Debit As Decimal = 0
                            Dim Credit As Decimal = 0
                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddDetailItem("", "", "", "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), "", "")
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            Else
                                grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccounts
                                grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            End If
                        End If
                    Else
                        Try
                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountCode").Selected = True
                        Catch ex As Exception
                            grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountCode").Selected = True
                        End Try
                        Exit Sub
                    End If
                Case 6
                    If grdcolLoanRef = "" And grdcolAcntRef = "" Then
                        frmCOAFilter.xModule = "Entry"
                        frmCOAFilter.ShowDialog()
                        If frmCOAFilter.DialogResult = DialogResult.OK Then
                            Dim cAccountTitle As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                            Dim cAccountCode As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                            Dim cAccounts As String = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(3).Value.ToString()
                            Dim Debit As Decimal = 0
                            Dim Credit As Decimal = 0
                            If grdcolID = "" And grdcolName = "" And grdcolLoanRef = "" And grdcolAcntRef = "" And grdcolCode = "" And grdcolTitle = "" Then
                                AddDetailItem("", "", "", "", "", cAccounts, cAccountCode, cAccountTitle, Format(CDec(Val(Debit)), "##,##0.00"), Format(CDec(Val(Credit)), "##,##0.00"), "", "")
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            Else
                                grdGenJournalDetails.Item("cAccounts", xRowIndex).Value = cAccounts
                                grdGenJournalDetails.Item("cAccountCode", xRowIndex).Value = cAccountCode
                                grdGenJournalDetails.Item("cAccountTitle", xRowIndex).Value = cAccountTitle
                                Try
                                    grdGenJournalDetails.Rows(xRowIndex - 1).Cells("Debit").Selected = True
                                Catch ex As Exception
                                    grdGenJournalDetails.Rows(xRowIndex).Cells("Debit").Selected = True
                                End Try
                            End If
                        End If
                    Else
                        Try
                            grdGenJournalDetails.Rows(xRowIndex - 1).Cells("cAccountTitle").Selected = True
                        Catch ex As Exception
                            grdGenJournalDetails.Rows(xRowIndex).Cells("cAccountTitle").Selected = True
                        End Try
                        Exit Sub
                    End If
            End Select
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If MsgBox("Are you sure you want to DELETE this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRY") = MsgBoxResult.Yes Then
            If isUpdate = True Then
                Dim sSQLCmd As String = "update mDocNumber Set fdDateUsed= Null where fcDocNumber='" & txtGeneralJournalNo.Text & "' "
                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                Call DeleteAllDetailedEntriesPerJV()
                'Call DeleteATransactrionHeader()
                Call PrepareForANewGeneralJournal()
                MsgBox("DELETE successful.", MsgBoxStyle.Information)
            Else
                grdGenJournalDetails.Columns.Clear()
                grdGenJournalDetails.DataSource = Nothing
                Call PrepareForANewGeneralJournal()
            End If
        Else

        End If
    End Sub

    Private Sub DeleteAllDetailedEntriesPerJV()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DeleteAllDetailedEntriesPerJV", _
                New SqlParameter("@fk_tJVEntry", GetJVKeyID()))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetAsReadOnly()
        'grdGenJournalDetails.Enabled = False
        grdGenJournalDetails.Columns("Debit").ReadOnly = True
        grdGenJournalDetails.Columns("Credit").ReadOnly = True
        grdListofEntries.Enabled = False
        txtMemo.ReadOnly = True
        dteGeneralJournal.Enabled = False
        btnSelectDocNum.Enabled = False
        SaveToolStripMenuItem.Enabled = False
        dtpDatePrepared.Enabled = False
        'chkCancelled.Enabled = False
    End Sub

    Private Sub MakeItEnabled()
        grdGenJournalDetails.Columns("Debit").ReadOnly = False
        grdGenJournalDetails.Columns("Credit").ReadOnly = False
        grdGenJournalDetails.Enabled = True
        grdListofEntries.Enabled = True
        txtMemo.ReadOnly = False
        dteGeneralJournal.Enabled = True
        btnSelectDocNum.Enabled = True
        DeleteCheckToolStripMenuItem.Enabled = True
        SaveToolStripMenuItem.Enabled = True
        dtpDatePrepared.Enabled = True
        chkCancelled.Enabled = True
        chkPosted.Enabled = True
        RecurringEntryToolStripMenuItem.Enabled = True
    End Sub

    Private Sub PrepareCheckEntry()
        'Try
        Dim rd As SqlDataReader
        Dim djournalNo As String
        Dim dAmount As Decimal
        Dim dtTransact As Date
        Dim dBankName As String
        Dim dKeyID As String
        Dim dBankCode As String
        Dim dPayee As String
        Dim dMaker As String
        Dim dCheckNumber As String
        Dim lastrow As Integer = grdGenJournalDetails.Rows.Count - 1
        Dim row As Integer = grdListofEntries.Rows.Count - 2
        For xRow As Integer = 0 To grdListofEntries.RowCount - 1
            If xRow = row Then

                rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_getAcntId",
                                        New SqlParameter("@co_id", gCompanyID),
                                        New SqlParameter("@acnt_name", NormalizeValuesInDataGridView(grdListofEntries, "Bank", xRow).ToString))
                With rd.Read
                    dKeyID = rd.Item("acnt_id").ToString
                    dBankCode = rd.Item("acnt_code").ToString
                End With

                djournalNo = txtGeneralJournalNo.Text
                dAmount = grdListofEntries.Item("checkAmount", xRow).Value
                dtTransact = dteGeneralJournal.Value.Date
                dCheckNumber = NormalizeValuesInDataGridView(grdListofEntries, "CheckNo", xRow).ToString
                dBankName = NormalizeValuesInDataGridView(grdListofEntries, "Bank", xRow).ToString
                dPayee = NormalizeValuesInDataGridView(grdListofEntries, "Payee", xRow).ToString
                dMaker = NormalizeValuesInDataGridView(grdListofEntries, "Maker", xRow).ToString
                Dim dMemID As String = NormalizeValuesInDataGridView(grdListofEntries, "MemID", xRow).ToString
                Dim dMemKey As String = NormalizeValuesInDataGridView(grdListofEntries, "MemKey", xRow).ToString

                If dBankName <> "" Then
                    AddDetailItem(dMemID, dPayee, "", "", "", dKeyID, dBankCode, dBankName, 0, dAmount, dMemKey, "")
                    Call ComputeDebitAndCreditSumDetails()
                    Call CheckNumberInsert(dKeyID, dAmount)
                End If
            End If
        Next
    End Sub

    Private Sub CheckNumberInsert(ByVal dKeyAccount As String, ByVal dCredit As Decimal)
        Try
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim dtTransact As Date = dteGeneralJournal.Value.Date
            Dim CheckNumber As String = grdListofEntries.Item("CheckNo", 0).Value.ToString
            Dim rowID As String = System.Guid.NewGuid.ToString()
            Dim MemKey As String = IIf(IsDBNull(grdListofEntries.Item("MemKey", 0).Value.ToString), "", grdListofEntries.Item("MemKey", 0).Value.ToString)
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Save", _
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID()), _
                                              New SqlParameter("@fiEntryNo", journalNo), _
                                              New SqlParameter("@fxKeyAccount", dKeyAccount), _
                                              New SqlParameter("@fdCredit", dCredit), _
                                              New SqlParameter("@transDate", dtTransact), _
                                              New SqlParameter("@fcCheckNo", CheckNumber),
                                              New SqlParameter("@rowID", rowID),
                                              New SqlParameter("@fxKeyNameID", MemKey),
                                              New SqlParameter("@memo", txtMemo.Text))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub PrepareLoanSubsidiaryEntry_Payment(ByVal checkstatus As Boolean)
        For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
            ProgressBar1.Value += 1
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim dDebit As Decimal = grdGenJournalDetails.Item("Debit", xRow).Value
            Dim dCredit As Decimal = grdGenJournalDetails.Item("Credit", xRow).Value
            Dim sMemo As String = txtMemo.Text
            Dim dtTransact As Date = dteGeneralJournal.Value.Date
            Dim LoanRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cLoanRef", xRow)
            Dim AcntID As String = grdGenJournalDetails.Item("cAccounts", xRow).Value.ToString
            Dim AcntCode As String = grdGenJournalDetails.Item("cAccountCode", xRow).Value.ToString
            Dim AccountRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow)
            Dim SoaRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSoa", xRow)
            If checkstatus = True Then
                '----For Loan
                If LoanRef <> "" Then
                    If dDebit = 0 And dCredit <> 0 Then
                        Call LoanAccountsDisbursement(LoanRef, AcntID, dtTransact, journalNo, sMemo, dCredit, AcntCode)
                    ElseIf dDebit <> 0 And dCredit = 0 Then
                        Call UpdateLoanSubsidiary_DebitEntry(LoanRef, dtTransact, journalNo, sMemo, dDebit, 1, 1)
                    End If
                End If

                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 1, SoaRef)
                End If
            Else
                If LoanRef <> "" Then
                    If dDebit = 0 And dCredit <> 0 Then
                        If LoanRef <> "" Then
                            'Call UpdateLoanSubsidiary_UnpostEntry(LoanRef, dtTransact, journalNo, dCredit)
                            Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_SelectLoanHeader", _
                                          New SqlParameter("@fcAccountCode", AcntCode))
                            If rd2.Read = True Then
                                Call UpdateLoanSubsidiary_UnpostEntry(LoanRef, dtTransact, journalNo, dCredit, 1)
                            Else
                                Call UpdateLoanSubsidiary_UnpostEntry(LoanRef, dtTransact, journalNo, dCredit, 0)
                            End If
                        End If
                    ElseIf dDebit <> 0 And dCredit = 0 Then
                        Call UpdateLoanSubsidiary_DebitEntry(LoanRef, dtTransact, journalNo, sMemo, dDebit, 1, 0)
                    End If
                End If
                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 0, SoaRef)
                End If

            End If
            LoanRef = ""
            AccountRef = ""
        Next
    End Sub

    Private Sub UpdateLoanSubsidiary_Payment(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal amountPay As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "_Insert_Amortization_Payments", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@fcDate", fdDate), _
                                      New SqlParameter("@fcDocNumber", docnum), _
                                      New SqlParameter("@AmountPay", amountPay))
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub UpdateLoanSubsidiary_Payment2(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal sMemo As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "_InsertCompute_LoanSubsidiary", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@Date", fdDate), _
                                      New SqlParameter("@DocNumber", docnum), _
                                      New SqlParameter("@Particulars", sMemo), _
                                      New SqlParameter("@isCheck", 1))
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub UpdateLoanSubsidiary_DebitEntry(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal particulars As String, ByVal dDebit As Decimal, ByVal ischeck As Boolean, ByVal checkstatus As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loan_InsertDebitAmount", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@Date", fdDate), _
                                      New SqlParameter("@DocNumber", docnum), _
                                      New SqlParameter("@Particulars", particulars), _
                                      New SqlParameter("@Debit", dDebit), _
                                      New SqlParameter("@isCheck", ischeck), _
                                      New SqlParameter("@CheckStatus", checkstatus))
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub UpdateLoanSubsidiary_UnpostEntry(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal amountPay As Decimal, ByVal LoanType As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_UnpostedEntry", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@fcDate", fdDate), _
                                      New SqlParameter("@fcDocNumber", docnum), _
                                      New SqlParameter("@AmountPay", amountPay),
                                      New SqlParameter("@isLoan", LoanType))
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    '=================================================================================================
    '============================================ POSTING ============================================
    '=================================================================================================
#Region "Posting"
    Private Sub PrepareLoanSubsidiaryEntry(ByVal checkstatus As Boolean)
        For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
            pRowCount = grdGenJournalDetails.RowCount
            pRow = xRow + 1
            journalNo = txtGeneralJournalNo.Text
            dDebit = grdGenJournalDetails.Item("Debit", xRow).Value
            dCredit = grdGenJournalDetails.Item("Credit", xRow).Value
            sMemo = txtMemo.Text
            dtTransact = dteGeneralJournal.Value.Date
            LoanRef = Trim(NormalizeValuesInDataGridView(grdGenJournalDetails, "cLoanRef", xRow))
            acntID = grdGenJournalDetails.Item("cAccounts", xRow).Value
            acntTitle = grdGenJournalDetails.Item("cAccountTitle", xRow).Value
            acntCode = Trim(grdGenJournalDetails.Item("cAccountCode", xRow).Value)
            AccountRef = Trim(NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow))
            pID = NormalizeValuesInDataGridView(grdGenJournalDetails, "cID", xRow)
            pName = NormalizeValuesInDataGridView(grdGenJournalDetails, "cName", xRow)
            pMemKey = NormalizeValuesInDataGridView(grdGenJournalDetails, "cMemID", xRow)
            Dim SoaRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", xRow)
            If checkstatus = True Then
                If LoanRef <> "" Then
                    Call LoanAccountsDisbursement2(LoanRef, acntID, dtTransact, journalNo, sMemo, dCredit, acntCode, dDebit, checkstatus, bLoanForRelease)
                End If

                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 1, SoaRef)
                End If

                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwPosting.ReportProgress(percent)
                'Thread.Sleep(0)
                'System.Threading.Thread.SpinWait(10)
            Else
                If Trim(LoanRef) <> "" Then
                    Call LoanAccountsDisbursement2(LoanRef, acntID, dtTransact, journalNo, sMemo, dCredit, acntCode, dDebit, checkstatus, bLoanForRelease)
                End If

                '----For SL
                If Trim(AccountRef) <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 0, SoaRef)
                End If

                Dim percent As Integer = (xRow / pRowCount) * 100
                bgwUnposting.ReportProgress(percent)
                'Thread.Sleep(0)
                'System.Threading.Thread.SpinWait(10)
            End If


            LoanRef = ""
            AccountRef = ""
        Next

    End Sub

    '--SAVE TO TEMP--'
    Private Sub LoanAccountsDisbursement(ByVal loanNo As String, ByVal acnt_id As String, ByVal transdate As Date, ByVal docnum As String, ByVal memo As String, ByVal amount As Decimal, ByVal acntCode As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loan_Accounts", _
                                      New SqlParameter("@loanNo", loanNo),
                                      New SqlParameter("@fk_Account", acnt_id))
            If rd.Read = True Then
                Dim acntType As String = rd.Item("AcctType").ToString
                If acntType = "Interest" Or acntType = "Service Fee" Or acntType = "Unearned Interest" Or acntType = "Penalty" Or acntType = "Others" Or acntType = "Credit Account" Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_AccountsPaymentInsertToTemp", _
                                      New SqlParameter("@LoanNo", loanNo),
                                      New SqlParameter("@DocNo", docnum),
                                      New SqlParameter("@fdDate", transdate),
                                      New SqlParameter("@amount", amount),
                                      New SqlParameter("@particulars", memo),
                                      New SqlParameter("@AcntType", acntType))
                End If
            Else
                Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_SelectLoanHeader", _
                                      New SqlParameter("@fcAccountCode", acntCode))
                If rd2.Read = True Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_AccountsPaymentInsertToTemp", _
                                      New SqlParameter("@LoanNo", loanNo),
                                      New SqlParameter("@DocNo", docnum),
                                      New SqlParameter("@fdDate", transdate),
                                      New SqlParameter("@amount", amount),
                                      New SqlParameter("@particulars", memo),
                                      New SqlParameter("@AcntType", "Balance"))
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString & vbCr & vbCr & "Subsidiary Entry")
        End Try
    End Sub
    Private Sub LoanAccountsDisbursement2(ByVal loanNo As String, ByVal acnt_id As String, ByVal transdate As Date, ByVal docnum As String, ByVal memo As String, ByVal amount As Decimal, ByVal acntCode As String, ByVal debit As Decimal, ByVal Posted As Boolean, ByVal ForRelease As Boolean)
        Dim mID As New Guid(pMemKey)

        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_Disbursement_New", _
                                          New SqlParameter("@LoanNo", loanNo),
                                          New SqlParameter("@DocNo", docnum),
                                          New SqlParameter("@fdDate", transdate),
                                          New SqlParameter("@debit", debit),
                                          New SqlParameter("@credit", amount),
                                          New SqlParameter("@particulars", memo),
                                          New SqlParameter("@Posted", Posted),
                                          New SqlParameter("@ForRelease", ForRelease),
                                          New SqlParameter("@empKey", pMemKey),
                                          New SqlParameter("@lineno", pRow),
                                          New SqlParameter("@acntRef", AccountRef),
                                          New SqlParameter("@memID", pID),
                                          New SqlParameter("@memName", pName),
                                          New SqlParameter("@acntID", acntID),
                                          New SqlParameter("@acntCode", acntCode))

    End Sub

    '--SAVE TO LEDGER, DELETE TEMP--'
    Private Sub UpdateLoanLedgerFormTemp(ByVal loanNo As String, ByVal docnum As String, ByVal checkstatus As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_UpdateLoanLedgerFromTempAccounts", _
                              New SqlParameter("@fcLoanNo", loanNo),
                              New SqlParameter("@DocNo", docnum),
                              New SqlParameter("@CheckStatus", checkstatus))
        Catch ex As Exception
            MsgBox("Error on Ledger update")
        End Try
    End Sub

    Private Sub UpdateLoanSubsidiary(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal particulars As String, ByVal dDebit As Decimal, ByVal dCredit As Decimal, ByVal dPayment As Decimal, ByVal ischeck As Boolean, ByVal checkstatus As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "_Insert_Released_Loan", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@Date", fdDate), _
                                      New SqlParameter("@DocNumber", docnum), _
                                      New SqlParameter("@Particulars", particulars), _
                                      New SqlParameter("@Debit", dDebit), _
                                      New SqlParameter("@Credit", dCredit), _
                                      New SqlParameter("@Payment", dPayment), _
                                      New SqlParameter("@isCheck", ischeck), _
                                      New SqlParameter("@CheckStatus", checkstatus))
        Catch ex As Exception

        End Try
    End Sub


    Private Sub PrepareSubsidiaryEntry(ByVal checkstatus As Boolean)
        For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim dDebit As Decimal = grdGenJournalDetails.Item("Debit", xRow).Value
            Dim dCredit As Decimal = grdGenJournalDetails.Item("Credit", xRow).Value
            Dim sMemo As String = txtMemo.Text
            Dim dtTransact As Date = dteGeneralJournal.Value.Date
            Dim AccountRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow)
            Dim SoaRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", xRow)

            If checkstatus = True Then
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 1, SoaRef)
                End If
            Else
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 0, SoaRef)
                End If
            End If
            AccountRef = ""
        Next
    End Sub

    Private Sub UpdateSubsidiary(ByVal AccountRef As String, ByVal RefNo As String, ByVal debit As Decimal, ByVal credit As Decimal, ByVal Memo As String, ByVal fdDate As Date, ByVal PayMethod As String, ByVal cStatus As Boolean, ByVal SoaRef As String)
        Dim co_id As New Guid(gCompanyID)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_Update", _
                                      New SqlParameter("@AccountRef", AccountRef), _
                                      New SqlParameter("@RefNo", RefNo), _
                                      New SqlParameter("@Debit", debit), _
                                      New SqlParameter("@Credit", credit), _
                                      New SqlParameter("@Description", Memo), _
                                      New SqlParameter("@fdDate", fdDate), _
                                      New SqlParameter("@PaymentMethod", PayMethod), _
                                      New SqlParameter("@CheckStatus", cStatus), _
                                      New SqlParameter("@SoaRef", SoaRef), _
                                      New SqlParameter("@co_id", co_id))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub UpdateSubsidiary_ForSoa(ByVal AccountRef As String, ByVal RefNo As String, ByVal debit As Decimal, ByVal credit As Decimal, ByVal Memo As String, ByVal fdDate As Date, ByVal PayMethod As String, ByVal cStatus As Boolean, ByVal SoaNo As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_Soa_Update", _
                                      New SqlParameter("@AccountRef", AccountRef), _
                                      New SqlParameter("@RefNo", RefNo), _
                                      New SqlParameter("@Debit", debit), _
                                      New SqlParameter("@Credit", credit), _
                                      New SqlParameter("@Description", Memo), _
                                      New SqlParameter("@fdDate", fdDate), _
                                      New SqlParameter("@PaymentMethod", PayMethod), _
                                      New SqlParameter("@CheckStatus", cStatus), _
                                      New SqlParameter("@SoaRef", SoaNo))
        Catch ex As Exception

        End Try

    End Sub
    Private Sub UpdateSubsidiary_ForSoaColumn(ByVal AccountRef As String, ByVal RefNo As String, ByVal cStatus As Boolean, ByVal SoaNo As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_SoaNo_Update", _
                                      New SqlParameter("@AccountRef", AccountRef), _
                                      New SqlParameter("@RefNo", RefNo), _
                                      New SqlParameter("@CheckStatus", cStatus), _
                                      New SqlParameter("@SoaRef", SoaNo))
        Catch ex As Exception

        End Try

    End Sub
#End Region

    '=================================================================================================
    '============================================ CANCEL =============================================
    '=================================================================================================

#Region "Cancel"
    Private Sub CANCELPrepareLoanSubsidiaryEntry(ByVal checkstatus As Boolean)
        For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
            ProgressBar1.Value += 1
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim dDebit As Decimal = 0.0
            Dim dCredit As Decimal = 0.0
            Dim sMemo As String = "**CANCELLED**"
            Dim dtTransact As Date = dteGeneralJournal.Value.Date
            Dim LoanRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cLoanRef", xRow)
            Dim acntID As String = grdGenJournalDetails.Item("cAccounts", xRow).Value
            Dim acntCode As String = grdGenJournalDetails.Item("cAccountCode", xRow).Value
            Dim AccountRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow)
            Dim SoaRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", xRow)
            If checkstatus = True Then
                If LoanRef <> "" Then
                    Call LoanAccountsDisbursement(LoanRef, acntID, dtTransact, journalNo, sMemo, dCredit, acntCode)
                End If

                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 1, SoaRef)
                End If
            Else
                If LoanRef <> "" Then
                    CANCELLoanSubsidiary(LoanRef, dtTransact, journalNo, sMemo, 0)
                End If

                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 0, SoaRef)
                End If
            End If
            LoanRef = ""
            AccountRef = ""
        Next

    End Sub


    Private Sub CANCELPrepareLoanSubsidiaryEntry_Payment(ByVal checkstatus As Boolean)
        For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
            ProgressBar1.Value += 1
            Dim journalNo As String = txtGeneralJournalNo.Text
            Dim dDebit As Decimal = 0.0
            Dim dCredit As Decimal = 0.0
            Dim sMemo As String = "**CANCELLED**"
            Dim dtTransact As Date = dteGeneralJournal.Value.Date
            Dim LoanRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cLoanRef", xRow)
            Dim AcntID As String = grdGenJournalDetails.Item("cAccounts", xRow).Value.ToString
            Dim AcntCode As String = grdGenJournalDetails.Item("cAccountCode", xRow).Value.ToString
            Dim AccountRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow)
            Dim SoaRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", xRow)
            If checkstatus = True Then
                '----For Loan
                If LoanRef <> "" Then
                    Call LoanAccountsDisbursement(LoanRef, AcntID, dtTransact, journalNo, sMemo, dCredit, AcntCode)
                End If

                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 1, SoaRef)
                End If
            Else
                If LoanRef <> "" Then
                    CANCELLoanSubsidiary(LoanRef, dtTransact, journalNo, sMemo, 0)
                End If
                '----For SL
                If AccountRef <> "" Then
                    Call UpdateSubsidiary(AccountRef, journalNo, dDebit, dCredit, sMemo, dtTransact, PaymentMethod(), 0, SoaRef)
                End If

            End If
            LoanRef = ""
            AccountRef = ""
        Next
    End Sub

    Private Sub CANCELLoanSubsidiary(ByVal loanref As String, ByVal fdDate As Date, ByVal docnum As String, ByVal particulars As String, ByVal checkstatus As Boolean)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Loans_InsertCancelledEntry", _
                                      New SqlParameter("@LoanNo", loanref), _
                                      New SqlParameter("@Date", fdDate), _
                                      New SqlParameter("@DocNumber", docnum), _
                                      New SqlParameter("@Particulars", particulars), _
                                      New SqlParameter("@CheckStatus", checkstatus))
        Catch ex As Exception

        End Try
    End Sub

#End Region
    Private Sub CheckNegativeBalance()
        Try
            For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
                Dim dDebit As Decimal = grdGenJournalDetails.Item("Debit", xRow).Value
                Dim dCredit As Decimal = grdGenJournalDetails.Item("Credit", xRow).Value
                Dim sNameID As String = grdGenJournalDetails.Item("cMemID", xRow).Value
                'Dim LoanRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cLoanRef", xRow)
                Dim AccountRef As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", xRow)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub chkPosted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPosted.Click
        'ProgressBar1.Value = 0
        Dim DocType As String = cboDoctype.Text
        Dim TotalAmount As Double
        If chkPosted.Checked = True Then
            Dim sSQLCmdDeleteTemp As String = "delete from tbl_Temp_PostingError"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmdDeleteTemp)
            If CheckDateRange(dteGeneralJournal.Value) = True Then
                Call SavePost()
            Else
                MsgBox("Date is not inside current Accounting Period" + vbCr + "Invalid Date.", MsgBoxStyle.Critical, "Transaction Entries")
                Exit Sub
            End If
            If SaveInButton = True Then
                Select Case DocType
                    Case "RECEIPTS"
                        TotalAmount = (txttotal.Text * -1)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text < "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = TotalAmount
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = "0.00"
                                    Call DefaultAccount_Save(TotalAmount)
                                End If

                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If

                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                'ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            'ProgressBar1.Visible = False
                        End If

                    Case "DEPOSIT SLIP"
                        TotalAmount = (txttotal.Text * -1)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text < "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = TotalAmount
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = "0.00"
                                    Call DefaultAccount_Save(TotalAmount)
                                End If
                                'If bLoanForRelease = True Then
                                '    Call PrepareLoanSubsidiaryEntry(1)
                                'Else
                                '    Call PrepareLoanSubsidiaryEntry_Payment(1)
                                'End If
                                'Call PrepareLoanSubsidiaryEntry(1)
                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If
                                'MsgBox("Posting Successful!", MsgBoxStyle.Information)
                                'AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Posted " & txtGeneralJournalNo.Text)
                                'grdGenJournalDetails.Refresh()
                                'grdGenJournalDetails.ClearSelection()
                                'grdGenJournalDetails.CurrentCell = Nothing
                                'Call LoadEntryDetails()
                                'Call CheckStatus()
                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                            End Try
                        Else
                            chkPosted.Checked = False
                            chkCancelled.Checked = False
                        End If

                    Case "WITHDRAWAL SLIP"
                        Call PrepareCheckEntry()
                        Call UpdateCheckMaster()
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If
                                'Call PrepareSubsidiaryEntry(1)
                                'If bLoanForRelease = True Then
                                '    Call PrepareLoanSubsidiaryEntry(1)
                                'Else
                                '    Call PrepareLoanSubsidiaryEntry_Payment(1)
                                'Call PrepareLoanSubsidiaryEntry(1)
                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If
                                'MsgBox("Posting Successful!", MsgBoxStyle.Information)
                                'AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Posted " & txtGeneralJournalNo.Text)
                                'grdGenJournalDetails.Refresh()
                                'grdGenJournalDetails.ClearSelection()
                                'grdGenJournalDetails.CurrentCell = Nothing
                                'Call LoadEntryDetails()
                                'Call CheckStatus()
                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                            End Try
                        Else
                            chkPosted.Checked = False
                            chkCancelled.Checked = False
                        End If
                    Case "SALES INVOICE"
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If

                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If

                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                'ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            'ProgressBar1.Visible = False
                        End If
                    Case "CASH VOUCHER"
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If

                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If

                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                'ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            'ProgressBar1.Visible = False
                        End If
                    Case "JOURNAL VOUCHER"
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If

                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If

                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                'ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            'ProgressBar1.Visible = False
                        End If
                    Case "ACCOUNTS PAYABLE VOUCHER"
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If

                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If

                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                'ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            'ProgressBar1.Visible = False
                        End If
                    Case "CREDIT MEMO"
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If

                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If

                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                'ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            'ProgressBar1.Visible = False
                        End If
                    Case "CHEK VOUCHER"
                        Call PrepareCheckEntry()
                        Call UpdateCheckMaster()
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If
                                If bgwPosting.IsBusy = False Then
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                Else
                                    bgwPosting.CancelAsync()
                                    ProgressBar1.Visible = True
                                    bgwPosting.WorkerReportsProgress = True
                                    bgwPosting.RunWorkerAsync()
                                End If
                            Catch ex As Exception
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                            End Try
                        Else
                            chkPosted.Checked = False
                            chkCancelled.Checked = False
                        End If
                    Case Else
                        If txttotal.Text <> 0.0 Then
                            Call PrepareCheckEntry()
                            'Call CheckNumberInsert()
                            Call UpdateCheckMaster()
                            If txttotal.Text = "0.00" Then

                                If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                                    Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                                    Try
                                        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                                        If bgwPosting.IsBusy = False Then
                                            ProgressBar1.Visible = True
                                            bgwPosting.WorkerReportsProgress = True
                                            bgwPosting.RunWorkerAsync()
                                        Else
                                            bgwPosting.CancelAsync()
                                            ProgressBar1.Visible = True
                                            bgwPosting.WorkerReportsProgress = True
                                            bgwPosting.RunWorkerAsync()
                                        End If
                                    Catch ex As Exception
                                        chkPosted.Checked = False
                                        chkCancelled.Checked = False
                                    End Try
                                Else
                                    chkPosted.Checked = False
                                    chkCancelled.Checked = False
                                End If
                            Else
                                MsgBox("Entry is not balanced", MsgBoxStyle.Critical, "Transaction Posting")
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                Exit Sub
                            End If
                        Else
                            If MsgBox("Do you want to POST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                                Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                                Try
                                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                    ' Call PrepareSubsidiaryEntry(1)
                                    'If bLoanForRelease = True Then
                                    '    Call PrepareLoanSubsidiaryEntry(1)
                                    'Else
                                    '    Call PrepareLoanSubsidiaryEntry_Payment(1)
                                    'End If

                                    'Call PrepareLoanSubsidiaryEntry(1)
                                    If bgwPosting.IsBusy = False Then
                                        ProgressBar1.Visible = True
                                        bgwPosting.WorkerReportsProgress = True
                                        bgwPosting.RunWorkerAsync()
                                    Else
                                        bgwPosting.CancelAsync()
                                        ProgressBar1.Visible = True
                                        bgwPosting.WorkerReportsProgress = True
                                        bgwPosting.RunWorkerAsync()
                                    End If

                                    'MsgBox("Posting Successful!", MsgBoxStyle.Information)
                                    'AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Posted " & txtGeneralJournalNo.Text)
                                    'grdGenJournalDetails.ClearSelection()
                                    'grdGenJournalDetails.CurrentCell = Nothing
                                    'Call LoadEntryDetails()
                                    'Call CheckStatus()
                                Catch ex As Exception
                                    chkPosted.Checked = False
                                    chkCancelled.Checked = False
                                End Try
                            Else
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                            End If
                        End If
                End Select
            Else
                MsgBox("Save Document first.", MsgBoxStyle.Critical)
                chkPosted.Checked = False
                chkCancelled.Checked = False
            End If
        Else
            If MsgBox("Do you want to UNPOST this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                Dim sSQLCmd As String = "update tJVEntry Set fdPosted='0' where fxKeyJVNo='" & GetJVKeyID & "' "
                AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Unpost " & txtGeneralJournalNo.Text)
                Try
                    'ProgressBar1.Visible = True
                    'ProgressBar1.Maximum = 0
                    'For v As Integer = 0 To grdGenJournalDetails.RowCount - 1
                    '    ProgressBar1.Maximum += 1
                    'Next
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                    Dim sSQLCmdDeleteTemp As String = "delete from tbl_Temp_PostingError"
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmdDeleteTemp)
                    'Call PrepareSubsidiaryEntry(0)
                    'If bLoanForRelease = True Then
                    '    Call PrepareLoanSubsidiaryEntry(0)
                    'Else
                    '    Call PrepareLoanSubsidiaryEntry_Payment(0)
                    'End If
                    If bgwUnposting.IsBusy = False Then
                        ProgressBar1.Visible = True
                        bgwUnposting.WorkerReportsProgress = True
                        bgwUnposting.RunWorkerAsync()
                    Else
                        bgwUnposting.CancelAsync()
                        ProgressBar1.Visible = True
                        bgwUnposting.WorkerReportsProgress = True
                        bgwUnposting.RunWorkerAsync()
                    End If
                    'ProgressBar1.Visible = False
                Catch ex As Exception
                    MsgBox(ex.Message)
                    'ProgressBar1.Visible = False
                End Try
            Else
                chkPosted.Checked = True
                'ProgressBar1.Visible = False
            End If
        End If
        DefaultAccount_Search()
    End Sub

    Private Sub chkCancelled_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCancelled.Click
        ProgressBar1.Value = 0
        Dim DocType As String = cboDoctype.Text
        Dim TotalAmount As Double
        If chkCancelled.Checked = True Then
            If CheckDateRange(dteGeneralJournal.Value) = True Then
                Call SavePost()
            Else
                MsgBox("Date is not inside current Accounting Period" + vbCr + "Invalid Date.", MsgBoxStyle.Critical, "Transaction Entries")
                Exit Sub
            End If
            If SaveInButton = True Then
                Select Case DocType
                    Case "RECEIPTS"
                        TotalAmount = (txttotal.Text * -1)
                        If MsgBox("Do you want to CANCEL this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                ProgressBar1.Visible = True
                                ProgressBar1.Maximum = 0
                                For v As Integer = 0 To grdGenJournalDetails.RowCount - 1
                                    ProgressBar1.Maximum += 1
                                Next
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text < "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = TotalAmount
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = "0.00"
                                    Call DefaultAccount_Save(TotalAmount)
                                End If
                                If bLoanForRelease = True Then
                                    Call CANCELPrepareLoanSubsidiaryEntry(1)
                                Else
                                    Call CANCELPrepareLoanSubsidiaryEntry_Payment(1)
                                End If
                                MsgBox("Cancelled.", MsgBoxStyle.Information)
                                chkPosted.Checked = True
                                AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Cancelled " & txtGeneralJournalNo.Text)
                                grdGenJournalDetails.Refresh()
                                grdGenJournalDetails.ClearSelection()
                                grdGenJournalDetails.CurrentCell = Nothing
                                Call LoadEntryDetails()
                                'Call CheckStatus()
                                ProgressBar1.Visible = False
                            Catch ex As Exception
                                chkCancelled.Checked = False
                                chkPosted.Checked = False
                                ProgressBar1.Visible = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                            ProgressBar1.Visible = False
                        End If

                    Case "DEPOSIT SLIP"
                        TotalAmount = (txttotal.Text * -1)
                        If MsgBox("Do you want to CANCEL this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text < "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = TotalAmount
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = "0.00"
                                    Call DefaultAccount_Save(TotalAmount)
                                End If
                                'Call PrepareSubsidiaryEntry(1)
                                If bLoanForRelease = True Then
                                    Call CANCELPrepareLoanSubsidiaryEntry(1)
                                Else
                                    Call CANCELPrepareLoanSubsidiaryEntry_Payment(1)
                                End If

                                MsgBox("Cancelled.", MsgBoxStyle.Information)
                                AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Cancelled " & txtGeneralJournalNo.Text)
                                grdGenJournalDetails.Refresh()
                                grdGenJournalDetails.ClearSelection()
                                grdGenJournalDetails.CurrentCell = Nothing
                                Call LoadEntryDetails()
                                'Call CheckStatus()
                            Catch ex As Exception
                                chkCancelled.Checked = False
                                chkPosted.Checked = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                        End If

                    Case "WITHDRAWAL SLIP"
                        Call PrepareCheckEntry()
                        'Call CheckNumberInsert()
                        Call UpdateCheckMaster()
                        TotalAmount = (txttotal.Text)
                        If MsgBox("Do you want to CANCEL this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                            Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                            Try
                                SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                If txttotal.Text > "0.00" Then
                                    DefaultAccount_Search()
                                    Dim lastRow As Integer = grdGenJournalDetails.RowCount - 1
                                    grdGenJournalDetails.Item("cAccountCode", lastRow).Value = ORcode
                                    grdGenJournalDetails.Item("cAccountTitle", lastRow).Value = ORAccount
                                    grdGenJournalDetails.Item("Debit", lastRow).Value = "0.00"
                                    grdGenJournalDetails.Item("Credit", lastRow).Value = TotalAmount
                                    Call DefaultAccount_Save(TotalAmount)
                                End If
                                'Call PrepareSubsidiaryEntry(1)
                                If bLoanForRelease = True Then
                                    Call CANCELPrepareLoanSubsidiaryEntry(1)
                                Else
                                    Call CANCELPrepareLoanSubsidiaryEntry_Payment(1)
                                End If
                                MsgBox("Cancelled.", MsgBoxStyle.Information)
                                AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Cancelled " & txtGeneralJournalNo.Text)
                                grdGenJournalDetails.Refresh()
                                grdGenJournalDetails.ClearSelection()
                                grdGenJournalDetails.CurrentCell = Nothing
                                Call LoadEntryDetails()
                                'Call CheckStatus()
                                Call UpdateCheckMaster()
                            Catch ex As Exception
                                chkCancelled.Checked = False
                                chkPosted.Checked = False
                            End Try
                        Else
                            chkCancelled.Checked = False
                            chkPosted.Checked = False
                        End If
                    Case Else
                        If txttotal.Text <> 0.0 Then
                            Call PrepareCheckEntry()
                            'Call CheckNumberInsert()
                            Call UpdateCheckMaster()
                            If txttotal.Text = "0.00" Then

                                If MsgBox("Do you want to CANCEL this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                                    Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                                    Try
                                        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                        'Call PrepareSubsidiaryEntry(1)
                                        If bLoanForRelease = True Then
                                            Call CANCELPrepareLoanSubsidiaryEntry(1)
                                        Else
                                            Call CANCELPrepareLoanSubsidiaryEntry_Payment(1)
                                        End If
                                        MsgBox("Cancelled.", MsgBoxStyle.Information)
                                        AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Cancelled " & txtGeneralJournalNo.Text)
                                        grdGenJournalDetails.ClearSelection()
                                        grdGenJournalDetails.CurrentCell = Nothing
                                        Call LoadEntryDetails()
                                        'Call CheckStatus()
                                        Call UpdateCheckMaster()
                                    Catch ex As Exception
                                        chkPosted.Checked = False
                                        chkCancelled.Checked = False
                                    End Try
                                Else
                                    chkPosted.Checked = False
                                    chkCancelled.Checked = False
                                End If
                            Else
                                MsgBox("Entry is not balanced", MsgBoxStyle.Critical)
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                                Exit Sub
                            End If
                        Else
                            If MsgBox("Do you want to CANCEL this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                                Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='1' where fxKeyJVNo='" & GetJVKeyID & "' "
                                Try
                                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                                    ' Call PrepareSubsidiaryEntry(1)
                                    If bLoanForRelease = True Then
                                        Call CANCELPrepareLoanSubsidiaryEntry(1)
                                    Else
                                        Call CANCELPrepareLoanSubsidiaryEntry_Payment(1)
                                    End If
                                    MsgBox("Cancelled.", MsgBoxStyle.Information)
                                    AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Cancelled " & txtGeneralJournalNo.Text)
                                    grdGenJournalDetails.ClearSelection()
                                    grdGenJournalDetails.CurrentCell = Nothing
                                    Call LoadEntryDetails()
                                    'Call CheckStatus()
                                Catch ex As Exception
                                    chkPosted.Checked = False
                                    chkCancelled.Checked = False
                                End Try
                            Else
                                chkPosted.Checked = False
                                chkCancelled.Checked = False
                            End If
                        End If
                End Select
            Else
                MsgBox("Save Document first.", MsgBoxStyle.Critical)
                chkPosted.Checked = False
                chkCancelled.Checked = False
            End If
        Else
            If MsgBox("Do you want to UNCANCEL this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRIES") = MsgBoxResult.Yes Then
                Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='0' where fxKeyJVNo='" & GetJVKeyID & "' "
                AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Uncancel " & txtGeneralJournalNo.Text)
                Try
                    ProgressBar1.Visible = True
                    ProgressBar1.Maximum = 0
                    For v As Integer = 0 To grdGenJournalDetails.RowCount - 1
                        ProgressBar1.Maximum += 1
                    Next
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                    'Call PrepareSubsidiaryEntry(0)
                    If bLoanForRelease = True Then
                        Call CANCELPrepareLoanSubsidiaryEntry(0)
                    Else
                        Call CANCELPrepareLoanSubsidiaryEntry_Payment(0)
                    End If
                    'Call CheckStatus()
                    Call UpdateCheckMaster()

                    ProgressBar1.Visible = False
                    chkPosted.Checked = False
                    chkPosted.Enabled = True
                    grdGenJournalDetails.Columns("Debit").ReadOnly = False
                    grdGenJournalDetails.Columns("Credit").ReadOnly = False
                    grdGenJournalDetails.Enabled = True
                    grdListofEntries.Enabled = True
                    txtMemo.ReadOnly = False
                    dteGeneralJournal.Enabled = True
                    btnSelectDocNum.Enabled = True
                    DeleteCheckToolStripMenuItem.Enabled = True
                    SaveToolStripMenuItem.Enabled = True
                    dtpDatePrepared.Enabled = True
                    chkCancelled.Enabled = True
                Catch ex As Exception
                    MsgBox(ex.Message)
                    ProgressBar1.Visible = False
                End Try
            Else
                chkCancelled.Checked = True
                chkPosted.Checked = True
                ProgressBar1.Visible = False
            End If
        End If
    End Sub

    Private Sub grdGenJournalDetails_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdGenJournalDetails.SelectionChanged
        If flag_cell_edited = True Then
            Select Case currentColumn
                Case 8
                    grdGenJournalDetails.CurrentCell = grdGenJournalDetails(currentColumn, currentRow)
                    grdGenJournalDetails("Credit", currentRow).Value = 0
                    flag_cell_edited = False
                Case 9
                    grdGenJournalDetails.CurrentCell = grdGenJournalDetails(currentColumn, currentRow)
                    grdGenJournalDetails("Debit", currentRow).Value = 0
                    flag_cell_edited = False
                Case Else
                    grdGenJournalDetails.CurrentCell = grdGenJournalDetails(currentColumn, currentRow)
                    flag_cell_edited = False
            End Select
            grdGenJournalDetails.CurrentCell = grdGenJournalDetails(currentColumn, currentRow)
            flag_cell_edited = False
        End If
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        If MsgBox("Are you sure you want to DELETE this LINE?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "DELETE LINE") = MsgBoxResult.Yes Then
            Call DeleteRow()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub DeleteRow()
        Try
            keyDetails = grdGenJournalDetails.Item("fxKey", grdGenJournalDetails.CurrentRow.Index).Value.ToString
        Catch ex As Exception

        End Try

        If chkPosted.Checked = True Then
            MsgBox("Unpost the Document First!", vbCritical, "Warning")
        Else
            Call DeleteSelectedRow()
            Call ClearWithdrawalInOtherForm()
            Call ComputeDebitAndCreditSumDetails()
            Try
                txtCode.Text = grdGenJournalDetails.Rows(0).Cells(0).Value.ToString()
                txtclientname.Text = grdGenJournalDetails.Rows(0).Cells(1).Value.ToString()
                Call ListCurrentBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString())
                Call ListAvailableBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString(), dteGeneralJournal.Text)
            Catch ex As Exception

            End Try
            MsgBox("LINE DELETED", MsgBoxStyle.Information, "TRANSACTION ENTRIES")
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        frmApprovedLoan.ShowDialog()
        If frmApprovedLoan.DialogResult = DialogResult.OK Then
            frmApprovedLoan.EntryDetails()
            frmApprovedLoan.AvoidDuplicate()
            If frmApprovedLoan.RecordedInAccounting = False Then
                FormatGeneralJournalDetailsGrid_OR()
                LoadEntryDetailsForLoanRelease()
            Else
                FormatGeneralJournalEntriesGrid()
                FormatGeneralJournalDetailsGrid_OR()
                LoadEntryDetails()
                LoadListofEntriesPosted()
                DeleteDocToolStripMenuItem.Enabled = True
            End If
            bLoanForRelease = True
            grdListofEntries.Visible = True
            grdListofEntries.Enabled = True
        End If
    End Sub

    Public Sub RemoveData()
        'On Error Resume Next
        If grdListofEntries.SelectedCells(0).Value = "" Then
            MsgBox("Theres no Record to Remove!", vbCritical)
        Else
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Check_Delete", _
                New SqlParameter("@FK_BankId", keyID),
                New SqlParameter("@FX_CheckNumber", BankID))
            grdListofEntries.Rows.Remove(grdListofEntries.CurrentRow)

        End If
    End Sub
    Private Sub EntryDetails()
        On Error Resume Next
        txtBank.Text = grdListofEntries.SelectedRows(0).Cells(1).Value.ToString()
        keyID = txtBank.Text
        txtcheck.Text = grdListofEntries.SelectedRows(0).Cells(2).Value.ToString()
        BankID = txtcheck.Text
    End Sub


    Private Sub AddCheckToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddCheckToolStripMenuItem.Click
        Try
            If grdGenJournalDetails.Rows(0).Cells(0).Value = "" Then
                MsgBox("Please enter a client before proceeding.", MsgBoxStyle.Information, "Add Check")
            Else
                grdListofEntries.Enabled = True
                Call FormatGeneralJournalEntriesGrid()
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString, vbInformation)
        End Try
    End Sub

    Private Sub ViewCheckToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewCheckToolStripMenuItem.Click
        frmCheckList.ShowDialog()
    End Sub

    Private Sub chkCancelled_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles chkCancelled.KeyDown
        If e.KeyCode = Keys.C Then
            chkCancelled.Checked = True
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub CloseF6ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseF6ToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        If CheckDateRange(dteGeneralJournal.Value) = True Then
            Call SaveEntry()
        Else
            MsgBox("Date is not inside current Accounting Period" + vbCr + "Invalid Date.", MsgBoxStyle.Critical, "Transaction Entries")
            Exit Sub
        End If
    End Sub

    Private Sub SaveEntry()
        'Dim i As Integer = grdGenJournalDetails.CurrentRow.Index
        'If frmClosingEntries.IsPeriodClosed(dteGeneralJournal.Value.Date, btnSaveNew) = False Then
        Dim Debit As Double
        Dim Credit As Double

        Dim i As Integer = grdGenJournalDetails.RowCount - 2
        If txtGeneralJournalNo.Text <> "" Then
            If grdGenJournalDetails.Rows(0).Cells(5).Value = "" Then
                MsgBox("No entry to save!", vbInformation, "Transaction Entry")
                Exit Sub
            Else
                Debit = grdGenJournalDetails.Item("Debit", i).Value.ToString
                Credit = grdGenJournalDetails.Item("Credit", i).Value.ToString
                If Debit = 0.0 And Credit = 0.0 Then
                    MsgBox("Theres a line has No Debit and Credit", vbInformation, "Transaction Entry")
                    Exit Sub
                Else
                    SaveInButton = True
                    Dim Doctype As String = cboDoctype.Text

                    Select Case Doctype
                        Case "CHECK VOUCHER"
                            Call UpdateCheckMaster()
                            Call RecordGeneralEntry()
                        Case "RECEIPTS"
                            Call ORCheckInsert()
                            Call RecordGeneralEntry()
                        Case "WITHDRAWAL SLIP"
                            Call UpdateCheckMaster()
                            Call RecordGeneralEntry()
                        Case "DEPOSIT SLIP"
                            Call ORCheckInsert()
                            Call RecordGeneralEntry()
                        Case Else
                            Call RecordGeneralEntry()
                    End Select
                    'SUCCESSFUL ENTRY
                    'isUpdate = True
                    If SaveInButton = True Then
                        If isUpdate = True Then
                            AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Update " & txtGeneralJournalNo.Text)
                            MsgBox("You have successfully saved the entry.", MsgBoxStyle.Information, "TRANSACTION ENTRY")
                            'Call FormatGeneralJournalDetailsGrid()
                            Call FormatGeneralJournalDetailsGrid_OR()
                            Call LoadEntryDetails()
                            If cboDoctype.Text = "RECEIPTS" Or cboDoctype.Text = "DEPOSIT SLIP" Then
                                Call ORCheckSearch()
                            Else
                                Call LoadListofEntriesPosted()
                            End If
                        Else
                            AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Add " & txtGeneralJournalNo.Text)
                            MsgBox("You have successfully entered an entry.", MsgBoxStyle.Information, "TRANSACTION ENTRY")
                            keyID = GetJVKeyID()
                            'Call FormatGeneralJournalDetailsGrid()
                            Call FormatGeneralJournalDetailsGrid_OR()
                            Call LoadEntryDetails()
                            'grdGenJournalDetails.Refresh()
                            If cboDoctype.Text = "RECEIPTS" Or cboDoctype.Text = "DEPOSIT SLIP" Then
                                Call ORCheckSearch()
                            Else
                                Call LoadListofEntriesPosted()
                            End If
                            SaveInButton = False
                        End If
                    End If
                End If
            End If
        Else
            MsgBox("No Document Number", MsgBoxStyle.Exclamation, "Transaction Entry")
        End If

    End Sub

    Private Sub SavePost()
        Dim Debit As Double
        Dim Credit As Double

        Dim i As Integer = grdGenJournalDetails.RowCount - 2
        If txtGeneralJournalNo.Text <> "" Then
            If grdGenJournalDetails.Rows(0).Cells(5).Value = "" Then
                MsgBox("No entry to save!", vbInformation, "Transaction Entry")
                Exit Sub
            Else
                Debit = grdGenJournalDetails.Item("Debit", i).Value.ToString
                Credit = grdGenJournalDetails.Item("Credit", i).Value.ToString
                If Debit = 0.0 And Credit = 0.0 Then
                    MsgBox("Theres a line has No Debit and Credit", vbInformation, "Transaction Entry")
                    Exit Sub
                Else
                    SaveInButton = True
                    Dim Doctype As String = cboDoctype.Text

                    Select Case Doctype
                        Case "CHECK VOUCHER"
                            Call UpdateCheckMaster()
                            Call RecordGeneralEntry()
                        Case "RECEIPTS"
                            Call ORCheckInsert()
                            Call RecordGeneralEntry()
                        Case "WITHDRAWAL SLIP"
                            Call UpdateCheckMaster()
                            Call RecordGeneralEntry()
                        Case "DEPOSIT SLIP"
                            Call ORCheckInsert()
                            Call RecordGeneralEntry()
                        Case Else
                            Call RecordGeneralEntry()
                    End Select
                    'SUCCESSFUL ENTRY
                    'isUpdate = True
                    If SaveInButton = True Then
                        If isUpdate = True Then
                            'AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Update " & txtGeneralJournalNo.Text)
                            'MsgBox("You have successfully saved the entry.", MsgBoxStyle.Information, "TRANSACTION ENTRY")
                            Call FormatGeneralJournalDetailsGrid_OR()
                            Call LoadEntryDetails()
                            If cboDoctype.Text = "RECEIPTS" Or cboDoctype.Text = "DEPOSIT SLIP" Then
                                Call ORCheckSearch()
                            Else
                                Call LoadListofEntriesPosted()
                            End If
                        Else
                            'AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Add " & txtGeneralJournalNo.Text)
                            'MsgBox("You have successfully entered an entry.", MsgBoxStyle.Information, "TRANSACTION ENTRY")
                            keyID = GetJVKeyID()
                            Call FormatGeneralJournalDetailsGrid_OR()
                            Call LoadEntryDetails()
                            'grdGenJournalDetails.Refresh()
                            If cboDoctype.Text = "RECEIPTS" Or cboDoctype.Text = "DEPOSIT SLIP" Then
                                Call ORCheckSearch()
                            Else
                                Call LoadListofEntriesPosted()
                            End If
                            SaveInButton = False
                        End If
                    End If
                End If
            End If
        Else
            MsgBox("No Document Number", MsgBoxStyle.Exclamation, "Transaction Entry")
        End If

    End Sub

    Private Sub ORCheckInsert()
        Try
            Dim rd As SqlDataReader
            Dim bank As String
            Dim no As String
            Dim row As Integer = grdListofEntries.RowCount - 2
            For xRow As Integer = 0 To grdListofEntries.RowCount - 2
                If row = xRow Then
                    'Dim Id As String = NormalizeValuesInDataGridView(grdListofEntries, "KeyID", xRow).ToString
                    Dim CheckNo As String = grdListofEntries.Rows(row).Cells("CheckNo").Value
                    Dim BankName As String = grdListofEntries.Rows(row).Cells("Bank").Value
                    Dim dDate As String = grdListofEntries.Rows(row).Cells("CheckDate").Value
                    Dim Amount As Double = grdListofEntries.Rows(row).Cells("checkAmount").Value
                    Dim Maker As String = grdListofEntries.Rows(row).Cells("Maker").Value
                    Dim Payee As String = grdListofEntries.Rows(row).Cells("Payee").Value
                    'rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "ClientBankMaster_CheckBankDuplicate",
                    '               New SqlParameter("@CheckNo", CheckNo),
                    '               New SqlParameter("@bankName", BankName))
                    'While rd.Read
                    '    bank = rd.Item("fcBankName")
                    '    no = rd.Item("fcCheckNo")
                    'End While
                    'If bank <> BankName And no <> CheckNo Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "ClientBankMaster_Insert",
                                                  New SqlParameter("@fkEntryNo", GetJVKeyID),
                                                  New SqlParameter("@fcBankName", BankName),
                                                  New SqlParameter("@fcCheckNo", CheckNo),
                                                  New SqlParameter("@fdDate", dDate),
                                                  New SqlParameter("@fnAmount", Amount),
                                                  New SqlParameter("@fcPayee", Payee),
                                                  New SqlParameter("@fcMaker", Maker))
                    'Else
                    '    Exit Sub
                    'End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub ORCheckSearch()
        'Try
        grdListofEntries.Rows.Clear()
        Dim rd As SqlDataReader
        Dim row As Integer
        rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "ClientBankMaster_Search",
                                 New SqlParameter("@fkEntryNo", GetJVKeyID))
        While rd.Read
            row = grdListofEntries.RowCount - 1
            For xRow As Integer = 0 To grdListofEntries.RowCount - 1
                If row = xRow Then
                    grdListofEntries.Rows.Add()
                    grdListofEntries.Item("Bank", row).Value = rd.Item("fcBankName")
                    grdListofEntries.Item("CheckNo", row).Value = rd.Item("fcCheckNo")
                    grdListofEntries.Item("CheckDate", row).Value = rd.Item("fdDate")
                    grdListofEntries.Item("checkAmount", row).Value = rd.Item("fnAmount")
                    grdListofEntries.Item("Maker", row).Value = rd.Item("fcMaker")
                    grdListofEntries.Item("Payee", row).Value = rd.Item("fcPayee")
                End If
            Next
        End While

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
    End Sub

    Private Sub NewFToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewFToolStripMenuItem.Click
        If Me.cboDoctype.Text = "" Then
            MessageBox.Show("Please Select Document Type", "NOTIFICATION", MessageBoxButtons.OK)
        Else
            'Select Case cboDoctype.Text
            'Case "RECEIPTS"
            lblPostingNotification.Visible = False
            grdGenJournalDetails.Enabled = True
            grdListofEntries.Enabled = True
            txtTotalDebit.Clear()
            txtTotalCredit.Clear()
            txtsoa.Clear()
            txttotal.Clear()
            Call ResetGridToNew()
            Call FormatGeneralJournalEntriesGrid()
            Call FormatGeneralJournalDetailsGrid_OR()
            Call LoadDocNum()
            Call ClearWithdrawalInOtherForm()
            SaveToolStripMenuItem.Enabled = True
            btnSelectDocNum.Enabled = True
            txtMemo.ReadOnly = False
            txtMemo.Text = ""
            'grdListofEntries.Visible = True
            isUpdate = False
            bLoanForRelease = False
            'Call GenerateKeyIDForDetailEntry(0)
            txtCreatedBy.Text = frmMain.currentUser.Text
            DefaultAccount_Search()
            '    Case Else
            'lblPostingNotification.Visible = False
            'grdGenJournalDetails.Enabled = True
            'grdListofEntries.Enabled = True
            'txtTotalDebit.Clear()
            'txtTotalCredit.Clear()
            'txtsoa.Clear()
            'txttotal.Clear()
            'Call ResetGridToNew()
            'Call FormatGeneralJournalEntriesGrid()
            'Call FormatGeneralJournalDetailsGrid()
            'Call LoadDocNum()
            'Call ClearWithdrawalInOtherForm()
            'SaveToolStripMenuItem.Enabled = True
            'btnSelectDocNum.Enabled = True
            'txtMemo.ReadOnly = False
            'txtMemo.Text = ""
            ''grdListofEntries.Visible = True
            'isUpdate = False
            'bLoanForRelease = False
            ''Call GenerateKeyIDForDetailEntry(0)
            'txtCreatedBy.Text = frmMain.currentUser.Text
            'DefaultAccount_Search()
            'End Select
        End If
    End Sub

    Private Sub SearchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchToolStripMenuItem.Click
        frmRecordedEntries.ShowDialog()
        If frmRecordedEntries.DialogResult = Windows.Forms.DialogResult.Yes Then
            Dim Cbodoctype As String = Me.cboDoctype.Text
            Select Case Cbodoctype
                Case "RECEIPTS"
                    'Call FormatGeneralJournalDetailsGrid()
                    Call FormatGeneralJournalDetailsGrid_OR()
                    Call FormatGeneralJournalEntriesGrid()
                    Call LoadEntryDetails()
                    Call ORCheckSearch()
                    DeleteDocToolStripMenuItem.Enabled = True
                Case "DEPOSIT SLIP"
                    'Call FormatGeneralJournalDetailsGrid()
                    Call FormatGeneralJournalDetailsGrid_OR()
                    Call FormatGeneralJournalEntriesGrid()
                    Call LoadEntryDetails()
                    Call ORCheckSearch()
                    DeleteDocToolStripMenuItem.Enabled = True
                Case "CHECK VOUCHER"
                    Call CheckIfForRelease()
                    'Call FormatGeneralJournalDetailsGrid()
                    Call FormatGeneralJournalDetailsGrid_OR()
                    Call FormatGeneralJournalEntriesGrid()
                    Call LoadEntryDetails()
                    Call LoadListofEntriesPosted()
                    DeleteDocToolStripMenuItem.Enabled = True
                Case "WITHDRAWAL SLIP"
                    Try
                        'Call FormatGeneralJournalDetailsGrid()
                        Call FormatGeneralJournalDetailsGrid_OR()
                        Call FormatGeneralJournalEntriesGrid()
                        Call LoadEntryDetails()
                        Call LoadListofEntriesPosted()

                        DeleteDocToolStripMenuItem.Enabled = True
                        If txtCode.Text <> "" Then
                            Call ListCurrentBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString())
                            Call ListAvailableBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString(), Now.Date)
                        End If
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                Case Else
                    ''Call FormatGeneralJournalDetailsGrid()
                    Call FormatGeneralJournalDetailsGrid_OR()
                    Call LoadEntryDetails()
                    DeleteDocToolStripMenuItem.Enabled = True
            End Select
            AuditTrail_Save("TRANSACTION ENTRIES (" & Cbodoctype & ")", "View " & txtGeneralJournalNo.Text)
            isUpdate = True
        End If
    End Sub

    Private Sub CheckIfForRelease()
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet

            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_CIMS_Loans_CheckIfForRelease",
                                          New SqlParameter("@filter", keyID))
            'MsgBox(ds.Tables(0).Rows.Count)
            If ds.Tables(0).Rows.Count <> 0 Then
                bLoanForRelease = True
            Else
                bLoanForRelease = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DeleteDocToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteDocToolStripMenuItem.Click
        If chkPosted.Checked = True Then
            MsgBox("Unpost the Document First!", vbCritical, "Warning")
        Else
            If MsgBox("Are you sure you want to DELETE this TRANSACTION?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "TRANSACTION ENTRY") = MsgBoxResult.Yes Then
                If isUpdate = True Then
                    AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Delete " & txtGeneralJournalNo.Text)
                    Dim sSQLCmd As String = "update mDocNumber Set fdDateUsed= Null where fcDocNumber='" & txtGeneralJournalNo.Text & "' "
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                    Call Update_SoaNumber_toNULL()
                    Call DeleteAllDetailedEntriesPerJV()
                    If cboDoctype.Text = "RECEIPTS" Then
                        Call DeleteEntryCheckOR()
                    Else
                        Call DeleteEntryCheck()
                    End If

                    Call PrepareForANewGeneralJournal()
                    Call CLearvalue()
                    MsgBox("DELETE successful.", MsgBoxStyle.Information)
                    Call LoadDocNum()
                    SaveToolStripMenuItem.Enabled = False

                End If
            Else
                'grdGenJournalDetails.Rows.Clear()
                Exit Sub
                'grdGenJournalDetails.DataSource = Nothing
                'Call PrepareForANewGeneralJournal()
                'Call UpdateDocDate_SOA(SoaNo)
            End If
        End If
    End Sub
    Private Sub Update_SoaNumber_toNULL()
        For xRow As Integer = 0 To grdGenJournalDetails.RowCount - 2
            Dim SoaNo As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cSOA", xRow)
            Call UpdateDocDate_ToNull_SOA(SoaNo)
        Next
    End Sub
    Private Sub DeleteEntryCheck()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Delete", _
                    New SqlParameter("@FK_TransID", GetJVKeyID()))
            grdListofEntries.Rows.Clear()
        Catch ex As Exception
            grdListofEntries.Rows.Clear()
        End Try
    End Sub

    Private Sub DeleteEntryCheckOR()
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumberOR_Delete", _
                    New SqlParameter("@transID", GetJVKeyID()))
            grdListofEntries.Rows.Clear()
        Catch ex As Exception
            grdListofEntries.Rows.Clear()
        End Try
    End Sub

    Private Sub DeleteCheckToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteCheckToolStripMenuItem.Click
        Dim i As Integer = grdListofEntries.CurrentRow.Index
        KeyListEntries = grdListofEntries.Item("CheckNo", i).Value.ToString
        Dim cBank As String = grdListofEntries.Item("Bank", i).Value.ToString

        If chkPosted.Checked = True Then
            MsgBox("Unpost the Document First!", vbCritical, "Warning")
            Exit Sub
        Else
            If cboDoctype.Text = "RECEIPTS" Or cboDoctype.Text = "DEPOSIT SLIP" Then
                Call DeleteORCheck(cBank, KeyListEntries, GetJVKeyID())
            Else
                Call NullCheck(grdListofEntries.Item("CheckNo", i).Value.ToString())
                Call ComputeDebitAndCreditSumDetails()
            End If
        End If
    End Sub

    Private Sub DeleteORCheck(ByVal bank As String, ByVal checkNo As String, ByVal transID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_ORCheck_Delete", _
                    New SqlParameter("@transID", transID),
                    New SqlParameter("@fcBankName", bank),
                    New SqlParameter("@fcCheckNo", checkNo))
            grdListofEntries.Rows.Remove(grdListofEntries.CurrentRow)
        Catch ex As Exception
            grdListofEntries.Rows.Remove(grdListofEntries.CurrentRow)
        End Try
    End Sub

    Private Sub NullCheck(ByVal ID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Delete1", _
                    New SqlParameter("@FX_CheckNumber", ID))
            grdListofEntries.Rows.Remove(grdListofEntries.CurrentRow)
        Catch ex As Exception
            grdListofEntries.Rows.Remove(grdListofEntries.CurrentRow)
        End Try
    End Sub
    Private Sub DeleteCheck()
        Try
            Dim cdelete As String
            Dim i As Integer = grdListofEntries.CurrentRow.Index
            cdelete = grdListofEntries.Item("CheckNo", i).Value.ToString
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DeletePerCheckEntriesPerJV", _
                            New SqlParameter("@FX_CheckNumber", cdelete))

        Catch ex As Exception

        End Try
    End Sub


    Private Sub PrintCheckToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintCheckToolStripMenuItem.Click
        Dim CheckNum As String = NormalizeValuesInDataGridView(grdListofEntries, "CheckNo", 0)
        frmCheckReport.txtcheckNo.Text = NormalizeValuesInDataGridView(grdListofEntries, "CheckNo", 0)
        If CheckNum = "" Then
            MsgBox("Theres no Check to Print", vbCritical, "Data not Found")
        ElseIf chkPosted.Checked = False Then
            MsgBox("Check not Approved!", vbInformation, "Print Check")
        Else
            frmCheckReport.ShowDialog()
            AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Print Check " & CheckNum & ", Document No. " & txtGeneralJournalNo.Text)
        End If
        frmCheckReport.txtcheckNo.Text = NormalizeValuesInDataGridView(grdListofEntries, "CheckNo", 0)
    End Sub
    Public Sub DefaultAccount_Search()
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "DefaultAccount_Search",
                                      New SqlParameter("@co_id", gCompanyID))
            While rd.Read
                ORacntId = rd.Item("acnt_id")
                ORcode = rd.Item("acnt_code")
                ORAccount = rd.Item("acnt_name")
            End While
            DefaultAccount_Balance(ORcode)
            rd.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub DefaultAccount_Balance(ByVal acnt_code As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_SelectCashBalance",
                                      New SqlParameter("@coid", gCompanyID()),
                                      New SqlParameter("@acnt_code", acnt_code))
            While rd.Read
                txtCashBalance.Text = Format(CDec(rd.Item("total")), "##,##0.00")
                If rd.Item("total") < 0 Then
                    txtCashBalance.ForeColor = Color.Red
                Else
                    txtCashBalance.ForeColor = Color.Black
                End If
            End While
            rd.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DefaultAccount_Save(ByVal Amount As Double)
        Select Case cboDoctype.Text
            Case "RECEIPTS"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "DEPOSIT SLIP"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "SALES INVOICE"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "CHECK VOUCHER"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "JOURNAL VOUCHER"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "ACCOUNTS PAYABLE VOUCHER"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "CREDIT MEMO"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "CASH VOUCHER"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Case "WITHDRAWAL SLIP"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "DefaultEntries_Save_W",
                                              New SqlParameter("@fxKeyJVNo", GetJVKeyID),
                                              New SqlParameter("@fiEntryNo", txtGeneralJournalNo.Text),
                                              New SqlParameter("@Acnt_Id", ORacntId),
                                              New SqlParameter("@debit", Amount),
                                              New SqlParameter("@dtDate", dteGeneralJournal.Value),
                                              New SqlParameter("@fcMemo", txtMemo.Text))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
        End Select
    End Sub
    Public Sub ListofEntriesReadOnly()
        grdListofEntries.Columns("Bank").ReadOnly = False
        grdListofEntries.Columns("CheckNo").ReadOnly = False
        grdListofEntries.Columns("CheckDate").ReadOnly = False
        grdListofEntries.Columns("checkAmount").ReadOnly = False
        grdListofEntries.Columns("Maker").ReadOnly = False
        grdListofEntries.Columns("Payee").ReadOnly = False
    End Sub

    Private Sub ViewVoucherF3ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewVoucherF3ToolStripMenuItem.Click

    End Sub

    Private Sub PrintVoucherToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintVoucherToolStripMenuItem.Click
        'If chkPosted.Checked = False Then
        '    MsgBox("Complete the entry first!", vbInformation, "Print Voucher")
        '    Exit Sub
        'Else
        Select Case cboDoctype.Text
            Case "CASH VOUCHER"
                frmVoucherReport.xTransaction = "CASH VOUCHER"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.xLoan = Me.NormalizeValuesInDataGridView(Me.grdGenJournalDetails, "cLoanRef", 0)
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "CHECK VOUCHER"
                'frmCheckVoucher.ShowDialog()
                'Exit Sub
                frmVoucherReport.xTransaction = "CHECK VOUCHER"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.xTransID = keyID
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "CREDIT MEMO"
                frmVoucherReport.xTransaction = "CREDIT MEMO"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "DEBIT MEMO"
                frmVoucherReport.xTransaction = "DEBIT MEMO"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "DEPOSIT SLIP"
                frmVoucherReport.xTransaction = "DEPOSIT SLIP"
                frmVoucherReport.docnum = Me.txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "JOURNAL VOUCHER"
                frmVoucherReport.xTransaction = "JOURNAL VOUCHER"
                frmVoucherReport.docnum = Me.txtGeneralJournalNo.Text
                frmVoucherReport.xLoan = Me.NormalizeValuesInDataGridView(Me.grdGenJournalDetails, "cLoanRef", 0)
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "RECEIPTS"
                frmVoucherReport.xTransaction = "RECEIPTS"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "PURCHASE INVOICE"
                frmVoucherReport.xTransaction = "PURCHASE INVOICE"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "PURCHASE ORDER"
                frmVoucherReport.xTransaction = "PURCHASE ORDER"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "PURCHASE RETURN"
                frmVoucherReport.xTransaction = "PURCHASE RETURN"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "SALES INVOICE"
                frmVoucherReport.xTransaction = "SALES INVOICE"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "SALES RETURN"
                frmVoucherReport.xTransaction = "SALES RETURN"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "WITHDRAWAL SLIP"
                frmVoucherReport.xTransaction = "WITHDRAWAL SLIP"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case "ACCOUNTS PAYABLE VOUCHER"
                frmVoucherReport.xTransaction = "ACCOUNTS PAYABLE VOUCHER"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
            Case Else
                frmVoucherReport.xTransaction = "DEFAULT"
                frmVoucherReport.docnum = txtGeneralJournalNo.Text
                frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
                frmVoucherReport.Show()
                Exit Sub
        End Select
        AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Print Voucher " & txtGeneralJournalNo.Text)
        'End If
    End Sub


    Private Sub DefaultAccountToolStripMenuItem_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DefaultAccountToolStripMenuItem.DoubleClick
        frmAccountList.ShowDialog()
    End Sub

    Private Sub cboDoctype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDoctype.TextChanged
        txtGeneralJournalNo.Text = ""
        LoadDocNum()
        DefaultAccount_Search()
        txtTotalDebit.Text = "0.00"
        txtTotalCredit.Text = "0.00"
        txttotal.Text = "0.00"
        txtsoa.Clear()
    End Sub

    Private Sub grdListofEntries_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListofEntries.CellClick
        Try
            KeyListEntries = grdListofEntries.Item("KeyID", e.RowIndex).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AddBankToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddBankToolStripMenuItem.Click
        frmClientBank.ShowDialog()
        frmClientBank.StartPosition = FormStartPosition.CenterScreen
    End Sub

    Private Sub txtCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCode.TextChanged
        Try
            If txtCode.Text = "" Then
                Exit Sub
                'Else
                '    If grdGenJournalDetails.Rows(0).Cells(3).Value = "" Then
                '        Exit Sub
            Else
                Call Listimage(txtCode.Text)
                'Call ListCurrentBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString())
                'Call ListAvailableBalance(txtCode.Text, grdGenJournalDetails.Rows(0).Cells(3).Value.ToString(), dteGeneralJournal.Text)
            End If
            'End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub Listimage(ByVal empno As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "View_Image",
                                          New SqlParameter("@fcEmployeeNo", empno))
            While rd.Read
                If rd("Fb_Picture") Is DBNull.Value = True Then
                    PBimage.Image = My.Resources.photo
                Else
                    Picdata = rd("Fb_Picture")
                End If
                If rd("Fb_Signature") Is DBNull.Value = True Then
                    PBsign.Image = My.Resources.signature
                Else
                    SigData = rd("Fb_Signature")
                End If

            End While
            rd.Close()
            PicData2 = Picdata
            SigData2 = SigData
            'load picture
            Dim ms As New MemoryStream(Picdata, 0, Picdata.Length)
            ms.Write(Picdata, 0, Picdata.Length)
            img = Image.FromStream(ms, True)
            PBimage.Image = img
            PBimage.SizeMode = PictureBoxSizeMode.StretchImage
            'load signature
            Dim mso As New MemoryStream(SigData, 0, SigData.Length)
            mso.Write(SigData, 0, SigData.Length)
            img = Image.FromStream(mso, True)
            PBsign.Image = img
            PBsign.SizeMode = PictureBoxSizeMode.StretchImage
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ListCurrentBalance(ByVal empno As String, ByVal Refno As String)
        Try

            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "SavingsLedger_CurrentBal",
                                          New SqlParameter("@EmployeeNo", empno), _
                                           New SqlParameter("@RefNo", Refno))
            While rd.Read
                Dim cBal As Double = rd.Item("balance")
                txtCurrent.Text = Format(cBal, "N")
            End While

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ListAvailableBalance(ByVal empno As String, ByVal Refno As String, ByVal ndate As Date)
        Try

            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "SavingsLedger_AvailableBalance",
                                          New SqlParameter("@EmployeeNo", empno), _
                                           New SqlParameter("@RefNo", Refno), _
                                           New SqlParameter("@Datenow", ndate))
            While rd.Read
                Dim aBal As Decimal = rd.Item("balance")
                txtAvailable.Text = Format(aBal, "N")
            End While
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        Dim Accountref As String = NormalizeValuesInDataGridView(grdGenJournalDetails, "cAccountRef", 0)
        If Accountref = "" Then
            MsgBox("No data in Savings Ledger!", vbInformation, "Savings Ledger")
        Else
            If txtCode.Text <> "" Then
                AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "View credit account(s) of " & txtCode.Text & " | " & txtclientname.Text)
            End If

            frmSavingsLedger.ShowDialog()
            frmSavingsLedger.StartPosition = FormStartPosition.CenterScreen
        End If
    End Sub

    Private Sub txtAvailable_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAvailable.TextChanged
        Format(CDec(Val(txtAvailable.Text)), "##,##0.00")
    End Sub

    Private Sub OR_getTotalAmount()
        ORtotalAmount = 0
        totalDebit = 0
        For i As Integer = 0 To grdListofEntries.RowCount - 1
            Dim amount As Double = grdListofEntries.Rows(i).Cells("checkAmount").Value
            ORtotalAmount = Val(ORtotalAmount) + Val(amount)
        Next
    End Sub

    Private Sub grdListofEntries_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdListofEntries.LostFocus
        'grdListofEntries.ClearSelection()
    End Sub

    Private Sub LinkLabel4_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        frmApprovedPayroll.ShowDialog()
        If frmApprovedPayroll.DialogResult = DialogResult.OK Then
            frmApprovedPayroll.EntryDetails()
            frmApprovedPayroll.AvoidDuplicate_Soa()
            'txtsoa.Text = frmApprovedPayroll.dgvApprovedLoan.Rows(0).Cells(5).Value.ToString()
            If frmApprovedPayroll.RecordedInAccounting = False Then
                FormatGeneralJournalDetailsGrid_OR()
                LoadEntryDetailsForPayroll()
            Else
                'FormatGeneralJournalEntriesGrid()
                FormatGeneralJournalDetailsGrid_OR()
                LoadEntryDetails()
                'LoadListofEntriesPosted()
                DeleteDocToolStripMenuItem.Enabled = True
            End If
            bLoanForRelease = True
            grdListofEntries.Visible = True
            grdListofEntries.Enabled = True
        End If
    End Sub
    Public Sub LoadEntryDetailsForPayroll()
        Try
            grdGenJournalDetails.DataSource = Nothing
            grdGenJournalDetails.Rows.Clear()

            Dim mycon As New Clsappconfiguration
            'Dim ds As DataSet
            Dim co_id As New Guid(gCompanyID)
            Dim rd = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "AprovePayrollTemplate",
                                                                New SqlParameter("@JVKeyID", keyID), _
                                                                New SqlParameter("@co_id", co_id))

            Try
                Dim row As Integer
                If rd.HasRows = True Then
                    While rd.Read
                        row = grdGenJournalDetails.RowCount - 1
                        txtGeneralJournalNo.Text = rd.Item("EntryNo").ToString
                        dteGeneralJournal.Value = rd.Item("fdTransDate").ToString
                        chkPosted.Checked = rd.Item("fdPosted").ToString
                        chkCancelled.Checked = rd.Item("fbCancelled").ToString
                        txtCreatedBy.Text = rd.Item("fuCreatedBy").ToString
                        txtMemo.Text = rd.Item("cMemo").ToString
                        cboDoctype.Text = rd.Item("fcDocName").ToString
                        '-------for withdrawal-------'
                        txtCode.Text = rd.Item("cID").ToString
                        txtclientname.Text = rd.Item("cName").ToString
                        GetJVKeyID() = rd.Item("fxKeyJVNo").ToString

                        AddDetailItem(rd.Item("cID").ToString, rd.Item("cName").ToString, rd.Item("cLoanRef").ToString, rd.Item("cAccountRef").ToString,
                               rd.Item("fcSAno").ToString, rd.Item("cAccounts").ToString, rd.Item("cAccountCode").ToString, rd.Item("cAccountTitle").ToString,
                                rd.Item("Debit"), rd.Item("Credit"), rd.Item("cMemID").ToString, rd.Item("fxKey").ToString)

                    End While
                    rd.Close()
                    isUpdate = True
                Else
                    Dim rdH As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "AprovePayrollTemplate_HeaderOnly",
                                                                                    New SqlParameter("@JVKeyID", keyID), _
                                                                                    New SqlParameter("co_id", co_id))
                    Try
                        Dim rowH As Integer
                        While rdH.Read
                            rowH = grdGenJournalDetails.RowCount - 1
                            txtGeneralJournalNo.Text = rdH.Item("EntryNo").ToString
                            dteGeneralJournal.Value = rdH.Item("fdTransDate").ToString
                            chkPosted.Checked = rdH.Item("fdPosted").ToString
                            chkCancelled.Checked = rdH.Item("fbCancelled").ToString
                            txtCreatedBy.Text = rdH.Item("fuCreatedBy").ToString
                            txtMemo.Text = rdH.Item("cMemo").ToString
                            cboDoctype.Text = rdH.Item("fcDocName").ToString
                            GetJVKeyID() = rdH.Item("fxKeyJVNo").ToString
                        End While
                        rdH.Close()
                    Catch
                        MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End Try
                    '---CHECK STATUS---'
                    lblNotif.Visible = False
                    Call CheckStatus()
                    'grdGenJournalDetails.Enabled = True
                    'grdListofEntries.Enabled = True
                    'txtMemo.ReadOnly = False
                    dteGeneralJournal.Enabled = True
                    btnSelectDocNum.Enabled = True
                    DeleteDocToolStripMenuItem.Enabled = True

                    mycon.sqlconn.Close()
                    isUpdate = True
                    Call GenerateKeyIDForDetailEntry(0)
                End If
                Call ComputeDebitAndCreditSumDetails()
            Catch
                MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
            grdGenJournalDetails.Rows(0).Cells(0).Selected = False
            lblNotif.Visible = False
            Call CheckStatus()
            mycon.sqlconn.Close()
            isUpdate = True

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub RecurringEntryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecurringEntryToolStripMenuItem.Click
        Dim ListName As String
        frmRecurringEntryList.ShowDialog()
        If frmRecurringEntryList.DialogResult = Windows.Forms.DialogResult.Yes Then
            ListName = frmRecurringEntryList.listDocNumber.SelectedItems.Item(0).Text
            SelectRecurringAccounts(ListName)
        Else
            Exit Sub
        End If
    End Sub
    Private Sub SelectRecurringAccounts(ByVal ListName As String)
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_RecurringEntryMaster_List",
                                                                            New SqlParameter("@ListName", ListName),
                                                                            New SqlParameter("@coid", gCompanyID()))
        While rd.Read = True
            AddDetailItem(rd.Item("memID").ToString, rd.Item("memName").ToString, rd.Item("LoanRef").ToString, rd.Item("AcntRef").ToString, "", rd.Item("acntID").ToString, rd.Item("acntCode").ToString, rd.Item("acntTitle").ToString, Format(CDec(rd.Item("debit")), "##,##0.00"), Format(CDec(rd.Item("credit")), "##,##0.00"), rd.Item("memKey").ToString, "")
        End While
    End Sub

    Private Sub grdGenJournalDetails_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellContentClick

    End Sub

    Private Sub bgwPosting_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwPosting.DoWork
        Call PrepareLoanSubsidiaryEntry(GetchkPostingValue)
    End Sub

    Private Sub bgwUnposting_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwUnposting.DoWork
        Call PrepareLoanSubsidiaryEntry(0)
    End Sub

    Private Sub lblStatus_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblStatus.TextChanged

    End Sub

    Private Sub bgwPosting_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwPosting.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
        Me.lblStatus.Text = e.ProgressPercentage & "%" & " ----Posting line " & pRow & ", " & pID & " | " & pName & " | " & LoanRef & " | " & AccountRef & " | " & acntCode & " | " & acntTitle
    End Sub

    Private Sub bgwPosting_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwPosting.RunWorkerCompleted
        MsgBox("Posting Successful!", MsgBoxStyle.Information)
        AuditTrail_Save("TRANSACTION ENTRIES (" & cboDoctype.Text & ")", "Posted " & txtGeneralJournalNo.Text)
        grdGenJournalDetails.ClearSelection()
        grdGenJournalDetails.CurrentCell = Nothing
        Call LoadEntryDetails()
        Call CheckStatus()
        lblStatus.Text = ""
        ProgressBar1.Visible = False
        PostingError()
    End Sub
    Private Sub PostingError()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_TempPosting_Select")
        If rd.Read = True Then
            frmPostingError.Show()
            Exit Sub
        Else
            Exit Sub
        End If
    End Sub

    Private Sub bgwUnposting_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwUnposting.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
        Me.lblStatus.Text = e.ProgressPercentage & "%" & " ----Unposting line " & pRow & ", " & pID & " | " & pName & " | " & LoanRef & " | " & AccountRef & " | " & acntCode & " | " & acntTitle
    End Sub

    Private Sub bgwUnposting_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwUnposting.RunWorkerCompleted
        Call MakeItEnabled()
        lblStatus.Text = ""
        ProgressBar1.Visible = False
    End Sub

    Private Sub bgwload_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwload.DoWork
    End Sub

    Private Sub bgwload_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwload.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
        'Me.lblStatus.Text = e.ProgressPercentage & "%" & " ----Posting line " & pRow & ", " & pID & " | " & pName & " | " & LoanRef & " | " & AccountRef & " | " & acntCode & " | " & acntTitle
    End Sub

    Private Sub bgwload_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwload.RunWorkerCompleted
        ProgressBar1.Visible = False
    End Sub

    Private Sub grdGenJournalDetails_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles grdGenJournalDetails.Layout

    End Sub

    Private Sub DefaultAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DefaultAccountToolStripMenuItem.Click

    End Sub

    Private Sub grdGenJournalDetails_CellMouseEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellMouseEnter
        For Each drow As DataGridViewRow In grdGenJournalDetails.Rows
            Dim dgvCell As DataGridViewCell
            dgvCell = drow.Cells("cAccountTitle")


            Dim dgvCell2 As DataGridViewCell
            dgvCell2 = drow.Cells("cAccounts")

            Dim guid As Guid
            If dgvCell2.Value <> Nothing Or dgvCell2.Value <> "" Then
                guid = New Guid(dgvCell2.Value.ToString())

                Dim rdH As SqlDataReader = SqlHelper.ExecuteReader(cs, CommandType.StoredProcedure, "[spu_SelectCashBalance]",
                                                                                    New SqlParameter("@acnt_code", guid))
                Try
                    While rdH.Read
                        dgvCell.ToolTipText = FormatNumber(rdH.Item("total").ToString, 2)
                    End While
                    rdH.Close()
                Catch
                    'MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End Try
            End If
        Next
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)
        frmAccountSubsidiaryFilter.Show()
    End Sub
End Class