﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmSubsidiaryAccountFilter

    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmSubsidiaryAccountFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadSubsidiaryAccounts()
        txtSearch.Text = ""
        Me.ActiveControl = txtSearch
    End Sub

    Private Sub loadSubsidiaryAccounts()
        Dim mycon As New Clsappconfiguration
        Dim co_id As New Guid(gCompanyID)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_CIMS_AccountRegister_List", _
                                      New SqlParameter("@co_id", co_id))
        grdSAList.DataSource = ds.Tables(0)

        With grdSAList
            .Columns("fxKeyAccountRegister").Visible = False
            .Columns("fcDocNumber").Width = 100
            .Columns("fcDocNumber").ReadOnly = True
            .Columns("fcDocNumber").HeaderText = "Account Ref."
            .Columns("fcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcEmployeeNo").Width = 100
            .Columns("fcEmployeeNo").ReadOnly = True
            .Columns("fcEmployeeNo").HeaderText = "ID"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcFullname").Width = 250
            .Columns("fcFullname").ReadOnly = True
            .Columns("fcFullname").HeaderText = "Name"
            .Columns("fcFullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcAccountName").Width = 290
            .Columns("fcAccountName").ReadOnly = True
            .Columns("fcAccountName").HeaderText = "Account Title"
            .Columns("fcAccountName").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fxKey_Account").Visible = False
            .Columns("pk_Employee").Visible = False
            .Columns("fcAccountCode").ReadOnly = True
            .Columns("fcAccountCode").HeaderText = "Code"
            .Columns("fcAccountCode").Width = 70
            .Columns("fcAccountCode").SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        'Dim mycon As New Clsappconfiguration
        'Dim rd As SqlDataReader
        'rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "spu_CIMS_AccountRegister_List")
        'Try

        '    While rd.Read
        '        Dim row As String() = New String() {rd.Item("fxKeyAccountRegister").ToString, rd.Item("fcDocNumber").ToString, rd.Item("fcEmployeeNo").ToString, rd.Item("fcFullname").ToString, rd.Item("fcAccountCode").ToString, rd.Item("fcAccountName").ToString, rd.Item("fxKey_Account").ToString, rd.Item("pk_Employee").ToString}
        '        grdSAList.Rows.Add(row)
        '    End While
        '    rd.Close()
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub SelectedColumnVAR()

        Select Case xModule
            Case "Entry"
                Try
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                    'txtSearch.Text = ""
                Catch ex As Exception

                End Try
            Case "Forward"
                Try
                    Dim row As String() = New String() {grdSAList.SelectedRows(0).Cells(2).Value.ToString(), grdSAList.SelectedRows(0).Cells(3).Value.ToString(), grdSAList.SelectedRows(0).Cells(1).Value.ToString(), grdSAList.SelectedRows(0).Cells(4).Value.ToString(), grdSAList.SelectedRows(0).Cells(5).Value.ToString(), Format(CDec(0), "##,##0.00"), Format(CDec(0), "##,##0.00")}
                    frmForwardBalance.grdList.Rows.Add(row)
                    Me.Close()
                    'txtSearch.Text = ""
                Catch ex As Exception

                End Try
        End Select
    End Sub

    Private Sub grdSAList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdSAList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Call SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
                txtSearch.Text = ""
        End Select
        'If e.KeyCode = Keys.Enter Then
        '    Call SelectedColumnVAR()
        'End If

        'If e.KeyCode = Keys.Escape Then
        '    Me.Close()
        '    txtSearch.Text = ""
        'End If
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Call SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
                txtSearch.Text = ""
        End Select
        'If e.KeyCode = Keys.Enter Then
        '    Call SelectedColumnVAR()
        'End If

        'If e.KeyCode = Keys.Escape Then
        '    Me.Close()
        '    txtSearch.Text = ""
        'End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call loadSubsidiaryAccountsFilter()
        'Call SelectedColumnVAR()
        If txtSearch.Text = "" Then
            Call loadSubsidiaryAccounts()
        End If
    End Sub

    Private Sub loadSubsidiaryAccountsFilter()
        Dim mycon As New Clsappconfiguration
        Dim co_id As New Guid(gCompanyID)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_CIMS_AccountRegister_Filter",
                                                        New SqlParameter("@filter", txtSearch.Text), _
                                                        New SqlParameter("@co_id", co_id))
        grdSAList.DataSource = ds.Tables(0)

        With grdSAList
            .Columns("fxKeyAccountRegister").Visible = False
            .Columns("fcDocNumber").Width = 100
            .Columns("fcDocNumber").ReadOnly = True
            .Columns("fcDocNumber").HeaderText = "Account Ref."
            .Columns("fcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcEmployeeNo").Width = 100
            .Columns("fcEmployeeNo").ReadOnly = True
            .Columns("fcEmployeeNo").HeaderText = "ID"
            .Columns("fcEmployeeNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcFullname").Width = 250
            .Columns("fcFullname").ReadOnly = True
            .Columns("fcFullname").HeaderText = "Name"
            .Columns("fcFullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcAccountName").Width = 290
            .Columns("fcAccountName").ReadOnly = True
            .Columns("fcAccountName").HeaderText = "Account Title"
            .Columns("fcAccountName").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fxKey_Account").Visible = False
            .Columns("pk_Employee").Visible = False
            .Columns("fcAccountCode").ReadOnly = True
            .Columns("fcAccountCode").HeaderText = "Code"
            .Columns("fcAccountCode").Width = 70
            .Columns("fcAccountCode").SortMode = DataGridViewColumnSortMode.NotSortable
        End With
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Call SelectedColumnVAR()
    End Sub

    'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Call loadSubsidiaryAccountsFilter()
    '    'Call SelectedColumnVAR()
    '    If txtSearch.Text = "" Then
    '        Call loadSubsidiaryAccounts()
    '    End If
    'End Sub
End Class