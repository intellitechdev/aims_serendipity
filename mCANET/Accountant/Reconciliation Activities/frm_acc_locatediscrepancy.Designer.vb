<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_locatediscrepancy
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboAccount = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.btnDisRpt = New System.Windows.Forms.Button
        Me.btnPrevRpt = New System.Windows.Forms.Button
        Me.btnUndoRecon = New System.Windows.Forms.Button
        Me.btnRestRecon = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Account"
        '
        'cboAccount
        '
        Me.cboAccount.FormattingEnabled = True
        Me.cboAccount.Location = New System.Drawing.Point(61, 18)
        Me.cboAccount.Name = "cboAccount"
        Me.cboAccount.Size = New System.Drawing.Size(186, 21)
        Me.cboAccount.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(266, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "date last reconciled"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(266, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "beginning balance amount"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Location = New System.Drawing.Point(12, 77)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(243, 44)
        Me.TextBox1.TabIndex = 4
        Me.TextBox1.Text = "Click Discrepany Report to view changes made to previously cleared transactions s" & _
            "ince this account was last reconciled."
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Location = New System.Drawing.Point(12, 130)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(243, 31)
        Me.TextBox2.TabIndex = 5
        Me.TextBox2.Text = "Click Previous Reports to view transactions that were cleared in a past reconcili" & _
            "ation."
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.Location = New System.Drawing.Point(12, 168)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(415, 31)
        Me.TextBox3.TabIndex = 6
        Me.TextBox3.Text = "When Previous Reports to view transactions that were cleared in a past reconcilia" & _
            "tion issues, click Restart Reconciliation to continue reconciling this account."
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.Location = New System.Drawing.Point(12, 205)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(415, 31)
        Me.TextBox4.TabIndex = 7
        Me.TextBox4.Text = "If you would like to unclear all the cleared transactions for the last reconcilia" & _
            "tion period, click Undo Last Reconciliation."
        '
        'btnDisRpt
        '
        Me.btnDisRpt.Location = New System.Drawing.Point(352, 81)
        Me.btnDisRpt.Name = "btnDisRpt"
        Me.btnDisRpt.Size = New System.Drawing.Size(181, 23)
        Me.btnDisRpt.TabIndex = 8
        Me.btnDisRpt.Text = "Discrepancy Report"
        Me.btnDisRpt.UseVisualStyleBackColor = True
        '
        'btnPrevRpt
        '
        Me.btnPrevRpt.Location = New System.Drawing.Point(352, 122)
        Me.btnPrevRpt.Name = "btnPrevRpt"
        Me.btnPrevRpt.Size = New System.Drawing.Size(181, 23)
        Me.btnPrevRpt.TabIndex = 9
        Me.btnPrevRpt.Text = "Previous Report"
        Me.btnPrevRpt.UseVisualStyleBackColor = True
        '
        'btnUndoRecon
        '
        Me.btnUndoRecon.Location = New System.Drawing.Point(13, 254)
        Me.btnUndoRecon.Name = "btnUndoRecon"
        Me.btnUndoRecon.Size = New System.Drawing.Size(181, 23)
        Me.btnUndoRecon.TabIndex = 10
        Me.btnUndoRecon.Text = "Undo Last Reconciliation"
        Me.btnUndoRecon.UseVisualStyleBackColor = True
        '
        'btnRestRecon
        '
        Me.btnRestRecon.Location = New System.Drawing.Point(199, 254)
        Me.btnRestRecon.Name = "btnRestRecon"
        Me.btnRestRecon.Size = New System.Drawing.Size(181, 23)
        Me.btnRestRecon.TabIndex = 11
        Me.btnRestRecon.Text = "Restart Reconciliation"
        Me.btnRestRecon.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(385, 254)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(181, 23)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frm_acc_locatediscrepancy
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(583, 285)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnRestRecon)
        Me.Controls.Add(Me.btnUndoRecon)
        Me.Controls.Add(Me.btnPrevRpt)
        Me.Controls.Add(Me.btnDisRpt)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboAccount)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_acc_locatediscrepancy"
        Me.ShowIcon = False
        Me.Text = "Locate Discrepancies"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboAccount As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents btnDisRpt As System.Windows.Forms.Button
    Friend WithEvents btnPrevRpt As System.Windows.Forms.Button
    Friend WithEvents btnUndoRecon As System.Windows.Forms.Button
    Friend WithEvents btnRestRecon As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
