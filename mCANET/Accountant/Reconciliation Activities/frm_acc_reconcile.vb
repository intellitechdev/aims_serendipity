Public Class frm_acc_reconcile
    Dim checkDate As New clsRestrictedDate

    Private Sub frm_acc_reconcile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_DisplayAccountsAll(cboReconAcnt)
        Call m_DisplayAccountsAll(cboSrvcAcnt)
        Call m_DisplayAccountsAll(cboIntAcnt)
        Call sLoad()
        IsDateRestricted()
    End Sub

    Private Sub btn_recon_continue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_recon_continue.Click
        If txt_recon_endBalance.Text = "0.00" Then
            MessageBox.Show("You must enter an ending balance for the account being reconciled.", "CLICKSOFTWARE@Accounting:" + Me.Text)
        ElseIf cboReconAcnt.Text = "" Then
            MessageBox.Show("You must select an account to be reconciled.", "CLICKSOFTWARE@Accounting:" + Me.Text)
        Else
            Dim sAccount() As String
            Dim sSrvcAcnt() As String
            Dim sIntAcnt() As String

            If cboReconAcnt.SelectedItem <> "" Then
                sAccount = Split(cboReconAcnt.SelectedItem, "-")
                gAccountID = getAccountID(sAccount(0))
            End If

            If cboSrvcAcnt.SelectedItem <> "" Then
                sSrvcAcnt = Split(cboSrvcAcnt.SelectedItem, "-")
                gServiceChargeAccount = getAccountID(sSrvcAcnt(0))
            End If

            If cboIntAcnt.SelectedItem <> "" Then
                sIntAcnt = Split(cboIntAcnt.SelectedItem, "-")
                gInterestEarnedAccount = getAccountID(sIntAcnt(0))
            End If

            gEndingBalance = Format(CDec(txt_recon_endBalance.Text), "##,##0.00")
            gClearedBalance = Format(CDec(txt_recon_beginBalance.Text), "##,##0.00")
            gServiceCharge = Format(CDec(txt_recon_serviceChg.Text), "##,##0.00")
            gInterestEarned = Format(CDec(txt_recon_intEarned.Text), "##,##0.00")
            gServiceChargeDate = Microsoft.VisualBasic.FormatDateTime(dte_recon_srvcChgDate.Value, DateFormat.ShortDate)
            gInterestEarnedDate = Microsoft.VisualBasic.FormatDateTime(dte_recon_intDate.Value, DateFormat.ShortDate)
            gReconDate = Microsoft.VisualBasic.FormatDateTime(dte_recon_statementdate.Value, DateFormat.ShortDate)
            gVerifyReconcile = 0
            frm_acc_reconcile2.Show()
            Me.Close()
            End If
    End Sub

    Private Sub btn_recon_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_recon_cancel.Click
        Me.Close()
    End Sub

    Private Sub cboReconAcnt_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReconAcnt.SelectedIndexChanged
        Dim sVal() As String
        sVal = Split(cboReconAcnt.SelectedItem, "-")
        txt_recon_beginBalance.Text = Format(CDec(m_GetBalanceAmount(sVal(0))), "##,##0.00#")
    End Sub

    Private Sub txt_recon_endBalance_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txt_recon_endBalance.Validating
        txt_recon_endBalance.Text = Format(CDec(txt_recon_endBalance.Text), "##,##0.00")
    End Sub

    Private Sub txt_recon_serviceChg_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txt_recon_serviceChg.Validating
        txt_recon_serviceChg.Text = Format(CDec(txt_recon_serviceChg.Text), "##,##0.00")
    End Sub

    Private Sub txt_recon_intEarned_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txt_recon_intEarned.Validating
        txt_recon_intEarned.Text = Format(CDec(txt_recon_intEarned.Text), "##,##0.00")
    End Sub

    Private Sub btn_recon_locdiscrep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_recon_locdiscrep.Click
        frm_acc_locatediscrepancy.cboAccount.SelectedItem = cboReconAcnt.SelectedItem
        frm_acc_locatediscrepancy.Show()
        Me.Close()
    End Sub

    Private Sub dte_recon_statementdate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dte_recon_statementdate.ValueChanged
        IsDateRestricted()
    End Sub

    Private Sub IsDateRestricted()
        'Validate Invoice Date
        If checkDate.checkIfRestricted(dte_recon_statementdate.Value) Then
            btn_recon_continue.Enabled = False 'true if date is NOT within range of period restricted
            btn_recon_lstrecon.Enabled = False
            btn_recon_locdiscrep.Enabled = False
        Else
            btn_recon_continue.Enabled = True 'false if date is within range of period restricted
            btn_recon_lstrecon.Enabled = True
            btn_recon_locdiscrep.Enabled = True
        End If
    End Sub

#Region "Functions/Procedures"

    Private Sub sLoad()
        If gVerifyReconcile = 1 Then
            cboReconAcnt.Text = getAccountName(gAccountID) + " - " + gAcntTypeName(gAccountID)
            txt_recon_beginBalance.Text = Format(CDec(gClearedBalance), "##,##0.00")
            txt_recon_endBalance.Text = Format(CDec(gEndingBalance), "##,##0.00")
            txt_recon_serviceChg.Text = Format(CDec(gServiceCharge), "##,##0.00")
            txt_recon_intEarned.Text = Format(CDec(gInterestEarned), "##,##0.00")
            dte_recon_statementdate.Value = Microsoft.VisualBasic.FormatDateTime(gReconDate, DateFormat.ShortDate)
        End If
    End Sub

#End Region

  
    
  

End Class