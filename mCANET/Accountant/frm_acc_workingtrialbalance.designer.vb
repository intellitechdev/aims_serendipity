<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_workingtrialbalance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_workingtrialbalance))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.dteFrom = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteEnd = New System.Windows.Forms.DateTimePicker
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboBasis = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtNetIncome = New System.Windows.Forms.TextBox
        Me.chkacntsWtransaction = New System.Windows.Forms.CheckBox
        Me.btn_trialbal_makeadj = New System.Windows.Forms.Button
        Me.btn_trialbal_print = New System.Windows.Forms.Button
        Me.grdWrkngBal = New System.Windows.Forms.DataGridView
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtEBCredit = New System.Windows.Forms.TextBox
        Me.txtCURCredit = New System.Windows.Forms.TextBox
        Me.txtEBDebit = New System.Windows.Forms.TextBox
        Me.txtCURDebit = New System.Windows.Forms.TextBox
        Me.txtOBCredit = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtOBDebit = New System.Windows.Forms.TextBox
        CType(Me.grdWrkngBal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "From Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Items.AddRange(New Object() {"Last Month", "Last Fiscal Quarter", "Last Fiscal Year", "This Month ", "This Fiscal Quarter", "This Fiscal Year", "Custom"})
        Me.cboPeriod.Location = New System.Drawing.Point(260, -5)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(184, 21)
        Me.cboPeriod.TabIndex = 1
        Me.cboPeriod.Visible = False
        '
        'dteFrom
        '
        Me.dteFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteFrom.Location = New System.Drawing.Point(95, 16)
        Me.dteFrom.Name = "dteFrom"
        Me.dteFrom.Size = New System.Drawing.Size(125, 21)
        Me.dteFrom.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(223, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Until"
        '
        'dteEnd
        '
        Me.dteEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteEnd.Location = New System.Drawing.Point(262, 16)
        Me.dteEnd.Name = "dteEnd"
        Me.dteEnd.Size = New System.Drawing.Size(125, 21)
        Me.dteEnd.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(754, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Basis"
        Me.Label4.Visible = False
        '
        'cboBasis
        '
        Me.cboBasis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboBasis.FormattingEnabled = True
        Me.cboBasis.Items.AddRange(New Object() {"Accrual", "Cash"})
        Me.cboBasis.Location = New System.Drawing.Point(797, 15)
        Me.cboBasis.Name = "cboBasis"
        Me.cboBasis.Size = New System.Drawing.Size(125, 21)
        Me.cboBasis.TabIndex = 7
        Me.cboBasis.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(17, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(82, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Net Income"
        '
        'txtNetIncome
        '
        Me.txtNetIncome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtNetIncome.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetIncome.ForeColor = System.Drawing.Color.Black
        Me.txtNetIncome.Location = New System.Drawing.Point(125, 21)
        Me.txtNetIncome.Name = "txtNetIncome"
        Me.txtNetIncome.ReadOnly = True
        Me.txtNetIncome.Size = New System.Drawing.Size(154, 21)
        Me.txtNetIncome.TabIndex = 16
        Me.txtNetIncome.Text = "0.00"
        Me.txtNetIncome.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkacntsWtransaction
        '
        Me.chkacntsWtransaction.AutoSize = True
        Me.chkacntsWtransaction.Location = New System.Drawing.Point(5, -1)
        Me.chkacntsWtransaction.Name = "chkacntsWtransaction"
        Me.chkacntsWtransaction.Size = New System.Drawing.Size(278, 17)
        Me.chkacntsWtransaction.TabIndex = 17
        Me.chkacntsWtransaction.Text = "Only show accounts with transaction activity"
        Me.chkacntsWtransaction.UseVisualStyleBackColor = True
        Me.chkacntsWtransaction.Visible = False
        '
        'btn_trialbal_makeadj
        '
        Me.btn_trialbal_makeadj.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_trialbal_makeadj.Location = New System.Drawing.Point(13, 58)
        Me.btn_trialbal_makeadj.Name = "btn_trialbal_makeadj"
        Me.btn_trialbal_makeadj.Size = New System.Drawing.Size(152, 23)
        Me.btn_trialbal_makeadj.TabIndex = 31
        Me.btn_trialbal_makeadj.Text = "Make Adjustment"
        Me.btn_trialbal_makeadj.UseVisualStyleBackColor = True
        Me.btn_trialbal_makeadj.Visible = False
        '
        'btn_trialbal_print
        '
        Me.btn_trialbal_print.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_trialbal_print.Location = New System.Drawing.Point(163, 58)
        Me.btn_trialbal_print.Name = "btn_trialbal_print"
        Me.btn_trialbal_print.Size = New System.Drawing.Size(116, 23)
        Me.btn_trialbal_print.TabIndex = 32
        Me.btn_trialbal_print.Text = "Print Preview"
        Me.btn_trialbal_print.UseVisualStyleBackColor = True
        '
        'grdWrkngBal
        '
        Me.grdWrkngBal.AllowUserToAddRows = False
        Me.grdWrkngBal.AllowUserToDeleteRows = False
        Me.grdWrkngBal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdWrkngBal.BackgroundColor = System.Drawing.SystemColors.ControlDark
        Me.grdWrkngBal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdWrkngBal.GridColor = System.Drawing.Color.WhiteSmoke
        Me.grdWrkngBal.Location = New System.Drawing.Point(8, 44)
        Me.grdWrkngBal.Name = "grdWrkngBal"
        Me.grdWrkngBal.ReadOnly = True
        Me.grdWrkngBal.Size = New System.Drawing.Size(914, 412)
        Me.grdWrkngBal.TabIndex = 33
        '
        'btnGenerate
        '
        Me.btnGenerate.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.Location = New System.Drawing.Point(394, 15)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(100, 23)
        Me.btnGenerate.TabIndex = 34
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btn_trialbal_print)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtNetIncome)
        Me.GroupBox1.Controls.Add(Me.btn_trialbal_makeadj)
        Me.GroupBox1.Location = New System.Drawing.Point(634, 461)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 87)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.txtEBCredit)
        Me.GroupBox2.Controls.Add(Me.txtCURCredit)
        Me.GroupBox2.Controls.Add(Me.txtEBDebit)
        Me.GroupBox2.Controls.Add(Me.txtCURDebit)
        Me.GroupBox2.Controls.Add(Me.txtOBCredit)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtOBDebit)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 461)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(625, 87)
        Me.GroupBox2.TabIndex = 36
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Totals"
        '
        'txtEBCredit
        '
        Me.txtEBCredit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtEBCredit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEBCredit.ForeColor = System.Drawing.Color.Black
        Me.txtEBCredit.Location = New System.Drawing.Point(447, 58)
        Me.txtEBCredit.Name = "txtEBCredit"
        Me.txtEBCredit.ReadOnly = True
        Me.txtEBCredit.Size = New System.Drawing.Size(158, 21)
        Me.txtEBCredit.TabIndex = 28
        Me.txtEBCredit.Text = "0.00"
        Me.txtEBCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCURCredit
        '
        Me.txtCURCredit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtCURCredit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCURCredit.ForeColor = System.Drawing.Color.Black
        Me.txtCURCredit.Location = New System.Drawing.Point(260, 58)
        Me.txtCURCredit.Name = "txtCURCredit"
        Me.txtCURCredit.ReadOnly = True
        Me.txtCURCredit.Size = New System.Drawing.Size(158, 21)
        Me.txtCURCredit.TabIndex = 27
        Me.txtCURCredit.Text = "0.00"
        Me.txtCURCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEBDebit
        '
        Me.txtEBDebit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtEBDebit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEBDebit.ForeColor = System.Drawing.Color.Black
        Me.txtEBDebit.Location = New System.Drawing.Point(447, 33)
        Me.txtEBDebit.Name = "txtEBDebit"
        Me.txtEBDebit.ReadOnly = True
        Me.txtEBDebit.Size = New System.Drawing.Size(158, 21)
        Me.txtEBDebit.TabIndex = 26
        Me.txtEBDebit.Text = "0.00"
        Me.txtEBDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCURDebit
        '
        Me.txtCURDebit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtCURDebit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCURDebit.ForeColor = System.Drawing.Color.Black
        Me.txtCURDebit.Location = New System.Drawing.Point(260, 33)
        Me.txtCURDebit.Name = "txtCURDebit"
        Me.txtCURDebit.ReadOnly = True
        Me.txtCURDebit.Size = New System.Drawing.Size(158, 21)
        Me.txtCURDebit.TabIndex = 25
        Me.txtCURDebit.Text = "0.00"
        Me.txtCURDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOBCredit
        '
        Me.txtOBCredit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtOBCredit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOBCredit.ForeColor = System.Drawing.Color.Black
        Me.txtOBCredit.Location = New System.Drawing.Point(75, 58)
        Me.txtOBCredit.Name = "txtOBCredit"
        Me.txtOBCredit.ReadOnly = True
        Me.txtOBCredit.Size = New System.Drawing.Size(158, 21)
        Me.txtOBCredit.TabIndex = 24
        Me.txtOBCredit.Text = "0.00"
        Me.txtOBCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(257, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Current Balance"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label8.Location = New System.Drawing.Point(23, 61)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Credit"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkRed
        Me.Label7.Location = New System.Drawing.Point(444, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Ending Balance"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label6.Location = New System.Drawing.Point(23, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "Debit"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkRed
        Me.Label5.Location = New System.Drawing.Point(72, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(115, 13)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Opening Balance"
        '
        'txtOBDebit
        '
        Me.txtOBDebit.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtOBDebit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOBDebit.ForeColor = System.Drawing.Color.Black
        Me.txtOBDebit.Location = New System.Drawing.Point(75, 33)
        Me.txtOBDebit.Name = "txtOBDebit"
        Me.txtOBDebit.ReadOnly = True
        Me.txtOBDebit.Size = New System.Drawing.Size(158, 21)
        Me.txtOBDebit.TabIndex = 16
        Me.txtOBDebit.Text = "0.00"
        Me.txtOBDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frm_acc_workingtrialbalance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(927, 555)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.grdWrkngBal)
        Me.Controls.Add(Me.chkacntsWtransaction)
        Me.Controls.Add(Me.cboBasis)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dteEnd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dteFrom)
        Me.Controls.Add(Me.cboPeriod)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_acc_workingtrialbalance"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Working Trial Balance"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.grdWrkngBal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents dteFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboBasis As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtNetIncome As System.Windows.Forms.TextBox
    Friend WithEvents chkacntsWtransaction As System.Windows.Forms.CheckBox
    Friend WithEvents btn_trialbal_makeadj As System.Windows.Forms.Button
    Friend WithEvents btn_trialbal_print As System.Windows.Forms.Button
    Friend WithEvents grdWrkngBal As System.Windows.Forms.DataGridView
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtOBDebit As System.Windows.Forms.TextBox
    Friend WithEvents txtCURCredit As System.Windows.Forms.TextBox
    Friend WithEvents txtEBDebit As System.Windows.Forms.TextBox
    Friend WithEvents txtCURDebit As System.Windows.Forms.TextBox
    Friend WithEvents txtOBCredit As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEBCredit As System.Windows.Forms.TextBox
End Class
