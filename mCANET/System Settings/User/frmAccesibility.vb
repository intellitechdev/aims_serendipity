Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: Sept 15, 2009                      ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Public Class frmAccesibility
#Region "Declaration"
    Dim GCon As New Clsappconfiguration
    Public Mode As String = "ADD"
    Public PreFxKeyUser As String
    Private xchChartofAccounts As Integer
    Private xchItemMaster As Integer
    Private xchSalesTax As Integer
    Private xchSalesRep As Integer
    Private xchCustType As Integer
    Private xchSupType As Integer
    Private xchTerm As Integer
    Private xchClass As Integer
    Private xchGenJournEnt As Integer
    Private xchAccntntRecon As Integer
    Private xchWorkngTrlBal As Integer
    Private xchWriteChecks As Integer
    Private xchBankRecon As Integer
    Private xchTransFund As Integer
    Private xchMkeDepot As Integer
    Private xchCustMaster As Integer
    Private xchSO As Integer
    Private xchInv As Integer
    Private xchSR As Integer
    Private xchRecievePaymnt As Integer
    Private xchCredit As Integer
    Private xchDebit As Integer
    Private xchSupMaster As Integer
    Private xchBills As Integer
    Private xchBills4Apprvl As Integer
    Private xchPayBills As Integer
    Private xchPO As Integer
    Private xchRecievedItem As Integer
    Private xchEnterBills4RcveItem As Integer
    Private xchUserOptn As Integer
    Private xchChngePwrd As Integer
    Private xchPref As Integer
    Private xchPeriodRestrict As Integer
    Private xchImport As Integer
    Private xchAPSummary As Integer
    Private xchBllsPymntAcctRpt As Integer
    Private xchAPCVList As Integer
    Private xchAging As Integer
    Private xchBalShit As Integer
    Private xchCVHistory As Integer
    Private xchCollectionRpt As Integer
    Private xchFinanceCharge As Integer
    Private xchGLListing As Integer
    Private xchGLByAccnt As Integer
    Private xchIncmeStatement As Integer
    Private xchJVSummary As Integer
    Private xchJVSummryperAccnt As Integer
    Private xchCustList As Integer
    Private xchItemMasterInventry As Integer
    Private xchSupMasterRpt As Integer
    Private xchReimburseExpnse As Integer
    Private xchSalesRpt As Integer
    Private xchSttmentofAccount As Integer
    Private xchTaxRpt As Integer
    Private xchUndepostFunds As Integer
    Private xchBudgetMonitor As Integer
    Private xchCustTrnsctnRpt As Integer
#End Region
#Region "Subs and Functions"
    Private Sub InsertingValues()
        If chChartofAccounts.Checked = True Then
            xchChartofAccounts = 1
        Else
            xchChartofAccounts = 0
        End If
        If chItemMaster.Checked = True Then
            xchItemMaster = 1
        Else
            xchItemMaster = 0
        End If
        If chSalesTax.Checked = True Then
            xchSalesTax = 1
        Else
            xchSalesTax = 0
        End If
        If chSalesRep.Checked = True Then
            xchSalesRep = 1
        Else
            xchSalesRep = 0
        End If
        If chCustType.Checked = True Then
            xchCustType = 1
        Else
            xchCustType = 0
        End If
        If chSupType.Checked = True Then
            xchSupType = 1
        Else
            xchSupType = 0
        End If
        If chTerm.Checked = True Then
            xchTerm = 1
        Else
            xchTerm = 0
        End If
        If ChClass.Checked = True Then
            xchClass = 1
        Else
            xchClass = 0
        End If
        If chGenJournEnt.Checked = True Then
            xchGenJournEnt = 1
        Else
            xchGenJournEnt = 0
        End If
        If chAccntntRecon.Checked = True Then
            xchAccntntRecon = 1
        Else
            xchAccntntRecon = 0
        End If
        If chWorkngTrlBal.Checked = True Then
            xchWorkngTrlBal = 1
        Else
            xchWorkngTrlBal = 0
        End If
        If chWriteChecks.Checked = True Then
            xchWriteChecks = 1
        Else
            xchWriteChecks = 0
        End If
        If chBankRecon.Checked = True Then
            xchBankRecon = 1
        Else
            xchBankRecon = 0
        End If
        If chTransFunds.Checked = True Then
            xchTransFund = 1
        Else
            xchTransFund = 0
        End If
        If chMakeDeposit.Checked = True Then
            xchMkeDepot = 1
        Else
            xchMkeDepot = 0
        End If

        If chCustMaster.Checked = True Then
            xchCustMaster = 1
        Else
            xchCustMaster = 0
        End If
        If chSO.Checked = True Then
            xchSO = 1
        Else
            xchSO = 0
        End If
        If chInv.Checked = True Then
            xchInv = 1
        Else
            xchInv = 0
        End If
        If chSR.Checked = True Then
            xchSR = 1
        Else
            xchSR = 0
        End If
        If chRecievePaymnt.Checked = True Then
            xchRecievePaymnt = 1
        Else
            xchRecievePaymnt = 0
        End If
        If chCredit.Checked = True Then
            xchCredit = 1
        Else
            xchCredit = 0
        End If
        If chDebit.Checked = True Then
            xchDebit = 1
        Else
            xchDebit = 0
        End If
        If chSupMaster.Checked = True Then
            xchSupMaster = 1
        Else
            xchSupMaster = 0
        End If
        If chBills.Checked = True Then
            xchBills = 1
        Else
            xchBills = 0
        End If
        If chBills4Apprvl.Checked = True Then
            xchBills4Apprvl = 1
        Else
            xchBills4Apprvl = 0
        End If
        If chPayBills.Checked = True Then
            xchPayBills = 1
        Else
            xchPayBills = 0
        End If
        If chPO.Checked = True Then
            xchPO = 1
        Else
            xchPO = 0
        End If
        If chRecievedItem.Checked = True Then
            xchRecievedItem = 1
        Else
            xchRecievedItem = 0
        End If
        If chEnterBills4RcveItem.Checked = True Then
            xchEnterBills4RcveItem = 1
        Else
            xchEnterBills4RcveItem = 0
        End If
        If chUserOptn.Checked = True Then
            xchUserOptn = 1
        Else
            xchUserOptn = 0
        End If
        If chChngePwrd.Checked = True Then
            xchChngePwrd = 1
        Else
            xchChngePwrd = 0
        End If
        If chPref.Checked = True Then
            xchPref = 1
        Else
            xchPref = 0
        End If
        If chPeriodRestrict.Checked = True Then
            xchPeriodRestrict = 1
        Else
            xchPeriodRestrict = 0
        End If
        If chImport.Checked = True Then
            xchImport = 1
        Else
            xchImport = 0
        End If
        If chAPSummary.Checked = True Then
            xchAPSummary = 1
        Else
            xchAPSummary = 0
        End If
        If chBllsPymntAcctRpt.Checked = True Then
            xchBllsPymntAcctRpt = 1
        Else
            xchBllsPymntAcctRpt = 0
        End If
        If chAPCVList.Checked = True Then
            xchAPCVList = 1
        Else
            xchAPCVList = 0
        End If
        If chAging.Checked = True Then
            xchAging = 1
        Else
            xchAging = 0
        End If
        If chBalShit.Checked = True Then
            xchBalShit = 1
        Else
            xchBalShit = 0
        End If
        If chCVHistory.Checked = True Then
            xchCVHistory = 1
        Else
            xchCVHistory = 1
        End If
        If chCollectionRpt.Checked = True Then
            xchCollectionRpt = 1
        Else
            xchCollectionRpt = 0
        End If
        If chFinanceCharge.Checked = True Then
            xchFinanceCharge = 1
        Else
            xchFinanceCharge = 0
        End If
        If chGLListing.Checked = True Then
            xchGLListing = 1
        Else
            xchGLListing = 0
        End If
        If chGLByAccnt.Checked = True Then
            xchGLByAccnt = 1
        Else
            xchGLByAccnt = 0
        End If
        If chIncmeStatement.Checked = True Then
            xchIncmeStatement = 1
        Else
            xchIncmeStatement = 0
        End If
        If chJVSummary.Checked = True Then
            xchJVSummary = 1
        Else
            xchJVSummary = 0
        End If
        If chJVSummryperAccnt.Checked = True Then
            xchJVSummryperAccnt = 1
        Else
            xchJVSummryperAccnt = 0
        End If
        If chCustList.Checked = True Then
            xchCustList = 1
        Else
            xchCustList = 0
        End If
        If chItemMasterInventry.Checked = True Then
            xchItemMasterInventry = 1
        Else
            xchItemMasterInventry = 0
        End If
        If chSupMasterRpt.Checked = True Then
            xchSupMasterRpt = 1
        Else
            xchSupMasterRpt = 0
        End If
        If chReimburseExpnse.Checked = True Then
            xchReimburseExpnse = 1
        Else
            xchReimburseExpnse = 0
        End If
        If chSalesRpt.Checked = True Then
            xchSalesRpt = 1
        Else
            xchSalesRpt = 0
        End If
        If chStatementofAccnt.Checked = True Then
            xchSttmentofAccount = 1
        Else
            xchSttmentofAccount = 0
        End If
        If chTaxRpt.Checked = True Then
            xchTaxRpt = 1
        Else
            xchTaxRpt = 0
        End If
        If chUndepostFunds.Checked = True Then
            xchUndepostFunds = 1
        Else
            xchUndepostFunds = 0
        End If
        If chBudgetMonitor.Checked = True Then
            xchBudgetMonitor = 1
        Else
            xchBudgetMonitor = 0
        End If
        If ChCustTrnsctnRpt.Checked = True Then
            xchCustTrnsctnRpt = 1
        Else
            xchCustTrnsctnRpt = 0
        End If
    End Sub
    Private Sub defaultForADD()
        chChartofAccounts.Checked = True
        chItemMaster.Checked = True
        chSalesTax.Checked = True
        chSalesRep.Checked = True
        chCustType.Checked = True
        chSupType.Checked = True
        chTerm.Checked = True
        ChClass.Checked = True
        chGenJournEnt.Checked = True
        chAccntntRecon.Checked = True
        chWorkngTrlBal.Checked = True
        chWriteChecks.Checked = True
        chBankRecon.Checked = True
        chCustMaster.Checked = True
        chSO.Checked = True
        chInv.Checked = True
        chSR.Checked = True
        chRecievePaymnt.Checked = True
        chCredit.Checked = True
        chDebit.Checked = True
        chSupMaster.Checked = True
        chBills.Checked = True
        chBills4Apprvl.Checked = True
        chPayBills.Checked = True
        chPO.Checked = True
        chRecievedItem.Checked = True
        chEnterBills4RcveItem.Checked = True
        chUserOptn.Checked = True
        chChngePwrd.Checked = True
        chPref.Checked = True
        chPeriodRestrict.Checked = True
        chImport.Checked = True
        chAPSummary.Checked = True
        chBllsPymntAcctRpt.Checked = True
        chAPCVList.Checked = True
        chAging.Checked = True
        chBalShit.Checked = True
        chCVHistory.Checked = True
        chCollectionRpt.Checked = True
        chFinanceCharge.Checked = True
        chGLListing.Checked = True
        chGLByAccnt.Checked = True
        chIncmeStatement.Checked = True
        chJVSummary.Checked = True
        chJVSummryperAccnt.Checked = True
        chCustList.Checked = True
        chItemMasterInventry.Checked = True
        chSupMasterRpt.Checked = True
        chReimburseExpnse.Checked = True
        chSalesRpt.Checked = True
        chStatementofAccnt.Checked = True
        chTaxRpt.Checked = True
        chUndepostFunds.Checked = True
        chBudgetMonitor.Checked = True
        ChCustTrnsctnRpt.Checked = True
    End Sub
    Private Sub getCurrentConfiguration(ByVal FxKeyUser As String)
        Dim sSqlCmd As String = "SELECT * FROM dbo.tUserAccesibilitySetting WHERE fxKeyUser='" & _
                                FxKeyUser & "' AND fxKeyCompany='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(GCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    While rd.Read
                        chChartofAccounts.Checked = rd.Item(2)
                        chItemMaster.Checked = rd.Item(3)
                        chSalesTax.Checked = rd.Item(4)
                        chSalesRep.Checked = rd.Item(5)
                        chCustType.Checked = rd.Item(6)
                        chSupType.Checked = rd.Item(7)
                        chTerm.Checked = rd.Item(8)
                        ChClass.Checked = rd.Item(9)
                        chGenJournEnt.Checked = rd.Item(10)
                        chAccntntRecon.Checked = rd.Item(11)
                        chWorkngTrlBal.Checked = rd.Item(12)
                        chWriteChecks.Checked = rd.Item(13)
                        chBankRecon.Checked = rd.Item(14)
                        chTransFunds.Checked = rd.Item(15)
                        chMakeDeposit.Checked = rd.Item(16)
                        chCustMaster.Checked = rd.Item(17)
                        chSO.Checked = rd.Item(18)
                        chInv.Checked = rd.Item(19)
                        chSR.Checked = rd.Item(20)
                        chRecievePaymnt.Checked = rd.Item(21)
                        chCredit.Checked = rd.Item(22)
                        chDebit.Checked = rd.Item(23)
                        chSupMaster.Checked = rd.Item(24)
                        chBills.Checked = rd.Item(25)
                        chBills4Apprvl.Checked = rd.Item(26)
                        chPayBills.Checked = rd.Item(27)
                        chPO.Checked = rd.Item(28)
                        chRecievedItem.Checked = rd.Item(29)
                        chEnterBills4RcveItem.Checked = rd.Item(30)
                        chUserOptn.Checked = rd.Item(31)
                        chChngePwrd.Checked = rd.Item(32)
                        chPref.Checked = rd.Item(33)
                        chPeriodRestrict.Checked = rd.Item(34)
                        chImport.Checked = rd.Item(35)
                        chAPSummary.Checked = rd.Item(36)
                        chBllsPymntAcctRpt.Checked = rd.Item(37)
                        chAPCVList.Checked = rd.Item(38)
                        chAging.Checked = rd.Item(39)
                        chBalShit.Checked = rd.Item(40)
                        chCVHistory.Checked = rd.Item(41)
                        chCollectionRpt.Checked = rd.Item(42)
                        chFinanceCharge.Checked = rd.Item(43)
                        chGLListing.Checked = rd.Item(44)
                        chGLByAccnt.Checked = rd.Item(45)
                        chIncmeStatement.Checked = rd.Item(46)
                        chJVSummary.Checked = rd.Item(47)
                        chJVSummryperAccnt.Checked = rd.Item(48)
                        chCustList.Checked = rd.Item(49)
                        chItemMasterInventry.Checked = rd.Item(50)
                        chSupMasterRpt.Checked = rd.Item(51)
                        chReimburseExpnse.Checked = rd.Item(52)
                        chSalesRpt.Checked = rd.Item(53)
                        chStatementofAccnt.Checked = rd.Item(54)
                        chTaxRpt.Checked = rd.Item(55)
                        chUndepostFunds.Checked = rd.Item(56)
                        chBudgetMonitor.Checked = rd.Item(57)
                        ChCustTrnsctnRpt.Checked = rd.Item(58)
                    End While
                Else
                    defaultForADD()
                End If
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Private Sub frmAccesibility_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If frmMain.LblClassActivator.Visible = True Then
            ChClass.Visible = True
        Else
            ChClass.Visible = False
        End If
        If Mode = "EDIT" Then
            getCurrentConfiguration(PreFxKeyUser)
        Else
            defaultForADD()
        End If
    End Sub
    Private Sub AddAccesibility()
        Dim sSqlCmd As String = "INSERT INTO dbo.tUserAccesibilitySetting" & _
                                                   "(fxKeyUser," & _
                                                   "fxKeyCompany," & _
                                                   "fcChartofAccnt," & _
                                                   "fcItemMaster," & _
                                                   "fcSalesTax," & _
                                                   "fcSalesRep," & _
                                                   "fcCustType," & _
                                                   "fcSupType," & _
                                                   "fcTerm," & _
                                                   "fcClass," & _
                                                   "fcGenJournalEntries," & _
                                                   "fcAccntRecon," & _
                                                   "fcWTrialBal," & _
                                                   "fcWriteChecks," & _
                                                   "fcBankRecon," & _
                                                   "fcTransFunds," & _
                                                   "fcMkeDepot," & _
                                                   "fcCustMaster," & _
                                                   "fcSO," & _
                                                   "fcInv," & _
                                                   "fcSR," & _
                                                   "fcRecvePayment," & _
                                                   "fcCreditMmo," & _
                                                   "fcDebitMmo," & _
                                                   "fcSupMaster," & _
                                                   "fcBills," & _
                                                   "fcBills4Approval," & _
                                                   "fcPayBills," & _
                                                   "fcPO," & _
                                                   "fcReciveItem," & _
                                                   "fcEnterBills4RcvItem," & _
                                                   "fcUserOptn," & _
                                                   "fcChaPass," & _
                                                   "fcPrefference," & _
                                                   "fcPerRestrct," & _
                                                   "fcImports," & _
                                                   "fcAPSummary," & _
                                                   "fcBllsPaymentAccntRpt," & _
                                                   "fcAPCVList," & _
                                                   "fcAging," & _
                                                   "fcBalSheet," & _
                                                   "fcCVHistory," & _
                                                   "fcCollectRpt," & _
                                                   "fcFinanceCharge," & _
                                                   "fcGLListing," & _
                                                   "fcGLByAccnt," & _
                                                   "fcIncmeSttmnt," & _
                                                   "fcJVSummary," & _
                                                   "fcJVSummaryByAccnt," & _
                                                   "fcCustListRpt," & _
                                                   "fcItemMasterInvntry," & _
                                                   "fcSupMasterRpt," & _
                                                   "fcReimburse," & _
                                                   "fcSalesRpt," & _
                                                   "fcSttmentofAccnt, " & _
                                                   "fcTaxRpt," & _
                                                   "fcUndepotstdFunds," & _
                                                   "fcBudgetMonitoringRpt," & _
                                                   "fcCustTransactRpt)" & _
                                           "VALUES ('" & PreFxKeyUser & "', '" & _
                                                       gCompanyID() & "', '" & _
                                                       xchChartofAccounts & "', '" & _
                                                       xchItemMaster & "', '" & _
                                                       xchSalesTax & "', '" & _
                                                       xchSalesRep & "', '" & _
                                                       xchCustType & "', '" & _
                                                       xchSupType & "', '" & _
                                                       xchTerm & "', '" & _
                                                       xchClass & "', '" & _
                                                       xchGenJournEnt & "', '" & _
                                                       xchAccntntRecon & "', '" & _
                                                       xchWorkngTrlBal & "', '" & _
                                                       xchWriteChecks & "', '" & _
                                                       xchBankRecon & "', '" & _
                                                       xchTransFund & "', '" & _
                                                       xchMkeDepot & "', '" & _
                                                       xchCustMaster & "', '" & _
                                                       xchSO & "', '" & _
                                                       xchInv & "', '" & _
                                                       xchSR & "', '" & _
                                                       xchRecievePaymnt & "', '" & _
                                                       xchCredit & "', '" & _
                                                       xchDebit & "', '" & _
                                                       xchSupMaster & "', '" & _
                                                       xchBills & "', '" & _
                                                       xchBills4Apprvl & "', '" & _
                                                       xchPayBills & "', '" & _
                                                       xchPO & "', '" & _
                                                       xchRecievedItem & "', '" & _
                                                       xchEnterBills4RcveItem & "', '" & _
                                                       xchUserOptn & "', '" & _
                                                       xchChngePwrd & "', '" & _
                                                       xchPref & "', '" & _
                                                       xchPeriodRestrict & "', '" & _
                                                       xchImport & "', '" & _
                                                       xchAPSummary & "', '" & _
                                                       xchBllsPymntAcctRpt & "', '" & _
                                                       xchAPCVList & "', '" & _
                                                       xchAging & "', '" & _
                                                       xchBalShit & "', '" & _
                                                       xchCVHistory & "', '" & _
                                                       xchCollectionRpt & "', '" & _
                                                       xchFinanceCharge & "', '" & _
                                                       xchGLListing & "', '" & _
                                                       xchGLByAccnt & "', '" & _
                                                       xchIncmeStatement & "', '" & _
                                                       xchJVSummary & "', '" & _
                                                       xchJVSummryperAccnt & "', '" & _
                                                       xchCustList & "', '" & _
                                                       xchItemMasterInventry & "', '" & _
                                                       xchSupMasterRpt & "', '" & _
                                                       xchReimburseExpnse & "', '" & _
                                                       xchSalesRpt & "', '" & _
                                                       xchSttmentofAccount & "', '" & _
                                                       xchTaxRpt & "', '" & _
                                                       xchUndepostFunds & "', '" & _
                                                       xchBudgetMonitor & "', '" & _
                                                       xchCustTrnsctnRpt & "')"
        Try
            SqlHelper.ExecuteNonQuery(GCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub EditAccesibility()
        Dim sSqlEditCmd As String = "UPDATE dbo.tUserAccesibilitySetting " & _
                        "SET fcChartofAccnt ='" & xchChartofAccounts & "', " & _
                            "fcItemMaster ='" & xchItemMaster & "', " & _
                            "fcSalesTax ='" & xchSalesTax & "', " & _
                            "fcSalesRep ='" & xchSalesRep & "', " & _
                            "fcCustType ='" & xchCustType & "', " & _
                            "fcSupType ='" & xchSupType & "', " & _
                            "fcTerm ='" & xchTerm & "', " & _
                            "fcClass ='" & xchClass & "', " & _
                            "fcGenJournalEntries ='" & xchGenJournEnt & "', " & _
                            "fcAccntRecon ='" & xchAccntntRecon & "', " & _
                            "fcWTrialBal ='" & xchWorkngTrlBal & "', " & _
                            "fcWriteChecks ='" & xchWriteChecks & "', " & _
                            "fcBankRecon ='" & xchBankRecon & "', " & _
                            "fcTransFunds ='" & xchTransFund & "', " & _
                            "fcMkeDepot ='" & xchMkeDepot & "', " & _
                            "fcCustMaster ='" & xchCustMaster & "', " & _
                            "fcSO ='" & xchSO & "', " & _
                            "fcInv ='" & xchInv & "', " & _
                            "fcSR ='" & xchSR & "', " & _
                            "fcRecvePayment ='" & xchRecievePaymnt & "', " & _
                            "fcCreditMmo ='" & xchCredit & "', " & _
                            "fcDebitMmo ='" & xchDebit & "', " & _
                            "fcSupMaster ='" & xchSupMaster & "', " & _
                            "fcBills ='" & xchBills & "', " & _
                            "fcBills4Approval ='" & xchBills4Apprvl & "', " & _
                            "fcPayBills ='" & xchPayBills & "', " & _
                            "fcPO ='" & xchPO & "', " & _
                            "fcReciveItem ='" & xchRecievedItem & "', " & _
                            "fcEnterBills4RcvItem ='" & xchEnterBills4RcveItem & "', " & _
                            "fcUserOptn ='" & xchUserOptn & "', " & _
                            "fcChaPass ='" & xchChngePwrd & "', " & _
                            "fcPrefference ='" & xchPref & "', " & _
                            "fcPerRestrct ='" & xchPeriodRestrict & "', " & _
                            "fcImports ='" & xchImport & "', " & _
                            "fcAPSummary ='" & xchAPSummary & "', " & _
                            "fcBllsPaymentAccntRpt ='" & xchBllsPymntAcctRpt & "', " & _
                            "fcAPCVList ='" & xchAPCVList & "', " & _
                            "fcAging ='" & xchAging & "', " & _
                            "fcBalSheet ='" & xchBalShit & "', " & _
                            "fcCVHistory ='" & xchCVHistory & "', " & _
                            "fcCollectRpt ='" & xchCollectionRpt & "', " & _
                            "fcFinanceCharge ='" & xchFinanceCharge & "', " & _
                            "fcGLListing ='" & xchGLListing & "', " & _
                            "fcGLByAccnt ='" & xchGLByAccnt & "', " & _
                            "fcIncmeSttmnt ='" & xchIncmeStatement & "', " & _
                            "fcJVSummary ='" & xchJVSummary & "', " & _
                            "fcJVSummaryByAccnt ='" & xchJVSummryperAccnt & "', " & _
                            "fcCustListRpt ='" & xchCustList & "', " & _
                            "fcItemMasterInvntry ='" & xchItemMasterInventry & "', " & _
                            "fcSupMasterRpt ='" & xchSupMasterRpt & "', " & _
                            "fcReimburse ='" & xchReimburseExpnse & "', " & _
                            "fcSalesRpt ='" & xchSalesRpt & "', " & _
                            "fcSttmentofAccnt ='" & xchSttmentofAccount & "', " & _
                            "fcTaxRpt ='" & xchTaxRpt & "', " & _
                            "fcUndepotstdFunds ='" & xchUndepostFunds & "', " & _
                            "fcBudgetMonitoringRpt ='" & xchBudgetMonitor & "', " & _
                            "fcCustTransactRpt ='" & xchCustTrnsctnRpt & "'" & _
                    "WHERE fxKeyUser ='" & PreFxKeyUser & "' AND fxKeyCompany ='" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteNonQuery(GCon.cnstring, CommandType.Text, sSqlEditCmd)
            MsgBox("Configuration successfully changed!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted")
            Me.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Function CheckAccessibilityCofigExist(ByVal xdbfxKeyUser As String)
        Dim sSqlCmd As String = "SELECT * FROM dbo.tUserAccesibilitySetting WHERE fxKeyUser ='" & _
                                xdbfxKeyUser & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(GCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = False Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
        End Try
    End Function
#End Region
    Private Sub BtnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOK.Click
        Dim Ask As String
        Ask = MsgBox("Are you sure for this configuration?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm Action")
        If Ask = vbYes Then
            InsertingValues()
            If Mode = "ADD" Then
                AddAccesibility()
            Else
                If CheckAccessibilityCofigExist(PreFxKeyUser) = True Then
                    EditAccesibility()
                Else
                    AddAccesibility()
                End If
                MsgBox("Accessibility has been configured.", MsgBoxStyle.Information + _
                      MsgBoxStyle.OkOnly, "Access Granted")
                Me.Close()
            End If
        End If
    End Sub

End Class