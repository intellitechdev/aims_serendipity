﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading

Public Class frmUserAccesibility
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Dim ds As DataSet
        Dim coid As New Guid(gCompanyID())

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_UserAccess_List",
                                      New SqlParameter("@fxKeyUser", getUserID()))

        grdUserAccess.DataSource = ds.Tables(0)

        With grdUserAccess
            .Columns("pk_UserModule").Visible = False
            .Columns("fcModuleName").HeaderText = "Module Name"
            .Columns("fcModuleName").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fcModuleName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("fcModuleName").ReadOnly = True
            .Columns("fbIsAllowed").HeaderText = "Allowed"
            .Columns("fk_ModuleKey").Visible = False
            .Columns("fk_UserKey").Visible = False
        End With
    End Sub

    Private Function getUserID()
        Dim mycon As New Clsappconfiguration
        Dim sSqlCmd As String = "SELECT fxKeyUser FROM dbo.tUsers WHERE fcLogName ='" & txtUsername.Text & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.Text, sSqlCmd)
                If rd.Read = True Then
                    Return rd.Item(0).ToString
                End If
            End Using
        Catch ex As Exception

        End Try
    End Function

    Private Sub frmUserAccesibility_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Call updateUserModules()

    End Sub

    Private Sub updateUserModules()
        Dim mycon As New Clsappconfiguration
        Dim coid As New Guid(gCompanyID())
        For xRow As Integer = 0 To grdUserAccess.RowCount - 1
            Dim fxModuleID As Integer = grdUserAccess.Item("pk_UserModule", xRow).Value
            Dim isAllowed As Boolean = grdUserAccess.Item("fbIsAllowed", xRow).Value
            Dim fxModuleKey As Integer = grdUserAccess.Item("fk_ModuleKey", xRow).Value
            Dim fk_UserKey As Integer = grdUserAccess.Item("fk_UserKey", xRow).Value
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "spu_UserAccess_Update",
                                          New SqlParameter("@pk_UserModule", fxModuleKey),
                                          New SqlParameter("@fbIsAllowed", isAllowed),
                                          New SqlParameter("@Userkey", fk_UserKey))
        Next
        MsgBox("User Access Modules Successfully Updated")
        AuditTrail_Save("User Access Modules", "Update user modules of " & txtUsername.Text)
    End Sub
End Class