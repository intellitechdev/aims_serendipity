<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterUploader
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterUploader))
        Me.txtUploader = New System.Windows.Forms.TextBox
        Me.BtnBrowse = New System.Windows.Forms.Button
        Me.btnUpload = New System.Windows.Forms.Button
        Me.GrdUploader = New System.Windows.Forms.DataGridView
        Me.DlgSaveUploader = New System.Windows.Forms.SaveFileDialog
        Me.DlgOpenUploader = New System.Windows.Forms.OpenFileDialog
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtDescription = New System.Windows.Forms.Label
        Me.txtTitleLabel = New System.Windows.Forms.Label
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.menuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.menuOpen = New System.Windows.Forms.ToolStripMenuItem
        Me.menuClose = New System.Windows.Forms.ToolStripMenuItem
        CType(Me.GrdUploader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtUploader
        '
        Me.txtUploader.BackColor = System.Drawing.Color.White
        Me.txtUploader.Location = New System.Drawing.Point(6, 155)
        Me.txtUploader.Name = "txtUploader"
        Me.txtUploader.ReadOnly = True
        Me.txtUploader.Size = New System.Drawing.Size(250, 23)
        Me.txtUploader.TabIndex = 0
        Me.txtUploader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Location = New System.Drawing.Point(6, 125)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(70, 24)
        Me.BtnBrowse.TabIndex = 2
        Me.BtnBrowse.Text = "Select File"
        Me.BtnBrowse.UseVisualStyleBackColor = True
        '
        'btnUpload
        '
        Me.btnUpload.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpload.Location = New System.Drawing.Point(6, 184)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(250, 47)
        Me.btnUpload.TabIndex = 4
        Me.btnUpload.Text = "Upload"
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'GrdUploader
        '
        Me.GrdUploader.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdUploader.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.GrdUploader.BackgroundColor = System.Drawing.Color.White
        Me.GrdUploader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdUploader.GridColor = System.Drawing.Color.Black
        Me.GrdUploader.Location = New System.Drawing.Point(268, 27)
        Me.GrdUploader.Name = "GrdUploader"
        Me.GrdUploader.Size = New System.Drawing.Size(684, 396)
        Me.GrdUploader.TabIndex = 5
        '
        'DlgSaveUploader
        '
        Me.DlgSaveUploader.Filter = "Excel 2007 file|*.xlsx|Excel 97-2003 file|*.xls"
        Me.DlgSaveUploader.Title = "Save Uploader in"
        '
        'DlgOpenUploader
        '
        Me.DlgOpenUploader.Filter = "Excel 2007 file|* .xlsx|Excel 97-2003 file|*.xls"
        Me.DlgOpenUploader.Title = "Select an uploader"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LinkLabel2.Location = New System.Drawing.Point(6, 398)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(256, 25)
        Me.LinkLabel2.TabIndex = 7
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Click Here to Generate the Required Template"
        Me.LinkLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.txtTitleLabel)
        Me.GroupBox1.Controls.Add(Me.BtnBrowse)
        Me.GroupBox1.Controls.Add(Me.txtUploader)
        Me.GroupBox1.Controls.Add(Me.btnUpload)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(262, 242)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(9, 44)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(235, 52)
        Me.txtDescription.TabIndex = 6
        Me.txtDescription.Text = "Upload Masterfile here. Make sure you follow the template required for this uploa" & _
            "der."
        '
        'txtTitleLabel
        '
        Me.txtTitleLabel.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTitleLabel.Location = New System.Drawing.Point(6, 19)
        Me.txtTitleLabel.Name = "txtTitleLabel"
        Me.txtTitleLabel.Size = New System.Drawing.Size(238, 25)
        Me.txtTitleLabel.TabIndex = 5
        Me.txtTitleLabel.Text = "Uploader"
        Me.txtTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(952, 24)
        Me.MenuStrip1.TabIndex = 9
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuNew, Me.menuOpen, Me.menuClose})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'menuNew
        '
        Me.menuNew.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.menuNew.Name = "menuNew"
        Me.menuNew.Size = New System.Drawing.Size(154, 22)
        Me.menuNew.Text = "New Template"
        '
        'menuOpen
        '
        Me.menuOpen.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.menuOpen.Name = "menuOpen"
        Me.menuOpen.Size = New System.Drawing.Size(154, 22)
        Me.menuOpen.Text = "Open Source"
        '
        'menuClose
        '
        Me.menuClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.menuClose.Name = "menuClose"
        Me.menuClose.Size = New System.Drawing.Size(154, 22)
        Me.menuClose.Text = "Close Uploader"
        '
        'frmMasterUploader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(952, 432)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LinkLabel2)
        Me.Controls.Add(Me.GrdUploader)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMasterUploader"
        Me.ShowInTaskbar = False
        Me.Text = "Uploader"
        CType(Me.GrdUploader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtUploader As System.Windows.Forms.TextBox
    Friend WithEvents BtnBrowse As System.Windows.Forms.Button
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents GrdUploader As System.Windows.Forms.DataGridView
    Friend WithEvents DlgSaveUploader As System.Windows.Forms.SaveFileDialog
    Friend WithEvents DlgOpenUploader As System.Windows.Forms.OpenFileDialog
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTitleLabel As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuClose As System.Windows.Forms.ToolStripMenuItem
End Class
