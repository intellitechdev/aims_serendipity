'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: October 13, 2010                   ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################
'                   ########################################################
'                   ### Updated by: Josh "Palladium" Dadulla             ###
'                   ### Date Created: 9/16/2012                          ###
'                   ### Website: http://divergentsociety.net             ###
'                   ########################################################
'                   ########################################################
'                   ### Disastered by: JB "SalingKET-KET" Barcelon       ###
'                   ### Date Created: 2/21/2013                          ###
'                   ### Website: http://www.?!*c",).net                  ###
'                   ########################################################
'


Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text

Public Class FrmMasterUploaderProcess

    Private gCon As New Clsappconfiguration
    Public xParent As frmMasterUploader
    Private xCount As Integer = 0
    Private CurrentItem As String = ""
    Private XCurrentRowIndex As Integer = 0
    Private Process As Boolean
    Private CurrentValidation As String = ""
    Public DTValidatedItem As New DataTable("ValidatedItems")
    Private rowIndex As Integer
    Private columnName As String
    Private fientry As String
    Private user As String
    Public Property GetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property


    Private Function ValidateLoanNo() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "LoanNo"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("LoanNo")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            xCount += 1
            CurrentItem = "validating " & xRow.Cells(1).Value.ToString
            XCurrentRowIndex = xRow.Index
            sSqlCmd = "SELECT	pk_Members_Loan " & _
                    "FROM dbo.CIMS_t_Member_Loans " & _
                    "WHERE	fnLoanNo = '" & xRow.Cells(1).Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells(1).Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("LoanNo") = xRow.Cells(1).Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateLoanNo in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ValidateTerms() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "Terms"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("Terms")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            xCount += 1
            CurrentItem = "validating " & xRow.Cells("Terms").Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT fxKeyTerms " & _
                        "FROM dbo.mTerms " & _
                        "WHERE fcTermsName = '" & xRow.Cells("Terms").Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Terms").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("Terms") = xRow.Cells("Terms").Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at Validate Terms in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occurred.")
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ValidateLoanType() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "LoanType"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("LoanType")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            xCount += 1
            CurrentItem = "validating " & xRow.Cells("Loan Type").Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT pk_LoanType " & _
                        "FROM dbo.CIMS_m_Loans_LoanType " & _
                        "WHERE fcLoanTypeName = '" & xRow.Cells("Loan Type").Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Loan Type").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("LoanType") = xRow.Cells("Loan Type").Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateLoanType in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function IsValidAccount()
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "Accounts"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("Accounts")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            xCount += 1
            CurrentItem = "validating " & xRow.Cells("Account Code").Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT acnt_id " & _
                        "FROM dbo.mAccounts " & _
                        "WHERE acnt_code = '" & xRow.Cells("Account Code").Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Account Code").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("Accounts") = xRow.Cells("Account Code").Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateLoanType in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ValidateLoanStatus() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "LoanStatus"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("LoanStatus")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            xCount += 1
            CurrentItem = "validating " & xRow.Cells("Loan Status").Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT pk_Status " & _
                        "FROM dbo.CIMS_m_Loans_Status " & _
                        "WHERE fcStatusName = '" & xRow.Cells("Loan Status").Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Loan Status").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("LoanStatus") = xRow.Cells("Loan Status").Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                Throw ex
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ValidateRequiredField_JournalEntries() As Boolean
        Dim isValid As Boolean

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            If xRow.IsNewRow = False Then
                If xRow.Cells("Account Code").Value.ToString() = "" Then
                    isValid = False

                    rowIndex = xRow.Index
                    columnName = "Account Code"

                    Exit Function
                ElseIf xRow.Cells("Account Name").Value.ToString() = "" Then
                    isValid = False

                    rowIndex = xRow.Index
                    columnName = "Account Name"

                    Exit Function
                ElseIf xRow.Cells("Date").Value.ToString() = "" Then
                    isValid = False

                    rowIndex = xRow.Index
                    columnName = "Date"

                    Exit Function
                ElseIf xRow.Cells("Particulars").Value.ToString() = "" Then
                    isValid = False

                    rowIndex = xRow.Index
                    columnName = "Particulars"

                    Exit Function
                Else
                    isValid = True
                End If
            End If
        Next

        Return isValid
    End Function

    Private Sub ValidateEmployee()
        Dim sSqlCmd As String
        Dim DR As DataRow

        CurrentValidation = "EmpNo"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("EmpNo")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            xCount += 1
            CurrentItem = "validating " & xRow.Cells(1).Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT pk_Employee " & _
                        "FROM dbo.CIMS_m_Member " & _
                        "WHERE cast(fcEmployeeNo as nvarchar) = CAST('" & xRow.Cells(1).Value.ToString & "' AS nvarchar)"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        DR = DTValidatedItem.NewRow()
                        DR("EmpNo") = xRow.Cells(1).Value.ToString
                        DTValidatedItem.Rows.Add(DR)
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateEmployee in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
                Process = False
            End Try
        Next
    End Sub
    Private Sub generateFIENTRY()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "createNewFIENTRYNO")
        Try
            While rd.Read
                fientry = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UploadMasterfiles()
        Dim compID As String = gCompanyID()
        Dim user As String = GetUser()
        Dim keyJVID As String = Guid.NewGuid.ToString
        generateFIENTRY()
        Select Case xParent.xMode
            Case "Journal Entries"
                'JV Header
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_JournalEntries_Header_Upload", _
                        New SqlParameter("@fxKeyJVNo", keyJVID), _
                        New SqlParameter("@fxKeyCompany", compID), _
                        New SqlParameter("@fdTransDate", xParent.GrdUploader.Item("Date", 0).Value), _
                        New SqlParameter("@particulars", xParent.GrdUploader.Item("Particulars", 0).Value.ToString()), _
                        New SqlParameter("@debit", xParent.GrdUploader.Item("Debit", 0).Value), _
                        New SqlParameter("@credit", xParent.GrdUploader.Item("Credit", 0).Value), _
                        New SqlParameter("@fdPosted", True), _
                        New SqlParameter("@subsidiaryNo", xParent.GrdUploader.Item("Subsidiary No", 0).Value), _
                        New SqlParameter("@user", user), _
                        New SqlParameter("@doctype", xParent.GrdUploader.Item("Doctype", 0).Value), _
                        New SqlParameter("@fientry", fientry))

                Catch ex As Exception
                    Throw ex
                    Process = False
                End Try

        End Select

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            xCount += 1
            XCurrentRowIndex = xRow.Index

            Select Case xParent.xMode
                Case "Journal Entries"
                    'JV Details
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_JournalEntries_Details_Upload", _
                            New SqlParameter("@fk_tJVEntry", keyJVID), _
                            New SqlParameter("@accountCode", xRow.Cells("Account Code").Value.ToString), _
                            New SqlParameter("@dtDate", xRow.Cells("Date").Value.ToString), _
                            New SqlParameter("@fdDebit", xRow.Cells("Debit").Value.ToString), _
                            New SqlParameter("@fdCredit", xRow.Cells("Credit").Value.ToString), _
                            New SqlParameter("@memo", xRow.Cells("Particulars").Value.ToString), _
                            New SqlParameter("@subsidiaryNo", xRow.Cells("Subsidiary No").Value.ToString), _
                            New SqlParameter("@referenceNo", xRow.Cells("Reference No").Value.ToString), _
                            New SqlParameter("@InHouse", xRow.Cells("In House").Value.ToString))
                    Catch ex As Exception
                        Throw ex
                        Process = False
                    End Try

                Case "Chart of Accounts"
                    Dim accountID As String = Guid.NewGuid.ToString
                    Dim accountSub As Object = xRow.Cells("Sub Account of").Value
                    Dim isAccountSub As Boolean

                    'Sub Account Code
                    If accountSub Is DBNull.Value Then
                        isAccountSub = False
                    Else
                        isAccountSub = True
                        accountSub = accountSub.ToString()
                    End If

                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_ChartOfAccounts_Upload", _
                                                New SqlParameter("@compID", compID), _
                                                New SqlParameter("@accountID", accountID), _
                                                New SqlParameter("@fcLevel", xRow.Cells("Level").Value.ToString), _
                                                New SqlParameter("@acnt_code", xRow.Cells("New Account Codes").Value.ToString), _
                                                New SqlParameter("@acnt_name", xRow.Cells("Account Description").Value.ToString), _
                                                New SqlParameter("@acnt_desc", xRow.Cells("Account Description").Value.ToString), _
                                                New SqlParameter("@acnt_type", xRow.Cells("Account Type").Value.ToString), _
                                                New SqlParameter("@fbActive", xRow.Cells("Active").Value.ToString), _
                                                New SqlParameter("@acnt_sub", isAccountSub), _
                                                New SqlParameter("@acnt_subof", accountSub), _
                                                New SqlParameter("@user", user))
                    Catch ex As Exception
                        Throw ex
                        Process = False
                    End Try


                Case "Customer List"
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_Customer_Upload", _
                                                  New SqlParameter("@companyID", compID), _
                                                  New SqlParameter("@customerCode", xRow.Cells("Customer Code").Value.ToString()), _
                                                  New SqlParameter("@fcCustomerName", xRow.Cells("Customer Name").Value), _
                                                  New SqlParameter("@fcCompanyName", xRow.Cells("Company Name").Value), _
                                                  New SqlParameter("@fcSalutation", xRow.Cells("Salutation").Value), _
                                                  New SqlParameter("@fcLastName", xRow.Cells("Last Name").Value), _
                                                  New SqlParameter("@fcFirstName", xRow.Cells("First Name").Value), _
                                                  New SqlParameter("@fcMidName", xRow.Cells("Middle Name").Value), _
                                                  New SqlParameter("@accountRecvbleCode", xRow.Cells("Accounts Receivable (Acnt_Code)").Value.ToString()), _
                                                  New SqlParameter("@accountSalesDscntCode", xRow.Cells("Sales Discount Account (Acnt_Code)").Value.ToString()), _
                                                  New SqlParameter("@fcPrimaryAddress", xRow.Cells("Primary Address").Value), _
                                                  New SqlParameter("@fcContactPerson1", xRow.Cells("Contact Person1").Value), _
                                                  New SqlParameter("@fcContactPerson2", xRow.Cells("Contact Person 2").Value), _
                                                  New SqlParameter("@fcTelNo1", xRow.Cells("Tel No 1").Value.ToString()), _
                                                  New SqlParameter("@fcTelNo2", xRow.Cells("Tel No 2").Value.ToString()), _
                                                  New SqlParameter("@fcFaxNo", xRow.Cells("Fax No").Value.ToString()), _
                                                  New SqlParameter("@fcEmail", xRow.Cells("Email").Value), _
                                                  New SqlParameter("@fcAccountNo", xRow.Cells("Bank Account No").Value), _
                                                  New SqlParameter("@fcNote", xRow.Cells("Note").Value), _
                                                  New SqlParameter("@fcCreditCardNo", xRow.Cells("CreditCard No").Value.ToString()), _
                                                  New SqlParameter("@fcNameOnCard", xRow.Cells("Name On Card").Value), _
                                                  New SqlParameter("@fcExpirationYear", xRow.Cells("Expiration Year").Value), _
                                                  New SqlParameter("@fcExpirationMonth", xRow.Cells("Expiration Month").Value), _
                                                  New SqlParameter("@fcAddressOnCard", xRow.Cells("Address On Card").Value), _
                                                  New SqlParameter("@customerType", xRow.Cells("Customer Type").Value), _
                                                  New SqlParameter("@termsName", xRow.Cells("Terms Name").Value), _
                                                  New SqlParameter("@salesRepName", xRow.Cells("Sales Rep").Value), _
                                                  New SqlParameter("@salesTaxCodeName", xRow.Cells("Sales Tax Code Name").Value.ToString()))
                    Catch ex As Exception
                        Process = False
                        Throw ex
                    End Try


                Case "Supplier List"
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "Masterfile_Supplier_Upload", _
                                                  New SqlParameter("@companyID", compID), _
                                                  New SqlParameter("@supplierCode", xRow.Cells("Supplier Code").Value.ToString()), _
                                                  New SqlParameter("@fcSupplierName", xRow.Cells("Supplier Name").Value.ToString()), _
                                                  New SqlParameter("@fcCompanyName", xRow.Cells("Company Name").Value.ToString()), _
                                                  New SqlParameter("@fcSalutation", xRow.Cells("Salutation").Value.ToString()), _
                                                  New SqlParameter("@fcLastName", xRow.Cells("Last Name").Value.ToString()), _
                                                  New SqlParameter("@fcFirstName", xRow.Cells("First Name").Value.ToString()), _
                                                  New SqlParameter("@fcMidName", xRow.Cells("Middle Name").Value.ToString()), _
                                                  New SqlParameter("@fcPrimaryAddress", xRow.Cells("Main Address").Value.ToString()), _
                                                  New SqlParameter("@fcAccountNo", xRow.Cells("Account No").Value.ToString()), _
                                                  New SqlParameter("@TIN", xRow.Cells("TIN").Value.ToString()), _
                                                  New SqlParameter("@creditLimit", xRow.Cells("Credit Limit").Value), _
                                                  New SqlParameter("@fcContactPerson1", xRow.Cells("Contact Person 1").Value.ToString()), _
                                                  New SqlParameter("@fcContactPerson2", xRow.Cells("Contact Person 2").Value.ToString()), _
                                                  New SqlParameter("@fcTelNo1", xRow.Cells("Tel No 1").Value.ToString()), _
                                                  New SqlParameter("@fcTelNo2", xRow.Cells("Tel No 2").Value.ToString()), _
                                                  New SqlParameter("@fcFaxNo", xRow.Cells("Fax No").Value.ToString()), _
                                                  New SqlParameter("@fcEmail", xRow.Cells("Email Address").Value.ToString()), _
                                                  New SqlParameter("@fcPrintCheck", xRow.Cells("Name on Printed Check").Value.ToString()), _
                                                  New SqlParameter("@fcNote", xRow.Cells("Note").Value.ToString()), _
                                                  New SqlParameter("@accountPayableCode", xRow.Cells("Accounts Payable (acnt_code)").Value.ToString()), _
                                                  New SqlParameter("@supplierType", xRow.Cells("Supplier Type").Value.ToString()), _
                                                  New SqlParameter("@termsName", xRow.Cells("Terms").Value.ToString()), _
                                                  New SqlParameter("@CostCenter", xRow.Cells("Cost Center").Value))

                    Catch ex As Exception
                        Process = False
                        Throw ex
                    End Try
            End Select
        Next
        Process = True
    End Sub

    Private Sub FrmLoanMasterUploaderProcess_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            With xParent.GrdUploader
                ProgressBar.Maximum = .Rows.Count * 3
                If .AllowUserToAddRows = True Then
                    .AllowUserToAddRows = False
                End If
                Timer.Enabled = True
                BGWorker.RunWorkerAsync()
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Private Function ValidateChart()

    Private Sub BGWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker.DoWork
        Try
            Select Case xParent.xMode
                Case "Journal Entries"
                    If ValidateRequiredField_JournalEntries() = False Then
                        Process = False
                    Else
                        If IsValidAccount() = True Then
                            Call UploadMasterfiles()
                            Process = True
                        Else
                            Process = False
                        End If
                    End If

                Case "Chart of Accounts"
                    Call UploadMasterfiles()

                Case "Customer List"
                    If ValidateTerms() = True Then
                        Call UploadMasterfiles()
                    Else
                        Process = False
                    End If

                Case "Supplier List"
                    If ValidateTerms() = True Then
                        Call UploadMasterfiles()
                    Else
                        Process = False
                    End If

                Case "Loan Master"
                    If ValidateLoanType() = True Then
                        If ValidateLoanStatus() = True Then
                            ValidateEmployee()
                            If DTValidatedItem.Rows.Count = 0 Then
                                Call UploadMasterfiles()
                            Else
                                Process = False
                            End If
                        End If
                    Else
                        Process = False
                    End If
                Case "Loan Payment"
                    If ValidateLoanType() = True Then
                        If ValidateLoanNo() = True Then
                            UploadMasterfiles()
                        Else
                            Process = False
                        End If
                    Else
                        Process = False
                    End If
                Case "Member Contribution"
                    Call ValidateEmployee()
                    Call UploadMasterfiles()


            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Data Migration Tool", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BGWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker.RunWorkerCompleted
        If Process = True Then
            MessageBox.Show("Uploading Successful", xParent.xMode, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            If DTValidatedItem.Rows.Count <> 0 Then
                Dim xForm As New FrmMasterValidationResult
                xForm.xParent = Me
                xForm.xFieldValidated = CurrentValidation
                xForm.ShowDialog()
            Else
                MessageBox.Show("Uploading Failed!", xParent.xMode, MessageBoxButtons.OK, MessageBoxIcon.Error)

                With frmMasterUploader
                    .GrdUploader.Rows(rowIndex).DefaultCellStyle.BackColor = Color.Red
                    .GrdUploader.CurrentCell() = frmMasterUploader.GrdUploader.Item(columnName, rowIndex)
                End With
            End If
        End If


        With xParent.GrdUploader
            If .AllowUserToAddRows = False Then
                .AllowUserToAddRows = True
            End If
            Timer.Enabled = False
        End With
        Me.Close()
    End Sub

    Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick
        ProgressBar.Value = xCount
        LblProcess.Text = CurrentItem
        LblCounter.Text = XCurrentRowIndex & " of " & ProgressBar.Maximum
    End Sub
End Class