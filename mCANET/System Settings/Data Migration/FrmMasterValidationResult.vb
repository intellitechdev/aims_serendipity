'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: October 13, 2010                   ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Public Class FrmMasterValidationResult

    Public xParent As FrmMasterUploaderProcess
    Public xFieldValidated As String = ""

    Private Sub FrmLoanMasterValidationResult_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case xFieldValidated
            Case "EmpNo"
                txtValidation.Text = "USER ERROR! Please check the following EMPLOYEE No before uploading. Records do not exist."
            Case "LoanType"
                txtValidation.Text = "USER ERROR! Please check the following LOAN TYPE before uploading. Records do not exist."
            Case "LoanNo"
                txtValidation.Text = "USER ERROR! Please check the following LOAN NUMBER before uploading. Records do not exist."
            Case "LoanStatus"
                txtValidation.Text = "USER ERROR! Please check the following LOAN STATUS before uploading. Records do not exist."
            Case "Terms"
                txtValidation.Text = "USER ERROR! Please check the following TERMS before uploading. Records do not exist."
            Case "Accounts"
                txtValidation.Text = "USER ERROR! Please check the following ACCOUNT CODES AND ACCOUNT NAMES before uploading. Records do not exist."""


        End Select

        Try
            With xParent.DTValidatedItem
                For Each xRow As DataRow In .Rows
                    listValidation.Items.Add(xRow(0).ToString)
                Next
            End With
        Catch ex As Exception
            MsgBox("Error at FrmLoanMasterValidationResult_Load in FrmLoanMasterValidationResult module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
        End Try

    End Sub

    Private Sub FrmLoanMasterValidationResult_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub FrmLoanMasterValidationResult_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub
End Class