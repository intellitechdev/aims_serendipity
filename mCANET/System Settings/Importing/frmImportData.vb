
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Data.OleDb
Imports CSAcctg.clsReadCSV

Public Class frmImportData

#Region "Declarations"

    Private pSwitch As Integer = 1
    Private gTrans As New clsTransactionFunctions
    Private gMaster As New modMasterFile
    Private txtbox As New TextBox

    'Connection to Database
    Private gCon As New Clsappconfiguration

    'Data Table for Uploaders
    Private AccountType As DataTable
    Private Account As DataTable
    Private SupplierMaster As DataTable
    Private StoreItemSource As DataTable

    'Item File Path
    Private ItemMasterFilePath As String

    'Check if Loading has been successful
    Private isloadFileSuccessful As Boolean = False

#End Region

#Region "Form Events"

    Private Sub frmImportData_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bgwMainUploader.IsBusy Then
            MsgBox("Please Do not Close while Uploading." & vbNewLine & "This will cause damage to your database.")
            e.Cancel = True
        ElseIf bgwStoreUploader.IsBusy Then
            MsgBox("Please Do not Close while Uploading." & vbNewLine & "This will cause damage to your database.")
            e.Cancel = True
        ElseIf bgwUpdate.IsBusy Then
            MsgBox("Please Do not Close while Uploading." & vbNewLine & "This will cause damage to your database.")
            e.Cancel = True
        End If
    End Sub
    Private Sub frmImportData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cleanTemporaryTables()
    End Sub

#End Region

#Region "Functions/Procedures"

    Private Sub getExcelPath(ByVal xVal As Integer)
        If pSwitch = 1 Then
            pSwitch = 0
            Call gTrans.removelink_server()
            Call loadpath(xVal)
        ElseIf pSwitch = 0 Then
            pSwitch = 1
            Call loadpath(xVal)
        End If
    End Sub
    Private Sub verifygrid(ByVal xVal As Integer)
        Select Case xVal
            Case 1
                If gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TSAPATH, 'SELECT * FROM [Chart of Accounts$]')", grdImport) = "Successful..." Then
                    pSwitch = 1
                Else
                    pSwitch = 0
                End If
            Case 2
                If gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TSAPATH, 'SELECT * FROM [Customer Master$]')", grdImport) = "Successful..." Then
                    pSwitch = 1
                Else
                    pSwitch = 0
                End If
            Case 3

                If gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TSAPATH, 'SELECT * FROM [Sheet1$]')", grdImport) = "Successful..." Then
                    pSwitch = 1
                Else
                    pSwitch = 0
                End If
            Case 4
                'Call gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TSAPATH, 'SELECT * FROM [Chart of Accounts$]')")
        End Select
    End Sub
    Private Sub loadpath(ByVal xVal As Integer)
        If Me.openfile.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtbox.Text = Me.openfile.FileName
            If gTrans.makelink_server(txtbox.Text) = "Successful..." Then
                Call verifygrid(xVal)
            Else
                MsgBox("Please Import excel file again...", MsgBoxStyle.Information, Me.Text)
            End If
        Else
            pSwitch = 0
        End If
    End Sub
    Private Sub saveChartofAccounts()
        Dim rowCount As Integer
        Dim acnttype As String
        Dim account As String = ""
        Dim subaccnt As Integer = 0
        Dim description As String = ""
        Dim seg1 As String = ""
        Dim seg2 As String = ""
        Dim seg3 As String = ""
        Dim seg4 As String = ""
        Dim seg5 As String = ""
        With grdImport
            If .RowCount > 0 Then
                For rowCount = 0 To .RowCount - 1
                    If gTrans.gmkDefaultValues(rowCount, 0, grdImport) <> Nothing Then
                        If getAcntTypeID(.Rows(rowCount).Cells(0).Value) <> "" Then
                            acnttype = getAcntTypeID(.Rows(rowCount).Cells(0).Value)
                            If gTrans.gmkDefaultValues(rowCount, 1, grdImport) <> Nothing Then
                                account = .Rows(rowCount).Cells(1).Value
                            End If
                            If gTrans.gmkDefaultValues(rowCount, 2, grdImport) <> Nothing Then
                                subaccnt = CInt(.Rows(rowCount).Cells(2).Value)
                            End If
                            If gTrans.gmkDefaultValues(rowCount, 3, grdImport) <> Nothing Then
                                description = .Rows(rowCount).Cells(3).Value
                            End If
                            If gTrans.gmkDefaultValues(rowCount, 4, grdImport) <> Nothing Then
                                seg1 = .Rows(rowCount).Cells(4).Value
                            End If
                            If gTrans.gmkDefaultValues(rowCount, 5, grdImport) <> Nothing Then
                                seg2 = .Rows(rowCount).Cells(5).Value
                            End If
                            If gTrans.gmkDefaultValues(rowCount, 6, grdImport) <> Nothing Then
                                seg3 = .Rows(rowCount).Cells(6).Value
                            End If
                            If gTrans.gmkDefaultValues(rowCount, 7, grdImport) <> Nothing Then
                                seg4 = .Rows(rowCount).Cells(7).Value
                            End If
                            'If gTrans.gmkDefaultValues(rowCount, 8, grdImport) <> Nothing Then
                            '    seg5 = .Rows(rowCount).Cells(8).Value
                            'End If
                            Call gMaster.gSaveAccountfromGrid(acnttype, account, description, CStr(seg1) + "-" + CStr(seg2) + "-" + CStr(seg3) + "-" + CStr(seg4), Me) ' + "-" + CStr(seg5), Me)
                        Else
                            gMaster.gSaveAcntTypefromGrd(.Rows(rowCount).Cells(0).Value, .Rows(rowCount).Cells(0).Value, _
                                                         .Rows(rowCount).Cells(4).Value.ToString + "" + defineNoCharacters(), Me)
                        End If
                    End If
                Next
            End If
        End With
    End Sub
    Private Sub saveCustomerMaster()
        With grdImport

        End With
    End Sub
    Private Sub cleanTemporaryTables()
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, "DELETE FROM mTemporaryUpload")
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, "DELETE FROM dbo.mTempStoreItems")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub
    'Private Function definefirstsegment(ByVal tmpVal As String) As String
    '    Dim cLength As Integer
    '    Dim tmpResult As String = ""
    '    Dim result As String
    '    For cLength = 0 To tmpVal.Length - 1
    '        If cLength < 2 Then
    '            tmpResult += tmpVal(cLength).ToString
    '        End If
    '    Next
    '    result = tmpResult + "00"
    '    Return result
    'End Function
    Private Function defineNoCharacters() As String
        Dim result As String = ""
        Dim xCnt As Integer = 0
        Dim sSQL As String = "Select seg_sequence, seg_charsize from dbo.mSegment where co_id = '" & gCompanyID() & "' "

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                If xCnt > 0 Then
                    result += createsegmentdefualtvalue(rd.Item(1))
                End If
                xCnt += 1
            End While
        End Using

        Return result
    End Function
    Private Function createsegmentdefualtvalue(ByVal xVal As Integer) As String
        Dim xCnt As Integer
        Dim result As String = ""
        For xCnt = 0 To xVal - 1
            result += "0"
        Next
        result = "-" + result
        Return result
    End Function
    Private Function GetDataTableFromFile(ByVal filePath As String, _
                                   ByVal separator As Char, _
                                   ByVal numberOfFields As Integer, _
                                   Optional ByVal tableName As String = "myTable") _
                                   As DataTable
        Dim dt As DataTable = Nothing
        Dim sr As System.IO.StreamReader = Nothing
        Dim columnFields As System.IO.StreamReader = Nothing
        Dim count As Integer

        If System.IO.File.Exists(filePath) Then
            Try
                dt = New DataTable(tableName)

                'Add columns to datatable
                columnFields = New System.IO.StreamReader(filePath)
                Dim header() As String = Nothing

                'Insert the Column Header
                header = columnFields.ReadLine().Split(separator)
                For i As Integer = 0 To header.Length - 1
                    dt.Columns.Add(New DataColumn(header(i)))
                Next

                'Open text file
                sr = New System.IO.StreamReader(filePath)
                Dim fields() As String
                While sr.Peek <> -1

                    fields = sr.ReadLine().Split(separator)
                    Dim row As DataRow = dt.NewRow()

                    'Assign values to each fields on the new row
                    If count <> 0 Then
                        For j As Integer = 0 To fields.Length - 1
                            row.Item(j) = fields(j)
                        Next
                        'Add the new row to datatable
                        dt.Rows.Add(row)
                    End If

                    count += 1
                End While

            Catch ex As Exception
                'If any error occurs during the process, display it
                MsgBox(ex.Message)
                Return Nothing

            Finally
                'Close the stream
                If Not IsNothing(sr) Then
                    sr.Close()
                End If
            End Try
        Else
            MsgBox("File Not Found")
        End If
        Return dt
    End Function
   

    Private Sub callOverloads()
        Call overload1("charl")
        Call overload1("charl", "charl2")
    End Sub

    Private Sub overload1(ByVal param1 As String)

    End Sub

    Private Sub overload1(ByVal param1 As String, ByVal param2 As String)

    End Sub

#End Region

#Region "Chart Of Accounts Upload"

    Private Sub MenuAccountTypeBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuAccountTypeBrowse.Click
        Dim _r As clsReadCSV

        Try
            openfile.Filter = "Text files (*.csv)|*.csv| All files|*.*"
            openfile.ShowDialog()

            If openfile.FileName <> "" Then
                _r = New clsReadCSV()
                _r.SetTable = FileType.AccountType
                _r.SetFile = openfile.FileName
                _r.ReadFile()

                AccountType = _r.GetData
                grdImport.DataSource = AccountType
            End If

            MenuUploadAccount_type.Enabled = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub MenuAccountTypeUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuUploadAccount_type.Click
        Dim CnString As New Clsappconfiguration
        Dim _b As SqlBulkCopy

        Try
            _b = New SqlBulkCopy(CnString.cnstring)
            _b.ColumnMappings.Add("Company ID", "co_id")
            _b.ColumnMappings.Add("Account Type", "acnt_type")
            _b.ColumnMappings.Add("Account Description", "description")
            _b.ColumnMappings.Add("Segment Value", "seg_value")
            _b.ColumnMappings.Add("Created By", "created_by")
            _b.ColumnMappings.Add("Date Created", "date_created")
            _b.ColumnMappings.Add("Updated By", "updated_by")
            _b.ColumnMappings.Add("Date Updated", "date_updated")
            _b.DestinationTableName = "mAccountType"
            _b.WriteToServer(AccountType)
            _b.Close()

            MessageBox.Show("Uploading complete!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub MenuBrowseAccounts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuBrowseAccounts.Click
        Dim _r As clsReadCSV

        Try
            openfile.Filter = "Text files (*.csv)|*.csv| All files|*.*"
            openfile.ShowDialog()

            If openfile.FileName <> "" Then
                _r = New clsReadCSV()
                _r.SetTable = FileType.CharOfAccounts
                _r.SetFile = openfile.FileName
                _r.ReadFile()

                Account = _r.GetData
                grdImport.DataSource = Account

                MenuUploadAccounts.Enabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub MenuImportAccounts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuUploadAccounts.Click

        Dim xRow As Integer = 0
        Dim levelDept As Integer = 0
        Dim DiggerDept As Integer = 1
        Dim Op_acnt_Note As String
        Dim Op_Createdby As String
        Dim Op_DateCreated As Date
        Dim Op_UpdateBy As String
        Dim Op_DateUpdated As Date
        For Each ItemRow As DataGridViewRow In grdImport.Rows
            xRow = ItemRow.Index
            If levelDept < grdImport.Item("Level", xRow).Value Then
                levelDept = grdImport.Item("Level", xRow).Value
            End If
        Next
       
        Do Until DiggerDept > levelDept
            xRow = 0
            For Each Item As DataGridViewRow In grdImport.Rows
                xRow = Item.Index
                If grdImport.Item("Level", xRow).Value = DiggerDept Then

                    Dim xAccountType As String
                    Dim acnt_type As String = grdImport.Item("Account type", xRow).Value
                    Dim acnt_name As String = grdImport.Item("account name", xRow).Value
                    Dim fbActive As Decimal
                    If grdImport.Item("Active", xRow).Value = True Then
                        fbActive = 1
                    Else
                        fbActive = 0
                    End If
                    Dim acnt_sub As Decimal
                    If grdImport.Item("Account sub", xRow).Value = True Then
                        acnt_sub = 1
                    Else
                        acnt_sub = 0
                    End If
                    Dim acnt_desc As String = grdImport.Item("account description", xRow).Value
                    Dim acnt_note As String = grdImport.Item("account note", xRow).Value
                    Dim acnt_code As String = grdImport.Item("account code", xRow).Value
                    Dim acnt_deleted As Decimal
                    If grdImport.Item("account deleted", xRow).Value = True Then
                        acnt_deleted = 1
                    Else
                        acnt_deleted = 0
                    End If
                    Dim acnt_balance As Decimal = grdImport.Item("account balance", xRow).Value
                    Dim acnt_balance_less As Decimal = grdImport.Item("account balance less", xRow).Value
                    Dim acnt_baldate As DateTime = grdImport.Item("account balance date", xRow).Value
                    Dim created_by As String = Convert.ToString(grdImport.Item("Created by", xRow).Value)
                    Dim date_created As DateTime
                    If IsDBNull(grdImport.Item("date created", xRow).Value) = False Then
                        date_created = grdImport.Item("date created", xRow).Value
                    End If
                    Dim updated_by As String = convert.ToString(grdImport.Item("updated by", xRow).Value)
                    Dim date_updated As DateTime
                    If IsDBNull(grdImport.Item("date update", xRow).Value) = False Then
                        date_updated = grdImport.Item("date update", xRow).Value
                    End If

                    Dim sSqlGetAccountType As String = "SELECT actType_id FROM dbo.mAccountType WHERE acnt_type ='" & _
                                                        acnt_type & "' AND co_id ='" & gCompanyID() & "'"
                    Try
                        Using rd1 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlGetAccountType)
                            While rd1.Read
                                xAccountType = rd1.Item(0).ToString
                            End While
                        End Using
                    Catch ex As Exception
                    End Try
                    Dim xSubAccountOf As String
                    Dim acnt_subof As String = grdImport.Item("Account sub of", xRow).Value
                    'MsgBox(acnt_subof)
                    Dim sSqlGetSubAccntOf As String = "SELECT acnt_id FROM dbo.mAccounts WHERE acnt_code ='" & _
                                                        acnt_subof & "' AND co_id ='" & gCompanyID() & "'"
                    Try
                        Using rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlGetSubAccntOf)
                            While rd2.Read
                                xSubAccountOf = rd2.Item(0).ToString
                            End While
                        End Using
                    Catch ex As Exception
                    End Try

                    Dim sSqlUploadToChartOfAccount As String = "INSERT INTO dbo.mAccounts "
                    sSqlUploadToChartOfAccount &= "(co_id"
                    If xAccountType <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", acnt_type"
                    End If
                    sSqlUploadToChartOfAccount &= ", acnt_name, fbActive, acnt_sub, "
                    If xSubAccountOf <> Nothing Then
                        sSqlUploadToChartOfAccount &= "acnt_subof, "
                    End If
                    sSqlUploadToChartOfAccount &= "acnt_desc, acnt_note, acnt_code, acnt_deleted, " & _
                                                    "acnt_balance, acnt_balance_less, acnt_baldate "
                    If created_by <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", created_by "
                    End If
                    If date_created <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", date_created "
                    End If
                    If updated_by <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", updated_by "
                    End If
                    If date_updated <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", date_updated"
                    End If
                    sSqlUploadToChartOfAccount &= ") "
                    sSqlUploadToChartOfAccount &= "VALUES ('" & gCompanyID()
                    If IsDBNull(xAccountType) = False Then
                        sSqlUploadToChartOfAccount &= "', '" & xAccountType
                    End If
                    sSqlUploadToChartOfAccount &= "', '" & acnt_name & "', " & fbActive & ", " & acnt_sub

                    If xSubAccountOf <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", '" & xSubAccountOf & "'"
                    End If
                    sSqlUploadToChartOfAccount &= ", '" & acnt_desc & "', '" & acnt_note & "', '" & acnt_code & "', " & acnt_deleted & _
                                                    ", " & acnt_balance & ", " & acnt_balance_less & ", " & acnt_baldate
                    If created_by <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", '" & created_by
                    End If
                    If date_created <> Nothing Then
                        sSqlUploadToChartOfAccount &= "', " & date_created
                    End If
                    If updated_by <> Nothing Then
                        sSqlUploadToChartOfAccount &= ", '" & updated_by
                    End If
                    If date_updated <> Nothing Then
                        sSqlUploadToChartOfAccount &= "', " & date_updated
                    End If
                    sSqlUploadToChartOfAccount &= ")"
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlUploadToChartOfAccount)
                    Catch ex As Exception
                        MsgBox(ex.ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error at UploadtoChartofAccount")
                        Exit Sub
                    End Try




                End If
            Next
            DiggerDept += 1
        Loop
        MsgBox("Uploading Successful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "New Item/s has been uploaded")
        'Dim CnString As New Clsappconfiguration
        'Dim _b As SqlBulkCopy
        '    _b = New SqlBulkCopy(CnString.cnstring)
        '    _b.ColumnMappings.Add("1", "co_id")
        '    _b.ColumnMappings.Add("2", "acnt_type")
        '    _b.ColumnMappings.Add("3", "acnt_name")
        '    _b.ColumnMappings.Add("4", "fbActive")
        '    _b.ColumnMappings.Add("5", "acnt_sub")
        '    _b.ColumnMappings.Add("6", "acnt_subof")
        '    _b.ColumnMappings.Add("7", "acnt_desc")
        '    _b.ColumnMappings.Add("8", "acnt_note")
        '    _b.ColumnMappings.Add("9", "acnt_code")
        '    _b.ColumnMappings.Add("10", "acnt_deleted")
        '    _b.ColumnMappings.Add("11", "acnt_balance")
        '    _b.ColumnMappings.Add("12", "acnt_balance_less")
        '    _b.ColumnMappings.Add("13", "acnt_baldate")
        '    _b.ColumnMappings.Add("14", "created_by")
        '    _b.ColumnMappings.Add("15", "date_created")
        '    _b.ColumnMappings.Add("16", "updated_by")
        '    _b.ColumnMappings.Add("17", "date_updated")

        '    _b.DestinationTableName = "TEMPAccounts"
        '    _b.WriteToServer(Account)

        '    SqlHelper.ExecuteNonQuery(CnString.cnstring, CommandType.StoredProcedure, "usp_t_mAccounts")
        '    'Next
        '    'Loop
        '    MessageBox.Show("Uploading complete!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

#End Region
#Region "Supplier Master Upload"
    Private Sub MenuBrowseSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuBrowseSupplier.Click
        Dim _r As clsReadCSV

        Try
            openfile.Filter = "Text files (*.csv)|*.csv| All files|*.*"
            openfile.ShowDialog()

            If openfile.FileName <> "" Then
                _r = New clsReadCSV()
                _r.SetTable = FileType.Supplier
                _r.SetFile = openfile.FileName
                _r.ReadFile()

                SupplierMaster = _r.GetData
                grdImport.DataSource = SupplierMaster
                MenuUploadSupplier.Enabled = True

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub MenuUploadSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuUploadSupplier.Click
        Dim CnString As New Clsappconfiguration
        Dim _b As SqlBulkCopy

        Try
            _b = New SqlBulkCopy(CnString.cnstring)
            _b.ColumnMappings.Add("1", "fcSupplierName")
            _b.ColumnMappings.Add("2", "fdDateAsOf")
            _b.ColumnMappings.Add("3", "fcCompanyName")
            _b.ColumnMappings.Add("4", "fcSalutation")
            _b.ColumnMappings.Add("5", "fcLastName")
            _b.ColumnMappings.Add("6", "fcFirstName")
            _b.ColumnMappings.Add("7", "fcMidName")
            _b.ColumnMappings.Add("8", "fcAccountNo")
            _b.ColumnMappings.Add("9", "fcTIN")
            _b.ColumnMappings.Add("10", "fcContactPerson1")
            _b.ColumnMappings.Add("11", "fcContactPerson2")
            _b.ColumnMappings.Add("12", "fcTelNo1")
            _b.ColumnMappings.Add("13", "fcTelNo2")
            _b.ColumnMappings.Add("14", "fcFaxNo")
            _b.ColumnMappings.Add("15", "fcEmail")
            _b.ColumnMappings.Add("16", "fcPrintCheck")
            _b.ColumnMappings.Add("17", "fdCreditLimit")
            _b.ColumnMappings.Add("18", "fcNote")
            _b.ColumnMappings.Add("19", "fdAverage")
            _b.ColumnMappings.Add("20", "fbActive")
            _b.ColumnMappings.Add("21", "fxKeyCompany")
            _b.ColumnMappings.Add("22", "fxKeySupplierType")
            _b.ColumnMappings.Add("23", "fxKeyTerms")
            _b.ColumnMappings.Add("24", "fxKeyBillRate")
            _b.ColumnMappings.Add("25", "fdBalance")
            _b.ColumnMappings.Add("26", "fxKeyAddress")
            _b.DestinationTableName = "mSupplier00Master"
            _b.WriteToServer(SupplierMaster)

            MessageBox.Show("Uploading complete!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
#Region "Customer Master Upload"

    Private Sub CustomerMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CustomerMasterToolStripMenuItem.Click
        save_CustomerData()
    End Sub
    Private Sub MenuBrowseCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuBrowseCustomer.Click
        LoadCustomerFile()
    End Sub

    Private Sub LoadCustomerFile()
        Dim filepath As String

        If Me.openfile.ShowDialog = Windows.Forms.DialogResult.OK Then
            filepath = Me.openfile.FileName
        Else
            Exit Sub
        End If

        Dim gMasterfile As New modMasterFileUploads(filepath)
        Dim Source_Customer As DataSet = gMasterfile.retrieveDataFromExcel(filepath)

        With grdImport
            .DataSource = Source_Customer
            .AutoGenerateColumns = True
            .DataMember = "Source Data"
            .AutoResizeColumns()
        End With

    End Sub
    Private Sub save_CustomerData()
        Dim sSqlCommand As String
        Const customerid As Integer = 0
        Const customername As Integer = 1
        Const contactperson As Integer = 2
        Const telephoneno As Integer = 3
        Const resaleno As Integer = 4
        Dim Upload_Status As String = "Successful"

        Dim header(4) As String

        Dim xRow As Integer
        Dim index As Integer

        With Me.grdImport
            For xRow = 0 To .Rows.Count - 1
                If gTrans.gmkDefaultValues(xRow, 0, grdImport) <> Nothing Then

                    For index = 0 To header.Length - 1
                        If gTrans.gmkDefaultValues(xRow, 1, grdImport) <> Nothing Then
                            header(index) = .Rows(xRow).Cells(index).Value
                        End If
                    Next

                End If

                sSqlCommand = "usp_m_customerUpload_NPF "
                sSqlCommand &= "@customerID ='" & header(customerid) & "'"
                sSqlCommand &= ",@customerName ='" & header(customername) & "'"
                sSqlCommand &= ",@contactPerson ='" & header(contactperson) & "'"
                sSqlCommand &= ",@TelephoneNo ='" & header(telephoneno) & "'"
                sSqlCommand &= ",@resaleNo ='" & CInt(IIf(header(resaleno) = "", 0, header(resaleno))) & "'"
                sSqlCommand &= ",@currentUser ='" & gUserName & "'"
                sSqlCommand &= ",@fxKeyCompany ='" & gCompanyID() & "'"

                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCommand)
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error in Uploading")
                    Upload_Status = "Unsuccessful"
                    Exit For
                End Try
            Next
        End With

        If Upload_Status = "Successful" Then
            MsgBox("Upload Successful!", MsgBoxStyle.Information, "Customer Upload")
        Else
            MsgBox("Upload Unsucessful! Please check your File.", MsgBoxStyle.Information, "Customer Uploader")
        End If

    End Sub

#End Region
#Region "Item Master Upload"

    Private Sub MenuBrowseItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuBrowseItem.Click
        loadItemCSVFilePath()
        If isloadFileSuccessful = True Then
            frmupload.Close()
            MenuUploadItem.Enabled = True
        End If
    End Sub
    Private Sub MenuUploadItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuUploadItem.Click
        If MessageBox.Show("Make sure the Item Master File is free from" & vbNewLine & _
                           "special characters such as comma, apostrophe and quotes." & vbNewLine & _
                           "As well as Spaces around the dash in Color Codes." & vbNewLine & _
                           "Are you sure you want to continue?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) _
                           = MsgBoxResult.Ok Then
            frmupload.Show()
            pbUpload.Visible = True
            MenuUploadItem.Enabled = False
            MenuBrowseItem.Enabled = False
            bgwMainUploader.RunWorkerAsync()
        End If
    End Sub
    Private Sub BrowseStoreItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BrowseStoreItems.Click
        LoadItemExcelFileToGrid()
        If isloadFileSuccessful = True Then
            UploadStoreItems.Enabled = True
        End If
    End Sub
    Private Sub UploadStoreItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UploadStoreItems.Click
        If MessageBox.Show("Make Sure that the Data you will upload is correct and verified " & _
                vbNewLine & "according to the template in uploading Store Items." & _
                vbNewLine & " Are you sure you want to continue?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) _
                = MsgBoxResult.Ok Then
            pbUpload.Visible = True
            BrowseStoreItems.Enabled = False
            UploadStoreItems.Enabled = False
            bgwStoreUploader.RunWorkerAsync()
        End If
    End Sub

    Private Function getHeaderFromCSVSource(ByVal filepath As String, _
                                       ByVal separator As String) As String()

        Dim columnFields = New System.IO.StreamReader(filepath)
        Dim header() As String = Nothing

        header = columnFields.ReadLine().Split(separator)

        Return header

    End Function
    Private Function getHeaderFromDestination(ByVal mTemporaryUpload As String) As String()
        Dim sSQLcmd As String = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns" & _
                                " WHERE TABLE_NAME = '" & mTemporaryUpload & "'"
        Dim countCommand As String = "SELECT COUNT(COLUMN_NAME) FROM INFORMATION_SCHEMA.columns" & _
                                " WHERE TABLE_NAME = '" & mTemporaryUpload & "'"
        Dim NumberOfColumns As Integer

        Try
            Using rdCount As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, countCommand)
                If rdCount.Read Then
                    NumberOfColumns = CInt(rdCount.Item(0))
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        Dim Header(NumberOfColumns) As String
        Dim index As Integer = 0

        Try
            Using rdColumns As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLcmd)
                For index = 0 To Header.Length - 1
                    If rdColumns.Read() Then
                        Header(index) = rdColumns.Item(0).ToString
                    End If
                Next
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return Header
    End Function
    Private Function getHeaderFromExcelSource(ByVal ItemMasterFilePath As String) As String()
        Dim excelConnection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                "Data Source=" & ItemMasterFilePath & ";Extended Properties=" & _
                                "'Excel 8.0;HDR=Yes;IMEX=1'")
        excelConnection.Open()
        Dim TableSchema As DataTable = excelConnection.GetSchema("Columns")


        Dim Header(TableSchema.Rows.Count - 1) As String
        Dim index As Integer = 0

        For index = 0 To Header.Length - 1
            Header(index) = TableSchema.Rows(index)("Column_Name")

        Next

        excelConnection.Close()

        Return Header
    End Function

    Private Sub uploadToItemCodeTable()
        Dim xItemCode As String
        Dim xDescription As String
        Dim xRow As Integer = 0
        grdImport.SelectAll()
        For Each SelectedRows As DataGridViewRow In grdImport.SelectedRows
            xRow = SelectedRows.Index
            xItemCode = grdImport.Item(0, xRow).Value
            xDescription = grdImport.Item(1, xRow).Value
            m_saveitemcode(xItemCode, xDescription, Me)
        Next

        '###################################################################################
        'Dim sSQLcmd As String = "SELECT fcItemCode,fcDescription FROM " & mTemporaryUpload

        'Try
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLcmd)
        '        While rd.Read
        '            m_saveitemcode(rd.Item(0), rd.Item(1), Me)
        '        End While
        '    End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

    End Sub
    Private Sub uploadToItemBrandTable() '(ByVal mTemporaryUpload As String)
        Dim xBrands As String
        Dim xRow As Integer = 0
        grdImport.SelectAll()
        For Each SelectedRows As DataGridViewRow In grdImport.SelectedRows
            xRow = SelectedRows.Index
            xBrands = grdImport.Item(15, xRow).Value
            m_saveitembrand(xBrands, xBrands, Me)
        Next


        '#################################################################################################
        'Dim sSQLcmd As String = "SELECT fcDepartment,fcDepartment FROM " & mTemporaryUpload

        'Try
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLcmd)
        '        While rd.Read
        'm_saveitembrand(rd.Item(0), rd.Item(1), Me)
        '        End While
        '    End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try


    End Sub
    Private Sub uploadToItemCategoryTable() '(ByVal mTemporaryUpload As String)
        Dim xRow As Integer = 0
        Dim xCategory As String
        grdImport.SelectAll()
        For Each selectedRows As DataGridViewRow In grdImport.SelectedRows
            xRow = selectedRows.Index
            xCategory = grdImport.Item(3, xRow).Value
            m_saveitemcategory(xCategory, xCategory, Me)
        Next


        '#############################################################################################
        'Dim sSQlcmd As String = "SELECT fcCategory,fcCategory FROM " & mTemporaryUpload

        'Try
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQlcmd)
        '        While rd.Read
        '            m_saveitemcategory(rd.Item(0), rd.Item(1), Me)
        '        End While
        '    End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub
    Private Sub uploadToItemSizeTable()
        Dim xRows As Integer = 0
        Dim xSize As String
        Dim xDescription As String
        Dim VirtualTxtbox As New TextBox
        Dim xCon1 As String
        Dim xCon2 As String
        grdImport.SelectAll()
        For Each selectedRow As DataGridViewRow In grdImport.SelectedRows
            xRows = selectedRow.Index
            xSize = grdImport.Item(6, xRows).Value
            VirtualTxtbox.Text = xSize
            VirtualTxtbox.SelectionStart = 0
            VirtualTxtbox.SelectionLength = 2
            xCon1 = VirtualTxtbox.SelectedText

            VirtualTxtbox.SelectionStart = 2
            VirtualTxtbox.SelectionLength = Len(-1)
            xCon2 = VirtualTxtbox.SelectedText
            If xCon2 Is Nothing Or xCon2 = "" Or xCon2 = " " Then
                xDescription = xCon1
            Else
                xDescription = xCon1 & "/" & xCon2
            End If
            'MsgBox(xSize + vbCr + xDescription)
            If grdImport.Item(6, xRows).Value = "NONE" Then
                m_saveitemsize(xSize, "NONE", Me)
            Else
                m_saveitemsize(xSize, xDescription, Me)
            End If
        Next

        '#####################################################################################################
        'Dim sSQlcmd As String = "SELECT fcProductSize," & _
        '          "LEFT(fcProductSize,2) + '/' + RIGHT(fcProductSize,1)" & _
        '          "FROM " & mTemporaryUpload
        'Try
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQlcmd)
        '        While rd.Read

        '            'If rd.Item(0) = "NONE" Then

        '            '' m_saveitemsize(rd.Item(0), "NONE", Me)
        '            'Else
        '            ''  m_saveitemsize(rd.Item(0), rd.Item(1), Me)
        '            'End If
        '        End While
        '    End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

    End Sub
    Private Sub uploadToColorTable() '(ByVal mTemporaryUpload As String)

        Dim xRow As Integer = 0
        Dim xColorWithCode(0) As String
        Dim xCode As String
        Dim xDescription As String
        grdImport.SelectAll()
        For Each selectedRows As DataGridViewRow In grdImport.SelectedRows
            xRow = selectedRows.Index
            xColorWithCode = Split(grdImport.Item(7, xRow).Value, "-")
            xCode = xColorWithCode(0)
            xDescription = xColorWithCode(1)
            m_saveitemcolor(xCode, xDescription, Me)
        Next
        'grdImport.Refresh()

        '######################################################################################################
        'Dim sSQlcmd As String = "SELECT	SUBSTRING(fcColor,1,CHARINDEX('-', fcColor)-1) "
        'sSQlcmd &= ", SUBSTRING(fcColor,CHARINDEX('-', fcColor)+1, LEN(fcColor)-CHARINDEX('-',fcColor)+1) "
        ''Dim sSQlcmd As String = "SELECT	fcColor "
        ''sSQlcmd &= ", fcColor "
        'sSQlcmd &= "FROM " & mTemporaryUpload



        'Try
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQlcmd)
        '        While rd.Read
        '            m_saveitemcolor(rd.Item(0), rd.Item(1), Me)

        '        End While
        '    End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

    End Sub
    Private Sub uploadToItemMaster() '(ByVal mTemporaryUpload As String)
        Dim xRow As Integer = 0
        Dim xItemCode As String = ""
        Dim xDescription As String = ""
        Dim xDepartment As String = ""
        Dim xCategory As String = ""
        Dim xSubcategory As String = ""
        Dim xStyle As String = ""
        Dim xSizeCode As String = ""
        Dim VirtualTextbox As New TextBox
        Dim Size1 As String
        Dim size2 As String
        Dim xColorCode As String = ""
        Dim xCode(0) As String
        Dim xUnitPrice As String = ""
        grdImport.SelectAll()
        For Each selectedRow As DataGridViewRow In grdImport.SelectedRows
            xRow = selectedRow.Index
            xItemCode = grdImport.Item(0, xRow).Value
            xDescription = grdImport.Item(1, xRow).Value
            xCategory = grdImport.Item(3, xRow).Value
            If grdImport.Item(2, xRow).Value = "" Or _
                grdImport.Item(2, xRow).Value Is Nothing Or _
                grdImport.Item(2, xRow).Value = " " Then
                xDepartment = grdImport.Item(15, xRow).Value
            Else
                xDepartment = grdImport.Item(2, xRow).Value
            End If
            xSubcategory = grdImport.Item(4, xRow).Value
            xStyle = grdImport.Item(5, xRow).Value
            VirtualTextbox.Text = grdImport.Item(6, xRow).Value
            VirtualTextbox.SelectionStart = 0
            VirtualTextbox.SelectionLength = 2
            Size1 = VirtualTextbox.SelectedText
            VirtualTextbox.SelectionStart = 2
            VirtualTextbox.SelectionLength = Len(-1)
            size2 = VirtualTextbox.SelectedText
            If size2 Is Nothing Or size2 = "" Or size2 = " " Then
                xSizeCode = Size1
            Else
                xSizeCode = Size1 & "/" & size2
            End If
            xCode = Split(grdImport.Item(7, xRow).Value, "-")
            xColorCode = xCode(0)
            xUnitPrice = grdImport.Item(12, xRow).Value
            m_saveItemMaster(xItemCode, xDescription, xDepartment, xCategory, xSubcategory, xStyle, _
                       xSizeCode, xColorCode, xUnitPrice)
        Next
        grdImport.ClearSelection()
        '###############################################################################################
        'Dim sSQLcmd As String = "SELECT fcItemCode, fcDescription, fcDepartment, fcCategory, fcSubcategory, "
        'sSQLcmd &= "fcStyle, fcProductSize, SUBSTRING(fcColor,1,CHARINDEX('-', fcColor)-1), fdUnitPrice "
        ''sSQLcmd &= "fcStyle, fcProductSize, fcColor, fdUnitPrice "
        'sSQLcmd &= "FROM " & mTemporaryUpload

        'Try
        '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLcmd)
        '        While rd.Read
        '            m_saveItemMaster(rd.Item(0), rd.Item(1), rd.Item(2), rd.Item(3), rd.Item(4), _
        '                             rd.Item(5), rd.Item(6), rd.Item(7), CDec(rd.Item(8)))

        '            'MsgBox(rd.Item(0) & vbCr & rd.Item(1) & vbCr & rd.Item(2) & vbCr & rd.Item(3) & vbCr & rd.Item(4) & vbCr & _
        '            '                                                 rd.Item(5) & vbCr & rd.Item(6) & vbCr & rd.Item(7) & vbCr & CDec(rd.Item(8)))
        '        End While
        '    End Using
        'Catch ex As Exception
        '    MsgBox(ex.Message)

        'End Try

    End Sub
    Private Sub executeUploadStoreItemsCode()
        Dim Uploadcommand As String = "usp_m_StoreItemMasterUpload"

        Try
            Dim ItemUploader As New SqlCommand
            gCon.sqlconn.Open()
            With ItemUploader
                .CommandTimeout = 0
                .Connection = gCon.sqlconn
                .CommandType = CommandType.StoredProcedure
                .CommandText = Uploadcommand
                .Parameters.AddWithValue("@fxKeyCompany", gCompanyID())
                .Parameters.AddWithValue("@currentUser", gUserName)
                .ExecuteScalar()
            End With
            gCon.sqlconn.Close()
            MsgBox("Upload Successful!!!", MsgBoxStyle.Information, "Upload")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!:")
            gCon.sqlconn.Close()
        End Try
    End Sub

    Private Sub loadItemCSVFilePath()

        openfile.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*"
        If Me.openfile.ShowDialog = Windows.Forms.DialogResult.OK Then

            If Me.openfile.FileName.ToLower().EndsWith(".csv") Then
                ItemMasterFilePath = Me.openfile.FileName
                Try
                    Dim txtFileToTable As DataTable = GetDataTableFromFile(ItemMasterFilePath, ",", 21)
                    grdImport.DataSource = txtFileToTable
                    isloadFileSuccessful = True
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
                    isloadFileSuccessful = False
                End Try
            Else
                frmupload.Close()
                MsgBox("Wrong Format: Please Select a CSV File.", MsgBoxStyle.Exclamation, "Warning")
                isloadFileSuccessful = False
            End If

        End If
    End Sub
    Private Sub LoadItemExcelFileToGrid()

        openfile.Filter = "Excel Files (*.xls)|*.xls|All files (*.*)|*.*"
        If Me.openfile.ShowDialog = Windows.Forms.DialogResult.OK Then

            If Me.openfile.FileName.ToLower().EndsWith(".xls") Then
                ItemMasterFilePath = Me.openfile.FileName

                Dim excelConnection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;" & _
                        "Data Source=" & ItemMasterFilePath & ";Extended Properties=" & _
                        "'Excel 8.0;HDR=Yes;IMEX=1'")

                Try
                    excelConnection.Open()
                    Dim TableSchema As DataTable = excelConnection.GetSchema("Tables")
                    Dim sqlCommand As String = String.Format("SELECT * FROM [{0}]", TableSchema.Rows(0)("TABLE_NAME"))
                    Dim excelAdapter As New OleDbDataAdapter(sqlCommand, excelConnection)
                    Dim excelDataset As DataSet = New DataSet()

                    excelAdapter.Fill(excelDataset, "Store Item Table")
                    StoreItemSource = excelDataset.Tables("Store Item Table")

                    grdImport.DataSource = excelDataset
                    grdImport.AutoGenerateColumns = True
                    grdImport.DataMember = "Store Item Table"
                    isloadFileSuccessful = True
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Error!")
                    isloadFileSuccessful = False
                Finally
                    excelConnection.Close()
                End Try

            Else
                MsgBox("Wrong Format: Please Select a Excel File.", MsgBoxStyle.Exclamation, "Warning")
                isloadFileSuccessful = False
            End If

        End If
    End Sub

    Private Sub MainProductsUpload()



        '################################################################################################
        'Dim mTemporaryUpload As String = "mTemporaryUpload"
        'Dim SourceTable As DataTable = GetDataTableFromFile(ItemMasterFilePath, ",", 21)
        'gCon.sqlconn.Open()
        'Using CopyDatabase As SqlBulkCopy = New SqlBulkCopy(gCon.sqlconn)
        '    'Dim worker As System.ComponentModel.BackgroundWorker = DirectCast(sender, System.ComponentModel.BackgroundWorker)
        '    Dim sourceHeader() As String = getHeaderFromCSVSource(ItemMasterFilePath, ",")
        '    Dim destinationHeader() As String = getHeaderFromDestination(mTemporaryUpload)
        '    Dim index As Integer = 0
        '    CopyDatabase.DestinationTableName = mTemporaryUpload
        '    'map
        '    For index = 0 To sourceHeader.Length - 1
        '        Dim mapColumn As New SqlBulkCopyColumnMapping(sourceHeader(index), destinationHeader(index))
        '        CopyDatabase.ColumnMappings.Add(mapColumn)
        '    Next
        '    Try
        'CopyDatabase.WriteToServer(SourceTable)
        uploadToItemCodeTable()
        uploadToItemBrandTable()
        uploadToItemCategoryTable()
        uploadToItemSizeTable()
        uploadToColorTable()
        uploadToItemMaster()
        MsgBox("Uploading Successful!", MsgBoxStyle.Exclamation, "Upload")
        '    Catch ex As Exception
        '    MsgBox("Error in Uploading")
        'End Try
        'End Using
        'gCon.sqlconn.Close()
    End Sub
    Private Sub StoreProductsUpload()
        Dim TemporarySourceTable As String = "mTempStoreItems"
        gCon.sqlconn.Open()
        Using CopyDatabase As SqlBulkCopy = New SqlBulkCopy(gCon.sqlconn)
            Dim sourceHeader() As String = getHeaderFromExcelSource(ItemMasterFilePath)
            Dim destinationHeader() As String = getHeaderFromDestination(TemporarySourceTable)

            Dim index As Integer = 0
            CopyDatabase.DestinationTableName = TemporarySourceTable
            For index = 0 To sourceHeader.Length - 1
                Dim mapcolumn As New SqlBulkCopyColumnMapping(sourceHeader(index), destinationHeader(index))
                CopyDatabase.ColumnMappings.Add(mapcolumn)
            Next

            Try
                CopyDatabase.WriteToServer(StoreItemSource)
            Catch ex As Exception

                MsgBox("Error in Uploading")
            End Try
        End Using
        gCon.sqlconn.Close()
        executeUploadStoreItemsCode()
    End Sub

    Private Sub DefaultState()

        pbUpload.Visible = False
    End Sub

    Private Sub bgwMainUploader_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwMainUploader.DoWork
        MainProductsUpload()
    End Sub
    Private Sub bgwMainUploader_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwMainUploader.RunWorkerCompleted
        frmupload.Close()
        DefaultState()
        MenuBrowseItem.Enabled = True
    End Sub
    Private Sub bgwStoreUploader_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwStoreUploader.DoWork
        StoreProductsUpload()
    End Sub
    Private Sub bgwStoreUploader_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwStoreUploader.RunWorkerCompleted
        DefaultState()
        BrowseStoreItems.Enabled = True
    End Sub
#End Region
#Region "Item Master Updates"

    Private Sub updateItemMaster()
        Dim getFromSourceCommand As String = "SELECT fcItemCode, "
        getFromSourceCommand &= "fcProductSize, LEFT(fcColor,4), fdUnitPrice "
        getFromSourceCommand &= "FROM mTemporaryUpload"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, getFromSourceCommand)
                While rd.Read
                    saveToItemMasterFile(rd.Item(0).ToString, rd.Item(1).ToString, _
                                         rd.Item(2).ToString, CDec(rd.Item(3)))
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub saveToItemMasterFile(ByVal ItemCode As String, _
                                     ByVal SizeCode As String, _
                                     ByVal ColorCode As String, _
                                     ByVal UnitPrice As Decimal)

        Dim ConnectionString As New Clsappconfiguration

        Dim updateCommand As String = "usp_m_updateItemMasterUnitPrice "
        updateCommand &= "@fxKeyCompany='" & gCompanyID() & "' "
        updateCommand &= ",@fuCreatedBy='" & gUserName & "' "
        updateCommand &= ",@fcItemCode='" & ItemCode & "' "
        updateCommand &= ",@fcSizeCode='" & SizeCode & "' "
        updateCommand &= ",@fcColorCode='" & ColorCode & "' "
        updateCommand &= ",@fdUnitPrice='" & UnitPrice & "' "

        Try
            SqlHelper.ExecuteDataset(ConnectionString.cnstring, CommandType.Text, updateCommand)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Save Error")
        End Try

    End Sub

    Private Sub MenuBrowseSRPUpdates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuBrowseSRPUpdates.Click
        loadItemCSVFilePath()
        If isloadFileSuccessful = True Then
            MenuUploadSRPUpdates.Enabled = True
        End If
    End Sub
    Private Sub MenuUploadSRPUpdates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuUploadSRPUpdates.Click
        If MessageBox.Show("Make sure the Item Master File is free from" & vbNewLine & _
                            "special characters such as comma, apostrophe and quotes." & vbNewLine & _
                            "As well as Spaces around the dash in Color Codes." & vbNewLine & _
                            "Are you sure you want to continue?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) _
                            = MsgBoxResult.Ok Then
            pbUpload.Visible = True
            MenuBrowseSRPUpdates.Enabled = False
            MenuUploadSRPUpdates.Enabled = False
            bgwUpdate.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwUpdate_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwUpdate.DoWork
        Dim mTemporaryUpload As String = "mTemporaryUpload"
        Dim SourceTable As DataTable = GetDataTableFromFile(ItemMasterFilePath, ",", 21)
        gCon.sqlconn.Open()

        Using CopyDatabase As SqlBulkCopy = New SqlBulkCopy(gCon.sqlconn)

            Dim worker As System.ComponentModel.BackgroundWorker = DirectCast(sender, System.ComponentModel.BackgroundWorker)
            Dim sourceHeader() As String = getHeaderFromCSVSource(ItemMasterFilePath, ",")
            Dim destinationHeader() As String = getHeaderFromDestination(mTemporaryUpload)
            Dim index As Integer = 0

            CopyDatabase.DestinationTableName = mTemporaryUpload
            For index = 0 To sourceHeader.Length - 1
                Dim mapColumn As New SqlBulkCopyColumnMapping(sourceHeader(index), destinationHeader(index))
                CopyDatabase.ColumnMappings.Add(mapColumn)
            Next

            Try
                CopyDatabase.WriteToServer(SourceTable)
                updateItemMaster()
                MsgBox("Updating Successful!", MsgBoxStyle.Exclamation, "Update SRP")
            Catch ex As Exception
                MsgBox("Error in Updating SRP!")
            End Try
        End Using
        gCon.sqlconn.Close()
    End Sub
    Private Sub bgwUpdate_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwUpdate.RunWorkerCompleted
        pbUpload.Visible = False
        MenuBrowseSRPUpdates.Enabled = True
        MenuUploadSRPUpdates.Enabled = True
    End Sub

#End Region



End Class