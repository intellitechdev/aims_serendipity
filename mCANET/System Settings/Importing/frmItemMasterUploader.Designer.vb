<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemMasterUploader
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmItemMasterUploader))
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.btnUpload = New System.Windows.Forms.Button
        Me.grdUploader = New System.Windows.Forms.DataGridView
        Me.DlgOpenUploader = New System.Windows.Forms.OpenFileDialog
        Me.DlgSaveUploader = New System.Windows.Forms.SaveFileDialog
        CType(Me.grdUploader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtPath.Location = New System.Drawing.Point(84, 7)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(290, 23)
        Me.txtPath.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Import From:"
        '
        'btnBrowse
        '
        Me.btnBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBrowse.Location = New System.Drawing.Point(380, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(79, 29)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'btnGenerate
        '
        Me.btnGenerate.Location = New System.Drawing.Point(465, 3)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(123, 29)
        Me.btnGenerate.TabIndex = 3
        Me.btnGenerate.Text = "Generate Uploader"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'btnUpload
        '
        Me.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpload.Location = New System.Drawing.Point(594, 3)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(78, 29)
        Me.btnUpload.TabIndex = 4
        Me.btnUpload.Text = "Upload"
        Me.btnUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'grdUploader
        '
        Me.grdUploader.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdUploader.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdUploader.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdUploader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdUploader.GridColor = System.Drawing.Color.Black
        Me.grdUploader.Location = New System.Drawing.Point(3, 36)
        Me.grdUploader.Name = "grdUploader"
        Me.grdUploader.Size = New System.Drawing.Size(692, 418)
        Me.grdUploader.TabIndex = 5
        '
        'DlgOpenUploader
        '
        Me.DlgOpenUploader.Filter = "Excel 2007 file|* .xlsx|Excel 97-2003 file|*.xls"
        Me.DlgOpenUploader.Title = "Select an uploader"
        '
        'DlgSaveUploader
        '
        Me.DlgSaveUploader.Filter = "Excel 2007 file|*.xlsx|Excel 97-2003 file|*.xls"
        Me.DlgSaveUploader.Title = "Save Uploader in"
        '
        'frmItemMasterUploader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(698, 457)
        Me.Controls.Add(Me.grdUploader)
        Me.Controls.Add(Me.btnUpload)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPath)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(682, 271)
        Me.Name = "frmItemMasterUploader"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Item Master Uploader"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.grdUploader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents grdUploader As System.Windows.Forms.DataGridView
    Friend WithEvents DlgOpenUploader As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DlgSaveUploader As System.Windows.Forms.SaveFileDialog
End Class
