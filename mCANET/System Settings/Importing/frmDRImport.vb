Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class frmDRImport

    Private gCon As New Clsappconfiguration
    Private gTrans As New clsTransactionFunctions
    Private txtbox As New TextBox
    Private xcustomer As String

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call loadpath()
    End Sub



    Private Sub frmDRImport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call creategrid()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Call verifycustomerexistence()
        Call importheader()
    End Sub

    Private Sub verifycustomerexistence()
        Try
            Dim customer As String = DataGridView1.Rows(0).Cells("cCustomer").Value
            Dim sSQLCmd As String = "usp_t_verifycustomerexistence "
            sSQLCmd &= "@fcCustomerName='" & customer & "' "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub loadpath()
        Dim strmFile As StreamReader
        Dim strFile, sArray(), sColor(), sSize, sBase, sSizeCode As String
        Dim nrow As Integer = 0
        If Me.Openfile.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtbox.Text = Me.Openfile.FileName
            strmFile = File.OpenText(txtbox.Text)
            Using strmFile
                strFile = strmFile.ReadLine
                While Not strFile Is Nothing
                    strFile = strmFile.ReadLine
                    If Not strFile Is Nothing Then
                        sArray = strFile.Split(",")
                        Try
                            sColor = Split(sArray(8), "-")
                            sSize = Microsoft.VisualBasic.Left(Trim(sArray(9)), 2)
                            sBase = Microsoft.VisualBasic.Right(Trim(sArray(9)), 1)
                            sSizeCode = CStr(sSize) + "/" + CStr(sBase)
                            Call populategrid(Trim(sArray(0)), Trim(sArray(1)), Trim(sArray(2)), Trim(sArray(3)), Trim(sArray(4)), Trim(sArray(5)), Trim(sArray(6)), Trim(sArray(7)), Trim(sColor(0)), Trim(sSizeCode), nrow)
                        Catch ex As Exception
                        End Try
                    End If
                    nrow += 1
                End While
            End Using
        End If
    End Sub

    Private Sub creategrid()
        With Me.DataGridView1
            Dim colName As New DataGridViewTextBoxColumn
            Dim colDRdate As New DataGridViewTextBoxColumn
            Dim colDRNo As New DataGridViewTextBoxColumn
            Dim colRefNo As New DataGridViewTextBoxColumn
            Dim colBarcode As New DataGridViewTextBoxColumn
            Dim colQty As New DataGridViewTextBoxColumn
            Dim colICode As New DataGridViewTextBoxColumn
            Dim colDescription As New DataGridViewTextBoxColumn
            Dim colColor As New DataGridViewTextBoxColumn
            Dim colSize As New DataGridViewTextBoxColumn
            With colName
                .HeaderText = "Customer"
                .Name = "cCustomer"
            End With
            With colDRdate
                .HeaderText = "D.Rcpt Date"
                .Name = "cDRdate"
            End With
            With colDRNo
                .HeaderText = "D.Rcpt #"
                .Name = "cDRNo"
            End With
            With colRefNo
                .HeaderText = "Ref #"
                .Name = "cRef"
            End With
            With colBarcode
                .HeaderText = "Barcode"
                .Name = "cBarcode"
            End With
            With colQty
                .HeaderText = "Quantity"
                .Name = "cQty"
            End With
            With colICode
                .HeaderText = "Item Code"
                .Name = "cCode"
            End With
            With colDescription
                .HeaderText = "Description"
                .Name = "cDescription"
            End With
            With colColor
                .HeaderText = "Color"
                .Name = "cColor"
            End With
            With colSize
                .HeaderText = "Size"
                .Name = "cSize"
            End With
            .Columns.Add(colName)
            .Columns.Add(colDRDate)
            .Columns.Add(colDRno)
            .Columns.Add(colRefno)
            .Columns.Add(colBarcode)
            .Columns.Add(colQty)
            .Columns.Add(colIcode)
            .Columns.Add(colDescription)
            .Columns.Add(colColor)
            .Columns.Add(colSize)

            .Columns("cRef").Visible = False
        End With
    End Sub

    Private Sub populategrid(ByVal sCustomer As String, ByVal dDRDate As Date, ByVal iDRNo As String, _
                            ByVal sRefNo As String, ByVal sBarcode As String, ByVal sQty As Integer, _
                            ByVal sCode As String, ByVal sDesc As String, ByVal sColor As String, _
                            ByVal sSize As String, ByVal nrow As Integer) ', _
        'ByVal sICode, ByVal sDescription, _
        'ByVal sColor)

        'Dim xRow As Integer
        With DataGridView1
            .Rows.Add()
            ' .Item("cCustomer", .Rows.Count).Value = sCustomer
            .Rows(nrow).Cells("cCustomer").Value = sCustomer
            .Rows(nrow).Cells("cDRdate").Value = dDRDate
            .Rows(nrow).Cells("cDRNo").Value = iDRNo
            .Rows(nrow).Cells("cRef").Value = sRefNo
            .Rows(nrow).Cells("cBarcode").Value = sBarcode
            .Rows(nrow).Cells("cQty").Value = sQty
            .Rows(nrow).Cells("cCode").Value = sCode
            .Rows(nrow).Cells("cDescription").Value = sDesc
            .Rows(nrow).Cells("cColor").Value = sColor
            .Rows(nrow).Cells("cSize").Value = sSize

        End With
    End Sub

    Private Sub importheader()
        With Me.DataGridView1
            Dim sCustomer As String = ""
            Dim dDRDate As Date
            Dim dDRNo As String = ""
            Dim sKey As String = Guid.NewGuid.ToString
            If mkDefaultValues(0, 4) <> Nothing Then

                If mkDefaultValues(0, 0) <> Nothing Then
                    sCustomer = .Rows(0).Cells("cCustomer").Value.ToString.Trim
                End If
                If mkDefaultValues(0, 1) <> Nothing Then
                    dDRDate = CDate(.Rows(0).Cells("cDRdate").Value)
                End If
                If mkDefaultValues(0, 2) <> Nothing Then
                    dDRNo = (.Rows(0).Cells("cDRNo").Value.ToString.Trim)
                End If
                Try
                    Dim sSQLCmd As String = "usp_t_importdeliveryreceipt_header "
                    sSQLCmd &= " @fxKeyInvoice='" & sKey & "' "
                    sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
                    sSQLCmd &= ",@fcInvoiceNo='" & CStr(TextBox1.Text) & "' "
                    sSQLCmd &= ",@fcDRNo='" & dDRNo & "' "
                    sSQLCmd &= ",@fcCustomer='" & sCustomer & "' "
                    sSQLCmd &= ",@fdDateTransact='" & dDRDate & "' "
                    sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                Catch ex As Exception
                    MsgBox("encountered an error, pls report this to your administrator...", MsgBoxStyle.Information, Me.Text)
                End Try
                If importitems(sKey) = True Then
                    MsgBox("successful..", MsgBoxStyle.Information, Me.Text)
                End If
            End If
        End With
    End Sub

    Private Function importitems(ByVal sKey As String) As Boolean
        With Me.DataGridView1
            Dim iRow As Integer
            For iRow = 0 To .Rows.Count
                Dim sBarcode As String = ""
                Dim iQty As Integer = 0
                Dim sDescription As String = ""

                If mkDefaultValues(iRow, 4) <> Nothing Then
                    If mkDefaultValues(iRow, 4) <> Nothing Then
                        sBarcode = .Rows(iRow).Cells("cBarcode").Value.ToString.Trim
                    End If
                    If mkDefaultValues(iRow, 5) <> Nothing Then
                        iQty = .Rows(iRow).Cells("cQty").Value
                    End If
                    If mkDefaultValues(iRow, 7) <> Nothing Then
                        sDescription = .Rows(iRow).Cells("cDescription").Value.ToString.Trim
                    End If

                    Try
                        Dim sSQLCmd As String = "usp_t_importdeliveryreceipt_item "
                        sSQLCmd &= " @fxKeyInvoice='" & sKey & "' "
                        sSQLCmd &= ",@fcBarcode='" & sBarcode & "' "
                        sSQLCmd &= ",@fcDescription='" & sDescription & "' "
                        sSQLCmd &= ",@fdQty='" & iQty & "' "
                        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "'"

                        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

                    Catch ex As Exception
                        MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
                        Return False
                    End Try

                End If
            Next
            Return True
        End With
    End Function

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Try
            Return IIf(IsDBNull(DataGridView1.Rows(irow).Cells(iCol).Value) = True, _
                                    Nothing, DataGridView1.Rows(irow).Cells(iCol).Value)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Private Sub DataGridView1_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles DataGridView1.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < DataGridView1.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub
End Class