<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSRImport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSRImport))
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPageSummary = New System.Windows.Forms.TabPage
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnImportSR_Summary = New System.Windows.Forms.Button
        Me.btnBrowseSR_Summary = New System.Windows.Forms.Button
        Me.grdSummary = New System.Windows.Forms.DataGridView
        Me.TabPageParticulars = New System.Windows.Forms.TabPage
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.btnImportSR_Particulars = New System.Windows.Forms.Button
        Me.btnBrowseSR_Particulars = New System.Windows.Forms.Button
        Me.grdParticulars = New System.Windows.Forms.DataGridView
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ProgressBarSRImport = New System.Windows.Forms.ProgressBar
        Me.btnClose = New System.Windows.Forms.Button
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog
        Me.BackgroundWorkerSRImport = New System.ComponentModel.BackgroundWorker
        Me.TabControl1.SuspendLayout()
        Me.TabPageSummary.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.grdSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageParticulars.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.grdParticulars, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPageSummary)
        Me.TabControl1.Controls.Add(Me.TabPageParticulars)
        Me.TabControl1.Location = New System.Drawing.Point(3, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(529, 436)
        Me.TabControl1.TabIndex = 0
        '
        'TabPageSummary
        '
        Me.TabPageSummary.Controls.Add(Me.Panel2)
        Me.TabPageSummary.Controls.Add(Me.grdSummary)
        Me.TabPageSummary.Location = New System.Drawing.Point(4, 22)
        Me.TabPageSummary.Name = "TabPageSummary"
        Me.TabPageSummary.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageSummary.Size = New System.Drawing.Size(521, 410)
        Me.TabPageSummary.TabIndex = 0
        Me.TabPageSummary.Text = "Summary"
        Me.TabPageSummary.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.Panel2.Controls.Add(Me.btnImportSR_Summary)
        Me.Panel2.Controls.Add(Me.btnBrowseSR_Summary)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(3, 372)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(515, 35)
        Me.Panel2.TabIndex = 1
        '
        'btnImportSR_Summary
        '
        Me.btnImportSR_Summary.Location = New System.Drawing.Point(91, 6)
        Me.btnImportSR_Summary.Name = "btnImportSR_Summary"
        Me.btnImportSR_Summary.Size = New System.Drawing.Size(86, 23)
        Me.btnImportSR_Summary.TabIndex = 1
        Me.btnImportSR_Summary.Text = "Import Data"
        Me.btnImportSR_Summary.UseVisualStyleBackColor = True
        '
        'btnBrowseSR_Summary
        '
        Me.btnBrowseSR_Summary.Location = New System.Drawing.Point(3, 6)
        Me.btnBrowseSR_Summary.Name = "btnBrowseSR_Summary"
        Me.btnBrowseSR_Summary.Size = New System.Drawing.Size(86, 23)
        Me.btnBrowseSR_Summary.TabIndex = 0
        Me.btnBrowseSR_Summary.Text = "Browse "
        Me.btnBrowseSR_Summary.UseVisualStyleBackColor = True
        '
        'grdSummary
        '
        Me.grdSummary.AllowUserToAddRows = False
        Me.grdSummary.AllowUserToDeleteRows = False
        Me.grdSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSummary.Dock = System.Windows.Forms.DockStyle.Top
        Me.grdSummary.Location = New System.Drawing.Point(3, 3)
        Me.grdSummary.Name = "grdSummary"
        Me.grdSummary.ReadOnly = True
        Me.grdSummary.Size = New System.Drawing.Size(515, 366)
        Me.grdSummary.TabIndex = 0
        '
        'TabPageParticulars
        '
        Me.TabPageParticulars.Controls.Add(Me.Panel3)
        Me.TabPageParticulars.Controls.Add(Me.grdParticulars)
        Me.TabPageParticulars.Location = New System.Drawing.Point(4, 22)
        Me.TabPageParticulars.Name = "TabPageParticulars"
        Me.TabPageParticulars.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageParticulars.Size = New System.Drawing.Size(521, 410)
        Me.TabPageParticulars.TabIndex = 1
        Me.TabPageParticulars.Text = "Particulars"
        Me.TabPageParticulars.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.Panel3.Controls.Add(Me.btnImportSR_Particulars)
        Me.Panel3.Controls.Add(Me.btnBrowseSR_Particulars)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 373)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(515, 34)
        Me.Panel3.TabIndex = 2
        '
        'btnImportSR_Particulars
        '
        Me.btnImportSR_Particulars.Location = New System.Drawing.Point(91, 6)
        Me.btnImportSR_Particulars.Name = "btnImportSR_Particulars"
        Me.btnImportSR_Particulars.Size = New System.Drawing.Size(86, 23)
        Me.btnImportSR_Particulars.TabIndex = 2
        Me.btnImportSR_Particulars.Text = "Import Data"
        Me.btnImportSR_Particulars.UseVisualStyleBackColor = True
        '
        'btnBrowseSR_Particulars
        '
        Me.btnBrowseSR_Particulars.Location = New System.Drawing.Point(3, 6)
        Me.btnBrowseSR_Particulars.Name = "btnBrowseSR_Particulars"
        Me.btnBrowseSR_Particulars.Size = New System.Drawing.Size(86, 23)
        Me.btnBrowseSR_Particulars.TabIndex = 1
        Me.btnBrowseSR_Particulars.Text = "Browse "
        Me.btnBrowseSR_Particulars.UseVisualStyleBackColor = True
        '
        'grdParticulars
        '
        Me.grdParticulars.AllowUserToAddRows = False
        Me.grdParticulars.AllowUserToDeleteRows = False
        Me.grdParticulars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdParticulars.Dock = System.Windows.Forms.DockStyle.Top
        Me.grdParticulars.Location = New System.Drawing.Point(3, 3)
        Me.grdParticulars.Name = "grdParticulars"
        Me.grdParticulars.ReadOnly = True
        Me.grdParticulars.Size = New System.Drawing.Size(515, 363)
        Me.grdParticulars.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ProgressBarSRImport)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 454)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(534, 33)
        Me.Panel1.TabIndex = 1
        '
        'ProgressBarSRImport
        '
        Me.ProgressBarSRImport.Location = New System.Drawing.Point(13, 8)
        Me.ProgressBarSRImport.MarqueeAnimationSpeed = 40
        Me.ProgressBarSRImport.Name = "ProgressBarSRImport"
        Me.ProgressBarSRImport.Size = New System.Drawing.Size(174, 18)
        Me.ProgressBarSRImport.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.ProgressBarSRImport.TabIndex = 3
        Me.ProgressBarSRImport.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(450, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFileDialog1"
        '
        'BackgroundWorkerSRImport
        '
        Me.BackgroundWorkerSRImport.WorkerSupportsCancellation = True
        '
        'frmSRImport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(534, 487)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSRImport"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SR Import"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPageSummary.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.grdSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageParticulars.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.grdParticulars, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPageSummary As System.Windows.Forms.TabPage
    Friend WithEvents grdSummary As System.Windows.Forms.DataGridView
    Friend WithEvents TabPageParticulars As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents grdParticulars As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnImportSR_Summary As System.Windows.Forms.Button
    Friend WithEvents btnBrowseSR_Summary As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnBrowseSR_Particulars As System.Windows.Forms.Button
    Friend WithEvents btnImportSR_Particulars As System.Windows.Forms.Button
    Friend WithEvents ProgressBarSRImport As System.Windows.Forms.ProgressBar
    Friend WithEvents BackgroundWorkerSRImport As System.ComponentModel.BackgroundWorker
End Class
