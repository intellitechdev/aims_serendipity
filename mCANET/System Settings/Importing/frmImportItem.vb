Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmImportItem

    Private txtbox As New TextBox
    Private gTrans As New clsTransactionFunctions
    Private gCon As New Clsappconfiguration

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Call gTrans.removelink_server()
        Call loadpath()
    End Sub

    Private Sub loadpath()
        If Me.openfile.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtbox.Text = Me.openfile.FileName
            If gTrans.makelink_server(txtbox.Text) = "Successful..." Then
                Call verifygrid()
            Else
                MsgBox("Please Import excel file again...", MsgBoxStyle.Information, Me.Text)
            End If
        End If
    End Sub

    Private Sub verifygrid()
        MsgBox(gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TSAPATH, 'SELECT * FROM [Sheet1$]')", DataGridView1), MsgBoxStyle.Information, Me.Text)
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If saveitems() = True Then
            MsgBox("Data import successful...", MsgBoxStyle.Information, Me.Text)
            DataGridView1.Columns.Clear()
            cboSupplier.Text = ""
            cboYear.Text = ""
            cboGroup.Text = ""
            Me.Close()
        Else
            MsgBox("Data import unsuccessful...", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub

    Private Function saveitems() As Boolean
        Dim rowCount As Integer = 0
        Dim barcode As String = ""
        Dim itemcode As String = ""
        Dim description As String = ""
        Dim colorcode As String = ""
        Dim sizecode As String = ""
        Dim cost As Decimal = 0.0
        Dim Price As Decimal = 0.0
        Dim beginbal As Integer = 0
        Dim onhand As Integer = 0
        Dim sBrand As String = ""

        Dim i As Integer = 0
        Dim iValue As Integer = 0
        Dim iTotal As Integer = DataGridView1.Rows.Count
        ProgressBar1.Visible = True

        With DataGridView1
            If .Rows.Count - 1 > 0 Then
                For rowCount = 0 To .Rows.Count - 1
                    If gTrans.gmkDefaultValues(rowCount, 0, Me.DataGridView1) <> Nothing Then
                        barcode = (.Rows(rowCount).Cells(0).Value)
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 1, Me.DataGridView1) <> Nothing Then
                        itemcode = (.Rows(rowCount).Cells(1).Value)
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 2, Me.DataGridView1) <> Nothing Then
                        description = (.Rows(rowCount).Cells(2).Value)
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 3, Me.DataGridView1) <> Nothing Then
                        Dim xColor() As String
                        xColor = Split((.Rows(rowCount).Cells(3).Value), "-")
                        colorcode = xColor(0).Trim
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 4, Me.DataGridView1) <> Nothing Then
                        Dim xSize, xBase As String
                        xSize = Microsoft.VisualBasic.Left((.Rows(rowCount).Cells(4).Value), 2).ToString
                        xBase = Microsoft.VisualBasic.Right((.Rows(rowCount).Cells(4).Value), 1).ToString
                        sizecode = (CStr(xSize).Trim + "/" + CStr(xBase).Trim).Trim
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 5, Me.DataGridView1) <> Nothing Then
                        cost = CDec(.Rows(rowCount).Cells(5).Value)
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 6, Me.DataGridView1) <> Nothing Then
                        Price = Format(CDec(.Rows(rowCount).Cells(6).Value), "##,##0.0000")
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 7, Me.DataGridView1) <> Nothing Then
                        beginbal = CInt(.Rows(rowCount).Cells(7).Value)
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 8, Me.DataGridView1) <> Nothing Then
                        onhand = CInt(.Rows(rowCount).Cells(8).Value)
                    End If
                    If gTrans.gmkDefaultValues(rowCount, 9, Me.DataGridView1) <> Nothing Then
                        sBrand = (.Rows(rowCount).Cells(9).Value)
                    End If

                    Dim sCOgs As String = ""
                    If cboCOGs.SelectedItem <> "" Then
                        sCOgs = getAccountID(cboCOGs.SelectedItem)
                    End If

                    Dim sIncome As String = ""
                    If cboIncomeAcnt.SelectedItem <> "" Then
                        sIncome = getAccountID(cboIncomeAcnt.SelectedItem)
                    End If

                    Dim sAsset As String = ""
                    If cboAssetAcnt.SelectedItem <> "" Then
                        sAsset = getAccountID(cboAssetAcnt.SelectedItem)
                    End If

                    Dim sYear As String = ""
                    If cboYear.SelectedItem <> "" Then
                        sYear = cboYear.SelectedItem
                    End If

                    iValue = i / iTotal * 100
                    ProgressBar1.Value = iValue
                    Try
                        Dim sSQLCmd As String = "usp_t_itemmaster_import "
                        sSQLCmd &= " @fcItemCode='" & itemcode & "' "
                        sSQLCmd &= ",@fcDescription='" & description & "' "
                        sSQLCmd &= ",@fcColorCode='" & colorcode & "' "
                        sSQLCmd &= ",@fcSizeCode='" & sizecode & "' "
                        sSQLCmd &= ",@fdUnitCost='" & cost & "' "
                        sSQLCmd &= ",@fdUnitPrice='" & Price & "' "
                        sSQLCmd &= ",@fdOnHand='" & onhand & "' "
                        sSQLCmd &= ",@fdBeginBalance='" & beginbal & "' "
                        sSQLCmd &= ",@fdPurchaseDate='" & Microsoft.VisualBasic.FormatDateTime(DateTimePicker1.Value, DateFormat.ShortDate) & " '"
                        sSQLCmd &= ",@fxKeyGroupName='" & getGroupID(cboGroup.SelectedItem) & "' "
                        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
                        sSQLCmd &= ",@fxKeySupplier='" & getSupplierID(cboSupplier.SelectedItem) & "' "
                        sSQLCmd &= ",@fxKeyCOGs=" & IIf(sCOgs = "", "NULL", "'" & sCOgs & "'") & " "
                        sSQLCmd &= ",@fxKeyTax='" & cboTax.SelectedItem & "' "
                        sSQLCmd &= ",@fxKeyIncomeAcnt=" & IIf(sCOgs = "", "NULL", "'" & sCOgs & "'") & " "
                        sSQLCmd &= ",@fxKeyAsset=" & IIf(sAsset = "", "NULL", "'" & sAsset & "'") & " "
                        sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
                        sSQLCmd &= ",@fcBarCode='" & barcode & "' "
                        sSQLCmd &= ",@fcYear='" & sYear & "' "
                        sSQLCmd &= ",@fcBrand='" & sBrand & "' "
                        'If i = 219 Then
                        '    sSQLCmd = ""
                        'End If
                        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                        'MsgBox("succussful....", MsgBoxStyle.Information, Me.Text)
                      
                        i = i + 1
                    Catch ex As Exception
                        MsgBox(Str(i) & " " & Err.Description, MsgBoxStyle.Information, Me.Text)
                        Return False
                    End Try
                Next
                ProgressBar1.Visible = False
                Return True
            End If
            Return False
        End With
    End Function

    Private Sub frmImportItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_loadTaxCode(cboTax)
        Call m_loadSuppliers(cboSupplier)
        Call m_DisplayAccountsAll(cboCOGs)
        Call m_DisplayAccountsAll(cboAssetAcnt)
        Call m_DisplayAccountsAll(cboIncomeAcnt)
        Call m_loadItemGroup(cboGroup)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        DataGridView1.Columns.Clear()
        cboSupplier.Text = ""
        cboYear.Text = ""
        cboGroup.Text = ""
        Me.Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        DataGridView1.Columns.Clear()
        cboSupplier.Text = ""
        cboYear.Text = ""
        cboGroup.Text = ""
    End Sub
End Class