Imports System.Data.OleDb, System.IO

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmItemMasterUploader

    Private xFilePath As String = ""
    Private gOleConn As New OleDbConnection
    Private gOleCmd As New OleDbCommand
    Private gDT As New DataTable
    Private UploaderType As String
    Private gCon As New Clsappconfiguration
    Private fxKeyCompany As String = gCompanyID()
    Public xMode As String = ""


    Private Sub GenerateTemplate()
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        xlApp = New Excel.ApplicationClass
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("sheet1")

        Select Case xMode
            Case "Item Master"
                xlWorkSheet.Cells(1, 1) = "Item Code" '0
                xlWorkSheet.Cells(1, 2) = "Description" '1
                xlWorkSheet.Cells(1, 3) = "Category" '2
                xlWorkSheet.Cells(1, 4) = "Is Active Account" '3
                xlWorkSheet.Cells(1, 5) = "Cost" '4
                xlWorkSheet.Cells(1, 6) = "Price" '5
                xlWorkSheet.Cells(1, 7) = "COGS Account" '6
                xlWorkSheet.Cells(1, 8) = "Asset Account" '7
                xlWorkSheet.Cells(1, 9) = "Income Account" '8
                xlWorkSheet.Cells(1, 10) = "Tax Account" '9

                xlWorkSheet.Columns(1).ColumnWidth = 25
                xlWorkSheet.Columns(2).ColumnWidth = 45
                xlWorkSheet.Columns(3).ColumnWidth = 45
                xlWorkSheet.Columns(4).ColumnWidth = 15
                xlWorkSheet.Columns(5).ColumnWidth = 20
                xlWorkSheet.Columns(6).ColumnWidth = 20
                xlWorkSheet.Columns(7).ColumnWidth = 30
                xlWorkSheet.Columns(8).ColumnWidth = 30
                xlWorkSheet.Columns(9).ColumnWidth = 30
                xlWorkSheet.Columns(10).ColumnWidth = 30

            Case "Chart of Accounts"
                xlWorkSheet.Cells(1, 1) = "Level" '0
                xlWorkSheet.Cells(1, 2) = "Account Code" '1
                xlWorkSheet.Cells(1, 3) = "Account Description" '2
                xlWorkSheet.Cells(1, 4) = "Sub Account of" '3
                xlWorkSheet.Cells(1, 5) = "Active" '4
                xlWorkSheet.Cells(1, 6) = "Account Type" '5

                xlWorkSheet.Columns(1).ColumnWidth = 5
                xlWorkSheet.Columns(2).ColumnWidth = 25
                xlWorkSheet.Columns(3).ColumnWidth = 50
                xlWorkSheet.Columns(4).ColumnWidth = 25
                xlWorkSheet.Columns(5).ColumnWidth = 20
                xlWorkSheet.Columns(6).ColumnWidth = 45

            Case "Customer Master"
                xlWorkSheet.Cells(1, 1) = "Company Name" '0
                xlWorkSheet.Cells(1, 2) = "Last Name" '1
                xlWorkSheet.Cells(1, 3) = "First Name" '2
                xlWorkSheet.Cells(1, 4) = "M I" '3
                xlWorkSheet.Cells(1, 5) = "Salutation" '4
                xlWorkSheet.Cells(1, 6) = "Contact" '5
                xlWorkSheet.Cells(1, 7) = "Phone" '6
                xlWorkSheet.Cells(1, 8) = "FAX" '7
                xlWorkSheet.Cells(1, 9) = "Alternate Phone" '8
                xlWorkSheet.Cells(1, 10) = "Alternate Contact" '9
                xlWorkSheet.Cells(1, 11) = "E-mail" '10
                xlWorkSheet.Cells(1, 12) = "CC" '11
                xlWorkSheet.Cells(1, 13) = "Billing Address" '12
                xlWorkSheet.Cells(1, 14) = "Tax Code" '13
                xlWorkSheet.Cells(1, 15) = "Classification" '14
                xlWorkSheet.Cells(1, 16) = "Terms" '15
                xlWorkSheet.Cells(1, 17) = "Sales Rep" '16
                xlWorkSheet.Cells(1, 18) = "Account No" '17
                xlWorkSheet.Cells(1, 19) = "Account Recievable" '18
                xlWorkSheet.Cells(1, 20) = "Sales Discount" '19

                xlWorkSheet.Columns(1).ColumnWidth = 45
                xlWorkSheet.Columns(2).ColumnWidth = 25
                xlWorkSheet.Columns(3).ColumnWidth = 25
                xlWorkSheet.Columns(4).ColumnWidth = 7
                xlWorkSheet.Columns(5).ColumnWidth = 9
                xlWorkSheet.Columns(6).ColumnWidth = 25
                xlWorkSheet.Columns(7).ColumnWidth = 15
                xlWorkSheet.Columns(8).ColumnWidth = 15
                xlWorkSheet.Columns(9).ColumnWidth = 15
                xlWorkSheet.Columns(10).ColumnWidth = 15
                xlWorkSheet.Columns(11).ColumnWidth = 15
                xlWorkSheet.Columns(12).ColumnWidth = 15
                xlWorkSheet.Columns(13).ColumnWidth = 45
                xlWorkSheet.Columns(14).ColumnWidth = 15
                xlWorkSheet.Columns(15).ColumnWidth = 25
                xlWorkSheet.Columns(16).ColumnWidth = 20
                xlWorkSheet.Columns(17).ColumnWidth = 25
                xlWorkSheet.Columns(18).ColumnWidth = 25
                xlWorkSheet.Columns(19).ColumnWidth = 45
                xlWorkSheet.Columns(20).ColumnWidth = 45

            Case "Supplier Master"
                xlWorkSheet.Cells(1, 1) = "Company Name" '0
                xlWorkSheet.Cells(1, 2) = "Last Name" '1
                xlWorkSheet.Cells(1, 3) = "First Name" '2
                xlWorkSheet.Cells(1, 4) = "M I" '3
                xlWorkSheet.Cells(1, 5) = "Salutation" '4
                xlWorkSheet.Cells(1, 6) = "Address" '5
                xlWorkSheet.Cells(1, 7) = "Contact" '6
                xlWorkSheet.Cells(1, 8) = "Phone" '7
                xlWorkSheet.Cells(1, 9) = "FAX" '8
                xlWorkSheet.Cells(1, 10) = "Alternate Phone" '9
                xlWorkSheet.Cells(1, 11) = "Alternate Contact" '10
                xlWorkSheet.Cells(1, 12) = "E-mail" '11
                xlWorkSheet.Cells(1, 13) = "CC" '12
                xlWorkSheet.Cells(1, 14) = "Account No" '13
                xlWorkSheet.Cells(1, 15) = "TIN" '14
                xlWorkSheet.Cells(1, 16) = "Accounts Payable" '15
                xlWorkSheet.Cells(1, 17) = "Terms" '16

                xlWorkSheet.Columns(1).ColumnWidth = 45
                xlWorkSheet.Columns(2).ColumnWidth = 25
                xlWorkSheet.Columns(3).ColumnWidth = 25
                xlWorkSheet.Columns(4).ColumnWidth = 7
                xlWorkSheet.Columns(5).ColumnWidth = 9
                xlWorkSheet.Columns(6).ColumnWidth = 45
                xlWorkSheet.Columns(7).ColumnWidth = 15
                xlWorkSheet.Columns(8).ColumnWidth = 15
                xlWorkSheet.Columns(9).ColumnWidth = 15
                xlWorkSheet.Columns(10).ColumnWidth = 15
                xlWorkSheet.Columns(11).ColumnWidth = 15
                xlWorkSheet.Columns(12).ColumnWidth = 15
                xlWorkSheet.Columns(13).ColumnWidth = 15
                xlWorkSheet.Columns(14).ColumnWidth = 20
                xlWorkSheet.Columns(15).ColumnWidth = 20
                xlWorkSheet.Columns(16).ColumnWidth = 45
                xlWorkSheet.Columns(17).ColumnWidth = 20
        End Select

        xlWorkSheet.Columns.Columns.HorizontalAlignment = 3
        xlWorkSheet.SaveAs(xFilePath)

        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        'MsgBox("Excel file created , you can find the file c:\")

        Dim ps As New ProcessStartInfo
        ps.UseShellExecute = True
        ps.FileName = xFilePath
        Process.Start(ps)

    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        DlgSaveUploader.FileName = "(" & xMode & ") Uploader " & Format(Now.Date, "MM-dd-yyyy").ToString
        DlgSaveUploader.ShowDialog()
    End Sub
    Private Sub CreateIdentifierForCOA()
        Dim sSqlCmd As String
        For Each xItem As DataGridViewRow In grdUploader.Rows
            sSqlCmd = "select acnt_id from dbo.mAccounts where co_id = '" & fxKeyCompany & "' and acnt_code = '" & xItem.Cells(1).Value & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    rd.Read()
                    If rd.HasRows = True Then
                        xItem.Cells("xfxKeyAccount").Value = rd.Item(0).ToString
                    Else
                        xItem.Cells("xfxKeyAccount").Value = Guid.NewGuid.ToString
                    End If
                End Using
            Catch ex As Exception

            End Try

        Next

        Dim xSubAcnt As String = ""
        Dim xParentKey As String = ""
        For Each xSubAccountOf As DataGridViewRow In grdUploader.Rows
            xSubAcnt = xSubAccountOf.Cells(3).Value.ToString
            If xSubAcnt <> "" Or xSubAcnt <> Nothing Then
                For Each xAccount As DataGridViewRow In grdUploader.Rows
                    If xAccount.Cells(1).Value.ToString = xSubAcnt Then
                        xParentKey = xAccount.Cells("xfxKeyAccount").Value.ToString
                        Exit For
                    End If
                Next
                xSubAccountOf.Cells("xfxKeySubAccountOf").Value = xParentKey
            End If
        Next
    End Sub
    Private Sub UploadExcelFile()
        If xFilePath <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(xFilePath)
            Dim FileName As String = xFilePath

            Dim oleDA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", gOleConn)
            Try
                'get data from excel
                oleDA.Fill(gDT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = gDT.Rows.Count

                If gDT.Rows.Count <> 0 Then
                    grdUploader.DataSource = gDT
                End If

                Select Case xMode
                    Case "Item Master"
                        grdUploader.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        grdUploader.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        grdUploader.Columns(4).DefaultCellStyle.Format = "##,##0.00"
                        grdUploader.Columns(5).DefaultCellStyle.Format = "##,##0.00"

                    Case "Chart of Accounts"
                        Dim xfxKeyAccount As New DataGridViewTextBoxColumn
                        Dim xfxKeySubAccountOf As New DataGridViewTextBoxColumn

                        With xfxKeyAccount
                            .Name = "xfxKeyAccount"
                            .Visible = False
                        End With

                        grdUploader.Columns(1).ReadOnly = True

                        With xfxKeySubAccountOf
                            .Name = "xfxKeySubAccountOf"
                            .Visible = False
                        End With
                        With grdUploader.Columns
                            .Add(xfxKeyAccount)
                            .Add(xfxKeySubAccountOf)
                        End With
                        CreateIdentifierForCOA()

                    Case "Customer Master"
                        grdUploader.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

                    Case "Supplier Master"
                        grdUploader.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells

                End Select
               
            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show(ex.Message, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                gOleConn.Close()
            End Try
        End If


    End Sub
    Public Sub GetConnection(ByVal FilePath As String)
        'check if the file exists
        Try
            If Not File.Exists(FilePath) Then
                MessageBox.Show("File does not exist or access is denied!", "Upload", MessageBoxButtons.OK)
            Else
                'connection string
                Dim gCnstring As String
                If UploaderType = ".xlsx" Then
                    gCnstring = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1""")
                Else
                    gCnstring = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", FilePath)
                End If
                gOleConn = New OleDbConnection(gCnstring)
                gOleCmd = gOleConn.CreateCommand
                gOleConn.Open()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub DlgSaveUploader_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgSaveUploader.FileOk
        xFilePath = DlgSaveUploader.FileName
        GenerateTemplate()
    End Sub
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        DlgOpenUploader.ShowDialog()
    End Sub
    Private Sub DlgOpenUploader_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgOpenUploader.FileOk
        txtPath.Text = DlgOpenUploader.FileName
        xFilePath = txtPath.Text
        UploaderType = IO.Path.GetExtension(xFilePath)
        If grdUploader.AllowUserToAddRows = True Then
            grdUploader.AllowUserToAddRows = False
        End If
        gDT.Rows.Clear()
        gDT.Columns.Clear()
        'If grdUploader.Rows.Count <> 0 Then
        '    Dim xAppendOption As MsgBoxResult = MsgBox("Do you want to add the contents of the selected uploader to the existing selected data?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Insertion of data")
        '    If xAppendOption = MsgBoxResult.No Then
        '        gDT.Rows.Clear()
        '    End If
        'End If
        UploadExcelFile()

        If grdUploader.AllowUserToAddRows = False Then
            grdUploader.AllowUserToAddRows = True
        End If
    End Sub

    Private Sub txtPath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPath.Click
        DlgOpenUploader.ShowDialog()
    End Sub

    
    Private Sub txtPath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        xFilePath = txtPath.Text
    End Sub
    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim xForm As New frmItemUploadProcess
        xForm.xParent = Me
        xForm.ShowDialog()
    End Sub
    Private Sub frmItemMasterUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = xMode & " Uploader"
    End Sub
End Class