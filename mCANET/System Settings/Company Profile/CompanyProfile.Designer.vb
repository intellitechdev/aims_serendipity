﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CompanyProfile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtcofax = New System.Windows.Forms.MaskedTextBox()
        Me.txtcophone = New System.Windows.Forms.MaskedTextBox()
        Me.txtcosite = New System.Windows.Forms.TextBox()
        Me.txtcoemail = New System.Windows.Forms.TextBox()
        Me.txtcostadd = New System.Windows.Forms.TextBox()
        Me.txtcotaxid = New System.Windows.Forms.TextBox()
        Me.txtcolegal = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtcocityadd = New System.Windows.Forms.TextBox()
        Me.txtcozipadd = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtcofax
        '
        Me.txtcofax.Location = New System.Drawing.Point(425, 132)
        Me.txtcofax.Mask = "(99) 000-0000"
        Me.txtcofax.Name = "txtcofax"
        Me.txtcofax.Size = New System.Drawing.Size(139, 23)
        Me.txtcofax.TabIndex = 29
        '
        'txtcophone
        '
        Me.txtcophone.Location = New System.Drawing.Point(139, 132)
        Me.txtcophone.Mask = "(99) 000-0000"
        Me.txtcophone.Name = "txtcophone"
        Me.txtcophone.Size = New System.Drawing.Size(139, 23)
        Me.txtcophone.TabIndex = 26
        '
        'txtcosite
        '
        Me.txtcosite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcosite.BackColor = System.Drawing.Color.White
        Me.txtcosite.Location = New System.Drawing.Point(139, 190)
        Me.txtcosite.Name = "txtcosite"
        Me.txtcosite.Size = New System.Drawing.Size(425, 23)
        Me.txtcosite.TabIndex = 31
        '
        'txtcoemail
        '
        Me.txtcoemail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcoemail.BackColor = System.Drawing.Color.White
        Me.txtcoemail.Location = New System.Drawing.Point(139, 161)
        Me.txtcoemail.Name = "txtcoemail"
        Me.txtcoemail.Size = New System.Drawing.Size(425, 23)
        Me.txtcoemail.TabIndex = 30
        '
        'txtcostadd
        '
        Me.txtcostadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcostadd.BackColor = System.Drawing.Color.White
        Me.txtcostadd.Location = New System.Drawing.Point(139, 75)
        Me.txtcostadd.Name = "txtcostadd"
        Me.txtcostadd.Size = New System.Drawing.Size(425, 23)
        Me.txtcostadd.TabIndex = 20
        '
        'txtcotaxid
        '
        Me.txtcotaxid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcotaxid.BackColor = System.Drawing.Color.White
        Me.txtcotaxid.Location = New System.Drawing.Point(139, 46)
        Me.txtcotaxid.Name = "txtcotaxid"
        Me.txtcotaxid.Size = New System.Drawing.Size(242, 23)
        Me.txtcotaxid.TabIndex = 19
        '
        'txtcolegal
        '
        Me.txtcolegal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcolegal.BackColor = System.Drawing.Color.White
        Me.txtcolegal.Location = New System.Drawing.Point(139, 17)
        Me.txtcolegal.Name = "txtcolegal"
        Me.txtcolegal.Size = New System.Drawing.Size(425, 23)
        Me.txtcolegal.TabIndex = 17
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(388, 136)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(28, 15)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Fax"
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 165)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(98, 15)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "Email Address"
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(16, 193)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 15)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Web Site"
        '
        'txtcocityadd
        '
        Me.txtcocityadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcocityadd.BackColor = System.Drawing.Color.White
        Me.txtcocityadd.Location = New System.Drawing.Point(139, 103)
        Me.txtcocityadd.Name = "txtcocityadd"
        Me.txtcocityadd.Size = New System.Drawing.Size(242, 23)
        Me.txtcocityadd.TabIndex = 23
        '
        'txtcozipadd
        '
        Me.txtcozipadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcozipadd.BackColor = System.Drawing.Color.White
        Me.txtcozipadd.Location = New System.Drawing.Point(425, 103)
        Me.txtcozipadd.MaxLength = 10
        Me.txtcozipadd.Name = "txtcozipadd"
        Me.txtcozipadd.Size = New System.Drawing.Size(139, 23)
        Me.txtcozipadd.TabIndex = 25
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 136)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 15)
        Me.Label8.TabIndex = 28
        Me.Label8.Text = "Phone"
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(388, 107)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(28, 15)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "Zip"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 107)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 15)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "City"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 78)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(105, 15)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Street Address"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 15)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Tax ID"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 15)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Legal Name"
        '
        'Button1
        '
        Me.Button1.Image = Global.CSAcctg.My.Resources.Resources.change_password
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(19, 221)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(138, 39)
        Me.Button1.TabIndex = 37
        Me.Button1.Text = "Change Password"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(489, 221)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 39)
        Me.btnClose.TabIndex = 36
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(413, 221)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 39)
        Me.btnSave.TabIndex = 35
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'CompanyProfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(586, 268)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtcofax)
        Me.Controls.Add(Me.txtcophone)
        Me.Controls.Add(Me.txtcosite)
        Me.Controls.Add(Me.txtcoemail)
        Me.Controls.Add(Me.txtcostadd)
        Me.Controls.Add(Me.txtcotaxid)
        Me.Controls.Add(Me.txtcolegal)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtcocityadd)
        Me.Controls.Add(Me.txtcozipadd)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "CompanyProfile"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Project Profile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtcofax As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtcophone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtcosite As System.Windows.Forms.TextBox
    Friend WithEvents txtcoemail As System.Windows.Forms.TextBox
    Friend WithEvents txtcostadd As System.Windows.Forms.TextBox
    Friend WithEvents txtcotaxid As System.Windows.Forms.TextBox
    Friend WithEvents txtcolegal As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtcocityadd As System.Windows.Forms.TextBox
    Friend WithEvents txtcozipadd As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
