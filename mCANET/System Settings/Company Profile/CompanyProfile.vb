﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class CompanyProfile
    Dim con As New Clsappconfiguration

    Private Sub CompanyProfile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadDetails()
    End Sub

    Private Sub LoadDetails()
        txtcolegal.Text = gLegalName
        txtcotaxid.Text = gTaxId
        txtcostadd.Text = gStAdd
        txtcocityadd.Text = gCityAdd
        txtcozipadd.Text = gZipAdd
        txtcophone.Text = gPhone
        txtcofax.Text = gFax
        txtcoemail.Text = gEmail
        txtcosite.Text = gWebSite
    End Sub


    Private Sub btnSave_Click() Handles btnSave.Click
        Try
            Dim cID As New Guid(gCompanyID)
            SqlHelper.ExecuteNonQuery(con.cnstring, "usp_m_company_update",
                    New SqlParameter("@coid", cID),
                    New SqlParameter("@coname", txtcolegal.Text),
                    New SqlParameter("@cotaxid", txtcotaxid.Text),
                                            New SqlParameter("@costreet", txtcostadd.Text),
                                            New SqlParameter("@cocity", txtcocityadd.Text),
                                            New SqlParameter("@cozip", txtcozipadd.Text),
                                            New SqlParameter("@cophone", txtcophone.Text),
                                            New SqlParameter("@cofax", txtcofax.Text),
                                            New SqlParameter("@coemail", txtcoemail.Text),
                                            New SqlParameter("@cosite", txtcosite.Text))
            MsgBox("Saved! Please close system to apply updates.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click

    End Sub
End Class