Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

''' <summary>
'''Restrict non-admin users in saving transactions on Restricted Date Range
'''</summary>
''' <remarks>TODO: Must be able to Enter range outside the current year</remarks>
Public Class frmPeriodRestrictions

    Dim gcon As New Clsappconfiguration

    Private Sub SavePeriodChanges(ByVal Month As String, ByVal Year As String, ByVal fbOpen As String)
        Dim sSqlCmd As String = "usp_t_PeriodRestrictions '" & Month & "', '" & _
                                Year & "', '" & fbOpen & "', '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim xRow As Integer = 0
        For Each Period As DataGridViewRow In DgvCalendar.Rows
            xRow = Period.Index
            Dim xMonth As String = DgvCalendar.Item(1, xRow).Value
            Dim xYear As String = cboCalendarYear.SelectedItem
            Dim xStatus As String
            If DgvCalendar.Item(0, xRow).Value = True Then
                xStatus = 1
            Else
                xStatus = 0
            End If
            SavePeriodChanges(xMonth, xYear, xStatus)
        Next
        loadPeriodRestrictionDetails()
        MsgBox("Update Successful", MsgBoxStyle.Information, "Period Restrictions")
        'Dim cbxBoxes() As CheckBox = {cbxJanuary, cbxFebruary, cbxMarch, cbxApril, cbxMay, cbxJune, cbxJuly, _
        '                cbxAugust, cbxSeptember, cbxOctober, cbxNovember, cbxDecember}

        'Dim sSQLCmd As String = "usp_t_PeriodRestrictions "
        'sSQLCmd &= CInt(cbxBoxes(0).Checked) & " "

        'For i As Integer = 1 To 11
        '    sSQLCmd &= "," & CInt(cbxBoxes(i).Checked) & " "
        'Next


    End Sub
    Private Sub ChangeStatus()

        For Each Item As DataGridViewRow In DgvCalendar.Rows
            If DgvCalendar.Item(0, Item.Index).Value = True Then
                DgvCalendar.Item(2, Item.Index).Value = "CLOSED"
            Else
                DgvCalendar.Item(2, Item.Index).Value = "OPEN"
            End If
        Next


    End Sub
    Private Sub frmPeriodRestrictions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadPeriodRestrictionDetails()
    End Sub
    Private Sub loadPeriodRestrictionDetails()
        'Dim cbxBoxes() As CheckBox = {cbxJanuary, cbxFebruary, cbxMarch, cbxApril, cbxMay, cbxJune, cbxJuly, _
        '                            cbxAugust, cbxSeptember, cbxOctober, cbxNovember, cbxDecember}
        'Dim lblLabels() As Label = {lblJanuary, lblFebruary, lblMarch, lblApril, lblMay, lblJune, lblJuly, _
        '                                lblAugust, lblSeptember, lblOctober, lblNovember, lblDecember}
        Dim xTable As DataTable
        DgvCalendar.Columns.Clear()
        'cbxJanuary.Checked = False
        'cbxFebruary.Checked = False
        'cbxMarch.Checked = False
        'cbxApril.Checked = False
        'cbxMay.Checked = False
        'cbxJune.Checked = False
        'cbxJuly.Checked = False
        'cbxAugust.Checked = False
        'cbxSeptember.Checked = False
        'cbxOctober.Checked = False
        'cbxNovember.Checked = False
        'cbxDecember.Checked = False

        Dim sSQLCmd As String
        sSQLCmd = "SELECT fbOpen, fcMonth FROM dbo.tPeriodRestrictions WHERE fxKeyCompany ='" & gCompanyID() & _
                    "' AND fbYear ='" & cboCalendarYear.SelectedItem & "'"
        Dim xDataSet As New DataSet
        xDataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Dim Status As New DataGridViewTextBoxColumn
        xTable = xDataSet.Tables(0)
        With DgvCalendar
            .DataSource = xTable.DefaultView
            .Columns(0).HeaderText = ""
            .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(1).HeaderText = "Month"
            .Columns(1).Width = 137
            .Columns(1).ReadOnly = True
            .Columns.Add(Status)
            .Columns(2).HeaderText = "Status"
            .Columns(2).ReadOnly = True

        End With
        ChangeStatus()

        'Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
        '    For i As Integer = 0 To 11
        '        rd.Read()
        '        If rd.Item(0).ToString = True Then
        '            cbxBoxes(i).CheckState = CheckState.Checked
        '            lblLabels(i).Text = "Close"
        '            lblLabels(i).ForeColor = Color.Red
        '        Else
        '            lblLabels(i).Text = "Open"
        '            lblLabels(i).ForeColor = Color.RoyalBlue
        '        End If
        '    Next
        'End Using

    End Sub
    'Private Sub updatePeriodRestrictions()
    '    'Dim cbxBoxes() As CheckBox = {cbxJanuary, cbxFebruary, cbxMarch, cbxApril, cbxMay, cbxJune, cbxJuly, _
    '    '                            cbxAugust, cbxSeptember, cbxOctober, cbxNovember, cbxDecember}
    '    'Dim lblLabels() As Label = {lblJanuary, lblFebruary, lblMarch, lblApril, lblMay, lblJune, lblJuly, _
    '    '                                lblAugust, lblSeptember, lblOctober, lblNovember, lblDecember}

    '    For i As Integer = 0 To 11
    '        If (cbxBoxes(i).Checked = True) Then
    '            lblLabels(i).ForeColor = Color.Red
    '            lblLabels(i).Text = "Close"
    '        Else
    '            lblLabels(i).ForeColor = Color.RoyalBlue
    '            lblLabels(i).Text = "Open"
    '        End If
    '    Next
    'End Sub
    Private Sub cboCalendarYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCalendarYear.SelectedIndexChanged
        loadPeriodRestrictionDetails()
    End Sub
    Private Sub DgvCalendar_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvCalendar.CellContentClick
        ChangeStatus()
    End Sub
    Private Sub DgvCalendar_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvCalendar.CurrentCellDirtyStateChanged
        If DgvCalendar.IsCurrentRowDirty = True Then
            ChangeStatus()
            btnSave.Select()
            DgvCalendar.Select()
        End If
    End Sub

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        frm_New_Period.ShowDialog()
        loadPeriodRestrictionDetails()
    End Sub
End Class