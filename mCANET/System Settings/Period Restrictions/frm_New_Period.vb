Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: Aug 28, 2009                       ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Public Class frm_New_Period

    Dim gcon As New Clsappconfiguration

    Private Sub Save_New_Period(ByVal DateStarted As String, ByVal DateEnd As String, _
                                ByVal Year As String, ByVal xMonth As String)
        Dim Month_in_Words As String
        Select Case xMonth
            Case 1
                Month_in_Words = "January"
            Case "2"
                Month_in_Words = "February"
            Case "3"
                Month_in_Words = "March"
            Case "4"
                Month_in_Words = "April"
            Case "5"
                Month_in_Words = "May"
            Case "6"
                Month_in_Words = "June"
            Case "7"
                Month_in_Words = "July"
            Case "8"
                Month_in_Words = "August"
            Case "9"
                Month_in_Words = "September"
            Case "10"
                Month_in_Words = "October"
            Case "11"
                Month_in_Words = "November"
            Case "12"
                Month_in_Words = "December"
        End Select
        Dim sSqlCmd As String = "SELECT * FROM dbo.tPeriodRestrictions WHERE fxKeyCompany = '" & _
                                gCompanyID() & "' AND fcMonth = '" & Month_in_Words & "' AND fbYear = '" & _
                                Year & "'"
        Try
            Using Finder As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While Finder.Read
                    MsgBox("Period already existed." & vbCr & "Please select another period.", _
                            MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Period existence detected")
                    Exit Sub
                End While
                Dim sSqlCmdAddPeriod As String = "INSERT INTO dbo.tPeriodRestrictions "
                sSqlCmdAddPeriod &= "(fcMonth, fcDateTo, fcDateFrom, fbOpen, fbYear, fxKeyCompany)"
                sSqlCmdAddPeriod &= "VALUES ('" & Month_in_Words & "', '" & _
                                    DateEnd & "', '" & DateStarted & "', '0','" & Year & "', '" & _
                                    gCompanyID() & "')"
                Try
                    SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmdAddPeriod)
                    MsgBox("Period sucessfully created.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Access Granted")
                    Me.Close()
                Catch ex As Exception
                End Try
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Private Sub BtnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCreate.Click
        Dim xDateStart As String
        Dim xDateEnd As String
        Dim xMonth As Integer = DTCalendar.Value.Month
        Dim xYear As String = DTCalendar.Value.Year
        Dim xEndDate As String = Date.DaysInMonth(DTCalendar.Value.Year, DTCalendar.Value.Month)
        xDateStart = xYear & "-" & Convert.ToString(xMonth) & "-01 00:00:00"
        xDateEnd = xYear & "-" & Convert.ToString(xMonth) & "-" & xEndDate & " 00:00:00"

        'MsgBox("Start: " & xDateStart & vbCr & "End: " & xDateEnd)
        Save_New_Period(xDateStart, xDateEnd, xYear, xMonth)
    End Sub

    Private Sub frm_New_Period_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DTCalendar.CustomFormat = "MMMM yyyy"
    End Sub
End Class