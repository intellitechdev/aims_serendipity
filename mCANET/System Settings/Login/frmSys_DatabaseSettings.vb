Public Class frmSys_DatabaseSettings

    Private Sub confirmDatabase()
        Dim sConfigContent As String
        Dim sErr As String = ""
        Dim bUpdate As Boolean
        Dim strDBSettings As String
        Dim strEncryptedPwd As String
        Dim myConfig As New Clsappconfiguration
        Dim dbOK As Boolean

        strEncryptedPwd = Clsappconfiguration.GetEncryptedData(txtPassword.Text)
        strDBSettings = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & _
                        vbCrLf & "<configuration>" & vbCrLf & vbNewLine & _
                          vbTab & "<appSettings>" & vbCrLf & vbNewLine & _
                                     vbTab & vbTab & "<add key=" & Chr(34) & "Server" & Chr(34) & " value=" & Chr(34) & txtServer.Text & Chr(34) & " />" & _
                            vbCrLf & vbTab & vbTab & "<add key=" & Chr(34) & "Database" & Chr(34) & " value= " & Chr(34) & txtDatabase.Text & Chr(34) & " />" & _
                            vbCrLf & vbTab & vbTab & "<add key=" & Chr(34) & "Username" & Chr(34) & " value=" & Chr(34) & txtUserName.Text & Chr(34) & " />" & _
                            vbCrLf & vbTab & vbTab & "<add key=" & Chr(34) & "Password" & Chr(34) & " value=" & Chr(34) & strEncryptedPwd & Chr(34) & " />" & _
                          vbNewLine & vbCrLf & vbTab & "</appSettings>" & vbNewLine & vbCrLf & "</configuration>"

        'Chr(34) & "Hello" & Chr(34)

        sConfigContent = GetFileContents(strCurrentFileName & ".config", sErr)
        If sErr = "" Then
            dbOK = Clsappconfiguration.ServerCheck
            If dbOK Then
                bUpdate = SaveTextToFile(strDBSettings, strCurrentFileName & ".config", sErr)
                If bUpdate Then
                    MessageBox.Show("Database connection parameter has been change" & vbCrLf & _
                           "The application will automatically run again.", "Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Application.Restart()
                    End
                Else
                    MessageBox.Show(sErr)
                End If
            Else
                MessageBox.Show("Please enter the correct parameter!", "Database Settings", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show(sErr)
        End If
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        confirmDatabase()
    End Sub

    Private Sub frmSys_DatabaseSettings_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Escape) Then
            Me.Close()
        ElseIf e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            confirmDatabase()
        End If
    End Sub

    Private Sub frmSys_DatabaseSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim myConnection As New Clsappconfiguration
        txtServer.Text = myConnection.Server
        txtDatabase.Text = myConnection.Database
    End Sub
End Class