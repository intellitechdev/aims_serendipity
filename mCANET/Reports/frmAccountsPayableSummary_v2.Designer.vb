<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountsPayableSummary_v2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountsPayableSummary_v2))
        Me.topPanel = New System.Windows.Forms.Panel()
        Me.btnLoadReport = New System.Windows.Forms.Button()
        Me.dteTo = New System.Windows.Forms.DateTimePicker()
        Me.dteFrom = New System.Windows.Forms.DateTimePicker()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker()
        Me.topPanel.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'topPanel
        '
        Me.topPanel.Controls.Add(Me.btnLoadReport)
        Me.topPanel.Controls.Add(Me.dteTo)
        Me.topPanel.Controls.Add(Me.dteFrom)
        Me.topPanel.Controls.Add(Me.lblTo)
        Me.topPanel.Controls.Add(Me.lblFrom)
        Me.topPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.topPanel.Location = New System.Drawing.Point(0, 0)
        Me.topPanel.Name = "topPanel"
        Me.topPanel.Size = New System.Drawing.Size(875, 46)
        Me.topPanel.TabIndex = 0
        '
        'btnLoadReport
        '
        Me.btnLoadReport.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnLoadReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLoadReport.Location = New System.Drawing.Point(340, 7)
        Me.btnLoadReport.Name = "btnLoadReport"
        Me.btnLoadReport.Size = New System.Drawing.Size(127, 34)
        Me.btnLoadReport.TabIndex = 6
        Me.btnLoadReport.Text = "Generate Report"
        Me.btnLoadReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLoadReport.UseVisualStyleBackColor = True
        '
        'dteTo
        '
        Me.dteTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteTo.Location = New System.Drawing.Point(220, 14)
        Me.dteTo.Name = "dteTo"
        Me.dteTo.Size = New System.Drawing.Size(114, 23)
        Me.dteTo.TabIndex = 3
        '
        'dteFrom
        '
        Me.dteFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteFrom.Location = New System.Drawing.Point(62, 14)
        Me.dteFrom.Name = "dteFrom"
        Me.dteFrom.Size = New System.Drawing.Size(114, 23)
        Me.dteFrom.TabIndex = 2
        Me.dteFrom.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(181, 16)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(28, 16)
        Me.lblTo.TabIndex = 1
        Me.lblTo.Text = "To:"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFrom.Location = New System.Drawing.Point(14, 16)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(42, 16)
        Me.lblFrom.TabIndex = 0
        Me.lblFrom.Text = "From:"
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.Location = New System.Drawing.Point(0, 46)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.ShowGotoPageButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(875, 515)
        Me.crvRpt.TabIndex = 1
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Location = New System.Drawing.Point(369, 226)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(128, 83)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 2
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'bgwLoadReport
        '
        '
        'frmAccountsPayableSummary_v2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(875, 561)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.topPanel)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(570, 568)
        Me.Name = "frmAccountsPayableSummary_v2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Accounts Payable Summary"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.topPanel.ResumeLayout(False)
        Me.topPanel.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents topPanel As System.Windows.Forms.Panel
    Friend WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents dteTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents btnLoadReport As System.Windows.Forms.Button
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
End Class
