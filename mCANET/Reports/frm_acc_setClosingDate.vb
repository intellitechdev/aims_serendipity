Public Class frm_acc_setClosingDate

    Private gTrans As New clsTransactionFunctions

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If txtClpassword.Text = txtCmpassword.Text Then
            gTrans.gSaveClosingDate(Microsoft.VisualBasic.FormatDateTime(dteClosingDate.Value, DateFormat.ShortDate), txtCmpassword.Text, Me)
            gTrans.gClearFormTxt(Me)
        ElseIf txtClpassword.Text = "" Or txtCmpassword.Text = "" Then
            MessageBox.Show("Please provide password for closing date.", "CLICKSOFTWARE:" + Me.Text)
        ElseIf txtClpassword.Text <> txtCmpassword.Text Then
            MessageBox.Show("Password mismatch.", "CLICKSOFTWARE:" + Me.Text)
            gTrans.gClearFormTxt(Me)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        gTrans.gClearFormTxt(Me)
        Me.Close()
    End Sub


End Class