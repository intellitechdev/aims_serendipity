﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Math
Imports System.IO
Imports System.ComponentModel
Public Class frmPostingError
    Private gCon As New Clsappconfiguration()
    Private Sub frmPostingError_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_TempPosting_Select")
        While rd.Read = True
            Try
                Dim row As String() =
                 {rd.Item("cLine").ToString, rd.Item("idNo").ToString, rd.Item("cName").ToString, rd.Item("LoanRef").ToString, rd.Item("acntRef").ToString}

                Dim nRowIndex As Integer
                With grdErrorList

                    .Rows.Add(row)
                    '.ClearSelection()
                    nRowIndex = .Rows.Count - 1
                    .FirstDisplayedScrollingRowIndex = nRowIndex
                    '.Rows.Add()
                End With
            Catch
                MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        End While

    End Sub
End Class