﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmViewDeduction
    Private gCon As New Clsappconfiguration()
    Private con As New Clsappconfiguration
    Dim cs = con.cnstring
    Private rptsummary As New ReportDocument
    Dim acntid As String

    Private Sub frmViewDeduction_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        'LoadAccountsDropdown()
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub LoadReport()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\ViewDeduction_Report.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@FdDate", dtpFrom.Text)
        rptsummary.SetParameterValue("@FdTo", dtpTo.Text)
        rptsummary.SetParameterValue("@FkAccountID", acntid)
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub frmBalanceSheetv2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        rptsummary.Close()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True
        'AuditTrail_Save("ACCOUNT BALANCE", "Preview account balance of '" & xTitle & "' as of " & dtpTrialBalance.Value.ToString)
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    'Private Sub LoadAccountsDropdown()
    '    Dim myid As New Guid(gCompanyID)
    '    Dim ds As New DataSet
    '    Dim ad As New SqlDataAdapter
    '    Dim cmd As New SqlCommand("spu_AccountAmortization_View", con.sqlconn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
    '    Try
    '        ad.SelectCommand = cmd
    '        ad.Fill(ds, "Accounts")
    '        With cboAcnt
    '            .ValueMember = "FkAccount"
    '            .DisplayMember = "acntwithcode"
    '            .DataSource = ds.Tables(0)
    '            '.SelectedIndex = -1
    '            .Text = "Select"
    '        End With
    '        con.sqlconn.Close()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub cboAcnt_TextChanged(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub SearchToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SearchToolStripMenuItem.Click
        frmCOAFilter.xModule = "Deductions"
        frmCOAFilter.ShowDialog()
        If frmCOAFilter.DialogResult = Windows.Forms.DialogResult.OK Then
            acntid = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
            txtAccount.Text = frmCOAFilter.grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
        End If
    End Sub

End Class