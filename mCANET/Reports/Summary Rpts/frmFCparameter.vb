Public Class frmFCparameter

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        gStartDate = Microsoft.VisualBasic.FormatDateTime(DateTimePicker1.Value, DateFormat.ShortDate)
        gEndDate = Microsoft.VisualBasic.FormatDateTime(DateTimePicker2.Value, DateFormat.ShortDate)
        frmFinanceCharge.Show()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        gStartDate = Now.Date
        gEndDate = Now.Date
        Me.Close()
    End Sub

End Class