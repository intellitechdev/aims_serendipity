Public Class frmUndepositedParameter

    Private Sub frmUndepositedParameter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_loadCustomer(cboCustomerName, cboCustomerID)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        gKeyCustomer = cboCustomerID.SelectedItem
        frmUndepositedFunds.Show()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        gKeyCustomer = cboCustomerID.SelectedItem
    End Sub
End Class