﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRptInterestComputation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpPurchaseReturn = New System.Windows.Forms.DateTimePicker()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.bgwProcessReport = New System.ComponentModel.BackgroundWorker()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(299, 240)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(124, 95)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 32
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.CachedPageNumberPerDoc = 10
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.DisplayStatusBar = False
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.crvRpt.Location = New System.Drawing.Point(0, 48)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.SelectionFormula = ""
        Me.crvRpt.ShowGotoPageButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(749, 460)
        Me.crvRpt.TabIndex = 31
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvRpt.ViewTimeSelectionFormula = ""
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.dtpPurchaseReturn)
        Me.Panel1.Controls.Add(Me.btnPreview)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(749, 48)
        Me.Panel1.TabIndex = 30
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(663, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(74, 31)
        Me.btnClose.TabIndex = 18
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(31, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 15)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Date"
        '
        'dtpPurchaseReturn
        '
        Me.dtpPurchaseReturn.CustomFormat = "MMMM dd, yyyy"
        Me.dtpPurchaseReturn.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPurchaseReturn.Location = New System.Drawing.Point(75, 18)
        Me.dtpPurchaseReturn.Name = "dtpPurchaseReturn"
        Me.dtpPurchaseReturn.Size = New System.Drawing.Size(165, 20)
        Me.dtpPurchaseReturn.TabIndex = 8
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(264, 11)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(96, 31)
        Me.btnPreview.TabIndex = 4
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'bgwProcessReport
        '
        '
        'frmRptInterestComputation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(749, 508)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmRptInterestComputation"
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Private WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpPurchaseReturn As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents bgwProcessReport As System.ComponentModel.BackgroundWorker
End Class
