﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Public Class frmIncomeStatementv2
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim RptType As String

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        rptsummary.Close()
        Select Case RptType
            Case "Single Form"
                If cboAsOF.Text = "As Of" Then
                    rptsummary.Load(Application.StartupPath & "\Accounting Reports\IncomeStatementSingleForm.rpt")
                    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@coid", gCompanyID())
                    rptsummary.SetParameterValue("@thismonth", dtpTrialBalance.Text)
                Else
                    rptsummary.Load(Application.StartupPath & "\Accounting Reports\IncomeStatementSingleFormForTheMonth.rpt")
                    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@coid", gCompanyID())
                    rptsummary.SetParameterValue("@thismonth", dtpTrialBalance.Text)
                End If
            Case "By Months"
                If cboAsOF.Text = "As Of" Then
                    rptsummary.Load(Application.StartupPath & "\Accounting Reports\IncomeStatementv2.rpt")
                    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@coid", gCompanyID())
                    rptsummary.SetParameterValue("@thismonth", GetLastDayOfMonth(dtpTrialBalance.Text))
                    rptsummary.SetParameterValue("@prevmonth", GetLastDayOfPreviousMonth(dtpTrialBalance.Text))
                Else
                    rptsummary.Load(Application.StartupPath & "\Accounting Reports\IncomeStatementv2ForTheMonth.rpt")
                    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@coid", gCompanyID())
                    rptsummary.SetParameterValue("@thismonth", GetLastDayOfMonth(dtpTrialBalance.Text))
                    rptsummary.SetParameterValue("@prevmonth", GetLastDayOfPreviousMonth(dtpTrialBalance.Text))
                End If
                
            Case "By Years"
                If rbMonth.Checked = True And rbYear.Checked = False Then
                    rptsummary.Load(Application.StartupPath & "\Accounting Reports\IncomeStatementv2ByYear.rpt")
                    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@coid", gCompanyID())
                    rptsummary.SetParameterValue("@thismonth", GetLastDayOfMonth(dtpTrialBalance.Text))
                    rptsummary.SetParameterValue("@prevmonth", GetLastDayOfMonth(dtp2.Text))
                ElseIf rbYear.Checked = True Then
                    rptsummary.Load(Application.StartupPath & "\Accounting Reports\IncomeStatementv2ByYear.rpt")
                    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@coid", gCompanyID())
                    rptsummary.SetParameterValue("@thismonth", GetLastDayOfTheYear(dtpTrialBalance.Value.Year.ToString))
                    rptsummary.SetParameterValue("@prevmonth", GetLastDayOfTheYear(dtp2.Value.Year.ToString))
                End If
        End Select
    End Sub

    Private Function GetLastDayOfMonth(ByVal dtDate As DateTime) As DateTime
        Dim dtTo As New DateTime(dtDate.Year, dtDate.Month, 1)
        dtTo = dtTo.AddMonths(1)
        dtTo = dtTo.AddDays(-(dtTo.Day))
        Return dtTo
    End Function

    Private Function GetLastDayOfPreviousMonth(ByVal dtDate As DateTime) As DateTime
        Dim dtTo As New DateTime(dtDate.Year, dtDate.Month, 1)
        dtTo = dtTo.AddMonths(0)
        dtTo = dtTo.AddDays(-(dtTo.Day))
        Return dtTo
    End Function

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub frmIncomeStatementv2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        rptsummary.Close()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True
        AuditSave()
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub AuditSave()
        RptType = cboType.Text
        Select Case RptType
            Case "Single Form"
                AuditTrail_Save("REPORTS", "View Financial Statements > Statements of Operation (" & cboType.Text & ") > " & dtpTrialBalance.Text)
            Case "By Months"
                AuditTrail_Save("REPORTS", "View Financial Statements > Statements of Operation (Comparative " & cboType.Text & ") > " & dtpTrialBalance.Text)
            Case "By Years"
                If rbMonth.Checked = True Then
                    AuditTrail_Save("REPORTS", "View Financial Statements > Statements of Operation (Comparative " & cboType.Text & ") > " & dtpTrialBalance.Text & " and " & dtp2.Text & " - (Months)")
                Else
                    AuditTrail_Save("REPORTS", "View Financial Statements > Statements of Operation (Comparative " & cboType.Text & ") > " & dtpTrialBalance.Text & " and " & dtp2.Text & " - (Years)")
                End If
        End Select
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub cboType_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboType.TextChanged
        RptType = cboType.Text
        Select Case RptType
            Case "Single Form"
                dtp2.Enabled = False
                rbMonth.Visible = False
                rbYear.Visible = False
                dtpTrialBalance.CustomFormat = "MMMM dd, yyyy"
                dtp2.CustomFormat = "MMMM dd, yyyy"
            Case "By Months"
                dtp2.Enabled = False
                rbMonth.Visible = False
                rbYear.Visible = False
                dtpTrialBalance.CustomFormat = "MMMM yyyy"
                dtp2.CustomFormat = "MMMM yyyy"
            Case "By Years"
                dtp2.Enabled = True
                rbMonth.Visible = True
                rbYear.Visible = True
                rbMonth.Checked = True
        End Select
    End Sub

    Private Sub rbMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbMonth.Click
        dtpTrialBalance.CustomFormat = "MMMM yyyy"
        dtp2.CustomFormat = "MMMM yyyy"
        rbYear.Checked = False
    End Sub

    Private Sub rbYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbYear.Click
        dtpTrialBalance.CustomFormat = "yyyy"
        dtp2.CustomFormat = "yyyy"
        rbMonth.Checked = False
    End Sub

    Private Function GetLastDayOfTheYear(ByVal dtDate As String) As Date
        Dim yr As String
        Dim LastDayOfYear As Date
        yr = ("December 31, " + dtDate)
        LastDayOfYear = Date.Parse(yr)
        Return LastDayOfYear
    End Function

    Private Sub frmIncomeStatementv2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboType.Text = "Single Form"
        cboAsOF.Text = "As Of"
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
    End Sub
End Class