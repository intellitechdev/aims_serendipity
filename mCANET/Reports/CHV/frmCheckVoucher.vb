﻿Public Class frmCheckVoucher

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If rbPrint.Checked = True Then
            frmVoucherReport.xTransaction = "CHECK VOUCHER"
            frmVoucherReport.docnum = frmGeneralJournalEntries.txtGeneralJournalNo.Text
            frmVoucherReport.xTransID = frmGeneralJournalEntries.keyID
            frmVoucherReport.xLoan = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cLoanRef", 0)
            frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
            frmVoucherReport.Show()
        ElseIf rbReprint.Checked = True Then
            frmVoucherReport.xTransaction = "CHECK VOUCHER REPRINT"
            frmVoucherReport.docnum = frmGeneralJournalEntries.txtGeneralJournalNo.Text
            frmVoucherReport.xTransID = frmGeneralJournalEntries.keyID
            frmVoucherReport.xLoan = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cLoanRef", 0)
            frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
            frmVoucherReport.Show()
        ElseIf rbRebate.Checked = True Then
            frmTempRebates.ShowDialog()
        ElseIf rbVoucher.Checked = True Then
            frmVoucherReport.xTransaction = "CHECK VOUCHER NOT LOAN"
            frmVoucherReport.docnum = frmGeneralJournalEntries.txtGeneralJournalNo.Text
            frmVoucherReport.xTransID = frmGeneralJournalEntries.keyID
            frmVoucherReport.xLoan = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cLoanRef", 0)
            frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
            frmVoucherReport.Show()
        End If
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub rbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPrint.Click
        rbPrint.Checked = True
        rbReprint.Checked = False
        rbRebate.Checked = False
        rbVoucher.Checked = False
    End Sub

    Private Sub rbReprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbReprint.Click
        rbPrint.Checked = False
        rbReprint.Checked = True
        rbRebate.Checked = False
        rbVoucher.Checked = False
    End Sub

    Private Sub rbRebate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbRebate.Click
        rbPrint.Checked = False
        rbReprint.Checked = False
        rbRebate.Checked = True
        rbVoucher.Checked = False
    End Sub

    Private Sub rbVoucher_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbVoucher.Click
        rbPrint.Checked = False
        rbReprint.Checked = False
        rbRebate.Checked = False
        rbVoucher.Checked = True
    End Sub
End Class