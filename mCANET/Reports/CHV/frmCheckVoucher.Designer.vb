﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckVoucher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rbPrint = New System.Windows.Forms.RadioButton()
        Me.rbReprint = New System.Windows.Forms.RadioButton()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.rbRebate = New System.Windows.Forms.RadioButton()
        Me.rbVoucher = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'rbPrint
        '
        Me.rbPrint.AutoSize = True
        Me.rbPrint.Checked = True
        Me.rbPrint.Location = New System.Drawing.Point(12, 21)
        Me.rbPrint.Name = "rbPrint"
        Me.rbPrint.Size = New System.Drawing.Size(151, 19)
        Me.rbPrint.TabIndex = 0
        Me.rbPrint.TabStop = True
        Me.rbPrint.Text = "Print Loan Voucher"
        Me.rbPrint.UseVisualStyleBackColor = True
        '
        'rbReprint
        '
        Me.rbReprint.AutoSize = True
        Me.rbReprint.Location = New System.Drawing.Point(170, 21)
        Me.rbReprint.Name = "rbReprint"
        Me.rbReprint.Size = New System.Drawing.Size(172, 19)
        Me.rbReprint.TabIndex = 1
        Me.rbReprint.Text = "Re-print Loan Voucher"
        Me.rbReprint.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(186, 57)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(267, 57)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'rbRebate
        '
        Me.rbRebate.AutoSize = True
        Me.rbRebate.Location = New System.Drawing.Point(12, 57)
        Me.rbRebate.Name = "rbRebate"
        Me.rbRebate.Size = New System.Drawing.Size(144, 19)
        Me.rbRebate.TabIndex = 4
        Me.rbRebate.Text = "Voucher w/ Rebate"
        Me.rbRebate.UseVisualStyleBackColor = True
        Me.rbRebate.Visible = False
        '
        'rbVoucher
        '
        Me.rbVoucher.AutoSize = True
        Me.rbVoucher.Location = New System.Drawing.Point(12, 82)
        Me.rbVoucher.Name = "rbVoucher"
        Me.rbVoucher.Size = New System.Drawing.Size(116, 19)
        Me.rbVoucher.TabIndex = 5
        Me.rbVoucher.Text = "Print Voucher"
        Me.rbVoucher.UseVisualStyleBackColor = True
        Me.rbVoucher.Visible = False
        '
        'frmCheckVoucher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(350, 99)
        Me.Controls.Add(Me.rbVoucher)
        Me.Controls.Add(Me.rbRebate)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.rbReprint)
        Me.Controls.Add(Me.rbPrint)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCheckVoucher"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Check Voucher"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rbPrint As System.Windows.Forms.RadioButton
    Friend WithEvents rbReprint As System.Windows.Forms.RadioButton
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents rbRebate As System.Windows.Forms.RadioButton
    Friend WithEvents rbVoucher As System.Windows.Forms.RadioButton
End Class
