﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDailyCashCollection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvCashCollectionReport = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.bgwProcessReport = New System.ComponentModel.BackgroundWorker()
        Me.dtpListing = New System.Windows.Forms.DateTimePicker()
        Me.RBDaily = New System.Windows.Forms.RadioButton()
        Me.RBMonthly = New System.Windows.Forms.RadioButton()
        Me.cbodate = New System.Windows.Forms.ComboBox()
        Me.dtpYear = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvCashCollectionReport
        '
        Me.crvCashCollectionReport.ActiveViewIndex = -1
        Me.crvCashCollectionReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvCashCollectionReport.CachedPageNumberPerDoc = 10
        Me.crvCashCollectionReport.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvCashCollectionReport.DisplayStatusBar = False
        Me.crvCashCollectionReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvCashCollectionReport.EnableDrillDown = False
        Me.crvCashCollectionReport.Location = New System.Drawing.Point(0, 45)
        Me.crvCashCollectionReport.Name = "crvCashCollectionReport"
        Me.crvCashCollectionReport.ShowGotoPageButton = False
        Me.crvCashCollectionReport.ShowGroupTreeButton = False
        Me.crvCashCollectionReport.ShowParameterPanelButton = False
        Me.crvCashCollectionReport.ShowRefreshButton = False
        Me.crvCashCollectionReport.Size = New System.Drawing.Size(980, 501)
        Me.crvCashCollectionReport.TabIndex = 1
        Me.crvCashCollectionReport.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'bgwProcessReport
        '
        '
        'dtpListing
        '
        Me.dtpListing.CustomFormat = "MMMM dd, yyyy"
        Me.dtpListing.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpListing.Location = New System.Drawing.Point(78, 12)
        Me.dtpListing.Name = "dtpListing"
        Me.dtpListing.Size = New System.Drawing.Size(184, 23)
        Me.dtpListing.TabIndex = 26
        Me.dtpListing.Visible = False
        '
        'RBDaily
        '
        Me.RBDaily.AutoSize = True
        Me.RBDaily.BackColor = System.Drawing.Color.Transparent
        Me.RBDaily.Location = New System.Drawing.Point(12, 16)
        Me.RBDaily.Name = "RBDaily"
        Me.RBDaily.Size = New System.Drawing.Size(60, 19)
        Me.RBDaily.TabIndex = 27
        Me.RBDaily.Text = "Daily"
        Me.RBDaily.UseVisualStyleBackColor = False
        '
        'RBMonthly
        '
        Me.RBMonthly.AutoSize = True
        Me.RBMonthly.BackColor = System.Drawing.Color.Transparent
        Me.RBMonthly.Location = New System.Drawing.Point(302, 16)
        Me.RBMonthly.Name = "RBMonthly"
        Me.RBMonthly.Size = New System.Drawing.Size(74, 19)
        Me.RBMonthly.TabIndex = 28
        Me.RBMonthly.Text = "Monthly"
        Me.RBMonthly.UseVisualStyleBackColor = False
        '
        'cbodate
        '
        Me.cbodate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbodate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbodate.FormattingEnabled = True
        Me.cbodate.Location = New System.Drawing.Point(382, 12)
        Me.cbodate.Name = "cbodate"
        Me.cbodate.Size = New System.Drawing.Size(106, 23)
        Me.cbodate.TabIndex = 29
        Me.cbodate.Visible = False
        '
        'dtpYear
        '
        Me.dtpYear.CustomFormat = "yyyy"
        Me.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpYear.Location = New System.Drawing.Point(494, 12)
        Me.dtpYear.Name = "dtpYear"
        Me.dtpYear.Size = New System.Drawing.Size(72, 23)
        Me.dtpYear.TabIndex = 32
        Me.dtpYear.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel1.Controls.Add(Me.dtpYear)
        Me.Panel1.Controls.Add(Me.cbodate)
        Me.Panel1.Controls.Add(Me.RBMonthly)
        Me.Panel1.Controls.Add(Me.RBDaily)
        Me.Panel1.Controls.Add(Me.dtpListing)
        Me.Panel1.Controls.Add(Me.MenuStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(980, 45)
        Me.Panel1.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem, Me.PreviewToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(980, 45)
        Me.MenuStrip1.TabIndex = 33
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CloseToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(70, 41)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'PreviewToolStripMenuItem
        '
        Me.PreviewToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.PreviewToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PreviewToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.PreviewToolStripMenuItem.Name = "PreviewToolStripMenuItem"
        Me.PreviewToolStripMenuItem.Size = New System.Drawing.Size(84, 41)
        Me.PreviewToolStripMenuItem.Text = "Preview"
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(416, 277)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(139, 117)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picLoading.TabIndex = 2
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'frmDailyCashCollection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(980, 546)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvCashCollectionReport)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmDailyCashCollection"
        Me.Text = "Cash Collections"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvCashCollectionReport As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents bgwProcessReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents dtpListing As System.Windows.Forms.DateTimePicker
    Friend WithEvents RBDaily As System.Windows.Forms.RadioButton
    Friend WithEvents RBMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents cbodate As System.Windows.Forms.ComboBox
    Friend WithEvents dtpYear As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PreviewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
