Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmCollection


    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

   
#Region "Events"
    'Private Sub frmCollection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    Call m_loadItemBrand(cboBrand)
    'End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            cboBrand.Enabled = True
        ElseIf RadioButton2.Checked = True Then
            cboBrand.Enabled = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton1.Checked = True Then
            cboBrand.Enabled = True
        ElseIf RadioButton2.Checked = True Then
            cboBrand.Enabled = False
        End If
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        If RadioButton1.Checked = True Then
            Call reportperbrand()
        ElseIf RadioButton2.Checked = True Then
            Call reportsummary()
        End If
    End Sub

    Private Sub cboBrand_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBrand.SelectedIndexChanged
        If cboBrand.SelectedItem = "create" Then
            frmBrand.ShowDialog()
        End If
    End Sub
#End Region

#Region "Procedures"
    Private Sub reportsummary()
        Dim sMonth As String = CStr(cboMonth.SelectedIndex + 1)
        Dim sYear As String = CStr(cboYear.SelectedItem)

        'rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\collectionreport.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@month", sMonth)
        rptsummary.SetParameterValue("@year", sYear)
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        'rptsummary.SetParameterValue("@brand", cboBrand.SelectedItem)

        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Private Sub reportperbrand()
        Dim sMonth As String = CStr(cboMonth.SelectedIndex + 1)
        Dim sYear As String = CStr(cboYear.SelectedItem)
        Dim sBrand As String = brandcode(cboBrand.SelectedItem)

        ' rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\collectionreport_perbrand.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@month", sMonth)
        rptsummary.SetParameterValue("@year", sYear)
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@brand", sBrand)

        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Private Function brandcode(ByVal sVal As String) As String
        Dim result As String = ""
        If sVal = "Havaianas" Then
            result = "HAV"
        ElseIf sVal = "David & Goliath" Then
            result = "D&G"
        ElseIf sVal = "TBox" Then
            result = "TBox"
        End If
        Return result
    End Function

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

#End Region

  
    Private Sub frmCollection_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub
End Class