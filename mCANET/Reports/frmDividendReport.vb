﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports Microsoft.Reporting.WinForms
Public Class frmDividendReport
    Private con As New Clsappconfiguration

    Private Sub LoadReport()
        RptVwer.SetDisplayMode(DisplayMode.PrintLayout)
        RptVwer.ZoomMode = ZoomMode.Percent
        RptVwer.ZoomPercent = 100

        RptVwer.Reset()
        RptVwer.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
        RptVwer.LocalReport.ReportPath = Environment.CurrentDirectory + "\Accounting Reports\SSRS Reports\DividendDistribution.rdl"

        Dim dsSummary As DataSet = PrintDividend()
        Dim rDs As ReportDataSource = New ReportDataSource("DataSet1", dsSummary.Tables(0))
        RptVwer.LocalReport.DataSources.Clear()
        RptVwer.LocalReport.DataSources.Add(rDs)
        RptVwer.ServerReport.Refresh()
        RptVwer.RefreshReport()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub frmDividendReport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        AuditTrail_Save("DIVIDEND", "Preview Dividend Distribution Report")
        'Me.RptVwer.RefreshReport()
        LoadReport()
    End Sub

    Private Sub PrintToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PrintToolStripMenuItem.Click

    End Sub
End Class