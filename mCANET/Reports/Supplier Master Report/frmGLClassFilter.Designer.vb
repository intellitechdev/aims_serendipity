<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGLClassFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGLClassFilter))
        Me.TVClass = New System.Windows.Forms.TreeView
        Me.BtnSingleAdd = New System.Windows.Forms.Button
        Me.btnSingleRemove = New System.Windows.Forms.Button
        Me.BtnOK = New System.Windows.Forms.Button
        Me.BtnCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.DGVClasses = New System.Windows.Forms.DataGridView
        Me.ChkSubClasses = New System.Windows.Forms.CheckBox
        Me.LstClass = New System.Windows.Forms.ListView
        Me.ClassName = New System.Windows.Forms.ColumnHeader
        Me.ClassRefNo = New System.Windows.Forms.ColumnHeader
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.DGVClasses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TVClass
        '
        Me.TVClass.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TVClass.HideSelection = False
        Me.TVClass.HotTracking = True
        Me.TVClass.Location = New System.Drawing.Point(3, 27)
        Me.TVClass.Name = "TVClass"
        Me.TVClass.ShowNodeToolTips = True
        Me.TVClass.Size = New System.Drawing.Size(234, 212)
        Me.TVClass.TabIndex = 0
        '
        'BtnSingleAdd
        '
        Me.BtnSingleAdd.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSingleAdd.Location = New System.Drawing.Point(243, 113)
        Me.BtnSingleAdd.Name = "BtnSingleAdd"
        Me.BtnSingleAdd.Size = New System.Drawing.Size(103, 36)
        Me.BtnSingleAdd.TabIndex = 4
        Me.BtnSingleAdd.Text = "Add Class"
        Me.ToolTip1.SetToolTip(Me.BtnSingleAdd, "Add single o multiple class to the filter.")
        Me.BtnSingleAdd.UseVisualStyleBackColor = True
        '
        'btnSingleRemove
        '
        Me.btnSingleRemove.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSingleRemove.Location = New System.Drawing.Point(243, 155)
        Me.btnSingleRemove.Name = "btnSingleRemove"
        Me.btnSingleRemove.Size = New System.Drawing.Size(103, 36)
        Me.btnSingleRemove.TabIndex = 7
        Me.btnSingleRemove.Text = "Remove Class"
        Me.ToolTip1.SetToolTip(Me.btnSingleRemove, "Remove single or multiple Classes in the filter.")
        Me.btnSingleRemove.UseVisualStyleBackColor = True
        '
        'BtnOK
        '
        Me.BtnOK.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOK.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.BtnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnOK.Location = New System.Drawing.Point(422, 245)
        Me.BtnOK.Name = "BtnOK"
        Me.BtnOK.Size = New System.Drawing.Size(81, 28)
        Me.BtnOK.TabIndex = 8
        Me.BtnOK.Text = "Ok"
        Me.BtnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnOK.UseVisualStyleBackColor = True
        '
        'BtnCancel
        '
        Me.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BtnCancel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.BtnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnCancel.Location = New System.Drawing.Point(509, 245)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(81, 28)
        Me.BtnCancel.TabIndex = 9
        Me.BtnCancel.Text = "Cancel"
        Me.BtnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Classes"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(349, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(123, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Current Class Filters"
        '
        'DGVClasses
        '
        Me.DGVClasses.AllowUserToAddRows = False
        Me.DGVClasses.AllowUserToDeleteRows = False
        Me.DGVClasses.AllowUserToResizeRows = False
        Me.DGVClasses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVClasses.Location = New System.Drawing.Point(3, 146)
        Me.DGVClasses.Name = "DGVClasses"
        Me.DGVClasses.Size = New System.Drawing.Size(234, 93)
        Me.DGVClasses.TabIndex = 10
        Me.DGVClasses.Visible = False
        '
        'ChkSubClasses
        '
        Me.ChkSubClasses.AutoSize = True
        Me.ChkSubClasses.BackColor = System.Drawing.Color.Transparent
        Me.ChkSubClasses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ChkSubClasses.FlatAppearance.BorderSize = 0
        Me.ChkSubClasses.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkSubClasses.Location = New System.Drawing.Point(251, 61)
        Me.ChkSubClasses.Name = "ChkSubClasses"
        Me.ChkSubClasses.Size = New System.Drawing.Size(95, 46)
        Me.ChkSubClasses.TabIndex = 12
        Me.ChkSubClasses.Text = "Include " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "SubClasses " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Mode"
        Me.ToolTip1.SetToolTip(Me.ChkSubClasses, "Include or not include Subclasses in adding or removing classes operation.")
        Me.ChkSubClasses.UseVisualStyleBackColor = False
        '
        'LstClass
        '
        Me.LstClass.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.LstClass.AutoArrange = False
        Me.LstClass.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClassName, Me.ClassRefNo})
        Me.LstClass.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LstClass.FullRowSelect = True
        Me.LstClass.GridLines = True
        Me.LstClass.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LstClass.HideSelection = False
        Me.LstClass.HotTracking = True
        Me.LstClass.HoverSelection = True
        Me.LstClass.Location = New System.Drawing.Point(352, 27)
        Me.LstClass.MultiSelect = False
        Me.LstClass.Name = "LstClass"
        Me.LstClass.ShowItemToolTips = True
        Me.LstClass.Size = New System.Drawing.Size(238, 212)
        Me.LstClass.TabIndex = 13
        Me.LstClass.UseCompatibleStateImageBehavior = False
        Me.LstClass.View = System.Windows.Forms.View.Details
        '
        'ClassName
        '
        Me.ClassName.Text = "Class Name"
        Me.ClassName.Width = 233
        '
        'ClassRefNo
        '
        Me.ClassRefNo.Text = "Reference Number"
        Me.ClassRefNo.Width = 0
        '
        'frmGLClassFilter
        '
        Me.AcceptButton = Me.BtnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.CancelButton = Me.BtnCancel
        Me.ClientSize = New System.Drawing.Size(596, 277)
        Me.ControlBox = False
        Me.Controls.Add(Me.LstClass)
        Me.Controls.Add(Me.DGVClasses)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnCancel)
        Me.Controls.Add(Me.BtnOK)
        Me.Controls.Add(Me.btnSingleRemove)
        Me.Controls.Add(Me.BtnSingleAdd)
        Me.Controls.Add(Me.TVClass)
        Me.Controls.Add(Me.ChkSubClasses)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGLClassFilter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Class Filter"
        CType(Me.DGVClasses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TVClass As System.Windows.Forms.TreeView
    Friend WithEvents BtnSingleAdd As System.Windows.Forms.Button
    Friend WithEvents btnSingleRemove As System.Windows.Forms.Button
    Friend WithEvents BtnOK As System.Windows.Forms.Button
    Friend WithEvents BtnCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DGVClasses As System.Windows.Forms.DataGridView
    Friend WithEvents ChkSubClasses As System.Windows.Forms.CheckBox
    Friend WithEvents LstClass As System.Windows.Forms.ListView
    Friend WithEvents ClassName As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClassRefNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
