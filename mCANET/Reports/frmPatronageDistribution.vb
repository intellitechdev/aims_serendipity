﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports Microsoft.Reporting.WinForms

Public Class frmPatronageDistribution

    Private con As New Clsappconfiguration

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub LoadReport()
        RptVwer.SetDisplayMode(DisplayMode.PrintLayout)
        RptVwer.ZoomMode = ZoomMode.Percent
        RptVwer.ZoomPercent = 100

        RptVwer.Reset()
        RptVwer.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
        RptVwer.LocalReport.ReportPath = Environment.CurrentDirectory + "\Accounting Reports\SSRS Reports\PatronageDistribution.rdl"

        Dim dsSummary As DataSet = PrintDividend()
        Dim rDs As ReportDataSource = New ReportDataSource("DataSet1", dsSummary.Tables(0))
        RptVwer.LocalReport.DataSources.Clear()
        RptVwer.LocalReport.DataSources.Add(rDs)
        RptVwer.ServerReport.Refresh()
        RptVwer.RefreshReport()
    End Sub

    Private Sub frmPatronageDistribution_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        AuditTrail_Save("Patronage", "Preview Patronage Distribution Report")
        'Me.RptVwer.RefreshReport()
        LoadReport()
    End Sub
End Class