<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIncomeStatement_PrevYear_DrillDown
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIncomeStatement_PrevYear_DrillDown))
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboLoadLevel = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnPreview = New System.Windows.Forms.Button
        Me.dateFrom = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.panelTop = New System.Windows.Forms.Panel
        Me.dateTo = New System.Windows.Forms.DateTimePicker
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.panelTop.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(478, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Type:"
        Me.Label3.Visible = False
        '
        'cboLoadLevel
        '
        Me.cboLoadLevel.FormattingEnabled = True
        Me.cboLoadLevel.Items.AddRange(New Object() {"Summarized", "Detailed"})
        Me.cboLoadLevel.Location = New System.Drawing.Point(518, 9)
        Me.cboLoadLevel.Name = "cboLoadLevel"
        Me.cboLoadLevel.Size = New System.Drawing.Size(157, 21)
        Me.cboLoadLevel.TabIndex = 12
        Me.cboLoadLevel.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Date from"
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Location = New System.Drawing.Point(339, 6)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(121, 23)
        Me.btnPreview.TabIndex = 11
        Me.btnPreview.Text = "Preview Report"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'dateFrom
        '
        Me.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateFrom.Location = New System.Drawing.Point(71, 7)
        Me.dateFrom.Name = "dateFrom"
        Me.dateFrom.Size = New System.Drawing.Size(111, 20)
        Me.dateFrom.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(188, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Until"
        '
        'panelTop
        '
        Me.panelTop.Controls.Add(Me.Label3)
        Me.panelTop.Controls.Add(Me.cboLoadLevel)
        Me.panelTop.Controls.Add(Me.Label1)
        Me.panelTop.Controls.Add(Me.btnPreview)
        Me.panelTop.Controls.Add(Me.dateFrom)
        Me.panelTop.Controls.Add(Me.Label2)
        Me.panelTop.Controls.Add(Me.dateTo)
        Me.panelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTop.Location = New System.Drawing.Point(0, 0)
        Me.panelTop.Name = "panelTop"
        Me.panelTop.Size = New System.Drawing.Size(703, 36)
        Me.panelTop.TabIndex = 1
        '
        'dateTo
        '
        Me.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateTo.Location = New System.Drawing.Point(222, 7)
        Me.dateTo.Name = "dateTo"
        Me.dateTo.Size = New System.Drawing.Size(111, 20)
        Me.dateTo.TabIndex = 9
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.EnableRefresh = False
        Me.crvRpt.Location = New System.Drawing.Point(0, 36)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(703, 450)
        Me.crvRpt.TabIndex = 2
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'bgwLoadReport
        '
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(262, 187)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(179, 113)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 15
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'frmIncomeStatement_PrevYear_DrillDown
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(703, 486)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.panelTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmIncomeStatement_PrevYear_DrillDown"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Income Statement Previous Year Comparison"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panelTop.ResumeLayout(False)
        Me.panelTop.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboLoadLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents dateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents panelTop As System.Windows.Forms.Panel
    Friend WithEvents dateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
End Class
