﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCRJDynamicColumn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCRJDynamicColumn))
        Me.txtColumn1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtColumn2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtColumn3 = New System.Windows.Forms.TextBox()
        Me.btnDelete3 = New System.Windows.Forms.Button()
        Me.btnDelete2 = New System.Windows.Forms.Button()
        Me.btnDelete1 = New System.Windows.Forms.Button()
        Me.btnSearch3 = New System.Windows.Forms.Button()
        Me.btnSearch2 = New System.Windows.Forms.Button()
        Me.btnSearch1 = New System.Windows.Forms.Button()
        Me.btnSave3 = New System.Windows.Forms.Button()
        Me.btnSave2 = New System.Windows.Forms.Button()
        Me.btnSave1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtColumn1
        '
        Me.txtColumn1.BackColor = System.Drawing.Color.White
        Me.txtColumn1.Location = New System.Drawing.Point(98, 24)
        Me.txtColumn1.Name = "txtColumn1"
        Me.txtColumn1.ReadOnly = True
        Me.txtColumn1.Size = New System.Drawing.Size(460, 20)
        Me.txtColumn1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Column 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 14)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Column 2"
        '
        'txtColumn2
        '
        Me.txtColumn2.BackColor = System.Drawing.Color.White
        Me.txtColumn2.Location = New System.Drawing.Point(98, 59)
        Me.txtColumn2.Name = "txtColumn2"
        Me.txtColumn2.ReadOnly = True
        Me.txtColumn2.Size = New System.Drawing.Size(460, 20)
        Me.txtColumn2.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 14)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Column 3"
        '
        'txtColumn3
        '
        Me.txtColumn3.BackColor = System.Drawing.Color.White
        Me.txtColumn3.Location = New System.Drawing.Point(98, 94)
        Me.txtColumn3.Name = "txtColumn3"
        Me.txtColumn3.ReadOnly = True
        Me.txtColumn3.Size = New System.Drawing.Size(460, 20)
        Me.txtColumn3.TabIndex = 4
        '
        'btnDelete3
        '
        Me.btnDelete3.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.btnDelete3.Location = New System.Drawing.Point(640, 89)
        Me.btnDelete3.Name = "btnDelete3"
        Me.btnDelete3.Size = New System.Drawing.Size(32, 29)
        Me.btnDelete3.TabIndex = 14
        Me.btnDelete3.UseVisualStyleBackColor = True
        '
        'btnDelete2
        '
        Me.btnDelete2.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.btnDelete2.Location = New System.Drawing.Point(640, 54)
        Me.btnDelete2.Name = "btnDelete2"
        Me.btnDelete2.Size = New System.Drawing.Size(32, 29)
        Me.btnDelete2.TabIndex = 13
        Me.btnDelete2.UseVisualStyleBackColor = True
        '
        'btnDelete1
        '
        Me.btnDelete1.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.btnDelete1.Location = New System.Drawing.Point(640, 19)
        Me.btnDelete1.Name = "btnDelete1"
        Me.btnDelete1.Size = New System.Drawing.Size(32, 29)
        Me.btnDelete1.TabIndex = 12
        Me.btnDelete1.UseVisualStyleBackColor = True
        '
        'btnSearch3
        '
        Me.btnSearch3.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearch3.Location = New System.Drawing.Point(564, 89)
        Me.btnSearch3.Name = "btnSearch3"
        Me.btnSearch3.Size = New System.Drawing.Size(32, 29)
        Me.btnSearch3.TabIndex = 11
        Me.btnSearch3.UseVisualStyleBackColor = True
        '
        'btnSearch2
        '
        Me.btnSearch2.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearch2.Location = New System.Drawing.Point(564, 54)
        Me.btnSearch2.Name = "btnSearch2"
        Me.btnSearch2.Size = New System.Drawing.Size(32, 29)
        Me.btnSearch2.TabIndex = 10
        Me.btnSearch2.UseVisualStyleBackColor = True
        '
        'btnSearch1
        '
        Me.btnSearch1.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearch1.Location = New System.Drawing.Point(564, 19)
        Me.btnSearch1.Name = "btnSearch1"
        Me.btnSearch1.Size = New System.Drawing.Size(32, 29)
        Me.btnSearch1.TabIndex = 9
        Me.btnSearch1.UseVisualStyleBackColor = True
        '
        'btnSave3
        '
        Me.btnSave3.Image = CType(resources.GetObject("btnSave3.Image"), System.Drawing.Image)
        Me.btnSave3.Location = New System.Drawing.Point(602, 89)
        Me.btnSave3.Name = "btnSave3"
        Me.btnSave3.Size = New System.Drawing.Size(32, 29)
        Me.btnSave3.TabIndex = 8
        Me.btnSave3.UseVisualStyleBackColor = True
        '
        'btnSave2
        '
        Me.btnSave2.Image = CType(resources.GetObject("btnSave2.Image"), System.Drawing.Image)
        Me.btnSave2.Location = New System.Drawing.Point(602, 54)
        Me.btnSave2.Name = "btnSave2"
        Me.btnSave2.Size = New System.Drawing.Size(32, 29)
        Me.btnSave2.TabIndex = 7
        Me.btnSave2.UseVisualStyleBackColor = True
        '
        'btnSave1
        '
        Me.btnSave1.Image = CType(resources.GetObject("btnSave1.Image"), System.Drawing.Image)
        Me.btnSave1.Location = New System.Drawing.Point(602, 19)
        Me.btnSave1.Name = "btnSave1"
        Me.btnSave1.Size = New System.Drawing.Size(32, 29)
        Me.btnSave1.TabIndex = 6
        Me.btnSave1.UseVisualStyleBackColor = True
        '
        'frmCRJDynamicColumn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(694, 136)
        Me.Controls.Add(Me.btnDelete3)
        Me.Controls.Add(Me.btnDelete2)
        Me.Controls.Add(Me.btnDelete1)
        Me.Controls.Add(Me.btnSearch3)
        Me.Controls.Add(Me.btnSearch2)
        Me.Controls.Add(Me.btnSearch1)
        Me.Controls.Add(Me.btnSave3)
        Me.Controls.Add(Me.btnSave2)
        Me.Controls.Add(Me.btnSave1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtColumn3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtColumn2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtColumn1)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCRJDynamicColumn"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CRJ Dynamic Column Setup"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtColumn1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtColumn2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtColumn3 As System.Windows.Forms.TextBox
    Friend WithEvents btnSave1 As System.Windows.Forms.Button
    Friend WithEvents btnSave2 As System.Windows.Forms.Button
    Friend WithEvents btnSave3 As System.Windows.Forms.Button
    Friend WithEvents btnSearch1 As System.Windows.Forms.Button
    Friend WithEvents btnSearch2 As System.Windows.Forms.Button
    Friend WithEvents btnSearch3 As System.Windows.Forms.Button
    Friend WithEvents btnDelete3 As System.Windows.Forms.Button
    Friend WithEvents btnDelete2 As System.Windows.Forms.Button
    Friend WithEvents btnDelete1 As System.Windows.Forms.Button
End Class
