<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrialBalanceSFM
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.lblDateTo = New System.Windows.Forms.Label
        Me.lblDateFrom = New System.Windows.Forms.Label
        Me.dteTo = New System.Windows.Forms.DateTimePicker
        Me.dteFrom = New System.Windows.Forms.DateTimePicker
        Me.crvTrialBalance = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker
        Me.Panel1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.picLoading)
        Me.Panel1.Controls.Add(Me.btnGenerate)
        Me.Panel1.Controls.Add(Me.lblDateTo)
        Me.Panel1.Controls.Add(Me.lblDateFrom)
        Me.Panel1.Controls.Add(Me.dteTo)
        Me.Panel1.Controls.Add(Me.dteFrom)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(860, 45)
        Me.Panel1.TabIndex = 0
        '
        'picLoading
        '
        Me.picLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.loading_gif_animation
        Me.picLoading.Location = New System.Drawing.Point(408, 8)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(28, 31)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLoading.TabIndex = 2
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'btnGenerate
        '
        Me.btnGenerate.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.Location = New System.Drawing.Point(298, 10)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(104, 26)
        Me.btnGenerate.TabIndex = 4
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'lblDateTo
        '
        Me.lblDateTo.AutoSize = True
        Me.lblDateTo.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(160, 14)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(28, 19)
        Me.lblDateTo.TabIndex = 3
        Me.lblDateTo.Text = "To:"
        '
        'lblDateFrom
        '
        Me.lblDateFrom.AutoSize = True
        Me.lblDateFrom.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(12, 13)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(45, 19)
        Me.lblDateFrom.TabIndex = 2
        Me.lblDateFrom.Text = "From:"
        '
        'dteTo
        '
        Me.dteTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTo.Location = New System.Drawing.Point(195, 13)
        Me.dteTo.Name = "dteTo"
        Me.dteTo.Size = New System.Drawing.Size(83, 20)
        Me.dteTo.TabIndex = 1
        '
        'dteFrom
        '
        Me.dteFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteFrom.Location = New System.Drawing.Point(63, 14)
        Me.dteFrom.Name = "dteFrom"
        Me.dteFrom.Size = New System.Drawing.Size(83, 20)
        Me.dteFrom.TabIndex = 0
        '
        'crvTrialBalance
        '
        Me.crvTrialBalance.ActiveViewIndex = -1
        Me.crvTrialBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvTrialBalance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvTrialBalance.Location = New System.Drawing.Point(0, 45)
        Me.crvTrialBalance.Name = "crvTrialBalance"
        Me.crvTrialBalance.SelectionFormula = ""
        Me.crvTrialBalance.ShowGroupTreeButton = False
        Me.crvTrialBalance.Size = New System.Drawing.Size(860, 473)
        Me.crvTrialBalance.TabIndex = 1
        Me.crvTrialBalance.ViewTimeSelectionFormula = ""
        '
        'bgwLoadReport
        '
        '
        'FrmTrialBalance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 518)
        Me.Controls.Add(Me.crvTrialBalance)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmTrialBalance"
        Me.ShowIcon = False
        Me.Text = "Trial Balance"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dteTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents crvTrialBalance As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
End Class
