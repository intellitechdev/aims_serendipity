Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmGeneralLedgerPerAccountvb

    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument

#Region "Procedures"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Sub m_DisplayAccountsAllToMultiColumnCbo(ByRef MultiColumnCbo As MTGCComboBox)
        Dim accounts As New DataTable("Accounts")
        Dim dataRowAccounts As DataRow
        Dim storedProc As String = "CAS_m_GetActiveAccounts"

        accounts.Columns.Add("acnt_code", System.Type.GetType("System.String"))
        accounts.Columns.Add("acnt_desc", System.Type.GetType("System.String"))
        accounts.Columns.Add("acnt_id", System.Type.GetType("System.String"))

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, storedProc, _
                            New SqlParameter("@compID", gCompanyID()))
                While rd.Read
                    dataRowAccounts = accounts.NewRow
                    dataRowAccounts("acnt_code") = rd.Item(0).ToString
                    dataRowAccounts("acnt_desc") = rd.Item(1).ToString
                    dataRowAccounts("acnt_id") = rd.Item(2).ToString
                    accounts.Rows.Add(dataRowAccounts)
                End While
            End Using

        Catch ex As Exception
            MsgBox("Error at modGlobalListing at m_DisplayAccountsAllToMultiColumnCbo." + vbCr + vbCr + ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        MultiColumnCbo.Items.Clear()
        MultiColumnCbo.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MultiColumnCbo.SourceDataString = New String(2) {"acnt_desc", "acnt_code", "acnt_id"}
        MultiColumnCbo.SourceDataTable = accounts
    End Sub

    Private Sub LoadReport()
        bgwGenLedPerAcnt.RunWorkerAsync()
    End Sub
#End Region

#Region "Events"
    Private Sub frmGeneralLedgerPerAccountvb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_DisplayAccountsAllToMultiColumnCbo(mtcboAccounts)
    End Sub

    Private Sub bgwGenLedPerAcnt_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwGenLedPerAcnt.DoWork
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\GeneralLedgerPerAccount.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))
            'rptsummary.Refresh()
            rptsummary.SetParameterValue("@acntID", mtcboAccounts.SelectedItem.Col3.ToString)
            rptsummary.SetParameterValue("@dateFrom", dteFrom.Value.Date)
            rptsummary.SetParameterValue("@dateTo", dteTo.Value.Date)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error: General Ledger Per Account", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        picLoading.Visible = True

        LoadReport()
    End Sub

    Private Sub bgwGenLedPerAcnt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwGenLedPerAcnt.RunWorkerCompleted
        Me.crvGenLedgerPerAccnt.Visible = True
        Me.crvGenLedgerPerAccnt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub
#End Region

End Class