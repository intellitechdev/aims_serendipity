﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmDailyCashCollection

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim monthValue As Integer

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        If RBDaily.Checked = True Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\DailyCashCollection.rpt")
            'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text)
            rptsummary.SetParameterValue("@coid", gCompanyID(), "SubDailyCashCollection.rpt")
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "SubDailyCashCollection.rpt")

            rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry.rpt")
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "Subreport_CancelledEntry.rpt")
            rptsummary.SetParameterValue("@Doctype", "Receipts", "Subreport_CancelledEntry.rpt")

            rptsummary.SetParameterValue("@fdDate", dtpListing.Text, "DCR_CheckDetails")
        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\MonthlyCashCollection.rpt")
            'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", monthValue)
            rptsummary.SetParameterValue("@Year", dtpYear.Text)
            rptsummary.SetParameterValue("@coid", gCompanyID(), "SubMonthlyCashCollection.rpt")
            rptsummary.SetParameterValue("@fdTransDate", monthValue, "SubMonthlyCashCollection.rpt")
            rptsummary.SetParameterValue("@Year", dtpYear.Text, "SubMonthlyCashCollection.rpt")

            rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry_Monthly.rpt")
            rptsummary.SetParameterValue("@fdTransDate", GetDateMonthly(), "Subreport_CancelledEntry_Monthly.rpt")
            rptsummary.SetParameterValue("@Doctype", "Receipts", "Subreport_CancelledEntry_Monthly.rpt")

            rptsummary.SetParameterValue("@fdDate", GetDateMonthly, "MCR_CheckDetails")
        End If
    End Sub

    Private Function GetDateMonthly()
        Return CDate(MonthValue & " " & dtpYear.Text)
    End Function

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvCashCollectionReport.Visible = True
        Me.crvCashCollectionReport.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub RBDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RBDaily.CheckedChanged
        If RBDaily.Checked = True Then
            dtpListing.Visible = True
            RBMonthly.Checked = False
            cbodate.Visible = False
            dtpYear.Visible = False
            cbodate.Text = ""
        End If
    End Sub

    Friend Class IntervalItem
        Public Property Text As String
        Public Property Value As Integer
    End Class

    Private Sub RBMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RBMonthly.CheckedChanged
        If RBMonthly.Checked = True Then
            RBDaily.Checked = False
            dtpListing.Visible = False
            cbodate.Visible = True
            dtpYear.Visible = True
            dtpListing.Value = Now
        End If
    End Sub

    Private Sub ComboMonth()
        Dim intervals As New List(Of IntervalItem)

        intervals.Add(New IntervalItem With {.Text = "", .Value = 0})
        intervals.Add(New IntervalItem With {.Text = "January", .Value = 1})
        intervals.Add(New IntervalItem With {.Text = "February", .Value = 2})
        intervals.Add(New IntervalItem With {.Text = "March", .Value = 3})
        intervals.Add(New IntervalItem With {.Text = "April", .Value = 4})
        intervals.Add(New IntervalItem With {.Text = "May", .Value = 5})
        intervals.Add(New IntervalItem With {.Text = "June", .Value = 6})
        intervals.Add(New IntervalItem With {.Text = "July", .Value = 7})
        intervals.Add(New IntervalItem With {.Text = "August", .Value = 8})
        intervals.Add(New IntervalItem With {.Text = "September", .Value = 9})
        intervals.Add(New IntervalItem With {.Text = "October", .Value = 10})
        intervals.Add(New IntervalItem With {.Text = "November", .Value = 11})
        intervals.Add(New IntervalItem With {.Text = "December", .Value = 12})

        With Me.cbodate
            .DisplayMember = "Text"
            .ValueMember = "Value"
            .DataSource = intervals
        End With
    End Sub

    Private Sub frmDailyCashCollection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RBDaily.Checked = False
        RBMonthly.Checked = False
        dtpYear.Visible = False
        dtpListing.Visible = False
        ComboMonth()
    End Sub

    Private Sub cbodate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbodate.SelectedIndexChanged
        monthValue = cbodate.SelectedIndex.ToString()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True
        If RBDaily.Checked = True Then
            AuditTrail_Save("REPORTS", "View Cash Receipts Journal > CCR - Per Collector > Daily > " & dtpListing.Text)
        Else
            AuditTrail_Save("REPORTS", "View Cash Receipts Journal > CCR - Per Collector > Monthly > " & cbodate.Text & " " & dtpYear.Text)
        End If
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class