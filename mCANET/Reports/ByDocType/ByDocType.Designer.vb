﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ByDocType
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim lblDateTo As System.Windows.Forms.Label
        Dim lblDateFrom As System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpDateRange = New System.Windows.Forms.GroupBox()
        Me.dteTo = New System.Windows.Forms.DateTimePicker()
        Me.dteFrom = New System.Windows.Forms.DateTimePicker()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.cboDoctype = New System.Windows.Forms.ComboBox()
        lblDateTo = New System.Windows.Forms.Label()
        lblDateFrom = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.grpDateRange.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblDateTo
        '
        lblDateTo.AutoSize = True
        lblDateTo.Location = New System.Drawing.Point(197, 26)
        lblDateTo.Name = "lblDateTo"
        lblDateTo.Size = New System.Drawing.Size(21, 13)
        lblDateTo.TabIndex = 2
        lblDateTo.Text = "To"
        '
        'lblDateFrom
        '
        lblDateFrom.AutoSize = True
        lblDateFrom.Location = New System.Drawing.Point(22, 26)
        lblDateFrom.Name = "lblDateFrom"
        lblDateFrom.Size = New System.Drawing.Size(36, 13)
        lblDateFrom.TabIndex = 0
        lblDateFrom.Text = "From"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.cboDoctype)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.grpDateRange)
        Me.Panel1.Controls.Add(Me.btnRefresh)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(721, 65)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Document Type"
        '
        'grpDateRange
        '
        Me.grpDateRange.Controls.Add(Me.dteTo)
        Me.grpDateRange.Controls.Add(lblDateTo)
        Me.grpDateRange.Controls.Add(Me.dteFrom)
        Me.grpDateRange.Controls.Add(lblDateFrom)
        Me.grpDateRange.Location = New System.Drawing.Point(341, 3)
        Me.grpDateRange.Name = "grpDateRange"
        Me.grpDateRange.Size = New System.Drawing.Size(367, 51)
        Me.grpDateRange.TabIndex = 1
        Me.grpDateRange.TabStop = False
        Me.grpDateRange.Text = "Date Range"
        '
        'dteTo
        '
        Me.dteTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTo.Location = New System.Drawing.Point(224, 23)
        Me.dteTo.Name = "dteTo"
        Me.dteTo.Size = New System.Drawing.Size(109, 21)
        Me.dteTo.TabIndex = 3
        '
        'dteFrom
        '
        Me.dteFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteFrom.Location = New System.Drawing.Point(63, 22)
        Me.dteFrom.Name = "dteFrom"
        Me.dteFrom.Size = New System.Drawing.Size(109, 21)
        Me.dteFrom.TabIndex = 1
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(248, 27)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 0
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 65)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(721, 355)
        Me.CrystalReportViewer1.TabIndex = 1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'cboDoctype
        '
        Me.cboDoctype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDoctype.FormattingEnabled = True
        Me.cboDoctype.Location = New System.Drawing.Point(15, 29)
        Me.cboDoctype.Name = "cboDoctype"
        Me.cboDoctype.Size = New System.Drawing.Size(227, 21)
        Me.cboDoctype.TabIndex = 49
        '
        'ByDocType
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(721, 420)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ByDocType"
        Me.Text = "ByDocType"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.grpDateRange.ResumeLayout(False)
        Me.grpDateRange.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents grpDateRange As System.Windows.Forms.GroupBox
    Friend WithEvents dteTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboDoctype As System.Windows.Forms.ComboBox
End Class
