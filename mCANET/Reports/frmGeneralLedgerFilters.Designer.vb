<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralLedgerFilters
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim lblDateFrom As System.Windows.Forms.Label
        Dim lblDateTo As System.Windows.Forms.Label
        Dim lblSubsidiary As System.Windows.Forms.Label
        Dim lblReference As System.Windows.Forms.Label
        Dim lblAccount As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGeneralLedgerFilters))
        Me.grpDateRange = New System.Windows.Forms.GroupBox
        Me.dteTo = New System.Windows.Forms.DateTimePicker
        Me.dteFrom = New System.Windows.Forms.DateTimePicker
        Me.cboSubsidiary = New MTGCComboBox
        Me.cboAccounts = New MTGCComboBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnApplyFilters = New System.Windows.Forms.Button
        Me.txtReference = New System.Windows.Forms.TextBox
        lblDateFrom = New System.Windows.Forms.Label
        lblDateTo = New System.Windows.Forms.Label
        lblSubsidiary = New System.Windows.Forms.Label
        lblReference = New System.Windows.Forms.Label
        lblAccount = New System.Windows.Forms.Label
        Me.grpDateRange.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblDateFrom
        '
        lblDateFrom.AutoSize = True
        lblDateFrom.Location = New System.Drawing.Point(22, 26)
        lblDateFrom.Name = "lblDateFrom"
        lblDateFrom.Size = New System.Drawing.Size(35, 15)
        lblDateFrom.TabIndex = 0
        lblDateFrom.Text = "From"
        '
        'lblDateTo
        '
        lblDateTo.AutoSize = True
        lblDateTo.Location = New System.Drawing.Point(22, 55)
        lblDateTo.Name = "lblDateTo"
        lblDateTo.Size = New System.Drawing.Size(19, 15)
        lblDateTo.TabIndex = 2
        lblDateTo.Text = "To"
        '
        'lblSubsidiary
        '
        lblSubsidiary.AutoSize = True
        lblSubsidiary.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblSubsidiary.Location = New System.Drawing.Point(242, 56)
        lblSubsidiary.Name = "lblSubsidiary"
        lblSubsidiary.Size = New System.Drawing.Size(66, 15)
        lblSubsidiary.TabIndex = 19
        lblSubsidiary.Text = "Subsidiary"
        '
        'lblReference
        '
        lblReference.AutoSize = True
        lblReference.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblReference.Location = New System.Drawing.Point(227, 85)
        lblReference.Name = "lblReference"
        lblReference.Size = New System.Drawing.Size(81, 15)
        lblReference.TabIndex = 18
        lblReference.Text = "Reference No."
        '
        'lblAccount
        '
        lblAccount.AutoSize = True
        lblAccount.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblAccount.Location = New System.Drawing.Point(248, 26)
        lblAccount.Name = "lblAccount"
        lblAccount.Size = New System.Drawing.Size(60, 15)
        lblAccount.TabIndex = 16
        lblAccount.Text = "Accounts:"
        '
        'grpDateRange
        '
        Me.grpDateRange.Controls.Add(Me.dteTo)
        Me.grpDateRange.Controls.Add(lblDateTo)
        Me.grpDateRange.Controls.Add(Me.dteFrom)
        Me.grpDateRange.Controls.Add(lblDateFrom)
        Me.grpDateRange.Location = New System.Drawing.Point(12, 26)
        Me.grpDateRange.Name = "grpDateRange"
        Me.grpDateRange.Size = New System.Drawing.Size(189, 92)
        Me.grpDateRange.TabIndex = 0
        Me.grpDateRange.TabStop = False
        Me.grpDateRange.Text = "Date Range"
        '
        'dteTo
        '
        Me.dteTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTo.Location = New System.Drawing.Point(63, 51)
        Me.dteTo.Name = "dteTo"
        Me.dteTo.Size = New System.Drawing.Size(109, 23)
        Me.dteTo.TabIndex = 3
        '
        'dteFrom
        '
        Me.dteFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteFrom.Location = New System.Drawing.Point(63, 22)
        Me.dteFrom.Name = "dteFrom"
        Me.dteFrom.Size = New System.Drawing.Size(109, 23)
        Me.dteFrom.TabIndex = 1
        '
        'cboSubsidiary
        '
        Me.cboSubsidiary.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboSubsidiary.ArrowColor = System.Drawing.Color.Black
        Me.cboSubsidiary.BindedControl = CType(resources.GetObject("cboSubsidiary.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboSubsidiary.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboSubsidiary.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboSubsidiary.ColumnNum = 2
        Me.cboSubsidiary.ColumnWidth = "121;0"
        Me.cboSubsidiary.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboSubsidiary.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboSubsidiary.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboSubsidiary.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboSubsidiary.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboSubsidiary.DisplayMember = "Text"
        Me.cboSubsidiary.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboSubsidiary.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboSubsidiary.DropDownForeColor = System.Drawing.Color.Black
        Me.cboSubsidiary.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cboSubsidiary.DropDownWidth = 141
        Me.cboSubsidiary.GridLineColor = System.Drawing.Color.LightGray
        Me.cboSubsidiary.GridLineHorizontal = False
        Me.cboSubsidiary.GridLineVertical = False
        Me.cboSubsidiary.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboSubsidiary.Location = New System.Drawing.Point(314, 52)
        Me.cboSubsidiary.ManagingFastMouseMoving = True
        Me.cboSubsidiary.ManagingFastMouseMovingInterval = 30
        Me.cboSubsidiary.Name = "cboSubsidiary"
        Me.cboSubsidiary.SelectedItem = Nothing
        Me.cboSubsidiary.SelectedValue = Nothing
        Me.cboSubsidiary.Size = New System.Drawing.Size(188, 24)
        Me.cboSubsidiary.TabIndex = 20
        '
        'cboAccounts
        '
        Me.cboAccounts.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboAccounts.ArrowColor = System.Drawing.Color.Black
        Me.cboAccounts.BindedControl = CType(resources.GetObject("cboAccounts.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboAccounts.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboAccounts.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboAccounts.ColumnNum = 2
        Me.cboAccounts.ColumnWidth = "121;0"
        Me.cboAccounts.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboAccounts.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboAccounts.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboAccounts.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboAccounts.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboAccounts.DisplayMember = "Text"
        Me.cboAccounts.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboAccounts.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboAccounts.DropDownForeColor = System.Drawing.Color.Black
        Me.cboAccounts.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cboAccounts.DropDownWidth = 341
        Me.cboAccounts.GridLineColor = System.Drawing.Color.LightGray
        Me.cboAccounts.GridLineHorizontal = False
        Me.cboAccounts.GridLineVertical = False
        Me.cboAccounts.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboAccounts.Location = New System.Drawing.Point(314, 22)
        Me.cboAccounts.ManagingFastMouseMoving = True
        Me.cboAccounts.ManagingFastMouseMovingInterval = 30
        Me.cboAccounts.Name = "cboAccounts"
        Me.cboAccounts.SelectedItem = Nothing
        Me.cboAccounts.SelectedValue = Nothing
        Me.cboAccounts.Size = New System.Drawing.Size(188, 24)
        Me.cboAccounts.TabIndex = 15
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(424, 111)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(78, 30)
        Me.btnCancel.TabIndex = 22
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnApplyFilters
        '
        Me.btnApplyFilters.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnApplyFilters.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnApplyFilters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApplyFilters.Location = New System.Drawing.Point(314, 111)
        Me.btnApplyFilters.Name = "btnApplyFilters"
        Me.btnApplyFilters.Size = New System.Drawing.Size(108, 30)
        Me.btnApplyFilters.TabIndex = 21
        Me.btnApplyFilters.Text = "Apply Filters"
        Me.btnApplyFilters.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApplyFilters.UseVisualStyleBackColor = True
        '
        'txtReference
        '
        Me.txtReference.Location = New System.Drawing.Point(314, 82)
        Me.txtReference.Name = "txtReference"
        Me.txtReference.Size = New System.Drawing.Size(188, 23)
        Me.txtReference.TabIndex = 23
        '
        'frmGeneralLedgerFilters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(516, 149)
        Me.Controls.Add(Me.txtReference)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnApplyFilters)
        Me.Controls.Add(Me.cboSubsidiary)
        Me.Controls.Add(lblSubsidiary)
        Me.Controls.Add(lblReference)
        Me.Controls.Add(lblAccount)
        Me.Controls.Add(Me.cboAccounts)
        Me.Controls.Add(Me.grpDateRange)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGeneralLedgerFilters"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Choose Filter"
        Me.grpDateRange.ResumeLayout(False)
        Me.grpDateRange.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grpDateRange As System.Windows.Forms.GroupBox
    Friend WithEvents dteFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboSubsidiary As MTGCComboBox
    Friend WithEvents cboAccounts As MTGCComboBox
    Friend WithEvents btnApplyFilters As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtReference As System.Windows.Forms.TextBox
End Class
