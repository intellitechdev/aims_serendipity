<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClosingEntryVoucher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClosingEntryVoucher))
        Me.crvClosingEntry = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvClosingEntry
        '
        Me.crvClosingEntry.ActiveViewIndex = -1
        Me.crvClosingEntry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvClosingEntry.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvClosingEntry.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.crvClosingEntry.Location = New System.Drawing.Point(0, 0)
        Me.crvClosingEntry.Name = "crvClosingEntry"
        Me.crvClosingEntry.ShowGroupTreeButton = False
        Me.crvClosingEntry.ShowParameterPanelButton = False
        Me.crvClosingEntry.ShowRefreshButton = False
        Me.crvClosingEntry.Size = New System.Drawing.Size(639, 503)
        Me.crvClosingEntry.TabIndex = 0
        Me.crvClosingEntry.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(258, 202)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(123, 98)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 13
        Me.picLoading.TabStop = False
        '
        'bgwLoadReport
        '
        '
        'frmClosingEntryVoucher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(639, 503)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvClosingEntry)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmClosingEntryVoucher"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Closing Entry Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvClosingEntry As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
End Class
