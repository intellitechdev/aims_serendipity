﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Public Class frmJournalListingReport
     Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim MonthValue As Integer
    Dim TransactionValue As String

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        If RBDaily.Checked = True Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\JournalListingDaily.rpt")
            'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text)
            rptsummary.SetParameterValue("@Transaction", TransactionValue)
            rptsummary.SetParameterValue("@coid", gCompanyID(), "SubReportJournalListingDaily.rpt")
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "SubReportJournalListingDaily.rpt")
            rptsummary.SetParameterValue("@Transaction", TransactionValue, "SubReportJournalListingDaily.rpt")

            rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry.rpt")
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "Subreport_CancelledEntry.rpt")
            rptsummary.SetParameterValue("@Doctype", TransactionValue, "Subreport_CancelledEntry.rpt")

        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\JournalListingMonthly.rpt")
            'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", MonthValue)
            rptsummary.SetParameterValue("@Transaction", TransactionValue)
            rptsummary.SetParameterValue("@Year", dtpYear.Text)
            rptsummary.SetParameterValue("@coid", gCompanyID(), "SubReportJournalListingMonthly.rpt")
            rptsummary.SetParameterValue("@fdTransDate", MonthValue, "SubReportJournalListingMonthly.rpt")
            rptsummary.SetParameterValue("@Transaction", TransactionValue, "SubReportJournalListingMonthly.rpt")
            rptsummary.SetParameterValue("@Year", dtpYear.Text, "SubReportJournalListingMonthly.rpt")

            rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry_Monthly.rpt")
            rptsummary.SetParameterValue("@fdTransDate", GetDateMonthly(), "Subreport_CancelledEntry_Monthly.rpt")
            rptsummary.SetParameterValue("@Doctype", TransactionValue, "Subreport_CancelledEntry_Monthly.rpt")

        End If
    End Sub

    Private Function GetDateMonthly()
        Return CDate(MonthValue & " " & dtpYear.Text)
    End Function

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
            LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub frmJournalListingReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub RBDaily_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RBDaily.CheckedChanged
        If RBDaily.Checked = True Then
            dtpListing.Visible = True
            RBMonthly.Checked = False
            cbodate.Visible = False
            dtpYear.Visible = False
            'cbotransaction.Text = ""
            cbotransaction.Enabled = True
            cbodate.Text = ""
        End If
    End Sub

    Private Sub frmJournalListingReport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        RBDaily.Checked = False
        RBMonthly.Checked = False
        cbotransaction.Enabled = False
        dtpYear.Visible = False
        Combo()
        cbodate.Text = "January"
        'ComboTransaction()
    End Sub
    Friend Class IntervalItem
        Public Property Text As String
        Public Property Value As Integer
        Public Property Sum As String
        Public Property SubSum As String
    End Class

    Private Sub Combo()
        Dim intervals As New List(Of IntervalItem)

        'intervals.Add(New IntervalItem With {.Text = "", .Value = 0})
        intervals.Add(New IntervalItem With {.Text = "January", .Value = 1})
        intervals.Add(New IntervalItem With {.Text = "February", .Value = 2})
        intervals.Add(New IntervalItem With {.Text = "March", .Value = 3})
        intervals.Add(New IntervalItem With {.Text = "April", .Value = 4})
        intervals.Add(New IntervalItem With {.Text = "May", .Value = 5})
        intervals.Add(New IntervalItem With {.Text = "June", .Value = 6})
        intervals.Add(New IntervalItem With {.Text = "July", .Value = 7})
        intervals.Add(New IntervalItem With {.Text = "August", .Value = 8})
        intervals.Add(New IntervalItem With {.Text = "September", .Value = 9})
        intervals.Add(New IntervalItem With {.Text = "October", .Value = 10})
        intervals.Add(New IntervalItem With {.Text = "November", .Value = 11})
        intervals.Add(New IntervalItem With {.Text = "December", .Value = 12})

        With Me.cbodate
            .DisplayMember = "Text"
            .ValueMember = "Value"
            .DataSource = intervals
        End With
    End Sub
    'Private Sub ComboTransaction()
    '    Dim intervals As New List(Of IntervalItem)

    '    intervals.Add(New IntervalItem With {.Text = "", .Value = 0})
    '    intervals.Add(New IntervalItem With {.Text = "All", .Sum = ""})
    '    intervals.Add(New IntervalItem With {.Text = "Receipts", .Sum = "OR"})
    '    intervals.Add(New IntervalItem With {.Text = "Disbursements", .Sum = "CV"})
    '    intervals.Add(New IntervalItem With {.Text = "Adjustments", .Sum = "JV"})

    '    With Me.cbotransaction
    '        .DisplayMember = "Text"
    '        .ValueMember = "Sum"
    '        .DataSource = intervals
    '    End With
    'End Sub

    Private Sub cbodate_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbodate.SelectedIndexChanged, cbodate.SelectionChangeCommitted
        MonthValue = cbodate.SelectedValue.ToString()
    End Sub

    Private Sub RBMonthly_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RBMonthly.CheckedChanged
        If RBMonthly.Checked = True Then
            RBDaily.Checked = False
            dtpListing.Visible = False
            cbodate.Visible = True
            dtpYear.Visible = True
            cbotransaction.Enabled = True
            'cbotransaction.Text = ""
            dtpListing.Value = Now
        End If
    End Sub

    Private Sub cbotransaction_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbotransaction.SelectedIndexChanged, cbotransaction.SelectionChangeCommitted
        TransactionValue = cbotransaction.Text
    End Sub

    Private Sub PreviousToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousToolStripMenuItem.Click
        picLoading.Visible = True
        AuditSave()
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub AuditSave()
        If RBDaily.Checked = True Then
            AuditTrail_Save("REPORTS", "View Journal Listings > Daily (" & cbotransaction.Text & ") > " & dtpListing.Text)
        Else
            AuditTrail_Save("REPORTS", "View Journal Listings > Monthly (" & cbotransaction.Text & ") > " & cbodate.Text & " " & dtpYear.Text)
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

End Class