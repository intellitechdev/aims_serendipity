Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmBudgetMonitoring
    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub frmBudgetMonitoring_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_accounts()
        Me.WindowState = FormWindowState.Maximized
        cboAccounts.SelectedIndex = 85
        cboYear.Text = DatePart(DateInterval.Year, Now())
    End Sub

#Region "Events"
    Private Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Mark_ParkedAccounts()
        Preview_Report()

    End Sub
    Private Sub btnSelectParkedBills_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectParkedBills.Click
        cleanDatabase()
        If IsAccountSelected() = True Then

            DisplayReport_toGrid()
            ColorRows()
            crvReport.Visible = False
        Else
            MsgBox("Please Select an Account First.", MsgBoxStyle.Exclamation, "Select Accounts")
        End If

    End Sub
    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        lblBudget.Text = "Budget for the Year " & cboYear.SelectedItem
    End Sub
    Private Sub cboAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccounts.SelectedIndexChanged
        Display_BudgetAmount()
    End Sub
    Private Sub dgvBudgetMonitoring_Sorted(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvBudgetMonitoring.Sorted
        ColorRows()
    End Sub
    Private Sub frmBudgetMonitoring_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        cleanDatabase()
        rptsummary.Close()
    End Sub
#End Region
#Region "Functions/SubRoutines"

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub Preview_Report()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\BudgetMonitoringReport.rpt")
        rptsummary.Refresh()
        Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))

        With rptsummary
            .SetParameterValue("@acnt_desc", cboAccounts.SelectedItem.ToString)
            .SetParameterValue("@asOfDate", dteAsof.Value)
            .SetParameterValue("@BudgetYear", cboYear.SelectedItem)
            .SetParameterValue("@selectedParkedBills", getSelectedParkedBills())
            .SetParameterValue("@fxKeyCompany", gCompanyID())
            .SetParameterValue("@company", gCompanyName)
        End With

        crvReport.Visible = True
        Me.crvReport.Visible = True
        Me.crvReport.ReportSource = rptsummary

    End Sub

    Private Sub DisplayReport_toGrid()

        Dim selectBills As New DataGridViewCheckBoxColumn
        Dim transactionDate As New DataGridViewTextBoxColumn
        Dim referenceNo As New DataGridViewTextBoxColumn
        Dim payee As New DataGridViewTextBoxColumn
        Dim particulars As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        Dim paidunpaid As New DataGridViewTextBoxColumn
        'Dim status As New DataGridViewTextBoxColumn

        dgvBudgetMonitoring.DataSource = Load_datasource().Tables(0).DefaultView
        dgvBudgetMonitoring.Columns.Clear()

        selectBills.DataPropertyName = "SELECT BILLS"
        selectBills.Name = "selectbills"
        selectBills.HeaderText = ""

        transactionDate.DataPropertyName = "TRANSACTION DATE"
        transactionDate.Name = "transactiondate"
        transactionDate.HeaderText = "Date"

        referenceNo.DataPropertyName = "APV/CV#"
        referenceNo.Name = "apvcv"
        referenceNo.HeaderText = "APV/CV#"

        payee.DataPropertyName = "PAYEE"
        payee.Name = "payee"
        payee.HeaderText = "Payee"

        particulars.DataPropertyName = "PARTICULARS"
        particulars.Name = "particulars"
        particulars.HeaderText = "Particulars"

        amount.DataPropertyName = "AMOUNT"
        amount.Name = "amount"
        amount.HeaderText = "Amount"

        paidunpaid.DataPropertyName = "PAID/UNPAID"
        paidunpaid.Name = "paidunpaid"
        paidunpaid.HeaderText = "Paid/Unpaid"

        'status.DataPropertyName = "STATUS"
        'status.Name = "status"
        'status.HeaderText = "Status"

        With dgvBudgetMonitoring.Columns
            .Add(selectBills)
            .Add(transactionDate)
            .Add(referenceNo)
            .Add(payee)
            .Add(particulars)
            .Add(amount)
            .Add(paidunpaid)
            '.Add(status)
        End With

        With dgvBudgetMonitoring
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .DefaultCellStyle.WrapMode = DataGridViewTriState.True
            .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            .AutoResizeRows()
        End With

        selectBills.Width = 20
        transactionDate.Width = 80
        particulars.Width = 360

        referenceNo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        payee.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        amount.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        paidunpaid.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

        transactionDate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        amount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        paidunpaid.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        transactionDate.DefaultCellStyle.Format = "MM/dd/yyyy"
        amount.DefaultCellStyle.Format = "##,##.00"

    End Sub
    Private Sub DisablePaidRows()
        Dim index As Integer = 0
        For index = 0 To dgvBudgetMonitoring.Rows.Count - 1
            If dgvBudgetMonitoring.Item("status", index).Value = "PAID" Then
                dgvBudgetMonitoring.Rows(index).ReadOnly = True
            End If
        Next
    End Sub
    Private Sub ColorRows()
        Dim index As Integer = 0
        For index = 0 To dgvBudgetMonitoring.Rows.Count - 1
            If dgvBudgetMonitoring.Item("paidunpaid", index).Value = "PAID" Then
                dgvBudgetMonitoring.Rows(index).DefaultCellStyle.BackColor = Color.LightBlue
            Else
                dgvBudgetMonitoring.Rows(index).DefaultCellStyle.BackColor = Color.LightPink
            End If
        Next
    End Sub
    Private Sub Display_BudgetAmount()
        Dim textCommand As String = "SELECT fdAcnt_budget FROM dbo.mAccountBudget "
        textCommand &= "WHERE acnt_id LIKE '" & m_GetAccountID(cboAccounts.SelectedItem.ToString) & "'"

        Try
            Dim BudgetAmount As Decimal = 0.0
            BudgetAmount = CDec(SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, textCommand))
            txtBudgetAmount.Text = Format(BudgetAmount, "##,##0.00")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try
    End Sub
    Private Sub Load_accounts()
        m_DisplayAccountsAll(cboAccounts)
    End Sub
    Private Sub Mark_ParkedAccounts()
        Dim sSQLcmd As String = "usp_t_markBills "
        sSQLcmd &= "@selectedParkedBills = '" & getSelectedParkedBills() & "'"

        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLcmd)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try
    End Sub
    Private Sub cleanDatabase()
        Dim ssQLcmd As String = "UPDATE tBills SET fbParked = 0"

        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, ssQLcmd)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try
    End Sub
    Private Function IsAccountSelected() As Boolean
        Const NoAccount = 0
        If cboAccounts.SelectedIndex <> NoAccount Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function Load_datasource() As DataSet
        Dim datasource As DataSet = New DataSet
        Dim commandStatement As String = "usp_rpt_BudgetMonitoringReport "
        commandStatement &= "@acnt_desc = '" & cboAccounts.SelectedItem.ToString & "'"
        commandStatement &= ",@asOfDate = '" & dteAsof.Value & "'"
        commandStatement &= ",@BudgetYear = '" & cboYear.SelectedItem & "'"
        commandStatement &= ",@selectedParkedBills = '" & getSelectedParkedBills() & "'"
        commandStatement &= ",@fxKeyCompany = '" & gCompanyID() & "'"

        Try
            datasource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, commandStatement)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try

        Return datasource
    End Function
    Private Function getSelectedParkedBills() As String
        Dim comma_delimted_APVNo As String = ""
        Dim index As Integer = 0
        Dim NoOfRows As Integer = dgvBudgetMonitoring.Rows.Count
        Dim Last_Row As Integer = NoOfRows

        For index = 0 To NoOfRows - 1
            If dgvBudgetMonitoring.Item("selectbills", index).Value = True Then
                If dgvBudgetMonitoring.Item("apvcv", index).Value.ToString.Length <> 17 Then
                    comma_delimted_APVNo &= dgvBudgetMonitoring.Item("apvcv", index).Value.ToString.Substring(0, 17) & ","
                Else
                    comma_delimted_APVNo &= dgvBudgetMonitoring.Item("apvcv", index).Value.ToString & ","
                End If

            End If
        Next

        Return comma_delimted_APVNo
    End Function

#End Region

End Class