﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountBalance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cboAcntType = New System.Windows.Forms.ComboBox()
        Me.lblTransaction = New System.Windows.Forms.Label()
        Me.dtpTrialBalance = New System.Windows.Forms.DateTimePicker()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.bgwProcessReport = New System.ComponentModel.BackgroundWorker()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.CachedPageNumberPerDoc = 10
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.DisplayStatusBar = False
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.EnableDrillDown = False
        Me.crvRpt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.crvRpt.Location = New System.Drawing.Point(0, 45)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.SelectionFormula = ""
        Me.crvRpt.ShowCloseButton = False
        Me.crvRpt.ShowCopyButton = False
        Me.crvRpt.ShowGotoPageButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.ShowTextSearchButton = False
        Me.crvRpt.Size = New System.Drawing.Size(954, 693)
        Me.crvRpt.TabIndex = 26
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvRpt.ViewTimeSelectionFormula = ""
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CloseToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(70, 41)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(266, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 15)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Account Title"
        '
        'PreviewToolStripMenuItem
        '
        Me.PreviewToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.PreviewToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PreviewToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.PreviewToolStripMenuItem.Name = "PreviewToolStripMenuItem"
        Me.PreviewToolStripMenuItem.Size = New System.Drawing.Size(84, 41)
        Me.PreviewToolStripMenuItem.Text = "Preview"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.Panel1.Controls.Add(Me.cboAcntType)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lblTransaction)
        Me.Panel1.Controls.Add(Me.dtpTrialBalance)
        Me.Panel1.Controls.Add(Me.MenuStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(954, 45)
        Me.Panel1.TabIndex = 25
        '
        'cboAcntType
        '
        Me.cboAcntType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAcntType.FormattingEnabled = True
        Me.cboAcntType.Location = New System.Drawing.Point(370, 8)
        Me.cboAcntType.Name = "cboAcntType"
        Me.cboAcntType.Size = New System.Drawing.Size(322, 23)
        Me.cboAcntType.TabIndex = 83
        '
        'lblTransaction
        '
        Me.lblTransaction.AutoSize = True
        Me.lblTransaction.BackColor = System.Drawing.Color.Transparent
        Me.lblTransaction.Location = New System.Drawing.Point(10, 16)
        Me.lblTransaction.Name = "lblTransaction"
        Me.lblTransaction.Size = New System.Drawing.Size(35, 15)
        Me.lblTransaction.TabIndex = 14
        Me.lblTransaction.Text = "Date"
        '
        'dtpTrialBalance
        '
        Me.dtpTrialBalance.CustomFormat = "MMMM dd, yyyy"
        Me.dtpTrialBalance.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTrialBalance.Location = New System.Drawing.Point(52, 12)
        Me.dtpTrialBalance.Name = "dtpTrialBalance"
        Me.dtpTrialBalance.Size = New System.Drawing.Size(176, 23)
        Me.dtpTrialBalance.TabIndex = 8
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseToolStripMenuItem, Me.PreviewToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip1.Size = New System.Drawing.Size(954, 45)
        Me.MenuStrip1.TabIndex = 21
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'bgwProcessReport
        '
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(405, 320)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(145, 110)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 27
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'frmAccountBalance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(954, 738)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmAccountBalance"
        Me.Text = "Account Balance"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PreviewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTransaction As System.Windows.Forms.Label
    Friend WithEvents dtpTrialBalance As System.Windows.Forms.DateTimePicker
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents bgwProcessReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents cboAcntType As System.Windows.Forms.ComboBox
End Class
