﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmConsolidatedReceipts
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Dim c1 As String
    Dim c2 As String
    Dim c3 As String

    Private Sub loadcolumns()
        c1 = ""
        c2 = ""
        c3 = ""

        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 1))

        If rd.Read = True Then
            c1 = rd.Item(1).ToString
        End If

        Dim rd2 As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 2))
        If rd2.Read = True Then
            c2 = rd2.Item(1).ToString
        End If

        Dim rd3 As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "spu_CRJDynamicColumn_ListByID",
                                                                            New SqlParameter("@FxColumn", 3))
        If rd3.Read = True Then
            c3 = rd3.Item(1).ToString
        End If
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub LoadReport()
        If RBDaily.Checked = True Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\CollectionReportDailyConsolidated.rpt")
            'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text)
            rptsummary.SetParameterValue("@FcColumn1", c1)
            rptsummary.SetParameterValue("@FcColumn2", c2)
            rptsummary.SetParameterValue("@FcColumn3", c3)
            rptsummary.SetParameterValue("@FcCashier", cboUser.Text)

            rptsummary.SetParameterValue("@coid", gCompanyID(), "SubConsolidatedDaily")
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "SubConsolidatedDaily")
            rptsummary.SetParameterValue("@FcCashier", cboUser.Text, "SubConsolidatedDaily")

            'rptsummary.SetParameterValue("@coid", gCompanyID(), "Subreport_CancelledEntry.rpt")
            'rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "Subreport_CancelledEntry.rpt")
            'rptsummary.SetParameterValue("@Doctype", "Receipts", "Subreport_CancelledEntry.rpt")

            'rptsummary.SetParameterValue("@fdDate", dtpListing.Text, "DCR_CheckDetails")
        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\CollectionReportMonthly.rpt")
            'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text)
            rptsummary.SetParameterValue("@FcColumn1", c1)
            rptsummary.SetParameterValue("@FcColumn2", c2)
            rptsummary.SetParameterValue("@FcColumn3", c3)
            rptsummary.SetParameterValue("@FcCashier", cboUser.Text)

            rptsummary.SetParameterValue("@coid", gCompanyID(), "PerCashierDDRCR")
            rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "PerCashierDDRCR")
            rptsummary.SetParameterValue("@FcCashier", cboUser.Text, "PerCashierDDRCR")
        End If
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        loadcolumns()
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvCashCollectionReport.Visible = True
        Me.crvCashCollectionReport.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub LoadDoctype()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_Users_Select", con.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "User")
            With cboUser
                .ValueMember = "fxKeyUser"
                .DisplayMember = "fcFullName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            con.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmConsolidatedReceipts_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        LoadDoctype()
    End Sub
End Class