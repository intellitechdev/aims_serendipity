﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrintBatchCheck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.grdCheckDetails = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpCheckDate = New System.Windows.Forms.DateTimePicker()
        Me.txtCheckNumber = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCHVNumber = New System.Windows.Forms.TextBox()
        Me.cboBankName = New System.Windows.Forms.ComboBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnAddCheck = New System.Windows.Forms.Button()
        Me.btnGetDetails = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNumberOfChecks = New System.Windows.Forms.TextBox()
        Me.btnDeleteAll = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnCheckVoucher = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnCancelled = New System.Windows.Forms.Button()
        Me.Done = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.memName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bankName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CheckNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.checkDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cancelled = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bankID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdCheckDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.grdCheckDetails)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(835, 191)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'grdCheckDetails
        '
        Me.grdCheckDetails.AllowUserToAddRows = False
        Me.grdCheckDetails.AllowUserToDeleteRows = False
        Me.grdCheckDetails.AllowUserToResizeColumns = False
        Me.grdCheckDetails.AllowUserToResizeRows = False
        Me.grdCheckDetails.BackgroundColor = System.Drawing.Color.White
        Me.grdCheckDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdCheckDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdCheckDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdCheckDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCheckDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Done, Me.memName, Me.Amount, Me.bankName, Me.CheckNumber, Me.checkDate, Me.cancelled, Me.bankID})
        Me.grdCheckDetails.Location = New System.Drawing.Point(6, 10)
        Me.grdCheckDetails.Name = "grdCheckDetails"
        Me.grdCheckDetails.RowHeadersVisible = False
        Me.grdCheckDetails.Size = New System.Drawing.Size(823, 175)
        Me.grdCheckDetails.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(613, 221)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Check Date:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 255)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Check Number"
        '
        'dtpCheckDate
        '
        Me.dtpCheckDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpCheckDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCheckDate.Location = New System.Drawing.Point(686, 217)
        Me.dtpCheckDate.Name = "dtpCheckDate"
        Me.dtpCheckDate.Size = New System.Drawing.Size(155, 20)
        Me.dtpCheckDate.TabIndex = 5
        '
        'txtCheckNumber
        '
        Me.txtCheckNumber.Location = New System.Drawing.Point(99, 251)
        Me.txtCheckNumber.Name = "txtCheckNumber"
        Me.txtCheckNumber.Size = New System.Drawing.Size(116, 20)
        Me.txtCheckNumber.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(316, 220)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Bank Name:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 220)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "CHV No:"
        '
        'txtCHVNumber
        '
        Me.txtCHVNumber.Location = New System.Drawing.Point(71, 216)
        Me.txtCHVNumber.Name = "txtCHVNumber"
        Me.txtCHVNumber.ReadOnly = True
        Me.txtCHVNumber.Size = New System.Drawing.Size(115, 20)
        Me.txtCHVNumber.TabIndex = 3
        '
        'cboBankName
        '
        Me.cboBankName.FormattingEnabled = True
        Me.cboBankName.Location = New System.Drawing.Point(389, 216)
        Me.cboBankName.Name = "cboBankName"
        Me.cboBankName.Size = New System.Drawing.Size(215, 21)
        Me.cboBankName.TabIndex = 3
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(442, 284)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnAddCheck
        '
        Me.btnAddCheck.Location = New System.Drawing.Point(361, 284)
        Me.btnAddCheck.Name = "btnAddCheck"
        Me.btnAddCheck.Size = New System.Drawing.Size(75, 23)
        Me.btnAddCheck.TabIndex = 1
        Me.btnAddCheck.Text = "Add Check"
        Me.btnAddCheck.UseVisualStyleBackColor = True
        '
        'btnGetDetails
        '
        Me.btnGetDetails.Location = New System.Drawing.Point(223, 215)
        Me.btnGetDetails.Name = "btnGetDetails"
        Me.btnGetDetails.Size = New System.Drawing.Size(81, 23)
        Me.btnGetDetails.TabIndex = 60
        Me.btnGetDetails.Text = "Get Details"
        Me.btnGetDetails.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(766, 284)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 61
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(221, 255)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 13)
        Me.Label5.TabIndex = 63
        Me.Label5.Text = "No. of Checks:"
        '
        'txtNumberOfChecks
        '
        Me.txtNumberOfChecks.Location = New System.Drawing.Point(318, 251)
        Me.txtNumberOfChecks.Name = "txtNumberOfChecks"
        Me.txtNumberOfChecks.Size = New System.Drawing.Size(50, 20)
        Me.txtNumberOfChecks.TabIndex = 62
        '
        'btnDeleteAll
        '
        Me.btnDeleteAll.Location = New System.Drawing.Point(523, 284)
        Me.btnDeleteAll.Name = "btnDeleteAll"
        Me.btnDeleteAll.Size = New System.Drawing.Size(75, 23)
        Me.btnDeleteAll.TabIndex = 64
        Me.btnDeleteAll.Text = "Delete All"
        Me.btnDeleteAll.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(604, 284)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 65
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCheckVoucher
        '
        Me.btnCheckVoucher.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnCheckVoucher.Location = New System.Drawing.Point(192, 214)
        Me.btnCheckVoucher.Name = "btnCheckVoucher"
        Me.btnCheckVoucher.Size = New System.Drawing.Size(25, 25)
        Me.btnCheckVoucher.TabIndex = 59
        Me.btnCheckVoucher.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "trasKey"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn2.HeaderText = "Document Number"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn4.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Check No."
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'btnCancelled
        '
        Me.btnCancelled.Location = New System.Drawing.Point(685, 284)
        Me.btnCancelled.Name = "btnCancelled"
        Me.btnCancelled.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelled.TabIndex = 66
        Me.btnCancelled.Text = "Cancelled"
        Me.btnCancelled.UseVisualStyleBackColor = True
        '
        'Done
        '
        Me.Done.HeaderText = "Done"
        Me.Done.Name = "Done"
        Me.Done.ReadOnly = True
        Me.Done.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Done.Width = 50
        '
        'memName
        '
        Me.memName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.memName.HeaderText = "Name"
        Me.memName.Name = "memName"
        Me.memName.ReadOnly = True
        Me.memName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Amount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Amount.DefaultCellStyle = DataGridViewCellStyle2
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        Me.Amount.ReadOnly = True
        Me.Amount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Amount.Width = 140
        '
        'bankName
        '
        Me.bankName.HeaderText = "Bank"
        Me.bankName.Name = "bankName"
        Me.bankName.ReadOnly = True
        Me.bankName.Width = 180
        '
        'CheckNumber
        '
        Me.CheckNumber.HeaderText = "Check No."
        Me.CheckNumber.Name = "CheckNumber"
        Me.CheckNumber.ReadOnly = True
        Me.CheckNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'checkDate
        '
        Me.checkDate.HeaderText = "Date"
        Me.checkDate.Name = "checkDate"
        Me.checkDate.ReadOnly = True
        '
        'cancelled
        '
        Me.cancelled.HeaderText = "Cancelled"
        Me.cancelled.Name = "cancelled"
        Me.cancelled.Width = 70
        '
        'bankID
        '
        Me.bankID.HeaderText = "bankID"
        Me.bankID.Name = "bankID"
        Me.bankID.Visible = False
        '
        'frmPrintBatchCheck
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(858, 319)
        Me.Controls.Add(Me.btnCancelled)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnDeleteAll)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtNumberOfChecks)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnGetDetails)
        Me.Controls.Add(Me.btnCheckVoucher)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnAddCheck)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboBankName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCheckNumber)
        Me.Controls.Add(Me.txtCHVNumber)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dtpCheckDate)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmPrintBatchCheck"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.grdCheckDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents grdCheckDetails As System.Windows.Forms.DataGridView
    Friend WithEvents btnAddCheck As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboBankName As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpCheckDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCHVNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCheckNumber As System.Windows.Forms.TextBox
    Friend WithEvents btnCheckVoucher As System.Windows.Forms.Button
    Friend WithEvents btnGetDetails As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNumberOfChecks As System.Windows.Forms.TextBox
    Friend WithEvents btnDeleteAll As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancelled As System.Windows.Forms.Button
    Friend WithEvents Done As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents memName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bankName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CheckNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents checkDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cancelled As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents bankID As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
