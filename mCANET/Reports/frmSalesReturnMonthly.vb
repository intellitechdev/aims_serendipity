﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Public Class frmSalesReturnMonthly
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim MonthValue As Integer
    Private Sub frmSalesReturnMonthly_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptsummary.Close()
        Combo()
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub LoadReport()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\SalesReturnMonthly.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@coid", gCompanyID())
        rptsummary.SetParameterValue("@fdTransDate", MonthValue)
        rptsummary.SetParameterValue("@Year", dtpYear.Text)
        rptsummary.SetParameterValue("@coid", gCompanyID(), "SubReportSalesReturnMonthly")
        rptsummary.SetParameterValue("@fdTransDate", MonthValue, "SubReportSalesReturnMonthly")
        rptsummary.SetParameterValue("@Year", dtpYear.Text, "SubReportSalesReturnMonthly")
    End Sub
    Friend Class IntervalItem
        Public Property Text As String
        Public Property Value As Integer
    End Class

    Private Sub Combo()
        Dim intervals As New List(Of IntervalItem)

        intervals.Add(New IntervalItem With {.Text = "", .Value = 0})
        intervals.Add(New IntervalItem With {.Text = "January", .Value = 1})
        intervals.Add(New IntervalItem With {.Text = "February", .Value = 2})
        intervals.Add(New IntervalItem With {.Text = "March", .Value = 3})
        intervals.Add(New IntervalItem With {.Text = "April", .Value = 4})
        intervals.Add(New IntervalItem With {.Text = "May", .Value = 5})
        intervals.Add(New IntervalItem With {.Text = "June", .Value = 6})
        intervals.Add(New IntervalItem With {.Text = "July", .Value = 7})
        intervals.Add(New IntervalItem With {.Text = "August", .Value = 8})
        intervals.Add(New IntervalItem With {.Text = "September", .Value = 9})
        intervals.Add(New IntervalItem With {.Text = "October", .Value = 10})
        intervals.Add(New IntervalItem With {.Text = "November", .Value = 11})
        intervals.Add(New IntervalItem With {.Text = "December", .Value = 12})

        With Me.cbodate
            .DisplayMember = "Text"
            .ValueMember = "Value"
            .DataSource = intervals
        End With
    End Sub

    Private Sub cbodate_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cbodate.MouseWheel
        Dim disable As HandledMouseEventArgs = e
        disable.Handled = True
    End Sub

    Private Sub cbodate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbodate.SelectedIndexChanged
        MonthValue = cbodate.SelectedValue.ToString()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If cbodate.Text = "" Then
            MsgBox("No Month Selected!", vbInformation, "Sales Return - Monthly")
        Else
            picLoading.Visible = True
            If bgwProcessReport.IsBusy = False Then
                bgwProcessReport.RunWorkerAsync()
            End If
        End If
    End Sub


End Class