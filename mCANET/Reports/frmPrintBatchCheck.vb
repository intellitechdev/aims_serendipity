﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmPrintBatchCheck
    Private gCon As New Clsappconfiguration()
    Dim TransKey As String
    Dim BlankCheckRow As Integer

#Region "Data Layer"
    Private Sub BankList()
        Dim da As New SqlDataAdapter("Select PK_BankID, fcBankName  from dbo.mBankList left join mAccounts on mAccounts.acnt_id = mBankList.FK_AcntId where FK_CoId='" & gCompanyID() & "'", gCon.cnstring)
        Dim ds As New DataSet
        da.Fill(ds, "Bank")

        With cboBankName
            .DataSource = ds.Tables("Bank")
            .DisplayMember = "fcBankName"
            .ValueMember = "PK_BankID"
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub AddDetailItem(ByVal Done As Boolean, ByVal memName As String,
                                  ByVal amount As Decimal, ByVal BankName As String, ByVal CheckNumber As String,
                                  ByVal CheckDate As String, ByVal Cancelled As Boolean, ByVal TransKey As String)

        Try
            Dim dt As Date = Convert.ToDateTime(CheckDate)
            Dim row As String() =
             {Done, memName, Format(CDec(amount), "##,##0.00"), BankName, CheckNumber, dt.ToString("MMMM") & " " & dt.Day & ", " & dt.Year, Cancelled, TransKey}

            Dim nRowIndex As Integer
            With grdCheckDetails

                .Rows.Add(row)
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            Dim row As String() =
             {Done, memName, Format(CDec(amount), "##,##0.00"), BankName, CheckNumber, CheckDate, Cancelled, TransKey}

            Dim nRowIndex As Integer
            With grdCheckDetails

                .Rows.Add(row)
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        End Try
    End Sub

    Private Sub LoadDetails()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Distribution_GetDetails",
                                                                        New SqlParameter("@fk_tJVEntry", TransKey))
        While rd.Read
            AddDetailItem(rd.Item(0), rd.Item(1).ToString, rd.Item(2),
                                      rd.Item(3).ToString, rd.Item(4).ToString,
                                      rd.Item(5).ToString, rd.Item(6), rd.Item(7).ToString)

        End While
    End Sub

    Private Sub AddCheckToGrid()
        Try
            GetBlankCheckRow()
            Dim NumberOfLoops As Integer = CInt(txtNumberOfChecks.Text)
            Dim CheckNumber As Integer = CInt(txtCheckNumber.Text)
            While NumberOfLoops > 0
                Dim rd As SqlDataReader
                rd = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "Check_Search", _
                                           New SqlParameter("@FX_CheckNumber", txtCheckNumber.Text),
                                           New SqlParameter("@FK_BankId", cboBankName.SelectedValue))
                If rd.Read = True Then
                    MsgBox("Existing Check Number: " & CheckNumber, MsgBoxStyle.Exclamation, "Check Printing")
                    rd.Close()
                    Exit While
                Else
                    If grdCheckDetails.Item("CheckNumber", BlankCheckRow).Value.ToString = "" Then
                        grdCheckDetails.Item("CheckNumber", BlankCheckRow).Value = CheckNumber
                        grdCheckDetails.Item("bankName", BlankCheckRow).Value = cboBankName.Text
                        grdCheckDetails.Item("bankID", BlankCheckRow).Value = cboBankName.SelectedValue
                        grdCheckDetails.Item("checkDate", BlankCheckRow).Value = dtpCheckDate.Value.ToString("MMMM") & " " & dtpCheckDate.Value.Day & ", " & dtpCheckDate.Value.Year
                        grdCheckDetails.Rows(BlankCheckRow).Cells("checkDate").Style.Format = Format("MMMM dd, yyyy")
                        NumberOfLoops = NumberOfLoops - 1
                        CheckNumber = CheckNumber + 1
                        BlankCheckRow = BlankCheckRow + 1
                        rd.Close()
                    Else
                        Exit While
                    End If
                End If
            End While
        Catch ex As Exception

        End Try

    End Sub

    Private Sub GetBlankCheckRow()
        For xRow As Integer = 0 To grdCheckDetails.RowCount - 1
            Try
                If grdCheckDetails.Item("CheckNumber", xRow).Value = "" Then
                    BlankCheckRow = xRow
                    Exit For
                End If
            Catch ex As Exception

            End Try
        Next
    End Sub

#End Region

#Region "Event Handler"
    Private Sub btnCheckVoucher_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckVoucher.Click
        Try
            frmJVEntry.xMode = "CHVUsed"
            frmJVEntry.ShowDialog()
            If frmJVEntry.DialogResult = Windows.Forms.DialogResult.OK Then
                txtCHVNumber.Text = frmJVEntry.grdList.SelectedRows(0).Cells(1).Value.ToString()
                TransKey = frmJVEntry.grdList.SelectedRows(0).Cells(0).Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnGetDetails_Click(sender As System.Object, e As System.EventArgs) Handles btnGetDetails.Click
        grdCheckDetails.Rows.Clear()
        LoadDetails()
    End Sub

    Private Sub btnAddCheck_Click(sender As System.Object, e As System.EventArgs) Handles btnAddCheck.Click
        AddCheckToGrid()
    End Sub

    Private Sub frmPrintBatchCheck_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        BankList()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub txtCheckNumber_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCheckNumber.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar)))) Then e.Handled = True
        End If
    End Sub
#End Region

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        AddCheckNum(grdCheckDetails.Item("CheckNumber", grdCheckDetails.CurrentRow.Index).Value.ToString,
                    grdCheckDetails.Item("bankID", grdCheckDetails.CurrentRow.Index).Value.ToString,
                    grdCheckDetails.Item("memName", grdCheckDetails.CurrentRow.Index).Value.ToString,
                    grdCheckDetails.Item("Amount", grdCheckDetails.CurrentRow.Index).Value,
                    grdCheckDetails.Item("CheckDate", grdCheckDetails.CurrentRow.Index).Value,
                    grdCheckDetails.Item("cancelled", grdCheckDetails.CurrentRow.Index).Value)

        frmCheckReport.txtcheckNo.Text = grdCheckDetails.Item("CheckNumber", grdCheckDetails.CurrentRow.Index).Value.ToString
        frmCheckReport.xTransID = TransKey
        frmCheckReport.ShowDialog()

        grdCheckDetails.Item("Done", grdCheckDetails.CurrentRow.Index).Value = True
    End Sub

    Private Sub AddCheckNum(ByVal CheckNum As String, ByVal bankID As String, ByVal memName As String, ByVal Amount As Decimal, ByVal CheckDate As Date, ByVal Cancelled As Boolean)
        Dim bID As New Guid(bankID)
        SqlHelper.ExecuteNonQuery(gCon.sqlconn, CommandType.StoredProcedure, "spu_Check_InsertUpdate", _
                                   New SqlParameter("@FX_CheckNumber", CheckNum),
                                   New SqlParameter("@FK_BankId", bID),
                                   New SqlParameter("@FK_TransId", TransKey),
                                   New SqlParameter("@fbIsUsed", 1),
                                   New SqlParameter("@fcPayee", memName),
                                   New SqlParameter("@fnAmount", Amount),
                                   New SqlParameter("@fbIsCancelled", Cancelled),
                                   New SqlParameter("@fdCheckDate", CheckDate),
                                   New SqlParameter("@fcMaker", gCompanyName))

    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CheckNumber_Delete1", _
                    New SqlParameter("@FX_CheckNumber", grdCheckDetails.Item("CheckNumber", grdCheckDetails.CurrentRow.Index).Value.ToString),
                    New SqlParameter("@transID", TransKey))

        grdCheckDetails.Item("CheckNumber", grdCheckDetails.CurrentRow.Index).Value = ""
        grdCheckDetails.Item("bankName", grdCheckDetails.CurrentRow.Index).Value = ""
        grdCheckDetails.Item("CheckDate", grdCheckDetails.CurrentRow.Index).Value = ""
        grdCheckDetails.Item("Done", grdCheckDetails.CurrentRow.Index).Value = False
        grdCheckDetails.Item("cancelled", grdCheckDetails.CurrentRow.Index).Value = False
        grdCheckDetails.Item("bankID", grdCheckDetails.CurrentRow.Index).Value = ""

    End Sub

    Private Sub btnDeleteAll_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteAll.Click
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_Check_DeletePerTransaction", _
                    New SqlParameter("@FK_TransId", TransKey))

        grdCheckDetails.Rows.Clear()
        LoadDetails()
    End Sub

    Private Sub btnCancelled_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelled.Click
        If MsgBox("Update check status?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Check Printing") = MsgBoxResult.Yes Then
            Dim sSQLCmd As String = "UPDATE dbo.mCheckNumberMaster SET fbIsCancelled = '" & grdCheckDetails.Item("cancelled", grdCheckDetails.CurrentRow.Index).Value & "' WHERE FX_CheckNumber='" & grdCheckDetails.Item("CheckNumber", grdCheckDetails.CurrentRow.Index).Value.ToString & "' AND FK_TransId ='" & TransKey & "'"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

            MsgBox("Done!")
            If grdCheckDetails.Item("cancelled", grdCheckDetails.CurrentRow.Index).Value = True Then
                grdCheckDetails.Item("cancelled", grdCheckDetails.CurrentRow.Index).Value = False
            Else
                grdCheckDetails.Item("cancelled", grdCheckDetails.CurrentRow.Index).Value = True
            End If

        End If
    End Sub
End Class