﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmAccountBalance
    Private gCon As New Clsappconfiguration()
    Private con As New Clsappconfiguration
    Dim cs = con.cnstring
    Private rptsummary As New ReportDocument

    Public xAccount As String
    Private xTitle As String

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        Dim co_id As New Guid(gCompanyID)
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\AccountBalance.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@coid", gCompanyID())
        rptsummary.SetParameterValue("@AccountTitle", xTitle)
        rptsummary.SetParameterValue("@fdDate", dtpTrialBalance.Text)
    End Sub

    Private Function GetLastDayOfMonth(ByVal dtDate As DateTime) As DateTime
        Dim dtTo As New DateTime(dtDate.Year, dtDate.Month, 1)
        dtTo = dtTo.AddMonths(1)
        dtTo = dtTo.AddDays(-(dtTo.Day))
        Return dtTo
    End Function

    Private Function GetLastDayOfPreviousMonth(ByVal dtDate As DateTime) As DateTime
        Dim dtTo As New DateTime(dtDate.Year, dtDate.Month, 1)
        dtTo = dtTo.AddMonths(0)
        dtTo = dtTo.AddDays(-(dtTo.Day))
        Return dtTo
    End Function

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub frmBalanceSheetv2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        rptsummary.Close()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True
        AuditTrail_Save("ACCOUNT BALANCE", "Preview account balance of '" & xTitle & "' as of " & dtpTrialBalance.Value.ToString)
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub frmAccountBalance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call LoadAccountType()
        'Call LoadAccountType_v2()
        xTitle = cboAcntType.Text
    End Sub

    Private Sub LoadAccountType()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim co_id As New Guid(gCompanyID)
        Dim cmd As New SqlCommand("spu_CIMS_DebitCreditAccount_ListByType", gCon.sqlconn)
        cmd.Parameters.Add("@filter", SqlDbType.VarChar)
        cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier)
        cmd.Parameters("@filter").Value = xAccount
        cmd.Parameters("@coid").Value = co_id
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Document")
            With cboAcntType
                .ValueMember = "fxkey_AccountID"
                .DisplayMember = "fcAccountName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception
            MsgBox("ERROR")
        End Try
    End Sub
    'Private Sub LoadAccountType_v2()
    '    'Dim ds As New DataSet
    '    Dim rd As SqlDataReader
    '    Dim co_id As New Guid(gCompanyID)

    '    rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_DebitCreditAccount_ListByType", _
    '                                New SqlParameter("@filter", xAccount), _
    '                                New SqlParameter("@coid", co_id))
    '    While rd.Read
    '        cboAcntType.DisplayMember = rd.Item("fcAccountName")
    '    End While
    'End Sub

    Private Sub cboAcntType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAcntType.SelectedValueChanged
        xTitle = cboAcntType.Text
    End Sub
End Class