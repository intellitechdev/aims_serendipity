
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmCheckVoucher_Individual

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Public OriginfrmAmount As String = "Write Check"

    Private Sub frmCheckVoucher_Individual_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmCheckVoucher_Individual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call m_savechecksprinted(gPayeeID, gCheckNumber, gCheckDate, gPayorder, gCheckAmt, _
        '                                         gCheckAmtwords, gPayeesAdd, gPayeesMemo, gCheckPrintMode, _
        '                                         gBankAccount, gBankEndBalance)
        'rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\checkvoucher_individual.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        If gPayeeID = "" Then
            rptsummary.SetParameterValue("@fxKeySupplierID", gfxKeySupplier)
        Else
            rptsummary.SetParameterValue("@fxKeySupplierID", gPayeeID)
        End If
        rptsummary.SetParameterValue("@fxKeyPayBills", gItemsKeys)
        rptsummary.SetParameterValue("@checknumber", gCheckNumber)
        rptsummary.SetParameterValue("@checkdate", gCheckDate)

        rptsummary.SetParameterValue("@checkamt", gCheckAmt)

        If OriginfrmAmount <> "Write Check" Then
            rptsummary.SetParameterValue("ChkAmountinNumber", OriginfrmAmount)
        Else
            rptsummary.SetParameterValue("ChkAmountinNumber", frm_acc_writechecks.txtChkAmt.Text)
        End If

        'rptsummary.SetParameterValue("@checkamt", frm_acc_writechecks.txtChkAmt.Text)
        rptsummary.SetParameterValue("@checkwords", gCheckAmtwords)
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        '  rptsummary.SetParameterValue("@preparedby", gUserName)
        rptsummary.SetParameterValue("@preparedby", gPreparedBy) 'strSysCurrentFullName)
        rptsummary.SetParameterValue("@reviewedby", gRvwd)
        rptsummary.SetParameterValue("@approvedby", gApprvd)
        'Dim a As String = "usp_rpt_checkvoucher_individual @fxKeySupplierID = '" & gPayeeID & "', @fxKeyPayBills = '" & _
        '                gItemsKeys & "', @checknumber = '" & gCheckNumber & "', @checkdate ='" & gCheckDate & _
        '                "', @checkamt ='," & gCheckAmt & "', ChkAmountinNumber = '" & OriginfrmAmount & "', @checkwords ='" & _
        '                gCheckAmtwords & "', @fxKeyCompany ='" & gCompanyID() & "'"
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
        OriginfrmAmount = "Write Check"
    End Sub
    
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub crvRpt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles crvRpt.Load

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Call m_savechecksprinted(gPayeeID, gCheckNumber, gCheckDate, gPayorder, gCheckAmt, _
                                                 gCheckAmtwords, gPayeesAdd, gPayeesMemo, gCheckPrintMode, _
                                                 gBankAccount, gBankEndBalance)
        crvRpt.PrintReport()
    End Sub
End Class