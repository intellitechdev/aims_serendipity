﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmVoucherReport
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Public docnum As String
    Public xTransaction As String
    Public xTransID As String
    Public xLoan As String

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub frmVoucherReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        rptsummary.Close()
    End Sub

    Private Sub frmVoucherReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        rptsummary.Close()
        Select Case xTransaction
            Case "CHECK VOUCHER"
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_CashDisbursement.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
                rptsummary.SetParameterValue("@vouchername", xTransaction)
                rptsummary.SetParameterValue("@co_name", gCompanyName)
                'rptsummary.SetParameterValue("@LoanNo", xLoan, "Subreport_BIR_CDJ.rpt")
                'rptsummary.SetParameterValue("@FX_CheckNumber", xTransID, "Subreport_CheckDetails")
                'Case "CHECK VOUCHER REPRINT"
                '    rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_CashDisbursementReprint.rpt")
                '    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                '    rptsummary.Refresh()
                '    rptsummary.SetParameterValue("@DocNum", docnum)
                '    'rptsummary.SetParameterValue("@LoanNo", xLoan, "Subreport_BIR_CDJ.rpt")
                '    rptsummary.SetParameterValue("@FX_CheckNumber", xTransID, "Subreport_CheckDetails")
                'Case "CHECK VOUCHER NOT LOAN"
                '    rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_CashDisbursementVoucherNotLoan.rpt")
                '    Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                '    rptsummary.Refresh()
                '    rptsummary.SetParameterValue("@DocNum", docnum)
                '    'rptsummary.SetParameterValue("@LoanNo", xLoan, "Subreport_BIR_CDJ.rpt")
                '    'rptsummary.SetParameterValue("@FX_CheckNumber", xTransID, "Subreport_CheckDetails")
            Case "CASH VOUCHER"
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_CashVoucher.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
                rptsummary.SetParameterValue("@vouchername", xTransaction)
                rptsummary.SetParameterValue("@co_name", gCompanyName)
                rptsummary.SetParameterValue("@co_name", gCompanyName, "CV_Payee_Subreport.rpt")
                rptsummary.SetParameterValue("@DocNum", docnum, "CV_Payee_Subreport.rpt")
            Case "JOURNAL VOUCHER"
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_JournalVoucher.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
                rptsummary.SetParameterValue("@vouchername", xTransaction)
                rptsummary.SetParameterValue("@co_name", gCompanyName)
            Case "RECEIPTS"
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_Collection.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
            Case "SALES INVOICE"
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_Collection.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
                'rptsummary.SetParameterValue("@vouchername", xTransaction)
            Case "ACCOUNTS PAYABLE VOUCHER"
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomPrint_AccountPayable.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
                rptsummary.SetParameterValue("@vouchername", xTransaction)
                rptsummary.SetParameterValue("@co_name", gCompanyName)
                rptsummary.SetParameterValue("@co_name", gCompanyName, "CV_Payee_Subreport.rpt")
                rptsummary.SetParameterValue("@DocNum", docnum, "CV_Payee_Subreport.rpt")
            Case Else
                rptsummary.Load(Application.StartupPath & "\Accounting Reports\PrintVouchers.rpt")
                Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
                rptsummary.Refresh()
                rptsummary.SetParameterValue("@DocNum", docnum)
        End Select


    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub btnSignatories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSignatories.Click
        frmVoucherSignatories.ShowDialog()
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class