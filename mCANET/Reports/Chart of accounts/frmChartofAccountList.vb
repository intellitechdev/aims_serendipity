﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmChartofAccountList
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim companyID As String

    Private Sub frmChartofAccounts_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()

    End Sub

    Private Sub frmChartofAccountList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        companyID = gCompanyID()
        LoadReport()
    End Sub
    Private Sub LoadReport()
        'rptsummary.Refresh()
        Dim reportPath As String = Application.StartupPath & "\Accounting Reports\Chart of accounts\rptChartofAccounts.rpt"
        rptsummary.Load(reportPath)
        rptsummary.Refresh()
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        'rptsummary.SetParameterValue("@co_id", companyID)
        rptsummary.SetParameterValue("@compID", companyID)
        Me.crvChartofAccount.Visible = True
        Me.crvChartofAccount.ReportSource = rptsummary
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
End Class