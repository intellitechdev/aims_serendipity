Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_cust_masterCustomerNote
    Private gCOn As New Clsappconfiguration
    Private sKeyCustomer As String

    Public Property KeyCustomer() As String
        Get
            Return sKeyCustomer
        End Get
        Set(ByVal value As String)
            sKeyCustomer = value
        End Set
    End Property

    Private Sub noteEdit()
        Dim sSQLCmd As String = "UPDATE mCustomer00Master SET fcNote = '" & txtNote.Text & "'"
        sSQLCmd &= " WHERE fxKeyCustomer ='" & sKeyCustomer & "'"

        Try
            SqlHelper.ExecuteDataset(gCOn.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated!", "Edit Note")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Edit Note")
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        noteEdit()
        m_customerList(frm_cust_masterCustomer.grdCustomer1, frm_cust_masterCustomer.cboView.SelectedItem)
        frm_cust_masterCustomer.loadCustomerInfo()
        Me.Close()
    End Sub

    Private Sub btnDateStamp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDateStamp.Click
        txtNote.Text = txtNote.Text & vbCrLf & Format(Now, "MM/dd/yyyy") & ": "
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_vend_masterVendorNote_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadNotes()
    End Sub

    Private Sub loadNotes()

        Dim sSQLCmd As String = "Select fcNote FROM mCustomer00Master WHERE fxKeyCustomer ='" & sKeyCustomer & "'"

        Try

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCOn.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtNote.Text = rd.Item("fcNote")
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Notes")
        End Try

    End Sub
End Class