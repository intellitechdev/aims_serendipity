Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_cust_masterCustomerAddress
    Private gCon As New Clsappconfiguration
    Public SKeyName As String = ""
    Private sKeyAddress As String
    Public fcAddressType As String = ""
    Private sKeyCustomer As String
    Private bShippingAdd As Boolean
    'Private iCtr() As Integer

    Public Property KeyAddress() As String
        Get
            Return sKeyAddress
        End Get
        Set(ByVal value As String)
            sKeyAddress = value
        End Set
    End Property

    Public Property KeyCustomer() As String
        Get
            Return sKeyCustomer
        End Get
        Set(ByVal value As String)
            sKeyCustomer = value
        End Set
    End Property

    Public Property IsShippingAdd() As Boolean
        Get
            Return bShippingAdd
        End Get
        Set(ByVal value As Boolean)
            bShippingAdd = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_cust_masterCustomerAddress_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bShippingAdd = True Then
            m_loadShippingAddress(frm_cust_masterCustomerAddEdit.cboShipToName, frm_cust_masterCustomerAddEdit.cboShipToID, sKeyCustomer)
            frm_cust_masterCustomerAddEdit.cboShipToName.SelectedItem = txtAddressName.Text
            frm_cust_masterCustomerAddEdit.cboShipToID.SelectedIndex = frm_cust_masterCustomerAddEdit.cboShipToName.SelectedIndex
            'If Trim(frm_cust_masterCustomerAddEdit.txtShipAddress.Text) <> txtAddress.Text Then
            '    frm_cust_masterCustomerAddEdit.txtShipAddress.Text = combineAddress()
            'End If
            'Else
            '    If Trim(frm_cust_masterCustomerAddEdit.txtBillAddress.Text) <> txtAddress.Text Then
            '        frm_cust_masterCustomerAddEdit.txtBillAddress.Text = combineAddress()
            '    End If
        End If
    End Sub

    Private Sub frm_cust_masterCustomerAddress_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        settings()
        loadAddress()
    End Sub

    Private Sub settings()
        If bShippingAdd = True Then
            'txtAddressName.ReadOnly = False
            'txtAddressName.Enabled = True
            'txtAddressName.Text = "Shipping Add " & addressName()
            Me.Text = "Shipping Address"
        Else
            'txtAddressName.ReadOnly = True
            'txtAddressName.Enabled = False
            'txtAddressName.Text = ""
            Me.Text = "Billing Address"
        End If
    End Sub

    Private Function addressName() As String
        Dim sRowAddress As String
        Dim sAddressName() As String = {""}
        Dim sWord() As String = {""}
        Dim sWordTemp As String = ""
        Dim sDefault As String = "1"
        Dim i As Integer = 0

        Dim sSQLCmd As String = "SELECT fcAddressName FROM mAddress "
        sSQLCmd &= "WHERE fbCustomer = 1 AND fxKeyID = '" & sKeyCustomer & "'"
        sSQLCmd &= " AND fcAddressName LIKE 'Shipping Add %'"
        sSQLCmd &= " ORDER BY fcAddressName"
        
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sRowAddress = rd.Item("fcAddressName")
                    sAddressName = sRowAddress.Split
                    sWordTemp &= Mid(sAddressName(2), 1, 1) & " "
                    'i += 1
                End While
            End Using

            sWord = sWordTemp.Split
            If UBound(sWord) > 0 Then
                For i = 0 To UBound(sWord)
                    If CStr(i + 1) <> sWord(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sWord(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next
            End If

            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Address Name")
            Return Nothing
        End Try
    End Function

    Private Sub loadAddress()

        Dim sSQLCmd As String = "SELECT isnull(fcCity,'') as fcCity, isnull(fnPostal,'') as fnPostal, isnull(fcNote,'') as fcNote, isnull(fcAddress,'') as fcAddress FROM mAddress WHERE fxKeyAddress='" & sKeyAddress & "' "
        Try
            If sKeyAddress <> "" Then

                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                    If rd.Read Then
                        txtCity.Text = rd.Item("fcCity")
                        txtPostal.Text = rd.Item("fnPostal")
                        txtNotes.Text = rd.Item("fcNote")
                        txtAddress.Text = rd.Item("fcAddress")
                    Else
                        txtCity.Text = ""
                        txtPostal.Text = "0"
                        txtNotes.Text = ""
                        txtAddress.Text = ""
                    End If
                End Using

            Else
                txtCity.Text = ""
                txtPostal.Text = "0"
                txtNotes.Text = ""
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Address")
        End Try

    End Sub

    Private Function addressUpdate()
        Dim sSQLCmd As String = "SPU_AddressUpdate "
        sSQLCmd &= "@fxKeyAddress ='" & sKeyAddress & "'"
        sSQLCmd &= ",@fxKeyID ='" & sKeyCustomer & "'"
        sSQLCmd &= ",@fcAddress = '" & txtAddress.Text & "'"
        sSQLCmd &= ",@fcStreet ='" & txtStreet.Text & "'"
        sSQLCmd &= ",@fcCity ='" & txtCity.Text & "'"
        sSQLCmd &= ",@fnPostal =" & txtPostal.Text
        sSQLCmd &= ",@fcNote ='" & txtNotes.Text & "'"
        sSQLCmd &= ",@fbSupplier = 0 , @fbCustomer = 1 , @fbOther = 0 "
        sSQLCmd &= ",@fcAddressName = '" & txtAddressName.Text & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            Return True
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Address")
        End Try
    End Function

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If addressUpdate() Then
            MessageBox.Show("Record successfully updated", "Update Address")
            If sKeyCustomer <> "" And sKeyCustomer <> Nothing Then
                frm_cust_masterCustomerAddEdit.txtBillAddress.Text = txtAddress.Text
            End If
            'm_loadShippingAddress(frm_vend_CreatePurchaseOrder.cboShipAddName, frm_vend_CreatePurchaseOrder.cboShipAddID, sKeyCustomer, "Customer")
            m_loadShippingAddress(frm_cust_CreateInvoice.cboShipToAddName, frm_cust_CreateInvoice.cboShipToAddID, sKeyCustomer, "Customer")
            m_loadShippingAddress(frm_cust_CreateSalesOrder.cboShipToAddName, frm_cust_CreateSalesOrder.cboShipToAddID, sKeyCustomer, "Customer")
            Me.Close()
        End If
    End Sub

    Private Function combineAddress() As String
        Dim sAddress As String

        sAddress = Trim(txtStreet.Text) & " " & Trim(txtCity.Text) & vbCrLf & _
        Trim(txtPostal.Text) & vbCrLf & txtNotes.Text

        If sAddress <> txtAddress.Text Then
            sAddress = txtAddress.Text & vbCrLf & sAddress
        End If

        Return sAddress
    End Function

    Private Sub txtPostal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPostal.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

End Class