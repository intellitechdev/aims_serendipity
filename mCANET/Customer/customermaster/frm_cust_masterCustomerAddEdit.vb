Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Public Class frm_cust_masterCustomerAddEdit
    Private sKeyCustomer As String
    Private sKeyAddress As String
    Private sKeyShipAddress As String
    Private sKeyType As String
    Private sKeyTerms As String
    Private sKeyRep As String
    Private sKeySendMenthod As String
    Private sKeySalesTaxCode As String = ""
    Private sKeyTaxItem As String
    Private sKeyPriceLevel As String
    Private sKeyPaymentMethod As String
    Private sKeyJobStatus As String = ""
    Private idKeyJobStat As Guid
    Private sKeyJobType As String

    Private bNew As Boolean
    Private bUpdateForm As Boolean = False
    Private bUpdateShippingAddress As Boolean = False

    Private gCon As New Clsappconfiguration


    Public Property KeyCustomer() As String
        Get
            Return sKeyCustomer
        End Get
        Set(ByVal value As String)
            sKeyCustomer = value
        End Set
    End Property

    Public Property KeyAddress() As String
        Get
            Return sKeyAddress
        End Get
        Set(ByVal value As String)
            sKeyAddress = value
        End Set
    End Property

    Public Property IsNew() As Boolean
        Get
            Return bNew
        End Get
        Set(ByVal value As Boolean)
            bNew = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnDefineField_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefineField.Click
        frm_vend_masterVendorDefineFields.ShowDialog()
    End Sub


    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Trim(txtCustomerName.Text) = "" Then
            MessageBox.Show("Customer name cannot be empty", "Field Validation")
            Exit Sub
        End If

        'Updates Miscellaneous Information of the Customer
        update_CustomerOtherInfo()

        'Validation: Tax Code
        If cboTaxCodeID.SelectedItem <> Nothing Then
            updateCustomer()
        Else
            MsgBox("Tax Code is required. Please select a Tax Code from the Additional Info Tab.", MsgBoxStyle.Information, Me.Text)
            Exit Sub
        End If
        m_customerList(frm_cust_masterCustomer.grdCustomer1, frm_cust_masterCustomer.cboView.SelectedItem)
        m_loadAllName(frm_MF_salesRepAddEdit.cboSalesRepName, frm_MF_salesRepAddEdit.cboSalesRepID, False)
        m_loadCustomer(frm_cust_CreateInvoice.cboCustomerName, frm_cust_CreateInvoice.cboCustomerID)
        m_loadCustomer(frm_cust_CreateSalesOrder.cboCustomerName, frm_cust_CreateSalesOrder.cboCustomerID)
        m_loadCustomer(frm_item_adjQty.cboCustomerName, frm_item_adjQty.cboCustomerID)
        m_loadCustomer(frm_cust_ReceivePayment.cboCustomerName, frm_cust_ReceivePayment.cboCustomerID)

        If frm_MF_salesRepAddEdit.KeySalesRep <> Nothing Then
            frm_MF_salesRepAddEdit.loadSalesRep()
        End If


        Me.Close()
    End Sub

    Private Sub update_CustomerOtherInfo()
        Dim iRow As Integer

        For iRow = 0 To (grdDefinedField.RowCount - 2)
            If isGrdEmpty(iRow) = False Then
                Try
                    Dim sSQLCmd As String = "usp_m_otherInfoValue_update "
                    sSQLCmd &= "@fxKeyID = '" & grdDefinedField.Rows(iRow).Cells(0).Value.ToString & "'"
                    sSQLCmd &= ",@fxKeyOtherInfo = '" & grdDefinedField.Rows(iRow).Cells(1).Value.ToString & "'"
                    sSQLCmd &= ",@fxKeyInfoValue = '" & grdDefinedField.Rows(iRow).Cells(2).Value.ToString & "'"
                    sSQLCmd &= ",@fdRate = 0"
                    sSQLCmd &= ",@fdPercentage = 0"
                    sSQLCmd &= ",@fcNotes = '" & grdDefinedField.Rows(iRow).Cells(4).Value & "'"

                    SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                Catch ex As Exception
                    MessageBox.Show(Err.Description, "Update Other Info.")
                End Try

            End If
        Next
    End Sub

    Private Function isGrdEmpty(ByVal i As Integer) As Boolean
        Try
            If grdDefinedField.Rows(i).Cells(4).Value.ToString = 0 And _
                grdDefinedField.Rows(i).Cells(5).Value.ToString = 0 And _
                grdDefinedField.Rows(i).Cells(6).Value = "" Then

                Return True
            Else
                Return False
            End If
        Catch
        End Try

    End Function

    Private Sub updateCustomer()
        If sKeyAddress = "" Then
            sKeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddress.KeyAddress = sKeyAddress
            frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
            frm_cust_masterCustomerAddress.IsShippingAdd = False
            frm_cust_masterCustomerAddress.ShowDialog()
        End If

        Dim sSQLCmd As String = "usp_m_customer_update "
        sSQLCmd &= " @fxKeyCustomer = '" & sKeyCustomer & "' "
        sSQLCmd &= ", @fcCustomerName = '" & txtCustomerName.Text & "' "
        sSQLCmd &= ", @fdBalance = '" & txtOpeningBal.Text & "' "
        sSQLCmd &= ", @fdDateAsOf = '" & dteOpeningBalDate.Value & "' "
        sSQLCmd &= ", @fcCompanyName = '" & txtCoName.Text & "' "
        sSQLCmd &= ", @fcSalutation = '" & txtSalutation.Text & "' "
        sSQLCmd &= ", @fcLastName = '" & txtLastName.Text & "' "
        sSQLCmd &= ", @fcFirstName  = '" & txtFirstName.Text & "' "
        sSQLCmd &= ", @fcMidName = '" & txtMiddleInitial.Text & "' "
        sSQLCmd &= ", @fcContactPerson1 = '" & txtContact.Text & "' "
        sSQLCmd &= ", @fcContactPerson2 = '" & txtAltContact.Text & "' "
        sSQLCmd &= ", @fcTelNo1 = '" & txtPhone.Text & "' "
        sSQLCmd &= ", @fcTelNo2 = '" & txtAltPhone.Text & "' "
        sSQLCmd &= ", @fcFaxNo = '" & txtFAX.Text & "' "
        sSQLCmd &= ", @fcEmail = '" & txtEmail.Text & "' "
        sSQLCmd &= ", @fcCC = '" & txtCC.Text & "' "
        sSQLCmd &= ", @fcAccountNo = '" & txtAcctNo.Text & "' "
        sSQLCmd &= ", @fdCreditLimit ='" & txtCreditLimit.Text & "' "
        sSQLCmd &= ", @fnResaleNo ='" & txtResaleNo.Text & "' "
        sSQLCmd &= ", @fcCreditCardNo = '" & txtCreditCardNo.Text & "' "
        sSQLCmd &= ", @fcNameOnCard = '" & txtNameOnCard.Text & "' "
        sSQLCmd &= ", @fcExpirationMonth = '" & txtMonth.Text & "' "
        sSQLCmd &= ", @fcExpirationYear = '" & txtYear.Text & "' "
        sSQLCmd &= ", @fcAddressOnCard = '" & txtAddress.Text & "' "
        sSQLCmd &= ", @fnPostalOnCard = '" & txtZipCode.Text & "' "
        sSQLCmd &= ", @fdDateStart = '" & dteStartDate.Value & "' "
        sSQLCmd &= ", @fdDateEndTemp = '" & dteEndDateTemp.Value & "' "
        sSQLCmd &= ", @fdDateEnd = '" & dteEndDate.Value & "' "
        sSQLCmd &= ", @fcJobDescription = '" & txtJobDescription.Text & "' "
        sSQLCmd &= ", @fbActive = " & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ", @fxKeyCompany = '" & gCompanyID() & "' "
        sSQLCmd &= ", @fcAddress = '" & txtBillAddress.Text & "' "
        sSQLCmd &= ", @fxKeyAddress = '" & sKeyAddress & "' "
        sSQLCmd &= ", @fxKeySendMethod = '" & cboSendMethod.SelectedValue.ToString & "' "
        sSQLCmd &= ", @fxKeyJobStatus = '" & cboJobStat.SelectedValue.ToString & "' "
        sSQLCmd &= ", @fbDeleted = 0"
        sSQLCmd &= ", @fcCreatedBy='" & gUserName & "' "
        sSQLCmd &= ", @fdDateCreated= '" & Now() & "' "
        sSQLCmd &= ", @fcUpdatedBy='" & gUserName & "' "
        sSQLCmd &= ", @fdDateUpdated='" & Now() & "' "

        If cboTaxCodeID.SelectedItem <> Nothing Then
            sSQLCmd &= ", @fxKeySalesTaxCode = '" & cboTaxCodeID.SelectedItem & "'"
        End If

        sSQLCmd &= ", @fcShippingAddress = '" & txtShipAddress.Text & "'"

        If sKeyShipAddress <> "" Then
            sSQLCmd &= ", @fxKeyShippingAddress = '" & sKeyShipAddress & "'"
        End If

        'If cboShipToID.SelectedIndex <> 0 And cboShipToID.SelectedIndex <> 1 Then
        '    sSQLCmd &= ", @fxKeyShippingAddress = '" & cboShipToID.SelectedItem & "'"
        'End If

        If cboTypeID.SelectedIndex <> 0 And cboTypeID.SelectedIndex <> 1 Then
            sSQLCmd &= ", @fxKeyCustomerType = '" & cboTypeID.SelectedItem & "'"
        End If

        If cboTermsID.SelectedIndex <> 0 And cboTermsID.SelectedIndex <> 1 Then
            sSQLCmd &= ", @fxKeyTerms = '" & cboTermsID.SelectedItem & "'"
        End If

        If cboRepID.SelectedIndex <> 0 And cboRepID.SelectedIndex <> 1 Then
            sSQLCmd &= ", @fxKeyRep = '" & cboRepID.SelectedItem & "'"
        End If

        If cboPaymentMethodID.SelectedIndex <> 0 And cboPaymentMethodID.SelectedIndex <> 1 Then
            sSQLCmd &= ", @fxKeyPaymentMethod = '" & cboPaymentMethodID.SelectedItem & "'"
        End If

        If cboJobTypeID.SelectedIndex <> 0 And cboJobTypeID.SelectedIndex <> 1 Then
            sSQLCmd &= ", @fxKeyJobType = '" & cboJobTypeID.SelectedItem & "'"
        End If

        ' TO BE CONT...
        'If cboPriceLvlID.SelectedIndex <> 0 And cboPriceLvlID.SelectedIndex <> 1 Then
        '    sSQLCmd &= ", @fxKeyPriceLevel = '" & cboPriceLvlID.SelectedItem & "'"
        'End If

        ' TO BE CONT...
        'If cboTaxItemID.SelectedIndex <> 0 And cboTaxItemID.SelectedIndex <> 1 Then
        '    sSQLCmd &= ", @fxKeyTaxItem = '" & cboTaxItemID.SelectedItem & "'"
        'End If

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated!", "Add/Edit Customer")
            bUpdateForm = True
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Customer")
        End Try

    End Sub

    Private Sub frm_cust_masterCustomerAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim x As DialogResult
        If bNew = True And bUpdateForm = False And bUpdateShippingAddress = True Then
            x = MessageBox.Show("Address Information will be lost. Are you sure you want to exit ?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            If x = Windows.Forms.DialogResult.Yes Then
                deleteShippingAddress()
            ElseIf x = Windows.Forms.DialogResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub deleteShippingAddress()
        Dim sSQLCmd As String = "DELETE FROM mAddress WHERE fxKeyAddress = '" & sKeyShipAddress & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Shipping Address")
        End Try
    End Sub

    Private Sub frm_cust_masterCustomerAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (sKeyCustomer = "" Or sKeyCustomer = Nothing) And sKeyAddress = "" Then
            sKeyCustomer = Guid.NewGuid.ToString
            sKeyAddress = Guid.NewGuid.ToString
            bNew = True
        End If

        If bNew = True Then
            txtOpeningBal.ReadOnly = False
            lblBalance.Text = "Opening Balance"
        Else
            txtOpeningBal.ReadOnly = True
            lblBalance.Text = "Current Balance"
        End If

        m_loadShippingAddress(cboShipToName, cboShipToID, sKeyCustomer)
        'gloadaddress(cboShipToName, cboShipToID)
        m_loadSendMethod(cboSendMethod)
        m_loadJobStatus(cboJobStat)
        m_loadSalesTaxCode(cboTaxCodeName, cboTaxCodeID)
        m_loadCustomerType(cboTypeName, cboTypeID)
        m_loadTerms(cboTermsName, cboTermsID)
        m_loadSalesRep(cboRepName, cboRepID)
        m_loadPaymentMethod(cboPaymentMethodName, cboPaymentMethodID)
        m_loadJobType(cboJobTypeName, cboJobTypeID)
        m_otherInfoValueGetVal(sKeyCustomer, False)
        m_otherInfoValList(grdDefinedField)
        loadCustomer()
    End Sub

    Public Sub loadCustomer()

        Dim sSQLCmd As String = "usp_m_customer_informationList "
        sSQLCmd &= "@fxKeyCustomer = '" & sKeyCustomer & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtCustomerName.Text = rd.Item("fcCustomerName")
                    txtOpeningBal.Text = rd.Item("fdBalance")
                    dteOpeningBalDate.Value = rd.Item("fdDateAsOf")
                    txtCoName.Text = rd.Item("fcCompanyName")
                    txtSalutation.Text = rd.Item("fcSalutation")
                    txtLastName.Text = rd.Item("fcLastName")
                    txtFirstName.Text = rd.Item("fcFirstName")
                    txtMiddleInitial.Text = rd.Item("fcMidName")
                    txtContact.Text = rd.Item("fcContactPerson1")
                    txtAltContact.Text = rd.Item("fcContactPerson2")
                    txtPhone.Text = rd.Item("fcTelNo1")
                    txtAltPhone.Text = rd.Item("fcTelNo2")
                    txtFAX.Text = rd.Item("fcFaxNo")
                    txtEmail.Text = rd.Item("fcEmail")
                    txtCC.Text = rd.Item("fcCC")
                    txtAcctNo.Text = rd.Item("fcAccountNo")
                    txtCreditLimit.Text = rd.Item("fdCreditLimit")
                    txtResaleNo.Text = rd.Item("fnResaleNo")
                    txtCreditCardNo.Text = rd.Item("fcCreditCardNo")
                    txtNameOnCard.Text = rd.Item("fcNameOnCard")
                    txtMonth.Text = rd.Item("fcExpirationMonth")
                    txtYear.Text = rd.Item("fcExpirationYear")
                    txtAddress.Text = rd.Item("fcAddressOnCard")
                    txtZipCode.Text = rd.Item("fnPostalOnCard")
                    dteStartDate.Value = rd.Item("fdDateStart")
                    dteEndDateTemp.Value = rd.Item("fdDateEndTemp")
                    dteEndDate.Value = rd.Item("fdDateEnd")
                    txtJobDescription.Text = rd.Item("fcJobDescription")
                    txtShipAddress.Text = rd.Item("fcShippingAddress")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                    sKeyType = rd.Item("fxKeyCustomerType").ToString
                    sKeyTerms = rd.Item("fxKeyTerms").ToString
                    sKeyRep = rd.Item("fxKeyRep").ToString
                    sKeySendMenthod = rd.Item("fxKeySendMethod").ToString
                    sKeySalesTaxCode = rd.Item("fxKeySalesTaxCode").ToString
                    sKeyTaxItem = rd.Item("fxKeyTaxItem").ToString
                    sKeyPriceLevel = rd.Item("fxKeyPriceLevel").ToString
                    sKeyPaymentMethod = rd.Item("fxKeyPaymentMethod").ToString
                    sKeyJobStatus = rd.Item("fxKeyJobStatus").ToString
                    'idKeyJobStat = rd.Item("fxKeyJobStatus")
                    sKeyJobType = rd.Item("fxKeyJobType").ToString
                    'sKeyAddress = rd.Item(52).ToString
                    sKeyAddress = rd.Item("fxKeyAddress").ToString
                    txtBillAddress.Text = rd.Item("fcBillingAddress").ToString
                    sKeyShipAddress = rd.Item("fxKeyShippingAddress").ToString
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'txtCustomerName.Text = rd.Item("fcCustomerName")
        'txtOpeningBal.Text = rd.Item("fdBalance")
        'dteOpeningBalDate.Value = rd.Item("fdDateAsOf")
        'txtCoName.Text = rd.Item("fcCompanyName")
        'txtSalutation.Text = rd.Item("fcSalutation")
        'txtLastName.Text = rd.Item("fcLastName")
        'txtFirstName.Text = rd.Item("fcFirstName")
        'txtMiddleInitial.Text = rd.Item("fcMidName")
        'txtContact.Text = rd.Item("fcContactPerson1")
        'txtAltContact.Text = rd.Item("fcContactPerson2")
        'txtPhone.Text = rd.Item("fcTelNo1")
        'txtAltPhone.Text = rd.Item("fcTelNo2")
        'txtFAX.Text = rd.Item("fcFaxNo")
        'txtEmail.Text = rd.Item("fcEmail")
        'txtCC.Text = rd.Item("fcCC")
        'txtAcctNo.Text = rd.Item("fcAccountNo")
        'txtCreditLimit.Text = rd.Item("fdCreditLimit")
        'txtResaleNo.Text = rd.Item("fnResaleNo")
        'txtCreditCardNo.Text = rd.Item("fcCreditCardNo")
        'txtNameOnCard.Text = rd.Item("fcNameOnCard")
        'txtMonth.Text = rd.Item("fcExpirationMonth")
        'txtYear.Text = rd.Item("fcExpirationYear")
        'txtAddress.Text = rd.Item("fcAddressOnCard")
        'txtZipCode.Text = rd.Item("fnPostalOnCard")
        'dteStartDate.Value = rd.Item("fdDateStart")
        'dteEndDateTemp.Value = rd.Item("fdDateEndTemp")
        'dteEndDate.Value = rd.Item("fdDateEnd")
        'txtJobDescription.Text = rd.Item("fcJobDescription")
        'txtShipAddress.Text = rd.Item("fcShippingAddress")
        'chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
        'sKeyType = rd.Item("fxKeyCustomerType").ToString
        'sKeyTerms = rd.Item("fxKeyTerms").ToString
        'sKeyRep = rd.Item("fxKeyRep").ToString
        'sKeySendMenthod = rd.Item("fxKeySendMethod").ToString
        'sKeySalesTaxCode = rd.Item("fxKeySalesTaxCode").ToString
        'sKeyTaxItem = rd.Item("fxKeyTaxItem").ToString
        'sKeyPriceLevel = rd.Item("fxKeyPriceLevel").ToString
        'sKeyPaymentMethod = rd.Item("fxKeyPaymentMethod").ToString
        'sKeyJobStatus = rd.Item("fxKeyJobStatus").ToString
        ''idKeyJobStat = rd.Item("fxKeyJobStatus")
        'sKeyJobType = rd.Item("fxKeyJobType").ToString
        'sKeyAddress = rd.Item(52).ToString
        'txtBillAddress.Text = rd.Item("fcBillingAddress").ToString
        'sKeyShipAddress = rd.Item("fxKeyShippingAddress").ToString

        If sKeySalesTaxCode <> "" Then
            m_selectedCboValue(cboTaxCodeName, cboTaxCodeID, sKeySalesTaxCode)
        Else
            selectTaxCode()
        End If


        m_selectedCboValue(cboShipToName, cboShipToID, sKeyShipAddress)
        m_selectedCboValue(cboTypeName, cboTypeID, sKeyType)
        m_selectedCboValue(cboTermsName, cboTermsID, sKeyTerms)
        m_selectedCboValue(cboRepName, cboRepID, sKeyRep)
        m_selectedCboValue(cboPaymentMethodName, cboPaymentMethodID, sKeyPaymentMethod)
        m_selectedCboValueJobStat()
        m_selectedCboValue(cboJobTypeName, cboJobTypeID, sKeyJobType)
        'TO BE CONT... m_selectedCboValue(cboPriceLvlName, cboPriceLvlID, sKeyPriceLevel)
        'TO BE CONT... m_selectedCboValue(cboTaxItemName, cboTaxItemID, sKeyTaxItem)
       
    End Sub

    Private Sub m_selectedCboValueJobStat()
        If sKeyJobStatus = "" Then
            'cboJobStat.SelectedText = "None"
            defaultJobStat()
            cboJobStat.SelectedItem = idKeyJobStat
            cboJobStat.SelectedValue = idKeyJobStat
        Else
            cboJobStat.SelectedText = sKeyJobStatus
            cboJobStat.SelectedItem = sKeyJobStatus
            cboJobStat.SelectedValue = sKeyJobStatus
        End If
    End Sub

    Private Sub selectTaxCode()
        If sKeySalesTaxCode = "" Then
            cboTaxCodeName.SelectedText = "Tax"
            cboTaxCodeName.SelectedItem = "Tax"
            cboTaxCodeID.SelectedIndex = cboTaxCodeName.SelectedIndex
        End If
    End Sub

    Private Sub txtOpeningBal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOpeningBal.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtResaleNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtResaleNo.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtCreditLimit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCreditLimit.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtZipCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtZipCode.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub btnEditBillAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBillAdd.Click
        If sKeyAddress = "" Then
            sKeyAddress = Guid.NewGuid.ToString
        End If
        frm_cust_masterCustomerAddress.KeyAddress = sKeyAddress
        frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
        frm_cust_masterCustomerAddress.IsShippingAdd = False
        frm_cust_masterCustomerAddress.ShowDialog()
    End Sub

    Private Sub btnAddShip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddShip.Click
        frm_cust_masterCustomerAddress.KeyAddress = Guid.NewGuid.ToString
        frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
        frm_cust_masterCustomerAddress.IsShippingAdd = True
        frm_cust_masterCustomerAddress.Show()
    End Sub

    Private Sub btnEditShipAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditShipAdd.Click
        frm_cust_masterCustomerAddress.KeyAddress = cboShipToID.SelectedItem
        frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
        frm_cust_masterCustomerAddress.IsShippingAdd = True
        frm_cust_masterCustomerAddress.Show()
    End Sub

    Private Sub loadShippingAddress()

        Dim sSQLCmd As String = "SELECT * FROM mAddress WHERE fxKeyAddress = '" & cboShipToID.SelectedItem & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtShipAddress.Text = rd.Item("fcAddress")
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Shipping Address")
        End Try
    End Sub

    Private Sub cboTaxCodeName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTaxCodeName.SelectedIndexChanged
        cboTaxCodeID.SelectedIndex() = cboTaxCodeName.SelectedIndex

        If cboTaxCodeName.SelectedIndex = 1 Then
            frm_vend_SalesTaxAdd.KeySalesTaxCode = Guid.NewGuid.ToString
            frm_vend_SalesTaxAdd.ShowDialog()
        End If
    End Sub

    Private Sub cboShipToName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboShipToName.SelectedIndexChanged
        If cboShipToName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddress.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
            frm_cust_masterCustomerAddress.IsShippingAdd = True
            frm_cust_masterCustomerAddress.ShowDialog()
        End If

        cboShipToID.SelectedIndex = cboShipToName.SelectedIndex
        If cboShipToID.SelectedItem Is Nothing Then
            cboShipToID.SelectedItem = ""
        End If
        sKeyShipAddress = cboShipToID.SelectedItem

        If sKeyShipAddress <> "" Then
            loadShippingAddress()
        Else
            txtShipAddress.Text = ""
        End If

        'If cboShipToName.SelectedIndex <> 1 And cboShipToName.SelectedIndex <> 0 Then
        '    loadShippingAddress()
        'End If


    End Sub

    Private Sub cboTypeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeName.SelectedIndexChanged
        cboTypeID.SelectedIndex() = cboTypeName.SelectedIndex

        If cboTypeName.SelectedIndex = 1 Then
            frm_MF_customerTypeAddEdit.KeyCustomerType = Guid.NewGuid.ToString
            frm_MF_customerTypeAddEdit.ShowDialog()
        End If
    End Sub

    Private Sub cboTermsName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTermsName.SelectedIndexChanged
        cboTermsID.SelectedIndex() = cboTermsName.SelectedIndex

        If cboTermsName.SelectedIndex = 1 Then
            frm_MF_termsAddEdit.Keyterms = Guid.NewGuid.ToString
            frm_MF_termsAddEdit.ShowDialog()
        End If
    End Sub

    Private Sub cboRepName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRepName.SelectedIndexChanged
        cboRepID.SelectedIndex = cboRepName.SelectedIndex

        If cboRepName.SelectedIndex = 1 Then
            frm_MF_salesRepAddEdit.KeySalesRep = Guid.NewGuid.ToString
            frm_MF_salesRepAddEdit.ShowDialog()
        End If
    End Sub

    Private Sub cboPaymentMethodName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMethodName.SelectedIndexChanged
        cboPaymentMethodID.SelectedIndex = cboPaymentMethodName.SelectedIndex

        If cboPaymentMethodName.SelectedIndex = 1 Then
            Dim x As New frm_MF_paymentMethodAddEdit
            x.Show()
        End If
    End Sub

    Private Sub cboJobTypeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJobTypeName.SelectedIndexChanged
        cboJobTypeID.SelectedIndex = cboJobTypeName.SelectedIndex

        If cboJobTypeName.SelectedIndex = 1 Then
            'TO BE CONT...
        End If
    End Sub

    Private Sub cboPriceLvlName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPriceLvlName.SelectedIndexChanged
        cboPriceLvlID.SelectedIndex = cboPriceLvlName.SelectedIndex

        If cboPriceLvlName.SelectedIndex = 1 Then
            'TO BE CONT...
        End If
    End Sub

    Private Sub cboTaxItemName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxItemName.SelectedIndexChanged
        cboTaxItemID.SelectedIndex = cboTaxItemName.SelectedIndex

        If cboTaxItemName.SelectedIndex = 1 Then
            'TO BE CONT...
        End If
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        If Trim(txtBillAddress.Text) <> "" Then
            txtShipAddress.Text = txtBillAddress.Text

            If sKeyShipAddress <> "" Then
                updateShippingAdd()
            Else
                frm_cust_masterCustomerAddress.KeyAddress = Guid.NewGuid.ToString
                frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
                frm_cust_masterCustomerAddress.IsShippingAdd = True
                frm_cust_masterCustomerAddress.txtAddress.Text = txtShipAddress.Text
                frm_cust_masterCustomerAddress.Show()
                bUpdateShippingAddress = True
            End If

        End If
    End Sub

    Private Sub txtShipAddress_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtShipAddress.LostFocus
        If Trim(txtBillAddress.Text) <> "" Then
            If sKeyShipAddress <> "" Then
                updateShippingAdd()
            Else
                frm_cust_masterCustomerAddress.KeyAddress = Guid.NewGuid.ToString
                frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
                frm_cust_masterCustomerAddress.IsShippingAdd = True
                frm_cust_masterCustomerAddress.txtAddress.Text = txtShipAddress.Text
                frm_cust_masterCustomerAddress.Show()
                bUpdateShippingAddress = True
            End If
        End If
    End Sub

    'Private Sub txtShipAddress_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtShipAddress.TextChanged

    'End Sub

    Private Sub updateShippingAdd()
        If Trim(txtShipAddress.Text) <> "" Then
            Dim sSQLCmd As String = "UPDATE mAddress SET fcAddress = '" & txtShipAddress.Text & "'"
            sSQLCmd &= "WHERE fxKeyAddress = '" & sKeyShipAddress & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            Catch ex As Exception
                MessageBox.Show(Err.Description, "Update Shipping Address")
            End Try
        End If
    End Sub

    Private Sub btnDeleteShipAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteShipAdd.Click
        If MessageBox.Show("Are you sure you want delete this record?", "Delete Shipping Address", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            deleteShippingAddress()
            m_loadShippingAddress(cboShipToName, cboJobTypeID, sKeyCustomer)
        End If
    End Sub

    Private Sub defaultJobStat()

        Dim sSQLCmd As String = "SELECT fxKeyJobStatus FROM mCustomer05JobStatus"
        sSQLCmd &= " WHERE fcJobStatusName = 'None'"

        Try

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If (rd.Read) Then
                    idKeyJobStat = rd.Item("fxKeyJobStatus")
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class