<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreditMemos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCreditMemos))
        Me.grdCredit = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.grdRefund = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblPayments = New System.Windows.Forms.TextBox
        Me.lblRetDiscount = New System.Windows.Forms.Label
        Me.lblRDPercent = New System.Windows.Forms.Label
        Me.txtRDAmt = New System.Windows.Forms.TextBox
        Me.cboRD = New System.Windows.Forms.ComboBox
        Me.lblSaleDiscount = New System.Windows.Forms.Label
        Me.lblSDpercent = New System.Windows.Forms.Label
        Me.txtSDAmt = New System.Windows.Forms.TextBox
        Me.cboSD = New System.Windows.Forms.ComboBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.cboTaxName = New System.Windows.Forms.ComboBox
        Me.cboTaxID = New System.Windows.Forms.ComboBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblTaxAmt = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.btnRefund = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        CType(Me.grdCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdRefund, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdCredit
        '
        Me.grdCredit.AllowUserToAddRows = False
        Me.grdCredit.AllowUserToDeleteRows = False
        Me.grdCredit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCredit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCredit.Location = New System.Drawing.Point(8, 53)
        Me.grdCredit.Name = "grdCredit"
        Me.grdCredit.Size = New System.Drawing.Size(727, 513)
        Me.grdCredit.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Customer"
        '
        'cboCustomerName
        '
        Me.cboCustomerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(77, 20)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(262, 21)
        Me.cboCustomerName.TabIndex = 53
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(77, 20)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(210, 21)
        Me.cboCustomerID.TabIndex = 52
        '
        'grdRefund
        '
        Me.grdRefund.AllowUserToAddRows = False
        Me.grdRefund.AllowUserToDeleteRows = False
        Me.grdRefund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdRefund.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRefund.Location = New System.Drawing.Point(7, 20)
        Me.grdRefund.Name = "grdRefund"
        Me.grdRefund.Size = New System.Drawing.Size(714, 310)
        Me.grdRefund.TabIndex = 54
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.lblPayments)
        Me.GroupBox1.Controls.Add(Me.lblRetDiscount)
        Me.GroupBox1.Controls.Add(Me.lblRDPercent)
        Me.GroupBox1.Controls.Add(Me.txtRDAmt)
        Me.GroupBox1.Controls.Add(Me.cboRD)
        Me.GroupBox1.Controls.Add(Me.lblSaleDiscount)
        Me.GroupBox1.Controls.Add(Me.lblSDpercent)
        Me.GroupBox1.Controls.Add(Me.txtSDAmt)
        Me.GroupBox1.Controls.Add(Me.cboSD)
        Me.GroupBox1.Controls.Add(Me.lblPercentage)
        Me.GroupBox1.Controls.Add(Me.cboTaxName)
        Me.GroupBox1.Controls.Add(Me.cboTaxID)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.lblTaxAmt)
        Me.GroupBox1.Controls.Add(Me.lblTotal)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.lblBalance)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.grdRefund)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 47)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(727, 519)
        Me.GroupBox1.TabIndex = 56
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Details"
        '
        'lblPayments
        '
        Me.lblPayments.Location = New System.Drawing.Point(572, 459)
        Me.lblPayments.Name = "lblPayments"
        Me.lblPayments.Size = New System.Drawing.Size(142, 21)
        Me.lblPayments.TabIndex = 107
        Me.lblPayments.Text = "0.00"
        Me.lblPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRetDiscount
        '
        Me.lblRetDiscount.Location = New System.Drawing.Point(572, 434)
        Me.lblRetDiscount.Name = "lblRetDiscount"
        Me.lblRetDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblRetDiscount.TabIndex = 106
        Me.lblRetDiscount.Text = "0.00"
        Me.lblRetDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRDPercent
        '
        Me.lblRDPercent.AutoSize = True
        Me.lblRDPercent.Location = New System.Drawing.Point(389, 435)
        Me.lblRDPercent.Name = "lblRDPercent"
        Me.lblRDPercent.Size = New System.Drawing.Size(47, 13)
        Me.lblRDPercent.TabIndex = 105
        Me.lblRDPercent.Text = "(0.0%)"
        '
        'txtRDAmt
        '
        Me.txtRDAmt.Enabled = False
        Me.txtRDAmt.Location = New System.Drawing.Point(444, 431)
        Me.txtRDAmt.Name = "txtRDAmt"
        Me.txtRDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtRDAmt.TabIndex = 104
        Me.txtRDAmt.Text = "0.00"
        Me.txtRDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboRD
        '
        Me.cboRD.FormattingEnabled = True
        Me.cboRD.Location = New System.Drawing.Point(280, 431)
        Me.cboRD.Name = "cboRD"
        Me.cboRD.Size = New System.Drawing.Size(105, 21)
        Me.cboRD.TabIndex = 103
        '
        'lblSaleDiscount
        '
        Me.lblSaleDiscount.Location = New System.Drawing.Point(572, 409)
        Me.lblSaleDiscount.Name = "lblSaleDiscount"
        Me.lblSaleDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblSaleDiscount.TabIndex = 102
        Me.lblSaleDiscount.Text = "0.00"
        Me.lblSaleDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSDpercent
        '
        Me.lblSDpercent.AutoSize = True
        Me.lblSDpercent.Location = New System.Drawing.Point(389, 410)
        Me.lblSDpercent.Name = "lblSDpercent"
        Me.lblSDpercent.Size = New System.Drawing.Size(47, 13)
        Me.lblSDpercent.TabIndex = 101
        Me.lblSDpercent.Text = "(0.0%)"
        '
        'txtSDAmt
        '
        Me.txtSDAmt.Enabled = False
        Me.txtSDAmt.Location = New System.Drawing.Point(444, 406)
        Me.txtSDAmt.Name = "txtSDAmt"
        Me.txtSDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtSDAmt.TabIndex = 100
        Me.txtSDAmt.Text = "0.00"
        Me.txtSDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboSD
        '
        Me.cboSD.FormattingEnabled = True
        Me.cboSD.Location = New System.Drawing.Point(280, 406)
        Me.cboSD.Name = "cboSD"
        Me.cboSD.Size = New System.Drawing.Size(105, 21)
        Me.cboSD.TabIndex = 99
        '
        'lblPercentage
        '
        Me.lblPercentage.AutoSize = True
        Me.lblPercentage.Location = New System.Drawing.Point(472, 351)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(47, 13)
        Me.lblPercentage.TabIndex = 97
        Me.lblPercentage.Text = "(0.0%)"
        '
        'cboTaxName
        '
        Me.cboTaxName.FormattingEnabled = True
        Me.cboTaxName.Location = New System.Drawing.Point(362, 346)
        Me.cboTaxName.Name = "cboTaxName"
        Me.cboTaxName.Size = New System.Drawing.Size(104, 21)
        Me.cboTaxName.TabIndex = 98
        '
        'cboTaxID
        '
        Me.cboTaxID.FormattingEnabled = True
        Me.cboTaxID.Location = New System.Drawing.Point(362, 346)
        Me.cboTaxID.Name = "cboTaxID"
        Me.cboTaxID.Size = New System.Drawing.Size(104, 21)
        Me.cboTaxID.TabIndex = 96
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(333, 349)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(59, 18)
        Me.Label25.TabIndex = 95
        Me.Label25.Text = "Tax"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(572, 392)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(142, 2)
        Me.Panel1.TabIndex = 94
        '
        'lblTaxAmt
        '
        Me.lblTaxAmt.Location = New System.Drawing.Point(572, 351)
        Me.lblTaxAmt.Name = "lblTaxAmt"
        Me.lblTaxAmt.Size = New System.Drawing.Size(143, 13)
        Me.lblTaxAmt.TabIndex = 92
        Me.lblTaxAmt.Text = "0.00"
        Me.lblTaxAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(572, 376)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(143, 13)
        Me.lblTotal.TabIndex = 91
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(448, 376)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 18)
        Me.Label22.TabIndex = 90
        Me.Label22.Text = "Total"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(452, 465)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(109, 13)
        Me.Label9.TabIndex = 88
        Me.Label9.Text = "Payments Applied"
        '
        'lblBalance
        '
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(572, 488)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(143, 13)
        Me.lblBalance.TabIndex = 93
        Me.lblBalance.Text = "0.00"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(452, 488)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(79, 13)
        Me.Label19.TabIndex = 89
        Me.Label19.Text = "Balance Due"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(513, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 23)
        Me.Button1.TabIndex = 60
        Me.Button1.Text = "For Credit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnRefund
        '
        Me.btnRefund.Location = New System.Drawing.Point(625, 18)
        Me.btnRefund.Name = "btnRefund"
        Me.btnRefund.Size = New System.Drawing.Size(109, 23)
        Me.btnRefund.TabIndex = 59
        Me.btnRefund.Text = "For Refund"
        Me.btnRefund.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(646, 589)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(88, 25)
        Me.btnClose.TabIndex = 60
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(554, 589)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(88, 25)
        Me.btnSave.TabIndex = 61
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmCreditMemos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(746, 632)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.grdCredit)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnRefund)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCreditMemos"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Credit Memos and Refunds"
        CType(Me.grdCredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdRefund, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdCredit As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents grdRefund As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRefund As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblPayments As System.Windows.Forms.TextBox
    Friend WithEvents lblRetDiscount As System.Windows.Forms.Label
    Friend WithEvents lblRDPercent As System.Windows.Forms.Label
    Friend WithEvents txtRDAmt As System.Windows.Forms.TextBox
    Friend WithEvents cboRD As System.Windows.Forms.ComboBox
    Friend WithEvents lblSaleDiscount As System.Windows.Forms.Label
    Friend WithEvents lblSDpercent As System.Windows.Forms.Label
    Friend WithEvents txtSDAmt As System.Windows.Forms.TextBox
    Friend WithEvents cboSD As System.Windows.Forms.ComboBox
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents cboTaxName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxID As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTaxAmt As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
End Class
