Imports Microsoft.ApplicationBlocks.Data

Public Class frmCreditMemos

    Private gcon As New Clsappconfiguration

    Private sKeyInvoice As String
    Private sKeyCustomer As String

    Private Sub frmCreditMemos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_loadCustomer(cboCustomerName, cboCustomerID)
        GroupBox1.Visible = False
    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem
        If cboCustomerName.SelectedIndex = 1 Then
            Dim x As New frm_cust_masterCustomerAddEdit
            x.ShowDialog()
        End If
        If cboCustomerName.SelectedIndex > 1 Then
            Call createcolumn()
        End If
    End Sub

    Private Sub grdCredit_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCredit.CellClick
        With Me.grdCredit
            If .CurrentRow.Cells("Invoice No").Value.ToString <> "0" Then
                sKeyInvoice = .CurrentRow.Cells("fxKeyInvoice").Value.ToString
                GroupBox1.Visible = True
                Call getinvoicedetails()
            End If
        End With
    End Sub

    Private Sub createcolumn()
        'Dim colInvoiceKey As New DataGridViewTextBoxColumn
        'Dim colRefno As New DataGridViewTextBoxColumn
        'Dim colBalancedue As New DataGridViewTextBoxColumn
        Dim colCreditAmt As New DataGridViewTextBoxColumn
        Dim colRefund As New DataGridViewTextBoxColumn
        'Dim colDateTransact As New DataGridViewTextBoxColumn
        Dim dtInvoice As DataTable = displaycreditdetails().Tables(0)
        With Me.grdCredit
            .Columns.Clear()
            .DataSource = dtInvoice.DefaultView
            'colInvoiceKey.DataPropertyName = "fxKeyInvoice"

            'With colRefno
            '    .DataPropertyName = "fcInvoiceNo"
            '    .HeaderText = "Reference #"
            'End With
            'With colBalancedue
            '    .DataPropertyName = "fdBalance"
            '    .HeaderText = "Balance Due"
            'End With
            With colCreditAmt
                .Name = "fdCredit"
                .HeaderText = "Credit Amount"
            End With
            With colRefund
                .Name = "fdRefund"
                .HeaderText = "Refund Amount"
            End With
            'With colDateTransact
            '    .DataPropertyName = "fdDateTransact"
            '    .HeaderText = "Date"
            'End With
            '.Columns.Add(colInvoiceKey)
            '.Columns.Add(colRefno)
            '.Columns.Add(colDateTransact)
            '.Columns.Add(colBalancedue)
            .Columns.Add(colCreditAmt)
            .Columns.Add(colRefund)

            .Columns("Invoice No").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter
            .Columns("Date").DefaultCellStyle.Format = "MM/dd/yyyy"
            .Columns("Balance Due").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("fdCredit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("fdRefund").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

            .Columns("Date").ReadOnly = True
            .Columns("Date").DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns("Invoice No").ReadOnly = True
            .Columns("Invoice No").DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns("Balance Due").ReadOnly = True
            .Columns("Balance Due").DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns("Balance Due").DefaultCellStyle.Format = "##,##0.00"
            .Columns("fdCredit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("fdRefund").DefaultCellStyle.Format = "##,##0.00"

            .Columns("fxKeyInvoice").Visible = False
            .Columns("Invoice No").Width = 100
            .Columns("Date").Width = 100
            .Columns("Balance Due").Width = 150
            .Columns("fdCredit").Width = 150
            .Columns("fdRefund").Width = 150
        End With
    End Sub

    Private Sub getinvoicedetails()
        Dim colRefundAmt As New DataGridViewTextBoxColumn
        Dim colDateTransact As New DataGridViewTextBoxColumn
        Dim dtInvoice As DataTable = displayinvoicedetails().Tables(0)
        With Me.grdRefund
            .Columns.Clear()
            .DataSource = dtInvoice.DefaultView
            With colRefundAmt
                .HeaderText = "Refund Amt"
                .Name = "refund"
            End With
            .Columns.Add(colRefundAmt)

            .Columns("Qty").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter
            .Columns("Price").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Orig Amt").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("refund").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

            .Columns("Item Description").ReadOnly = True
            .Columns("Item Description").DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns("Price").ReadOnly = True
            .Columns("Price").DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns("Orig Amt").ReadOnly = True
            .Columns("Orig Amt").DefaultCellStyle.BackColor = Color.Gainsboro

            .Columns("Price").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Orig Amt").DefaultCellStyle.Format = "##,##0.00"
            .Columns("refund").DefaultCellStyle.Format = "##,##0.00"

            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns("Item Description").Width = 240
            .Columns("Qty").Width = 80
            .Columns("Price").Width = 100
            .Columns("Orig Amt").Width = 100
            .Columns("refund").Width = 100
        End With
    End Sub

    Private Function displaycreditdetails() As DataSet
        Dim sSQLCmd As String = "select fxKeyInvoice, isnull(fcInvoiceNo, 0) as [Invoice No], fdDateTransact as [Date], fdBalance as [Balance Due] "
        sSQLCmd &= " from dbo.tInvoice where fxKeyCustomer ='" & sKeyCustomer & "' "
        Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
    End Function

    Private Function displayinvoicedetails() As DataSet
        Dim sSQLcmd As String = "usp_displayInvoiceItem "
        sSQLcmd &= " @fxKeyInvoice='" & sKeyInvoice & "' "
        Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLcmd)
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GroupBox1.Visible = False
    End Sub

    Private Sub btnRefund_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefund.Click
        GroupBox1.Visible = False
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
End Class