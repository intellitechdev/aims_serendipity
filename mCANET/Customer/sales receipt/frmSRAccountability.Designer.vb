<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSRAccountability
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSRAccountability))
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtPercent = New System.Windows.Forms.TextBox
        Me.cboTaxAcnt = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.cboOutputTax = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPer1 = New System.Windows.Forms.TextBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.cboCOS = New System.Windows.Forms.ComboBox
        Me.cboSalesAcnt = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboPayAcnt = New System.Windows.Forms.ComboBox
        Me.cboMerch = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cboSalesDiscount = New System.Windows.Forms.ComboBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(410, 81)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 13)
        Me.Label24.TabIndex = 107
        Me.Label24.Text = "%"
        '
        'txtPercent
        '
        Me.txtPercent.Location = New System.Drawing.Point(368, 78)
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Size = New System.Drawing.Size(40, 21)
        Me.txtPercent.TabIndex = 108
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTaxAcnt
        '
        Me.cboTaxAcnt.DropDownWidth = 500
        Me.cboTaxAcnt.FormattingEnabled = True
        Me.cboTaxAcnt.Location = New System.Drawing.Point(168, 78)
        Me.cboTaxAcnt.MaxDropDownItems = 25
        Me.cboTaxAcnt.Name = "cboTaxAcnt"
        Me.cboTaxAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboTaxAcnt.Sorted = True
        Me.cboTaxAcnt.TabIndex = 110
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(6, 82)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(116, 13)
        Me.Label23.TabIndex = 109
        Me.Label23.Text = "Tax Credit Account"
        '
        'cboOutputTax
        '
        Me.cboOutputTax.DropDownWidth = 500
        Me.cboOutputTax.FormattingEnabled = True
        Me.cboOutputTax.Location = New System.Drawing.Point(168, 105)
        Me.cboOutputTax.MaxDropDownItems = 25
        Me.cboOutputTax.Name = "cboOutputTax"
        Me.cboOutputTax.Size = New System.Drawing.Size(199, 21)
        Me.cboOutputTax.Sorted = True
        Me.cboOutputTax.TabIndex = 118
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 13)
        Me.Label3.TabIndex = 117
        Me.Label3.Text = "Credit Output Tax"
        '
        'txtPer1
        '
        Me.txtPer1.Location = New System.Drawing.Point(368, 105)
        Me.txtPer1.Name = "txtPer1"
        Me.txtPer1.Size = New System.Drawing.Size(40, 21)
        Me.txtPer1.TabIndex = 116
        Me.txtPer1.Text = "0.00"
        Me.txtPer1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(280, 213)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 28)
        Me.btnClose.TabIndex = 114
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(410, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 13)
        Me.Label2.TabIndex = 115
        Me.Label2.Text = "%"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(184, 213)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(90, 28)
        Me.btnSave.TabIndex = 113
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboCOS
        '
        Me.cboCOS.DropDownWidth = 500
        Me.cboCOS.FormattingEnabled = True
        Me.cboCOS.Location = New System.Drawing.Point(168, 132)
        Me.cboCOS.MaxDropDownItems = 25
        Me.cboCOS.Name = "cboCOS"
        Me.cboCOS.Size = New System.Drawing.Size(199, 21)
        Me.cboCOS.Sorted = True
        Me.cboCOS.TabIndex = 119
        '
        'cboSalesAcnt
        '
        Me.cboSalesAcnt.DropDownWidth = 500
        Me.cboSalesAcnt.FormattingEnabled = True
        Me.cboSalesAcnt.Location = New System.Drawing.Point(168, 24)
        Me.cboSalesAcnt.MaxDropDownItems = 25
        Me.cboSalesAcnt.Name = "cboSalesAcnt"
        Me.cboSalesAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboSalesAcnt.Sorted = True
        Me.cboSalesAcnt.TabIndex = 112
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 13)
        Me.Label4.TabIndex = 120
        Me.Label4.Text = "Credit COS"
        '
        'cboPayAcnt
        '
        Me.cboPayAcnt.DropDownWidth = 500
        Me.cboPayAcnt.FormattingEnabled = True
        Me.cboPayAcnt.Location = New System.Drawing.Point(168, 51)
        Me.cboPayAcnt.MaxDropDownItems = 25
        Me.cboPayAcnt.Name = "cboPayAcnt"
        Me.cboPayAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboPayAcnt.Sorted = True
        Me.cboPayAcnt.TabIndex = 106
        '
        'cboMerch
        '
        Me.cboMerch.DropDownWidth = 500
        Me.cboMerch.FormattingEnabled = True
        Me.cboMerch.Location = New System.Drawing.Point(168, 159)
        Me.cboMerch.MaxDropDownItems = 25
        Me.cboMerch.Name = "cboMerch"
        Me.cboMerch.Size = New System.Drawing.Size(199, 21)
        Me.cboMerch.Sorted = True
        Me.cboMerch.TabIndex = 121
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 55)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(152, 13)
        Me.Label20.TabIndex = 105
        Me.Label20.Text = "Receivable Debit Account"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 162)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 13)
        Me.Label5.TabIndex = 122
        Me.Label5.Text = "Debit Merchandise"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 13)
        Me.Label1.TabIndex = 111
        Me.Label1.Text = "Credit Gross Sales"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtPercent)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.cboTaxAcnt)
        Me.GroupBox1.Controls.Add(Me.cboSalesDiscount)
        Me.GroupBox1.Controls.Add(Me.cboMerch)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.cboPayAcnt)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cboSalesAcnt)
        Me.GroupBox1.Controls.Add(Me.cboCOS)
        Me.GroupBox1.Controls.Add(Me.btnSave)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnClose)
        Me.GroupBox1.Controls.Add(Me.txtPer1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cboOutputTax)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(430, 244)
        Me.GroupBox1.TabIndex = 123
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Accounts"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 189)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(125, 13)
        Me.Label6.TabIndex = 122
        Me.Label6.Text = "Debit Sales Discount"
        '
        'cboSalesDiscount
        '
        Me.cboSalesDiscount.DropDownWidth = 500
        Me.cboSalesDiscount.FormattingEnabled = True
        Me.cboSalesDiscount.Location = New System.Drawing.Point(168, 186)
        Me.cboSalesDiscount.MaxDropDownItems = 25
        Me.cboSalesDiscount.Name = "cboSalesDiscount"
        Me.cboSalesDiscount.Size = New System.Drawing.Size(199, 21)
        Me.cboSalesDiscount.Sorted = True
        Me.cboSalesDiscount.TabIndex = 121
        '
        'frmSRAccountability
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(439, 253)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(443, 229)
        Me.Name = "frmSRAccountability"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sales Receipt Accountability"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtPercent As System.Windows.Forms.TextBox
    Friend WithEvents cboTaxAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cboOutputTax As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPer1 As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cboCOS As System.Windows.Forms.ComboBox
    Friend WithEvents cboSalesAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboPayAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents cboMerch As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboSalesDiscount As System.Windows.Forms.ComboBox
End Class
