Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient

Public Class frm_cust_CreateSalesReceipt
    Private gCon As New Clsappconfiguration

    Private Const cKeySRItem = 0
    Private Const cItem = 1
    Private Const cDescription = 2
    Private Const cQuantity = 3
    Private Const cUnit = 4
    Private Const cRate = 5
    Private Const cAmount = 6

    Private sKeySR As String
    Private sKeyCustomer As String
    Private sKeySalesTaxCode As String
    Private sKeyTax As String
    Private sKeyShipAdd As String
    Private sKeyPaymentMethod As String
    Private dPercentage As Long = 0
    Private dTotalInitial As Long = 0
    Private dTaxAmntInitial As Long = 0
    Private sBillToInitial As String
    Private sKeyToDel() As String = {""}
    Private iDel As Integer = 0
    Private sCoAddress As String
    Private ClassRefNo As String = ""
    Dim checkDate As New clsRestrictedDate

#Region "Properties"
    Public Property KeySR() As String
        Get
            Return sKeySR
        End Get
        Set(ByVal value As String)
            sKeySR = value
            refreshForm()
        End Set
    End Property
#End Region

#Region "Events"
    Private Sub frm_cust_CreateSalesReceipt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadcomboboxs()
        If sKeySR = "" Or sKeySR = Nothing Then
            sKeySR = Guid.NewGuid.ToString
            txtSaleNo.Text = defaultInvoiceNo()
            refreshForm()
            IsDateRestricted()
        Else
            refreshForm()
            gEditEvent = 1
            loadsalesreceipts()
            IsDateRestricted()
        End If
        ' refreshForm()

        If frmMain.LblClassActivator.Visible = True Then
            Label9.Visible = True
            txtClassName.Visible = True
            txtClassName.Enabled = True
            BtnBrowse.Enabled = True
            BtnBrowse.Visible = True
        Else
            Label9.Visible = False
            txtClassName.Visible = False
            txtClassName.Enabled = False
            BtnBrowse.Enabled = False
            BtnBrowse.Visible = False
        End If

        If validateAccountability("SR") = "Kulang" Then
            frmSRAccountability.ShowDialog()
        End If
    End Sub

    Private Sub btn_rcpt_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If validateAccountability("SR") = "Kulang" Then
            frmSRAccountability.ShowDialog()
            Exit Sub
        Else
            gKeySR = ""
            Me.Dispose()
            Me.Close()
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If validateAccountability("SR") = "Kulang" Then
            frmSRAccountability.ShowDialog()
            exit Sub
        Else
            If cboCustomerID.SelectedItem = "" Then
                MessageBox.Show("Customer field cannot be empty!", "Data Validation")
            Else
                updateSR()
            End If
        End If
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        createColumn()
        iDel = 0
        sKeyToDel(iDel) = ""
    End Sub
    Private Sub btnSelDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelDel.Click
        Dim xCnt As Integer
        With Me.grdSalesRcpt

            For xCnt = 0 To .RowCount - 1
                If .Rows(xCnt).Cells("cSelect").Value = True Then
                    If .Rows(xCnt).Cells(3).Value IsNot Nothing Then
                        If sKeySR <> "" Then
                            Call m_deleteReceiptItem(.Rows(xCnt).Cells(0).Value.ToString, Me)
                        End If
                        .Rows.Remove(.Rows(xCnt))
                    End If
                End If
            Next

        End With
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        frmSalesReceipt.ShowDialog()
    End Sub
    Private Sub tsPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsPreview.Click
        frmSalesReceipt.ShowDialog()
    End Sub
    Private Sub btnAccountability_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccountability.Click
        frmSRAccountability.ShowDialog()
    End Sub
    Private Sub grdSalesRcpt_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSalesRcpt.CellClick
        gEditEvent = 0
    End Sub
    Private Sub cboSD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSD.SelectedIndexChanged
        If cboSD.SelectedIndex = 1 Then
            frmSalesDiscount.ShowDialog()
        ElseIf cboSD.SelectedIndex > 2 Then
            lblSDpercent.Text = CStr(Format(CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)), "##0.0#")) + "%"
            Call computesalesdiscount()
        ElseIf cboSD.SelectedIndex = 0 Then
            lblSaleDiscount.Text = "0.00"
            lblSDpercent.Text = "(0.0%)"
            Call calculatedueamount()
        End If
        computeretailersdiscount()
        LblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)

        'If cboSD.SelectedIndex = 1 Then
        '    frmSalesDiscount.ShowDialog()
        'ElseIf cboSD.SelectedIndex > 2 Then
        '    lblSDpercent.Text = CStr(Format(CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)), "##0.0#")) + "%"
        '    Call computesalesdiscount()
        'End If
    End Sub
    Private Sub cboRD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRD.SelectedIndexChanged

        If cboRD.SelectedIndex = 1 Then
            frmRetailersDiscount.ShowDialog()
        ElseIf cboRD.SelectedIndex > 2 Then
            lblRDPercent.Text = CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")) + "%"
            Call computeretailersdiscount()
        ElseIf cboRD.SelectedIndex = 0 Then
            lblRetDiscount.Text = "0.00"
            lblRDPercent.Text = "(0.0%)"
            Call calculatedueamount()
        End If
        LblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)

    End Sub
    Private Sub dteSale_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteSale.ValueChanged
        IsDateRestricted()
    End Sub
    Private Sub grdSalesRcpt_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSalesRcpt.CellValueChanged
        If gEditEvent <> 1 Then
            If e.RowIndex >= 0 Then
                Try
                    Dim sItemTemp As String = grdSalesRcpt.CurrentRow.Cells("cItem").Value.ToString
                    Dim sKeyItemTemp As String = m_GetItemByName(sItemTemp)
                    Dim dtItemTemp As DataTable = m_GetItem(sKeyItemTemp).Tables(0)
                    Dim dQuantity As Long = 0
                    Dim dAmount As String
                    Dim dRate As Decimal = 0
                    If mkDefaultValues(grdSalesRcpt.CurrentRow.Index, 3) <> Nothing Then
                        dQuantity = grdSalesRcpt.CurrentRow.Cells("cQty").Value
                    End If
                    If mkDefaultValues(grdSalesRcpt.CurrentRow.Index, 4) <> Nothing Then
                        dRate = grdSalesRcpt.CurrentRow.Cells("cRate").Value
                    End If

                    If e.ColumnIndex = cUnit Then
                        If sItemTemp <> "" Then
                            Try
                                'If dRate <> dtItemTemp.Rows(0)("fdPrice") Then
                                '    Dim sWarn As String
                                '    Dim msgWarn As DialogResult
                                '    sWarn = "You have changed the price for: "
                                '    sWarn &= sItemTemp & vbCrLf & "Do you want to update the item with the new cost?"
                                '    msgWarn = MessageBox.Show(sWarn, "Item's Price Changed", MessageBoxButtons.YesNo)
                                '    If msgWarn = Windows.Forms.DialogResult.Yes Then
                                '        updateItem(sKeyItemTemp, dRate)
                                '    End If
                                'End If
                                dAmount = dRate * dQuantity
                                grdSalesRcpt.CurrentRow.Cells("cAmount").Value = dAmount
                            Catch ex As Exception
                            End Try
                        End If
                    End If
                    If e.ColumnIndex = cItem Then
                        If sItemTemp <> "" Then
                            Try
                                grdSalesRcpt.CurrentRow.Cells("cDescription").Value = dtItemTemp.Rows(0)("fcItemDescription")
                                'grdSalesRcpt.CurrentRow.Cells(cUnit).Value = dtItemTemp.Rows(0)("fcUnitAbbreviation")
                                grdSalesRcpt.CurrentRow.Cells("cRate").Value = dtItemTemp.Rows(0)("fdPrice")
                                '     grdSalesRcpt.CurrentRow.Cells(cTax).Value = dtItemTemp.Rows(0)("fcSalesTaxCodeName")
                            Catch
                                MessageBox.Show(Err.Description, "Sales Receipt. Item")
                            End Try
                        End If
                    End If
                    If e.ColumnIndex = cQuantity Then
                        Try
                            'Dim dQuantityTemp As Long
                            'Dim sWarnTemp As String
                            'Dim sOnHand As String
                            'Dim sOnSR As String

                            'sOnHand = dtItemTemp.Rows(0)("fdOnHand").ToString
                            'sOnSR = dtItemTemp.Rows(0)("fdQuantityOnSO").ToString
                            'dQuantityTemp = sOnHand - sOnSR

                            'If dQuantityTemp < dQuantity Then

                            '    sWarnTemp = "You don't have sufficient quantity available to sell " _
                            '         & dQuantity & " of the item: " _
                            '         & vbCrLf & grdSalesRcpt.CurrentRow.Cells(cItem).Value & vbCrLf & vbCrLf _
                            '         & "Quantity on hand" & vbTab & vbTab & vbTab & vbTab _
                            '         & sOnHand.PadLeft(20 - Len(sOnHand)) & vbCrLf _
                            '         & "Quantity on other Sales Order" & vbTab & vbTab _
                            '         & sOnSR.PadLeft(20 - Len(sOnSR)) & vbCrLf & vbCrLf _
                            '         & "___________________________________________________" & vbCrLf _
                            '         & "Quantity available" & vbTab & vbTab & vbTab & vbTab _
                            '         & Format(dQuantityTemp, "0.00").PadLeft(20 - Len(Format(dQuantityTemp, "0.00")))

                            '    MessageBox.Show(sWarnTemp, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                            'End If
                            dAmount = CDec(grdSalesRcpt.CurrentRow.Cells("cQty").Value) * CDec(grdSalesRcpt.CurrentRow.Cells("cRate").Value)
                            grdSalesRcpt.CurrentRow.Cells("cAmount").Value = CDec(dAmount.ToString)
                        Catch
                            MessageBox.Show(Err.Description, "Data Validation")
                        End Try

                    End If

                    ' If mkDefaultValues(e.RowIndex, cAmount) <> Nothing Then
                    Call total()
                    'End If

                Catch ex As Exception
                    MessageBox.Show(Err.Description, "Sales Receipt Item")
                End Try
            End If
        End If
    End Sub
    Private Sub grdSalesRcpt_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdSalesRcpt.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdSalesRcpt.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub
    Private Sub grdSalesRcpt_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdSalesRcpt.UserDeletedRow
        Call total()
    End Sub
    Private Sub grdSalesRcpt_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdSalesRcpt.UserDeletingRow
        If grdSalesRcpt.Rows.Count > 0 Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                ReDim Preserve sKeyToDel(iDel)
                sKeyToDel(iDel) = e.Row.Cells(cKeySRItem).Value.ToString
                deleteSRItem(sKeyToDel(iDel))
                iDel += 1
            Else
                e.Cancel = True
            End If
        End If
    End Sub
    Private Sub grdSalesRcpt_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdSalesRcpt.EditingControlShowing
        Dim comboBoxColumn As DataGridViewComboBoxColumn = grdSalesRcpt.Columns("cItem")
        If (grdSalesRcpt.CurrentCellAddress.X = comboBoxColumn.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
                cb.DropDownWidth = 400
            End If
        End If
    End Sub
    Private Sub lblSaleDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSaleDiscount.TextChanged
        Call calculatedueamount()
    End Sub
    Private Sub lblTaxAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTaxAmt.TextChanged
        Call total()
    End Sub
    Private Sub lblTotal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTotal.TextChanged
        calculatedueamount()
    End Sub
    Private Sub lblRetDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblRetDiscount.TextChanged
        Call calculatedueamount()
    End Sub

#End Region

    Private Function defaultInvoiceNo() As String
        Dim sDefault As String = "1"
        Dim sInvc As String = ""
        Dim sInvcNum As String = ""
        Dim sSCLCmd As String = "SELECT fcSaleNo FROM tSalesReceipt where fxKeyCompany ='" & gCompanyID()
        sSCLCmd &= "' AND LEN(fcSaleNo) < 10"
        sSCLCmd &= " ORDER BY CAST(fcSaleNo AS INTEGER)"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    sInvc = rd.Item(0).ToString
                End While
            End Using

            If sInvc = "" Or sInvc = "0" Then
                sInvcNum = sDefault
            ElseIf sInvc <> "" Or sInvc <> "0" Then
                sInvcNum = CInt(sInvc) + 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        Return sInvcNum
    End Function
    Private Function defaultSRNo() As String
        Dim sDefault As String = "1"
        Dim sSRNoFetch As String = ""
        Dim sSRNo() As String = {""}
        Dim sSRNosTemp As String = ""
        Dim i As Integer = 0

        Dim sSCLCmd As String = "SELECT fcSaleNo FROM tSalesReceipt ORDER BY fcSaleNo"
        Try
            Using rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    If IsNumeric(rd.Item("fcSaleNo")) Then
                        sSRNoFetch = rd.Item("fcSaleNo")
                        sSRNosTemp &= sSRNoFetch & " "
                    End If
                End While
            End Using

            sSRNo = sSRNosTemp.Split
            If UBound(sSRNo) > 0 Then
                For i = 0 To UBound(sSRNo)
                    If CStr(i + 1) <> sSRNo(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sSRNo(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next

            End If
            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Sales Receipt No.")
            Return sDefault
        End Try
    End Function
    Private Sub refreshForm()
        Dim dtComp As DataRow = gCompanyInfo().Tables(0).Rows(0)
        sCoAddress = dtComp("co_name").ToString + _
                         vbCrLf + dtComp("co_street").ToString + _
                        " " + dtComp("co_city").ToString + _
                        vbCrLf + dtComp("co_zip").ToString

        If sKeySR = "" Then
            sKeySR = Guid.NewGuid.ToString
        End If

        txtSoldTo.Text = sBillToInitial
        lblTotal.Text = Format(dTotalInitial, "0.00")
        lblTaxAmt.Text = Format(dTaxAmntInitial, "0.00")
        createColumn()
    End Sub
    Private Sub createColumn()
        grdSalesRcpt.Columns.Clear()

        Dim colKeySRItem As New DataGridViewTextBoxColumn
        Dim colItem As New DataGridViewComboBoxColumn
        Dim colDescription As New DataGridViewTextBoxColumn
        Dim colQuantity As New DataGridViewTextBoxColumn
        Dim colUnit As New DataGridViewTextBoxColumn
        Dim colRate As New DataGridViewTextBoxColumn
        Dim colAmount As New DataGridViewTextBoxColumn
        Dim colSelect As New DataGridViewCheckBoxColumn
        Dim dtItem As DataTable = m_GetItem.Tables(0)
        'Dim dtSR As DataTable = loadSRItem(sKeySR).Tables(0)

        Try
            colKeySRItem.Name = "cKeyItem"
            With colItem
                .Name = "cItem"
                .HeaderText = "Item"
                .DataPropertyName = "fcItemName"
                .DataSource = dtItem.DefaultView
                .DisplayMember = "fcItemName"
                .ValueMember = "fcItemName"
                .DropDownWidth = 250
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            End With

            colDescription.HeaderText = "Description"
            colDescription.Name = "cDescription"

            colQuantity.HeaderText = "Ordered"
            colQuantity.Name = "cQty"

            colRate.HeaderText = "Rate"
            colRate.Name = "cRate"

            colAmount.HeaderText = "Amount"
            colAmount.Name = "cAmount"

            With colSelect
                .HeaderText = "Select"
                .Name = "cSelect"
            End With


            With grdSalesRcpt
                .Columns.Clear()

                .Columns.Add(colKeySRItem)
                .Columns.Add(colItem)
                .Columns.Add(colDescription)
                .Columns.Add(colQuantity)

                .Columns.Add(colRate)
                .Columns.Add(colAmount)
                .Columns.Add(colSelect)

                .Columns(0).Visible = False
                .Columns(cUnit).ReadOnly = False
                .Columns("cSelect").Width = 50

                .Columns("cItem").Width = 255
                .Columns("cDescription").Width = 150
                .Columns("cQty").Width = 80
                .Columns("cRate").Width = 120
                .Columns("cAmount").Width = 120
                '  .Columns("cSelect").Width = 50
            End With

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Sales Receipt Item")
        End Try

    End Sub
    Private Sub loadcomboboxs()
        Call m_loadCustomer(cboCustomerName, cboCustomerID)
        Call m_loadSalesTaxCode(cboTaxCodeName, cboTaxCodeID)
        Call m_loadPaymentMethod(cboPaymentMethodName, cboPaymentMethodID)
        Call m_loadTax(cboTaxName, cboTaxID)
        Call m_DisplayAccountsAll(cboPayAcnt)
        Call m_DisplayAccountsAll(cboTaxAcnt)
        Call loaddiscounts()
    End Sub
    Private Sub loaddiscounts()
        Call m_GetSalesDiscount(cboSD, Me)
        Call m_GetRetailerDiscount(cboRD, Me)
    End Sub
    Private Sub loadsalesreceipts()
        Call loadSR()
        Call m_selectedCboValue(cboCustomerName, cboCustomerID, sKeyCustomer)
        Call m_selectedCboValue(cboTaxCodeName, cboTaxCodeID, sKeySalesTaxCode)
        Call m_selectedCboValue(cboTaxName, cboTaxID, sKeyTax)
        Call m_selectedCboValue(cboPaymentMethodName, cboPaymentMethodID, sKeyPaymentMethod)
    End Sub
    Private Sub loadSR()
        Dim dtSR As DataSet = m_GetSR(sKeySR)

        Try
            Using rd As DataTableReader = dtSR.CreateDataReader
                If rd.Read Then
                    Call loadReceiptItems()
                    total()

                    If IsDBNull(rd.Item("fxKeyCustomer")) Then
                        MsgBox("Customer Field is empty. Please indicate the name " & _
                                "of the Customer.", MsgBoxStyle.Information, "Notice")
                        cboCustomerName.Focus()
                    End If

                    sKeyCustomer = rd.Item("fxKeyCustomer").ToString
                    sKeySalesTaxCode = rd.Item("fxKeySalesTaxCode").ToString
                    sKeyPaymentMethod = rd.Item("fxKeyPaymentMethod").ToString
                    sKeyTax = rd.Item("fxKeyTax").ToString
                    txtSaleNo.Text = rd.Item("fcSaleNo")
                    dteSale.Value = rd.Item("fdDateTransact")
                    dteSalesPeriodFROM.Value = rd.Item("fdSalesPeriodFrom")
                    dteSalesPeriodTo.Value = rd.Item("fdSalesPeriodTo")
                    txtCheckNo.Text = IIf(IsDBNull(rd.Item("fcCheckNo")), "", rd.Item("fcCheckNo"))
                    txtCustomerMsg.Text = IIf(IsDBNull(rd.Item("fcCustomerMessage")), "", rd.Item("fcCustomerMessage"))
                    dPercentage = rd.Item("fdTaxRate")
                    lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
                    dTotalInitial = rd.Item("fdTotal")
                    dTaxAmntInitial = rd.Item("fdTaxAmount")
                    sBillToInitial = IIf(IsDBNull(rd.Item("fcBillTo")), "", rd.Item("fcBillTo"))
                    txtMemo.Text = IIf(IsDBNull(rd.Item("fcMemo")), "", rd.Item("fcMemo"))
                    chkPrint.Checked = rd.Item("fbPrint")
                    chkEmail.Checked = rd.Item("fbEmail")
                    lblSaleDiscount.Text = Format(CDec(rd.Item("fdSDAmt")), "##,##0.00")
                    lblRetDiscount.Text = Format(CDec(rd.Item("fdRDAmt")), "##,##0.00")
                    cboSD.SelectedItem = rd.Item("fcSDCode")
                    cboRD.SelectedItem = rd.Item("fcRDCode")

                    If rd.Item("fxKeyPayAcnt").ToString <> "" Then
                        cboPayAcnt.SelectedItem = m_GetAccountName(rd.Item("fxKeyPayAcnt").ToString)
                    End If
                    If rd.Item("fxKeyTaxAcnt").ToString <> "" Then
                        cboTaxAcnt.SelectedItem = m_GetAccountName(rd.Item("fxKeyTaxAcnt").ToString)
                    End If
                    txtPercent.Text = Format(CDec(rd.Item("fdTaxPercent")), "#0.00")

                    lblBalance.Text = Format(CDec(rd.Item("fdBalanceDue")), "##,##0.00")
                    If lblBalance.Text = "0.00" Then
                        calculatedueamount()
                    End If
                    ClassRefNo = rd.Item("fxClassRefNo").ToString
                    TxtClassRefno.Text = ClassRefNo
                    txtClassName.Text = GetClassName(ClassRefNo)
                Else
                    sKeyCustomer = ""
                    sKeySalesTaxCode = ""
                    sKeyPaymentMethod = ""
                    sKeyTax = ""
                    txtSaleNo.Text = defaultSRNo()
                    dteSale.Value = Now
                    txtCheckNo.Text = ""
                    txtCustomerMsg.Text = ""
                    dPercentage = 0
                    lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
                    dTotalInitial = 0
                    dTaxAmntInitial = 0
                    sBillToInitial = ""
                    txtMemo.Text = ""
                    chkPrint.Checked = True
                    chkEmail.Checked = False

                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Sales Receipt")
        End Try
    End Sub
    Private Sub loadReceiptItems()
        'Call createColumn()
        Call loadRecpItems()
    End Sub
    Private Sub loadRecpItems()
        Dim sSQLCmd As String = "usp_t_salesReceiptItem_list "
        sSQLCmd &= "@fxKeySR = '" & sKeySR & "'"
        With Me.grdSalesRcpt
            Try
                Dim dtsTable As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                Dim xRow As Integer = 0
                For xRow = 0 To dtsTable.Tables(0).Rows.Count - 1
                    .Rows.Add()
                    .Item("cKeyItem", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fxKeySRItem").ToString
                    .Item("cItem", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("Item").ToString
                    .Item("cQty", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("Qty")
                    .Item("cDescription", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("Order")
                    .Item("cRate", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("Rate")), "##,##0.00##")
                    .Item("cAmount", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("Amount")), "##,##0.00##")
                Next
                'MsgBox(.Rows(0).Cells("cItem").Value.ToString, MsgBoxStyle.Information)
            Catch ex As Exception
                MessageBox.Show(Err.Description, "Load Sales Receipt Items")
            End Try
        End With
    End Sub
    Private Function loadSRItem(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = "SELECT tSalesReceipt_item.fxKeySRItem, "
        sSQLCmd &= "mItem00Master.fcItemName, "
        sSQLCmd &= "tSalesReceipt_item.fcSRDescription, "
        sSQLCmd &= "tSalesReceipt_item.fdQuantity, "
        sSQLCmd &= "mItme04MeasurementUsed.fcUnitAbbreviation, "
        sSQLCmd &= "tSalesReceipt_item.fdRate, "
        sSQLCmd &= "tSalesReceipt_item.fdAmount, "
        sSQLCmd &= "mSalesTaxCode.fcSalesTaxCodeName "
        sSQLCmd &= "FROM tSalesReceipt_item "
        sSQLCmd &= "INNER JOIN mItem00Master "
        sSQLCmd &= "ON mItem00Master.fxKeyItem = tSalesReceipt_item.fxKeyItem "
        sSQLCmd &= "LEFT JOIN mSalesTaxCode "
        sSQLCmd &= "ON mSalesTaxCode.fxKeySalesTaxCode = tSalesReceipt_item.fxKeySalesTaxCode "
        sSQLCmd &= "LEFT JOIN  mItme04MeasurementUsed "
        sSQLCmd &= "ON mItme04MeasurementUsed.fxKeyUnitUsed = mItem00Master.fxKeyUnitUsed "
        sSQLCmd &= "WHERE tSalesReceipt_item.fxKeySR = '" & sKeySR & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Sales Receipt Items")
            Return Nothing
        End Try
    End Function

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem

        If cboCustomerName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
            frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.IsNew = True
            frm_cust_masterCustomerAddEdit.Text = "Add Customer"
            frm_cust_masterCustomerAddEdit.Show()
        End If

        If sKeyCustomer <> "" Then
            Dim dtCustomer As DataTable = m_GetCustomer(sKeyCustomer).Tables(0)
            txtSoldTo.Text = dtCustomer.Rows(0)("fcBillingAddress").ToString

            cboTaxCodeID.SelectedItem = dtCustomer.Rows(0)("fxKeySalesTaxCode").ToString
            cboTaxCodeName.SelectedIndex = cboTaxCodeID.SelectedIndex
            cboPaymentMethodID.SelectedItem = dtCustomer.Rows(0)("fxKeyPaymentMethod").ToString
            cboPaymentMethodName.SelectedIndex = cboPaymentMethodID.SelectedIndex
        End If
    End Sub
    Private Sub cboPaymentMethodName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMethodName.SelectedIndexChanged
        cboPaymentMethodID.SelectedIndex = cboPaymentMethodName.SelectedIndex
        sKeyPaymentMethod = cboPaymentMethodID.SelectedItem
        If cboPaymentMethodName.SelectedIndex = 1 Then
            Dim x As New frm_MF_paymentMethodAddEdit
            x.ShowDialog()
        End If
    End Sub
    Private Sub cboTaxName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxName.SelectedIndexChanged
        cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        sKeyTax = cboTaxID.SelectedItem.ToString

        Call computeTax()
        computesalesdiscount()
        computeretailersdiscount()

        If cboTaxName.SelectedIndex = 1 Then
            frm_MF_taxAddEdit.Text = "New Sales Tax"
            frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
            frm_MF_taxAddEdit.Show()
        End If
        LblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)

    End Sub

    Private Sub computeTax()
        Dim dAmt As Decimal = 0

        If sKeyTax <> "" Then
            Dim dtTax As DataSet = m_GetTax(sKeyTax)
            dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        Else
            dPercentage = 0
        End If
        lblPercentage.Text = Format(CDec(dPercentage), "0.00")

        With Me.grdSalesRcpt
            Dim xCnt As Integer = 0
            Dim xTotal As Decimal = 0
            For xCnt = 0 To .RowCount - 1
                If .Item("cDescription", xCnt).Value IsNot Nothing Then
                    dAmt = CDec(.Item("cAmount", xCnt).Value)
                    xTotal += dAmt
                End If
            Next
            lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "##,##0.00")
        End With
    End Sub
    Private Sub updateSR()
        Dim msgWarning As DialogResult
        Dim sWarning As String
        Dim dtAddress As DataTable

        Dim sSQLCmd As String = "usp_t_salesReceipt_update "
        sSQLCmd &= "@fxKeySR = '" & sKeySR & "'"
        sSQLCmd &= ",@fcSaleNo = '" & txtSaleNo.Text & "'"
        sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"
        sSQLCmd &= ",@fdDateTransact = '" & Microsoft.VisualBasic.FormatDateTime(dteSale.Value, DateFormat.ShortDate) & "'"
        sSQLCmd &= ",@fcBillTo ='" & txtSoldTo.Text & "'"
        sSQLCmd &= ",@fdSalesPeriodFrom = '" & dteSalesPeriodFROM.Value & "'"
        sSQLCmd &= ",@fdSalesPeriodTo = '" & dteSalesPeriodTo.Value & "'"

        dtAddress = m_GetCustomer(sKeyCustomer).Tables(0)
        Dim sAddressTemp As String = dtAddress.Rows(0)("fcBillingAddress").ToString
        If Trim(txtSoldTo.Text) <> Trim(sAddressTemp) Then
            sWarning = "Billing address for Customer: "
            sWarning &= cboCustomerName.SelectedItem
            sWarning &= " is different from the master file." & vbCrLf
            sWarning &= "Would you like to have this new information appear next time?"
            msgWarning = MessageBox.Show(sWarning, "New Information Changed", MessageBoxButtons.YesNoCancel)
            If msgWarning = Windows.Forms.DialogResult.Yes Then
                updateBillAddress()
            ElseIf msgWarning = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            End If
        End If

        Dim pxPayAccount As String = ""
        Dim pxTaxAccount As String = ""

        If cboPayAcnt.SelectedItem <> " " Then
            pxPayAccount = getAccountID(cboPayAcnt.SelectedItem)
        End If

        If cboTaxAcnt.SelectedItem <> " " Then
            pxTaxAccount = getAccountID(cboTaxAcnt.SelectedItem)
        End If

        sSQLCmd &= ",@fxKeyPayAcnt =" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
        sSQLCmd &= ",@fxKeyTaxAcnt =" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
        sSQLCmd &= ",@fxKeySalesAcnt =" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
        sSQLCmd &= ",@fdTaxPercent ='" & gTaxPercent & "' "
        sSQLCmd &= ",@fxOutPutTaxAcnt =" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
        sSQLCmd &= ",@fdOutputTaxpercent ='" & gOutputTaxPercent & "' "
        sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
        sSQLCmd &= ",@fcCheckNo = '" & txtCheckNo.Text & "'"
        sSQLCmd &= ",@fcCustomerMessage = '" & txtCustomerMsg.Text & "'"
        sSQLCmd &= ",@fcMemo = '" & txtMemo.Text & "'"
        sSQLCmd &= ",@fdTaxAmount ='" & CDec(lblTaxAmt.Text) & "' "
        sSQLCmd &= ",@fdTotal ='" & CDec(lblTotal.Text) & "' "
        sSQLCmd &= ",@fbPrint ='" & IIf(chkPrint.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fbEmail ='" & IIf(chkEmail.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fcSDCode ='" & cboSD.SelectedItem & "' "
        sSQLCmd &= ",@fcRDCode ='" & cboRD.SelectedItem & "' "
        sSQLCmd &= ",@fdSDAmt ='" & CDec(lblSaleDiscount.Text) & "' "
        sSQLCmd &= ",@fdRDAmt ='" & CDec(lblRetDiscount.Text) & "' "
        sSQLCmd &= ",@fdSDAccountAmt='" & LblSalesDiscountAmount.Text & "' "
        sSQLCmd &= ",@fdBalanceDue='" & CDec(lblBalance.Text) & "' "
        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "' "

        If sKeySalesTaxCode <> "" Then
            sSQLCmd &= ",@fxKeySalesTaxCode = '" & sKeySalesTaxCode & "' "
        End If
        If sKeyTax <> "" Then
            sSQLCmd &= ",@fxKeyTax = '" & sKeyTax & "'"
        End If
        If sKeyPaymentMethod <> "" Then
            sSQLCmd &= ",@fxKeyPaymentMethod = '" & sKeyPaymentMethod & "'"
        End If

        sSQLCmd &= ", @fxKeyClassRefNo='" & ClassRefNo & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            gKeySR = sKeySR
            If updateSOItem() Then
                MessageBox.Show("Record successfully Updated.", "Add/Edit S.Receipt.")
                frm_cust_masterCustomer.loadGrdCustomer2("Sales Receipts")
                frm_item_availability.loadDetails("Sales Receipts")
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Sales Receipt")
        End Try
    End Sub
    Private Function updateSOItem() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Dim sKeySRItem As String
        Dim sItem As String = ""
        Dim bClosed As Boolean = False
        Dim sDescription As String = ""
        Dim dQuantity As Long = 0
        Dim dRate As Decimal
        Dim dAmount As Decimal
        Dim sKeyTaxCode As String = ""
        Try
            If Me.grdSalesRcpt.Rows.Count > 0 Then
                For iRow = 0 To (grdSalesRcpt.RowCount - 1)
                    If mkDefaultValues(iRow, 1) <> Nothing Then
                        If mkDefaultValues(iRow, 0) <> Nothing Then
                            sKeySRItem = grdSalesRcpt.Rows(iRow).Cells(0).Value.ToString
                        Else
                            sKeySRItem = Guid.NewGuid.ToString
                            grdSalesRcpt.Rows(iRow).Cells(0).Value = sKeySRItem
                        End If
                        If mkDefaultValues(iRow, 1) <> Nothing Then
                            sItem = grdSalesRcpt.Rows(iRow).Cells(1).Value.ToString
                            sItem = m_GetItemByName(sItem)
                        End If
                        If mkDefaultValues(iRow, 2) <> Nothing Then
                            sDescription = grdSalesRcpt.Rows(iRow).Cells("cDescription").Value
                        End If
                        If mkDefaultValues(iRow, 3) <> Nothing Then
                            dQuantity = grdSalesRcpt.Rows(iRow).Cells("cQty").Value
                        Else
                            dQuantity = 0
                        End If
                        If mkDefaultValues(iRow, 4) <> Nothing Then
                            dRate = grdSalesRcpt.Rows(iRow).Cells("cRate").Value
                        Else
                            dRate = 0
                        End If
                        If mkDefaultValues(iRow, 5) <> Nothing Then
                            dAmount = grdSalesRcpt.Rows(iRow).Cells("cAmount").Value
                        Else
                            dAmount = 0
                        End If

                        sSQLCmd = "usp_t_salesReceiptItem_update "
                        sSQLCmd &= "@fxKeySRItem = '" & sKeySRItem & "'"
                        sSQLCmd &= ",@fxKeySR = '" & sKeySR & "'"
                        sSQLCmd &= ",@fxKeyItem = '" & sItem & "'"
                        sSQLCmd &= ",@fcSRDescription = '" & sDescription & "'"
                        sSQLCmd &= ",@fdQuantity ='" & dQuantity & "' "
                        sSQLCmd &= ",@fdRate ='" & dRate & "' "
                        sSQLCmd &= ",@fdAmount ='" & dAmount & "' "

                        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                    End If
                Next
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Sales Receipt Item")
            Return False
        End Try
    End Function
    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdSalesRcpt.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdSalesRcpt.Rows(irow).Cells(iCol).Value)
    End Function
    Private Sub updateBillAddress()
        Dim sSQLCmd As String = "UPDATE mAddress SET fcAddress = '" & txtSoldTo.Text & "' "
        sSQLCmd &= "WHERE fxKeyID = '" & sKeyCustomer & "' AND "
        sSQLCmd &= "fbCustomer = 1 AND fcAddressName = ''"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Billing Address")
        End Try
    End Sub
    Private Sub updateItem(ByVal sKey As String, ByVal nPrice As Long)
        Dim sSQLCmd As String = "UPDATE mItem00Master "
        sSQLCmd &= "SET fdPrice = " & nPrice
        sSQLCmd &= " WHERE fxKeyItem = '" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Item Price")
        End Try
    End Sub
    Private Function psGetTotal() As Decimal
        Dim xRow As Integer
        Dim xTotal As Integer = 0
        With Me.grdSalesRcpt
            For xRow = 0 To .Rows.Count - 1
                If Not .Rows(xRow).Cells(3).Value Is Nothing Then
                    xTotal += .Rows(xRow).Cells(7).Value
                End If
            Next
        End With
    End Function
    Private Sub deleteSRItem(ByVal SRItemUniqueID As String)
        Try
            Dim deleteCommand As String = "DELETE FROM tSalesReceipt_item WHERE fxKeySRItem = '" & SRItemUniqueID & "'"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, deleteCommand)
            MessageBox.Show("Item Successfully Deleted!", "Delete Sales Receipt Item")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Sales Receipt Item")
        End Try
    End Sub

    Private Sub computesalesdiscount()
        Try
            If lblTotal.Text <> "0.00" Then
                lblSaleDiscount.Text = Format(CDec(lblTotal.Text * (CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)) / 100)), "##,##0.00")
            Else
                lblSaleDiscount.Text = "0.00"
            End If
        Catch ex As Exception
            lblSaleDiscount.Text = "0.00"
        End Try

    End Sub
    Private Sub computeretailersdiscount()
        Try
            If lblTotal.Text <> "0.00" Then
                lblRetDiscount.Text = Format(CDec(CDec(lblTotal.Text) - CDec(lblSaleDiscount.Text)) * (CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)) / 100), "##,##0.00")
            Else
                lblRetDiscount.Text = "0.00"
            End If
        Catch ex As Exception
            lblRetDiscount.Text = "0.00"
        End Try

    End Sub
    Private Sub calculatedueamount()
        lblBalance.Text = CDec(IIf(lblTotal.Text = "", 0, lblTotal.Text)) - (CDec(IIf(lblSaleDiscount.Text = "", 0, lblSaleDiscount.Text)) + CDec(IIf(lblRetDiscount.Text = "", 0, lblRetDiscount.Text))) '+ CDec(lblPayments.Text))
        lblBalance.Text = Format(CDec(lblBalance.Text), "#,##0.00")
    End Sub
    Private Sub total()

        Call computeTax()
        With Me.grdSalesRcpt
            Dim xCnt As Integer = 0
            Dim xTotal As Decimal = 0
            For xCnt = 0 To .RowCount - 1
                If .Item("cDescription", xCnt).Value IsNot Nothing Then
                    xTotal += CDec(.Item("cAmount", xCnt).Value)
                End If
            Next
            lblTotal.Text = Format(CDec(xTotal + CDec(lblTaxAmt.Text)), "#,##0.00")
        End With

    End Sub

    Private Sub IsDateRestricted()
        If checkDate.checkIfRestricted(dteSale.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
            btnAccountability.Enabled = False
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
            btnAccountability.Enabled = True
        End If
    End Sub

#Region "Disabled Codes"
    'Private Function getTotal() As Decimal
    '    Dim iRow As Integer
    '    Dim dTotal As Decimal = 0
    '    Dim sKeyItm As String

    '    Try
    '        For iRow = 0 To grdSalesRcpt.Rows.Count - 2
    '            sKeyItm = grdSalesRcpt.Item(cItem, iRow).Value.ToString
    '            sKeyItm = m_GetItemByName(sKeyItm)
    '            If sKeyCustomer <> "" Then
    '                '    If isTaxable(1) Then
    '                '        If isTaxable(0, iRow) Then
    '                '            dTotalTx = dTotalTx + grdSalesRcpt.Item(cAmount, iRow).Value
    '                '        Else
    '                '            dTotalNTx = dTotalNTx + grdSalesRcpt.Item(cAmount, iRow).Value
    '                '        End If
    '                '    Else
    '                '        dTotalNTx = dTotalNTx + grdSalesRcpt.Item(cAmount, iRow).Value
    '                '    End If
    '                'Else
    '                '    If isTaxable(0, iRow) Then
    '                '        dTotalTx = dTotalTx + grdSalesRcpt.Item(cAmount, iRow).Value
    '                '    Else
    '                '        dTotalNTx = dTotalNTx + grdSalesRcpt.Item(cAmount, iRow).Value
    '                '    End If
    '            End If
    '        Next
    '        'dTax = (dTotalTx * (dPercentage / 100))
    '        'dTotal = dTotalTx + dTotalNTx + dTax

    '        lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
    '        ' lblTaxAmt.Text = Format(dTax, "0.00")
    '        lblTotal.Text = Format(CLng(dTotal), "0.00")

    '        Return dTotal
    '    Catch
    '        Return 0
    '    End Try
    'End Function
#End Region

    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        txtClassName.Text = FrmBrowseClass.Data1
        TxtClassRefno.Text = FrmBrowseClass.Data2
        ClassRefNo = TxtClassRefno.Text
    End Sub

    Private Sub cboTaxID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTaxID.SelectedIndexChanged

    End Sub

    Private Sub grdSalesRcpt_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSalesRcpt.CellContentClick

    End Sub
End Class