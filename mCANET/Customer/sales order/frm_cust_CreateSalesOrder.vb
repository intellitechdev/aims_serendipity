Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient

Imports System.Net.Mail
Public Class frm_cust_CreateSalesOrder
    Private gCon As New Clsappconfiguration

    Private Const cKeySOItem = 0
    Private Const cItem = 1
    Private Const cDescription = 2
    Private Const cQuantity = 3
    Private Const cUnit = 4
    Private Const cRate = 5
    Private Const cAmount = 6
    ' Private Const cTax = 7
    Private Const cInvoiced = 7
    Private Const cClose = 8

    Private bIsNew As Boolean
    Private sKeySO As String
    Private sKeyCustomer As String
    Private sKeySalesTaxCode As String
    Private sKeyTax As String
    Private sKeyShipAdd As String

    Private dPercentage As Long

    Private sBillAddInitial As String = ""
    Private sShipAddInitial As String = ""
    Private sKeyShipAddInitial As String = ""
    Private sKeySalesTaxCodeInitial As String
    Private dTotalInitial As Long = 0
    Private dTaxAmntInitial As Long = 0

    Private sKeyToDel() As String = {""}
    Private iDel As Integer = 0

    Private sCoAddress As String

    Dim checkDate As New clsRestrictedDate

    Public Property IsNew() As Boolean
        Get
            Return bIsNew
        End Get
        Set(ByVal value As Boolean)
            bIsNew = value
        End Set
    End Property
    Public Property KeySO() As String
        Get
            Return sKeySO
        End Get
        Set(ByVal value As String)
            sKeySO = value
            refreshForm()
        End Set
    End Property
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        sKeySO = ""
        Me.Close()
    End Sub
    Private Sub frm_cust_CreateSalesOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sKeySO = "" Or sKeySO = Nothing Then
            sKeySO = Guid.NewGuid.ToString
            Me.Text = "New Sales Order"
            bIsNew = True
            refreshForm()
            IsDateRestricted()
        Else
            refreshForm()
            gEditEvent = 1
            loadSO()
            IsDateRestricted()
        End If

    End Sub
    Private Sub grdSO_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSO.CellClick
        gEditEvent = 0
    End Sub
    Private Sub refreshForm()
        Dim dsComp As DataSet = gCompanyInfo()
        If dsComp IsNot Nothing Then

            Dim dtComp As DataRow = dsComp.Tables(0).Rows(0)
            sCoAddress = dtComp("co_name").ToString + _
                             vbCrLf + dtComp("co_street").ToString + _
                            " " + dtComp("co_city").ToString + _
                            vbCrLf + dtComp("co_zip").ToString

            m_loadCustomer(cboCustomerName, cboCustomerID)
            m_loadTax(cboTaxName, cboTaxID)
            m_loadSalesTaxCode(cboTaxCodeName, cboTaxCodeID)

            m_selectedCboValue(cboCustomerName, cboCustomerID, sKeyCustomer)
            m_selectedCboValue(cboTaxCodeName, cboTaxCodeID, sKeySalesTaxCodeInitial)
            m_selectedCboValue(cboTaxName, cboTaxID, sKeyTax)

            If sKeyCustomer <> "" Then
                m_loadShippingAddress(cboShipToAddName, cboShipToAddID, sKeyCustomer)
                m_selectedCboValue(cboShipToAddName, cboShipToAddID, sKeyShipAddInitial)
            End If

            txtBillingAdd.Text = sBillAddInitial
            txtShippingAdd.Text = sShipAddInitial
            lblTotal.Text = Format(dTotalInitial, "0.00")
            lblTaxAmt.Text = Format(dTaxAmntInitial, "0.00")
            createColumn()
        Else
            Me.Close()
        End If
    End Sub
    Private Sub createColumn()

        Dim colKeySOItem As New DataGridViewTextBoxColumn
        Dim colItem As New DataGridViewComboBoxColumn
        Dim colDescription As New DataGridViewTextBoxColumn
        Dim colQuantity As New DataGridViewTextBoxColumn
        Dim colUnit As New DataGridViewTextBoxColumn
        Dim colRate As New DataGridViewTextBoxColumn
        Dim colAmount As New DataGridViewTextBoxColumn
        Dim colTax As New DataGridViewComboBoxColumn
        Dim colInvoiced As New DataGridViewTextBoxColumn
        Dim colClosed As New DataGridViewCheckBoxColumn

        Dim dtItem As DataTable = m_GetItem.Tables(0)
        Dim dtTax As DataTable = m_GetSalesTaxCode.Tables(0)
        '  Dim dtSO As DataTable = loadSOItem(sKeySO).Tables(0)

        Try
            With colItem
                .Items.Clear()
                .HeaderText = "Item"
                .DataPropertyName = "fcItemName"
                .Name = "cItem"
                .DataSource = dtItem.DefaultView
                .DisplayMember = "fcItemName"
                .ValueMember = "fcItemName"
                .DropDownWidth = 200
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True
            End With

            ' grdSO.DataSource = dtSO.DefaultView

            colKeySOItem.Name = "fxKeySOItem"


            colDescription.HeaderText = "Description"
            colDescription.Name = "fcSODescription"

            colQuantity.HeaderText = "Ordered"
            colQuantity.Name = "fdQuantity"

            colUnit.HeaderText = "Unit"
            colUnit.Name = "fcUnitAbbreviation"

            colRate.HeaderText = "Rate"
            colRate.Name = "fdRate"

            colAmount.HeaderText = "Amount"
            colAmount.Name = "fdAmount"


            With colTax
                .HeaderText = "Tax"
                .DataPropertyName = "fcSalesTaxCodeName"
                .DataSource = dtTax.DefaultView
                .DisplayMember = "fcSalesTaxCodeName"
                .ValueMember = "fcSalesTaxCodeName"
            End With

            colInvoiced.HeaderText = "Invoiced"
            colInvoiced.Name = "fdQuantity"

            colClosed.HeaderText = "Closed"
            colClosed.Name = "fbClosed"

            With grdSO
                .Columns.Clear()

                .Columns.Add(colKeySOItem)
                .Columns.Add(colItem)
                .Columns.Add(colDescription)
                .Columns.Add(colQuantity)
                .Columns.Add(colUnit)
                .Columns.Add(colRate)
                .Columns.Add(colAmount)
                '.Columns.Add(colTax)
                .Columns.Add(colInvoiced)
                .Columns.Add(colClosed)

                .Columns(cKeySOItem).Visible = False
                .Columns(cUnit).ReadOnly = True

                If IsNew Then
                    .Columns(cInvoiced).Visible = False
                    .Columns(cClose).Visible = False
                End If

                .Columns(cItem).Width = 285
                .Columns(cDescription).Width = 120
                .Columns(cQuantity).Width = 70
                .Columns(cUnit).Width = 80
                .Columns(cRate).Width = 100
                .Columns(cAmount).Width = 120


                .Columns(cRate).ReadOnly = True
                .Columns(cRate).DefaultCellStyle.BackColor = Color.Gainsboro

                .Columns(cRate).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(cAmount).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(cRate).DefaultCellStyle.Format = "##,##0.0000"
                .Columns(cAmount).DefaultCellStyle.Format = "##,##0.0000"


            End With

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load S.O. Item")
        End Try

    End Sub
    Private Function loadSOItem(ByVal sKey As String) As DataSet
        Dim sSQLCmd As String = " SELECT tSalesOrder_Item.fxKeySOItem, "
        sSQLCmd &= "mItem00Master.fcItemName, "
        sSQLCmd &= "tSalesOrder_Item.fcSODescription, "
        sSQLCmd &= "tSalesOrder_Item.fdQuantity, "
        sSQLCmd &= "mItme04MeasurementUsed.fcUnitAbbreviation, "
        sSQLCmd &= "tSalesOrder_Item.fdRate, "
        sSQLCmd &= "tSalesOrder_Item.fdAmount, "
        sSQLCmd &= "mSalesTaxCode.fcSalesTaxCodeName, "
        sSQLCmd &= "tSalesOrder_Item.fdQuantity, "
        sSQLCmd &= "tSalesOrder_Item.fbClosed "
        sSQLCmd &= "FROM tSalesOrder_Item "
        sSQLCmd &= "INNER JOIN mItem00Master "
        sSQLCmd &= "ON mItem00Master.fxKeyItem = tSalesOrder_Item.fxKeyItem "
        sSQLCmd &= "LEFT JOIN mSalesTaxCode "
        sSQLCmd &= "ON mSalesTaxCode.fxKeySalesTaxCode = tSalesOrder_Item.fxKeySalesTaxCode "
        sSQLCmd &= "LEFT JOIN  mItme04MeasurementUsed "
        sSQLCmd &= "ON mItme04MeasurementUsed.fxKeyUnitUsed = mItem00Master.fxKeyUnitUsed "
        sSQLCmd &= "WHERE tSalesOrder_Item.fxKeySO = '" & sKeySO & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Sales Order Items")
            Return Nothing
        End Try
    End Function
    'Private Function loadSOItem(ByVal sKey As String) As DataSet
    '    Dim sSQLCmd As String = "usp_t_salesOrderItem_list "
    '    sSQLCmd &= "@fxKeySO = '" & sKey & "'"
    '    Try
    '        Return SqlHelper.ExecuteDataset(gCon.sqlconn, CommandType.Text, sSQLCmd)
    '    Catch ex As Exception
    '        MessageBox.Show(Err.Description, "Get S.O. Item")
    '        Return Nothing
    '    End Try
    'End Function
    Private Sub loadSO()
        Dim dtSO As DataSet = m_GetSO(sKeySO)
        Try
            Using rd As DataTableReader = dtSO.CreateDataReader
                '  Try
                If rd.Read Then
                    sBillAddInitial = rd.Item("fcBillTo")
                    sShipAddInitial = rd.Item("fcShipto")
                    txtSONo.Text = rd.Item("fcSONo")
                    txtPONo.Text = rd.Item("fcPoNo")
                    dteTransDate.Value = rd.Item("fdDateTransact")

                    sKeyCustomer = rd.Item("fxKeyCustomer").ToString
                    sKeyTax = rd.Item("fxKeyTax").ToString
                    sKeySalesTaxCodeInitial = rd.Item("fxKeySalesTaxCode").ToString
                    sKeyShipAddInitial = rd.Item("fxKeyShipAdd").ToString

                    dPercentage = rd.Item("fdTaxRate")
                    lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"

                    dTotalInitial = rd.Item("fdTotal")
                    dTaxAmntInitial = rd.Item("fdTaxAmount")
                    loadSO_Items()
                Else
                    txtSONo.Text = defaultSONo()
                    txtPONo.Text = ""
                    dteTransDate.Value = Now
                    dPercentage = 0
                    lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
                    lblTotal.Text = "0.00"
                    sBillAddInitial = ""
                    sShipAddInitial = ""
                    sKeyCustomer = ""
                    sKeyTax = ""
                    sKeySalesTaxCode = ""
                    sKeyShipAddInitial = ""
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        'Catch ex As Exception
        '    MessageBox.Show(Err.Description, "Load S.O.")
        'End Try
    End Sub
    Private Sub loadSO_Items()
        Dim sSQLCmd As String = " usp_t_SalesOrderItem_load "
        sSQLCmd &= " @fxKeySO='" & sKeySO & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        With Me.grdSO
            'Try
            Dim dtsTable As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            Dim xRow As Integer = 0
            For xRow = 0 To dtsTable.Tables(0).Rows.Count - 1
                .Rows.Add()
                '.Item(cKeySOItem, xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fxKeySOItem").ToString
                .Item(cQuantity, xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fdQuantity")
                .Item(cDescription, xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcSODescription")
                .Item(cItem, xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcItemName")
                .Item(cUnit, xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcUnitMeasurement")
                .Item(cRate, xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdRate")), "##,##0.00##")
                .Item(cAmount, xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdAmount")), "##,##0.00##")
                '.Item("cTax", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fdTaxCode")
            Next
            'Catch ex As Exception
            '    MessageBox.Show(Err.Description, "Load SO Items")
            'End Try
        End With
    End Sub
    Private Function defaultSONo() As String
        Dim sDefault As String = "1"
        Dim sPONoFetch As String = ""
        Dim sPONo() As String = {""}
        Dim sPONosTemp As String = ""
        Dim i As Integer = 0

        Dim sSCLCmd As String = "SELECT fcSONo FROM tSalesOrder ORDER BY fcSONo"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    If IsNumeric(rd.Item("fcSONo")) Then
                        sPONoFetch = rd.Item("fcSONo")
                        sPONosTemp &= sPONoFetch & " "
                    End If
                End While
            End Using

            sPONo = sPONosTemp.Split
            If UBound(sPONo) > 0 Then
                For i = 0 To UBound(sPONo)
                    If CStr(i + 1) <> sPONo(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sPONo(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next

            End If
            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "S.O. No.")
            Return sDefault
        End Try
    End Function
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        createColumn()
        iDel = 0
        sKeyToDel(iDel) = ""
    End Sub
    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem

        If cboCustomerName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
            frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.IsNew = True
            frm_cust_masterCustomerAddEdit.Text = "Add Customer"
            frm_cust_masterCustomerAddEdit.Show()
        End If

        If sKeyCustomer <> "" Then
            m_loadShippingAddress(cboShipToAddName, cboShipToAddID, sKeyCustomer, "Customer")

            Dim dtAddress As DataSet = m_getAddress(sKeyCustomer, "Customer")
            Try
                Using rdAddress As DataTableReader = dtAddress.CreateDataReader
                    If rdAddress.Read Then
                        txtShippingAdd.Text = rdAddress.Item("fcAddress")
                        cboShipToAddID.SelectedItem = rdAddress.Item("fxKeyAddress").ToString
                        cboShipToAddName.SelectedIndex = cboShipToAddID.SelectedIndex
                    Else
                        txtShippingAdd.Text = ""
                    End If
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Dim dtCustomer As DataTable = m_GetCustomer(sKeyCustomer).Tables(0)
            txtBillingAdd.Text = dtCustomer.Rows(0)("fcBillingAddress").ToString

            cboTaxCodeID.SelectedItem = dtCustomer.Rows(0)("fxKeySalesTaxCode").ToString
            cboTaxCodeName.SelectedIndex = cboTaxCodeID.SelectedIndex

        End If
    End Sub
    Private Sub cboShipToAddName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboShipToAddName.SelectedIndexChanged
        cboShipToAddID.SelectedIndex = cboShipToAddName.SelectedIndex
        sKeyShipAdd = cboShipToAddID.SelectedItem

        If cboShipToAddName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddress.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
            frm_cust_masterCustomerAddress.IsShippingAdd = True
            frm_cust_masterCustomerAddress.ShowDialog()
        End If

        If cboShipToAddName.SelectedIndex <> 1 And cboShipToAddName.SelectedIndex <> 0 Then
            If sKeyShipAdd <> "" Then
                Dim dtAddress As DataTable = m_getAddressByID(sKeyShipAdd).Tables(0)
                txtShippingAdd.Text = dtAddress.Rows(0)("fcAddress").ToString
            End If
        Else
            txtShippingAdd.Text = sCoAddress
        End If
    End Sub
    Private Sub cboTaxCodeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxCodeName.SelectedIndexChanged
        cboTaxCodeID.SelectedIndex = cboTaxCodeName.SelectedIndex
        sKeySalesTaxCode = cboTaxCodeID.SelectedItem.ToString

        getTotal()

        If cboTaxCodeName.SelectedIndex = 1 Then
            frm_vend_SalesTaxAdd.Text = "New Sales Tax Code"
            frm_vend_SalesTaxAdd.KeySalesTaxCode = Guid.NewGuid.ToString
            frm_vend_SalesTaxAdd.Show()
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If cboCustomerID.SelectedItem = "" Then
            MessageBox.Show("Customer field cannot be empty!", "Data Validation")
        Else
            updateSO()
        End If
    End Sub
    Private Sub updateSO()
        Dim msgWarning As DialogResult
        Dim sWarning As String
        Dim dtAddress As DataTable
        Dim sSQLCmd As String = "usp_t_salesOrder_update "
        sSQLCmd &= "@fxKeySO = '" & sKeySO & "'"
        sSQLCmd &= ",@fcSONo = '" & txtSONo.Text & "'"
        sSQLCmd &= ",@fcPONo = '" & txtPONo.Text & "'"
        sSQLCmd &= ",@fdDateTransact = '" & dteTransDate.Value & "'"
        sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"
        sSQLCmd &= ",@fcBillTo ='" & txtBillingAdd.Text & "'"

        dtAddress = m_GetCustomer(sKeyCustomer).Tables(0)
        Dim sAddressTemp As String = dtAddress.Rows(0)("fcBillingAddress").ToString
        If Trim(txtBillingAdd.Text) <> Trim(sAddressTemp) Then
            sWarning = "Billing address for Customer: "
            sWarning &= cboCustomerName.SelectedItem
            sWarning &= " is different from the master file." & vbCrLf
            sWarning &= "Would you like to have this new information appear next time?"
            msgWarning = MessageBox.Show(sWarning, "New Information Changed", MessageBoxButtons.YesNoCancel)
            If msgWarning = Windows.Forms.DialogResult.Yes Then
                updateBillAddress()
            ElseIf msgWarning = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            End If
        End If

        sSQLCmd &= ",@fcShipto = '" & txtShippingAdd.Text & "'"
        sSQLCmd &= ",@fcCustomerMessage = '" & txtCustomerMsg.Text & "'"
        sSQLCmd &= ",@fcMemo ='" & txtMemo.Text & "'"
        sSQLCmd &= ",@fdTaxAmount ='" & CDec(lblTaxAmt.Text) & "' "
        sSQLCmd &= ",@fdTotal ='" & CDec(lblTotal.Text) & "' "
        sSQLCmd &= ",@fdBalance =' " & CDec(lblTotal.Text) & "' "
        sSQLCmd &= ",@fbPrint ='" & IIf(chkPrint.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fbEmail ='" & IIf(chkEmail.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fbClosed ='" & IIf(chkClose.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "'"

        If sKeyShipAdd <> "" Then
            sSQLCmd &= ",@fxKeyShipAdd = '" & sKeyShipAdd & "'"
            If sKeyCustomer <> "" Then
                dtAddress = m_getAddress(sKeyCustomer, "Customer").Tables(0)
                Using rdAddTemp As DataTableReader = dtAddress.CreateDataReader
                    Dim sKeyAddTemp As String
                    If rdAddTemp.Read Then
                        sKeyAddTemp = rdAddTemp.Item("fxKeyAddress").ToString
                        sWarning = "Shipping address for Customer: "
                        sWarning &= cboCustomerName.SelectedItem
                        sWarning &= " is different from the master file." & vbCrLf
                        sWarning &= "Would you like to have this new information appear next time?"

                        If sKeyShipAdd <> sKeyAddTemp Then
                            msgWarning = MessageBox.Show(sWarning, "New Information Changed", MessageBoxButtons.YesNoCancel)
                            If msgWarning = Windows.Forms.DialogResult.Yes Then
                                updateShipAddress()
                            ElseIf msgWarning = Windows.Forms.DialogResult.Cancel Then
                                Exit Sub
                            End If
                        End If
                    Else
                        updateShipAddress()
                    End If
                End Using
            End If
        End If

        If sKeySalesTaxCode <> "" Then
            sSQLCmd &= ",@fxKeySalesTaxCode =" & IIf(sKeySalesTaxCode = "", "NULL", "'" & sKeySalesTaxCode & "'") & " "
        End If

        If sKeyTax <> "" Then
            sSQLCmd &= ",@fxKeyTax =" & IIf(sKeyTax = "", "NULL", "'" & sKeyTax & "'") & " "
            '",@acntsubof=" & IIf(gAccountsID() = "", "NULL", "'" & gAccountsID() & "'") & " "
        End If

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            If updateSOItem() Then
                MessageBox.Show("Record successfully Updated.", "Add/Edit S.O.")
                frm_cust_masterCustomer.loadGrdCustomer2("Sales Orders")
                frm_item_availability.loadDetails("Sales Orders")
                sKeySO = ""
                Me.Close()
            Else
                sSQLCmd = "DELETE FROM tSalesOrder WHERE fxKeySO = '" & sKeySO & "'"
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Cannot update this record!" & vbCrLf & "Please check the S.O. Items.", "S.O.")
                Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit S.O.")
        End Try
    End Sub
    Private Function updateSOItem() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Dim sKeySOItem As String
        Dim sItem As String = ""
        Dim bClosed As Boolean = False
        Dim sDescription As String = ""
        Dim dQuantity As Long = 0
        Dim dRate As Long
        Dim dAmount As Long
        Dim sKeyTaxCode As String = ""
        Try
            If sKeyToDel(0) <> "" Then
                Dim i As Integer
                For i = 0 To UBound(sKeyToDel)
                    If sKeyToDel(i) <> "" Then
                        sSQLCmd = "DELETE FROM tSalesOrder_Item "
                        sSQLCmd &= "WHERE fxKeySOItem = '" & sKeyToDel(i) & "'"
                        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                    End If
                Next
            End If

            If Me.grdSO.Rows.Count >= 1 Then
                For iRow = 0 To (grdSO.RowCount - 2)
                    If mkDefaultValues(iRow, cKeySOItem) <> Nothing Then
                        sKeySOItem = grdSO.Rows(iRow).Cells(cKeySOItem).Value.ToString
                    Else
                        sKeySOItem = Guid.NewGuid.ToString
                    End If

                    If mkDefaultValues(iRow, cItem) <> Nothing Then
                        sItem = grdSO.Rows(iRow).Cells(cItem).Value.ToString
                        sItem = m_GetItemByName(sItem)
                    End If

                    If mkDefaultValues(iRow, cDescription) <> Nothing Then
                        sDescription = grdSO.Rows(iRow).Cells(cDescription).Value
                    End If

                    If mkDefaultValues(iRow, cQuantity) <> Nothing Then
                        dQuantity = grdSO.Rows(iRow).Cells(cQuantity).Value
                    Else
                        dQuantity = 0
                    End If

                    If mkDefaultValues(iRow, cRate) <> Nothing Then
                        dRate = grdSO.Rows(iRow).Cells(cRate).Value
                    Else
                        dRate = 0
                    End If

                    If mkDefaultValues(iRow, cAmount) <> Nothing Then
                        dAmount = grdSO.Rows(iRow).Cells(cAmount).Value
                    Else
                        dAmount = 0
                    End If

                    If mkDefaultValues(iRow, cClose) <> Nothing Then
                        bClosed = grdSO.Rows(iRow).Cells(cClose).Value
                    End If

                    'If mkDefaultValues(iRow, cTax) <> Nothing Then
                    '    sKeyTaxCode = grdSO.Rows(iRow).Cells(cTax).Value
                    '    sKeyTaxCode = m_getSalesTaxCodeByName(sKeyTaxCode)
                    'End If

                    sSQLCmd = "usp_t_salesOrderItem_update "
                    sSQLCmd &= "@fxKeySOItem = '" & sKeySOItem & "'"
                    sSQLCmd &= ",@fxKeySO = '" & sKeySO & "'"
                    sSQLCmd &= ",@fxKeyItem = '" & sItem & "'"
                    sSQLCmd &= ",@fcSODescription = '" & sDescription & "'"
                    sSQLCmd &= ",@fdQuantity = " & dQuantity
                    sSQLCmd &= ",@fdRate = " & dRate
                    sSQLCmd &= ",@fdAmount = " & dAmount
                    sSQLCmd &= ",@fbClosed = " & IIf(bClosed = True, 1, 0)
                    sSQLCmd &= ",@fxKeySalesTaxCode =" & IIf(sKeyTaxCode = "", "NULL", "'" & sKeyTaxCode & "'") & " "
                    '",@acntsubof=" & IIf(gAccountsID() = "", "NULL", "'" & gAccountsID() & "'") & " "

                    SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                Next
                Return True
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit S.O. Item")
            Return False
        End Try

    End Function
    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdSO.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdSO.Rows(irow).Cells(iCol).Value)

    End Function

    Private Sub updateBillAddress()
        Dim sSQLCmd As String = "UPDATE mAddress SET fcAddress = '" & txtBillingAdd.Text & "' "
        sSQLCmd &= "WHERE fxKeyID = '" & sKeyCustomer & "' AND "
        sSQLCmd &= "fbCustomer = 1 AND fcAddressName = ''"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Billing Address")
        End Try
    End Sub

    Private Sub updateShipAddress()
        Dim sSQLCmd1 As String
        Dim sSQLCmd2 As String
        sSQLCmd1 = "UPDATE mCustomer00Master "
        sSQLCmd1 &= "SET fxKeyShippingAddress = '" & sKeyShipAdd & "' "
        sSQLCmd1 &= "WHERE fxKeyCustomer = '" & sKeyCustomer & "'"

        sSQLCmd2 = "UPDATE mAddress "
        sSQLCmd2 &= "SET fcAddress = '" & txtShippingAdd.Text & "' "
        sSQLCmd2 &= "WHERE fxKeyAddress = '" & sKeyShipAdd & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd1)
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd2)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Shipping Address")
        End Try
    End Sub

    Private Sub updateItem(ByVal sKey As String, ByVal nPrice As Long)
        Dim sSQLCmd As String = "UPDATE mItem00Master "
        sSQLCmd &= "SET fdPrice = " & nPrice
        sSQLCmd &= " WHERE fxKeyItem = '" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Item Price")
        End Try
    End Sub

    Private Sub grdSO_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdSO.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdSO.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub

    Private Sub grdSO_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSO.CellValueChanged
        If gEditEvent <> 1 Then
            If e.RowIndex >= 0 Then
                'Try
                Dim sItemTemp As String = grdSO.CurrentRow.Cells(cItem).Value.ToString
                Dim sKeyItemTemp As String = m_GetItemByName(sItemTemp)
                Dim dtItemTemp As DataTable = m_GetItem(sKeyItemTemp).Tables(0)
                Dim dQuantity As Integer = 0
                Dim dAmount As Decimal = 0.0
                Dim dRate As Decimal = 0.0
                If mkDefaultValues(grdSO.CurrentRow.Index, cQuantity) <> Nothing Then
                    dQuantity = grdSO.CurrentRow.Cells(cQuantity).Value
                End If
                If mkDefaultValues(grdSO.CurrentRow.Index, cRate) <> Nothing Then
                    dRate = grdSO.CurrentRow.Cells(cRate).Value
                End If
                If e.ColumnIndex = cRate Then
                    If sItemTemp <> "" Then
                        Try
                            If dRate <> dtItemTemp.Rows(0)("fdPrice") Then
                                Dim sWarn As String
                                Dim msgWarn As DialogResult
                                sWarn = "You have changed the price for: "
                                sWarn &= sItemTemp & vbCrLf & "Do you want to update the item with the new cost?"
                                msgWarn = MessageBox.Show(sWarn, "Item's Price Changed", MessageBoxButtons.YesNo)
                                If msgWarn = Windows.Forms.DialogResult.Yes Then
                                    updateItem(sKeyItemTemp, dRate)
                                End If
                            End If
                            dAmount = dRate * dQuantity
                            grdSO.CurrentRow.Cells(cAmount).Value = dAmount
                        Catch ex As Exception
                        End Try
                    End If
                End If
                If e.ColumnIndex = cItem Then
                    If sItemTemp <> "" Then
                        Try
                            grdSO.CurrentRow.Cells(cDescription).Value = dtItemTemp.Rows(0)("fcItemDescription")
                            grdSO.CurrentRow.Cells(cUnit).Value = dtItemTemp.Rows(0)("fcUnitAbbreviation")
                            grdSO.CurrentRow.Cells(cRate).Value = dtItemTemp.Rows(0)("fdPrice")
                            ' grdSO.CurrentRow.Cells(cTax).Value = dtItemTemp.Rows(0)("fcSalesTaxCodeName")
                        Catch
                            MessageBox.Show(Err.Description, "S.O. Item")
                        End Try
                    End If
                End If
                If e.ColumnIndex = cQuantity Then
                    Try
                        Dim dQuantityTemp As Long
                        Dim sWarnTemp As String
                        Dim sOnHand As String
                        Dim sOnSO As String
                        sOnHand = dtItemTemp.Rows(0)("fdOnHand").ToString
                        sOnSO = dtItemTemp.Rows(0)("fdQuantityOnSO").ToString
                        dQuantityTemp = sOnHand - sOnSO
                        If dQuantityTemp < dQuantity Then
                            sWarnTemp = "You don't have sufficient quantity available to sell " _
                                 & dQuantity & " of the item: " _
                                 & vbCrLf & grdSO.CurrentRow.Cells(cItem).Value & vbCrLf & vbCrLf _
                                 & "Quantity on hand" & vbTab & vbTab & vbTab & vbTab _
                                 & sOnHand.PadLeft(20 - Len(sOnHand)) & vbCrLf _
                                 & "Quantity on other Sales Order" & vbTab & vbTab _
                                 & sOnSO.PadLeft(20 - Len(sOnSO)) & vbCrLf & vbCrLf _
                                 & "___________________________________________________" & vbCrLf _
                                 & "Quantity available" & vbTab & vbTab & vbTab & vbTab _
                                 & Format(dQuantityTemp, "0.00").PadLeft(20 - Len(Format(dQuantityTemp, "0.00")))
                            MessageBox.Show(sWarnTemp, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                        dAmount = dQuantity * dRate
                        grdSO.CurrentRow.Cells(cAmount).Value = dAmount.ToString
                    Catch
                        MessageBox.Show(Err.Description, "Data Validation")
                    End Try
                End If
                'If mkDefaultValues(e.RowIndex, cAmount) <> Nothing Then
                getTotal()
                'End If
                '  Catch ex As Exception
                '     MessageBox.Show(Err.Description, "S.O. Item")
                'End Try
            End If
        End If
    End Sub

    Private Function isTaxable(ByVal bCustomer As Boolean, _
       Optional ByVal iRow As Integer = 0) As Boolean
        Dim bTaxable As Boolean
        If bCustomer Then
            If sKeySalesTaxCode <> "" Then
                bTaxable = m_isTaxable(sKeySalesTaxCode)
            Else
                bTaxable = False
            End If
        Else
            bTaxable = False
            'Dim sKey As String
            'Dim sName As String
            'sName = grdSO.Item(cTax, iRow).Value
            'sKey = m_getSalesTaxCodeByName(sName).ToString
            'bTaxable = m_isTaxable(sKey)
        End If
        Return bTaxable
    End Function


    'Private Function getTotal() As Decimal
    Private Function getTotal() As Decimal
        Dim iRow As Integer
        Dim dTotal As Decimal = 0
        Dim dTotalTx As Decimal = 0
        Dim dTax As Decimal = 0
        Dim dTotalNTx As Decimal = 0
        Dim sKeyItm As String
        Try
            For iRow = 0 To grdSO.Rows.Count - 2
                sKeyItm = grdSO.Item(cItem, iRow).Value.ToString
                sKeyItm = m_GetItemByName(sKeyItm)
                If sKeyCustomer <> "" Then
                    If isTaxable(1) Then
                        If isTaxable(0, iRow) Then
                            dTotalTx = dTotalTx + grdSO.Item(6, iRow).Value
                        Else
                            dTotalNTx = dTotalNTx + grdSO.Item(6, iRow).Value
                        End If
                    Else
                        dTotalNTx = dTotalNTx + grdSO.Item(6, iRow).Value
                    End If
                Else
                    If isTaxable(0, iRow) Then
                        dTotalTx = dTotalTx + grdSO.Item(6, iRow).Value
                    Else
                        dTotalNTx = dTotalNTx + grdSO.Item(6, iRow).Value
                    End If
                End If
            Next
            dTax = (dTotalTx * (dPercentage / 100))
            dTotal = dTotalTx + dTotalNTx + dTax

            lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
            lblTaxAmt.Text = Format(dTax, "0.00")
            lblTotal.Text = Format(CDec(dTotal), "##,##0.00")

            Return dTotal
        Catch
            Return 0
        End Try
    End Function
    'Dim iRow As Integer = 0
    'Dim dTotal As Decimal = 0.0
    'Try
    '    With Me.grdSO
    '        For iRow = 0 To .Rows.Count - 1
    '            If Not .Rows(iRow).Cells(2).Value.ToString Is Nothing Then
    '                dTotal += .Rows(iRow).Cells(cAmount).Value
    '            End If
    '        Next
    '    End With
    '    If lblTaxAmt.Text <> "0.00" Then
    '        lblTotal.Text = dTotal + CDec(lblTaxAmt.Text)
    '    Else
    '        lblTotal.Text = Format(CDec(dTotal), "##,##0.00")
    '    End If
    '    Return CDec(lblTotal.Text)
    'Catch
    '    Return 0.0
    'End Try
    'End Function

    'Private Function isTaxable(ByVal bCustomer As Boolean, _
    'Optional ByVal iRow As Integer = 0) As Boolean
    '    Dim bTaxable As Boolean
    '    If bCustomer Then
    '        If sKeySalesTaxCode <> "" Then
    '            bTaxable = m_isTaxable(sKeySalesTaxCode)
    '        Else
    '            bTaxable = False
    '        End If
    '    Else
    '        Dim sKey As String
    '        Dim sName As String
    '        'sName = grdSO.Item(cTax, iRow).Value
    '        sKey = m_getSalesTaxCodeByName(sName).ToString
    '        bTaxable = m_isTaxable(sKey)
    '    End If
    '    Return bTaxable
    'End Function

    Private Sub cboTaxName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxName.SelectedIndexChanged
        cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        sKeyTax = cboTaxID.SelectedItem.ToString

        If sKeyTax <> "" Then
            Dim dtTax As DataSet = m_GetTax(sKeyTax)
            dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        Else
            dPercentage = 0
        End If
        lblPercentage.Text = Format(CDec(dPercentage), "0.00")

        Try
            With Me.grdSO
                Dim xCnt As Integer = 0
                Dim xTotal As Decimal = 0
                For xCnt = 0 To .RowCount - 1
                    If .Item(2, xCnt).Value IsNot Nothing Then
                        xTotal += CDec(.Item(6, xCnt).Value)
                    End If
                Next
                lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "##,##0.00")
            End With
        Catch ex As Exception
        End Try

        If cboTaxName.SelectedIndex = 1 Then
            frm_MF_taxAddEdit.Text = "New Sales Tax"
            frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
            frm_MF_taxAddEdit.Show()
        End If


        'cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        'sKeyTax = cboTaxID.SelectedItem.ToString
        'If sKeyTax <> "" Then
        '    Dim dtTax As DataSet = m_GetTax(sKeyTax)
        '    dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        'Else
        '    dPercentage = 0
        'End If
        'getTotal()
        'If cboTaxName.SelectedIndex = 1 Then
        '    frm_MF_taxAddEdit.Text = "New Sales Tax"
        '    frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
        '    frm_MF_taxAddEdit.Show()
        'End If
    End Sub

    Private Sub grdSO_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdSO.EditingControlShowing
        Dim comboBoxItem As DataGridViewComboBoxColumn = grdSO.Columns(1)
        If (grdSO.CurrentCellAddress.X = comboBoxItem.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If
        'Dim comboBoxTax As DataGridViewComboBoxColumn = grdSO.Columns(7)
        'If (grdSO.CurrentCellAddress.X = comboBoxTax.DisplayIndex) Then
        '    Dim cbo As ComboBox = e.Control
        '    If (cbo IsNot Nothing) Then
        '        cbo.DropDownStyle = ComboBoxStyle.DropDown
        '        cbo.AutoCompleteMode = AutoCompleteMode.Suggest
        '        cbo.AutoCompleteSource = AutoCompleteSource.ListItems
        '    End If
        'End If
    End Sub

    Private Sub grdSO_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdSO.UserDeletedRow
        getTotal()
    End Sub

    Private Sub grdSO_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdSO.UserDeletingRow
        If grdSO.Rows.Count > 0 Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                ReDim Preserve sKeyToDel(iDel)
                'sKeyToDel(iDel) = e.Row.Cells(cKeySOItem).Value.ToString
                iDel += 1
                getTotal()
                lblTotal.Text = lblTotal.Text - e.Row.Cells(cAmount).Value
                lblTotal.Text = Format(CLng(lblTotal.Text), "0.00")
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub lblTaxAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTaxAmt.TextChanged
        Try
            lblTotal.Text = Format((CDec(lblTotal.Text) + CDec(lblTaxAmt.Text)), "##,##0.00")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub IsDateRestricted()
        'Validate Invoice Date
        If checkDate.checkIfRestricted(dteTransDate.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
        End If
    End Sub

    Private Sub dteTransDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteTransDate.ValueChanged
        IsDateRestricted()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

    End Sub
End Class