<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_CreateStatementCharges
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_CreateStatementCharges))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ts_Edit = New System.Windows.Forms.ToolStripButton
        Me.ts_TimeCost = New System.Windows.Forms.ToolStripButton
        Me.ts_Report = New System.Windows.Forms.ToolStripButton
        Me.ts_Print = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.grdAccountsRcvble = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkOneLine = New System.Windows.Forms.CheckBox
        Me.chkShowOpenBal = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnRecord = New System.Windows.Forms.Button
        Me.btnStore = New System.Windows.Forms.Button
        Me.cboSortBy = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdAccountsRcvble, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Edit, Me.ts_TimeCost, Me.ts_Report, Me.ts_Print})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(553, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_Edit
        '
        Me.ts_Edit.Image = CType(resources.GetObject("ts_Edit.Image"), System.Drawing.Image)
        Me.ts_Edit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Edit.Name = "ts_Edit"
        Me.ts_Edit.Size = New System.Drawing.Size(45, 22)
        Me.ts_Edit.Text = "Edit"
        '
        'ts_TimeCost
        '
        Me.ts_TimeCost.Image = CType(resources.GetObject("ts_TimeCost.Image"), System.Drawing.Image)
        Me.ts_TimeCost.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_TimeCost.Name = "ts_TimeCost"
        Me.ts_TimeCost.Size = New System.Drawing.Size(80, 22)
        Me.ts_TimeCost.Text = "Time/Costs"
        '
        'ts_Report
        '
        Me.ts_Report.Image = CType(resources.GetObject("ts_Report.Image"), System.Drawing.Image)
        Me.ts_Report.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Report.Name = "ts_Report"
        Me.ts_Report.Size = New System.Drawing.Size(60, 22)
        Me.ts_Report.Text = "Report"
        '
        'ts_Print
        '
        Me.ts_Print.Image = CType(resources.GetObject("ts_Print.Image"), System.Drawing.Image)
        Me.ts_Print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.Size = New System.Drawing.Size(49, 22)
        Me.ts_Print.Text = "Print"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Customer:Job"
        '
        'cboCustomerName
        '
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(89, 31)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(209, 21)
        Me.cboCustomerName.TabIndex = 2
        '
        'grdAccountsRcvble
        '
        Me.grdAccountsRcvble.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdAccountsRcvble.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdAccountsRcvble.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8})
        Me.grdAccountsRcvble.Location = New System.Drawing.Point(0, 58)
        Me.grdAccountsRcvble.Name = "grdAccountsRcvble"
        Me.grdAccountsRcvble.Size = New System.Drawing.Size(553, 196)
        Me.grdAccountsRcvble.TabIndex = 3
        '
        'Column1
        '
        Me.Column1.HeaderText = "Date"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Number Type"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Item"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Qty"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Rate"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Amt Chrg"
        Me.Column6.Name = "Column6"
        '
        'Column7
        '
        Me.Column7.HeaderText = "Amt Paid"
        Me.Column7.Name = "Column7"
        '
        'Column8
        '
        Me.Column8.HeaderText = "Balance"
        Me.Column8.Name = "Column8"
        '
        'chkOneLine
        '
        Me.chkOneLine.AutoSize = True
        Me.chkOneLine.Location = New System.Drawing.Point(15, 260)
        Me.chkOneLine.Name = "chkOneLine"
        Me.chkOneLine.Size = New System.Drawing.Size(55, 17)
        Me.chkOneLine.TabIndex = 4
        Me.chkOneLine.Text = "&1-Line"
        Me.chkOneLine.UseVisualStyleBackColor = True
        '
        'chkShowOpenBal
        '
        Me.chkShowOpenBal.AutoSize = True
        Me.chkShowOpenBal.Location = New System.Drawing.Point(89, 260)
        Me.chkShowOpenBal.Name = "chkShowOpenBal"
        Me.chkShowOpenBal.Size = New System.Drawing.Size(121, 17)
        Me.chkShowOpenBal.TabIndex = 5
        Me.chkShowOpenBal.Text = "Show Open Balance"
        Me.chkShowOpenBal.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(363, 261)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Ending Balance"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(459, 261)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "0.00"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnRecord
        '
        Me.btnRecord.Location = New System.Drawing.Point(387, 288)
        Me.btnRecord.Name = "btnRecord"
        Me.btnRecord.Size = New System.Drawing.Size(75, 23)
        Me.btnRecord.TabIndex = 8
        Me.btnRecord.Text = "Record"
        Me.btnRecord.UseVisualStyleBackColor = True
        '
        'btnStore
        '
        Me.btnStore.Location = New System.Drawing.Point(466, 288)
        Me.btnStore.Name = "btnStore"
        Me.btnStore.Size = New System.Drawing.Size(75, 23)
        Me.btnStore.TabIndex = 9
        Me.btnStore.Text = "Store"
        Me.btnStore.UseVisualStyleBackColor = True
        '
        'cboSortBy
        '
        Me.cboSortBy.FormattingEnabled = True
        Me.cboSortBy.Location = New System.Drawing.Point(352, 31)
        Me.cboSortBy.Name = "cboSortBy"
        Me.cboSortBy.Size = New System.Drawing.Size(189, 21)
        Me.cboSortBy.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(306, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Sort by"
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(89, 31)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(209, 21)
        Me.cboCustomerID.TabIndex = 12
        '
        'frm_cust_CreateStatementCharges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(553, 319)
        Me.Controls.Add(Me.cboSortBy)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnStore)
        Me.Controls.Add(Me.btnRecord)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkShowOpenBal)
        Me.Controls.Add(Me.chkOneLine)
        Me.Controls.Add(Me.grdAccountsRcvble)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_cust_CreateStatementCharges"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Accounts Receivable"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdAccountsRcvble, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_Edit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_TimeCost As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Report As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents grdAccountsRcvble As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkOneLine As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowOpenBal As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnRecord As System.Windows.Forms.Button
    Friend WithEvents btnStore As System.Windows.Forms.Button
    Friend WithEvents cboSortBy As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
End Class
