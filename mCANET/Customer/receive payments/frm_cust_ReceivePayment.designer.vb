<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_ReceivePayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_ReceivePayment))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ts_Previous = New System.Windows.Forms.ToolStripButton
        Me.ts_Next = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_History = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_ProcessedPaymentRcpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Journal = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboPaymentMethodID = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.dteRcvPaymentBal = New System.Windows.Forms.DateTimePicker
        Me.BtnBrowse = New System.Windows.Forms.TextBox
        Me.txtReferenceNo = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnFind = New System.Windows.Forms.Button
        Me.grdRcvPayment = New System.Windows.Forms.DataGridView
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblTotalPayment = New System.Windows.Forms.Label
        Me.lblTotalAmtDue = New System.Windows.Forms.Label
        Me.lblTotalOrigAmt = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblSDAmt = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblRDAmt = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.lblPayment = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.lblAmtDue = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.btnAutoApplyPayment = New System.Windows.Forms.Button
        Me.btnDiscount = New System.Windows.Forms.Button
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.cboPaymentMethodName = New System.Windows.Forms.ComboBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.chkProcess = New System.Windows.Forms.CheckBox
        Me.grpUnderPay = New System.Windows.Forms.GroupBox
        Me.rbtnWriteOff = New System.Windows.Forms.RadioButton
        Me.rbtnLeave = New System.Windows.Forms.RadioButton
        Me.lblUnderPay = New System.Windows.Forms.Label
        Me.lblCardNo = New System.Windows.Forms.Label
        Me.txtCardNo = New System.Windows.Forms.TextBox
        Me.lblExDate = New System.Windows.Forms.Label
        Me.txtMonth = New System.Windows.Forms.TextBox
        Me.lblChar = New System.Windows.Forms.Label
        Me.txtYear = New System.Windows.Forms.TextBox
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.txtPayMethod = New System.Windows.Forms.TextBox
        Me.cboPayAcnt = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.LblClassName = New System.Windows.Forms.Label
        Me.BtnBrowseClass = New System.Windows.Forms.Button
        Me.TxtClassRefNo = New System.Windows.Forms.TextBox
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdRcvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpUnderPay.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Previous, Me.ts_Next, Me.ToolStripButton3, Me.ts_Journal})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(859, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_Previous
        '
        Me.ts_Previous.Image = CType(resources.GetObject("ts_Previous.Image"), System.Drawing.Image)
        Me.ts_Previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Previous.Name = "ts_Previous"
        Me.ts_Previous.Size = New System.Drawing.Size(84, 22)
        Me.ts_Previous.Text = "Previous"
        '
        'ts_Next
        '
        Me.ts_Next.Image = CType(resources.GetObject("ts_Next.Image"), System.Drawing.Image)
        Me.ts_Next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Next.Name = "ts_Next"
        Me.ts_Next.Size = New System.Drawing.Size(57, 22)
        Me.ts_Next.Text = "Next"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_History, Me.ts_ProcessedPaymentRcpt})
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(86, 22)
        Me.ToolStripButton3.Text = "History"
        Me.ToolStripButton3.Visible = False
        '
        'ts_History
        '
        Me.ts_History.Name = "ts_History"
        Me.ts_History.Size = New System.Drawing.Size(265, 22)
        Me.ts_History.Text = "History"
        '
        'ts_ProcessedPaymentRcpt
        '
        Me.ts_ProcessedPaymentRcpt.Name = "ts_ProcessedPaymentRcpt"
        Me.ts_ProcessedPaymentRcpt.Size = New System.Drawing.Size(265, 22)
        Me.ts_ProcessedPaymentRcpt.Text = "Processed Payment Receipt"
        '
        'ts_Journal
        '
        Me.ts_Journal.Image = CType(resources.GetObject("ts_Journal.Image"), System.Drawing.Image)
        Me.ts_Journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Journal.Name = "ts_Journal"
        Me.ts_Journal.Size = New System.Drawing.Size(75, 22)
        Me.ts_Journal.Text = "Journal"
        Me.ts_Journal.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(191, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Customer Payment"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Received From"
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(140, 65)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(273, 21)
        Me.cboCustomerID.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Amount"
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(140, 90)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(137, 21)
        Me.txtAmount.TabIndex = 5
        Me.txtAmount.Text = "0.00"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 118)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Payment Method"
        '
        'cboPaymentMethodID
        '
        Me.cboPaymentMethodID.FormattingEnabled = True
        Me.cboPaymentMethodID.Location = New System.Drawing.Point(140, 115)
        Me.cboPaymentMethodID.Name = "cboPaymentMethodID"
        Me.cboPaymentMethodID.Size = New System.Drawing.Size(273, 21)
        Me.cboPaymentMethodID.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Memo"
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(140, 165)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(273, 21)
        Me.txtMemo.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(581, 68)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Customer Balance"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(580, 93)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Date"
        '
        'dteRcvPaymentBal
        '
        Me.dteRcvPaymentBal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteRcvPaymentBal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteRcvPaymentBal.Location = New System.Drawing.Point(705, 89)
        Me.dteRcvPaymentBal.Name = "dteRcvPaymentBal"
        Me.dteRcvPaymentBal.Size = New System.Drawing.Size(143, 21)
        Me.dteRcvPaymentBal.TabIndex = 12
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowse.Location = New System.Drawing.Point(705, 65)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(144, 21)
        Me.BtnBrowse.TabIndex = 13
        Me.BtnBrowse.Text = "0.00"
        Me.BtnBrowse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtReferenceNo.Location = New System.Drawing.Point(704, 115)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(144, 21)
        Me.txtReferenceNo.TabIndex = 15
        Me.txtReferenceNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(580, 118)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Reference/OR #"
        '
        'btnFind
        '
        Me.btnFind.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFind.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFind.Location = New System.Drawing.Point(19, 506)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(87, 23)
        Me.btnFind.TabIndex = 16
        Me.btnFind.Text = "&Find"
        Me.btnFind.UseVisualStyleBackColor = True
        Me.btnFind.Visible = False
        '
        'grdRcvPayment
        '
        Me.grdRcvPayment.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.grdRcvPayment.AllowUserToResizeColumns = False
        Me.grdRcvPayment.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grdRcvPayment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdRcvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRcvPayment.Location = New System.Drawing.Point(15, 194)
        Me.grdRcvPayment.Name = "grdRcvPayment"
        Me.grdRcvPayment.Size = New System.Drawing.Size(834, 158)
        Me.grdRcvPayment.TabIndex = 18
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.Menu
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblTotalPayment)
        Me.Panel1.Controls.Add(Me.lblTotalAmtDue)
        Me.Panel1.Controls.Add(Me.lblTotalOrigAmt)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Location = New System.Drawing.Point(15, 351)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(834, 21)
        Me.Panel1.TabIndex = 19
        '
        'lblTotalPayment
        '
        Me.lblTotalPayment.Location = New System.Drawing.Point(581, 3)
        Me.lblTotalPayment.Name = "lblTotalPayment"
        Me.lblTotalPayment.Size = New System.Drawing.Size(114, 13)
        Me.lblTotalPayment.TabIndex = 22
        Me.lblTotalPayment.Text = "0.00"
        Me.lblTotalPayment.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotalAmtDue
        '
        Me.lblTotalAmtDue.Location = New System.Drawing.Point(461, 3)
        Me.lblTotalAmtDue.Name = "lblTotalAmtDue"
        Me.lblTotalAmtDue.Size = New System.Drawing.Size(117, 13)
        Me.lblTotalAmtDue.TabIndex = 21
        Me.lblTotalAmtDue.Text = "0.00"
        Me.lblTotalAmtDue.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotalOrigAmt
        '
        Me.lblTotalOrigAmt.Location = New System.Drawing.Point(335, 3)
        Me.lblTotalOrigAmt.Name = "lblTotalOrigAmt"
        Me.lblTotalOrigAmt.Size = New System.Drawing.Size(119, 13)
        Me.lblTotalOrigAmt.TabIndex = 20
        Me.lblTotalOrigAmt.Text = "0.00"
        Me.lblTotalOrigAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(190, 3)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Total"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBox1.Controls.Add(Me.lblSDAmt)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.lblRDAmt)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.lblPayment)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.lblAmtDue)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Location = New System.Drawing.Point(555, 378)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(293, 121)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selected Invoices"
        '
        'lblSDAmt
        '
        Me.lblSDAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSDAmt.Location = New System.Drawing.Point(137, 80)
        Me.lblSDAmt.Name = "lblSDAmt"
        Me.lblSDAmt.Size = New System.Drawing.Size(150, 15)
        Me.lblSDAmt.TabIndex = 13
        Me.lblSDAmt.Text = "0.00"
        Me.lblSDAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(10, 95)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(121, 19)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Retailers Discount"
        '
        'lblRDAmt
        '
        Me.lblRDAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRDAmt.Location = New System.Drawing.Point(137, 95)
        Me.lblRDAmt.Name = "lblRDAmt"
        Me.lblRDAmt.Size = New System.Drawing.Size(150, 15)
        Me.lblRDAmt.TabIndex = 11
        Me.lblRDAmt.Text = "0.00"
        Me.lblRDAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(10, 76)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(121, 19)
        Me.Label18.TabIndex = 10
        Me.Label18.Text = "Sales Discount"
        '
        'lblPayment
        '
        Me.lblPayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayment.Location = New System.Drawing.Point(165, 38)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(122, 13)
        Me.lblPayment.TabIndex = 9
        Me.lblPayment.Text = "0.00"
        Me.lblPayment.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(10, 38)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(49, 13)
        Me.Label16.TabIndex = 8
        Me.Label16.Text = "Applied"
        '
        'lblAmtDue
        '
        Me.lblAmtDue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmtDue.Location = New System.Drawing.Point(165, 17)
        Me.lblAmtDue.Name = "lblAmtDue"
        Me.lblAmtDue.Size = New System.Drawing.Size(122, 13)
        Me.lblAmtDue.TabIndex = 7
        Me.lblAmtDue.Text = "0.00"
        Me.lblAmtDue.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(10, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 13)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Amount Due"
        '
        'btnAutoApplyPayment
        '
        Me.btnAutoApplyPayment.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAutoApplyPayment.Location = New System.Drawing.Point(293, 414)
        Me.btnAutoApplyPayment.Name = "btnAutoApplyPayment"
        Me.btnAutoApplyPayment.Size = New System.Drawing.Size(135, 23)
        Me.btnAutoApplyPayment.TabIndex = 24
        Me.btnAutoApplyPayment.Text = "Auto Apply Payment"
        Me.btnAutoApplyPayment.UseVisualStyleBackColor = True
        Me.btnAutoApplyPayment.Visible = False
        '
        'btnDiscount
        '
        Me.btnDiscount.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnDiscount.Location = New System.Drawing.Point(293, 385)
        Me.btnDiscount.Name = "btnDiscount"
        Me.btnDiscount.Size = New System.Drawing.Size(135, 23)
        Me.btnDiscount.TabIndex = 25
        Me.btnDiscount.Text = "Discoun&t && Credits"
        Me.btnDiscount.UseVisualStyleBackColor = True
        Me.btnDiscount.Visible = False
        '
        'cboCustomerName
        '
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(140, 65)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(273, 21)
        Me.cboCustomerName.TabIndex = 26
        '
        'cboPaymentMethodName
        '
        Me.cboPaymentMethodName.FormattingEnabled = True
        Me.cboPaymentMethodName.Location = New System.Drawing.Point(140, 115)
        Me.cboPaymentMethodName.Name = "cboPaymentMethodName"
        Me.cboPaymentMethodName.Size = New System.Drawing.Size(273, 21)
        Me.cboPaymentMethodName.TabIndex = 27
        '
        'btnClose
        '
        Me.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(755, 509)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 23)
        Me.btnClose.TabIndex = 58
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(664, 509)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 23)
        Me.btnSave.TabIndex = 57
        Me.btnSave.Text = "S&ave "
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'chkProcess
        '
        Me.chkProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkProcess.AutoSize = True
        Me.chkProcess.Location = New System.Drawing.Point(584, 167)
        Me.chkProcess.Name = "chkProcess"
        Me.chkProcess.Size = New System.Drawing.Size(210, 17)
        Me.chkProcess.TabIndex = 17
        Me.chkProcess.Text = "Process credit card when saving"
        Me.chkProcess.UseVisualStyleBackColor = True
        Me.chkProcess.Visible = False
        '
        'grpUnderPay
        '
        Me.grpUnderPay.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grpUnderPay.Controls.Add(Me.rbtnWriteOff)
        Me.grpUnderPay.Controls.Add(Me.rbtnLeave)
        Me.grpUnderPay.Controls.Add(Me.lblUnderPay)
        Me.grpUnderPay.Location = New System.Drawing.Point(20, 375)
        Me.grpUnderPay.Name = "grpUnderPay"
        Me.grpUnderPay.Size = New System.Drawing.Size(258, 104)
        Me.grpUnderPay.TabIndex = 65
        Me.grpUnderPay.TabStop = False
        Me.grpUnderPay.Visible = False
        '
        'rbtnWriteOff
        '
        Me.rbtnWriteOff.AutoSize = True
        Me.rbtnWriteOff.Location = New System.Drawing.Point(10, 78)
        Me.rbtnWriteOff.Name = "rbtnWriteOff"
        Me.rbtnWriteOff.Size = New System.Drawing.Size(177, 17)
        Me.rbtnWriteOff.TabIndex = 8
        Me.rbtnWriteOff.TabStop = True
        Me.rbtnWriteOff.Text = "Write off the extra amount"
        Me.rbtnWriteOff.UseVisualStyleBackColor = True
        Me.rbtnWriteOff.Visible = False
        '
        'rbtnLeave
        '
        Me.rbtnLeave.AutoSize = True
        Me.rbtnLeave.Location = New System.Drawing.Point(10, 55)
        Me.rbtnLeave.Name = "rbtnLeave"
        Me.rbtnLeave.Size = New System.Drawing.Size(205, 17)
        Me.rbtnLeave.TabIndex = 7
        Me.rbtnLeave.TabStop = True
        Me.rbtnLeave.Text = "Leave this as an underpayment"
        Me.rbtnLeave.UseVisualStyleBackColor = True
        Me.rbtnLeave.Visible = False
        '
        'lblUnderPay
        '
        Me.lblUnderPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnderPay.Location = New System.Drawing.Point(7, 17)
        Me.lblUnderPay.Name = "lblUnderPay"
        Me.lblUnderPay.Size = New System.Drawing.Size(217, 41)
        Me.lblUnderPay.TabIndex = 6
        Me.lblUnderPay.Text = "Underpayment 0.00. "
        '
        'lblCardNo
        '
        Me.lblCardNo.AutoSize = True
        Me.lblCardNo.Location = New System.Drawing.Point(16, 143)
        Me.lblCardNo.Name = "lblCardNo"
        Me.lblCardNo.Size = New System.Drawing.Size(100, 13)
        Me.lblCardNo.TabIndex = 59
        Me.lblCardNo.Text = "Bank/Check No."
        '
        'txtCardNo
        '
        Me.txtCardNo.Location = New System.Drawing.Point(140, 140)
        Me.txtCardNo.Name = "txtCardNo"
        Me.txtCardNo.Size = New System.Drawing.Size(273, 21)
        Me.txtCardNo.TabIndex = 60
        '
        'lblExDate
        '
        Me.lblExDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblExDate.AutoSize = True
        Me.lblExDate.Location = New System.Drawing.Point(580, 143)
        Me.lblExDate.Name = "lblExDate"
        Me.lblExDate.Size = New System.Drawing.Size(63, 13)
        Me.lblExDate.TabIndex = 61
        Me.lblExDate.Text = "Exp. Date"
        '
        'txtMonth
        '
        Me.txtMonth.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMonth.Location = New System.Drawing.Point(704, 140)
        Me.txtMonth.MaxLength = 2
        Me.txtMonth.Name = "txtMonth"
        Me.txtMonth.Size = New System.Drawing.Size(32, 21)
        Me.txtMonth.TabIndex = 62
        Me.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblChar
        '
        Me.lblChar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblChar.AutoSize = True
        Me.lblChar.Location = New System.Drawing.Point(744, 143)
        Me.lblChar.Name = "lblChar"
        Me.lblChar.Size = New System.Drawing.Size(12, 13)
        Me.lblChar.TabIndex = 63
        Me.lblChar.Text = "/"
        '
        'txtYear
        '
        Me.txtYear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtYear.Location = New System.Drawing.Point(761, 140)
        Me.txtYear.MaxLength = 4
        Me.txtYear.Name = "txtYear"
        Me.txtYear.Size = New System.Drawing.Size(55, 21)
        Me.txtYear.TabIndex = 64
        Me.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Location = New System.Drawing.Point(110, 506)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(87, 23)
        Me.btnRefresh.TabIndex = 66
        Me.btnRefresh.Text = "&Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'txtPayMethod
        '
        Me.txtPayMethod.Location = New System.Drawing.Point(212, 32)
        Me.txtPayMethod.Name = "txtPayMethod"
        Me.txtPayMethod.Size = New System.Drawing.Size(273, 21)
        Me.txtPayMethod.TabIndex = 67
        Me.txtPayMethod.Visible = False
        '
        'cboPayAcnt
        '
        Me.cboPayAcnt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cboPayAcnt.FormattingEnabled = True
        Me.cboPayAcnt.Location = New System.Drawing.Point(136, 483)
        Me.cboPayAcnt.Name = "cboPayAcnt"
        Me.cboPayAcnt.Size = New System.Drawing.Size(277, 21)
        Me.cboPayAcnt.TabIndex = 94
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(19, 487)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(111, 13)
        Me.Label20.TabIndex = 93
        Me.Label20.Text = "Pay Debit Account"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(535, 35)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 95
        Me.Label11.Text = "Class:"
        '
        'LblClassName
        '
        Me.LblClassName.BackColor = System.Drawing.Color.White
        Me.LblClassName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LblClassName.Location = New System.Drawing.Point(584, 32)
        Me.LblClassName.Name = "LblClassName"
        Me.LblClassName.Size = New System.Drawing.Size(192, 23)
        Me.LblClassName.TabIndex = 96
        '
        'BtnBrowseClass
        '
        Me.BtnBrowseClass.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowseClass.Location = New System.Drawing.Point(782, 32)
        Me.BtnBrowseClass.Name = "BtnBrowseClass"
        Me.BtnBrowseClass.Size = New System.Drawing.Size(67, 23)
        Me.BtnBrowseClass.TabIndex = 97
        Me.BtnBrowseClass.Text = "Browse"
        Me.BtnBrowseClass.UseVisualStyleBackColor = True
        '
        'TxtClassRefNo
        '
        Me.TxtClassRefNo.Enabled = False
        Me.TxtClassRefNo.Location = New System.Drawing.Point(584, 12)
        Me.TxtClassRefNo.Name = "TxtClassRefNo"
        Me.TxtClassRefNo.Size = New System.Drawing.Size(193, 21)
        Me.TxtClassRefNo.TabIndex = 98
        Me.TxtClassRefNo.Visible = False
        '
        'frm_cust_ReceivePayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(859, 539)
        Me.Controls.Add(Me.BtnBrowseClass)
        Me.Controls.Add(Me.LblClassName)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cboPaymentMethodName)
        Me.Controls.Add(Me.cboPayAcnt)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtPayMethod)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.grpUnderPay)
        Me.Controls.Add(Me.txtYear)
        Me.Controls.Add(Me.lblChar)
        Me.Controls.Add(Me.txtMonth)
        Me.Controls.Add(Me.lblExDate)
        Me.Controls.Add(Me.txtCardNo)
        Me.Controls.Add(Me.lblCardNo)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.btnDiscount)
        Me.Controls.Add(Me.btnAutoApplyPayment)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.grdRcvPayment)
        Me.Controls.Add(Me.chkProcess)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.txtReferenceNo)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.BtnBrowse)
        Me.Controls.Add(Me.dteRcvPaymentBal)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboPaymentMethodID)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.TxtClassRefNo)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(867, 400)
        Me.Name = "frm_cust_ReceivePayment"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receive Payment"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdRcvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpUnderPay.ResumeLayout(False)
        Me.grpUnderPay.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_Previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_History As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_ProcessedPaymentRcpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboPaymentMethodID As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dteRcvPaymentBal As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtnBrowse As System.Windows.Forms.TextBox
    Friend WithEvents txtReferenceNo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents grdRcvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTotalPayment As System.Windows.Forms.Label
    Friend WithEvents lblTotalAmtDue As System.Windows.Forms.Label
    Friend WithEvents lblTotalOrigAmt As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblRDAmt As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblAmtDue As System.Windows.Forms.Label
    Friend WithEvents btnAutoApplyPayment As System.Windows.Forms.Button
    Friend WithEvents btnDiscount As System.Windows.Forms.Button
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents cboPaymentMethodName As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents chkProcess As System.Windows.Forms.CheckBox
    Friend WithEvents grpUnderPay As System.Windows.Forms.GroupBox
    Friend WithEvents lblUnderPay As System.Windows.Forms.Label
    Friend WithEvents rbtnWriteOff As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnLeave As System.Windows.Forms.RadioButton
    Friend WithEvents lblCardNo As System.Windows.Forms.Label
    Friend WithEvents txtCardNo As System.Windows.Forms.TextBox
    Friend WithEvents lblExDate As System.Windows.Forms.Label
    Friend WithEvents txtMonth As System.Windows.Forms.TextBox
    Friend WithEvents lblChar As System.Windows.Forms.Label
    Friend WithEvents txtYear As System.Windows.Forms.TextBox
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents lblSDAmt As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPayMethod As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cboPayAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents LblClassName As System.Windows.Forms.Label
    Friend WithEvents BtnBrowseClass As System.Windows.Forms.Button
    Friend WithEvents TxtClassRefNo As System.Windows.Forms.TextBox
End Class
