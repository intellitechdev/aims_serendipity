Public Class frmRetailersDiscount

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Call m_saveRetailersDiscount(txtDiscCode.Text, txtDescription.Text, txtPercent.Text, Me)
        m_GetRetailerDiscount(frm_cust_CreateInvoice.cboRD, Me)
        txtDiscCode.Text = ""
        txtDescription.Text = ""
        txtPercent.Text = "0.00"
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtDiscCode.Text = ""
        txtDescription.Text = ""
        txtPercent.Text = "0.00"
        Me.Close()
    End Sub

    Private Sub txtPercent_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtPercent.Validating
        txtPercent.Text = Format(CDec(txtPercent.Text), "##0.00")
    End Sub
End Class