﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frm000Popup

    Private con As New Clsappconfiguration
    Public xmode As String

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frm000Popup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If xmode = "Existing" Then
            LoadExistingBilling("")
        End If
        If xmode = "New" Then
            LoadNewDocNo("")
        End If
    End Sub

    Private Sub LoadExistingBilling(ByVal q As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "_Billing_Search_DocNo",
                                                     New SqlParameter("@key", q))
        dgvList.DataSource = ds.Tables(0)
    End Sub
    Private Sub LoadNewDocNo(ByVal q As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "[_Billing_SelectDocNumber]",
                                                     New SqlParameter("@search", q))
        dgvList.DataSource = ds.Tables(0)
    End Sub

    Private Sub txtsearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtsearch.TextChanged
        If xmode = "Existing" Then
            LoadExistingBilling(txtsearch.Text)
        End If
        If xmode = "New" Then
            LoadNewDocNo(txtsearch.Text)
        End If
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Try
            frm000BillingModule.txtDocNumber.Text = dgvList.CurrentRow.Cells(0).Value.ToString
            frm000BillingModule.dtDatePrepared.Text = dgvList.CurrentRow.Cells(1).Value.ToString
            frm000BillingModule.txtPreparedby.Text = dgvList.CurrentRow.Cells(2).Value.ToString
            Me.Close()
        Catch ex As Exception
            Me.Close()
        End Try
    End Sub
End Class