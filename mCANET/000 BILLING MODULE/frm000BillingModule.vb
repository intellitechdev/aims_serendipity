﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frm000BillingModule

#Region "Variable Declarations"
    Private con As New Clsappconfiguration
#End Region

#Region "Functions and Events"

    Private Sub frm000BillingModule_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        fillCombo()
    End Sub
    Public Sub fillCombo()
        Dim rd As SqlDataReader

        rd = LoadFilters("Group")
        cboGroup.Items.Clear()
        While rd.Read
            cboGroup.Items.Add(rd(0))
        End While
        cboGroup.Items.Add("All")
        cboGroup.Text = "All"

        rd = Nothing
        rd = LoadFilters("SubGroup")
        cboSgroup.Items.Clear()
        While rd.Read
            cboSgroup.Items.Add(rd(0))
        End While
        'cboSgroup.Items.Add("All")
        cboSgroup.Text = " "

        rd = Nothing
        rd = LoadFilters("Category")
        cboCategory.Items.Clear()
        While rd.Read
            cboCategory.Items.Add(rd(0))
        End While
        'cboCategory.Items.Add("All")
        cboCategory.Text = " "

        rd = Nothing
        rd = LoadFilters("type")
        cboType.Items.Clear()
        While rd.Read
            cboType.Items.Add(rd(0))
        End While
        'cboType.Items.Add("All")
        cboType.Text = " "
    End Sub

    Private Sub GenerateBilling_Acnt(ByVal dt As String, ByVal group As String, ByVal sg As String, ByVal cat As String, ByVal type As String, ByVal dtpTo As String)
        Dim total As Decimal = 0
        Dim cur As Decimal = 0
        Dim past As Decimal = 0
        Dim per_line_amt As Decimal = 0
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_AccountAmort_GenerateBilling",
                                                          New SqlParameter("@duedate", dt),
                                                          New SqlParameter("@xGroup", group),
                                                          New SqlParameter("@Subgroup", sg),
                                                          New SqlParameter("@Category", cat),
                                                          New SqlParameter("@Type", type),
                                                          New SqlParameter("@Rank", ""),
                                                          New SqlParameter("@Status", ""),
                                                          New SqlParameter("@dueto", dtpTo))
        If rd.HasRows Then
            While rd.Read
                With dgvBilling

                    per_line_amt = 0
                    Dim colval As String() = rd(6).Split(New Char() {","c})
                    Dim cv As String
                    For Each cv In colval
                        per_line_amt += CDec(cv)
                    Next

                    .Rows.Add(rd(0), rd(1), rd(2), rd(3), rd(4), rd(5), Format(per_line_amt, "##,##0.00"), "0.00", Format(per_line_amt, "##,##0.00"))
                    cur += per_line_amt
                    past += 0
                    total += per_line_amt
                End With
            End While
            txtTotalCurrent.Text = Format(cur, "##,##0.00")
            txtTotalPast.Text = Format(past, "##,##0.00")
            txtTotalBilling.Text = Format(total, "##,##0.00")
        End If
    End Sub

    Private Sub GenerateBilling(dt As Date, ByVal group As String, ByVal sg As String, ByVal cat As String, ByVal type As String)
        Try
            Dim total As Decimal = 0
            Dim cur As Decimal = 0
            Dim past As Decimal = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_Module_Generate",
                                                              New SqlParameter("@duedate", dt),
                                                              New SqlParameter("@xGroup", group),
                                                              New SqlParameter("@Subgroup", sg),
                                                              New SqlParameter("@Category", cat),
                                                              New SqlParameter("@Type", type),
                                                              New SqlParameter("@Rank", ""),
                                                              New SqlParameter("@Status", ""))
            If rd.HasRows Then
                While rd.Read
                    With dgvBilling
                        .Rows.Add(rd(0), rd(1), rd(2), rd(3), rd(4), rd(5), Format(CDec(rd(6)), "##,##0.00"), Format(CDec(rd(7)), "##,##0.00"), Format(CDec(rd(8)), "##,##0.00"))
                        cur += CDec(rd(6))
                        past += CDec(rd(7))
                        total += CDec(rd(8))
                    End With
                End While
                txtTotalCurrent.Text = Format(cur, "##,##0.00")
                txtTotalPast.Text = Format(past, "##,##0.00")
                txtTotalBilling.Text = Format(total, "##,##0.00")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnGenerateBilling_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerateBilling.Click
        dgvBilling.Rows.Clear()
        'Generate Billing Loans
        If chkLoans.Checked Then
            GenerateBilling(dtDueDate.Text, cboGroup.Text, cboSgroup.Text, cboCategory.Text, cboType.Text)
        End If

        'Generate Billing Other Receivables
        If chkOthers.Checked Then
            GenerateBilling_Acnt(dtDueDate.Text, cboGroup.Text, cboSgroup.Text, cboCategory.Text, cboType.Text, dtpTo.Text)
        End If
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If txtDocNumber.Text = "" Then
            MsgBox("Please Select Document Number.")
            Exit Sub
        End If
        Try
            SaveHeader()
            DeleteDetail()
            For i As Integer = 0 To dgvBilling.RowCount - 1
                With dgvBilling.Rows(i)
                    SaveDetail(.Cells(0).Value.ToString,
                        .Cells(1).Value.ToString,
                        .Cells(2).Value.ToString,
                        .Cells(3).Value.ToString,
                        .Cells(4).Value.ToString,
                        .Cells(5).Value.ToString,
                        .Cells(6).Value.ToString,
                        .Cells(7).Value.ToString,
                        .Cells(8).Value.ToString)
                End With
            Next
            UpdateDocNumberUsed()
            MsgBox("Save Successfull.")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub UpdateDocNumberUsed()
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_Module_UpdateDocNumber",
                          New SqlParameter("@docnumber", txtDocNumber.Text),
                          New SqlParameter("@dateused", dtDatePrepared.Text))
    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click
        If txtDocNumber.Text = "" Then
            MsgBox("Please Select Document to delete")
        Else
            If MsgBox("Are you sure? Delete document number " + txtDocNumber.Text + " ?", vbYesNo, "Delete Options") = vbYes Then
                DeleteBillingData(txtDocNumber.Text)
                dgvBilling.Rows.Clear()
                txtPreparedby.Text = ""
                txtDocNumber.Text = ""
                txtTotalCurrent.Text = "0.00"
                txtTotalPast.Text = "0.00"
                txtTotalBilling.Text = "0.00"
            End If
        End If
    End Sub

    Private Sub DeleteBillingData(ByVal doc As String)
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_Module_Delete_Header",
                                  New SqlParameter("@docnumber", doc))
    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        frm000Popup.xmode = "Existing"
        frm000Popup.StartPosition = FormStartPosition.CenterScreen
        frm000Popup.ShowDialog()
    End Sub
    Private Sub btnBrowseDoc_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowseDoc.Click
        frm000Popup.xmode = "New"
        frm000Popup.StartPosition = FormStartPosition.CenterScreen
        frm000Popup.ShowDialog()
    End Sub

#End Region

#Region "Models"

    Private Function LoadFilters(ByVal type As String) As SqlDataReader
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_Final_LoadFilters",
                                           New SqlParameter("@type", type))
            If rd.HasRows Then
                Return rd
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Sub SaveHeader()
        SqlHelper.ExecuteNonQuery(con.cnstring, "[_Billing_InsertUpdate_Header]",
                                  New SqlParameter("@fcDocNumber", txtDocNumber.Text),
                                  New SqlParameter("@fcCreatedBy", txtPreparedby.Text),
                                  New SqlParameter("@dtDate", dtDatePrepared.Text),
                                  New SqlParameter("@fbPosted", True))

    End Sub
    Private Sub DeleteDetail()
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_Module_Delete",
                                  New SqlParameter("@docNumber", txtDocNumber.Text))
    End Sub
    Private Sub SaveDetail(ByVal idno As String,
                           ByVal cname As String,
                           ByVal loanref As String,
                           ByVal acctreg As String,
                           ByVal code As String,
                           ByVal title As String,
                           ByVal curdue As String,
                           ByVal pastdue As String,
                           ByVal total As String)
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Billing_Module_InsertDetail",
                                  New SqlParameter("@docNumber", txtDocNumber.Text),
                                  New SqlParameter("@fcIDNo", idno),
                                  New SqlParameter("@fcName", cname),
                                  New SqlParameter("@fcLoanRef", loanref),
                                  New SqlParameter("@fcAcctRef", acctreg),
                                  New SqlParameter("@fcCode", code),
                                  New SqlParameter("@fcTitle", title),
                                  New SqlParameter("@fdDueAmt", curdue),
                                  New SqlParameter("@fdPastDue", pastdue),
                                  New SqlParameter("@fdTotal", total))
    End Sub

#End Region


    Private Sub txtDocNumber_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtDocNumber.TextChanged
        dgvBilling.Rows.Clear()
        LoadExistingBillingDetails(txtDocNumber.Text)
    End Sub

    Public Sub LoadExistingBillingDetails(ByVal docnumber As String)
        Dim total As Decimal = 0
        Dim cur As Decimal = 0
        Dim past As Decimal = 0
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Billing_Module_SelectExisting",
                                                          New SqlParameter("@docNumber", docnumber))
        If rd.HasRows Then
            While rd.Read
                With dgvBilling
                    .Rows.Add(rd(0), rd(1), rd(2), rd(3), rd(4), rd(5), Format(CDec(rd(6)), "##,##0.00"), Format(CDec(rd(7)), "##,##0.00"), Format(CDec(rd(8)), "##,##0.00"))
                    cur += CDec(rd(6))
                    past += CDec(rd(7))
                    total += CDec(rd(8))
                End With
            End While
            txtTotalCurrent.Text = Format(cur, "##,##0.00")
            txtTotalPast.Text = Format(past, "##,##0.00")
            txtTotalBilling.Text = Format(total, "##,##0.00")
        End If
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        dgvBilling.Rows.Clear()
        txtPreparedby.Text = ""
        txtDocNumber.Text = ""
        txtTotalCurrent.Text = "0.00"
        txtTotalPast.Text = "0.00"
        txtTotalBilling.Text = "0.00"
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        frmExportLoanDeductions._Print_BILLINGMODULE(txtDocNumber.Text)
        frmExportLoanDeductions.ShowDialog()
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        fillCombo()
    End Sub

    Private Sub btnRemoveRow_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveRow.Click
        txtTotalCurrent.Text = Format(CDec(txtTotalCurrent.Text) - CDec(dgvBilling.CurrentRow.Cells(6).Value), "##,##0.00")
        txtTotalPast.Text = Format(CDec(txtTotalPast.Text) - CDec(dgvBilling.CurrentRow.Cells(7).Value), "##,##0.00")
        txtTotalBilling.Text = Format(CDec(txtTotalCurrent.Text) - CDec(txtTotalPast.Text), "##,##0.00")
        dgvBilling.Rows.Remove(dgvBilling.CurrentRow)
    End Sub

End Class