﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm000BillingModule
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm000BillingModule))
        Me.dgvBilling = New System.Windows.Forms.DataGridView()
        Me.fcIDNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcFullname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LoanRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AcctRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AcctCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AcctTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CurrentDue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PastDue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalAmt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnGenerateBilling = New System.Windows.Forms.Button()
        Me.dtDueDate = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboType = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboCategory = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboSgroup = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboGroup = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtDatePrepared = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnBrowseDoc = New System.Windows.Forms.Button()
        Me.txtDocNumber = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPreparedby = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTotalBilling = New System.Windows.Forms.TextBox()
        Me.txtTotalPast = New System.Windows.Forms.TextBox()
        Me.txtTotalCurrent = New System.Windows.Forms.TextBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkOthers = New System.Windows.Forms.CheckBox()
        Me.chkLoans = New System.Windows.Forms.CheckBox()
        Me.btnRemoveRow = New System.Windows.Forms.Button()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        CType(Me.dgvBilling, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvBilling
        '
        Me.dgvBilling.AllowUserToAddRows = False
        Me.dgvBilling.AllowUserToDeleteRows = False
        Me.dgvBilling.AllowUserToResizeColumns = False
        Me.dgvBilling.AllowUserToResizeRows = False
        Me.dgvBilling.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBilling.BackgroundColor = System.Drawing.Color.White
        Me.dgvBilling.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvBilling.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvBilling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBilling.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.fcIDNo, Me.fcFullname, Me.LoanRef, Me.AcctRef, Me.AcctCode, Me.AcctTitle, Me.CurrentDue, Me.PastDue, Me.TotalAmt})
        Me.dgvBilling.Location = New System.Drawing.Point(12, 12)
        Me.dgvBilling.Name = "dgvBilling"
        Me.dgvBilling.RowHeadersVisible = False
        Me.dgvBilling.Size = New System.Drawing.Size(1208, 373)
        Me.dgvBilling.TabIndex = 0
        '
        'fcIDNo
        '
        Me.fcIDNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.fcIDNo.FillWeight = 79.35844!
        Me.fcIDNo.HeaderText = "ID No."
        Me.fcIDNo.Name = "fcIDNo"
        '
        'fcFullname
        '
        Me.fcFullname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.fcFullname.FillWeight = 182.7411!
        Me.fcFullname.HeaderText = "Name"
        Me.fcFullname.Name = "fcFullname"
        '
        'LoanRef
        '
        Me.LoanRef.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.LoanRef.FillWeight = 79.35844!
        Me.LoanRef.HeaderText = "Loan Ref."
        Me.LoanRef.Name = "LoanRef"
        '
        'AcctRef
        '
        Me.AcctRef.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.AcctRef.FillWeight = 79.35844!
        Me.AcctRef.HeaderText = "Acct. Ref"
        Me.AcctRef.Name = "AcctRef"
        '
        'AcctCode
        '
        Me.AcctCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.AcctCode.FillWeight = 79.35844!
        Me.AcctCode.HeaderText = "Code"
        Me.AcctCode.Name = "AcctCode"
        '
        'AcctTitle
        '
        Me.AcctTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.AcctTitle.FillWeight = 161.7497!
        Me.AcctTitle.HeaderText = "Title"
        Me.AcctTitle.Name = "AcctTitle"
        '
        'CurrentDue
        '
        Me.CurrentDue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CurrentDue.FillWeight = 79.35844!
        Me.CurrentDue.HeaderText = "Current Due"
        Me.CurrentDue.Name = "CurrentDue"
        '
        'PastDue
        '
        Me.PastDue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.PastDue.FillWeight = 79.35844!
        Me.PastDue.HeaderText = "Past Due"
        Me.PastDue.Name = "PastDue"
        '
        'TotalAmt
        '
        Me.TotalAmt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TotalAmt.FillWeight = 79.35844!
        Me.TotalAmt.HeaderText = "Total Amount"
        Me.TotalAmt.Name = "TotalAmt"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpTo)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.btnGenerateBilling)
        Me.GroupBox1.Controls.Add(Me.dtDueDate)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cboType)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cboCategory)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cboSgroup)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cboGroup)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 391)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(383, 206)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filters"
        '
        'btnGenerateBilling
        '
        Me.btnGenerateBilling.Location = New System.Drawing.Point(103, 175)
        Me.btnGenerateBilling.Name = "btnGenerateBilling"
        Me.btnGenerateBilling.Size = New System.Drawing.Size(217, 25)
        Me.btnGenerateBilling.TabIndex = 10
        Me.btnGenerateBilling.Text = "Generate Billing"
        Me.btnGenerateBilling.UseVisualStyleBackColor = True
        '
        'dtDueDate
        '
        Me.dtDueDate.CustomFormat = "MMMM dd , yyy"
        Me.dtDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDueDate.Location = New System.Drawing.Point(103, 123)
        Me.dtDueDate.Name = "dtDueDate"
        Me.dtDueDate.Size = New System.Drawing.Size(216, 20)
        Me.dtDueDate.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(52, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "From :"
        '
        'cboType
        '
        Me.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboType.FormattingEnabled = True
        Me.cboType.Location = New System.Drawing.Point(102, 96)
        Me.cboType.Name = "cboType"
        Me.cboType.Size = New System.Drawing.Size(216, 21)
        Me.cboType.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(53, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Type :"
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(103, 69)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(216, 21)
        Me.cboCategory.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Category :"
        '
        'cboSgroup
        '
        Me.cboSgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSgroup.FormattingEnabled = True
        Me.cboSgroup.Location = New System.Drawing.Point(102, 41)
        Me.cboSgroup.Name = "cboSgroup"
        Me.cboSgroup.Size = New System.Drawing.Size(216, 21)
        Me.cboSgroup.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Sub-Group :"
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(102, 14)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(216, 21)
        Me.cboGroup.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(47, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Group :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtDatePrepared)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.btnBrowseDoc)
        Me.GroupBox2.Controls.Add(Me.txtDocNumber)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtPreparedby)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Location = New System.Drawing.Point(413, 417)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(382, 138)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Billing Parameters"
        '
        'dtDatePrepared
        '
        Me.dtDatePrepared.CustomFormat = "MMMM dd , yyyy"
        Me.dtDatePrepared.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDatePrepared.Location = New System.Drawing.Point(118, 99)
        Me.dtDatePrepared.Name = "dtDatePrepared"
        Me.dtDatePrepared.Size = New System.Drawing.Size(200, 20)
        Me.dtDatePrepared.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 105)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Date Prepared :"
        '
        'btnBrowseDoc
        '
        Me.btnBrowseDoc.Location = New System.Drawing.Point(299, 53)
        Me.btnBrowseDoc.Name = "btnBrowseDoc"
        Me.btnBrowseDoc.Size = New System.Drawing.Size(61, 23)
        Me.btnBrowseDoc.TabIndex = 4
        Me.btnBrowseDoc.Text = "...."
        Me.btnBrowseDoc.UseVisualStyleBackColor = True
        '
        'txtDocNumber
        '
        Me.txtDocNumber.Location = New System.Drawing.Point(118, 55)
        Me.txtDocNumber.Name = "txtDocNumber"
        Me.txtDocNumber.ReadOnly = True
        Me.txtDocNumber.Size = New System.Drawing.Size(175, 20)
        Me.txtDocNumber.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 58)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Doc. Number :"
        '
        'txtPreparedby
        '
        Me.txtPreparedby.Location = New System.Drawing.Point(118, 29)
        Me.txtPreparedby.Name = "txtPreparedby"
        Me.txtPreparedby.Size = New System.Drawing.Size(242, 20)
        Me.txtPreparedby.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Prepared By :"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(832, 405)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(55, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Totals :"
        '
        'txtTotalBilling
        '
        Me.txtTotalBilling.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalBilling.Location = New System.Drawing.Point(1115, 398)
        Me.txtTotalBilling.Name = "txtTotalBilling"
        Me.txtTotalBilling.Size = New System.Drawing.Size(105, 20)
        Me.txtTotalBilling.TabIndex = 7
        Me.txtTotalBilling.Text = "0.00"
        Me.txtTotalBilling.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPast
        '
        Me.txtTotalPast.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalPast.Location = New System.Drawing.Point(1004, 398)
        Me.txtTotalPast.Name = "txtTotalPast"
        Me.txtTotalPast.Size = New System.Drawing.Size(105, 20)
        Me.txtTotalPast.TabIndex = 8
        Me.txtTotalPast.Text = "0.00"
        Me.txtTotalPast.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalCurrent
        '
        Me.txtTotalCurrent.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalCurrent.Location = New System.Drawing.Point(893, 398)
        Me.txtTotalCurrent.Name = "txtTotalCurrent"
        Me.txtTotalCurrent.Size = New System.Drawing.Size(105, 20)
        Me.txtTotalCurrent.TabIndex = 9
        Me.txtTotalCurrent.Text = "0.00"
        Me.txtTotalCurrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(917, 556)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(93, 33)
        Me.btnDelete.TabIndex = 11
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(671, 556)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(117, 34)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "Save Billing"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.Image = CType(resources.GetObject("btnSearch.Image"), System.Drawing.Image)
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(794, 556)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(117, 34)
        Me.btnSearch.TabIndex = 5
        Me.btnSearch.Text = "Search Billing"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Image = CType(resources.GetObject("btnPrint.Image"), System.Drawing.Image)
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(1014, 556)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(112, 34)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print Billing"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(1132, 556)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(88, 33)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID No."
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.FillWeight = 182.7411!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Loan Ref."
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn4.HeaderText = "Acct. Ref"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn5.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.FillWeight = 161.7497!
        Me.DataGridViewTextBoxColumn6.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn7.HeaderText = "Current Due"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn8.HeaderText = "Past Due"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.FillWeight = 79.35844!
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total Amount"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.Image = CType(resources.GetObject("btnNew.Image"), System.Drawing.Image)
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNew.Location = New System.Drawing.Point(591, 556)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(74, 34)
        Me.btnNew.TabIndex = 12
        Me.btnNew.Text = "New"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(503, 556)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(82, 34)
        Me.btnRefresh.TabIndex = 13
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkOthers)
        Me.GroupBox3.Controls.Add(Me.chkLoans)
        Me.GroupBox3.Location = New System.Drawing.Point(835, 429)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 100)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Billing Options"
        '
        'chkOthers
        '
        Me.chkOthers.AutoSize = True
        Me.chkOthers.Location = New System.Drawing.Point(45, 64)
        Me.chkOthers.Name = "chkOthers"
        Me.chkOthers.Size = New System.Drawing.Size(128, 17)
        Me.chkOthers.TabIndex = 1
        Me.chkOthers.Text = "Other Receivables"
        Me.chkOthers.UseVisualStyleBackColor = True
        '
        'chkLoans
        '
        Me.chkLoans.AutoSize = True
        Me.chkLoans.Location = New System.Drawing.Point(44, 33)
        Me.chkLoans.Name = "chkLoans"
        Me.chkLoans.Size = New System.Drawing.Size(56, 17)
        Me.chkLoans.TabIndex = 0
        Me.chkLoans.Text = "Loans"
        Me.chkLoans.UseVisualStyleBackColor = True
        '
        'btnRemoveRow
        '
        Me.btnRemoveRow.Location = New System.Drawing.Point(623, 395)
        Me.btnRemoveRow.Name = "btnRemoveRow"
        Me.btnRemoveRow.Size = New System.Drawing.Size(172, 23)
        Me.btnRemoveRow.TabIndex = 15
        Me.btnRemoveRow.Text = "Remove Row"
        Me.btnRemoveRow.UseVisualStyleBackColor = True
        '
        'dtpTo
        '
        Me.dtpTo.CustomFormat = "MMMM dd , yyy"
        Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTo.Location = New System.Drawing.Point(102, 149)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(216, 20)
        Me.dtpTo.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(63, 151)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "To :"
        '
        'frm000BillingModule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(1232, 601)
        Me.Controls.Add(Me.btnRemoveRow)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtTotalCurrent)
        Me.Controls.Add(Me.txtTotalPast)
        Me.Controls.Add(Me.txtTotalBilling)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvBilling)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm000BillingModule"
        Me.Text = "Billing Module"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvBilling, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvBilling As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboType As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboSgroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents dtDueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnGenerateBilling As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBrowseDoc As System.Windows.Forms.Button
    Friend WithEvents txtDocNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPreparedby As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtDatePrepared As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtTotalBilling As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcIDNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcFullname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LoanRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AcctRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AcctCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AcctTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CurrentDue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PastDue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotalPast As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalCurrent As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkOthers As System.Windows.Forms.CheckBox
    Friend WithEvents chkLoans As System.Windows.Forms.CheckBox
    Friend WithEvents btnRemoveRow As System.Windows.Forms.Button
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class
